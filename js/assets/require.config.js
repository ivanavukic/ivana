require.config({
  paths: {
    // core: "./js/assets/core",
    app: "./js/app",
    //jquery: "lib/jquery-min",
    //"jquery-ui": "lib/jquery-ui",
    underscore: "./js/lib/underscore-min",
    bluebird: "./js/lib/bluebird",
    coreLogin: "./js/assets/Core/coreLogin",
    // appConfig: "./appConfig",
    coreChanEpg: "./js/assets/Core/coreChanEpg",
    // coreSearch: "./js/assets/Core/coreSearch",
    // coreSettings: "./js/assets/Core/coreSettings",
    coreVods: "./js/assets/Core/coreVods",
    // coreHeartbeat: "./js/assets/Core/coreHeartbeat",
    // coreConfiguration: "./js/assets/Core/coreConfiguration",
  },
});

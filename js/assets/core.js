define("core", ["bluebird"], function (Promise) {
  //Define foo/title object in here.
  //OnLineHeartbeat, PlayChannelHeartbeat, PlayVODHeartbeat, or PlayNPVRHeartbeat

  //https://10.222.0.39:443/
  //http://10.222.0.39//
  //http://mts.local:3333/

  var tv = { delay: 0, lastCall: 0 };
  tv.basicRequestWithUrl = function (options) {
    return tv.basicRequest({
      url: apiEndpoint,
      url1: options.url1,
      data: options.data,
    });
  };

  tv.basicRequestWithUrlDelay = function (options) {
    if (new Date().getTime() - tv.lastCall > 200) {
      tv.lastCall = new Date().getTime();
      tv.delay = 0;
    } else {
      tv.delay = 200 - (new Date().getTime() - tv.lastCall) + tv.delay;
      tv.lastCall = new Date().getTime();
    }
    console.log(tv.delay, tv.lastCall);
    return new Promise(function (resolve, reject) {
      setTimeout(function () {
        tv.basicRequest({
          url: apiEndpoint,
          url1: options.url1,
          data: options.data,
        })
          .then(function (res) {
            resolve(res);
          })
          .catch(function (err) {
            reject(err);
          });
      }, tv.delay);
    });
  };
  /**
   *
   * @param {object||{}} options
   * @param {string} options.url
   * @param {object} [options.data]
   * @param {string} [options.url1]
   * @param {string} [options.method='POST']
   * @param {string} [options.authorization]
   */
  ////////////    PROVERI  ////////////
  tv.basicRequest = function (options) {
    //console.log(options);
    return new Promise(function (resolve, reject) {
      var odgovor;
      options = options || {};
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (
          this.readyState === 2 &&
          !(this.status > 200) &&
          !(this.status <= 400)
        ) {
          reject("Failed");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          //console.log(odgovor);

          if (odgovor.retCode == 200) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(options.method, options.url + options.url1);

      if (options.authorization) {
        xhr.setRequestHeader("Authorization", options.authorization);
      }
      if (options.method === "POST") {
        xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
      }
      if (options.data) {
        xhr.send(JSON.stringify(options.data));
      } else {
        xhr.send();
      }
    });
  };

  // setTimeout(function () {
  //   chanEpg._kanaliDynamicProps().then((res) => console.log(res));
  //   chanEpg.getKanale("", "").then((res) => console.log(res));
  //   chanEpg
  //     .getTrenutniEpg(1208)
  //     .then((res) => console.log(res))
  //     .then(() => chanEpg.getNapredEpg(1208).then((res) => console.log(res)));
  // }, 2000);
  // setTimeout(function () {
  //   searches.searchAll("vetar").then((res) => console.log(res));
  // }, 2000);
  // setTimeout(function () {
  //   searches.searchAll("vetar").then((res) => console.log(res));
  // }, 2000);
  // setTimeout(function () {
  //   vods.getVodRecm().then((res) => console.log(res));
  // }, 2000);
  // setTimeout(function () {
  //   settings.queryProfile().then((res) => console.log(res));
  // }, 2000);

  return tv;
});

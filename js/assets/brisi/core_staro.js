// ***** ISPRAVI DA SU TI SVE PUTANJE BEZ POCETNOG / znaka
// NEGO NEKA BUDU tipa src=image/xxx//
//
// ***********************OVO STAVI NA POCETAK DOKUMENTA
// <!doctype html>
//<html lang="no" manifest="cache.manifest">
//
//******************************* NA BODY DODAJES
// <body onload="onInit()">
//
// ********************************** A SAM SKRIPT DODAJES NAKON ZATVORENOG BODY TAGA
//</body>
//<script src="dodaj.js"></script>
//</html>
//
var mopMP = null;

var apiEndpoint = "http://mts.local:3333/";
var apiEndpoint1 = "http://iris.mts.rs/";
var apiEndpoint2 = "https://app-iris.mts-si.tv/";
var apiEndpoint3 = "";

var lastChanId = 0;
var lastChanNum = 0;

/*
window.onerror = function (msg, url, line, col, error) {
  var extra = !col ? "" : "\ncolumn: " + col;
  extra += !error ? "" : "\nerror: " + error;
  dbgLog("Error: " + msg + " URL: " + url + " LINE: " + line + extra);
  $("#dbgDiv").css("display", "block");
  var suppressErrorAlert = true;
  return suppressErrorAlert;
};
*/

if (!window["Utility"]) {
  window["Utility"] = {
    isMock: true,

    trigger: function (event /*type,error_code*/) {
      this.event = event;
      document.onkeydown({
        which: 768,
      });
    },

    getValueByName: function (key) {
      return localStorage.getItem(key);
    },
    setValueByName: function (key, value) {
      localStorage.setItem(key, value);
    },
    getEvent: function () {
      return JSON.stringify(this.event);
    },
  };
  window["iPanel"] = {
    ioctlRead: function () {},
  };
}

function setStore(name, val) {
  Utility.setValueByName(name, val);
}
function getStore(name) {
  return Utility.getValueByName(name);
}

function onInit() {
  if (typeof Utility != "undefined" && Utility !== null) {
    setStore("userstatus", 0);
    setStore("ShowPic", "2");
    setStore("EPGReady", "XXXX");
  }
}

function clearEpgCache() {
  setStore("ClearEPGCache", 1);
  setStore("STBMonitorEnable", 1);
  setStore("System.op.Reboot", 1);
}

function openSettings() {
  setStore("hw_op_stb", "setpage");
}

function dbgLog(line) {
  if (line == "") {
    document.getElementById("dbgDiv").innerHTML = "";
  } else {
    document.getElementById("dbgDiv").innerHTML = "<mark>" + line + "</mark>" + "<br>" + document.getElementById("dbgDiv").innerHTML.replace("<mark>", "</mark>");
  }
}

// SCREEN SCALE
// PERCENT UP OR DOWN
function epgScale(wup, hup) {
  var curW = parseInt(getStore("displayZoomWidth"));
  var curH = parseInt(getStore("displayZoomHeight"));

  if (wup != 0) {
    curW = curW + wup;
    setStore("displayZoomWidth", curW);
  }

  if (hup != 0) {
    curH = curH + hup;
    setStore("displayZoomHeight", curH);
  }
}

function default_key(keyCode) {
  if (keyCode == 768) {
    return false;
  } // NEKI EVENT-i

  switch (keyCode) {
    case KEY_RED:
      clearEpgCache();
      return false;
      break;
    case KEY_BLUE:
      setStore("ClearEPGCache", 1);
      setStore("STBMonitorEnable", 1);
      location.reload();
      return false;
      break;
    case KEY_YELLOW:
      if ($('link[href="css/style.css"]').attr("href") == "css/style.css") {
        $('link[href="css/style.css"]').attr("href", "css/style_dark.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      } else if ($('link[href="css/style_dark.css"]').attr("href") == "css/style_dark.css") {
        $('link[href="css/style_dark.css"]').attr("href", "css/style.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      }

      break;
    case KEY_SETTINGS:
      openSettings();
      break;
    case KEY_F1:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
    case KEY_GREEN:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
  }
}

function set_previous_element_focus() {
  var focused = document.activeElement;
  $("#id_previous_focus").attr("focus_element", focused.id);
}

function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join(".");
}

function formatDate_yyyymmdd(date, param) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;
  //if (day.length < 2) day =  day;

  return [year, month, day].join(param);
}

function formatTime(date) {
  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth() < 10 ? "0" : "") + date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours() < 10 ? "0" : "") + date.getHours();
  var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();

  return hours + ":" + minutes;
}

function formatTimeEpoch(date) {
  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth() < 10 ? "0" : "") + date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours() < 10 ? "0" : "") + date.getHours();
  var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();

  return hours + ":" + minutes;
}

var isStbBox = false;
var stbModel = getStore("STB_model");
if (typeof Utility != "undefined" && Utility !== null && stbModel !== "" && stbModel !== null) {
  isStbBox = true;
}

var username = "testui";
var domain = "IPTV";
var pass = "1111";
var device = "EC2108V5";
var macAddress = "6C:EF:C6:59:56:09";
var chipSN = "";
var stbSerial = "";
var cnonce = "";
var MQMURL = "";
var SQMURL = "";

var cSessID = "";
var jSessID = "";
var usrToken = "";
var devId = "";

var CSMIP = "";
var srvAddr = "";
var CSMPort = "";
var company = "";
var VKSAddr = "";
var srvPort = "";

if (isStbBox) {
  apiEndpoint = "http://10.222.0.39:33200/";
  username = getStore("userid");
  domain = getStore("domain");
  device = getStore("STB_model");
  chipSN = getStore("ChipSeriaNumber");
  macAddress = getStore("MACAddress");
  stbSerial = getStore("STBSerialNumber");
  cnonce = getStore("cnonce");
  dbgLog("USER: " + username + " device: " + device + " mac: " + macAddress);
}

function platforma(url) {
  this.url = url;
  this.token = "";
  this.epg = [];
  this.epgProsloVreme = "123";
  this.epgBuduceVreme = "321";
  this.epgList = [];
  this.epgListAll = [];
  this.tvChannels = [];
  //this.radioChannels = [];
  this.recmTvChannels = [];
  this.vodList = {
    filmovi: [],
    serije: [],
  };
  this.vodCatalogue = "";
  this.trenutnoNaTv = {};
  this.favoriti;
  this.zakljucaniKanali;
  this.kategorije = [];
  this.popularniSearch;
  this.profileModifier = {
    reminderInterval: "10", //opcije su 0, 5, 10, 15
    language: "sr", //opcije su "en" i "sr"
    parentalControlID: "180", //opcije su 0, 7, 12, 14, 16, 18, 180 (18+)
    spendLimit: 1000, //opcije su 0, 500, 1000, 2000, -1 (neograniceno)
    profileName: "testui", //menja ime profila u zeljeni string
  };
}

platforma.prototype.login = function (sync) {
  // ≤1 time/20s
  var data = JSON.stringify({
    subscriberID: username,
    deviceModel: device,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      var obj = JSON.parse(xhr.responseText);
      MQMURL = obj.parameters.MQMCURL;
      SQMURL = obj.parameters.SQMCURL;
    }
  });
  xhr.open("POST", this.url + "VSP/V3/Login", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
};
platforma.prototype._setSystemEnv = function (obj) {
  cSessID = obj.cSessionId;
  jSessID = obj.jSessionID;
  usrToken = obj.userToken;
  var sqmToken = obj.sqmToken;
  var sqmSessID = obj.sqmSessionId;
  var antiTamperURI = obj.antiTamperURI;
  setStore("antiTamperURI", antiTamperURI);
  setStore("sqmToken", sqmToken);
  setStore("sqmSessionId", sqmSessID);

  devId = obj.deviceID;
  dbgLog("Authen 3 cSessID: " + cSessID + " jSessID: " + jSessID);
  //dbgLog("deviceID: " + devId);
  CSMIP = obj.CA.verimatrix.CSMIP;
  srvAddr = obj.CA.verimatrix.serverAddr;
  CSMPort = obj.CA.verimatrix.CSMPort;
  company = obj.CA.verimatrix.company;
  VKSAddr = obj.CA.verimatrix.VKSAddr;
  srvPort = obj.CA.verimatrix.serverPort;
  //*setStore("deviceId", devId);
  //*setStore("deviceId", chipSN);
  //setStore("deviceId", stbSerial);
  setStore("usertoken", usrToken);
  setStore("sessionid", jSessID);
  setStore("MQMCURL", MQMURL);
  setStore("SQMURL", SQMURL);
  setStore("MacrovisionEnableDefault", "3");
  setStore("HDCPEnableDefault", "0");
  setStore("CGMSAEnableDefault", "0");
  setStore("Verimatrix.Company", company);
  setStore("Verimatrix.ServerAddress", srvAddr);
  setStore("Verimatrix.ServerPort", srvPort);
  setStore("Verimatrix.PreferredVKS", VKSAddr);
  setStore("Verimatrix.OTT.ServerAddress", CSMIP);
  setStore("Verimatrix.OTT.ServerPort", CSMPort);
  setStore("CA_Enable", "1");

  setStore("epgurl", apiEndpoint + "EPG/");

  setStore("hw_op_refreshchannellist", "XXXX");
  dbgLog("Parse Authen OK");
};
platforma.prototype._setSystemEnv1 = function (obj) {
  if (obj.result.retCode == "000000000") {
    if (isStbBox) {
      this._setSystemEnv(obj);
    }
  }
};
platforma.prototype.auth = function (sync) {
  //  ≤1 time/20s
  var caDevice = macAddress;
  if (device == "Q11") {
    ceDevice = chipSN;
  }
  var data = JSON.stringify({
    authenticateBasic: {
      userID: username,
      userType: "0",
      clientPasswd: "1111",
      isSupportWebpImgFormat: "1",
      needPosterTypes: ["1", "2", "3", "4", "5", "6", "7"],
      timeZone: "Europe/Belgrade",
      authType: "0",
      lang: "en",
      pageTrackerUIType: "HW_STB_EPGUI",
      //cnonce: cnonce,
    },
    authenticateDevice: {
      physicalDeviceID: macAddress,
      deviceModel: device + "@" + domain,
      CADeviceInfo: {
        CADeviceID: caDevice,
        CADeviceType: "3",
      },
    },
    authenticateTolerant: {
      areaId: "32",
      templateName: "default",
      subnetID: "38101",
      bossID: "TSBOSS",
      userGroup: "-1",
    },
  });

  var xhr = new XMLHttpRequest();
  var varijabla = this;
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      var obj = JSON.parse(xhr.responseText);
      varijabla._setSystemEnv1(obj);
      varijabla.token = JSON.parse(this.responseText).userToken;
      console.log(JSON.parse(this.responseText).userToken);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/Authenticate", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
};
platforma.prototype.switchProfile = function (sync) {
  // ≤1 time/20s
  var data = JSON.stringify({
    profileID: username,
    loginName: username,
    password: pass,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      var obj = JSON.parse(xhr.responseText);
      if (obj.result.retCode == "000000000") {
        //dbgLog("OK SWITCH V3");
      }
    }
  });

  xhr.open("POST", this.url + "VSP/V3/SwitchProfile", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
};
platforma.prototype._kategorijeKanala = function (sync) {
  //≤2 times/s
  var data = '{"subjectID":"1000","offset":0,"count":50}';
  var odgovor;
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText).subjects;
      odgovor.splice(0, 0, {
        code: "1",
        name: "Svi kanali",
        ID: "1",
        contentType: "VIDEO_CHANNEL",
      });

      odgovor.splice(1, 0, {
        code: "2",
        name: "Omiljeni",
        ID: "2",
        contentType: "VIDEO_CHANNEL",
      });
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryChannelSubjectList", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  this.kategorije = odgovor;
};
platforma.prototype.getKategorije = function (sync) {
  if (this.kategorije.length > 0) {
    return this.kategorije;
  } else {
    this._kategorijeKanala(sync);
    return this.kategorije;
  }
};
platforma.prototype._favorite = function (sync) {
  //≤2 times/s
  var data = '{"contentTypes":["CHANNEL"],"ratingID":"180","sortType":"FAVO_TIME:DESC","offset":0,"count":50}';
  var odgovor;
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      // console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryFavorite", sync);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
  this.favoriti = odgovor;
};
platforma.prototype._kanaliDynamicProps = function () {
  //≤1 time/15min
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      //console.log(JSON.parse(this.responseText));
    }
  });

  xhr.open("POST", apiEndpoint + "/VSP/V3/QueryAllChannelDynamicProperties", false);

  xhr.send();
};
platforma.prototype._kanali1 = function (sync) {
  var nizKanala = [];
  var date = new Date();
  var apiDatum = date.getFullYear() + "" + (date.getMonth() + 1) + "" + date.getDate() + "" + date.getHours() + "" + date.getMinutes();
  var odgovor;
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      //console.log(this.responseText);
      odgovor = this.responseText;
    }
  });
  xhr.open("GET", this.url + "EPG/jsp/getchannellist.jsp?usertoken=" + this.token + "&User=" + username + "&pwd=&NTID=" + macAddress + "&ip=&Version=" + device + "&lang=1&channellisttype=0&checkwords=&timestamp=" + apiDatum, sync);

  xhr.send();
  nizKanala = odgovor.split("ChanID");
  nizKanala.shift();
  var dodatniKanalInfo = [];
  for (var i = 0; i < nizKanala.length; i++) {
    var tempKanal = {
      ChanStatus: "",
      PreviewEnable: "",
      ChanKey: "",
      PLTVEnable: "",
      PauseLength: "",
      ChanURL: "",
    };
    nizKanala[i] = "ChanID" + nizKanala[i];
    nizKanala[i] = nizKanala[i].split("\n");
    tempKanal.ChanStatus = nizKanala[i][1].split("ChanStatus=");
    tempKanal.ChanStatus = tempKanal.ChanStatus[1];
    tempKanal.PreviewEnable = nizKanala[i][2].split("PreviewEnable=");
    tempKanal.PreviewEnable = tempKanal.PreviewEnable[1];
    tempKanal.ChanKey = nizKanala[i][3].split("ChanKey=");
    tempKanal.ChanKey = tempKanal.ChanKey[1];
    tempKanal.PLTVEnable = nizKanala[i][4].split("PLTVEnable=");
    tempKanal.PLTVEnable = tempKanal.PLTVEnable[1];
    tempKanal.PauseLength = nizKanala[i][5].split("PauseLength=");
    tempKanal.PauseLength = tempKanal.PauseLength[1];
    tempKanal.ChanURL = nizKanala[i][9].split("ChanURL=");
    tempKanal.ChanURL = tempKanal.ChanURL[1].substring(0, tempKanal.ChanURL[1].length - 3);
    dodatniKanalInfo.push(tempKanal);
  }
  //console.log(nizKanala);
  return dodatniKanalInfo;
};
platforma.prototype._kanali = function (sync) {
  //≤1 time/15min
  var odgovor;
  this._favorite(sync);
  this.queryLocked(username, "CHANNEL", sync);
  var dodatniInfo = this._kanali1(sync);
  var data = JSON.stringify({
    profileID: username,
    password: "1111",
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
      console.log(odgovor);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryAllChannel", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);

  for (var i = 0; i < odgovor.channelDetails.length; i++) {
    var tempKanal = {
      idKanala: "",
      idOriginal: "",
      mediaId: "",
      omiljeni: 0,
      zakljucan: 0,
      slika: "",
      kategorija: "",
      ime: "",
      ChanStatus: dodatniInfo[i].ChanStatus,
      PreviewEnable: dodatniInfo[i].PreviewEnable,
      ChanKey: dodatniInfo[i].ChanKey,
      PLTVEnable: dodatniInfo[i].PLTVEnable,
      PauseLength: dodatniInfo[i].PauseLength,
      ChanURL: dodatniInfo[i].ChanURL,
    };
    if (this.favoriti.favorites) {
      for (var j = 0; j < this.favoriti.favorites.length; j++) {
        if (this.favoriti.favorites[j].channel.ID == odgovor.channelDetails[i].ID) {
          tempKanal.omiljeni = 1;
        }
      }
    }

    if (this.zakljucaniKanali.locks) {
      for (var x = 0; x < this.zakljucaniKanali.locks.length; x++) {
        if (this.zakljucaniKanali.locks[x].channel.ID == odgovor.channelDetails[i].ID) {
          tempKanal.zakljucan = 1;
        }
      }
    }

    tempKanal.idKanala = i;
    tempKanal.idOriginal = odgovor.channelDetails[i].ID;
    if (odgovor.channelDetails[i].picture) tempKanal.slika = i <= 230 ? "image/channel/" + odgovor.channelDetails[i].name + ".png" : odgovor.channelDetails[i].picture.icons[0];

    for (var x = 0; x < odgovor.channelDetails[i].subjectIDs.length; x++) {
      if (odgovor.channelDetails[i].subjectIDs[x].length == 4) {
        tempKanal.kategorija = odgovor.channelDetails[i].subjectIDs[x];
        break;
      }
    }
    tempKanal.ime = odgovor.channelDetails[i].name;
    tempKanal.mediaId = odgovor.channelDetails[i].physicalChannels[0].ID;

    //if (odgovor.channelDetails[i].contentType == "VIDEO_CHANNEL")
    this.tvChannels.push(tempKanal);
    //else this.radioChannels.push(tempKanal);
  }
};
// platforma.prototype.getRadio = function () {
//   if (this.radioChannels.length == 0) this._kanali();
//   return this.radioChannels;
// };
platforma.prototype.getKanale = function (cat, fav, sync) {
  // ≤1 time/15min
  var filteredChannels = [];
  if (this.tvChannels.length == 0) {
    this._kanali(sync);
  }
  if (typeof cat == "number" && typeof fav == "number") {
    for (var i = 0; i < this.tvChannels.length; i++) {
      if (this.tvChannels[i].kategorija == cat && this.tvChannels[i].omiljeni == 1) filteredChannels.push(this.tvChannels[i]);
    }
    return filteredChannels;
  } else if (typeof cat == "number") {
    for (var i = 0; i < this.tvChannels.length; i++) {
      if (this.tvChannels[i].kategorija == cat) filteredChannels.push(this.tvChannels[i]);
    }
    return filteredChannels;
  } else if (typeof fav == "number") {
    for (var i = 0; i < this.tvChannels.length; i++) {
      if (this.tvChannels[i].omiljeni == 1) filteredChannels.push(this.tvChannels[i]);
    }
    return filteredChannels;
  } else {
    return this.tvChannels;
  }
};
platforma.prototype.getKanalItem = function (id, sync) {
  if (this.tvChannels.length == 0) {
    this._kanali(sync);
  }
  for (var i = 0; i < this.tvChannels.length; i++) {
    if (id == this.tvChannels[i].idOriginal) return this.tvChannels[i];
  }
};
platforma.prototype.getTrenutniEpg = function (chanId, sync) {
  //≤5 times/s
  if (this.epg.length > 0) {
    if (this.epg[0].channelID != chanId) {
      console.log("drugi kanal");
      this.epg = [];
    }
  }
  var trenutniDatum = new Date();
  var triNazad = new Date(trenutniDatum.getTime() - 10800000);
  var triNapred = new Date(trenutniDatum.getTime() + 10800000);
  var odgovor;
  var data = JSON.stringify({
    needChannel: 0,
    queryChannel: {
      channelIDs: [chanId],
    },
    queryPlaybill: {
      type: 0,
      startTime: triNazad.getTime(),
      endTime: triNapred.getTime(),
      count: 100,
      offset: 0,
      isFillProgram: 2,
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      //console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText).channelPlaybills[0].playbillLites;
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.setRequestHeader("Accept", "*/*");

  xhr.send(data);
  this.epgProsloVreme = odgovor[0].startTime;
  this.epgBuduceVreme = odgovor[odgovor.length - 1].endTime;
  this.epg = odgovor;
  for (var i = 0; i < this.tvChannels.length; i++) {
    if (chanId == this.tvChannels[i].idOriginal) {
      for (var j = 0; j < this.epg.length; j++) {
        this.epg[j].mediaId = this.tvChannels[i].mediaId;
      }
      break;
    }
  }
  return this.epg;
};
platforma.prototype.getNapredEpg = function (chanId, sync) {
  //≤5 times/s
  var odgovor;
  var a = this.epgBuduceVreme;
  var b = this.epgBuduceVreme * 1 + 21600000;
  console.log(a, b);
  var data = JSON.stringify({
    needChannel: 0,
    queryChannel: {
      channelIDs: [chanId],
    },
    queryPlaybill: {
      type: 0,
      startTime: a,
      endTime: b,
      count: 100,
      offset: 0,
      isFillProgram: 2,
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText).channelPlaybills[0].playbillLites;
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.setRequestHeader("Accept", "*/*");

  xhr.send(data);
  this.epgBuduceVreme = odgovor[odgovor.length - 1].endTime;
  for (var i = 0; i < odgovor.length; i++) {
    this.epg.push(odgovor[i]);
  }
  for (var i = 0; i < this.tvChannels.length; i++) {
    if (chanId == this.tvChannels[i].idOriginal) {
      for (var j = 0; j < this.epg.length; j++) {
        this.epg[j].mediaId = this.tvChannels[i].mediaId;
      }
      break;
    }
  }
  return this.epg;
};
platforma.prototype.getNazadEpg = function (chanId, sync) {
  // ≤5 times/s
  var odgovor;
  var a = this.epgProsloVreme * 1 - 21600000;
  var b = this.epgProsloVreme;
  //console.log(a, b);
  var data = JSON.stringify({
    needChannel: 0,
    queryChannel: {
      channelIDs: [chanId],
    },
    queryPlaybill: {
      type: 0,
      startTime: a,
      endTime: b,
      count: 100,
      offset: 0,
      isFillProgram: 2,
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText).channelPlaybills[0].playbillLites;
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
  xhr.setRequestHeader("Content-type", "application/json");
  xhr.setRequestHeader("Accept", "*/*");

  xhr.send(data);
  this.epgProsloVreme = odgovor[0].startTime;
  for (var i = odgovor.length - 1; i >= 0; i--) {
    this.epg.unshift(odgovor[i]);
  }
  for (var i = 0; i < this.tvChannels.length; i++) {
    if (chanId == this.tvChannels[i].idOriginal) {
      for (var j = 0; j < this.epg.length; j++) {
        this.epg[j].mediaId = this.tvChannels[i].mediaId;
      }
      break;
    }
  }
  return this.epg;
};
platforma.prototype.getTrenutno = function (chanId) {
  var danas = new Date();
  var trenutniEpoch = danas.getTime();
  if (this.epg.length == 0) {
    console.log("if");
    this.getTrenutniEpg(chanId, false);
  } else if (this.epg[0].channelID != chanId) {
    console.log("else if", this.epg[0].channelID);
    this.getTrenutniEpg(chanId, false);
  }
  for (var i = 0; i < this.epg.length; i++) {
    if (trenutniEpoch > this.epg[i].startTime && trenutniEpoch < this.epg[i].endTime) {
      return this.epg[i];
    }
  }
};
platforma.prototype.getEpgItem = function (epgId) {
  if (this.epg.length == 0) return;
  for (var i = 0; i < this.epg.length; i++) {
    if (this.epg[i].ID == epgId) {
      return this.epg[i];
    }
  }
  return;
};
// platforma.prototype._epgApi = function (i, date, refresh, sync) {
//≤5 times/s
//   var parts = date.match(/(\d+)/g);
//   var odgovor;
//   var datum = new Date(parts[0], parts[1] - 1, parts[2]);
//   var startTime = datum.getTime();
//   var endTime = datum.getTime();
//   var startDate = new Date(startTime);
//   var endDate = new Date(endTime);
//   startDate.setHours(0),
//     startDate.setMinutes(1),
//     startDate.setSeconds(0),
//     endDate.setHours(23),
//     endDate.setMinutes(59),
//     endDate.setSeconds(59);
//   var godina = startDate.getFullYear();
//   var mesec = startDate.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = startDate.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var formatiranDatum = godina + "" + mesec + "" + dan;
//   if (refresh && this.epgList[i]) {
//     var zaBrisanje = this.epgList[i].datum;
//     for (var x = 0; x < zaBrisanje.length; x++) {
//       if (zaBrisanje[x].datum == formatiranDatum) {
//         this.epgList[i].datum.splice(x, 1);
//       }
//     }
//   }
//   if (i >= 0 && i <= this.tvChannels.length && !this.epgList[i]) {
//     var tempEpg = {
//       index: "",
//       ChanKey: this.tvChannels[i].ChanKey,
//       idOriginal: this.tvChannels[i].idOriginal,
//       mediaId: this.tvChannels[i].mediaId,
//       kanal: "",
//       datum: [],
//     };
//     var tempDatum = { datum: "", epg: [] };

//     var data = JSON.stringify({
//       needChannel: 0,
//       queryChannel: {
//         channelIDs: [this.tvChannels[i].idOriginal],
//       },
//       queryPlaybill: {
//         type: 0,
//         startTime: startDate.getTime(),
//         endTime: endDate.getTime(),
//         count: 100,
//         offset: 0,
//         isFillProgram: 2,
//       },
//     });

//     var xhr = new XMLHttpRequest();

//     xhr.addEventListener("readystatechange", function () {
//       if (this.readyState === 4) {
//         console.log(JSON.parse(this.responseText));
//         odgovor = JSON.parse(this.responseText);
//       }
//     });

//     xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//     xhr.setRequestHeader("Content-type", "application/json");
//     xhr.setRequestHeader("Accept", "*/*");

//     xhr.send(data);
//     console.log(odgovor.result.retCode);
//     if (odgovor.result.retCode != "000000000") {
//       console.log(odgovor.result.retMsg);
//       return;
//     }
//     tempEpg.index = this.tvChannels[i].idKanala;
//     tempEpg.kanal = this.tvChannels[i].ime;
//     tempDatum.datum = formatiranDatum;
//     tempDatum.epg = odgovor.channelPlaybills[0].playbillLites;
//     for (var x = 0; x < tempDatum.epg.length; x++) {
//       tempDatum.epg[x].mediaId = this.tvChannels[i].mediaId;
//     }
//     tempEpg.datum.push(tempDatum);
//     this.epgList[i] = tempEpg;
//     console.log(tempEpg);
//   } else if (
//     i >= 0 &&
//     i <= this.tvChannels.length &&
//     proveraEpg(this.epgList[i].datum)
//   ) {
//     var tempDatum = { datum: "", epg: [] };

//     var data = JSON.stringify({
//       needChannel: 0,
//       queryChannel: {
//         channelIDs: [this.tvChannels[i].idOriginal],
//       },
//       queryPlaybill: {
//         type: 0,
//         startTime: startDate.getTime(),
//         endTime: endDate.getTime(),
//         count: 100,
//         offset: 0,
//         isFillProgram: 2,
//       },
//     });

//     var xhr = new XMLHttpRequest();

//     xhr.addEventListener("readystatechange", function () {
//       if (this.readyState === 4) {
//         console.log(JSON.parse(this.responseText));
//         odgovor = JSON.parse(this.responseText);
//       }
//     });

//     xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//     xhr.setRequestHeader("Content-type", "application/json");
//     xhr.setRequestHeader("Accept", "*/*");

//     xhr.send(data);
//     console.log(odgovor.result.retCode);
//     if (odgovor.result.retCode != "000000000") {
//       console.log(odgovor.result.retMsg);
//       return;
//     }
//     tempDatum.datum = formatiranDatum;
//     tempDatum.epg = odgovor.channelPlaybills[0].playbillLites;
//     for (var x = 0; x < tempDatum.epg.length; x++) {
//       tempDatum.epg[x].mediaId = this.tvChannels[i].mediaId;
//     }
//     this.epgList[i].datum.push(tempDatum);
//     console.log(tempDatum);
//   }
//   if (i >= 0 && i <= this.tvChannels.length) {
//     this._getTrenutniProgram(i);
//     if (this.epgList[i].datum.length > 1) {
//       for (var a = 0; a < this.epgList[i].datum.length; a++) {
//         for (var b = 0; b < this.epgList[i].datum.length; b++) {
//           var tempVal;
//           if (
//             this.epgList[i].datum[a].epg[0].startTime <
//             this.epgList[i].datum[b].epg[0].startTime
//           ) {
//             tempVal = this.epgList[i].datum[b];
//             this.epgList[i].datum[b] = this.epgList[i].datum[a];
//             this.epgList[i].datum[a] = tempVal;
//           }
//         }
//       }
//     }
//   }

//   function proveraEpg(datumi) {
//     for (var i = 0; i < datumi.length; i++) {
//       if (datumi[i].datum == formatiranDatum) return false;
//     }
//     return true;
//   }
// };
// platforma.prototype._epg = function (id, datum, refresh, sync) {
//   //if (this.tvChannels.length > 0) {
//   var index;
//   for (var i = 0; i < this.tvChannels.length; i++) {
//     if (this.tvChannels[i].idOriginal == id) {
//       index = i;
//       break;
//     }
//   }
//   // for (var i = index - 2; i < index + 3; i++) {
//   //   this._epgApi(i, datum, refresh, sync);
//   // }
//   this._epgApi(index, datum, refresh, sync);
//   // } else console.log("tv list empty");
// };
// platforma.prototype.getEpg = function (id, datum, refresh, sync) {
//   this._epg(id, datum, refresh, sync);
//   console.log("Skidam epg");
//   return this.epgList;
// };
// platforma.prototype.getEpgByDate = function (chId, date, sync) {
//   var odgovor = {
//     trenutno: "",
//     epg: "",
//   };
//   var trenutniDatum = new Date();
//   var godina = trenutniDatum.getFullYear();
//   var mesec = trenutniDatum.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = trenutniDatum.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var formatiranDatum = godina + "-" + mesec + "-" + dan;
//   if (this.epgList.length == 0) {
//     console.log("NEMA EPGa");
//     this.getEpg(chId, formatiranDatum, false, sync);
//   }
//   if (!date) {
//     for (var i = 0; i < this.epgList.length; i++) {
//       if (!this.epgList[i]) continue;
//       if (this.epgList[i].idOriginal == chId) {
//         odgovor.epg = this.epgList[i].datum;
//         if (!this.epgList[i].trenutno) {
//           this.getEpg(chId, formatiranDatum, false, sync);
//           odgovor.trenutno = this.epgList[i].trenutno;
//         } else {
//           odgovor.trenutno = this.epgList[i].trenutno;
//         }
//         return odgovor;
//       }
//       if (i == this.epgList.length - 1) {
//         console.log("NEMA KANALA");
//         this.getEpg(chId, formatiranDatum, false, sync);
//         i = -1;
//       }
//     }
//   } else {
//     for (var i = 0; i < this.epgList.length; i++) {
//       if (!this.epgList[i]) continue;
//       if (this.epgList[i].idOriginal == chId) {
//         if (!this.epgList[i].trenutno) {
//           this.getEpg(chId, formatiranDatum, false, sync);
//           odgovor.trenutno = this.epgList[i].trenutno;
//         } else {
//           odgovor.trenutno = this.epgList[i].trenutno;
//         }
//         for (var j = 0; j < this.epgList[i].datum.length; j++) {
//           console.log(j, this.epgList[i].datum.length);

//           if (this.epgList[i].datum[j].datum == date) {
//             odgovor.epg = this.epgList[i].datum[j];
//             return odgovor;
//           }
//           if (j == this.epgList[i].datum.length - 1) {
//             console.log("NEMA DATUMA");
//             var stringDate = date.toString();
//             var date1 = [
//               stringDate.slice(0, 4),
//               "-",
//               stringDate.slice(4, 6),
//               "-",
//               stringDate.slice(6, 8),
//             ].join("");
//             this.getEpg(chId, date1, false, sync);
//             j = -1;
//           }
//         }
//       }
//       if (i == this.epgList.length - 1) {
//         console.log("NEMA KANALA");
//         this.getEpg(chId, formatiranDatum, false, sync);
//         i = -1;
//       }
//     }
//   }
// };
// platforma.prototype.getEpgItem = function (chId, epgId) {
//   for (var i = 0; i < this.epgList.length; i++) {
//     if (!this.epgList[i]) continue;
//     if (this.epgList[i].idOriginal == chId) {
//       for (var j = 0; j < this.epgList[i].datum.length; j++) {
//         for (var x = 0; x < this.epgList[i].datum[j].epg.length; x++)
//           if (this.epgList[i].datum[j].epg[x].ID == epgId) {
//             this.epgList[i].datum[j].epg[x].mediaId = this.epgList[i].mediaId;
//             var date = this.epgList[i].datum[j].datum;
//             var output = [
//               date.slice(0, 4),
//               "-",
//               date.slice(4, 6),
//               "-",
//               date.slice(6, 8),
//             ].join("");
//             this.epgList[i].datum[j].epg[x].datum = output;
//             return this.epgList[i].datum[j].epg[x];
//           }
//       }
//     }
//   }
// };
// platforma.prototype._getTrenutniProgram = function (index) {
//   var datum = new Date();
//   var godina = datum.getFullYear();
//   var mesec = datum.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = datum.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var datumProvera = godina + "" + mesec + "" + dan;
//   var danas;
//   for (var i = 0; i < this.epgList[index].datum.length; i++) {
//     if (this.epgList[index].datum[i].datum == datumProvera) {
//       danas = this.epgList[index].datum[i].epg;
//       break;
//     }
//     if (i == this.epgList[index].datum.length - 1) return;
//   }
//   var startTime = datum.getTime();
//   for (var i = 0; i < danas.length; i++) {
//     if (startTime > danas[i].startTime && startTime < danas[i].endTime) {
//       danas[i].remainingDuration = danas[i].endTime - startTime;
//       this.epgList[index].trenutno = danas[i];
//     }
//   }
// };

platforma.prototype._searchHotKey = function (sync) {
  //≤2 times/s
  //Popularni search-evi
  var odgovor;
  var data = '{"offset":0,"count":5}';

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", apiEndpoint + "VSP/V3/SearchHotKey", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  this.popularniSearch = odgovor;
};
platforma.prototype.getSearchHotKey = function (sync) {
  if (!this.popularniSearch) this._searchHotKey(sync);
  return this.popularniSearch;
};
platforma.prototype.suggestKeyword = function (searchVal, sync) {
  //≤5 times/s
  //Autocomplete api
  var odgovor;
  var data = '{"key":"' + searchVal + '","count":10}';

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/SuggestKeyword", sync);
  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(data);
  return odgovor.suggests;
};
platforma.prototype.search = function (searchCat, searchVal, sync) {
  //≤5 times/s
  ////////////searchCat parametri//////////////
  // "VIDEO_VOD"; Search kroz VOD-ove        //
  // "TVOD";  Search kroz catchup            //
  // "PROGRAM"; Search kroz EPG              //
  // "VIDEO_CHANNEL"; Search kroz kanale     //
  /////////////////////////////////////////////

  //contentTypes prima vise parametara
  if (searchVal.length > 2) {
    var odgovor;
    var data = '{"contentTypes":["' + searchCat + '"],"searchKey":"' + searchVal + '","offset":0,"count":100,"searchScopes":["ALL"],"sortType":["RELEVANCE"],"excluder":{"ratingID":"180"}}\r\n';

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        console.log(JSON.parse(this.responseText));
        odgovor = JSON.parse(this.responseText);
      }
    });

    xhr.open("POST", this.url + "VSP/V3/SearchContent", sync);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);
    return odgovor.contents;
  }
};
platforma.prototype.searchAll = function (searchVal, sync) {
  ////////////searchCat parametri//////////////
  // "VIDEO_VOD"; Search kroz VOD-ove        //
  // "TVOD";  Search kroz catchup            //
  // "PROGRAM"; Search kroz EPG              //
  // "VIDEO_CHANNEL"; Search kroz kanale     //
  /////////////////////////////////////////////

  //contentTypes prima vise parametara
  var kategorije = ["VIDEO_VOD", "TVOD", "PROGRAM"];
  var searchNiz = [];
  function dur(end, start) {
    var diff = parseInt(end) - parseInt(start);
    return formatTimeEpoch(diff);
  }
  for (var i = 0; i < kategorije.length; i++) {
    if (searchVal.length > 2) {
      var odgovor;
      var data = '{"contentTypes":["' + kategorije[i] + '"],"searchKey":"' + searchVal + '","offset":0,"count":100,"searchScopes":["ALL"],"sortType":["RELEVANCE"],"excluder":{"ratingID":"180"}}\r\n';

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText).contents;
        }
      });

      xhr.open("POST", this.url + "VSP/V3/SearchContent", sync);
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
      for (var j = 0; j < odgovor.length; j++) {
        var tempResult = {
          id: "",
          id_item: "",
          name: "",
          id_org: "",
          vr_media: "",
          year_group: "",
          maturity_rating: "",
          synopsis: "",
          creators: "",
          image: "", //img['stills']
          image_lg: "", //image_lg_src(resp[i].slika),img['background']
          id_list: "", //(0 - epg, 1 - catchup, 2 - VOD)
          vod_id: "", //(0 - epg, 1 - catchup, 2 - VOD)
          duration: "", //duration(element.endTime, element.startTime)
        };
        if (i == 0) {
          tempResult.id = searchNiz.length;
          tempResult.id_item = odgovor[j].VOD.ID;
          tempResult.vr_media = "VOD";
          tempResult.name = odgovor[j].VOD.name;
          if (odgovor[j].VOD.genres) tempResult.year_group = odgovor[j].VOD.genres[0].genreName;
          tempResult.maturity_rating = odgovor[j].VOD.rating.ID;
          tempResult.image = odgovor[j].VOD.picture.stills[0];
          tempResult.image_lg = odgovor[j].VOD.picture.backgrounds[0];
          tempResult.id_list = 2;
          tempResult.vod_id = 2;
          searchNiz.push(tempResult);
        }
        if (i == 1) {
          tempResult.id = searchNiz.length;
          tempResult.id_item = odgovor[j].playbill.ID;
          tempResult.id_org = odgovor[j].playbill.channel.ID;
          tempResult.vr_media = "CATCHUP";
          tempResult.name = odgovor[j].playbill.name;
          tempResult.image = odgovor[j].playbill.picture.stills[0];
          tempResult.image_lg = odgovor[j].playbill.picture.backgrounds[0];
          tempResult.id_list = 1;
          tempResult.vod_id = 1;
          tempResult.duration = dur(odgovor[j].playbill.endTime, odgovor[j].playbill.startTime);
          searchNiz.push(tempResult);
        }
        if (i == 2) {
          tempResult.id = searchNiz.length;
          tempResult.id_item = odgovor[j].playbill.ID;
          tempResult.id_org = odgovor[j].playbill.channel.ID;
          tempResult.vr_media = "UNAPRED";
          tempResult.name = odgovor[j].playbill.name;
          tempResult.image = odgovor[j].playbill.picture.stills[0];
          tempResult.image_lg = odgovor[j].playbill.picture.backgrounds[0];
          tempResult.id_list = 0;
          tempResult.vod_id = 0;
          tempResult.duration = dur(odgovor[j].playbill.endTime, odgovor[j].playbill.startTime);
          searchNiz.push(tempResult);
        }
      }
    }
  }
  return searchNiz;
};
platforma.prototype._vodRecm = function (sync) {
  // ≤2 times/s
  var odgovor;
  var ovde = this;
  //recommended/filmovi
  (function () {
    var data = JSON.stringify({
      action: 6,
      count: 10,
      recmVODFilter: {
        ratingID: "18",
      },
      offset: 0,
    });

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        odgovor = JSON.parse(this.responseText).VODs;
        console.log(odgovor);
      }
    });

    xhr.open("POST", ovde.url + "VSP/V3/QueryRecmVODList", sync);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);
    //obrisati for petlju kasnije
    for (var i = 0; i < odgovor.length; i++) {
      if (odgovor[i].VODType == 0) {
        ovde.vodList.filmovi.push(odgovor[i]);
      } else ovde.vodList.serije.push(odgovor[i]);
    }
    //ovde.vodList.recommended = odgovor;
  })();
  //recommended1/serije
  (function () {
    var data = JSON.stringify({
      queryDynamicRecmContent: {
        recmScenarios: [
          {
            contentType: "VOD",
            entrance: "VOD_TOP_PICKS_FOR_YOU",
            recmType: 4,
            count: 60,
            offset: 0,
            ratingID: "180",
          },
        ],
        ratingID: "180",
      },
    });

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 4) {
        odgovor = JSON.parse(this.responseText).recmContents[0].recmVODs; // obrisati posle .recmContents[0].recmVODs
        //console.log(odgovor);
      }
    });

    xhr.open("POST", ovde.url + "VSP/V3/QueryRecmContent", sync);
    xhr.setRequestHeader("Content-Type", "application/json");

    xhr.send(data);
    //obrisati for petlju kasnije
    for (var i = 0; i < odgovor.length; i++) {
      if (odgovor[i].VODType == 0) {
        ovde.vodList.filmovi.push(odgovor[i]);
      } else ovde.vodList.serije.push(odgovor[i]);
    }
    //ovde.vodList.recommended1 = odgovor.recmContents[0].recmVODs;
  })();
};
platforma.prototype.getBookmark = function (sync) {
  //≤2 times/s
  //bookmark
  var odgovor;
  var data = JSON.stringify({
    bookmarkTypes: ["VOD", "SERIES", "PROGRAM"],
    filter: {
      ratingID: "180",
    },
    count: 50,
    offset: 0,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", ovde.url + "VSP/V3/QueryBookmark", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getVodRecm = function (sync) {
  if (this.vodList.filmovi.length == 0) this._vodRecm(sync);
  return this.vodList.filmovi;
};
platforma.prototype.getSeriesRecm = function (sync) {
  if (this.vodList.serije.length == 0) this._vodRecm(sync);
  return this.vodList.serije;
};
platforma.prototype._vodCatalogue = function (id, sync) {
  // ≤2 times/s
  //lista kataloga
  var odgovor;
  var data = JSON.stringify({
    subjectID: id,
    count: 50,
    offset: 0,
    contentTypes: ["VOD"],
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText).subjects;
      odgovor.splice(0, 0, {
        code: "APOFIL",
        name: "APOLON FILMOVI",
        ID: "APOFIL",
      });
      odgovor.splice(0, 0, {
        code: "APOSER",
        name: "APOLON SERIJE",
        ID: "APOSER",
      });
      odgovor.splice(0, 0, {
        code: "00001",
        name: "KUCNI BIOSKOP NOVO U PONUDI",
        ID: "00001",
      });
      odgovor.splice(0, 0, {
        code: "00004",
        name: "KUCNI BIOSKOP FILMOVI",
        ID: "00004",
      });
      odgovor.splice(0, 0, {
        code: "00003",
        name: "KUCNI BIOSKOP SERIJE",
        ID: "00003",
      });
      odgovor.splice(0, 0, {
        code: "BIOGRAFIJE",
        name: "KUCNI BIOSKOP BIOGRAFIJE",
        ID: "BIOGRAFIJE",
      });
      // odgovor.splice(0, 0, {
      //   code: "SSF",
      //   name: "SUPERSTAR FILMOVI",
      //   ID: "SSF",
      // });
      odgovor.splice(0, 0, {
        code: "SSS",
        name: "SUPERSTAR SERIJE",
        ID: "SSS",
      });
      odgovor.splice(0, 0, {
        code: "7410",
        name: "HBO FILMOVI",
        ID: "7410",
      });
      odgovor.splice(0, 0, {
        code: "7425",
        name: "HBO SERIJE",
        ID: "7425",
      });
      odgovor.splice(0, 0, {
        code: "7400",
        name: "HBO DISNEY",
        ID: "7400",
      });
      odgovor.splice(0, 0, {
        code: "7111",
        name: "HBO DECIJI",
        ID: "7111",
      });
      odgovor.splice(0, 0, {
        code: "7080",
        name: "PICKBOX NOW PREPORUKE",
        ID: "7080",
      });
      odgovor.splice(0, 0, {
        code: "7503",
        name: "PICKBOX NOW SERIJE",
        ID: "7503",
      });
      odgovor.splice(0, 0, {
        code: "7502",
        name: "PICKBOX NOW FILMOVI",
        ID: "7502",
      });
      odgovor.splice(0, 0, {
        code: "ARENA2",
        name: "ARENA SPORTSKI DOGADJAJI",
        ID: "ARENA2",
      });
      // odgovor.splice(0, 0, {
      //   code: "ARENA4",
      //   name: "ARENA4",
      //   ID: "ARENA4",
      // });
      odgovor.splice(0, 0, {
        code: "7938",
        name: "FILMBOX OD FILMOVI",
        ID: "7938",
      });
      odgovor.splice(0, 0, {
        code: "11170",
        name: "FILMBOX OD SERIJE",
        ID: "11170",
      });
      odgovor.splice(0, 0, {
        code: "11160",
        name: "FILMBOX OD DOKUMENTARNI",
        ID: "11160",
      });
      odgovor.splice(0, 0, {
        code: "BBC_S",
        name: "BBC EARTH SERIJE",
        ID: "BBC_S",
      });
      odgovor.splice(0, 0, {
        code: "ED1",
        name: "EPIC DRAMA SERIJE",
        ID: "ED1",
      });
      odgovor.splice(0, 0, {
        code: "7971",
        name: "RTS SERIJE",
        ID: "7971",
      });
      odgovor.splice(0, 0, {
        code: "SER",
        name: "HISTORY SERIJE",
        ID: "SER",
      });
      odgovor.splice(0, 0, {
        code: "7730",
        name: "AXN NOW SERIJE",
        ID: "7730",
      });
      // odgovor.splice(0, 0, {
      //   code: "7710",
      //   name: "AXN",
      //   ID: "7710",
      // });
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryVODSubjectList", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  this.vodCatalogue = odgovor;
};

platforma.prototype.getVodCatalogue = function (id, sync) {
  // ≤2 times/s
  if (!this.vodCatalogue) this._vodCatalogue(id, sync);
  var filmovi = [];

  for (var i = 0; i < this.vodCatalogue.length; i++) {
    if (this.vodCatalogue[i].name.indexOf("SERIJE") == -1) filmovi.push(this.vodCatalogue[i]);
  }
  return filmovi;
};
platforma.prototype.getSeriesCatalogue = function (id, sync) {
  if (!this.vodCatalogue) this._vodCatalogue(id, sync);
  var serije = [];
  //console.log("serije");
  for (var i = 0; i < this.vodCatalogue.length; i++) {
    if (this.vodCatalogue[i].name.indexOf("SERIJE") !== -1) serije.push(this.vodCatalogue[i]);
  }
  return serije;
};
platforma.prototype.getVodCategory = function (category, sync) {
  //≤5 times/s
  var data = JSON.stringify({
    subjectID: category,
    count: 21,
    sortType: "STARTTIME:DESC",
    offset: 0,
    VODFilter: {
      ratingID: "18",
    },
    VODExcluder: {
      supersetType: "1",
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText).VODs;
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryVODListStcPropsBySubject", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  try {
    for (var i = 0; i < odgovor.length; i++) {
      odgovor[i].category = category;
    }
  } catch (err) {
    console.log(err);
  }

  return odgovor;
};
platforma.prototype.getSingleVod = function (id, sync) {
  // ≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    VODID: id,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText).VODDetail;
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryVOD", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getEpisodesAndSeasons = function (id, sync) {
  var odgovor;
  var data = JSON.stringify({
    VODID: id,
    offset: 0,
    count: 50,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryEpisodeList", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getAllEpisodes = function (id, sync) {
  var sezone = this.getEpisodesAndSeasons(id, false);
  var sveEpizode = [];
  for (var i = 0; i < sezone.episodes.length; i++) {
    sezona = this.getEpisodesAndSeasons(sezone.episodes[i].VOD.ID, false);
    for (var j = 0; j < sezona.episodes.length; j++) {
      sezona.episodes[j].VOD.idSezone = sezone.episodes[i].VOD.ID;
    }
    sveEpizode.push(sezona);
  }
  return sveEpizode;
};
//Pocetna strana
platforma.prototype.getReminder = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    offset: 0,
    count: 50,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryReminder", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getFrontpageBookmarks = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    bookmarkTypes: ["VOD", "SERIES", "PROGRAM"],
    filter: {
      ratingID: "180",
    },
    count: 50,
    offset: 0,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryBookmark", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getCatchupRecm = function (sync) {
  // ≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    queryDynamicRecmContent: {
      recmScenarios: [
        {
          contentType: "PROGRAM",
          businessType: "CUTV",
          entrance: "Preferred_CatchupTV",
          recmType: 4,
          count: 60,
          offset: 0,
          ratingID: "180",
        },
      ],
      ratingID: "180",
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryRecmContent", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getTrenutniProgram = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    queryDynamicRecmContent: {
      recmScenarios: [
        {
          contentType: "PROGRAM",
          businessType: "BTV",
          entrance: "Program_Whats_On",
          recmType: 4,
          forceReload: true,
          count: 60,
          offset: 0,
          ratingID: "180",
        },
      ],
      ratingID: "180",
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryRecmContent", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getTrenutniProgram1 = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    queryDynamicRecmContent: {
      recmScenarios: [
        {
          contentType: "PROGRAM",
          businessType: "BTV",
          entrance: "PROGRAM_HAPPY_NEXT",
          recmType: 4,
          count: 60,
          offset: 0,
          ratingID: "180",
        },
      ],
      ratingID: "180",
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryRecmContent", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.getPopularnoOveNedelje = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    boardType: 2,
    count: 12,
    offset: 0,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryHotPlaybill", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Podesavanja
/////////////////////////////////
//apiji koji se pozivaju prilikom unosa pina i ulaska u meni podesavanja
platforma.prototype.checkPass = function (pass, sync) {
  // ≤1 time/20s
  //proverava uneti pin kod
  var odgovor;
  var data = JSON.stringify({
    password: pass,
    type: 3,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/CheckPassword", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.queryProfile = function (sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    type: 1,
    count: 1000,
    offset: 0,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryProfile", false);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.queryCredit = function (usrName, sync) {
  //≤1 time/s
  var odgovor;
  var data = JSON.stringify({
    queryType: 0,
    profileID: usrName,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryCreditBalance", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
////////////////////////////////////////////////////////////////////////
//Reminders - poziva se VSP/V3/QueryReminder koji je sadrzan u funkciji .getReminder
//Promeni vreme za podsetnik, jezik, roditeljska kontrola, definisi limit potrosnje, promeni ime profila,
platforma.prototype.modifyProfile = function (sync) {
  //≤2 times/s
  // this.profileModifier. = {
  //   this.profileModifier.reminderInterval: "5", //opcije su 0, 5, 10, 15
  //   this.profileModifier.language: "sr", //opcije su "en" i "sr"
  //   this.profileModifier.parentalControlID: "0", //opcije su 0, 7, 12, 14, 16, 18, 180 (18+)
  //   this.profileModifier.spendLimit: 1000, //opcije su 0, 500, 1000, 2000, -1 (neograniceno)
  //   this.profileModifier.profileName: "testui" //menja ime profila u zeljeni string
  // };
  var prntlCtrlName;
  if (this.profileModifier.parentalControlID == 180) {
    prntlCtrlName = "R18+";
  } else {
    prntlCtrlName = "R" + this.profileModifier.parentalControlID;
  }
  var odgovor;
  var data = JSON.stringify({
    profile: {
      isNeedSubscribePIN: "0",
      isDisplayInfoBar: "0",
      isShowMessage: "1",
      subscriberID: "testui",
      extensionFields: [
        {
          values: ["0"],
          key: "refreshPasswordFlag",
        },
      ],
      profileType: "0",
      purchaseEnable: "1",
      ratingName: prntlCtrlName,
      loginName: "testui",
      ratingID: this.profileModifier.parentalControlID,
      quota: this.profileModifier.spendLimit,
      ID: "testui",
      lang: this.profileModifier.language,
      isDefaultProfile: "1",
      hasCollectUserPreference: "0",
      isSendSMSForReminder: "0",
      profilePINEnable: "1",
      isReceiveSMS: "0",
      profileVersion: "1636194211348",
      pushStatus: "1",
      logoURL: "pic0.png",
      leadTimeForSendReminder: "5",
      isFilterLevel: "0",
      multiscreenEnable: "0",
      name: this.profileModifier.profileName,
      receiveADType: "1",
      reminderInterval: this.profileModifier.reminderInterval,
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/ModifyProfile", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Lista uredjaja i promena imena istih
platforma.prototype.deviceList = function (usrName, sync) {
  //≤2 times/s
  var data = JSON.stringify({
    deviceType: "2",
    subscriberID: usrName,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryDeviceList", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.changeDeviceName = function (deviceId, input, sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    device: {
      ID: deviceId,
      name: input,
      deviceType: "2",
    },
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/ModifyDeviceInfo", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Omiljeni kanali - Koriste se funkcije za dobijanje kanala, i funkcije ._favorite, .createFavorite i .deleteFavorite za manipulaciju favoritima
platforma.prototype.createFavorite = function (id, sync) {
  //≤2 times/s
  var odgovor;
  var data = '{"favorites":[{"contentID":"' + id + '","contentType":"CHANNEL"}]}';

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/CreateFavorite", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.deleteFavorite = function (id, sync) {
  //≤2 times/s
  var odgovor;
  var data = '{"contentIDs":["' + id + '"],"contentTypes":["CHANNEL"],"deleteType":0}';
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      console.log(JSON.parse(this.responseText));
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/DeleteFavorite", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Zakljucavanje kanala i kategorija - Koriste se funkcije za dobijanje kanala, i funkcije .queryLocked, .lock i .unlock za manipulaciju zakljucanim kanalima
//Funkcijama lock i unlock treba predati ID kanala ili ID kategorije, i "CHANNEL" ili "SUBJECT" u zavisnosti da li je kanal ili kategorija
//id kategorija se dobija iz .getVodCatalogue
platforma.prototype.queryLocked = function (usrName, category, sync) {
  //≤2 times/s
  //category moze biti "CHANNEL" za kanale i "SUBJECT" za kategorije
  var odgovor;
  var data = JSON.stringify({
    lockTypes: [category],
    profileID: usrName,
    offset: 0,
    count: 50,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/QueryLock", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  this.zakljucaniKanali = odgovor;
  return odgovor;
};
platforma.prototype.lock = function (id, category, usrName, sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    locks: [
      {
        itemID: id,
        lockType: category,
        profileID: usrName,
      },
    ],
    profileID: usrName,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/CreateLock", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
platforma.prototype.unlock = function (id, category, usrName, sync) {
  //≤2 times/s
  var odgovor;
  var data = JSON.stringify({
    itemIDs: [id],
    lockTypes: [category],
    profileID: usrName,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/DeleteLock", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Istorija kupovine
//Vraca kupljenje VODove ili pakete. parametri fromDate i toDate nisam siguran odakle se dobijaju
//parametar contenttype: "VOD" je za VODove, dok za pakete ga nema
platforma.prototype.istorijaKupovine = function (sync) {
  var odgovor;
  var data = JSON.stringify({
    profileId: -1,
    fromDate: "20210806034901",
    toDate: "20211130115959",
    subscriptionScope: 1,
    count: 50,
    offset: 0,
    contenttype: "VOD",
    producttype: 1,
    orderType: 1,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "EPG/JSON/QueryOrder", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Promena PINa - koristi se .checkPass za proveru pina pa .changePass za promenu
platforma.prototype.changePass = function (
  //≤1 time/20s
  currentPass,
  newPass,
  confirmNewPass,
  usrName,
  sync
) {
  var odgovor;
  var data = JSON.stringify({
    oldPwd: currentPass,
    newPwd: newPass,
    confirmPwd: confirmNewPass,
    type: 0,
    subProfileID: usrName,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/ModifyPassword", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
//Promena pina za kupovinu - koristi se .checkPass za proveru trenutnog pina pa .changePurchasePass za promenu pina za kupovinu
platforma.prototype.changePurchasePass = function (
  // ≤1 time/20s
  currentPass,
  newPurchasePass,
  usrName,
  sync
) {
  var odgovor;
  var data = JSON.stringify({
    profileID: usrName,
    type: 2,
    adminProfilePassword: currentPass,
    resetPwd: newPurchasePass,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/ResetPassword", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
};
//Informacije o uredjajima - info o uredjajima, primer type-s je "Chrome_Irdeto_Widevine", nisam mogao da testiram za druge
platforma.prototype.deviceInfo = function (type, sync) {
  var odgovor;
  var data = JSON.stringify({
    terminalType: type,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("POST", this.url + "EPG/JSON/QueryAccessLicense", sync);
  xhr.setRequestHeader("Content-Type", "application/json");

  xhr.send(data);
  return odgovor;
};
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
platforma.prototype.heartBeat = function (sync) {
  // ≤1 time/15min
  var odgovor;
  var data = "";

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
      console.log(odgovor);
    }
  });

  xhr.open("POST", this.url + "VSP/V3/OnLineHeartbeat", sync);

  xhr.send(data);
  return odgovor;
};
platforma.prototype.subscribe = function (id, sync) {
  var odgovor;
  var data = JSON.stringify({
    productid: id,
  });

  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 4) {
      odgovor = JSON.parse(this.responseText);
    }
  });

  xhr.open("GET", this.url + "EPG/JSON/Subscribe", sync);
  xhr.setRequestHeader("Content-Type", "text/plain");

  xhr.send(data);
  return odgovor;
};
//OnLineHeartbeat, PlayChannelHeartbeat, PlayVODHeartbeat, or PlayNPVRHeartbeat
//odgovor = JSON.parse(this.responseText);
// this.url + "
//https://10.222.0.39:443/
//http://10.222.0.39//
//http://mts.local:3333/
var tv = new platforma(apiEndpoint);
tv.login();
tv.auth();
tv.switchProfile();
tv.heartBeat(false);
setInterval(function () {
  tv.heartBeat(false);
}, 900000);
// tv._kanali1();.
//tv._kanaliDynamicProps();
// console.log(tv.getKategorije());
// console.log(tv.getRadio());

//console.log(tv.getKanale("", ""));
// console.log(tv.getKanalItem(1212));
// console.log(tv._kanaliDynamicProps(false));
// console.log(tv._kanali1(false));
// console.log(tv.getEpg(1208, "2021-11-23", false, false));
//console.log(tv.getTrenutniEpg("1208", false));
// console.log(tv.getNazadEpg("1208", false));
// console.log(tv.getNapredEpg("1208", false));
// console.log(tv.getTrenutno("1208"));
// console.log(tv.getEpgItem("543515417"));
// console.log(tv.trenutniEpg("1212", false));

// console.log(tv.getEpg(640, "2021-11-07", false, false));
// console.log(tv.getEpgByDate(1212, "20211112", false));
// console.log(tv.getEpgItem(1212, "536513558"));

// console.log(tv.getVodCatalogue(7000, false));
// console.log(tv.getSeriesCatalogue(7000, false));
// console.log(tv.getVodCategory("APOFIL", false));
// console.log(tv.getVodRecm(false));
// console.log(tv.getSingleVod(119534813, false));
// console.log(tv.getSearchHotKey());
// console.log(tv.suggestKeyword("r"));
// console.log(tv.search("VIDEO_VOD", "bet"));
// console.log(tv.createFavorite(189));
// console.log(tv.deleteFavorite(189));
// console.log("Reminder", tv.getReminder(false));
// console.log("Frontpage bookmarks", tv.getFrontpageBookmarks(false));
// console.log("Catchup recm", tv.getCatchupRecm(false));
// console.log("Trenutno", tv.getTrenutniProgram(false));
// console.log("Trenutno1", tv.getTrenutniProgram1(false));
// console.log("Popularno ove nedelje", tv.getPopularnoOveNedelje(false));

// console.log(tv.checkPass(1111, false));
// console.log(tv.queryProfile(false));
// console.log(tv.queryCredit(username, false));
// console.log(tv.modifyProfile(false));
// console.log(tv.deviceList(username, false));
// console.log(tv.lock(364, "CHANNEL", username, false));
// console.log(tv.lock(7501, "SUBJECT", username, false));
// console.log(tv.queryLocked(username, "CHANNEL", false));
// console.log(tv.queryLocked(username, "SUBJECT", false));
// console.log(tv.deviceInfo("Chrome_Irdeto_Widevine", false));
// console.log(tv.istorijaKupovine(false));
//console.log(tv.getVodCatalogue("7000", false));

// console.log(tv.getSeriesCatalogue("7000", false));
// console.log(tv.getVodCategory("APOSER", false));
// console.log(tv.getAllEpisodes("509336735", false));
// console.log(tv.getEpisodesAndSeasons("509336735", false));
// console.log(tv.getVodRecm(false));
// console.log(tv.getSeriesRecm(false));
// console.log(tv.getVodCategory("APOSER", false));

// console.log(tv.getSingleVod("483925103", false));
// console.log(tv.getCatchupRecm(false));
//console.log(tv.getPopularnoOveNedelje(false));
//console.log("Frontpage bookmarks", tv.getFrontpageBookmarks(false));
//console.log("Catchup recm", tv.getCatchupRecm(false));
//console.log("Trenutno", tv.getTrenutniProgram(false));

// "VIDEO_VOD"; Search kroz VOD-ove        //
// "TVOD";  Search kroz catchup            //
// "PROGRAM"; Search kroz EPG              //
// "VIDEO_CHANNEL"; Search kroz kanale

//console.log(tv.search("PROGRAM", "ves"));
//console.log(tv.search("TVOD", "ves"));
//console.log(tv.search("VIDEO_VOD", "ves"));
// console.log(tv.searchAll("jagod", false));

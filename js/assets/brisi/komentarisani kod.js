// function alenApi() {
//     function loginAlen() {
//       var data = JSON.stringify({
//         subscriberID: "testui",
//         deviceModel: "EC2108V5",
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//           authAlen();
//         }
//       });

//       xhr.open("POST", `${apiEndpoint}VSP/V3/Login`);
//       xhr.setRequestHeader("Content-Type", "application/json");

//       xhr.send(data);
//     }
//     loginAlen();
//     function authAlen() {
//       var data = JSON.stringify({
//         authenticateBasic: {
//           userID: "testui",
//           userType: "0",
//           clientPasswd: "1111",
//           isSupportWebpImgFormat: "1",
//           needPosterTypes: ["1", "2", "3", "4", "5", "6", "7"],
//           timeZone: "Europe/Belgrade",
//           authType: "0",
//           lang: "en",
//           pageTrackerUIType: "HW_STB_EPGUI",
//         },
//         authenticateDevice: {
//           physicalDeviceID: "00:07:63:F1:E3:7E",
//           deviceModel: "EC2108V5",
//         },
//         authenticateTolerant: {
//           areaId: "32",
//           templateName: "default",
//           subnetID: "38101",
//           bossID: "TSBOSS",
//           userGroup: "-1",
//         },
//         ignoreReLogin: true,
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//           switchAlen();
//         }
//       });

//       xhr.open("POST", `${apiEndpoint2}VSP/V3/Authenticate`);
//       xhr.setRequestHeader("Content-Type", "application/json");

//       xhr.send(data);
//     }

//     function switchAlen() {
//       var data = JSON.stringify({
//         profileID: "testui",
//         loginName: "testui",
//         password: "1111",
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//           kanaliAlen();
//           recmVodAlen();
//           recmChannelsAlen();
//         }
//       });

//       xhr.open("POST", `${apiEndpoint2}VSP/V3/SwitchProfile`);
//       xhr.setRequestHeader("Content-Type", "application/json");
//       xhr.setRequestHeader(
//         "Cookie",
//         "JSESSIONID=030RIVMBJFXK7RBEFV1FL3GMTAF0J3ZE"
//       );

//       xhr.send(data);
//     }
//     function kanaliAlen() {
//       var data = JSON.stringify({
//         profileID: "testui",
//         password: "1111",
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//         }
//       });

//       xhr.open("POST", `${apiEndpoint2}VSP/V3/QueryAllChannel`);
//       xhr.setRequestHeader("Content-Type", "application/json");
//       xhr.setRequestHeader(
//         "Cookie",
//         "JSESSIONID=030RIVMBJFXK7RBEFV1FL3GMTAF0J3ZE"
//       );

//       xhr.send(data);
//     }

//     function recmVodAlen() {
//       var data = JSON.stringify({
//         action: 6,
//         count: 10,
//         recmVODFilter: {
//           ratingID: "18",
//         },
//         offset: 0,
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//         }
//       });

//       xhr.open("POST", `${apiEndpoint2}VSP/V3/QueryRecmVODList`);
//       xhr.setRequestHeader("Content-Type", "application/json");
//       xhr.setRequestHeader(
//         "Cookie",
//         "JSESSIONID=030RIVMBJFXK7RBEFV1FL3GMTAF0J3ZE"
//       );

//       xhr.send(data);
//     }

//     function recmChannelsAlen() {
//       var data = JSON.stringify({
//         queryDynamicRecmContent: {
//           recmScenarios: [
//             {
//               contentType: "PROGRAM",
//               businessType: "CUTV",
//               entrance: "Preferred_CatchupTV",
//               recmType: 4,
//               count: 60,
//               offset: 0,
//               ratingID: "0",
//             },
//           ],
//           ratingID: "0",
//         },
//       });

//       var xhr = new XMLHttpRequest();
//       xhr.withCredentials = true;

//       xhr.addEventListener("readystatechange", function () {
//         if (this.readyState === 4) {
//           console.log(JSON.parse(this.responseText));
//         }
//       });

//       xhr.open("POST", `${apiEndpoint2}VSP/V3/QueryRecmContent`);
//       xhr.setRequestHeader("Content-Type", "application/json");
//       xhr.setRequestHeader(
//         "Cookie",
//         "JSESSIONID=030RIVMBJFXK7RBEFV1FL3GMTAF0J3ZE"
//       );

//       xhr.send(data);
//     }
//   }

// console.log(apiEndpoint);
// function getTvChannels() {
//   var requestOptions = {
//     method: "POST",
//     redirect: "follow",
//   };

//   fetch(`${apiEndpoint}VSP/V3/QueryAllChannel`, requestOptions)
//     .then((response) => response.text())
//     .then((result) => {
//       result = JSON.parse(result);
//       console.log(result);
//       console.timeEnd("api poziv");
//       console.time("petlja");
//       for (var i = 0; i < result.channelDetails.length; i++) {
//         var tempKanal = {
//           idKanala: "",
//           idOriginal: "",
//           slika: "",
//           kategorija: "",
//           ime: "",
//         };
//         tempKanal.idKanala = i;
//         tempKanal.idOriginal = result.channelDetails[i].ID;
//         tempKanal.slika = result.channelDetails[i].picture.icons[0];
//         tempKanal.kategorija = result.channelDetails[i].subjectIDs[1];
//         tempKanal.ime = result.channelDetails[i].name;
//         tvChannels.push(tempKanal);
//       }
//       console.timeEnd("petlja");
//       console.log(tvChannels);
//     })
//     .catch((error) => console.log("error", error));
// }
// function getTvChannels2() {
//   var odgovor;
//   var xhr = new XMLHttpRequest();
//   xhr.withCredentials = true;

//   xhr.addEventListener("readystatechange", function () {
//     if (this.readyState === 4) {
//       tvChannels = [];
//       //console.log(this.responseText);
//       odgovor = JSON.parse(this.responseText);
//       console.log(odgovor);
//       for (var i = 0; i < odgovor.channelDetails.length; i++) {
//         var tempKanal = {
//           idKanala: "",
//           idOriginal: "",
//           slika: "",
//           kategorija: "",
//           ime: "",
//         };
//         tempKanal.idKanala = i;
//         tempKanal.idOriginal = odgovor.channelDetails[i].ID;
//         tempKanal.slika = odgovor.channelDetails[i].picture.icons[0];
//         tempKanal.kategorija = odgovor.channelDetails[i].subjectIDs[1];
//         tempKanal.ime = odgovor.channelDetails[i].name;
//         tvChannels.push(tempKanal);
//       }
//       console.log(tvChannels);
//       getEpg();
//     }
//   });

//   xhr.open("POST", `${apiEndpoint}VSP/V3/QueryAllChannel`);

//   xhr.send();
// }
// getTvChannels2();
// ///////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// var vodList = [];
// function getVod() {
//   var settings = {
//     url: `${apiEndpoint}VSP/V3/QueryVODSubjectList`,
//     method: "POST",
//     timeout: 0,
//   };
//   console.time("api poziv");
//   $.ajax(settings).done(function (response) {
//     console.timeEnd("api poziv");
//     console.time("petlja");
//     // for (var i = 0; i < response.channelDetails.length; i++) {
//     //   var tempVod = {
//     //     idKanala: "",
//     //     idOriginal: "",
//     //     slika: "",
//     //     kategorija: "",
//     //     ime: "",
//     //   };
//     //   tempKanal.idKanala = i;
//     //   tempKanal.idOriginal = response.channelDetails[i].ID;
//     //   tempKanal.slika = response.channelDetails[i].picture.icons[0];
//     //   tempKanal.kategorija = response.channelDetails[i].subjectIDs[1];
//     //   tempKanal.ime = response.channelDetails[i].name;
//     //   vodList.push(tempVod);
//     // }
//     console.timeEnd("petlja");
//     console.log(response);
//     console.log(vodList);
//   });
// }

//epg za 10 dana
// var startTime = datum.getTime() - 7 * 86400000;
//   var endTime = datum.getTime() + 2 * 86400000;
//   var startDate = new Date(startTime);
//   var endDate = new Date(endTime);
//   startDate.setHours(0),
//     startDate.setMinutes(1),
//     startDate.setSeconds(0),
//     endDate.setHours(23),
//     endDate.setMinutes(59),
//     endDate.setSeconds(59);
//   console.log(
//     startDate.getDate(),
//     startDate.getHours(),
//     startDate.getMinutes(),
//     startDate.getSeconds(),
//     endDate.getDate(),
//     endDate.getHours(),
//     endDate.getMinutes(),
//     endDate.getSeconds(),
//     startDate.getTime(),
//     endDate.getTime()
//   );

// console.log(
//   startDate.getDate(),
//   startDate.getMonth(),
//   startDate.getHours(),
//   startDate.getMinutes(),
//   startDate.getSeconds(),
//   endDate.getDate(),
//   endDate.getMonth(),
//   endDate.getHours(),
//   endDate.getMinutes(),
//   endDate.getSeconds(),
//   startDate.getTime(),
//   endDate.getTime()
// );

// var i = 1; // set your counter to 1

// function myLoop() {
//   setTimeout(function () {
//     console.log("hello");
//     i++;
//     if (i < 10) {
//       myLoop();
//     }
//   }, 3000);
// }

// myLoop();

// for (var i = 0; i < 10; i++) {
//   console.log("Petlja", i);
//   (function (i) {
//     setTimeout(function () {
//       console.log("funkcija", i);
//     }, 1000 * i);
//   })(i);
// }

// (function (i) {
//   setTimeout(function () {
//   }, 5 * i);
// })(i);

// 0: {code: '1604', name: 'Filmski', ID: '1604', contentType: 'VIDEO_CHANNEL'}
// 1: {code: '1100', name: 'Sportski', ID: '1100', contentType: 'VIDEO_CHANNEL'}
// 2: {code: '1605', name: 'Serijski', ID: '1605', contentType: 'VIDEO_CHANNEL'}
// 3: {code: '1607', name: 'Kolažni', ID: '1607', contentType: 'VIDEO_CHANNEL'}
// 4: {code: '1603', name: 'Dokumentarni', ID: '1603', contentType: 'VIDEO_CHANNEL'}
// 5: {code: '1602', name: 'Obrazovni', ID: '1602', contentType: 'VIDEO_CHANNEL'}
// 6: {code: '1606', name: 'Dečiji', ID: '1606', contentType: 'VIDEO_CHANNEL'}
// 7: {code: '1600', name: 'Muzički', ID: '1600', contentType: 'VIDEO_CHANNEL'}
// 8: {code: '1608', name: 'Zabavni', ID: '1608', contentType: 'VIDEO_CHANNEL'}
// 9: {code: '1601', name: 'Za odrasle', ID: '1601', contentType: 'VIDEO_CHANNEL'}
// 10: {code: '1609', name: 'Reality', ID: '1609', contentType: 'VIDEO_CHANNEL'}
// 11: {code: '1611', name: 'Promo', ID: '1611', contentType: 'VIDEO_CHANNEL'}
// 12: {code: '1610', name: 'Info', ID: '1610', contentType: 'VIDEO_CHANNEL'}

//function sprave() {
//   fetch(`${apiEndpoint}VSP/V3/QueryDeviceList`, {
//     headers: {
//       accept: "*/*",
//       "accept-language": "en-US,en;q=0.9",
//       "content-type": "application/json",
//       "sec-ch-ua":
//         '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
//       "sec-ch-ua-mobile": "?0",
//       "sec-ch-ua-platform": '"Windows"',
//       "sec-fetch-dest": "empty",
//       "sec-fetch-mode": "cors",
//       "sec-fetch-site": "same-origin",
//     },
//     referrerPolicy: "strict-origin-when-cross-origin",
//     body: '{"subscriberID":"testui"}',
//     method: "POST",
//     mode: "cors",
//     credentials: "include",
//   })
//     .then((res) => res.json())
//     .then((resp) => console.log(resp));
//   // fetch(`${apiEndpoint}VSP/V3/ReplaceDevice`, {
//   //   headers: {
//   //     accept: "*/*",
//   //     "accept-language": "en-US,en;q=0.9",
//   //     "content-type": "application/json",
//   //     "sec-ch-ua":
//   //       '"Chromium";v="94", "Google Chrome";v="94", ";Not A Brand";v="99"',
//   //     "sec-ch-ua-mobile": "?0",
//   //     "sec-ch-ua-platform": '"Windows"',
//   //     "sec-fetch-dest": "empty",
//   //     "sec-fetch-mode": "cors",
//   //     "sec-fetch-site": "same-origin",
//   //   },
//   //   referrerPolicy: "strict-origin-when-cross-origin",
//   //   body: '{"orgDeviceID":"10001000679932","subscriberID":"testui"}',
//   //   method: "POST",
//   //   mode: "cors",
//   //   credentials: "include",
//   // })
//   //   .then((res) => res.json())
//   //   .then((resp) => console.log(resp));
// }
// sprave();

//// function getAllEpg() {
//   if (tvChannels.length > 0) {
//     console.time("api poziv");
//     for (var i = 0; i < tvChannels.length; i++) {
//       var tempEpg = {
//         index: "",
//         kanal: "",
//         datum: [],
//       };
//       tempEpg.index = tvChannels[i].idKanala;
//       tempEpg.kanal = tvChannels[i].ime;
//       epgListAll[i] = tempEpg;
//       (function (i) {
//         setTimeout(function () {
//           for (var j = -7; j < 3; j++) {
//             (function (j) {
//               setTimeout(function () {
//                 var datum = new Date();
//                 var startTime = datum.getTime() + j * 86400000;
//                 var endTime = datum.getTime() + j * 86400000;
//                 var startDate = new Date(startTime);
//                 var endDate = new Date(endTime);
//                 startDate.setHours(0),
//                   startDate.setMinutes(1),
//                   startDate.setSeconds(0),
//                   endDate.setHours(23),
//                   endDate.setMinutes(59),
//                   endDate.setSeconds(59);
//                 var tempDatum = { datum: "", epg: [] };
//                 var data = JSON.stringify({
//                   needChannel: 0,
//                   queryChannel: {
//                     channelIDs: [tvChannels[i].idOriginal],
//                   },
//                   queryPlaybill: {
//                     type: 0,
//                     startTime: startDate.getTime(),
//                     endTime: endDate.getTime(),
//                     count: 100,
//                     offset: 0,
//                     isFillProgram: 2,
//                   },
//                 });

//                 var xhr = new XMLHttpRequest();
//                 xhr.withCredentials = true;

//                 xhr.addEventListener("readystatechange", function () {
//                   if (this.readyState === 4) {
//                     //console.log(JSON.parse(this.responseText));

//                     tempDatum.datum =
//                       startDate.getFullYear() +
//                       "" +
//                       (startDate.getMonth() + 1) +
//                       "" +
//                       startDate.getDate() * 1;
//                     tempDatum.epg = JSON.parse(
//                       this.responseText
//                     ).channelPlaybills[0].playbillLites;
//                     epgListAll[i].datum.push(tempDatum);
//                     console.log(tempDatum);
//                     console.log(epgListAll);
//                   }
//                 });

//                 xhr.open(
//                   "POST",
//                   `${apiEndpoint}VSP/V3/QueryPlaybillListStcProps`
//                 );
//                 xhr.setRequestHeader("Content-type", "application/json");
//                 xhr.setRequestHeader("Accept", "*/*");

//                 xhr.send(data);
//               }, 50 * (j + 7));
//             })(j);
//           }
//         }, 1000 * i);
//       })(i);
//     }
//     console.timeEnd("api poziv");
//   } else console.log("tv list empty");
// }

//$("body").keydown(function (target) {
//   if (target.originalEvent.keyCode === 38) {
//     if (rbrKanala > 0) rbrKanala--;
//     console.log(rbrKanala);
//   } else if (target.originalEvent.keyCode === 40) {
//     if (rbrKanala < tvChannels.length) rbrKanala++;
//     console.log(rbrKanala);
//   } else if (target.originalEvent.keyCode === 37) {
//     if (danEpg > 0) {
//       danEpg--;
//       msEpg -= 86400000;
//     }
//   } else if (target.originalEvent.keyCode === 39) {
//     if (danEpg < 9) {
//       danEpg++;
//       msEpg += 86400000;
//     }
//   }
// });
// $("body").keyup(function (target) {
//   if (
//     target.originalEvent.keyCode === 38 ||
//     target.originalEvent.keyCode === 40 ||
//     target.originalEvent.keyCode === 37 ||
//     target.originalEvent.keyCode === 39
//   ) {
//     getEpg();
//   } else if (target.originalEvent.keyCode === 96) trenutniProgram();
//   else if (target.originalEvent.keyCode === 110) {
//     search("VIDEO_VOD", "betmen");
//     suggestKeyword("rts");
//     searchHotKey();
//   }
// });

// tv._kanali1();.
//tv._kanaliDynamicProps();
// console.log(tv.getKategorije());
// console.log(tv.getRadio());

// console.log(tv.getKanale("", ""));
// console.log(tv.getKanalItem(1212));
// console.log(tv._kanaliDynamicProps(false));
// console.log(tv._kanali1(false));
// console.log(tv.getEpg(718, "2021-11-25", false, false));
// console.log(tv.getEpg(718, "2021-11-24", false, false));
// console.log(tv.getEpg(718, "2021-11-22", false, false));
// console.log(tv.getEpg(718, "2021-11-21", false, false));
// console.log(tv.getEpg(718, "2021-11-20", false, false));
// console.log(tv.getEpg(718, "2021-11-19", false, false));
// console.log(tv.getEpg(718, "2021-11-26", false, false));
// console.log(tv.getEpg(718, "2021-11-27", false, false));
// console.log(tv.getEpg(718, "2021-11-28", false, false));
// console.log(tv.getEpg(718, "2021-11-25", true, false));
// console.log(tv.getEpg(718, "2021-11-24", true, false));
// console.log(tv.getEpg(718, "2021-11-22", true, false));
// console.log(tv.getEpg(718, "2021-11-21", true, false));
// console.log(tv.getEpg(718, "2021-11-20", true, false));
// console.log(tv.getEpg(718, "2021-11-19", true, false));
// console.log(tv.getEpg(718, "2021-11-26", true, false));
// console.log(tv.getEpg(718, "2021-11-27", true, false));
// console.log(tv.getEpg(718, "2021-11-28", true, false));
// console.log(tv.getTrenutniEpg("1208", false));
// setTimeout(() => console.log(tv.getNazadEpg("1208", false)), 2000);
// setTimeout(() => console.log(tv.getNapredEpg("1208", false)), 4000);

// console.log(tv.getTrenutno("1208"));
// console.log(tv.getEpgItem("543515417"));
// console.log(tv.trenutniEpg("1212", false));

// console.log(tv.getEpg(640, "2021-11-07", false, false));
// console.log(tv.getEpgByDate(1212, "20211112", false));
// console.log(tv.getEpgItem(1212, "536513558"));

// console.log(tv.getVodCatalogue(7000, false));
// console.log(tv.getSeriesCatalogue(7000, false));
// console.log(tv.getVodCategory("APOFIL", false));
// console.log(tv.getVodRecm(false));
// console.log(tv.getSingleVod(119534813, false));
// console.log(tv.getSearchHotKey());
// console.log(tv.suggestKeyword("r"));
// console.log(tv.search("VIDEO_VOD", "bet"));
// console.log(tv.createFavorite(189));
// console.log(tv.deleteFavorite(189));
// console.log("Reminder", tv.getReminder(false));
// console.log("Frontpage bookmarks", tv.getFrontpageBookmarks(false));
// console.log("Catchup recm", tv.getCatchupRecm(false));
// console.log("Trenutno", tv.getTrenutniProgram(false));
// console.log("Trenutno1", tv.getTrenutniProgram1(false));
// console.log("Popularno ove nedelje", tv.getPopularnoOveNedelje(false));

// console.log(tv.checkPass(1111, false));
// console.log(tv.queryProfile(false));
// console.log(tv.queryCredit(username, false));
// console.log(tv.modifyProfile(false));
// console.log(tv.deviceList(username, false));
// console.log(tv.lock(364, "CHANNEL", username, false));
// console.log(tv.lock(7501, "SUBJECT", username, false));
// console.log(tv.queryLocked(username, "CHANNEL", false));
// console.log(tv.queryLocked(username, "SUBJECT", false));
// console.log(tv.deviceInfo("Chrome_Irdeto_Widevine", false));
// console.log(tv.istorijaKupovine(false));
//console.log(tv.getVodCatalogue("7000", false));

// console.log(tv.getSeriesCatalogue("7000", false));
// console.log(tv.getVodCategory("APOSER", false));
// console.log(tv.getAllEpisodes("509336735", false));
// console.log(tv.getEpisodesAndSeasons("509336735", false));
// console.log(tv.getVodRecm(false));
// console.log(tv.getSeriesRecm(false));
// console.log(tv.getVodCategory("APOSER", false));

// console.log(tv.getSingleVod("483925103", false));
// console.log(tv.getCatchupRecm(false));
//console.log(tv.getPopularnoOveNedelje(false));
//console.log("Frontpage bookmarks", tv.getFrontpageBookmarks(false));
//console.log("Catchup recm", tv.getCatchupRecm(false));
//console.log("Trenutno", tv.getTrenutniProgram(false));

// "VIDEO_VOD"; Search kroz VOD-ove        //
// "TVOD";  Search kroz catchup            //
// "PROGRAM"; Search kroz EPG              //
// "VIDEO_CHANNEL"; Search kroz kanale

//console.log(tv.search("PROGRAM", "ves"));
//console.log(tv.search("TVOD", "ves"));
//console.log(tv.search("VIDEO_VOD", "ves"));
// console.log(tv.searchAll("jagod", false));

// for (var i = 0; i < tv.tvChannels.length; i++) {
//   if (tv.tvChannels[i].ime.includes("Baby")) console.log(i);
// }

//epg stari
// platforma.prototype._epgApi = function (i, date, refresh, sync) {
//   // ≤5 times/s
//   var parts = date.match(/(\d+)/g);
//   var odgovor;
//   var tempOdgovor;
//   var datum = new Date(parts[0], parts[1] - 1, parts[2]);
//   var startTime = datum.getTime();
//   var endTime = datum.getTime();
//   var startDate = new Date(startTime);
//   var endDate = new Date(endTime);
//   startDate.setHours(0),
//     startDate.setMinutes(1),
//     startDate.setSeconds(0),
//     endDate.setHours(23),
//     endDate.setMinutes(59),
//     endDate.setSeconds(59);
//   var godina = startDate.getFullYear();
//   var mesec = startDate.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = startDate.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var formatiranDatum = godina + "" + mesec + "" + dan;
//   if (refresh && this.epgList[i]) {
//     var zaBrisanje = this.epgList[i].datum;
//     for (var x = 0; x < zaBrisanje.length; x++) {
//       if (zaBrisanje[x].datum == formatiranDatum) {
//         this.epgList[i].datum.splice(x, 1);
//       }
//     }
//   }
//   if (i >= 0 && i <= this.tvChannels.length && !this.epgList[i]) {
//     var tempEpg = {
//       index: "",
//       ChanKey: this.tvChannels[i].ChanKey,
//       idOriginal: this.tvChannels[i].idOriginal,
//       mediaId: this.tvChannels[i].mediaId,
//       kanal: "",
//       datum: [],
//     };
//     var tempDatum = { datum: "", epg: [] };

//     var data = JSON.stringify({
//       needChannel: 0,
//       queryChannel: {
//         channelIDs: [this.tvChannels[i].idOriginal],
//       },
//       queryPlaybill: {
//         type: 0,
//         startTime: startDate.getTime(),
//         endTime: endDate.getTime(),
//         count: 100,
//         offset: 0,
//         isFillProgram: 2,
//       },
//     });

//     var xhr = new XMLHttpRequest();

//     xhr.addEventListener("readystatechange", function () {
//       if (this.readyState === 4) {
//         console.log(JSON.parse(this.responseText));
//         odgovor = JSON.parse(this.responseText);
//         tempOdgovor = JSON.parse(this.responseText);
//       }
//     });

//     xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//     xhr.setRequestHeader("Content-type", "application/json");
//     xhr.setRequestHeader("Accept", "*/*");

//     xhr.send(data);
//     //offset proba///////////////////////////////////////////////////////////////////////
//     for (var y = 0, offSet = 100; y < 4; y++, offSet += 100) {
//       if (tempOdgovor.channelPlaybills[0].playbillLites.length == 100) {
//         var data = JSON.stringify({
//           needChannel: 0,
//           queryChannel: {
//             channelIDs: [this.tvChannels[i].idOriginal],
//           },
//           queryPlaybill: {
//             type: 0,
//             startTime: startDate.getTime(),
//             endTime: endDate.getTime(),
//             count: 100,
//             offset: offSet,
//             isFillProgram: 2,
//           },
//         });

//         var xhr = new XMLHttpRequest();

//         xhr.addEventListener("readystatechange", function () {
//           if (this.readyState === 4) {
//             console.log(JSON.parse(this.responseText));
//             tempOdgovor = JSON.parse(this.responseText);
//           }
//         });

//         xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//         xhr.setRequestHeader("Content-type", "application/json");
//         xhr.setRequestHeader("Accept", "*/*");

//         xhr.send(data);
//       } else {
//         break;
//       }
//       odgovor.channelPlaybills[0].playbillLites =
//         odgovor.channelPlaybills[0].playbillLites.concat(
//           tempOdgovor.channelPlaybills[0].playbillLites
//         );
//       if (
//         y == 3 &&
//         tempOdgovor.channelPlaybills[0].playbillLites.length == 100
//       ) {
//         y = 0;
//       }
//     }
//     ///////////////////////////////////////////////////////////////////////
//     if (odgovor.result.retCode != "000000000") {
//       console.log(odgovor.result.retMsg);
//       return;
//     }
//     tempEpg.index = this.tvChannels[i].idKanala;
//     tempEpg.kanal = this.tvChannels[i].ime;
//     tempDatum.datum = formatiranDatum;
//     tempDatum.epg = odgovor.channelPlaybills[0].playbillLites;
//     for (var x = 0; x < tempDatum.epg.length; x++) {
//       tempDatum.epg[x].mediaId = this.tvChannels[i].mediaId;
//     }
//     tempEpg.datum.push(tempDatum);
//     this.epgList[i] = tempEpg;
//     console.log(tempEpg);
//   } else if (
//     i >= 0 &&
//     i <= this.tvChannels.length &&
//     proveraEpg(this.epgList[i].datum)
//   ) {
//     var tempDatum = { datum: "", epg: [] };

//     var data = JSON.stringify({
//       needChannel: 0,
//       queryChannel: {
//         channelIDs: [this.tvChannels[i].idOriginal],
//       },
//       queryPlaybill: {
//         type: 0,
//         startTime: startDate.getTime(),
//         endTime: endDate.getTime(),
//         count: 100,
//         offset: 0,
//         isFillProgram: 2,
//       },
//     });

//     var xhr = new XMLHttpRequest();

//     xhr.addEventListener("readystatechange", function () {
//       if (this.readyState === 4) {
//         console.log(JSON.parse(this.responseText));
//         odgovor = JSON.parse(this.responseText);
//         tempOdgovor = JSON.parse(this.responseText);
//       }
//     });

//     xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//     xhr.setRequestHeader("Content-type", "application/json");
//     xhr.setRequestHeader("Accept", "*/*");

//     xhr.send(data);
//     //offset proba///////////////////////////////////////////////////////////////////////
//     for (var y = 0, offSet = 100; y < 4; y++, offSet += 100) {
//       if (tempOdgovor.channelPlaybills[0].playbillLites.length == 100) {
//         var data = JSON.stringify({
//           needChannel: 0,
//           queryChannel: {
//             channelIDs: [this.tvChannels[i].idOriginal],
//           },
//           queryPlaybill: {
//             type: 0,
//             startTime: startDate.getTime(),
//             endTime: endDate.getTime(),
//             count: 100,
//             offset: offSet,
//             isFillProgram: 2,
//           },
//         });

//         var xhr = new XMLHttpRequest();

//         xhr.addEventListener("readystatechange", function () {
//           if (this.readyState === 4) {
//             console.log(JSON.parse(this.responseText));
//             tempOdgovor = JSON.parse(this.responseText);
//           }
//         });

//         xhr.open("POST", this.url + "VSP/V3/QueryPlaybillListStcProps", sync);
//         xhr.setRequestHeader("Content-type", "application/json");
//         xhr.setRequestHeader("Accept", "*/*");

//         xhr.send(data);
//       } else {
//         break;
//       }
//       odgovor.channelPlaybills[0].playbillLites =
//         odgovor.channelPlaybills[0].playbillLites.concat(
//           tempOdgovor.channelPlaybills[0].playbillLites
//         );
//       if (
//         y == 3 &&
//         tempOdgovor.channelPlaybills[0].playbillLites.length == 100
//       ) {
//         y = 0;
//       }
//     }
//     ///////////////////////////////////////////////////////////////////////
//     console.log(odgovor.result.retCode);
//     if (odgovor.result.retCode != "000000000") {
//       console.log(odgovor.result.retMsg);
//       return;
//     }
//     tempDatum.datum = formatiranDatum;
//     tempDatum.epg = odgovor.channelPlaybills[0].playbillLites;
//     for (var x = 0; x < tempDatum.epg.length; x++) {
//       tempDatum.epg[x].mediaId = this.tvChannels[i].mediaId;
//     }
//     this.epgList[i].datum.push(tempDatum);
//     console.log(tempDatum);
//   }
//   if (i >= 0 && i <= this.tvChannels.length) {
//     this._getTrenutniProgram(i);
//     if (this.epgList[i].datum.length > 1) {
//       for (var a = 0; a < this.epgList[i].datum.length; a++) {
//         for (var b = 0; b < this.epgList[i].datum.length; b++) {
//           var tempVal;
//           if (
//             this.epgList[i].datum[a].epg[0].startTime <
//             this.epgList[i].datum[b].epg[0].startTime
//           ) {
//             tempVal = this.epgList[i].datum[b];
//             this.epgList[i].datum[b] = this.epgList[i].datum[a];
//             this.epgList[i].datum[a] = tempVal;
//           }
//         }
//       }
//     }
//   }

//   function proveraEpg(datumi) {
//     for (var i = 0; i < datumi.length; i++) {
//       if (datumi[i].datum == formatiranDatum) return false;
//     }
//     return true;
//   }
// };
// platforma.prototype._epg = function (id, datum, refresh, sync) {
//   //if (this.tvChannels.length > 0) {
//   var index;
//   for (var i = 0; i < this.tvChannels.length; i++) {
//     if (this.tvChannels[i].idOriginal == id) {
//       index = i;
//       break;
//     }
//   }
//   // for (var i = index - 2; i < index + 3; i++) {
//   //   this._epgApi(i, datum, refresh, sync);
//   // }
//   this._epgApi(index, datum, refresh, sync);
//   // } else console.log("tv list empty");
// };
// platforma.prototype.getEpg = function (id, datum, refresh, sync) {
//   this._epg(id, datum, refresh, sync);
//   console.log("Skidam epg");
//   return this.epgList;
// };
// platforma.prototype.getEpgByDate = function (chId, date, sync) {
//   var odgovor = {
//     trenutno: "",
//     epg: "",
//   };
//   var trenutniDatum = new Date();
//   var godina = trenutniDatum.getFullYear();
//   var mesec = trenutniDatum.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = trenutniDatum.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var formatiranDatum = godina + "-" + mesec + "-" + dan;
//   if (this.epgList.length == 0) {
//     console.log("NEMA EPGa");
//     this.getEpg(chId, formatiranDatum, false, sync);
//   }
//   if (!date) {
//     for (var i = 0; i < this.epgList.length; i++) {
//       if (!this.epgList[i]) continue;
//       if (this.epgList[i].idOriginal == chId) {
//         odgovor.epg = this.epgList[i].datum;
//         if (!this.epgList[i].trenutno) {
//           this.getEpg(chId, formatiranDatum, false, sync);
//           odgovor.trenutno = this.epgList[i].trenutno;
//         } else {
//           odgovor.trenutno = this.epgList[i].trenutno;
//         }
//         return odgovor;
//       }
//       if (i == this.epgList.length - 1) {
//         console.log("NEMA KANALA");
//         this.getEpg(chId, formatiranDatum, false, sync);
//         i = -1;
//       }
//     }
//   } else {
//     for (var i = 0; i < this.epgList.length; i++) {
//       if (!this.epgList[i]) continue;
//       if (this.epgList[i].idOriginal == chId) {
//         if (!this.epgList[i].trenutno) {
//           this.getEpg(chId, formatiranDatum, false, sync);
//           odgovor.trenutno = this.epgList[i].trenutno;
//         } else {
//           odgovor.trenutno = this.epgList[i].trenutno;
//         }
//         for (var j = 0; j < this.epgList[i].datum.length; j++) {
//           console.log(j, this.epgList[i].datum.length);

//           if (this.epgList[i].datum[j].datum == date) {
//             odgovor.epg = this.epgList[i].datum[j];
//             return odgovor;
//           }
//           if (j == this.epgList[i].datum.length - 1) {
//             console.log("NEMA DATUMA");
//             var stringDate = date.toString();
//             var date1 = [
//               stringDate.slice(0, 4),
//               "-",
//               stringDate.slice(4, 6),
//               "-",
//               stringDate.slice(6, 8),
//             ].join("");
//             this.getEpg(chId, date1, false, sync);
//             j = -1;
//           }
//         }
//       }
//       if (i == this.epgList.length - 1) {
//         console.log("NEMA KANALA");
//         this.getEpg(chId, formatiranDatum, false, sync);
//         i = -1;
//       }
//     }
//   }
// };
// platforma.prototype.getEpgItem1 = function (chId, epgId) {
//   for (var i = 0; i < this.epgList.length; i++) {
//     if (!this.epgList[i]) continue;
//     if (this.epgList[i].idOriginal == chId) {
//       for (var j = 0; j < this.epgList[i].datum.length; j++) {
//         for (var x = 0; x < this.epgList[i].datum[j].epg.length; x++)
//           if (this.epgList[i].datum[j].epg[x].ID == epgId) {
//             this.epgList[i].datum[j].epg[x].mediaId = this.epgList[i].mediaId;
//             var date = this.epgList[i].datum[j].datum;
//             var output = [
//               date.slice(0, 4),
//               "-",
//               date.slice(4, 6),
//               "-",
//               date.slice(6, 8),
//             ].join("");
//             this.epgList[i].datum[j].epg[x].datum = output;
//             return this.epgList[i].datum[j].epg[x];
//           }
//       }
//     }
//   }
// };
// platforma.prototype._getTrenutniProgram = function (index) {
//   var datum = new Date();
//   var godina = datum.getFullYear();
//   var mesec = datum.getMonth() + 1;
//   if (mesec < 10) {
//     mesec = "0" + mesec;
//   }
//   var dan = datum.getDate();
//   if (dan < 10) {
//     dan = "0" + dan;
//   }
//   var datumProvera = godina + "" + mesec + "" + dan;
//   var danas;
//   for (var i = 0; i < this.epgList[index].datum.length; i++) {
//     if (this.epgList[index].datum[i].datum == datumProvera) {
//       danas = this.epgList[index].datum[i].epg;
//       break;
//     }
//     if (i == this.epgList[index].datum.length - 1) return;
//   }
//   var startTime = datum.getTime();
//   for (var i = 0; i < danas.length; i++) {
//     if (startTime > danas[i].startTime && startTime < danas[i].endTime) {
//       danas[i].remainingDuration = danas[i].endTime - startTime;
//       this.epgList[index].trenutno = danas[i];
//     }
//   }
// };

// odgovor.splice(0, 0, {
//     code: "APOFIL",
//     name: "APOLON FILMOVI",
//     ID: "APOFIL",
//   });
//   odgovor.splice(0, 0, {
//     code: "APOSER",
//     name: "APOLON SERIJE",
//     ID: "APOSER",
//   });
//   odgovor.splice(0, 0, {
//     code: "00001",
//     name: "KUCNI BIOSKOP NOVO U PONUDI",
//     ID: "00001",
//   });
//   odgovor.splice(0, 0, {
//     code: "00004",
//     name: "KUCNI BIOSKOP FILMOVI",
//     ID: "00004",
//   });
//   odgovor.splice(0, 0, {
//     code: "00003",
//     name: "KUCNI BIOSKOP SERIJE",
//     ID: "00003",
//   });
//   odgovor.splice(0, 0, {
//     code: "BIOGRAFIJE",
//     name: "KUCNI BIOSKOP BIOGRAFIJE",
//     ID: "BIOGRAFIJE",
//   });
//   // odgovor.splice(0, 0, {
//   //   code: "SSF",
//   //   name: "SUPERSTAR FILMOVI",
//   //   ID: "SSF",
//   // });
//   odgovor.splice(0, 0, {
//     code: "SSS",
//     name: "SUPERSTAR SERIJE",
//     ID: "SSS",
//   });
//   odgovor.splice(0, 0, {
//     code: "7410",
//     name: "HBO FILMOVI",
//     ID: "7410",
//   });
//   odgovor.splice(0, 0, {
//     code: "7425",
//     name: "HBO SERIJE",
//     ID: "7425",
//   });
//   odgovor.splice(0, 0, {
//     code: "7400",
//     name: "HBO DISNEY",
//     ID: "7400",
//   });
//   odgovor.splice(0, 0, {
//     code: "7111",
//     name: "HBO DECIJI",
//     ID: "7111",
//   });
//   odgovor.splice(0, 0, {
//     code: "7080",
//     name: "PICKBOX NOW PREPORUKE",
//     ID: "7080",
//   });
//   odgovor.splice(0, 0, {
//     code: "7503",
//     name: "PICKBOX NOW SERIJE",
//     ID: "7503",
//   });
//   odgovor.splice(0, 0, {
//     code: "7502",
//     name: "PICKBOX NOW FILMOVI",
//     ID: "7502",
//   });
//   odgovor.splice(0, 0, {
//     code: "ARENA2",
//     name: "ARENA SPORTSKI DOGADJAJI",
//     ID: "ARENA2",
//   });
//   // odgovor.splice(0, 0, {
//   //   code: "ARENA4",
//   //   name: "ARENA4",
//   //   ID: "ARENA4",
//   // });
//   odgovor.splice(0, 0, {
//     code: "7938",
//     name: "FILMBOX OD FILMOVI",
//     ID: "7938",
//   });
//   odgovor.splice(0, 0, {
//     code: "11170",
//     name: "FILMBOX OD SERIJE",
//     ID: "11170",
//   });
//   odgovor.splice(0, 0, {
//     code: "11160",
//     name: "FILMBOX OD DOKUMENTARNI",
//     ID: "11160",
//   });
//   odgovor.splice(0, 0, {
//     code: "BBC_S",
//     name: "BBC EARTH SERIJE",
//     ID: "BBC_S",
//   });
//   odgovor.splice(0, 0, {
//     code: "ED1",
//     name: "EPIC DRAMA SERIJE",
//     ID: "ED1",
//   });
//   odgovor.splice(0, 0, {
//     code: "7971",
//     name: "RTS SERIJE",
//     ID: "7971",
//   });
//   odgovor.splice(0, 0, {
//     code: "SER",
//     name: "HISTORY SERIJE",
//     ID: "SER",
//   });
//   odgovor.splice(0, 0, {
//     code: "7730",
//     name: "AXN NOW SERIJE",
//     ID: "7730",
//   });
//   // odgovor.splice(0, 0, {
//   //   code: "7710",
//   //   name: "AXN",
//   //   ID: "7710",
//   // });

// platforma.prototype.getSeriesCatalogue = function (id, sync) {
//     if (!this.vodCatalogue) this._vodCatalogue(id, sync);
//     var serije = [];
//     console.log("serije");
//     for (var i = 0; i < this.vodCatalogue.length; i++) {
//       if (this.vodCatalogue[i].name.indexOf("SERIJE") !== -1)
//         serije.push(this.vodCatalogue[i]);
//     }
//     return serije;
//   };

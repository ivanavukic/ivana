function player(targetIP) {
  this.targetIP = targetIP;
  this.playerID = 0;
  this.mpMP	= new MediaPlayer();
  this.playerID = this.mpMP.getNativePlayerInstanceID(); 
}


player.prototype.setPlayerFromPos = function (playString, position) {
  if (this.mpMP == null) {
    this.mpMP = new MediaPlayer();
  }
  this.playerID = this.mpMP.getNativePlayerInstanceID();
  dbgLog("PLAYER INSTANCE ID: " + this.playerID);

  dbgLog(playString);
  if (playString == "failed") {
    dbgLog("PROBLEM SA PUSTANJEM KANALA");
  } else {
    this.mpMP.setSingleMedia(playString);
    this.mpMP.playByTime(1, position);
    //this.mpMP.playFromStart();
  }
};

player.prototype.setPlayer = function (playString) {
  if (this.mpMP == null) {
    this.mpMP = new MediaPlayer();
  }
  this.playerID = this.mpMP.getNativePlayerInstanceID();
  dbgLog("PLAYER INSTANCE ID: " + this.playerID);

  dbgLog(playString);
  if (playString == "failed") {
    dbgLog("PROBLEM SA PUSTANJEM KANALA");
  } else {
    this.mpMP.setSingleMedia(playString);
    this.mpMP.playFromStart();
  }
};
// Ulazni parametri su channelID (idOriginal) i mediaID
// playbillId je 0 u slucaju live kanala
//RTS 1 HD "1208" id, "530435913" playbill, "21000997" mediaId
player.prototype.playV3 = function (channelId, mediaId, playbillId, position) {
  var url = this.targetIP + "/VSP/V3/PlayChannel";
  // RTS 1 HD sa Verimatrix-om
  if (position == "undefined" || position == "" || position == null) {
	position = 0;
  }

  var mType = 1;
  var req;
  if (playbillId == 0) {
    req = { channelID: channelId, mediaID: mediaId, businessType: "BTV" };
  } else {
    mType = 3;
    req = {
      channelID: channelId,
      mediaID: mediaId,
      businessType: "CUTV",
      playbillID: playbillId,
    };
  }

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      if (obj.result.retCode == "000000000") {
        var playUrl = obj.playURL;
        var mUrl = { mediaURL: playUrl, mediaType: mType };
	if (position != 0) {
		player.setPlayerFromPos(JSON.stringify(mUrl), position)
	}
	else {
        	player.setPlayer(JSON.stringify(mUrl));
	}
      } else {
	dbgLog(xhr.responseText);
        dbgLog("ERROR GETING CHANNEL");
        return "failed";
      }
    } else {
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));
};

/*
// NOVA VOD FUNKCIJA
player.prototype.playVodV3 = function (vodId) {

  var url = this.targetIP + "VSP/V3/QueryVOD";
  // VOD FILM
  var req = { VODID: vodId };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      if (obj.result.retCode == "000000000") {
	try {
		//dbgLog(xhr.responseText);
		var vodType = obj.VODDetail.VODType;
		dbgLog("PROSAO VOD TYPE: " + vodType);
//		if (vodType == "0") { // OVO JE FILM
       		var mediaId = obj.VODDetail.mediaFiles[0].code;
        	player.playVOD(vodId, mediaId);
//		}
//		else {
			// OVO JE NEKA SERIJA
//		}
	}
	catch(eh) { dbgLog(xhr.responseText); }
      } else {
        dbgLog(xhr.responseText);
        return "failed";
      }
    } else {
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));
};


// OVO JE NEKI FILM "The Jackal"
// var req = {"VODID":"530912412","mediaID":"530912415","checkLock":{"checkType":0}};
player.prototype.playVOD = function (vodId, mediaId) {
  dbgLog("VOD ID: " + vodId + " MEDIA ID: " + mediaId);


//  vodId = 530912412;
//  mediaId = 530912415;
  var url = this.targetIP + "/VSP/V3/PlayVOD";
  // VOD FILM
  var req = { VODID: vodId, mediaID: mediaId, checkLock: { checkType: 0 } };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      dbgLog("VOD 2: ");
      var obj = JSON.parse(xhr.responseText);
      if (obj.result.retCode == "000000000") {
        dbgLog("VOD 3 ");
        var playUrl = obj.playURL;
        var productId = obj.authorizeResult.productID; // Ne znam jos dal nam treba al da ga uzmemo :)
        var mUrl = { mediaURL: playUrl, mediaType: 2 }; // OVDJE JE mediaType = 2
        dbgLog("mURL: " + mUrl);
        player.setPlayer(JSON.stringify(mUrl));
      } else {
        dbgLog("ERROR GETING VOD");
        dbgLog(xhr.responseText);
        return "failed";
      }
    } else {
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));
};
*/

player.prototype.playVodV3 = function (vodId) {
  console.log("playVodV3");
  var url = apiEndpoint + "/VSP/V3/QueryVOD";
  // VOD FILM
  var req = { VODID: vodId };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      dbgLog(obj);
      if (obj.result.retCode == "000000000") {
        try {
		dbgLog("KRECE U TRY");	
                var vodType = obj.VODDetail.VODType;
		dbgLog("PROSAO VOD TYPE: " + vodType);
		console.log("PROSAO VOD TYPE: " + vodType);
                if (vodType == "0") { // OVO JE FILM
                        var mediaId = obj.VODDetail.mediaFiles[0].code;
                        player.playVOD(vodId, mediaId);
                }
                else if (vodType == "1") { // OVO JE SEZONA VJEROVATNO 
			console.log(obj);
                }
                else if (vodType == "2") { // OVO JE NEKI SERIAL
			console.log(obj);
                }
        }
        catch(eh) { dbgLog("PlayVODV3 - CATCH ERROR CODE: " + xhr.responseText); }
      } else {
        dbgLog("playVODV3 retCODE: " + xhr.responseText);
        return "failed";
      }
    } else {
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));

};

player.prototype.playVOD = function (vodId, mediaId) {
  console.log("VOD ID: " + vodId + " MEDIA ID: " + mediaId);

  var url = apiEndpoint + "/VSP/V3/PlayVOD";
  // VOD FILM
  var req = { VODID: vodId, mediaID: mediaId, checkLock: { checkType: 0 } };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      console.log("VOD 2: ");
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.result.retCode == "000000000") {
        dbgLog("VOD 3 ");
        console.log("VOD 3: ");
        var playUrl = obj.playURL;
        var productId = obj.authorizeResult.productID; // Ne znam jos dal nam treba al da ga uzmemo :)
        var mUrl = { mediaURL: playUrl, mediaType: 2 }; // OVDJE JE mediaType = 2
        dbgLog("mURL: " + mUrl);
        player.setPlayer(JSON.stringify(mUrl));
      }
      else if (obj.result.retCode == "144020008") {  // MOZE DA SE KUPI
		var productID	= obj.authorizeResult.pricedProducts[0].ID;	
		var priceID	= obj.authorizeResult.pricedProducts[0].priceObject.ID;	
		var productCode	= obj.authorizeResult.pricedProducts[0].productCode;	
		player.subscribeVOD(productID, priceID, productCode);
		player.playVOD2(vodId, mediaId);

      } else {
        console.log("VOD 4: ");
        dbgLog("ERROR GETING VOD");
        dbgLog(xhr.responseText);
        return "failed";
      }
    } else {
        console.log("VOD 6: ");
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));

};

player.prototype.playVOD2 = function (vodId, mediaId) {
  console.log("VOD2 ID: " + vodId + " MEDIA ID: " + mediaId);

  var url = apiEndpoint + "/VSP/V3/PlayVOD";
  // VOD FILM
  var req = { VODID: vodId, mediaID: mediaId, checkLock: { checkType: 0 } };

  var xhr = new XMLHttpRequest();
  xhr.open("POST", url, false);

  xhr.onreadystatechange = function () {
    if (xhr.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.result.retCode == "000000000") {
        dbgLog("VOD 3 ");
        console.log("VOD 3: ");
        var playUrl = obj.playURL;
        var productId = obj.authorizeResult.productID; // Ne znam jos dal nam treba al da ga uzmemo :)
        var mUrl = { mediaURL: playUrl, mediaType: 2 }; // OVDJE JE mediaType = 2
        dbgLog("mURL: " + mUrl);
        player.setPlayer(JSON.stringify(mUrl));
      } else {
        console.log("VOD 4: ");
        dbgLog("ERROR GETING VOD");
        dbgLog(xhr.responseText);
        return "failed";
      }
    } else {
        console.log("VOD 6: ");
      dbgLog("GET PLAY FAILED: " + xhr.status);
      //console.log("AUTH STATUS: " + xhr.status);
    }
  };

  xhr.setRequestHeader("Content-Type", "application/json");
  xhr.send(JSON.stringify(req));
};

player.prototype.subscribeVOD = function (productID, priceID, productCode) {
	var url = apiEndpoint + "/VSP/V3/GetDiscountInfos";
	var req = {"productIds":[productID]};
	var xhr = new XMLHttpRequest();
  	xhr.open("POST", url, false);

  	xhr.onreadystatechange = function () {
    		if (xhr.readyState === 4) {
      			var obj = JSON.parse(xhr.responseText);
			console.log(obj);
 			if (obj.result.retCode == "000000000") {
				player.checkPassVOD(productID, priceID, productCode);

			}
			else { 
				dbgLog("FAILED CHECK PASSWORD");
				dbgLog(xht.responseText);
			}
		}
	};
  	xhr.setRequestHeader("Content-Type", "application/json");
	xhr.send(JSON.stringify(req));
};

player.prototype.checkPassVOD = function(productID, priceID, productCode) {
        var url = apiEndpoint + "/VSP/V3/CheckPassword";
	var req = {"password":"1111","type":2}
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, false);

        xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                        var obj = JSON.parse(xhr.responseText);
                        console.log(obj);
                        if (obj.result.retCode == "000000000") {
				dbgLog("VOD CHECK PASS OK");
				player.subscribeProductVOD(productID, priceID, productCode);
                        }
			else { 
				dbgLog("FAILED SUBSCIBE VOD PRODUCT");
				dbgLog(xht.responseText);
			}
                }
        };
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(req));
};

player.prototype.subscribeProductVOD = function(productID, priceID, productCode) {

        var url = apiEndpoint + "/VSP/V3/SubscribeProduct";
	var req = {"type":4,"correlator":"1111","subscribe":{"productID":productID,"isAutoExtend":0,"payment":{"servicePayType":"CASH","subServicePayType":1},"priceObjects":[{"ID":priceID,"type":"2","contentType":"VOD"}]}};
        var xhr = new XMLHttpRequest();
        xhr.open("POST", url, false);

        xhr.onreadystatechange = function () {
                if (xhr.readyState === 4) {
                        var obj = JSON.parse(xhr.responseText);
                        console.log(obj);
                        if (obj.result.retCode == "000000000") {
				// PROSAO
				dbgLog("PROSAO SUBSCIBE VOD PRODUCT");
			}
			else {
				dbgLog("FAILED SUBSCIBE VOD PRODUCT");
				dbgLog(xhr.responseText);
			}
		}
	};
        xhr.setRequestHeader("Content-Type", "application/json");
        xhr.send(JSON.stringify(req));
};










player.prototype.stop = function () {
  if (this.mpMP != null) {
    this.mpMP.stop();
  } else {
  }
};

player.prototype.pause = function () {
  if (this.mpMP != null) {
    this.mpMP.pause();
  } else {
  }
};

player.prototype.resume = function () {
  if (this.mpMP != null) {
    this.mpMP.resume();
  } else {
  }
};

player.prototype.mediaDur = function () {
  if (this.mpMP != null) {
    var dur = this.mpMP.getMediaDuration();
    var pos = this.mpMP.getCurrentPlayTime();
    dbgLog("MEDIA DURATION: " + dur);
    dbgLog("MEDIA POSITION: " + pos);
  }
};
player.prototype.getState = function () {
  if (this.mpMP != null) {
    var pState = JSON.parse(this.mpMP.getPlaybackMode()).PlayMode;
    dbgLog("PLAYER STATE: " + pState);
    return pState; // VRIJEDNOSTI BI TREBALE BITI: Stop/Pause/Normal Play/Trickmode/Multicast live/Unicast live/Buffering
  } else {
    return -1; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};

player.prototype.getCurDur = function () {
  if (this.mpMP != null) {
    var dur = parseInt(this.mpMP.getMediaDuration());
    return dur; // OVO KOD GLEDANJA UZIVO NEMA NEKOG SMISLA
  } else {
    return -1; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};
player.prototype.getCurPos = function (startTime) {
  //var someDate = new Date(dateString);
  //someDate = someDate.getTime();
  dbgLog("GET CUR POS START TIME: "+ startTime);

  if (this.mpMP != null) {
    var dur = player.getCurDur();
    dbgLog("CUR POS TOTAL DURATION: " + dur);
    var pos = this.mpMP.getCurrentPlayTime();
    if (pos.len > 8) {
	return 900;
    }
    else {
	return pos;
    }
  } else {
    return -1; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};
player.prototype.seekFwdMP = function (step) {
  if (this.mpMP != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    if (dur == -1 || pos == -1) {
      return -1;
    }

    var newPos = pos + parseInt(step);
    dbgLog("MEDIA DURATION: " + dur);
    dbgLog("MEDIA POSITION: " + pos);

    if (newPos < dur) {
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.mpMP.playByTime(1, newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    } else {
      newPos = dur - 1; // PREMOTAVMO DO KRAJA minus jednu sekundu
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.mpMP.playByTime(1, newPos);
    }
  } else {
    return -1; // NE VALJA NESTO PLAYER NULL PRAVIMO FUNKCIJU ZA OPORAVAK GRESKE
  }
};
player.prototype.seekRewMP = function (step) {
  if (this.mpMP != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    if (dur == -1 || pos == -1) {
      return -1;
    }

    var newPos = pos - parseInt(step);
    dbgLog("MEDIA DURATION: " + dur);
    dbgLog("MEDIA POSITION: " + pos);

    if (newPos > 0) {
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.mpMP.playByTime(1, newPos);
    } else {
      newPos = 0;
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.mpMP.playByTime(1, newPos);
    }
  } else {
    return -1; // NESTO NE VALJA
  }
};
player.prototype.seekToPos = function (newPos) {
  dbgLog("NEW POS SHOULD BE: " + newPos);
  if (this.mpMP != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    dbgLog("MEDIA DUR: " + dur + " POS: " + pos);
    if (dur == -1 || pos == -1) {
      return -1;
    }

    if (newPos > dur || newPos < 0) {
      return -1; // NESTO NE VALJA
    } else {
      this.mpMP.playByTime(1, newPos);
    }
  } else {
    dbgLog("Player is NULL!!!!");
    return -1; // NESTO NE VALJA
  }
};

player.prototype.fastForward = function() {
	var sValue = this.mpMP.getPlaybackMode();
	dbgLog("PLAYBACK MODE: " + sValue);
    	var pMode = JSON.parse(sValue).PlayMode;
	var curSpeedStr = "1";
	curSpeedStr = JSON.parse(sValue).Speed.replace("x","");
	var newSpeed = 1;
	var curSpeed = 1;
	if (pMode == "Normal Play") {
		newSpeed = 2;	
		this.mpMP.fastForward(newSpeed);
		return newSpeed;
	}
	else if (pMode == "Multicast Live") {
		dbgLog("MULTICAST LIVE");
		// THINK ABOUT
	}
	else if (pMode == "Trickmode") {
		dbgLog("CUR SPEED: " + curSpeedStr);
		if (curSpeedStr.charAt(0) == '-') {
		// REWIND
			newSpeed = 2;
			this.mpMP.fastForward(newSpeed);
			return newSpeed;
		}
		else {
			newSpeed = parseInt(curSpeedStr)*2;
			this.mpMP.fastForward(newSpeed);
			return newSpeed;
		}
	}
	else {
		return 0;
	}
};

player.prototype.fastRewind = function() {
	var sValue = this.mpMP.getPlaybackMode();
    	var pMode = JSON.parse(sValue).PlayMode;
	var curSpeedStr = "1";
	curSpeedStr = JSON.parse(sValue).Speed.replace("x","");
	var newSpeed = 1;
	var curSpeed = 1;
	if (pMode == "Normal Play") {
		newSpeed = -2;	
		this.mpMP.fastRewind(newSpeed);
		return newSpeed;
	}
	else if (pMode == "Multicast Live") {
		// THINK ABOUT
	}
	else if (pMode == "Trickmode") {
		dbgLog("CUR SPEED: " + curSpeedStr);
		if (curSpeedStr.charAt(0) == '-') {
		// REWIND
    			newSpeed = -2 * parseInt(curSpeedStr);
			this.mpMP.fastRewind(newSpeed);
			return newSpeed;
		}
		else {
			newSpeed = 2;
			this.mpMP.fastForward(newSpeed);
			return newSpeed;
		}
	}
	else {
		return 0;
	}
};




player.prototype.joinLive = function (chNum) {
  // na osnovu chId treba naci koji je ID kanala i predati ga funkciji joinChannel
  if (this.mpMP == null) {
	this.mpMP = new MediaPlayer();
  }
  if (this.mpMP != null) {
    lastChanNum = chNum;
    this.mpMP.joinChannel(chNum);
  } else {
    return -1;
  }
};
// TELETEXT FUNKCIJA
player.prototype.checkTeletext = function () {
  if (this.mpMP.checkTeletextAvailability()) {
    this.mpMP.teletextSurfaceSwitch("1");
  } else {
    return -1;
  }
};

player.prototype.setArea = function (mLeft, mTop, mWidth, mHeight) {
    if (this.mpMP == null) {
        try {
                this.mpMP = new MediaPlayer();
        } catch(e) {return 0; }
    }
    this.mpMP.setVideoDisplayMode(0);
    this.mpMP.setVideoDisplayArea(mLeft, mTop, mWidth, mHeight);
};

player.prototype.setFullscreen = function() {
    if (this.mpMP == null) {
        try {
                this.mpMP = new MediaPlayer();
        } catch(e) {return 0; }
    }
    this.mpMP.setVideoDisplayArea(0, 0, 1920, 1080);
    //this.mpMP.setVideoDisplayMode(1);
};



var player = new player(apiEndpoint);

// ***** ISPRAVI DA SU TI SVE PUTANJE BEZ POCETNOG / znaka
// NEGO NEKA BUDU tipa src=image/xxx//
//
// ***********************OVO STAVI NA POCETAK DOKUMENTA
// <!doctype html>
//<html lang="no" manifest="cache.manifest">
//
//******************************* NA BODY DODAJES
// <body onload="onInit()">
//
// ********************************** A SAM SKRIPT DODAJES NAKON ZATVORENOG BODY TAGA
//</body>
//<script src="dodaj.js"></script>
//</html>
//
var mopMP = null;

var apiEndpoint = "http://mts.local:3333/";
var apiEndpoint1 = "http://iris.mts.rs/";
var apiEndpoint2 = "https://app-iris.mts-si.tv/";
var apiEndpoint3 = "";

/*
window.onerror = function(msg, url, line, col, error) {
  var extra = !col ? '' : '\ncolumn: ' + col;
  extra += !error ? '' : '\nerror: ' + error;
  dbgLog("Error: " + msg + " URL: " + url + " LINE: " + line + extra);
  $("#dbgDiv").css("display", "block");
  var suppressErrorAlert = true;
  return suppressErrorAlert;
};
*/



if (!window["Utility"]) {
  window["Utility"] = {
    isMock: true,

    trigger: function (event /*type,error_code*/) {
      this.event = event;
      document.onkeydown({
        which: 768,
      });
    },

    getValueByName: function (key) {
      return localStorage.getItem(key);
    },
    setValueByName: function (key, value) {
      localStorage.setItem(key, value);
    },
    getEvent: function () {
      return JSON.stringify(this.event);
    },
  };
  window["iPanel"] = {
    ioctlRead: function () {},
  };
}

function onInit() {
  if (typeof Utility != "undefined" && Utility !== null) {
    Utility.setValueByName("userstatus", 0);
    Utility.setValueByName("ShowPic", "2");
    Utility.setValueByName("EPGReady", "XXXX");
  }
}

function clearEpgCache() {
  Utility.setValueByName("ClearEPGCache", 1);
  Utility.setValueByName("STBMonitorEnable", 1);
  Utility.setValueByName("System.op.Reboot", 1);
}

function playChan(chid) {
  //dbgLog("Satart playChan");

  //var url = "http://live1.play4.tv/TSPinkKuvar/index.m3u8?token=backtoken12";

  var url = getURL(chid);

  //dbgLog(url);

  //console.log('chid', chid);
  //console.log(url);
  var mUrl = { mediaURL: url, mediaType: 3, allowTrickmode: 0 };

  try {
    if (mopMP == null) {
      mopMP = new MediaPlayer();
    }
    var instanceId = mopMP.getNativePlayerInstanceID();

    mopMP.setSingleMedia(JSON.stringify(mUrl));
    mopMP.playFromStart();
  } catch (e1) {
    dbgLog("MEDIA PLAYER ERROR");
    console.log("MEDIA PLAYER ERROR");
  }
}

function stopPlay() {
  if (mopMP !== null) {
    mopMP.stop();
  }
}

function pausePlay() {
  if (mopMP !== null) {
    mopMP.pause();
  }
}

function getURL(chid) {
  var request = new XMLHttpRequest();
  request.open("GET", "http://api.play4.tv/ws/fake_url.pl?chid=" + chid, false);

  request.send();

  var rsp = null;
  if (request.status === 200 && request.readyState === 4) {
    rsp = request.responseText;
  }

  return rsp;
}

function openSettings() {
  Utility.setValueByName("hw_op_stb", "setpage");
}

function dbgLog(line) {
  if (line == "") {
    document.getElementById("dbgDiv").innerHTML = "";
  } else {
    document.getElementById("dbgDiv").innerHTML =
      "<mark>" +
      line +
      "</mark>" +
      "<br>" +
      document.getElementById("dbgDiv").innerHTML.replace("<mark>", "</mark>");
  }
}

var model = Utility.getValueByName("STB_model");
console.log('STB_model',model);

var product = Utility.getValueByName("product_name");
var mac = Utility.getValueByName("MACAddress");
var ip = Utility.getValueByName("stbIP");

// PERCENT UP OR DOWN

function epgScale(wup, hup) {
  var curW = parseInt(Utility.getValueByName("displayZoomWidth"));
  var curH = parseInt(Utility.getValueByName("displayZoomHeight"));

  if (wup != 0) {
    curW = curW + wup;
    Utility.setValueByName("displayZoomWidth", curW);
  }

  if (hup != 0) {
    curH = curH + hup;
    Utility.setValueByName("displayZoomHeight", curH);
  }
}

function default_key(keyCode) {
  if (keyCode == 768) {
    return false;
  } // NEKI EVENT-i

  switch (keyCode) {
    case KEY_RED:
      clearEpgCache();
      return false;
      break;
    case KEY_BLUE:
      location.reload();
      return false;
      break;
    case KEY_YELLOW:
      if ($('link[href="css/style.css"]').attr("href") == "css/style.css") {
        $('link[href="css/style.css"]').attr("href", "css/style_dark.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      } else if (
        $('link[href="css/style_dark.css"]').attr("href") ==
        "css/style_dark.css"
      ) {
        $('link[href="css/style_dark.css"]').attr(
          "href",
          "css/style.css"
        );
        $("#btn_logo").attr("src", "image/logo.svg");
      } 

      break;
    case KEY_SETTINGS:
      openSettings();
      break;
    case KEY_F1:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
    case KEY_GREEN:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
  }
}

function set_previous_element_focus() {
  var focused = document.activeElement;
  $("#id_previous_focus").attr("focus_element", focused.id);
}

function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join(".");
}

function formatDate_yyyymmdd(date,param) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;
  //if (day.length < 2) day =  day;

  return [year, month, day].join(param);
}

function formatTime(date) {
  
  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth()<10?'0':'') +  date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours()<10?'0':'') + date.getHours();
  var minutes = (date.getMinutes()<10?'0':'') +date.getMinutes();
  
  return hours+":"+minutes;

}

function formatTimeEpoch(date) {

  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth()<10?'0':'') +  date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours()<10?'0':'') + date.getHours();
  var minutes = (date.getMinutes()<10?'0':'') +date.getMinutes();

  return hours+":"+minutes;

}
function player(targetIP) {}

player.prototype.setPlayer = function () {
  console.log("setPlayer");
};
player.prototype.playV3 = function () {
  console.log("playV3");
};

player.prototype.playVodV3 = function () {
  console.log("playVodV3");
};

player.prototype.playVOD = function () {
  console.log("playVOD");
};

player.prototype.stop = function () {
  console.log("stop");
};

player.prototype.pause = function () {
  console.log("pause");
};

player.prototype.resume = function () {
  console.log("resume");
};

player.prototype.mediaDur = function () {
  console.log("mediaDur");
};
player.prototype.getState = function () {
  console.log("getState");
};
player.prototype.getCurDur = function () {
  console.log("getCurDur");
};
player.prototype.getCurPos = function () {
  console.log("getCurPos");
};
player.prototype.seekFwdMP = function () {
  console.log("seekFwdMP");
};
player.prototype.seekRewMP = function () {
  console.log("seekRewMP");
};
player.prototype.seekToPos = function () {
  console.log("seekToPos");
};

player.prototype.fastForward = function () {
  console.log("fastForward");
};

player.prototype.fastRewind = function () {
  console.log("fastRewind");
};

player.prototype.joinLive = function () {
  console.log("joinLive");
};
player.prototype.checkTeletext = function () {
  console.log("checkTeletext");
};

player.prototype.setArea = function () {
  console.log("setArea");
};

player.prototype.setFullscreen = function () {
  console.log("setFullscreen");
};

var player = new player();

function Utility_event(ev) {
  console.log("STB EVENT TYPE: " + JSON.stringify(ev));
}

document.onvisibilitychange = event => {
  console.log("Visibility changed!");
  console.log(event);
};

function onPlaying() {
  console.log("ON PLAYING");
  player.mediaDur();
}

function onPause() {
  console.log("ON PAUSE");
}

function onEnded() {
  console.log("ON ENDED");
}
function onLoaded() {
  console.log("ON LOADED");
}
var mycontType = "";
var mycontId = ""; // channelId | vodId
var mymediaId = ""; // mediaId
var myplaybillId = ""; // playbillId if catchup

function player(targetIP) {
  this.targetIP = targetIP;
  this.playerID = 0;
  this.mpMP = null;
  this.contType = "none"; // live|vod
  this.contId = ""; // channelId | vodId
  this.mediaId = ""; // mediaId
  this.playbillId = ""; // playbillId if catchup
  this.player = null;
}

player.prototype.setPlayer = function (contUrl, position) {
  //var manifestUrl = 'https://edge-bg-1.mts-si.tv/rts1/index.mpd?token=K25e2joh5Cd4ytl4XUN6)';
  Utility.play(contUrl, position);
  console.log("SET PLAYER URL_A: " + contUrl);
};
player.prototype.playV3 = function (channelId, mediaId, playbillId, position) {
  console.log(
    "IN playV3 : " + channelId + " mediaId: " + mediaId + " playbillId: " + playbillId + " position: " + position
  );
  console.log(
    "OLD playV3 : " + mycontId + " mediaId: " + mymediaId + " playbillId: " + myplaybillId + " position: " + position
  );
  if (playbillId != 0) {
    if (mycontId == channelId && mymediaId == mediaId && myplaybillId == playbillId && position > 0) {
      // SEEK CONTENT TO POSITION
      player.seekToPos(position);
    } else {
      player.playCatchup(channelId, mediaId, playbillId, position);
    }
  } else if (isHybrid == 0) {
    console.log("MEDIA ID", mediaId, this.mediaId);
    if (mediaId != this.mediaId) player.joinLive(mediaId);
  } else {
    if (mediaId != this.mediaId) player.joinDVBC(mediaId);
  }
};

player.prototype.playCatchup = function (channelId, mediaId, playbillId, position) {
  var url =
    "https://apidev-bg-1.mts-si.tv/api/client/customerProfile/" +
    sys.getItemPermanent("customer_profile") +
    "/content/epgSchedule/" +
    playbillId +
    "?dtype=1";
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting channel status: " + this.status);
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.success == true) {
        var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
        obj.encryption[0].enabled = true;
        pUrl = obj.content_url + "?token=" + obj.protection[0].value;
        //var pUrl = obj.content_url.replace('.mpd', '.m3u8') + "?token="+ obj.protection[0].value;
        console.log("URL : " + pUrl);
        if (obj.encryption[0].enabled) {
          // CHECK WIDEVINE
          var mUrl =
            '{mediaId: "' + mediaId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';
          player.setPlayer(mUrl, position);
          mycontType = "live"; // live|vod
          mycontId = channelId; // channelId | vodId
          mymediaId = mediaId; // mediaId
          myplaybillId = playbillId; // playbillId if catchup
        } else {
          //var mUrl = "{mediaId: \""+channelId+"\", contentId: \"1\", srcType: \"ott\", drmType: \"nodrm\", playUrl: \""+pUrl+"\"}";
          var mUrl =
            '{mediaId: "' + mediaId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';
          player.setPlayer(mUrl, position);
          mycontType = "live"; // live|vod
          mycontId = channelId; // channelId | vodId
          mymediaId = mediaId; // mediaId
          myplaybillId = playbillId; // playbillId if catchup
        }
      } else {
        console.log("GRESKA sucess false");
      }
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.joinLive = function (mediaId) {
  var self = this;
  var url =
    this.targetIP +
    "/api/client/customer/" +
    sys.getItemPermanent("customerId") +
    "/content/liveContent/" +
    mediaId +
    "?dtype=1";
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting channel");
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.success == true) {
        var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
        //var pUrl = obj.content_url.replace('.mpd', '.m3u8') + "?token="+ obj.protection[0].value;
        console.log("URL : " + pUrl);
        if (obj.encryption[0].enabled) {
          // CHECK WIDEVINE
          var mUrl =
            '{mediaId: "' + mediaId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';
          player.setPlayer(mUrl, 0);
          mycontType = "live"; // live|vod
          self.contId = mediaId; // channelId | vodId
          self.mediaId = mediaId; // mediaId
          self.playbillId = ""; // playbillId if catchup
        } else {
          var mUrl =
            '{mediaId: "' + mediaId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';
          player.setPlayer(mUrl, 0);
          mycontType = "live"; // live|vod
          self.contId = mediaId; // channelId | vodId
          self.mediaId = ""; // mediaId
          self.playbillId = ""; // playbillId if catchup
          //var mUrl = "{mediaId: \""+channelId+"\", contentId: \"1\", srcType: \"ott\", drmType: \"nodrm\", playUrl: \""+pUrl+"\"}";
          //player.setPlayer(mUrl, 0);
        }
      } else {
        console.log("GRESKA sucess false:  " + xhr.responseText);
      }
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.joinDVBC = function (mediaId) {
  var url =
    this.targetIP +
    "/api/client/customer/" +
    sys.getItemPermanent("customerId") +
    "/content/liveContent/" +
    mediaId +
    "?dtype=2";
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting channel");
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log("RATKO: " + xhr.responseText);
      if (obj.success == true) {
        var pUrl = obj.content_url;
        // FREQUENCY:714|SYMBOL:6900|ORIGINAL_NETWORK_ID:40961|TRANSPORT_STREAM_ID:17001|SERVICE_ID:110"
        //
        var mUrl =
          '{mediaId: "' + mediaId + '", contentId: "1", srcType: "dvbUri", drmType: "nodrm", playUrl: "' + pUrl + '"}';
        player.setPlayer(mUrl, 0);
        mycontType = "live"; // live|vod
        self.contId = mediaId; // channelId | vodId
        self.mediaId = mediaId; // mediaId
        self.playbillId = ""; // playbillId if catchup
      } else {
        console.log("GRESKA sucess false");
      }
    } else {
      console.log("GRESKA GET CHANNEL");
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.playVodV3 = function (vodId, position) {
  console.log("PLAY VOD vodID: " + vodId + " position: " + position);
  if (mycontType == "vod" && mycontId == vodId && position > 0) {
    player.seekToPos(position);
    return 0;
  } else {
    var url =
      this.targetIP +
      "/api/client/customer/" +
      sys.getItemPermanent("customerId") +
      "/content/movieContent/" +
      vodId +
      "?dtype=1";
    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 2 && this.status != 200) {
        console.log("Error getting channel");
      } else if (this.readyState === 4) {
        var obj = JSON.parse(xhr.responseText);
        console.log(obj);
        if (obj.success == true) {
          //var pUrl = obj.content_url.replace('.mpd', '.m3u8') + "?token="+ obj.protection[0].value;
          var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
          console.log("URL : " + pUrl);
          /*
                                if (obj.encryption[0].enabled) {
                                // CHECK WIDEVINE
        				var mUrl = "{mediaId: \""+vodId+"\", contentId: \"1\", srcType: \"ott\", drmType: \"widevine\", playUrl: \""+pUrl+"\"}";
                                        player.setPlayer(mUrl, position);
                                        mycontType = "vod"; // live|vod
                                        mycontId = vodId; // channelId | vodId
                                        mymediaId = ""; // mediaId
                                        myplaybillId = ""; // playbillId if catchup
                                }
                                else {
				*/
          var mUrl =
            '{mediaId: "' + vodId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';
          player.setPlayer(mUrl, position);
          mycontType = "vod"; // live|vod
          mycontId = vodId; // channelId | vodId
          mymediaId = ""; // mediaId
          myplaybillId = ""; // playbillId if catchup
          //}
        } else {
          console.log("GRESKA sucess false: " + xhr.responseText);
        }
      }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
    xhr.send();
  }
};

player.prototype.playShowV3 = function (showId, position) {
  console.log("PLAY SHOW showID: " + showId + " position: " + position);
  if (mycontType == "show" && mycontId == showId && position > 0) {
    player.seekToPos(position);
    return 0;
  } else {
    var url =
      this.targetIP +
      "/api/client/customer/" +
      sys.getItemPermanent("customerId") +
      "/content/tvShowContent/" +
      showId +
      "?dtype=1";

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 2 && this.status != 200) {
        console.log("Error getting channel");
      } else if (this.readyState === 4) {
        var obj = JSON.parse(xhr.responseText);
        console.log(xhr.responseText);
        if (obj.success == true) {
          var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
          var mUrl =
            '{mediaId: "' + showId + '", contentId: "1", srcType: "ott", drmType: "nodrm", playUrl: "' + pUrl + '"}';

          //if (obj.encryption[0].enabled) {
          // CHECK WIDEVINE
          player.setPlayer(mUrl, position);
          mycontType = "show"; // live|vod
          mycontId = showId; // channelId | vodId
          mymediaId = ""; // mediaId
          myplaybillId = ""; // playbillId if catchup
          /*} else {
                        player.setPlayer(pUrl, "nodrm", position);
                        mycontType = "show"; // live|vod
                        mycontId = showId; // channelId | vodId
                        mymediaId = ""; // mediaId
                        myplaybillId = ""; // playbillId if catchup
                    }
		    */
        } else {
          console.log("GRESKA sucess false");
        }
      }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
    xhr.send();
  }
};

player.prototype.stop = function () {
  Utility.stopPlayer();
  console.log("PLAYER STOP");
};

player.prototype.pause = function () {
  console.log("PLAYER PAUSE");
  Utility.pausePlayer();
};

player.prototype.resume = function () {
  console.log("PLAYER RESUME");
};

player.prototype.mediaDur = function () {
  var dur = Utility.getPlayerDuration();
  var pos = Utility.getPlayerPosition();
  console.log("MEDIA DURATION: " + dur + " POSITION: " + pos);
};
player.prototype.getState = function () {
  var pState = Utility.getPlayerState();
};

player.prototype.getCurDur = function () {
  var dur = Utility.getPlayerDuration();
  return dur; // OVO KOD GLEDANJA UZIVO NEMA NEKOG SMISLA
};
player.prototype.getCurPos = function (startTime) {
  //var someDate = new Date(dateString);
  //someDate = someDate.getTime();
  var pos = Utility.getPlayerPosition();
  return pos;
};
player.prototype.seekFwdMP = function (step) {
  var dur = player.getCurDur();
  var pos = player.getCurPos();
  // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
  if (dur == -1 || pos == -1) {
    return -1;
  }

  var newPos = pos + parseInt(step);
  if (newPos < dur) {
    dbgLog("SHOULD SEEK TO: " + newPos);
    Utility.seekPlayer(newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
  } else {
    newPos = dur - 1; // PREMOTAVMO DO KRAJA minus jednu sekundu
    dbgLog("SHOULD SEEK TO: " + newPos);
    Utility.seekPlayer(newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
  }
};
player.prototype.seekRewMP = function (step) {
  if (this.player != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    if (dur == -1 || pos == -1) {
      return -1;
    }

    var newPos = pos - parseInt(step);
    dbgLog("MEDIA DURATION: " + dur);
    dbgLog("MEDIA POSITION: " + pos);

    if (newPos > 0) {
      Utility.seekPlayer(newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
      console.log("SHOULD SEEK TO: " + newPos);
    } else {
      newPos = 0;
      console.log("SHOULD SEEK TO: " + newPos);
      Utility.seekPlayer(newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    }
  } else {
    return -1; // NESTO NE VALJA
  }
};
player.prototype.seekToPos = function (newPos) {
  console.log("NEW POS SHOULD BE: " + newPos);
  var dur = player.getCurDur();
  var pos = player.getCurPos();
  // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
  dbgLog("MEDIA DUR: " + dur + " POS: " + pos);
  if (dur == -1 || pos == -1) {
    return -1;
  }

  if (newPos > dur || newPos < 0) {
    return -1; // NESTO NE VALJA
  } else {
    Utility.seekPlayer(newPos); // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
  }
};

player.prototype.fastForward = function () {
  return 0;
  var sValue = this.mpMP.getPlaybackMode();
  dbgLog("PLAYBACK MODE: " + sValue);
  var pMode = JSON.parse(sValue).PlayMode;
  var curSpeedStr = "1";
  curSpeedStr = JSON.parse(sValue).Speed.replace("x", "");
  var newSpeed = 1;
  var curSpeed = 1;
  if (pMode == "Normal Play") {
    newSpeed = 2;
    this.mpMP.fastForward(newSpeed);
    return newSpeed;
  } else if (pMode == "Multicast Live") {
    dbgLog("MULTICAST LIVE");
    // THINK ABOUT
  } else if (pMode == "Trickmode") {
    dbgLog("CUR SPEED: " + curSpeedStr);
    if (curSpeedStr.charAt(0) == "-") {
      // REWIND
      newSpeed = 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    } else {
      newSpeed = parseInt(curSpeedStr) * 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    }
  } else {
    return 0;
  }
};

player.prototype.fastRewind = function () {
  return 0;
  var sValue = this.mpMP.getPlaybackMode();
  var pMode = JSON.parse(sValue).PlayMode;
  var curSpeedStr = "1";
  curSpeedStr = JSON.parse(sValue).Speed.replace("x", "");
  var newSpeed = 1;
  var curSpeed = 1;
  if (pMode == "Normal Play") {
    newSpeed = -2;
    this.mpMP.fastRewind(newSpeed);
    return newSpeed;
  } else if (pMode == "Multicast Live") {
    // THINK ABOUT
  } else if (pMode == "Trickmode") {
    dbgLog("CUR SPEED: " + curSpeedStr);
    if (curSpeedStr.charAt(0) == "-") {
      // REWIND
      newSpeed = -2 * parseInt(curSpeedStr);
      this.mpMP.fastRewind(newSpeed);
      return newSpeed;
    } else {
      newSpeed = 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    }
  } else {
    return 0;
  }
};

// TELETEXT FUNKCIJA
player.prototype.checkTeletext = function () {
  if (this.mpMP.checkTeletextAvailability()) {
    this.mpMP.teletextSurfaceSwitch("1");
  } else {
    return -1;
  }
};

player.prototype.setArea = function (mLeft, mTop, mWidth, mHeight) {
  if (this.mpMP == null) {
    try {
      this.mpMP = new MediaPlayer();
    } catch (e) {
      return 0;
    }
  }
  this.mpMP.setVideoDisplayMode(0);
  this.mpMP.setVideoDisplayArea(mLeft, mTop, mWidth, mHeight);
};

player.prototype.setFullscreen = function () {
  if (this.mpMP == null) {
    try {
      this.mpMP = new MediaPlayer();
    } catch (e) {
      return 0;
    }
  }
  this.mpMP.setVideoDisplayArea(0, 0, 1920, 1080);
  //this.mpMP.setVideoDisplayMode(1);
};

/*

player.prototype.resetDvbTable = function () {
    try {
	//Utility.clearDvbcDb();
    } catch (e) {
      return 0;
    }
};

player.prototype.checkDvbTableVersion = function () {
    try {
	var dvbVer = Utility.getValueByName("dvbChannelVersion);
	if (dvbVer !== dvbChanVersion) {
		//player.resetDvbTable();
		// FILL TABLE WITH DATA
	}
    } catch (e) {
      return 0;
    }
};


player.prototype.fillDvbTable = function () {
    try {
	console.log("DUZINA TABELE JE: " + dvbTable.length);
    } catch (e) {
      return 0;
    }
};
*/

var player = new player("https://apidev-bg-1.mts-si.tv/");

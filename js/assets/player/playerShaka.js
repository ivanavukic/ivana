document.onvisibilitychange = event => {
  console.log("Visibility changed!");
  console.log(event);
};

function onPlaying() {
  console.log("ON PLAYING");
  player.mediaDur();
}

function onPause() {
  console.log("ON PAUSE");
}

function onEnded() {
  console.log("ON ENDED");
}

function onLoaded() {
  console.log("ON LOADED");
}

function onProgress() {
  player.showProgress();
}
var mycontType = ""; // channelId | vodId
var mycontId = ""; // channelId | vodId
var mymediaId = ""; // mediaId
var myplaybillId = ""; // playbillId if catchup

function player(targetIP) {
  this.targetIP = targetIP;
  this.playerID = 0;
  this.mpMP = null;
  this.contType = "none"; // live|vod
  this.contId = ""; // channelId | vodId
  this.mediaId = ""; // mediaId
  this.playbillId = ""; // playbillId if catchup
  this.player = null;
  this.video = document.getElementById("video");
  this.video.addEventListener("playing", onPlaying);
  this.video.addEventListener("pause", onPause);
  this.video.addEventListener("ended", onEnded);
  this.video.addEventListener("loadeddata", onLoaded);
  this.video.addEventListener("progress", onProgress);
  shaka.polyfill.installAll();
}

player.prototype.showProgress = function () {
  var state = this.video.readyState;
  var vdur = player.getCurDur();
  var vpos = player.getCurPos();
  //console.log("PROGRESS STATE: " + state + " DUR: " + vdur + " POS: " + vpos);
};

player.prototype.setPlayer = function (contUrl, encrypt, position) {
  encrypt = "widevine";
  if (this.player == null) {
    this.player = new shaka.Player(this.video);
    //window.player = this.player;
    console.log("NULL !!! - KREIRAO PLAYER");
  } else {
    player.stop();
  }
  ////
  //var manifestUrl = 'https://edge-bg-1.mts-si.tv/rts1/index.mpd?token=K25e2joh5Cd4ytl4XUN6)';
  if (encrypt == "widevine") {
    this.player.configure({
      drm: {
        servers: { "com.widevine.alpha": "https://widevine-dash.ezdrm.com/proxy?pX=E5625E" },
        advanced: {
          "com.widevine.alpha": {
            persistentStateRequired: true
          }
        }
      }
    });
    if (position > 0) {
      this.player.load(contUrl, position);
      this.video.play();
    } else {
      this.player.load(contUrl);
      this.video.play();
    }
    console.log("WITH WIDEVINE URL_A: " + contUrl);
  } else {
    if (position > 0) {
      this.player.load(contUrl, position);
    } else {
      this.player.load(contUrl);
    }
    console.log("NO WIDEVINE URL_A: " + contUrl);
  }
};
player.prototype.playV3 = function (channelId, mediaId, playbillId, position) {
  console.log(
    "IN playV3 : " + channelId + " mediaId: " + mediaId + " playbillId: " + playbillId + " position: " + position
  );
  console.log(
    "OLD playV3 : " + mycontId + " mediaId: " + mymediaId + " playbillId: " + myplaybillId + " position: " + position
  );
  if (playbillId != 0) {
    if (mycontId == channelId && mymediaId == mediaId && myplaybillId == playbillId && position > 0) {
      // SEEK CONTENT TO POSITION
      player.seekToPos(position);
    } else {
      player.playCatchup(channelId, mediaId, playbillId, position);
    }
  } else {
    player.joinLive(mediaId);
  }
};

player.prototype.playCatchup = function (channelId, mediaId, playbillId, position) {
  var url =
    "https://apidev-bg-1.mts-si.tv/api/client/customerProfile/" +
    sys.getItemPermanent("customer_profile") +
    "/content/epgSchedule/" +
    playbillId +
    "?dtype=1";
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting channel status: " + this.status);
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.success == true) {
        var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
        console.log("URL : " + pUrl);
        if (obj.encryption[0].enabled) {
          // CHECK WIDEVINE
          player.setPlayer(pUrl, "widevine", position);
          mycontType = "live"; // live|vod
          mycontId = channelId; // channelId | vodId
          mymediaId = mediaId; // mediaId
          myplaybillId = playbillId; // playbillId if catchup
        } else {
          player.setPlayer(pUrl, "", position);
          mycontType = "live"; // live|vod
          mycontId = channelId; // channelId | vodId
          mymediaId = mediaId; // mediaId
          myplaybillId = playbillId; // playbillId if catchup
        }
      } else {
        console.log("GRESKA sucess false");
      }
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.joinLive = function (mediaId) {
  var self = this;
  console.log(mediaId);
  var url =
    this.targetIP +
    "/api/client/customer/" +
    sys.getItemPermanent("customerId") +
    "/content/liveContent/" +
    mediaId +
    "?dtype=1";
  var xhr = new XMLHttpRequest();
  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting channel");
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log(obj);
      if (obj.success == true) {
        var url = obj.content_url.replace("m3u8", "mpd");
        var pUrl = url + "?token=" + obj.protection[0].value;
        console.log("URL : " + pUrl);
        if (obj.encryption[0].enabled || mediaId == 2) {
          // CHECK WIDEVINE
          if (self.mediaId != mediaId) player.setPlayer(pUrl, "widevine");
          mycontType = "live"; // live|vod
          mycontId = mediaId; // channelId | vodId
          mymediaId = mediaId; // mediaId
          myplaybillId = ""; // playbillId if catchup
          self.mediaId = mediaId;
        } else {
          // if (self.mediaId != mediaId)
          player.setPlayer(pUrl, "");
        }
      } else {
        console.log("GRESKA sucess false");
      }
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.playVodV3 = function (vodId, position) {
  console.log("PLAY VOD vodID: " + vodId + " position: " + position);
  if (mycontType == "vod" && mycontId == vodId && position > 0) {
    player.seekToPos(position);
    return 0;
  } else {
    var url =
      this.targetIP +
      "/api/client/customer/" +
      sys.getItemPermanent("customerId") +
      "/content/movieContent/" +
      vodId +
      "?dtype=1";
    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 2 && this.status != 200) {
        console.log("Error getting channel");
      } else if (this.readyState === 4) {
        var obj = JSON.parse(xhr.responseText);
        console.log(obj);
        if (obj.success == true) {
          var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
          console.log("URL : " + pUrl);
          if (obj.encryption[0].enabled) {
            // CHECK WIDEVINE
            player.setPlayer(pUrl, "widevine", position);
            mycontType = "vod"; // live|vod
            mycontId = vodId; // channelId | vodId
            mymediaId = ""; // mediaId
            myplaybillId = ""; // playbillId if catchup
          } else {
            player.setPlayer(pUrl, "nodrm", position);
            mycontType = "vod"; // live|vod
            mycontId = vodId; // channelId | vodId
            mymediaId = ""; // mediaId
            myplaybillId = ""; // playbillId if catchup
          }
        } else {
          console.log("GRESKA sucess false");
        }
      }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
    xhr.send();
  }
};

// PUSTANJE SERIJA tj.EPIZODE NEKOG SERIJALA
// 	   OVO SAM NASAO IZ ODGOVORA ZA SERIJE ID EPIZODE I ONA MI RADI KAD JE PUSTIM
// 	   MISLIM DA SE RADI O SERIJI SETLAND contentId":5833,"contentName":"Šetland
//         player.playShowV3(30585, 0);
//
player.prototype.playShowV3 = function (showId, position) {
  console.log("PLAY SHOW showID: " + showId + " position: " + position);
  if (mycontType == "show" && mycontId == showId && position > 0) {
    player.seekToPos(position);
    return 0;
  } else {
    var url =
      this.targetIP +
      "/api/client/customer/" +
      sys.getItemPermanent("customerId") +
      "/content/tvShowContent/" +
      showId +
      "?dtype=1";

    var xhr = new XMLHttpRequest();

    xhr.addEventListener("readystatechange", function () {
      if (this.readyState === 2 && this.status != 200) {
        console.log("Error getting channel");
      } else if (this.readyState === 4) {
        var obj = JSON.parse(xhr.responseText);
        console.log(obj);
        if (obj.success == true) {
          var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
          console.log("URL : " + pUrl);
          if (obj.encryption[0].enabled) {
            // CHECK WIDEVINE
            player.setPlayer(pUrl, "widevine", position);
            mycontType = "show"; // live|vod
            mycontId = showId; // channelId | vodId
            mymediaId = ""; // mediaId
            myplaybillId = ""; // playbillId if catchup
          } else {
            player.setPlayer(pUrl, "nodrm", position);
            mycontType = "show"; // live|vod
            mycontId = showId; // channelId | vodId
            mymediaId = ""; // mediaId
            myplaybillId = ""; // playbillId if catchup
          }
        } else {
          console.log("GRESKA sucess false");
        }
      }
    });

    xhr.open("GET", url);
    xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
    xhr.send();
  }
};

// TREBA OBRISATI
// POMOCNA FUNKCIJA DJE SAM ISPROBAVAO DA NADJEM NEKU EPIZODU ZA KOJU SAM SUBSCRIBED
player.prototype.checkSerije = function (showId) {
  console.log("GET SERIJA POKUSAJ");
  showId = 30585;
  //var url = this.targetIP + "/api/client/customerProfile/"+sys.getItemPermanent("customer_profile")+"/content/tvShowContent";
  //var url = this.targetIP + "/api/client/customerProfile/"+sys.getItemPermanent("customer_profile")+"/content/tvShowContent/5833/get";
  var url =
    this.targetIP +
    "/api/client/customer/" +
    sys.getItemPermanent("customerId") +
    "/content/tvShowContent/" +
    showId +
    "?dtype=1";
  var xhr = new XMLHttpRequest();

  xhr.addEventListener("readystatechange", function () {
    if (this.readyState === 2 && this.status != 200) {
      console.log("Error getting SERIJA");
    } else if (this.readyState === 4) {
      var obj = JSON.parse(xhr.responseText);
      console.log("SERIJA: " + JSON.stringify(obj));
      if (obj.success == true) {
        var pUrl = obj.content_url + "?token=" + obj.protection[0].value;
        console.log("URL : " + pUrl);
        if (obj.encryption[0].enabled) {
          // CHECK WIDEVINE
          player.setPlayer(pUrl, "widevine", position);
          mycontType = "show"; // live|vod
          mycontId = showId; // channelId | vodId
          mymediaId = ""; // mediaId
          myplaybillId = ""; // playbillId if catchup
        } else {
          player.setPlayer(pUrl, "nodrm", position);
          mycontType = "show"; // live|vod
          mycontId = showId; // channelId | vodId
          mymediaId = ""; // mediaId
          myplaybillId = ""; // playbillId if catchup
        }
      } else {
        console.log("GRESKA sucess false");
      }
    } else {
      console.log("GET SERIJE GRESKA 2");
    }
  });

  xhr.open("GET", url);
  xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
  xhr.send();
};

player.prototype.stop = function () {
  console.log("PLAYER STOP");
  if (this.video != null) {
    this.video.pause();
    this.video.currentTime = 0;
    if (this.player != null) {
      this.player.unload();
    }
  } else {
  }
};

player.prototype.pause = function () {
  console.log("PLAYER PAUSE");
  if (this.video != null) {
    this.video.pause();
  } else {
  }
};

player.prototype.resume = function () {
  if (this.video != null) {
    this.video.play();
  } else {
  }
};

player.prototype.mediaDur = function () {
  if (this.player != null) {
    var dur = this.video.duration;
    var dur2 = this.player.seekRange().end;
    var pos = this.video.currentTime;
    console.log("MEDIA DURATION: " + dur + " POSITION: " + pos + " DUR2: " + dur2);
  }
};
player.prototype.getState = function () {
  if (this.video != null) {
    var pState = this.video.readyState;
    if (pState == 4) {
      return "Play";
    } else {
      return "NotPlay";
    }
    dbgLog("PLAYER STATE: " + pState);
  } else {
    return "notPlay"; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};

player.prototype.getCurDur = function () {
  if (this.video != null) {
    var dur = parseInt(this.video.duration);
    return dur; // OVO KOD GLEDANJA UZIVO NEMA NEKOG SMISLA
  } else {
    return -1; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};
player.prototype.getCurPos = function (startTime) {
  //var someDate = new Date(dateString);
  //someDate = someDate.getTime();
  dbgLog("GET CUR POS START TIME: " + startTime);

  if (this.video != null) {
    var dur = player.getCurDur();
    dbgLog("CUR POS TOTAL DURATION: " + dur);
    var pos = this.video.currentTime;
    if (pos.len > 8) {
      return 900;
    } else {
      return pos;
    }
  } else {
    return -1; // ovako znamo da je player null... tj. da nista ne gledamo
  }
};
player.prototype.seekFwdMP = function (step) {
  if (this.player != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    if (dur == -1 || pos == -1) {
      return -1;
    }

    var newPos = pos + parseInt(step);
    if (newPos < dur) {
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.video.currentTime = newPos; // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    } else {
      newPos = dur - 1; // PREMOTAVMO DO KRAJA minus jednu sekundu
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.video.currentTime = newPos; // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    }
  } else {
    return -1; // NE VALJA NESTO PLAYER NULL PRAVIMO FUNKCIJU ZA OPORAVAK GRESKE
  }
};
player.prototype.seekRewMP = function (step) {
  if (this.player != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    if (dur == -1 || pos == -1) {
      return -1;
    }

    var newPos = pos - parseInt(step);
    dbgLog("MEDIA DURATION: " + dur);
    dbgLog("MEDIA POSITION: " + pos);

    if (newPos > 0) {
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.video.currentTime = newPos; // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    } else {
      newPos = 0;
      dbgLog("SHOULD SEEK TO: " + newPos);
      this.video.currentTime = newPos; // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
    }
  } else {
    return -1; // NESTO NE VALJA
  }
};
player.prototype.seekToPos = function (newPos) {
  console.log("NEW POS SHOULD BE: " + newPos);
  if (this.player != null) {
    var dur = player.getCurDur();
    var pos = player.getCurPos();
    // NESTO NE VALJA PRAVICEMO NOVU FUNKCIJU ZA OPORAVAK GRESKE
    dbgLog("MEDIA DUR: " + dur + " POS: " + pos);
    if (dur == -1 || pos == -1) {
      return -1;
    }

    if (newPos > dur || newPos < 0) {
      return -1; // NESTO NE VALJA
    } else {
      this.video.currentTime = newPos; // OVO JE JEDAN NACIN PREMOTAVANJA IMA U DOKUMENTACIJI OPISANO
      this.video.play();
    }
  } else {
    console.log("Player is NULL!!!!");
    return -1; // NESTO NE VALJA
  }
};

player.prototype.fastForward = function () {
  return 0;
  var sValue = this.mpMP.getPlaybackMode();
  dbgLog("PLAYBACK MODE: " + sValue);
  var pMode = JSON.parse(sValue).PlayMode;
  var curSpeedStr = "1";
  curSpeedStr = JSON.parse(sValue).Speed.replace("x", "");
  var newSpeed = 1;
  var curSpeed = 1;
  if (pMode == "Normal Play") {
    newSpeed = 2;
    this.mpMP.fastForward(newSpeed);
    return newSpeed;
  } else if (pMode == "Multicast Live") {
    dbgLog("MULTICAST LIVE");
    // THINK ABOUT
  } else if (pMode == "Trickmode") {
    dbgLog("CUR SPEED: " + curSpeedStr);
    if (curSpeedStr.charAt(0) == "-") {
      // REWIND
      newSpeed = 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    } else {
      newSpeed = parseInt(curSpeedStr) * 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    }
  } else {
    return 0;
  }
};

player.prototype.fastRewind = function () {
  return 0;
  var sValue = this.mpMP.getPlaybackMode();
  var pMode = JSON.parse(sValue).PlayMode;
  var curSpeedStr = "1";
  curSpeedStr = JSON.parse(sValue).Speed.replace("x", "");
  var newSpeed = 1;
  var curSpeed = 1;
  if (pMode == "Normal Play") {
    newSpeed = -2;
    this.mpMP.fastRewind(newSpeed);
    return newSpeed;
  } else if (pMode == "Multicast Live") {
    // THINK ABOUT
  } else if (pMode == "Trickmode") {
    dbgLog("CUR SPEED: " + curSpeedStr);
    if (curSpeedStr.charAt(0) == "-") {
      // REWIND
      newSpeed = -2 * parseInt(curSpeedStr);
      this.mpMP.fastRewind(newSpeed);
      return newSpeed;
    } else {
      newSpeed = 2;
      this.mpMP.fastForward(newSpeed);
      return newSpeed;
    }
  } else {
    return 0;
  }
};

// TELETEXT FUNKCIJA
player.prototype.checkTeletext = function () {
  if (this.mpMP.checkTeletextAvailability()) {
    this.mpMP.teletextSurfaceSwitch("1");
  } else {
    return -1;
  }
};

player.prototype.setArea = function (mLeft, mTop, mWidth, mHeight) {
  if (this.mpMP == null) {
    try {
      this.mpMP = new MediaPlayer();
    } catch (e) {
      return 0;
    }
  }
  this.mpMP.setVideoDisplayMode(0);
  this.mpMP.setVideoDisplayArea(mLeft, mTop, mWidth, mHeight);
};

player.prototype.setFullscreen = function () {
  if (this.mpMP == null) {
    try {
      this.mpMP = new MediaPlayer();
    } catch (e) {
      return 0;
    }
  }
  this.mpMP.setVideoDisplayArea(0, 0, 1920, 1080);
  //this.mpMP.setVideoDisplayMode(1);
};

var player = new player("https://apidev-bg-1.mts-si.tv/");

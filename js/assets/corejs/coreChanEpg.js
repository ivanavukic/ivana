define("coreChanEpg", ["bluebird"], function (Promise) {
  function ChannelsEpg() {}
  ChannelsEpg.prototype._channels = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting channels");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          console.log(sys.getItemPermanent("customer_profile"));
          if (odgovor.success) {
            for (var i = 0; i < odgovor.content_live.length; i++) {
              odgovor.content_live[i].contentLogo.light =
                dedicatedServer + odgovor.content_live[i].contentLogo.light;
              odgovor.content_live[i].contentLogo.grayscale =
                dedicatedServer + odgovor.content_live[i].contentLogo.grayscale;
              odgovor.content_live[i].contentLogo.dark =
                dedicatedServer + odgovor.content_live[i].contentLogo.dark;
            }
            chanVersion = odgovor.live_version;
            channels = odgovor;
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "GET",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/liveContent"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  ChannelsEpg.prototype.getChannels = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (!channels) {
        ovo
          ._channels()
          .then(function () {
            resolve(channels);
          })
          .catch(function (err) {
            reject(err);
          });
      } else resolve(channels);
    });
  };
  ChannelsEpg.prototype._epgContent = function (content_id) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting epg");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            epg[content_id] = odgovor.content_epg;
            epgVersion = odgovor.epg_version;
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      //console.log(url + "/api/client/customer/" + sys.getItemPermanent("customerId") + "/content/epgContent/" + content_id);
      //console.log(sys.getItemPermanent("token"));

      xhr.open(
        "GET",
        url +
          "/api/client/customer/" +
          sys.getItemPermanent("customerId") +
          "/content/epgContent/" +
          content_id
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  ChannelsEpg.prototype.getEpgContent = function (content_id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (!epg[content_id]) {
        ovo
          ._epgContent(content_id)
          .then(function () {
            resolve(epg);
          })
          .catch(function (err) {
            reject(err);
          });
      } else {
        resolve(epg);
      }
    });
  };
  ChannelsEpg.prototype.getEpgUrl = function (
    // type,
    epgScheduleId,
    dtype
  ) {
    //type -> movie/tvShow
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = JSON.stringify({
        epgScheduleId: epgScheduleId,
      });

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getEpgContent");
        } else if (this.readyState === 4) {
          //console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "GET",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          // type +
          // "Content/" +
          "epgSchedule/" +
          epgScheduleId +
          "?dtype=" +
          dtype
        // dtype
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    });
  };
  ChannelsEpg.prototype._categories = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting categories");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            categories = odgovor;
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "GET",
        url +
          "/api/client/customer/" +
          sys.getItemPermanent("customerId") +
          "/content/categories"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  ChannelsEpg.prototype.getCategories = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (!categories) {
        ovo
          ._categories()
          .then(function () {
            resolve(categories);
          })
          .catch(function (err) {
            reject(err);
          });
      } else resolve(categories);
    });
  };
  ChannelsEpg.prototype.setLiveFavourite = function (chanId) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error setting live favourite");
        } else if (this.readyState === 4) {
          //console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "POST",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/contentLive/" +
          chanId +
          "/favourite"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  ChannelsEpg.prototype.deleteLiveFavourite = function (chanId) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error deleting live favourite");
        } else if (this.readyState === 4) {
          //console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "DELETE",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/contentLive/" +
          chanId +
          "/favourite"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  ///////////////////////////////////////////////////////////////////////
  ChannelsEpg.prototype.getKategorije = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getCategories()
        .then(function (res) {
          translated = [];
          for (var i = 0; i < res.live_categories.length; i++) {
            var temp = {
              code: res.live_categories[i].live_category_id,
              name: res.live_categories[i].name,
              ID: res.live_categories[i].live_category_id,
              contentType: "VIDEO_CHANNEL",
            };
            translated.push(temp);
          }
          translated.splice(0, 0, {
            code: 1001,
            name: "Svi kanali",
            ID: 1,
            contentType: "VIDEO_CHANNEL",
          });

          translated.splice(1, 0, {
            code: 1002,
            name: "Omiljeni",
            ID: 2,
            contentType: "VIDEO_CHANNEL",
          });
          resolve(translated);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  ChannelsEpg.prototype.getKanale = function (cat, fav) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getChannels()
        .then(function (res) {
          var translated = [];
          console.log(res);
          for (var i = 0; i < res.content_live.length; i++) {
            var temp = {
              ChanKey: res.content_live[i].contentPosition,
              ChanStatus: "",
              ChanURL: "",
              PLTVEnable: "",
              PauseLength: "",
              PreviewEnable: "",
              contentId: res.content_live[i].contentId,
              idKanala: i,
              idOriginal: res.content_live[i].liveId,
              ime: res.content_live[i].contentName,
              kategorija: "",
              mediaId: res.content_live[i].liveId,
              omiljeni: res.content_live[i].favourite,
              slika: res.content_live[i].contentLogo.light,
              zakljucan: "0",
              subscribed: res.content_live[i].subscribed,
              adult: res.content_live[i].adult,
              catchupStatus: res.content_live[i].contentLiveCatchupStatus,
              catchupDuration: res.content_live[i].contentLiveCatchupDuration,
            };
            if (res.content_live[i].categories[0]) {
              temp.kategorija =
                res.content_live[i].categories[0].live_category_id;
            }
            if (temp.omiljeni) temp.omiljeni = 1;
            else temp.omiljeni = 0;
            translated.push(temp);
          }
          var filteredChannels = [];
          if (typeof cat == "number" && typeof fav == "number") {
            for (var i = 0; i < translated.length; i++) {
              if (
                translated[i].kategorija == cat &&
                translated[i].omiljeni == 1
              )
                filteredChannels.push(translated[i]);
            }
            resolve(filteredChannels);
          } else if (typeof cat == "number") {
            for (var i = 0; i < translated.length; i++) {
              if (translated[i].kategorija == cat)
                filteredChannels.push(translated[i]);
            }
            resolve(filteredChannels);
          } else if (typeof fav == "number") {
            for (var i = 0; i < translated.length; i++) {
              if (translated[i].omiljeni == 1)
                filteredChannels.push(translated[i]);
            }
            resolve(filteredChannels);
          } else {
            resolve(translated);
          }
        })
        .catch(function (res) {
          reject(res);
        });
    });
  };
  ChannelsEpg.prototype.getKanalItem = function (id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getKanale("", "")
        .then(function (res) {
          for (var i = 0; i < res.length; i++) {
            if (id == res[i].idOriginal) resolve(res[i]);
          }
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };

  ChannelsEpg.prototype.getChannelNumber = function (id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      var temp = {
        ChanKey: id,
        ChanStatus: "",
        ChanURL: "",
        PLTVEnable: "",
        PauseLength: "",
        PreviewEnable: "",
        contentId: id,
        idKanala: i,
        idOriginal: id,
        ime: "",
        kategorija: "",
        mediaId: id,
        omiljeni: "",
        slika: "",
        zakljucan: "0",
      };

      resolve(temp);
    });
  };

  ChannelsEpg.prototype.getTrenutniEpg = function (id) {
    var ovo = this;
    var contentId;
    return new Promise(function (resolve, reject) {
      ovo
        .getKanalItem(id)
        .then(function (res) {
          contentId = res.contentId;
          ovo
            .getEpgContent(contentId)
            .then(function (res) {
              //console.log(res);
              var epgItem = res[contentId];
              var translated = [];
              for (var i = 0; i < epgItem.length; i++) {
                var temp = {
                  CUTVStatus: "",
                  ID: epgItem[i].scheduleId,
                  channelID: id,
                  mediaId: id,
                  contentType: "",
                  customFields: [],
                  endTime: epgItem[i].end,
                  isCPVR: "",
                  isCUTV: "",
                  isFillProgram: "",
                  isNPVR: "",
                  name: epgItem[i].title,
                  picture: {
                    backgrounds: [dedicatedServer + epgItem[i].background],
                    stills: [dedicatedServer + epgItem[i].background],
                  },
                  purchaseEnable: "0",
                  rating: { name: "", ID: epgItem[i].rating },
                  slsType: "",
                  startTime: epgItem[i].start,
                };
                temp.endTime = new Date(temp.endTime).getTime();
                // -
                // new Date().getTimezoneOffset() * 60000;
                temp.startTime = new Date(temp.startTime).getTime();
                // -
                // new Date().getTimezoneOffset() * 60000;
                if (temp.rating.ID == null) {
                  temp.rating.ID = 0;
                } else if (temp.rating.ID == 18) {
                  temp.rating.ID = temp.rating.ID * 10;
                }
                if (temp.rating.ID == 18) {
                  temp.rating.name = "R18";
                } else {
                  temp.rating.name = "R" + temp.rating.ID;
                }
                translated.push(temp);
              }
              resolve(translated);
            })
            .catch(function (err) {
              reject(err);
            });
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  ChannelsEpg.prototype.getTrenutno = function (id) {
    var danas = new Date();
    var trenutniEpoch = danas.getTime();
    var ovo = this;
    var contentId;
    return new Promise(function (resolve, reject) {
      ovo
        .getTrenutniEpg(id)
        .then(function (res) {
          nadjiTrenutno(res);
        })
        .catch(function (err) {
          reject(err);
        });

      function nadjiTrenutno(data) {
        var sr = Math.ceil(data.length / 2); //zaokruzi na vece
        var l = 0;
        var d = data.length - 1;

        var index = 0;
        if (d > 0 && trenutniEpoch > data[1].startTime) index = 1;

        while (d - l > 1) {
          if (trenutniEpoch < data[sr].startTime) d = sr;
          else if (trenutniEpoch > data[sr].startTime) l = sr;
          else {
            index = sr;
            break;
          }
          sr = Math.ceil((l + d) / 2);

          if (d == sr) index = sr - 1;
          else if (l == sr) index = sr;
        }
        resolve(data[index]);
      }
      // function nadjiTrenutno() {
      //   for (var i = 0; i < epg.length; i++) {
      //     if (
      //       trenutniEpoch > epg[i].startTime &&
      //       trenutniEpoch < epg[i].endTime
      //     ) {
      //       resolve(epg[i]);
      //     }
      //   }
      // }
    });
  };
  ChannelsEpg.prototype.getEpgItem = function (epgId) {
    if (this.epg.length == 0) return;
    for (var i = 0; i < this.epg.length; i++) {
      if (!this.epg[i]) {
        continue;
      }
      for (var j = 0; j < this.epg[i].length; j++) {
        if (this.epg[i][j].scheduleId == epgId) {
          return this.epg[i][j];
        }
      }
    }

    return;
  };
  ChannelsEpg.prototype.createFavorite = function (id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .setLiveFavourite(id)
        .then(function (res) {
          resolve(res);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  ChannelsEpg.prototype.deleteFavorite = function (id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .deleteLiveFavourite(id)
        .then(function (res) {
          resolve(res);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  return new ChannelsEpg();
});

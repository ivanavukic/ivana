define("coreHeartbeat", ["bluebird", "core"], function (Promise, core) {
  function Hbeat() {}
  Hbeat.prototype.heartBeat = function () {
    // ≤1 time/15min
    return new Promise(function (resolve, reject) {
      resolve(true);
    });
  };
  return new Hbeat();
});

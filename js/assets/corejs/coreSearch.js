define("coreSearch", ["bluebird", "core"], function (Promise, core) {
  function Search() {}

  Search.prototype.search = function (searchVal) {
    //type:tvShow/movie
    //sort:az, za, oldest, newest, rating
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting vod");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });
      //apidev-bg-1.mts-si.tv/api/client/customerProfile/3/content/search?search=a
      https: xhr.open("GET", url + "/api/client/customerProfile/" + sys.getItemPermanent("customer_profile") + "/content/search?search=" + searchVal);
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  return new Search();
});

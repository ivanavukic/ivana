define("coreLogin", ["bluebird"], function (Promise) {
  function Login() {}
  Login.prototype.loginNonDynamicDevice = function () {
    var odgovor;
    var data = "";
    var ovo = this;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error logging in");
        } else if (this.readyState === 4) {
          console.log(this.responseText);
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            console.log(odgovor);
            dedicatedServer = odgovor.dedicated_server;
            macAddress = odgovor.uid;
            stbVersion = odgovor.deviceModelId;
            console.log(device);
            console.log(odgovor.auth_token);

            sys.setItemPermanent("token", odgovor.auth_token);
            sys.setItemPermanent("customerId", odgovor.customer_id);
            sys.setItemPermanent("deviceId", odgovor.device_id);
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open("POST", url + "/api/client/device/" + device + "/login");

      xhr.send(data);
    });
  };
  Login.prototype.loginDynamicDevice = function (
    user,
    pass,
    uid,
    modelId,
    devName
  ) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = JSON.stringify({
        username: user,
        password: pass,
        uid: uid,
        deviceModelId: modelId,
        deviceName: devName,
      });

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error logging in");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            dedicatedServer = odgovor.dedicated_server;
            sys.setItemPermanent("token", odgovor.auth_token);
            sys.setItemPermanent("customerId", odgovor.customer_id);
            sys.setItemPermanent("deviceId", odgovor.device_id);
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open("POST", url + "/api/client/login");
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    });
  };
  Login.prototype.logoutDevice = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error logging out");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "DELETE",
        url +
          "/api/client/customer/" +
          sys.getItemPermanent("customerId") +
          "/device/" +
          sys.getItemPermanent("deviceId") +
          "/token"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Login.prototype.initDevice = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error initializing device");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(
        'sys.getItemPermanent("token")',
        sys.getItemPermanent("token")
      );

      xhr.open("GET", url + "/api/client/device/" + device + "/init");
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Login.prototype.tokenValidation = function () {
    // TODO proveriti da li radi
    var ovo = this;
    var odgovor;
    var xhr = new XMLHttpRequest();
    return new Promise(function (resolve, reject) {
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error validating token");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open("GET", url + "/api/client/token/status");
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
      xhr.send();
    });
  };
  Login.prototype.getCustomerProfiles = function () {
    var ovo = this;
    var odgovor;
    var temp = {};

    var customerId = sys.getItemPermanent("customerId");

    // var xhr = new XMLHttpRequest();
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting customer profiles");
        } else if (this.readyState === 4) {
          console.log(this.readyState);
          odgovor = JSON.parse(this.responseText);
          console.log(odgovor);
          console.log(
            curProfilePosition,
            "Ovo je trenutna pozicija u nizu profila"
          );
          if (odgovor.success) {
            profili = odgovor.profiles;
            // for (var i = 0; i < odgovor.profiles.length; i++) {
            //   temp.customer_profile_id =
            //     odgovor.profiles[i].customer_profile_id;
            //   temp.customer_id = odgovor.profiles[i].customer_id;
            //   temp.name = odgovor.profiles[i].name;
            //   temp.avatar_id = odgovor.profiles[i].avatar_id;
            //   temp.child = odgovor.profiles[i].child;
            //   temp.logo = odgovor.profiles[i].logo;
            //   temp.pin = odgovor.profiles[i].pin;
            //profiles.push(temp);

            //}
            sys.setItemPermanent(
              "curProfileId",
              odgovor.profiles[curProfilePosition].customer_profile_id
            );
            console.log(profili);
            resolve(profili);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(sys.getItemPermanent("token"));

      xhr.open(
        "GET",
        url +
          "/api/client/customer/" +
          sys.getItemPermanent("customerId") +
          "/profiles"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  Login.prototype.checkSubscriberPassword = function (text) {
    var customer_id = profili[curProfilePosition].customer_id;
    var odgovor;

    return new Promise(function (resolve, reject) {
      var data = "pin=" + text;

      var xhr = new XMLHttpRequest();
      var data = JSON.stringify({ pin: text });
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting customer profiles");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          console.log(odgovor);

          if (odgovor.success) {
            resolve(true);
          } else {
            reject(odgovor.message);
          }
        }
      });
      console.log(url);

      xhr.open(
        "POST",
        url + "api/client/customer/" + customer_id + "/pin/verify"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    });
  };

  Login.prototype.checkProfilePassword = function (text) {
    var profileID = profili[curProfilePosition].customer_profile_id;
    var odgovor;
    var passOK;

    return new Promise(function (resolve, reject) {
      var data = "pin=" + text;

      var xhr = new XMLHttpRequest();
      var data = JSON.stringify({ pin: text });
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting customer profiles");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          console.log(odgovor);
          if (odgovor.success) {
            resolve(true);
          } else {
            reject(odgovor.message);
          }
        }
      });
      console.log(url);
      xhr.open(
        "POST",
        url + "api/client/customerProfile/" + profileID + "/pin/verify"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    });
  };

  Login.prototype.changeProfilePassword = function (old_pin, confirmed_pin) {
    var odgovor;
    var passOK;
    var profileID = profili[curProfilePosition].customer_profile_id;
    var xhr = new XMLHttpRequest();
    console.log(
      old_pin,
      confirmed_pin,
      "Old pin i confirmed za metodu check pass"
    );

    return new Promise(function (resolve, reject) {
      var data = JSON.stringify({ pin: old_pin, newPin: confirmed_pin });

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting customer profiles");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);

          if (odgovor.success) {
            passOK = true;
            console.log("Password changed");
            resolve("password changed");
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "POST",
        url + "api/client/customerProfile/" + profileID + "/pin/change"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));
      xhr.setRequestHeader("Content-Type", "application/json");

      xhr.send(data);
    });
  };

  Login.prototype.myPurchases = function () {};
  Login.prototype.allPackages = function () {};

  return new Login();
});

define("coreProducts", ["bluebird", "core", "appConfig", "moment"], function(Promise, core, appConfig, moment) {
  var exports = {};
  var internal = {};

  /**
   * @summary Using V2 platform to get purchase history for the last month
   * @param {object} options
   * @param {number} [options.period] Return history for last 'period' months
   * @param {number} options.subscriptionScope
   * 0: querying the subscription relationships generated in the period starting from the fromDate value to the toDate value.
   * 1: querying the subscription relationships whose validity period overlaps the period starting from the fromDate value to the toDate value.
   * @param {string} options.contenttype Separate multiple values with a comma (,). The options are as follows: VAS, CHANNEL, AUDIO_CHANNEL, VIDEO_CHANNEL, VOD, AUDIO_VOD, VIDEO_VOD, PROGRAM
   * @param {string} options.producttype 1: times-based product 0: monthly package product
   * @param {string} options.orderType
   * 0: by subscription time in ascending order
   * 1: by subscription time in descending order
   * 2: by subscription relationship expiration time in ascending order
   * 3: by subscription relationship expiration time in descending order
   * 4: by pricing object name in ascending order
   * 5: by pricing object name in descending order
   * @returns {Promise<any>}
   */
  exports.getPurchaseHistoryV2 = function(options) {
    options = options || {};

    var period = options.period || 1;
    var fromDate = moment()
      .subtract(period, "months")
      .format("YYYYMMDDhhmmss");
    var toDate = moment()
      .endOf("month")
      .format("YYYYMMDDhhmmss");

    var data = {
      profileId: options.profileId,
      fromDate: fromDate,
      toDate: toDate,
      subscriptionScope: options.subscriptionScope,
      count: appConfig.platformFetchCountLimit,
      offset: 0,
      contenttype: options.contenttype,
      producttype: options.producttype,
      orderType: options.orderType,
      priceobjectType: options.priceobjectType
    };
    return core.sendEpgRequest({ path: "QueryOrder", data: data });
  };


  /**
   *
   * @param options
   * @param options.profileId
   * @returns {Promise<*>}
   */
  exports.getVodPurchaseHistory = function(options) {
    return exports.getPurchaseHistoryV2({
      period: 3,
      contenttype: "VOD",
      producttype: 1,
      orderType: 1,
      subscriptionScope: 1,
      profileId: options.profileId
    });
  };

  /**
   *
   * @param options
   * @param options.profileId
   * @returns {Promise<*>}
   */
  exports.getPackagePurchaseHistory = function(options) {
    return exports.getPurchaseHistoryV2({
      producttype: 0,
      orderType: 1,
      isMain: 0,
      priceobjectType: 4,
      subscriptionScope: 0,
      profileId: options.profileId
    });
  };


  /**
   *
   * @param options
   * @param options.profileId
   * @returns {*}
   */
  exports.getAllPurchaseHistory = function(options) {
    return new Promise(function(resolve, reject) {
      Promise.all([exports.getPackagePurchaseHistory({ profileId: options.profileId }), exports.getVodPurchaseHistory({ profileId: options.profileId })]).then(function(data) {
        var allProducts = data[0] && data[0].counttotal ? data[0].productlist : [];
        if (data[1] && data[1].counttotal) {
          allProducts = allProducts.concat(data[1].productlist);
        }
        var activeVodProducts = [];
        var activePackageProducts = [];
        var inactiveProducts = [];
        allProducts.forEach(function(product) {
          if (moment(product.endtime, "YYYYMMDDHHmmss").isAfter() && product.type === "1") {
            activeVodProducts.push(product);
          } else if (moment(product.endtime, "YYYYMMDDHHmmss").isAfter() && product.type === "0") {
            activePackageProducts.push(product);
          } else {
            inactiveProducts.push(product);
          }
        });
        resolve({
          activeVodProducts: activeVodProducts,
          activePackageProducts: activePackageProducts,
          inactiveProducts: inactiveProducts
        });
      }).catch(reject);
    });
  };


  /**
   * A subscriber uses this interface to query product information.
   * For example, a subscriber obtains subscribable main packages.
   * @param {Object} options
   * @param {string} options.queryType -
   * ALL: querying all products
   * UNORDERED: querying products that have not been subscribed to
   * BY_IDLIST: querying products by ID list
   *
   * @param {string[]} [options.productIds] -
   * Product ID.
   * This parameter is mandatory when the value of queryType is BY_IDLIST. If this parameter is contained, all other subsequent input parameters will not take effect.
   *
   * @param {string} [options.productType="ALL"] -
   * ORDER_BY_CYCLE: duration-based
   * PPV: times-based
   * ALL: all products
   *
   * @param {Number} [options.count=-1] - Default:-1 Maximum:50 Minimum:-1
   * Number of records obtained in one query. The client specifies the total number of obtained records, which cannot exceed 50. When the number of obtained records exceeds the maximum, an error is returned.
   * If this parameter is set to -1 or not set, a maximum of 50 records are obtained in one query.
   *
   * @param {Number} [options.offset=0] - Default:0 Minimum:0 -
   * The default value is 0, indicating that the query starts from the first record.
   *
   * @param {string} [options.isMain="ALL"] -
   * NO: non-main package | YES: main package | ALL: all
   *
   * @param {string} [options.isEnableOnlinePurchase="NO"] -
   * NO: no | YES: back
   * @returns {Promise<any>}
   */
  exports.queryProducts = function(options) {
    return new Promise(function(resolve, reject) {
      options = options || {};
      var path = "QueryProduct";
      var data = {
        queryType: options.queryType || "ALL",
        productIds: options.productIds || [],
        productType: options.productType || "ALL",
        count: options.count || -1,
        offset: options.offset || 0,
        isMain: options.isMain || "ALL",
        isEnableOnlinePurchase: options.isEnableOnlinePurchase || "NO"
      };

      core.sendVspReqest({ path: path, data: data }).then(resolve, reject);
    });
  };

  /**
   *
   * @param {{correlator: string, productID: string, priceID: string, priceObjects?: Object[]}|{}} options
   * @returns {Promise<any>}
   */
  internal.subscribe = function(options) {
    return new Promise(function(resolve, reject) {
      options = options || {};
      var path = "/VSP/V3/SubscribeProduct";


      var data = {
        type: 4,
        correlator: options.correlator,
        subscribe: {
          productID: options.productID,
          isAutoExtend: 0,
          payment: { servicePayType: "CASH", subServicePayType: 1 },
          priceObjects: options.priceObjects ? options.priceObjects : [{
            ID: options.priceID,
            type: "2",
            contentType: "VOD"
          }]
        }
      };

      console.log("///////////////////////////////////");
      console.log(data);

      core
        .sendRequestWithThrottling({
          url: path,
          data: data
        }).then(resolve, reject);
    });
  };


  /**
   *
   * @param {{correlator: string, productID: string, priceID: string, priceObjects?: Object[]}|{}} options
   * @returns {Promise<any>}
   */
  exports.subscribe = function(options) {
    return new Promise(function(resolve, reject) {
      internal.subscribe(options).then(function(data) {
        resolve(data);
      }, reject);
    });
  };

  return exports;
});

define("products", ["bluebird", "corePackages"], function(Promise, corePackages) {
  var exports = {};


  /**
   *
   * @param options
   * @param options.username
   * @returns {*}
   */
  exports.getAvailablePackages = function(options) {
    return new Promise(function(resolve, reject) {
      corePackages.getPackages({ username: options.username }).then(resolve, reject);
    });
  };


  /**
   * @param {Object} options
   * @param {string} options.username    Username for login
   * @param {array} options.packageIds packageTisId from getPackages
   * @par
   */
  exports.deactivatePackage = function(options) {
    return new Promise(function(resolve, reject) {
      corePackages.sendGatewayWithTokenRequest({ method: "deactivate_package", data: options }).then(resolve, reject);
    });
  };


  /**
   *@summary Get account that user used to activate account
   * @param {Object} options
   * @param {string} options.username  Username for login
   * @returns {Promise<any>}
   */
  exports.getUserActivationAccount = function(options) {
    return new Promise(function(resolve, reject) {
      corePackages.getPackages({
        username: options.username,
        forceReload: options.forceReload || undefined,
      }).then(function(data) {
        var activationPayment = undefined;
        var accountId = undefined;
        var account = undefined;
        _.forEach(data.packages, function(item) {
          if (item.status === "Aktivan") {
            activationPayment = item.activationPayment;
            accountId = item.billingAccountId;
          }
        });
        if (accountId && activationPayment === 1) {
          corePackages.getAccounts(options).then(function(accounts) {
            _.forEach(accounts.listGetActiveBillingAccountResponse, function(item) {

              if (item.billingAccounId === accountId) {
                account = item;
              }
            });
            resolve({ account: account });
          }, reject);
        } else {
          resolve({ activationPayment: activationPayment });
        }
      }, reject);
    });
  };


  return exports;
});

define("password", ["bluebird", "core"], function(Promise, core) {
  var exports = {};


  /**
   *
   * @param {object} options
   * @param {string} options.password - MaxLength:128
   * Password, in plaintext.
   * The QueryCustomizeConfig interface is invoked to
   * query the configuration of UserPwdMinLength and UserPwdUpperCaseLetters for password verification.
   *
   * @param {Number} [options.type=3] - Password type.
   * The options are as follows:
   * 0: common profiles password
   * 1: subscription password
   * 2: subscriber login password
   * 3: super profiles password
   * 4: parental control password
   * NOTE:
   * The VSC server checks the password based on the value of type.
   * If the password is correct, the check is passed.
   * The client determines the specific scenario to invoke the interface.
   *
   * @param {string} [options.correlator] - MaxLength:128
   * Unique ID of a password verification request sent by the client.
   * When the client needs to execute a transaction that requires password verification,
   * this parameter must be contained in the password verification request.
   * The VSP generates a transaction and returns the transaction token.
   * To pass the authentication by the VSP in subsequent operation requests,
   * the client needs to carry the correlator parameter value and the transaction token.
   * The transaction token is valid for 15 minutes.
   *
   *
   * @returns {Promise<any>}
   */
  exports.checkPassword = function(options) {
    return new Promise(function(resolve, reject) {
      options = options || {};
      core
        .sendRequestWithThrottling({
          url: "/VSP/V3/CheckPassword",
          data: options
        })
        .then(resolve, reject);
    });
  };


  /**
   * @param {Object} options
   * @param {String} options.password
   * @param {String|Number} options.type - if type:2 it is purchase pin
   * @param {String|Number} [options.correlator]
   */
  exports.checkMainProfilePassword = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: options.type || 1,
      correlator: options.correlator
    });
  };

  /**
   * @param {Object} options
   * @param {String} options.password
   * @param {String|Number} [options.correlator]
   */
  exports.commonProfilePassword = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: 0,
      correlator: options.correlator
    });
  };

  /**
   * @summary common profiles password - current common profile (Main or Sub profile)
   * @param {Object} options
   * @param {String} options.password
   * @param {String} options.type
   * @param {String|Number} [options.correlator]
   */
  exports.checkCurrentProfilePassword = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: options.type || 0,
      correlator: options.correlator
    });
  };

  /**
   * @param {Object} options
   * @param {String} options.password
   * @param {String|Number} [options.correlator]
   */
  exports.checkSubscriptionPassword = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: 1,
      correlator: options.correlator
    });
  };

  /**
   * @param {Object} options
   * @param {String} options.password
   * @param {String|Number} [options.correlator]
   */
  exports.checkPurchasePin = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: 2,
      correlator: options.correlator
    });
  };

  /**
   * @param {Object} options
   * @param {String} options.password
   * @param {String|Number} [options.correlator]
   */
  exports.checkParentalControlPin = function(options) {
    return exports.checkPassword({
      password: options.password,
      type: 4,
      correlator: options.correlator
    });
  };


  return exports;
});

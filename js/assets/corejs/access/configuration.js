define("configuration", ["bluebird", "core", "coreConfiguration", "underscore"], function(Promise, core, coreConfiguration, _) {

  function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

  function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }

  var exports = {};
  var internal = {
    clientParameters:{},
    platformParameters:{},
    languageParameters:{}
  };

  exports.getHeartBeatTimings = function() {
    return { interval: internal.platformParameters.TVMSHeartbitInterval, delay: internal.platformParameters.TVMSDelayLength }
  };

  exports.getMusicChannelCategory = function() {
    return internal.clientParameters.MusicChannelCategory
  };


  exports.getCurrencyInfo = function(){
    return internal.clientParameters.currency
  }

  internal.flattenConfigurationObject = function(configurationObject) {
    var result = configurationObject;
    var valuesUnprepared = result.values;
    delete result.values;
    _.forEach(valuesUnprepared, function(item) {
      item.values = _.isArray(item.values)
        ? item.values[0] !== null && item.values[0].split(",").length > 1
          ? item.values[0].split(",")
          : item.values[0]
        : item.values;
      result[item.key] = item.values;
    });
    return result;
  };

  exports.getConfigurations = function() {
    return new Promise(function(resolve,reject) {
      coreConfiguration.queryCustomizeConfig({ queryType: "0,2,4,7" }).then(function(resp) {

        var platformParameters = {};
        var clientParameters = {};
        _.forEach(resp.configurations,function(item) {
          if (item.cfgType === "2") {
            platformParameters = internal.flattenConfigurationObject(_objectSpread({}, item));
          } else if (item.cfgType === "0") {
            clientParameters = internal.flattenConfigurationObject(_objectSpread({}, item));
          }
        });
        internal.clientParameters = clientParameters
        internal.platformParameters =platformParameters
        internal.clientParameters.currency = { currencyAlphCode: resp.currencyAlphCode, currencyRate: resp.currencyRate }
        internal.clientParameters.ratings = resp.ratings && resp.ratings[0] ? resp.ratings[0].ratings : undefined

        resolve({ platformParameters: platformParameters, clientParameters: clientParameters });
      }, reject);
    });
  };

  exports.getLanguageConfiguration = function() {
    return new Promise(function(resolve, reject){
      coreConfiguration.queryCustomizeConfig({ queryType: '5' }).then(function (data)  {
        if (data && data.languages) {
          internal.languageParameters = data.languages
          resolve(data)
        } else {
          reject()
        }
      }, reject)
    })
  }
  return exports;
});

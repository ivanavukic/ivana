define('heartbeat', ['coreHeartbeat', 'configuration', 'constants',"bluebird"], function (coreHeartbeat, configuration, constants, Promise) {
  function ownKeys(object, enumerableOnly) { var keys = Object.keys(object); if (Object.getOwnPropertySymbols) { var symbols = Object.getOwnPropertySymbols(object); enumerableOnly && (symbols = symbols.filter(function (sym) { return Object.getOwnPropertyDescriptor(object, sym).enumerable; })), keys.push.apply(keys, symbols); } return keys; }

  function _objectSpread(target) { for (var i = 1; i < arguments.length; i++) { var source = null != arguments[i] ? arguments[i] : {}; i % 2 ? ownKeys(Object(source), !0).forEach(function (key) { _defineProperty(target, key, source[key]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(target, Object.getOwnPropertyDescriptors(source)) : ownKeys(Object(source)).forEach(function (key) { Object.defineProperty(target, key, Object.getOwnPropertyDescriptor(source, key)); }); } return target; }

  function _defineProperty(obj, key, value) { if (key in obj) { Object.defineProperty(obj, key, { value: value, enumerable: true, configurable: true, writable: true }); } else { obj[key] = value; } return obj; }


  var exports = {};
  var lowestTime = 60;
  var defaultTime = 900;
  var defaultPlayHeartbeat = 300;
  var keyMinuteTickTime = 1000 * 60;
  var internal = {
    currentChannel: undefined,
    currentVOD: undefined,
    blackoutIntervalId: 0,
    playVODHeartbeatIntervalId: 0,
    playChannelHeartbeatIntervalId: 0,
    heartbeatIntervalId: 0,
    nextCallInterval: 0,
    lastHeartbeatInterval: defaultTime,
    lastHeartbeatResp: {},
    currentVersions: {
      channelversion: {
        id: 'channelVersion',
        event: constants.KEY_VERSION_CHANGED_CHANNEL
      },
      subscribeversion: {
        id: 'subscribeversion',
        event: constants.KEY_VERSION_CHANGED_SUBSCRIBE
      },
      reminder: {
        id: 'reminder',
        event: constants.KEY_VERSION_CHANGED_REMINDER
      },
      favorite: {
        id: 'favorite',
        event: constants.KEY_VERSION_CHANGED_FAVORITE
      },
      preFavorite: {
        id: 'preFavorite',
        event: constants.KEY_VERSION_CHANGED_PRE_FAVORITE
      },
      lock: {
        id: 'lock',
        event: constants.KEY_VERSION_CHANGED_LOCK
      },
      preLock: {
        id: 'preLock',
        event: constants.KEY_VERSION_CHANGED_PRE_LOCK
      },
      bookmark: {
        id: 'bookmarkVersion',
        event: constants.KEY_VERSION_CHANGED_BOOKMARK
      },
      ppvVersion: {
        id: 'ppvVersion',
        event: constants.KEY_VERSION_CHANGED_PPV
      },
      pvrVersion: {
        id: 'pvrVersion',
        event: constants.KEY_VERSION_CHANGED_NPVR
      },
      profile: {
        id: 'profile',
        event: constants.KEY_VERSION_CHANGED_PROFILE
      }
    }
  };

  // events.on(constants.KEY_LOGOUT, function () {
  //   exports.stopHeartBeat();
  // });


  exports.getInternalData = function () {
    return internal;
  };

  exports.getReminderCurrentVersion = function () {
    return internal.currentVersions.reminder.current || 0;
  };

  exports.getLastHeartbeatResp = function () {
    return internal.lastHeartbeatResp;
  };

  exports.stopHeartBeat = function () {
    clearTimeout(internal.heartbeatIntervalId);
  };

  exports.delayStartHeartbeat = function (nextCallInterval) {
    var conf = configuration.getHeartBeatTimings();
    var time = Math.max(nextCallInterval || conf.delay, lowestTime);
    internal.lastHeartbeatInterval = time;
    exports.stopHeartBeat();
    internal.heartbeatIntervalId = setTimeout(function () {
      exports.startHeartBeat().then(function (t) {
        internal.nextCallInterval = t;
      }).catch(function (err) {
        exports.stopHeartBeat();
      });
    }, 1000 * time);
  };

  exports.getCurrentVersions = function () {
    return internal.currentVersions;
  };

  exports.getLastHeartBeatVersions = function (data) {
    var _ref = data || internal.lastHeartbeatResp && internal.lastHeartbeatResp.personalDataVersions || {},
      bookmark = _ref.bookmark,
      channel = _ref.channel,
      favorite = _ref.favorite,
      lock = _ref.lock,
      ppvVersion = _ref.ppvVersion,
      reminder = _ref.reminder,
      profile = _ref.profile,
      preFavorite = _ref.preFavorite,
      preLock = _ref.preLock,
      pvrVersion = _ref.pvrVersion;

    var channelVersions = (channel || '0|0').split('|');
    var channelversion = channelVersions[0],
      subscribeversion = channelVersions[1];
    var mod = {
      channel: channel || '0|0',
      //The format is channelversion|subscribeversion.
      channelversion: channelversion || '0',
      subscribeversion: subscribeversion || '0',
      bookmark: bookmark || '0',
      favorite: favorite || '0',
      lock: lock || '0',
      ppvVersion: ppvVersion || '0',
      reminder: reminder || '0',
      profile: profile || '0',
      preFavorite: preFavorite || '0',
      preLock: preLock || '0',
      pvrVersion: pvrVersion || '0'
    };
    internal.lastHeartbeatResp.personalDataVersions = _objectSpread({}, mod);
    return internal.lastHeartbeatResp.personalDataVersions;
  };

  exports.getEventNameByEventProperty = function (currentEventProperty) {
    if (currentEventProperty && internal.currentVersions[currentEventProperty]) {
      return internal.currentVersions[currentEventProperty];
    }
  };

  exports.setEventCurrentVersionByEventProperty = function (currentEventProperty, version) {
    if (currentEventProperty && internal.currentVersions[currentEventProperty]) {
      internal.currentVersions[currentEventProperty].current = version;
    }
  };

  exports.setLastVersionByEventString = function (eventString, data) {
    return exports.setEventCurrentVersionByEventProperty(eventString, data);
  };

  exports.setLastVersionByEventStringAndProperty = function (eventString, property, data) {
    var eventProp = exports.getEventNameByEventProperty(eventString);
    eventProp[property] = data;
  };

  exports.startHeartBeat = function () {
    return new Promise(function (resolve, reject) {
      exports.stopHeartBeat();
      coreHeartbeat.onLineHeartbeat().then(function (data) {
        var temp = +(data && data.nextCallInterval);
        internal.nextCallInterval = !temp || temp < lowestTime ? lowestTime : data && data.nextCallInterval;
        exports.delayStartHeartbeat(internal.nextCallInterval);
        exports.checkForNewVersionAndTriggerEvent(data);
        resolve(internal.nextCallInterval);
      }).catch(function (e) {
        return reject(e);
      });
    });
  };

  exports.checkForNewVersionAndTriggerEvent = function (option) {
    // console.debug('checking if we have new versions ::: ')
    var data = exports.getLastHeartBeatVersions(option && option.personalDataVersions);
    var currentVersions = exports.getCurrentVersions();
    Object.keys(currentVersions).forEach(function (key) {
      if (currentVersions[key].current && data[key] && data[key] !== currentVersions[key].current) {
        console.debug("heartbeat update for ".concat(currentVersions[key].event, " from version ").concat(currentVersions[key].current, " to version ").concat(data[key]));
        exports.setEventCurrentVersionByEventProperty(key, data[key]);
        // events.trigger(currentVersions[key].event);
      } else {
        currentVersions[key].current = data[key];
      }
    });
  };

  exports.onMinuteTick = function () {
    var seconds = Math.floor(Date.now() / 1000 % 60);
    var executeInMilliseconds = (60 - seconds) * 1000;
    // console.debug(constants.KEY_MINUTE_TICK + ' executing in milliseconds: ', executeInMilliseconds)
    // console.debug('since time is: ', new Date().toString())

    setTimeout(function () {
      internal.blackoutIntervalId = setInterval(function () {
        console.debug('tick ' + constants.KEY_MINUTE_TICK + ' date: ', new Date().toString())
        // events.trigger(constants.KEY_MINUTE_TICK);
      }, keyMinuteTickTime);
    }, executeInMilliseconds);
  };

  return exports;
});

define("coreSettings", ["bluebird", "core"], function (Promise, core) {
  function Settings() {}

  //Podesavanja
  /////////////////////////////////
  //apiji koji se pozivaju prilikom unosa pina i ulaska u meni podesavanja
  // Settings.prototype.checkPass = function (pass) {
  //   // ≤1 time/20s
  //   //proverava uneti pin kod
  //   var ovo = this;
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       password: pass,
  //       type: 3,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/CheckPassword", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.getBookmark = function () {
  //   //≤2 times/s
  //   //bookmark

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       bookmarkTypes: ["VOD", "SERIES", "PROGRAM"],
  //       filter: {
  //         ratingID: "180",
  //       },
  //       count: 50,
  //       offset: 0,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryBookmark", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.queryProfile = function () {
  //   //≤2 times/s
  //   var ovde = this;
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       type: 1,
  //       count: 1000,
  //       offset: 0,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryProfile", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.queryCredit = function (usrName) {
  //   //≤1 time/s

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       queryType: 0,
  //       profileID: usrName,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryCreditBalance", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // ////////////////////////////////////////////////////////////////////////
  // //Reminders - poziva se VSP/V3/QueryReminder koji je sadrzan u funkciji .getReminder
  // //Promeni vreme za podsetnik, jezik, roditeljska kontrola, definisi limit potrosnje, promeni ime profila,
  // Settings.prototype.modifyProfile = function () {
  //   //≤2 times/s
  //   // this.profileModifier. = {
  //   //   this.profileModifier.reminderInterval: "5", //opcije su 0, 5, 10, 15
  //   //   this.profileModifier.language: "sr", //opcije su "en" i "sr"
  //   //   this.profileModifier.parentalControlID: "0", //opcije su 0, 7, 12, 14, 16, 18, 180 (18+)
  //   //   this.profileModifier.spendLimit: 1000, //opcije su 0, 500, 1000, 2000, -1 (neograniceno)
  //   //   this.profileModifier.profileName: "testui" //menja ime profila u zeljeni string
  //   // };

  //   return new Promise(function (resolve, reject) {
  //     var prntlCtrlName;
  //     if (profileModifier.parentalControlID == 180) {
  //       prntlCtrlName = "R18+";
  //     } else {
  //       prntlCtrlName = "R" + profileModifier.parentalControlID;
  //     }

  //     var data = {
  //       profile: {
  //         isNeedSubscribePIN: "0",
  //         isDisplayInfoBar: "0",
  //         isShowMessage: "1",
  //         subscriberID: "testui",
  //         extensionFields: [
  //           {
  //             values: ["0"],
  //             key: "refreshPasswordFlag",
  //           },
  //         ],
  //         profileType: "0",
  //         purchaseEnable: "1",
  //         ratingName: prntlCtrlName,
  //         loginName: "testui",
  //         ratingID: profileModifier.parentalControlID,
  //         quota: profileModifier.spendLimit,
  //         ID: "testui",
  //         lang: profileModifier.language,
  //         isDefaultProfile: profileModifier.defaultProfile,
  //         hasCollectUserPreference: "0",
  //         isSendSMSForReminder: "0",
  //         profilePINEnable: "1",
  //         isReceiveSMS: "0",
  //         profileVersion: "1636194211348",
  //         pushStatus: "1",
  //         logoURL: "pic0.png",
  //         leadTimeForSendReminder: "5",
  //         isFilterLevel: "0",
  //         multiscreenEnable: "0",
  //         name: profileModifier.profileName,
  //         receiveADType: "1",
  //         reminderInterval: profileModifier.reminderInterval,
  //       },
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/ModifyProfile", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Lista uredjaja i promena imena istih
  // Settings.prototype.deviceList = function (usrName) {
  //   //≤2 times/s
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       deviceType: "2",
  //       subscriberID: usrName,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryDeviceList", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.changeDeviceName = function (deviceId, input) {
  //   //≤2 times/s

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       device: {
  //         ID: deviceId,
  //         name: input,
  //         deviceType: "2",
  //       },
  //     };
  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/ModifyDeviceInfo", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Omiljeni kanali - Koriste se funkcije za dobijanje kanala, i funkcije ._favorite, .createFavorite i .deleteFavorite za manipulaciju favoritima
  // Settings.prototype.createFavorite = function (id) {
  //   //≤2 times/s

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       favorites: [{ contentID: id, contentType: "CHANNEL" }],
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/CreateFavorite", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.deleteFavorite = function (id) {
  //   //≤2 times/s

  //   return new Promise(function (resolve, reject) {
  //     var data = { contentIDs: [id], contentTypes: ["CHANNEL"], deleteType: 0 };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/DeleteFavorite", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype._favorite = function () {
  //   //≤2 times/s

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       contentTypes: ["CHANNEL"],
  //       ratingID: "180",
  //       sortType: "FAVO_TIME:DESC",
  //       offset: 0,
  //       count: 50,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryFavorite", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Zakljucavanje kanala i kategorija - Koriste se funkcije za dobijanje kanala, i funkcije .queryLocked, .lock i .unlock za manipulaciju zakljucanim kanalima
  // //Funkcijama lock i unlock treba predati ID kanala ili ID kategorije, i "CHANNEL" ili "SUBJECT" u zavisnosti da li je kanal ili kategorija
  // //id kategorija se dobija iz .getVodCatalogue
  // Settings.prototype.queryLocked = function (usrName, category) {
  //   //≤2 times/s
  //   //category moze biti "CHANNEL" za kanale i "SUBJECT" za kategorije
  //   var ovo = this;
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       lockTypes: [category],
  //       profileID: usrName,
  //       offset: 0,
  //       count: 50,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/QueryLock", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.lock = function (id, category, usrName) {
  //   //≤2 times/s

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       locks: [
  //         {
  //           itemID: id,
  //           lockType: category,
  //           profileID: usrName,
  //         },
  //       ],
  //       profileID: usrName,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/CreateLock", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // Settings.prototype.unlock = function (id, category, usrName) {
  //   //≤2 times/s
  //   //Naknadno promeniti ulaz u niz po potrebi

  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       itemIDs: [id],
  //       lockTypes: [category],
  //       profileID: usrName,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/DeleteLock", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Istorija kupovine
  // //Vraca kupljenje VODove ili pakete. parametri fromDate i toDate nisam siguran odakle se dobijaju
  // //parametar contenttype: "VOD" je za VODove, dok za pakete ga nema
  // Settings.prototype.istorijaKupovine = function () {
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       profileId: -1,
  //       fromDate: "20210806034901",
  //       toDate: "20211231115959",
  //       subscriptionScope: 1,
  //       count: 50,
  //       offset: 0,
  //       contenttype: "VOD",
  //       producttype: 1,
  //       orderType: 1,
  //     };

  //     core
  //       .basicRequestWithUrl({
  //         url1: "EPG/JSON/QueryOrder",
  //         data: data,
  //       })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Promena PINa - koristi se .checkPass za proveru pina pa .changePass za promenu
  // Settings.prototype.changePass = function (
  //   //≤1 time/20s
  //   currentPass,
  //   newPass,
  //   confirmNewPass,
  //   usrName
  // ) {
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       oldPwd: currentPass,
  //       newPwd: newPass,
  //       confirmPwd: confirmNewPass,
  //       type: 0,
  //       subProfileID: usrName,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/ModifyPassword", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Promena pina za kupovinu - koristi se .checkPass za proveru trenutnog pina pa .changePurchasePass za promenu pina za kupovinu
  // Settings.prototype.changePurchasePass = function (
  //   // ≤1 time/20s
  //   currentPass,
  //   newPurchasePass,
  //   usrName
  // ) {
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       profileID: usrName,
  //       type: 2,
  //       adminProfilePassword: currentPass,
  //       resetPwd: newPurchasePass,
  //     };

  //     core
  //       .basicRequestWithUrl({ url1: "VSP/V3/ResetPassword", data: data })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // //Informacije o uredjajima - info o uredjajima, primer type-s je "Chrome_Irdeto_Widevine", nisam mogao da testiram za druge
  // Settings.prototype.deviceInfo = function (type) {
  //   return new Promise(function (resolve, reject) {
  //     var data = {
  //       terminalType: type,
  //     };

  //     core
  //       .basicRequestWithUrl({
  //         url1: "EPG/JSON/QueryAccessLicense",
  //         data: data,
  //       })
  //       .then(function (res) {
  //         resolve(res);
  //       })
  //       .catch(function (err) {
  //         reject(err);
  //       });
  //   });
  // };
  // ////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
  // Settings.prototype.subscribe = function (id) {
  //   var ovo = this;
  //   return new Promise(function (resolve, reject) {
  //     var odgovor;
  //     var data = JSON.stringify({
  //       productid: id,
  //     });

  //     var xhr = new XMLHttpRequest();

  //     xhr.addEventListener("readystatechange", function () {
  //       if (this.readyState === 2 && this.status != 200) {
  //         reject("Heartbeat failed");
  //       } else if (this.readyState === 4) {
  //         odgovor = JSON.parse(this.responseText);
  //         console.log(odgovor);
  //         if (odgovor.result.retCode == "000000000") {
  //           resolve(odgovor);
  //         } else {
  //           reject(odgovor.result.retMsg);
  //         }
  //       }
  //     });

  //     xhr.open("GET", ovo.url + "EPG/JSON/Subscribe");
  //     xhr.setRequestHeader("Content-Type", "text/plain");

  //     xhr.send(data);
  //   });
  // };
  return new Settings();
});

define("coreVods", ["bluebird"], function (Promise) {
  function Vods() {}
  Vods.prototype._announcements = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting announcements");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            frontPage = odgovor;
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open("GET", url + "/api/client/customerProfile/" + sys.getItemPermanent("customer_profile") + "/intro");
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  Vods.prototype.getAnnouncements = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (!frontPage) {
        ovo
          ._announcements()
          .then(function () {
            resolve(frontPage);
          })
          .catch(function (err) {
            reject(err);
          });
      } else resolve(frontPage);
    });
  };
  Vods.prototype.getAllVodCatalogues = function () {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting vod catalogues");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open("GET", url + "/api/client/customer/" + sys.getItemPermanent("customerId") + "/content/catalogs");
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Vods.prototype.getVod = function (type, catId, sort) {
    //type:tvShow/movie
    //sort:az, za, oldest, newest, rating
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting vod");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            if (type == "tvShow") tvShowVersion = odgovor.tvShow_version;
            else if (type == "movie") movieVersion = odgovor.movie_version;
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "GET",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content?categoryId=" +
          catId +
          "&sort=" +
          sort
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  Vods.prototype.getMovieTvShowDetails = function (type, contentId) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var xhr = new XMLHttpRequest();
      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting movie or show details");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/" +
          contentId +
          "/get"
      );

      xhr.open(
        "GET",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/" +
          contentId +
          "/get"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send();
    });
  };
  Vods.prototype.getMoviesShowsByPackage = function (type) {
    //type => movie/tvShow
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting movies/shows by package");
        } else if (this.readyState === 4) {
          odgovor = JSON.parse(this.responseText);
          console.log(odgovor);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(
        url + "/api/client/customerProfile/" + sys.getItemPermanent("customer_profile") + "/content/" + type + "Content"
      );
      console.log(sys.getItemPermanent("token"));
      console.log(data);

      xhr.open(
        "GET",
        url + "/api/client/customerProfile/" + sys.getItemPermanent("customer_profile") + "/content/" + type + "Content"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Vods.prototype.getMoviesShowsBySpecificPackage = function (type, package) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting movies/shows by package");
        } else if (this.readyState === 4) {
          //console.log(this.responseText);
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/catalog/" +
          package
      );

      xhr.open(
        "GET",
        url +
          "/api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/catalog/" +
          package
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Vods.prototype.getAllMoviesShows = function (type, category, catalogue, sort, pagination) {
    var ovo = this;
    var odgovor;
    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getting all movies/shows");
        } else if (this.readyState === 4) {
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      console.log(
        url +
          "api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/all?categoryId=" +
          category +
          "&sort=" +
          sort +
          "&page=" +
          pagination +
          "&catalogId=" +
          catalogue
      );
      console.log(sys.getItemPermanent("token"));

      xhr.open(
        "GET",
        url +
          "api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/" +
          type +
          "Content/all?categoryId=" +
          category +
          "&sort=" +
          sort +
          "&page=" +
          pagination +
          "&catalogId=" +
          catalogue
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  Vods.prototype.getTvShowEpisodeDetail = function (tvShowId) {
    var odgovor;
    var ovo = this;

    return new Promise(function (resolve, reject) {
      var data = "";

      var xhr = new XMLHttpRequest();

      xhr.addEventListener("readystatechange", function () {
        if (this.readyState === 2 && this.status != 200) {
          reject("Error getTvShowEpisodeDetail");
        } else if (this.readyState === 4) {
          console.log("Djordjije");
          console.log(JSON.parse(this.responseText));
          odgovor = JSON.parse(this.responseText);
          if (odgovor.success) {
            resolve(odgovor);
          } else {
            reject(odgovor.message);
          }
        }
      });

      xhr.open(
        "GET",
        url +
          "api/client/customerProfile/" +
          sys.getItemPermanent("customer_profile") +
          "/content/tvShowContent/" +
          tvShowId +
          "/getEpisode"
      );
      xhr.setRequestHeader("x-auth-token", sys.getItemPermanent("token"));

      xhr.send(data);
    });
  };
  ////////////////////////////////////////////////////////

  Vods.prototype.getTrenutniProgram = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[1]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  Vods.prototype.getFrontpageBookmarks = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[2]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  Vods.prototype.getPopularnoOveNedelje = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[3]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  Vods.prototype.getTrenutniProgram1 = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[4]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  Vods.prototype.getCatchupRecm = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[7]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };
  Vods.prototype.getVodCatalogue = function (type, id) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      var katalozi = [];
      if (id == 7000) {
        console.log("vodsByCatalogue", vodsByCatalogue);

        //var vodsByCatalogue = "";
        if (type == "movie" && vodsByCatalogue) {
          sortByCatalogue(vodsByCatalogue);
        } else if (type == "tvShow" && seriesByCatalogue) {
          sortByCatalogue(seriesByCatalogue);
        } else {
          console.log("x3");

          ovo
            .getMoviesShowsByPackage(type)
            .then(function (res) {
              console.log(res);
              if (type == "movie") {
                res = res.catalog_content;
                vodsByCatalogue = res;
                console.log("getVodCatalogue", vodsByCatalogue);
              } else {
                res = res.content_tvShows;
                seriesByCatalogue = res;
              }
              sortByCatalogue(res);
            })
            .catch(function (err) {
              reject(err);
            });
        }

        function sortByCatalogue(res) {
          var tempKatalog = [];
          for (var i = 0; i < res.length; i++) {
            tempKatalog[i] = {};
            tempKatalog[i].name = res[i].packageName;
            tempKatalog[i].ID = res[i].packageId;
            tempKatalog[i].code = res[i].packageId;
            tempKatalog[i].hasChildren = "1";
            for (var j = i; j < res.length; j++) {
              if (res[j].packageName != tempKatalog[i].name) {
                i = j;
                break;
              }
              if (j == res.length - 1) {
                i = j;
              }
            }
          }
          var tempKatalog1 = [];
          for (var i = 0; i < tempKatalog.length; i++) {
            if (tempKatalog[i]) {
              tempKatalog1.push(tempKatalog[i]);
            }
          }

          if (type == "movie") vodCatalogue = tempKatalog1;
          else seriesCatalogue = tempKatalog1;

          console.log(tempKatalog1);

          resolve(tempKatalog1);
        }
      } else {
        ovo
          .getMoviesShowsBySpecificPackage(type, id)
          .then(function (res) {
            if (type == "movie") {
              res = res.catalog_content;
              vodsByGenre = res;
            } else {
              res = res.content_tvShows;
              seriesByGenre = res;
            }
            var tempKatalog = [];

            for (var i = 0; i < res.length; i++) {
              tempKatalog[i] = {};
              tempKatalog[i].name = res[i].categoryName;
              tempKatalog[i].ID = res[i].categoryId;
              for (var j = i; j < res.length; j++) {
                if (res[j].categoryName != tempKatalog[i].name) {
                  i = j;
                  break;
                }
                if (j == res.length - 1) {
                  i = j;
                }
              }
            }
            var tempKatalog1 = [];
            for (var i = 0; i < tempKatalog.length; i++) {
              if (tempKatalog[i]) {
                tempKatalog1.push(tempKatalog[i]);
              }
            }
            ovo.getVodCatalogue(type, 7000).then(function (res) {
              for (var i = 0; i < res.length; i++) {
                if (res[i].ID == id) id = res[i].name;
              }
              for (var i = 0; i < tempKatalog1.length; i++) {
                tempKatalog1[i] = {
                  ID: tempKatalog1[i].ID + "|" + id,
                  code: tempKatalog1[i].ID + "|" + id,
                  hasChildren: "1",
                  name: tempKatalog1[i].name,
                  parentSubjectID: ""
                };
              }
              resolve(tempKatalog1);
            });
          })
          .catch(function (err) {
            reject(err);
          });
      }
    });
  };
  Vods.prototype.getVodCategory = function (type, category, all) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (all) {
        var cat = "";
        var ctlg = "";
        var niz = category.split("");
        for (var i = 0; i < niz.length; i++) {
          if (typeof Number(niz[i]) == "number" && !isNaN(Number(niz[i]))) {
            cat += niz[i];
          } else {
            niz.shift();
            for (var j = i; j < niz.length; j++) {
              ctlg += niz[j];
            }
            if (type == "movie") {
              for (var x = 0; x < vodCatalogue.length; x++) {
                if (ctlg == vodCatalogue[x].name) ctlg = vodCatalogue[x].ID;
              }
            } else {
              for (var y = 0; y < seriesCatalogue.length; y++) {
                if (ctlg == seriesCatalogue[y].name) ctlg = seriesCatalogue[y].ID;
              }
            }

            break;
          }
        }
        console.log(type, cat, ctlg);
        ovo.getAllMoviesShows(type, cat, ctlg, "az", 1).then(function (res) {
          resolve(res.catalog_content);
        });
      } else if (category.indexOf("|") != -1) {
        var tempNiz = [];
        var cat = "";
        var niz = category.split("");
        for (var i = 0; i < niz.length; i++) {
          if (typeof Number(niz[i]) == "number" && !isNaN(Number(niz[i]))) {
            cat += niz[i];
          } else {
            niz.shift();
            break;
          }
        }
        if (type == "movie") {
          for (var i = 0; i < vodsByGenre.length; i++) {
            if (cat == vodsByGenre[i].categoryId) {
              tempNiz.push(vodsByGenre[i]);
            }
          }
        } else {
          for (var i = 0; i < seriesByGenre.length; i++) {
            if (cat == seriesByGenre[i].categoryId) {
              tempNiz.push(seriesByGenre[i]);
            }
          }
        }
        resolve(tempNiz);
      } else {
        var tempNiz = [];
        if (type == "movie") {
          for (var i = 0; i < vodsByCatalogue.length; i++) {
            if (category == vodsByCatalogue[i].packageId) {
              tempNiz.push(vodsByCatalogue[i]);
            }
          }
        } else {
          for (var i = 0; i < seriesByCatalogue.length; i++) {
            if (category == seriesByCatalogue[i].packageId) {
              tempNiz.push(seriesByCatalogue[i]);
            }
          }
        }
        resolve(tempNiz);
      }
    });
  };
  Vods.prototype.getSingleVod = function (type, id, isEpisode) {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      if (isEpisode) {
        ovo
          .getTvShowEpisodeDetail(id)
          .then(function (res) {
            resolve(res);
          })
          .catch(function (err) {
            reject(err);
          });
      } else {
        ovo
          .getMovieTvShowDetails(type, id)
          .then(function (res) {
            resolve(res);
          })
          .catch(function (err) {
            reject(err);
          });
      }
    });
  };

  /***************************************************************************************************/

  Vods.prototype.get_mts_najgledanije_u_bioskopu = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[4]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };

  Vods.prototype.get_mts_trenutno_na_tv = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[1]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };

  Vods.prototype.get_mts_najgledaniji_kanali = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          resolve(res.content[3]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };

  Vods.prototype.get_mts_propusteno = function () {
    var ovo = this;
    return new Promise(function (resolve, reject) {
      ovo
        .getAnnouncements()
        .then(function (res) {
          console.log(res);
          resolve(res.content[0]);
        })
        .catch(function (err) {
          reject(err);
        });
    });
  };

  return new Vods();
});

var setMaliVideo = null;
var activePage = "";

/*
window.onerror = function (msg, url, line, col, error) {
  var extra = !col ? "" : "\ncolumn: " + col;
  extra += !error ? "" : "\nerror: " + error;
  dbgLog("Error: " + msg + " URL: " + url + " LINE: " + line + extra);
  $("#dbgDiv").css("display", "block");
  var suppressErrorAlert = true;
  return suppressErrorAlert;
};
*/

if (!window["Utility"]) {
  window["Utility"] = {
    isMock: true,

    trigger: function (event /*type,error_code*/) {
      this.event = event;
      document.onkeydown({
        which: 768
      });
    },

    getValueByName: function (key) {
      return localStorage.getItem(key);
    },
    setValueByName: function (key, value) {
      localStorage.setItem(key, value);
    },
    getEvent: function () {
      return JSON.stringify(this.event);
    }
  };
  window["iPanel"] = {
    ioctlRead: function () {}
  };
}

function setStore(name, val) {
  Utility.setValueByName(name, val);
}

function getStore(name) {
  return Utility.getValueByName(name);
}

function onInit() {
  if (typeof Utility != "undefined" && Utility !== null) {
    setStore("userstatus", 0);
    setStore("ShowPic", "2");
    setStore("EPGReady", "XXXX");
  }
}

function clearEpgCache() {
  setStore("ClearEPGCache", 1);
  setStore("STBMonitorEnable", 1);
  setStore("System.op.Reboot", 1);
}

function openSettings() {
  setStore("hw_op_stb", "setpage");
}

function dbgLog(line) {
  if (line == "") {
    document.getElementById("dbgDiv").innerHTML = "";
  } else {
    document.getElementById("dbgDiv").innerHTML =
      "<mark>" + line + "</mark>" + "<br>" + document.getElementById("dbgDiv").innerHTML.replace("<mark>", "</mark>");
  }
}

// SCREEN SCALE
// PERCENT UP OR DOWN
function epgScale(wup, hup) {
  var curW = parseInt(getStore("displayZoomWidth"));
  var curH = parseInt(getStore("displayZoomHeight"));

  if (wup != 0) {
    curW = curW + wup;
    setStore("displayZoomWidth", curW);
  }

  if (hup != 0) {
    curH = curH + hup;
    setStore("displayZoomHeight", curH);
  }
}

function default_key(keyCode) {
  if (keyCode == 768) {
    return false;
  } // NEKI EVENT-i

  switch (keyCode) {
    case KEY_RED:
      clearEpgCache();
      return false;
      break;
    case KEY_BLUE:
      setStore("ClearEPGCache", 1);
      setStore("STBMonitorEnable", 1);
      location.reload();
      return false;
      break;
    case KEY_YELLOW:
      if ($('link[href="css/style.css"]').attr("href") == "css/style.css") {
        $('link[href="css/style.css"]').attr("href", "css/style_dark.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      } else if ($('link[href="css/style_dark.css"]').attr("href") == "css/style_dark.css") {
        $('link[href="css/style_dark.css"]').attr("href", "css/style.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      }

      break;
    case KEY_SETTINGS:
      openSettings();
      break;
    case KEY_F1:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
    case KEY_GREEN:
      if ($("#dbgDiv:visible").length != 0) {
        $("#dbgDiv").css("display", "none");
      } else {
        $("#dbgDiv").css("display", "block");
      }
      return false;
      break;
  }
}

function set_previous_element_focus() {
  var focused = document.activeElement;
  $("#id_previous_focus").attr("focus_element", focused.id);
}

function formatDate(date) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;

  return [day, month, year].join(".");
}

function formatDate_yyyymmdd(date, param) {
  var d = new Date(date),
    month = "" + (d.getMonth() + 1),
    day = "" + d.getDate(),
    year = d.getFullYear();

  if (month.length < 2) month = "0" + month;
  if (day.length < 2) day = "0" + day;
  //if (day.length < 2) day =  day;

  return [year, month, day].join(param);
}

function formatTime(date) {
  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth() < 10 ? "0" : "") + date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours() < 10 ? "0" : "") + date.getHours();
  var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();

  return hours + ":" + minutes;
}

function formatTimeEpoch(date) {
  var date = new Date(date);
  var day = date.getDate();
  var month = (date.getMonth() < 10 ? "0" : "") + date.getMonth();
  var year = date.getFullYear();
  var hours = (date.getHours() < 10 ? "0" : "") + date.getHours();
  var minutes = (date.getMinutes() < 10 ? "0" : "") + date.getMinutes();

  return hours + ":" + minutes;
}

function hideBody() {
  document.body.style.opacity = "0";
}

function showBody() {
  document.body.style.opacity = "1";
}

function isBodyVisible() {
  if (document.body.style.opacity != "0") {
    return 1;
  } else {
    return 0;
  }
}

function toogleBody() {
  if (isBodyVisible()) {
    hideBody();
  } else {
    showBody();
  }
}

function shutDown() {
  setStore("hw_op_stb", "powerOff");
}

function volumeUP() {}

function volumeDown() {}

function maliVideo(id_l, id_c, direction, id_item, id_media) {
  dbgLog(
    "id_l " + id_l + " id_c " + id_c + " direction " + direction + " id_item " + id_item + " id_media " + id_media
  );
  if (id_l != 0) {
    return 0;
  }
  showMyElem(id_l, id_c, direction);
  clearInterval(setMaliVideo);
  setMaliVideo = setInterval(hideMyElem, 1500, id_l, id_c, direction, id_item, id_media);
}

function showMyElem(id_l, id_c, direction) {
  if (id_l != 0) {
    dbgLog("NIJE LISTA 0 ZA SHOW");
    return;
  }
  dbgLog("SHOW ELEM: " + id_l + " C: " + id_c);
  var elem2 = null;
  document.getElementById("sjenka-div").style.backgroundColor = "#e8e8e8";
  player.stop();
  if (direction == "down") {
    try {
      elem2 = document.getElementById("id_start_" + id_l + "_" + (Number(id_c) - 1)).children[0].children[0]
        .children[0];
      elem2.style.opacity = "1";
    } catch (eh) {}
    try {
      elem2 = document.getElementById("id_start_" + id_l + "_" + Number(id_c)).children[0].children[0].children[0];
      elem2.style.opacity = "1";
    } catch (eh) {}
    try {
      elem2 = document.getElementById("id_start_" + id_l + "_" + (Number(id_c) + 1)).children[0].children[0]
        .children[0];
      elem2.style.opacity = "1";
    } catch (eh) {}
  } else if (direction == "right") {
    if (id_c - 1 < 0) {
      return 0;
    }
    try {
      elem2 = document.getElementById("id_start_" + id_l + "_" + Number(id_c - 1)).children[0].children[0].children[0];
      if (elem2 != null) {
        elem2.style.opacity = "1";
      }
    } catch (eh) {}
  } else {
    try {
      elem2 = document.getElementById("id_start_" + id_l + "_" + Number(id_c + 1)).children[0].children[0].children[0];
      if (elem2 != null) {
        elem2.style.opacity = "1";
      }
    } catch (eh) {}
  }
}

function hideMyElem(id_l, id_c, direction, id_item, id_media) {
  //$('#id_start_' + id_l + '_' + Number(id_c)).css("opacity", "0.5");
  clearInterval(setMaliVideo);
  playMali(0, 0, 0, 0, id_item, id_media);
  document.getElementById("sjenka-div").style.backgroundColor = "transparent";
  dbgLog("mali Video: " + id_c);
  var elem = null;
  var elem2 = null;
  if (direction == "right") {
    try {
      elem = document.getElementById("id_start_" + id_l + "_" + Number(id_c)).children[0].children[0].children[0];
      if (elem != null) {
        elem.style.opacity = "0";
      }
    } catch (eh) {}
  } else {
    try {
      elem = document.getElementById("id_start_" + id_l + "_" + Number(id_c)).children[0].children[0].children[0];
      if (elem != null) {
        elem.style.opacity = "0";
      }
    } catch (eh) {}
  }
}

function playMali(mLeft, mTop, mWidth, mHeight, id_item, id_media) {
  mLeft = 150;
  mTop = 93;
  mWidth = 486;
  mHeight = 274;
  player.setArea(mLeft, mTop, mWidth, mHeight);
  var mChMedia = tv.getKanalItem(id_media).mediaId;
  player.playV3(id_media, mChMedia, id_item);
  console.log("CH_ID: " + id_media + " mChMedia: " + mChMedia + " Playbill_id: " + id_item);
  dbgLog("CH_ID: " + id_media + " mChMedia: " + mChMedia + " Playbill_id: " + id_item);
}

function vodTranslate(res) {
  var list = [];

  console.log(res);
  console.log("usao", res.length);
  for (var i = 0; i < res.length; i++) {
    var item = res[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    //console.log(dedicatedServer);
    //console.log(item);

    if (item.hasOwnProperty("posterUrl")) icons = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) ads = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) stills = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) posters = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

    var s = {
      id_channel: item.movieId,
      id_media: "",
      id_item: item.movieId,
      vr_media: "vod",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 500000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "2008 - Akcija | Triler ???",
      "maturity-rating": "16+ ???",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function SeriesTranslate(res) {
  var list = [];

  console.log(res);

  if (res !== undefined) {
    for (var i = 0; i < res.length; i++) {
      var item = res[i];

      var icons = "";
      var ads = "";
      var stills = "";
      var posters = "";
      var backgrounds = "";

      //console.log(dedicatedServer);
      //console.log(item);

      if (item.hasOwnProperty("posterUrl")) icons = dedicatedServer + item.posterUrl;
      if (item.hasOwnProperty("posterUrl")) ads = dedicatedServer + item.posterUrl;
      if (item.hasOwnProperty("posterUrl")) stills = dedicatedServer + item.posterUrl;
      if (item.hasOwnProperty("posterUrl")) posters = dedicatedServer + item.posterUrl;
      if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

      var s = {
        id_channel: item.contentId,
        id_media: "",
        id_item: item.contentId,
        vr_media: "vod",
        name: item.contentName,
        description: "",
        startTime: 0,
        endTime: 500000,
        duration: duration(item.startTime, item.endTime),
        "year-group": "2008 - Akcija | Triler ???",
        "maturity-rating": "16+ ???",
        creators: "",
        genres: "",
        picture: {
          icons: icons,
          ads: ads,
          stills: stills,
          posters: posters,
          backgrounds: backgrounds
        }
      };

      list.push(s);
    }
  }
  return list;
}

function LiveTranslate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.recmContents[0].recmPrograms.length; i++) {
    var item = res.recmContents[0].recmPrograms[i].playbillLites[0];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.picture.hasOwnProperty("icons")) icons = item.picture.icons[0];
    if (item.picture.hasOwnProperty("ads")) ads = item.picture.ads[0];
    if (item.picture.hasOwnProperty("stills")) stills = item.picture.stills[0];
    if (item.picture.hasOwnProperty("posters")) posters = item.picture.posters[0];
    if (item.picture.hasOwnProperty("backgrounds")) backgrounds = item.picture.backgrounds[0];

    var s = {
      id_channel: item.channelID,
      id_media: "",
      id_item: item.ID,
      vr_media: "live",
      name: item.name,
      description: "",
      startTime: item.startTime,
      endTime: item.endTime,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function CatchupTranslate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.recmContents[0].recmPrograms.length; i++) {
    var item = res.recmContents[0].recmPrograms[i].playbillLites[0];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.picture.hasOwnProperty("icons")) icons = item.picture.icons[0];
    if (item.picture.hasOwnProperty("ads")) ads = item.picture.ads[0];
    if (item.picture.hasOwnProperty("stills")) stills = item.picture.stills[0];
    if (item.picture.hasOwnProperty("posters")) posters = item.picture.posters[0];
    if (item.picture.hasOwnProperty("backgrounds")) backgrounds = item.picture.backgrounds[0];

    var s = {
      id_channel: item.channelID,
      id_media: "",
      id_item: item.ID,
      vr_media: "catchup",
      name: item.name,
      description: "",
      startTime: item.startTime,
      endTime: item.endTime,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function PlaybillTranslate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.liveContent.length; i++) {
    var item = res.data.liveContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.picture.hasOwnProperty("icons")) icons = item.picture.icons[0];
    if (item.picture.hasOwnProperty("ads")) ads = item.picture.ads[0];
    if (item.picture.hasOwnProperty("stills")) stills = item.picture.stills[0];
    if (item.picture.hasOwnProperty("posters")) posters = item.picture.posters[0];
    if (item.picture.hasOwnProperty("backgrounds")) backgrounds = item.picture.backgrounds[0];

    var s = {
      id_channel: item.channel.ID,
      id_media: "",
      id_item: item.ID,
      vr_media: "catchup",
      name: item.name,
      description: "",
      startTime: item.startTime,
      endTime: item.endTime,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function Search_VIDEO_VOD_Translate(res) {
  var list = [];

  for (var i = 0; i < res.contents.length; i++) {
    var item = res.contents[i].VOD;

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.picture.hasOwnProperty("icons")) icons = item.picture.icons[0];
    if (item.picture.hasOwnProperty("ads")) ads = item.picture.ads[0];
    if (item.picture.hasOwnProperty("stills")) stills = item.picture.stills[0];
    if (item.picture.hasOwnProperty("posters")) posters = item.picture.posters[0];
    if (item.picture.hasOwnProperty("backgrounds")) backgrounds = item.picture.backgrounds[0];

    var s = {
      id_channel: item.ID,
      id_media: "",
      id_item: item.ID,
      vr_media: "vod",
      name: item.name,
      description: item.introduce,
      startTime: item.startTime,
      endTime: item.endTime,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: item.genres,
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function Search_TVOD_Translate(res) {
  var list = [];

  for (var i = 0; i < res.contents.length; i++) {
    var item = res.contents[i].playbill;

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.picture.hasOwnProperty("icons")) icons = item.picture.icons[0];
    if (item.picture.hasOwnProperty("ads")) ads = item.picture.ads[0];
    if (item.picture.hasOwnProperty("stills")) stills = item.picture.stills[0];
    if (item.picture.hasOwnProperty("posters")) posters = item.picture.posters[0];
    if (item.picture.hasOwnProperty("backgrounds")) backgrounds = item.picture.backgrounds[0];

    var s = {
      id_channel: item.ID,
      id_media: "",
      id_item: item.ID,
      vr_media: "catchup",
      name: item.name,
      description: item.introduce,
      startTime: item.startTime,
      endTime: item.endTime,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: item.genres,
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

/***********************************************************************************/

function mts_najgledanije_u_bioskopu_filmovi_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.movieContent.length; i++) {
    var item = res.data.movieContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("backgroundUrl")) icons = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) ads = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) stills = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) posters = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

    var s = {
      id_channel: item.movieId,
      id_media: "",
      id_item: item.contentId,
      vr_media: "vod",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 10000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_najgledanije_u_bioskopu_serije_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.tvShowContent.length; i++) {
    var item = res.data.tvShowContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("posterUrl")) icons = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) ads = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) stills = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) posters = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) backgrounds = dedicatedServer + item.posterUrl;

    var s = {
      id_channel: item.tvShowId,
      id_media: "",
      id_item: item.contentId,
      vr_media: "vod",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 10000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_trenutno_na_tv_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.liveContent.length; i++) {
    var item = res.data.liveContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("background")) icons = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) ads = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) stills = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) posters = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) backgrounds = dedicatedServer + item.background;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.liveId,
      vr_media: "live",
      name: item.title,
      description: "",
      startTime: conv_epoch(item.start),
      endTime: conv_epoch(item.end),
      duration: duration(conv_epoch(item.start), item.end),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_najgledaniji_kanali_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.liveContent.length; i++) {
    var item = res.data.liveContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.contentLogo.hasOwnProperty("dark")) icons = dedicatedServer + item.contentLogo.dark;
    if (item.contentLogo.hasOwnProperty("dark")) ads = dedicatedServer + item.contentLogo.dark;
    if (item.contentLogo.hasOwnProperty("grayscale")) stills = dedicatedServer + item.contentLogo.grayscale;
    if (item.contentLogo.hasOwnProperty("light")) posters = dedicatedServer + item.contentLogo.light;
    if (item.contentLogo.hasOwnProperty("light")) backgrounds = dedicatedServer + item.contentLogo.light;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.liveId,
      vr_media: "live",
      name: item.contentName,
      description: "",
      startTime: 0,
      endTime: 1000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_najgledanije_u_bioskopu_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.movieContent.length; i++) {
    var item = res.data.movieContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("backgroundUrl")) icons = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) ads = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) stills = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) posters = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.movieId,
      vr_media: "vod",
      name: item.title,
      description: item.description,
      startTime: 0,
      endTime: 1000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_search_program_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.length; i++) {
    var item = res.data[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("background")) icons = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) ads = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) stills = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) posters = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) backgrounds = dedicatedServer + item.background;

    var s = {
      id_channel: item.epgId,
      id_media: "",
      id_item: item.epgScheduleId,
      vr_media: "live",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 1000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_search_kanali_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.length; i++) {
    var item = res.data[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.contentLogo.hasOwnProperty("dark")) icons = dedicatedServer + item.contentLogo.dark;
    if (item.contentLogo.hasOwnProperty("dark")) ads = dedicatedServer + item.contentLogo.dark;
    if (item.contentLogo.hasOwnProperty("grayscale")) stills = dedicatedServer + item.contentLogo.grayscale;
    if (item.contentLogo.hasOwnProperty("light")) posters = dedicatedServer + item.contentLogo.light;
    if (item.contentLogo.hasOwnProperty("light")) backgrounds = dedicatedServer + item.contentLogo.light;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.liveId,
      vr_media: "live",
      name: item.contentName,
      description: "",
      startTime: 0,
      endTime: 1000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_search_filmovi_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.length; i++) {
    var item = res.data[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("backgroundUrl")) icons = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) ads = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) stills = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) posters = dedicatedServer + item.backgroundUrl;
    if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

    var s = {
      id_channel: item.movieId,
      id_media: "",
      id_item: item.contentId,
      vr_media: "vod",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 10000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_search_serije_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.length; i++) {
    var item = res.data[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("posterUrl")) icons = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) ads = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) stills = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) posters = dedicatedServer + item.posterUrl;
    if (item.hasOwnProperty("posterUrl")) backgrounds = dedicatedServer + item.posterUrl;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.contentId,
      vr_media: "vod",
      name: item.title,
      description: "",
      startTime: 0,
      endTime: 10000000,
      duration: duration(item.startTime, item.endTime),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_propusteno_Translate(res) {
  var list = [];

  console.log(res);

  for (var i = 0; i < res.data.liveContent.length; i++) {
    var item = res.data.liveContent[i];

    var icons = "";
    var ads = "";
    var stills = "";
    var posters = "";
    var backgrounds = "";

    if (item.hasOwnProperty("background")) icons = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) ads = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) stills = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) posters = dedicatedServer + item.background;
    if (item.hasOwnProperty("background")) backgrounds = dedicatedServer + item.background;

    var s = {
      id_channel: item.contentId,
      id_media: "",
      id_item: item.scheduleId,
      vr_media: "catchup_show",
      name: item.contentName + " - " + item.title,
      description: "",
      startTime: conv_epoch(item.start),
      endTime: conv_epoch(item.end),
      duration: duration(conv_epoch(item.start), item.end),
      "year-group": "",
      "maturity-rating": "",
      creators: "",
      genres: "",
      picture: {
        icons: icons,
        ads: ads,
        stills: stills,
        posters: posters,
        backgrounds: backgrounds
      }
    };

    list.push(s);
  }

  return list;
}

function mts_episodes_list_Translate(res) {
  var list = [];

  //console.log(res);

  if (res !== undefined) {
    for (var i = 0; i < res.length; i++) {
      var item = res[i];

      var icons = "";
      var ads = "";
      var stills = "";
      var posters = "";
      var backgrounds = "";

      //console.log(dedicatedServer);
      //console.log(item);

      if (item.hasOwnProperty("backgroundUrl")) icons = dedicatedServer + item.backgroundUrl;
      if (item.hasOwnProperty("backgroundUrl")) ads = dedicatedServer + item.backgroundUrl;
      if (item.hasOwnProperty("backgroundUrl")) stills = dedicatedServer + item.backgroundUrl;
      if (item.hasOwnProperty("backgroundUrl")) posters = dedicatedServer + item.backgroundUrl;
      if (item.hasOwnProperty("backgroundUrl")) backgrounds = dedicatedServer + item.backgroundUrl;

      var s = {
        id_channel: item.tvShowId,
        id_media: "",
        id_item: item.tvShowId,
        vr_media: "vod_tvshow",
        name: "EP " + item.episode + " - " + item.title,
        name_sec: item.title,
        episode: item.episode,
        episode_name: "EP " + item.episode,
        season: item.season,
        description: item.description,
        startTime: 0,
        endTime: 500000,
        duration: item.duration,
        "year-group": item.year,
        "maturity-rating": "16+ ???",
        creators: item.actor,
        genres: "",
        picture: {
          icons: icons,
          ads: ads,
          stills: stills,
          posters: posters,
          backgrounds: backgrounds
        }
      };

      list.push(s);
    }
  }
  return list;
}

/***********************************************************************************/
var duration = function (end, start) {
  var diff = parseInt(end) - parseInt(start);
  return formatTimeEpoch(diff);
};

var positionLive = function (start, curent) {
  var diff = parseInt(curent) - parseInt(start);
  return Math.floor(diff / 1000);
};

var conv_epoch = function (date) {
  return Date.parse(date);
};

var getCurentEpoch = function (date) {
  var today = new Date();
  return Date.parse(today);
};

function loaderShow(st) {
  if (st) document.getElementsByClassName("id_loading")[0].style.display = "block";
  else {
    setTimeout(function () {
      document.getElementsByClassName("id_loading")[0].style.display = "none";
    }, 500);
  }
}

function getRowSizeAnimate(size) {
  var card_size = {
    lg: {
      width: 38.28125,
      height: 59.02777777777778
    },
    plg: {
      width: 19.53125,
      height: 59.02777777777778
    },
    md: {
      width: 18.75,
      height: 280
    },
    pmd: {
      width: 11.71875,
      height: 38.88888888888889
    }
  };

  return card_size[size];
}

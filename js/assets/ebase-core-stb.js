var CryptoJS = CryptoJS || function(u, p) {
    var d = {},
        l = d.lib = {},
        s = function() {},
        t = l.Base = {
            extend: function(a) {
                s.prototype = this;
                var c = new s;
                a && c.mixIn(a);
                c.hasOwnProperty("init") || (c.init = function() {
                    c.$super.init.apply(this, arguments)
                });
                c.init.prototype = c;
                c.$super = this;
                return c
            },
            create: function() {
                var a = this.extend();
                a.init.apply(a, arguments);
                return a
            },
            init: function() {},
            mixIn: function(a) {
                for (var c in a) a.hasOwnProperty(c) && (this[c] = a[c]);
                a.hasOwnProperty("toString") && (this.toString = a.toString)
            },
            clone: function() {
                return this.init.prototype.extend(this)
            }
        },
        r = l.WordArray = t.extend({
            init: function(a, c) {
                a = this.words = a || [];
                this.sigBytes = c != p ? c : 4 * a.length
            },
            toString: function(a) {
                return (a || v).stringify(this)
            },
            concat: function(a) {
                var c = this.words,
                    e = a.words,
                    j = this.sigBytes;
                a = a.sigBytes;
                this.clamp();
                if (j % 4)
                    for (var k = 0; k < a; k++) c[j + k >>> 2] |= (e[k >>> 2] >>> 24 - 8 * (k % 4) & 255) << 24 - 8 * ((j + k) % 4);
                else if (65535 < e.length)
                    for (k = 0; k < a; k += 4) c[j + k >>> 2] = e[k >>> 2];
                else c.push.apply(c, e);
                this.sigBytes += a;
                return this
            },
            clamp: function() {
                var a = this.words,
                    c = this.sigBytes;
                a[c >>> 2] &= 4294967295 <<
                    32 - 8 * (c % 4);
                a.length = u.ceil(c / 4)
            },
            clone: function() {
                var a = t.clone.call(this);
                a.words = this.words.slice(0);
                return a
            },
            random: function(a) {
                for (var c = [], e = 0; e < a; e += 4) c.push(4294967296 * u.random() | 0);
                return new r.init(c, a)
            }
        }),
        w = d.enc = {},
        v = w.Hex = {
            stringify: function(a) {
                var c = a.words;
                a = a.sigBytes;
                for (var e = [], j = 0; j < a; j++) {
                    var k = c[j >>> 2] >>> 24 - 8 * (j % 4) & 255;
                    e.push((k >>> 4).toString(16));
                    e.push((k & 15).toString(16))
                }
                return e.join("")
            },
            parse: function(a) {
                for (var c = a.length, e = [], j = 0; j < c; j += 2) e[j >>> 3] |= parseInt(a.substr(j,
                    2), 16) << 24 - 4 * (j % 8);
                return new r.init(e, c / 2)
            }
        },
        b = w.Latin1 = {
            stringify: function(a) {
                var c = a.words;
                a = a.sigBytes;
                for (var e = [], j = 0; j < a; j++) e.push(String.fromCharCode(c[j >>> 2] >>> 24 - 8 * (j % 4) & 255));
                return e.join("")
            },
            parse: function(a) {
                for (var c = a.length, e = [], j = 0; j < c; j++) e[j >>> 2] |= (a.charCodeAt(j) & 255) << 24 - 8 * (j % 4);
                return new r.init(e, c)
            }
        },
        x = w.Utf8 = {
            stringify: function(a) {
                try {
                    return decodeURIComponent(escape(b.stringify(a)))
                } catch (c) {
                    throw Error("Malformed UTF-8 data");
                }
            },
            parse: function(a) {
                return b.parse(unescape(encodeURIComponent(a)))
            }
        },
        q = l.BufferedBlockAlgorithm = t.extend({
            reset: function() {
                this._data = new r.init;
                this._nDataBytes = 0
            },
            _append: function(a) {
                "string" == typeof a && (a = x.parse(a));
                this._data.concat(a);
                this._nDataBytes += a.sigBytes
            },
            _process: function(a) {
                var c = this._data,
                    e = c.words,
                    j = c.sigBytes,
                    k = this.blockSize,
                    b = j / (4 * k),
                    b = a ? u.ceil(b) : u.max((b | 0) - this._minBufferSize, 0);
                a = b * k;
                j = u.min(4 * a, j);
                if (a) {
                    for (var q = 0; q < a; q += k) this._doProcessBlock(e, q);
                    q = e.splice(0, a);
                    c.sigBytes -= j
                }
                return new r.init(q, j)
            },
            clone: function() {
                var a = t.clone.call(this);
                a._data = this._data.clone();
                return a
            },
            _minBufferSize: 0
        });
    l.Hasher = q.extend({
        cfg: t.extend(),
        init: function(a) {
            this.cfg = this.cfg.extend(a);
            this.reset()
        },
        reset: function() {
            q.reset.call(this);
            this._doReset()
        },
        update: function(a) {
            this._append(a);
            this._process();
            return this
        },
        finalize: function(a) {
            a && this._append(a);
            return this._doFinalize()
        },
        blockSize: 16,
        _createHelper: function(a) {
            return function(b, e) {
                return (new a.init(e)).finalize(b)
            }
        },
        _createHmacHelper: function(a) {
            return function(b, e) {
                return (new n.HMAC.init(a,
                    e)).finalize(b)
            }
        }
    });
    var n = d.algo = {};
    return d
}(Math);
(function() {
    var u = CryptoJS,
        p = u.lib.WordArray;
    u.enc.Base64 = {
        stringify: function(d) {
            var l = d.words,
                p = d.sigBytes,
                t = this._map;
            d.clamp();
            d = [];
            for (var r = 0; r < p; r += 3)
                for (var w = (l[r >>> 2] >>> 24 - 8 * (r % 4) & 255) << 16 | (l[r + 1 >>> 2] >>> 24 - 8 * ((r + 1) % 4) & 255) << 8 | l[r + 2 >>> 2] >>> 24 - 8 * ((r + 2) % 4) & 255, v = 0; 4 > v && r + 0.75 * v < p; v++) d.push(t.charAt(w >>> 6 * (3 - v) & 63));
            if (l = t.charAt(64))
                for (; d.length % 4;) d.push(l);
            return d.join("")
        },
        parse: function(d) {
            var l = d.length,
                s = this._map,
                t = s.charAt(64);
            t && (t = d.indexOf(t), -1 != t && (l = t));
            for (var t = [], r = 0, w = 0; w <
                l; w++)
                if (w % 4) {
                    var v = s.indexOf(d.charAt(w - 1)) << 2 * (w % 4),
                        b = s.indexOf(d.charAt(w)) >>> 6 - 2 * (w % 4);
                    t[r >>> 2] |= (v | b) << 24 - 8 * (r % 4);
                    r++
                }
            return p.create(t, r)
        },
        _map: "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/="
    }
})();
(function(u) {
    function p(b, n, a, c, e, j, k) {
        b = b + (n & a | ~n & c) + e + k;
        return (b << j | b >>> 32 - j) + n
    }

    function d(b, n, a, c, e, j, k) {
        b = b + (n & c | a & ~c) + e + k;
        return (b << j | b >>> 32 - j) + n
    }

    function l(b, n, a, c, e, j, k) {
        b = b + (n ^ a ^ c) + e + k;
        return (b << j | b >>> 32 - j) + n
    }

    function s(b, n, a, c, e, j, k) {
        b = b + (a ^ (n | ~c)) + e + k;
        return (b << j | b >>> 32 - j) + n
    }
    for (var t = CryptoJS, r = t.lib, w = r.WordArray, v = r.Hasher, r = t.algo, b = [], x = 0; 64 > x; x++) b[x] = 4294967296 * u.abs(u.sin(x + 1)) | 0;
    r = r.MD5 = v.extend({
        _doReset: function() {
            this._hash = new w.init([1732584193, 4023233417, 2562383102, 271733878])
        },
        _doProcessBlock: function(q, n) {
            for (var a = 0; 16 > a; a++) {
                var c = n + a,
                    e = q[c];
                q[c] = (e << 8 | e >>> 24) & 16711935 | (e << 24 | e >>> 8) & 4278255360
            }
            var a = this._hash.words,
                c = q[n + 0],
                e = q[n + 1],
                j = q[n + 2],
                k = q[n + 3],
                z = q[n + 4],
                r = q[n + 5],
                t = q[n + 6],
                w = q[n + 7],
                v = q[n + 8],
                A = q[n + 9],
                B = q[n + 10],
                C = q[n + 11],
                u = q[n + 12],
                D = q[n + 13],
                E = q[n + 14],
                x = q[n + 15],
                f = a[0],
                m = a[1],
                g = a[2],
                h = a[3],
                f = p(f, m, g, h, c, 7, b[0]),
                h = p(h, f, m, g, e, 12, b[1]),
                g = p(g, h, f, m, j, 17, b[2]),
                m = p(m, g, h, f, k, 22, b[3]),
                f = p(f, m, g, h, z, 7, b[4]),
                h = p(h, f, m, g, r, 12, b[5]),
                g = p(g, h, f, m, t, 17, b[6]),
                m = p(m, g, h, f, w, 22, b[7]),
                f = p(f, m, g, h, v, 7, b[8]),
                h = p(h, f, m, g, A, 12, b[9]),
                g = p(g, h, f, m, B, 17, b[10]),
                m = p(m, g, h, f, C, 22, b[11]),
                f = p(f, m, g, h, u, 7, b[12]),
                h = p(h, f, m, g, D, 12, b[13]),
                g = p(g, h, f, m, E, 17, b[14]),
                m = p(m, g, h, f, x, 22, b[15]),
                f = d(f, m, g, h, e, 5, b[16]),
                h = d(h, f, m, g, t, 9, b[17]),
                g = d(g, h, f, m, C, 14, b[18]),
                m = d(m, g, h, f, c, 20, b[19]),
                f = d(f, m, g, h, r, 5, b[20]),
                h = d(h, f, m, g, B, 9, b[21]),
                g = d(g, h, f, m, x, 14, b[22]),
                m = d(m, g, h, f, z, 20, b[23]),
                f = d(f, m, g, h, A, 5, b[24]),
                h = d(h, f, m, g, E, 9, b[25]),
                g = d(g, h, f, m, k, 14, b[26]),
                m = d(m, g, h, f, v, 20, b[27]),
                f = d(f, m, g, h, D, 5, b[28]),
                h = d(h, f,
                    m, g, j, 9, b[29]),
                g = d(g, h, f, m, w, 14, b[30]),
                m = d(m, g, h, f, u, 20, b[31]),
                f = l(f, m, g, h, r, 4, b[32]),
                h = l(h, f, m, g, v, 11, b[33]),
                g = l(g, h, f, m, C, 16, b[34]),
                m = l(m, g, h, f, E, 23, b[35]),
                f = l(f, m, g, h, e, 4, b[36]),
                h = l(h, f, m, g, z, 11, b[37]),
                g = l(g, h, f, m, w, 16, b[38]),
                m = l(m, g, h, f, B, 23, b[39]),
                f = l(f, m, g, h, D, 4, b[40]),
                h = l(h, f, m, g, c, 11, b[41]),
                g = l(g, h, f, m, k, 16, b[42]),
                m = l(m, g, h, f, t, 23, b[43]),
                f = l(f, m, g, h, A, 4, b[44]),
                h = l(h, f, m, g, u, 11, b[45]),
                g = l(g, h, f, m, x, 16, b[46]),
                m = l(m, g, h, f, j, 23, b[47]),
                f = s(f, m, g, h, c, 6, b[48]),
                h = s(h, f, m, g, w, 10, b[49]),
                g = s(g, h, f, m,
                    E, 15, b[50]),
                m = s(m, g, h, f, r, 21, b[51]),
                f = s(f, m, g, h, u, 6, b[52]),
                h = s(h, f, m, g, k, 10, b[53]),
                g = s(g, h, f, m, B, 15, b[54]),
                m = s(m, g, h, f, e, 21, b[55]),
                f = s(f, m, g, h, v, 6, b[56]),
                h = s(h, f, m, g, x, 10, b[57]),
                g = s(g, h, f, m, t, 15, b[58]),
                m = s(m, g, h, f, D, 21, b[59]),
                f = s(f, m, g, h, z, 6, b[60]),
                h = s(h, f, m, g, C, 10, b[61]),
                g = s(g, h, f, m, j, 15, b[62]),
                m = s(m, g, h, f, A, 21, b[63]);
            a[0] = a[0] + f | 0;
            a[1] = a[1] + m | 0;
            a[2] = a[2] + g | 0;
            a[3] = a[3] + h | 0
        },
        _doFinalize: function() {
            var b = this._data,
                n = b.words,
                a = 8 * this._nDataBytes,
                c = 8 * b.sigBytes;
            n[c >>> 5] |= 128 << 24 - c % 32;
            var e = u.floor(a /
                4294967296);
            n[(c + 64 >>> 9 << 4) + 15] = (e << 8 | e >>> 24) & 16711935 | (e << 24 | e >>> 8) & 4278255360;
            n[(c + 64 >>> 9 << 4) + 14] = (a << 8 | a >>> 24) & 16711935 | (a << 24 | a >>> 8) & 4278255360;
            b.sigBytes = 4 * (n.length + 1);
            this._process();
            b = this._hash;
            n = b.words;
            for (a = 0; 4 > a; a++) c = n[a], n[a] = (c << 8 | c >>> 24) & 16711935 | (c << 24 | c >>> 8) & 4278255360;
            return b
        },
        clone: function() {
            var b = v.clone.call(this);
            b._hash = this._hash.clone();
            return b
        }
    });
    t.MD5 = v._createHelper(r);
    t.HmacMD5 = v._createHmacHelper(r)
})(Math);
(function() {
    var u = CryptoJS,
        p = u.lib,
        d = p.Base,
        l = p.WordArray,
        p = u.algo,
        s = p.EvpKDF = d.extend({
            cfg: d.extend({
                keySize: 4,
                hasher: p.MD5,
                iterations: 1
            }),
            init: function(d) {
                this.cfg = this.cfg.extend(d)
            },
            compute: function(d, r) {
                for (var p = this.cfg, s = p.hasher.create(), b = l.create(), u = b.words, q = p.keySize, p = p.iterations; u.length < q;) {
                    n && s.update(n);
                    var n = s.update(d).finalize(r);
                    s.reset();
                    for (var a = 1; a < p; a++) n = s.finalize(n), s.reset();
                    b.concat(n)
                }
                b.sigBytes = 4 * q;
                return b
            }
        });
    u.EvpKDF = function(d, l, p) {
        return s.create(p).compute(d,
            l)
    }
})();
CryptoJS.lib.Cipher || function(u) {
    var p = CryptoJS,
        d = p.lib,
        l = d.Base,
        s = d.WordArray,
        t = d.BufferedBlockAlgorithm,
        r = p.enc.Base64,
        w = p.algo.EvpKDF,
        v = d.Cipher = t.extend({
            cfg: l.extend(),
            createEncryptor: function(e, a) {
                return this.create(this._ENC_XFORM_MODE, e, a)
            },
            createDecryptor: function(e, a) {
                return this.create(this._DEC_XFORM_MODE, e, a)
            },
            init: function(e, a, b) {
                this.cfg = this.cfg.extend(b);
                this._xformMode = e;
                this._key = a;
                this.reset()
            },
            reset: function() {
                t.reset.call(this);
                this._doReset()
            },
            process: function(e) {
                this._append(e);
                return this._process()
            },
            finalize: function(e) {
                e && this._append(e);
                return this._doFinalize()
            },
            keySize: 4,
            ivSize: 4,
            _ENC_XFORM_MODE: 1,
            _DEC_XFORM_MODE: 2,
            _createHelper: function(e) {
                return {
                    encrypt: function(b, k, d) {
                        return ("string" == typeof k ? c : a).encrypt(e, b, k, d)
                    },
                    decrypt: function(b, k, d) {
                        return ("string" == typeof k ? c : a).decrypt(e, b, k, d)
                    }
                }
            }
        });
    d.StreamCipher = v.extend({
        _doFinalize: function() {
            return this._process(!0)
        },
        blockSize: 1
    });
    var b = p.mode = {},
        x = function(e, a, b) {
            var c = this._iv;
            c ? this._iv = u : c = this._prevBlock;
            for (var d = 0; d < b; d++) e[a + d] ^=
                c[d]
        },
        q = (d.BlockCipherMode = l.extend({
            createEncryptor: function(e, a) {
                return this.Encryptor.create(e, a)
            },
            createDecryptor: function(e, a) {
                return this.Decryptor.create(e, a)
            },
            init: function(e, a) {
                this._cipher = e;
                this._iv = a
            }
        })).extend();
    q.Encryptor = q.extend({
        processBlock: function(e, a) {
            var b = this._cipher,
                c = b.blockSize;
            x.call(this, e, a, c);
            b.encryptBlock(e, a);
            this._prevBlock = e.slice(a, a + c)
        }
    });
    q.Decryptor = q.extend({
        processBlock: function(e, a) {
            var b = this._cipher,
                c = b.blockSize,
                d = e.slice(a, a + c);
            b.decryptBlock(e, a);
            x.call(this,
                e, a, c);
            this._prevBlock = d
        }
    });
    b = b.CBC = q;
    q = (p.pad = {}).Pkcs7 = {
        pad: function(a, b) {
            for (var c = 4 * b, c = c - a.sigBytes % c, d = c << 24 | c << 16 | c << 8 | c, l = [], n = 0; n < c; n += 4) l.push(d);
            c = s.create(l, c);
            a.concat(c)
        },
        unpad: function(a) {
            a.sigBytes -= a.words[a.sigBytes - 1 >>> 2] & 255
        }
    };
    d.BlockCipher = v.extend({
        cfg: v.cfg.extend({
            mode: b,
            padding: q
        }),
        reset: function() {
            v.reset.call(this);
            var a = this.cfg,
                b = a.iv,
                a = a.mode;
            if (this._xformMode == this._ENC_XFORM_MODE) var c = a.createEncryptor;
            else c = a.createDecryptor, this._minBufferSize = 1;
            this._mode = c.call(a,
                this, b && b.words)
        },
        _doProcessBlock: function(a, b) {
            this._mode.processBlock(a, b)
        },
        _doFinalize: function() {
            var a = this.cfg.padding;
            if (this._xformMode == this._ENC_XFORM_MODE) {
                a.pad(this._data, this.blockSize);
                var b = this._process(!0)
            } else b = this._process(!0), a.unpad(b);
            return b
        },
        blockSize: 4
    });
    var n = d.CipherParams = l.extend({
            init: function(a) {
                this.mixIn(a)
            },
            toString: function(a) {
                return (a || this.formatter).stringify(this)
            }
        }),
        b = (p.format = {}).OpenSSL = {
            stringify: function(a) {
                var b = a.ciphertext;
                a = a.salt;
                return (a ? s.create([1398893684,
                    1701076831
                ]).concat(a).concat(b) : b).toString(r)
            },
            parse: function(a) {
                a = r.parse(a);
                var b = a.words;
                if (1398893684 == b[0] && 1701076831 == b[1]) {
                    var c = s.create(b.slice(2, 4));
                    b.splice(0, 4);
                    a.sigBytes -= 16
                }
                return n.create({
                    ciphertext: a,
                    salt: c
                })
            }
        },
        a = d.SerializableCipher = l.extend({
            cfg: l.extend({
                format: b
            }),
            encrypt: function(a, b, c, d) {
                d = this.cfg.extend(d);
                var l = a.createEncryptor(c, d);
                b = l.finalize(b);
                l = l.cfg;
                return n.create({
                    ciphertext: b,
                    key: c,
                    iv: l.iv,
                    algorithm: a,
                    mode: l.mode,
                    padding: l.padding,
                    blockSize: a.blockSize,
                    formatter: d.format
                })
            },
            decrypt: function(a, b, c, d) {
                d = this.cfg.extend(d);
                b = this._parse(b, d.format);
                return a.createDecryptor(c, d).finalize(b.ciphertext)
            },
            _parse: function(a, b) {
                return "string" == typeof a ? b.parse(a, this) : a
            }
        }),
        p = (p.kdf = {}).OpenSSL = {
            execute: function(a, b, c, d) {
                d || (d = s.random(8));
                a = w.create({
                    keySize: b + c
                }).compute(a, d);
                c = s.create(a.words.slice(b), 4 * c);
                a.sigBytes = 4 * b;
                return n.create({
                    key: a,
                    iv: c,
                    salt: d
                })
            }
        },
        c = d.PasswordBasedCipher = a.extend({
            cfg: a.cfg.extend({
                kdf: p
            }),
            encrypt: function(b, c, d, l) {
                l = this.cfg.extend(l);
                d = l.kdf.execute(d,
                    b.keySize, b.ivSize);
                l.iv = d.iv;
                b = a.encrypt.call(this, b, c, d.key, l);
                b.mixIn(d);
                return b
            },
            decrypt: function(b, c, d, l) {
                l = this.cfg.extend(l);
                c = this._parse(c, l.format);
                d = l.kdf.execute(d, b.keySize, b.ivSize, c.salt);
                l.iv = d.iv;
                return a.decrypt.call(this, b, c, d.key, l)
            }
        })
}();
(function() {
    for (var u = CryptoJS, p = u.lib.BlockCipher, d = u.algo, l = [], s = [], t = [], r = [], w = [], v = [], b = [], x = [], q = [], n = [], a = [], c = 0; 256 > c; c++) a[c] = 128 > c ? c << 1 : c << 1 ^ 283;
    for (var e = 0, j = 0, c = 0; 256 > c; c++) {
        var k = j ^ j << 1 ^ j << 2 ^ j << 3 ^ j << 4,
            k = k >>> 8 ^ k & 255 ^ 99;
        l[e] = k;
        s[k] = e;
        var z = a[e],
            F = a[z],
            G = a[F],
            y = 257 * a[k] ^ 16843008 * k;
        t[e] = y << 24 | y >>> 8;
        r[e] = y << 16 | y >>> 16;
        w[e] = y << 8 | y >>> 24;
        v[e] = y;
        y = 16843009 * G ^ 65537 * F ^ 257 * z ^ 16843008 * e;
        b[k] = y << 24 | y >>> 8;
        x[k] = y << 16 | y >>> 16;
        q[k] = y << 8 | y >>> 24;
        n[k] = y;
        e ? (e = z ^ a[a[a[G ^ z]]], j ^= a[a[j]]) : e = j = 1
    }
    var H = [0, 1, 2, 4, 8,
            16, 32, 64, 128, 27, 54
        ],
        d = d.AES = p.extend({
            _doReset: function() {
                for (var a = this._key, c = a.words, d = a.sigBytes / 4, a = 4 * ((this._nRounds = d + 6) + 1), e = this._keySchedule = [], j = 0; j < a; j++)
                    if (j < d) e[j] = c[j];
                    else {
                        var k = e[j - 1];
                        j % d ? 6 < d && 4 == j % d && (k = l[k >>> 24] << 24 | l[k >>> 16 & 255] << 16 | l[k >>> 8 & 255] << 8 | l[k & 255]) : (k = k << 8 | k >>> 24, k = l[k >>> 24] << 24 | l[k >>> 16 & 255] << 16 | l[k >>> 8 & 255] << 8 | l[k & 255], k ^= H[j / d | 0] << 24);
                        e[j] = e[j - d] ^ k
                    }
                c = this._invKeySchedule = [];
                for (d = 0; d < a; d++) j = a - d, k = d % 4 ? e[j] : e[j - 4], c[d] = 4 > d || 4 >= j ? k : b[l[k >>> 24]] ^ x[l[k >>> 16 & 255]] ^ q[l[k >>>
                    8 & 255]] ^ n[l[k & 255]]
            },
            encryptBlock: function(a, b) {
                this._doCryptBlock(a, b, this._keySchedule, t, r, w, v, l)
            },
            decryptBlock: function(a, c) {
                var d = a[c + 1];
                a[c + 1] = a[c + 3];
                a[c + 3] = d;
                this._doCryptBlock(a, c, this._invKeySchedule, b, x, q, n, s);
                d = a[c + 1];
                a[c + 1] = a[c + 3];
                a[c + 3] = d
            },
            _doCryptBlock: function(a, b, c, d, e, j, l, f) {
                for (var m = this._nRounds, g = a[b] ^ c[0], h = a[b + 1] ^ c[1], k = a[b + 2] ^ c[2], n = a[b + 3] ^ c[3], p = 4, r = 1; r < m; r++) var q = d[g >>> 24] ^ e[h >>> 16 & 255] ^ j[k >>> 8 & 255] ^ l[n & 255] ^ c[p++],
                    s = d[h >>> 24] ^ e[k >>> 16 & 255] ^ j[n >>> 8 & 255] ^ l[g & 255] ^ c[p++],
                    t =
                    d[k >>> 24] ^ e[n >>> 16 & 255] ^ j[g >>> 8 & 255] ^ l[h & 255] ^ c[p++],
                    n = d[n >>> 24] ^ e[g >>> 16 & 255] ^ j[h >>> 8 & 255] ^ l[k & 255] ^ c[p++],
                    g = q,
                    h = s,
                    k = t;
                q = (f[g >>> 24] << 24 | f[h >>> 16 & 255] << 16 | f[k >>> 8 & 255] << 8 | f[n & 255]) ^ c[p++];
                s = (f[h >>> 24] << 24 | f[k >>> 16 & 255] << 16 | f[n >>> 8 & 255] << 8 | f[g & 255]) ^ c[p++];
                t = (f[k >>> 24] << 24 | f[n >>> 16 & 255] << 16 | f[g >>> 8 & 255] << 8 | f[h & 255]) ^ c[p++];
                n = (f[n >>> 24] << 24 | f[g >>> 16 & 255] << 16 | f[h >>> 8 & 255] << 8 | f[k & 255]) ^ c[p++];
                a[b] = q;
                a[b + 1] = s;
                a[b + 2] = t;
                a[b + 3] = n
            },
            keySize: 8
        });
    u.AES = p._createHelper(d)
})();
/*
CryptoJS v3.1.2
code.google.com/p/crypto-js
(c) 2009-2013 by Jeff Mott. All rights reserved.
code.google.com/p/crypto-js/wiki/License
*/
var CryptoJS = CryptoJS || function(h, s) {
    var f = {},
        t = f.lib = {},
        g = function() {},
        j = t.Base = {
            extend: function(a) {
                g.prototype = this;
                var c = new g;
                a && c.mixIn(a);
                c.hasOwnProperty("init") || (c.init = function() {
                    c.$super.init.apply(this, arguments)
                });
                c.init.prototype = c;
                c.$super = this;
                return c
            },
            create: function() {
                var a = this.extend();
                a.init.apply(a, arguments);
                return a
            },
            init: function() {},
            mixIn: function(a) {
                for (var c in a) a.hasOwnProperty(c) && (this[c] = a[c]);
                a.hasOwnProperty("toString") && (this.toString = a.toString)
            },
            clone: function() {
                return this.init.prototype.extend(this)
            }
        },
        q = t.WordArray = j.extend({
            init: function(a, c) {
                a = this.words = a || [];
                this.sigBytes = c != s ? c : 4 * a.length
            },
            toString: function(a) {
                return (a || u).stringify(this)
            },
            concat: function(a) {
                var c = this.words,
                    d = a.words,
                    b = this.sigBytes;
                a = a.sigBytes;
                this.clamp();
                if (b % 4)
                    for (var e = 0; e < a; e++) c[b + e >>> 2] |= (d[e >>> 2] >>> 24 - 8 * (e % 4) & 255) << 24 - 8 * ((b + e) % 4);
                else if (65535 < d.length)
                    for (e = 0; e < a; e += 4) c[b + e >>> 2] = d[e >>> 2];
                else c.push.apply(c, d);
                this.sigBytes += a;
                return this
            },
            clamp: function() {
                var a = this.words,
                    c = this.sigBytes;
                a[c >>> 2] &= 4294967295 <<
                    32 - 8 * (c % 4);
                a.length = h.ceil(c / 4)
            },
            clone: function() {
                var a = j.clone.call(this);
                a.words = this.words.slice(0);
                return a
            },
            random: function(a) {
                for (var c = [], d = 0; d < a; d += 4) c.push(4294967296 * h.random() | 0);
                return new q.init(c, a)
            }
        }),
        v = f.enc = {},
        u = v.Hex = {
            stringify: function(a) {
                var c = a.words;
                a = a.sigBytes;
                for (var d = [], b = 0; b < a; b++) {
                    var e = c[b >>> 2] >>> 24 - 8 * (b % 4) & 255;
                    d.push((e >>> 4).toString(16));
                    d.push((e & 15).toString(16))
                }
                return d.join("")
            },
            parse: function(a) {
                for (var c = a.length, d = [], b = 0; b < c; b += 2) d[b >>> 3] |= parseInt(a.substr(b,
                    2), 16) << 24 - 4 * (b % 8);
                return new q.init(d, c / 2)
            }
        },
        k = v.Latin1 = {
            stringify: function(a) {
                var c = a.words;
                a = a.sigBytes;
                for (var d = [], b = 0; b < a; b++) d.push(String.fromCharCode(c[b >>> 2] >>> 24 - 8 * (b % 4) & 255));
                return d.join("")
            },
            parse: function(a) {
                for (var c = a.length, d = [], b = 0; b < c; b++) d[b >>> 2] |= (a.charCodeAt(b) & 255) << 24 - 8 * (b % 4);
                return new q.init(d, c)
            }
        },
        l = v.Utf8 = {
            stringify: function(a) {
                try {
                    return decodeURIComponent(escape(k.stringify(a)))
                } catch (c) {
                    throw Error("Malformed UTF-8 data");
                }
            },
            parse: function(a) {
                return k.parse(unescape(encodeURIComponent(a)))
            }
        },
        x = t.BufferedBlockAlgorithm = j.extend({
            reset: function() {
                this._data = new q.init;
                this._nDataBytes = 0
            },
            _append: function(a) {
                "string" == typeof a && (a = l.parse(a));
                this._data.concat(a);
                this._nDataBytes += a.sigBytes
            },
            _process: function(a) {
                var c = this._data,
                    d = c.words,
                    b = c.sigBytes,
                    e = this.blockSize,
                    f = b / (4 * e),
                    f = a ? h.ceil(f) : h.max((f | 0) - this._minBufferSize, 0);
                a = f * e;
                b = h.min(4 * a, b);
                if (a) {
                    for (var m = 0; m < a; m += e) this._doProcessBlock(d, m);
                    m = d.splice(0, a);
                    c.sigBytes -= b
                }
                return new q.init(m, b)
            },
            clone: function() {
                var a = j.clone.call(this);
                a._data = this._data.clone();
                return a
            },
            _minBufferSize: 0
        });
    t.Hasher = x.extend({
        cfg: j.extend(),
        init: function(a) {
            this.cfg = this.cfg.extend(a);
            this.reset()
        },
        reset: function() {
            x.reset.call(this);
            this._doReset()
        },
        update: function(a) {
            this._append(a);
            this._process();
            return this
        },
        finalize: function(a) {
            a && this._append(a);
            return this._doFinalize()
        },
        blockSize: 16,
        _createHelper: function(a) {
            return function(c, d) {
                return (new a.init(d)).finalize(c)
            }
        },
        _createHmacHelper: function(a) {
            return function(c, d) {
                return (new w.HMAC.init(a,
                    d)).finalize(c)
            }
        }
    });
    var w = f.algo = {};
    return f
}(Math);
(function(h) {
    for (var s = CryptoJS, f = s.lib, t = f.WordArray, g = f.Hasher, f = s.algo, j = [], q = [], v = function(a) {
            return 4294967296 * (a - (a | 0)) | 0
        }, u = 2, k = 0; 64 > k;) {
        var l;
        a: {
            l = u;
            for (var x = h.sqrt(l), w = 2; w <= x; w++)
                if (!(l % w)) {
                    l = !1;
                    break a
                }
            l = !0
        }
        l && (8 > k && (j[k] = v(h.pow(u, 0.5))), q[k] = v(h.pow(u, 1 / 3)), k++);
        u++
    }
    var a = [],
        f = f.SHA256 = g.extend({
            _doReset: function() {
                this._hash = new t.init(j.slice(0))
            },
            _doProcessBlock: function(c, d) {
                for (var b = this._hash.words, e = b[0], f = b[1], m = b[2], h = b[3], p = b[4], j = b[5], k = b[6], l = b[7], n = 0; 64 > n; n++) {
                    if (16 > n) a[n] =
                        c[d + n] | 0;
                    else {
                        var r = a[n - 15],
                            g = a[n - 2];
                        a[n] = ((r << 25 | r >>> 7) ^ (r << 14 | r >>> 18) ^ r >>> 3) + a[n - 7] + ((g << 15 | g >>> 17) ^ (g << 13 | g >>> 19) ^ g >>> 10) + a[n - 16]
                    }
                    r = l + ((p << 26 | p >>> 6) ^ (p << 21 | p >>> 11) ^ (p << 7 | p >>> 25)) + (p & j ^ ~p & k) + q[n] + a[n];
                    g = ((e << 30 | e >>> 2) ^ (e << 19 | e >>> 13) ^ (e << 10 | e >>> 22)) + (e & f ^ e & m ^ f & m);
                    l = k;
                    k = j;
                    j = p;
                    p = h + r | 0;
                    h = m;
                    m = f;
                    f = e;
                    e = r + g | 0
                }
                b[0] = b[0] + e | 0;
                b[1] = b[1] + f | 0;
                b[2] = b[2] + m | 0;
                b[3] = b[3] + h | 0;
                b[4] = b[4] + p | 0;
                b[5] = b[5] + j | 0;
                b[6] = b[6] + k | 0;
                b[7] = b[7] + l | 0
            },
            _doFinalize: function() {
                var a = this._data,
                    d = a.words,
                    b = 8 * this._nDataBytes,
                    e = 8 * a.sigBytes;
                d[e >>> 5] |= 128 << 24 - e % 32;
                d[(e + 64 >>> 9 << 4) + 14] = h.floor(b / 4294967296);
                d[(e + 64 >>> 9 << 4) + 15] = b;
                a.sigBytes = 4 * d.length;
                this._process();
                return this._hash
            },
            clone: function() {
                var a = g.clone.call(this);
                a._hash = this._hash.clone();
                return a
            }
        });
    s.SHA256 = g._createHelper(f);
    s.HmacSHA256 = g._createHmacHelper(f)
})(Math);
CryptoJS.mode.ECB = function() {
    var a = CryptoJS.lib.BlockCipherMode.extend();
    a.Encryptor = a.extend({
        processBlock: function(a, b) {
            this._cipher.encryptBlock(a, b)
        }
    });
    a.Decryptor = a.extend({
        processBlock: function(a, b) {
            this._cipher.decryptBlock(a, b)
        }
    });
    return a
}();

function des(a, b, c, d, e, f) {
    var g, h, i, j, k, l, m, n, o, p, q, r, s, t, u = new Array(16843776, 0, 65536, 16843780, 16842756, 66564, 4, 65536, 1024, 16843776, 16843780, 1024, 16778244, 16842756, 16777216, 4, 1028, 16778240, 16778240, 66560, 66560, 16842752, 16842752, 16778244, 65540, 16777220, 16777220, 65540, 0, 1028, 66564, 16777216, 65536, 16843780, 4, 16842752, 16843776, 16777216, 16777216, 1024, 16842756, 65536, 66560, 16777220, 1024, 4, 16778244, 66564, 16843780, 65540, 16842752, 16778244, 16777220, 1028, 66564, 16843776, 1028, 16778240, 16778240, 0, 65540, 66560, 0, 16842756),
        v = new Array(-2146402272, -2147450880, 32768, 1081376, 1048576, 32, -2146435040, -2147450848, -2147483616, -2146402272, -2146402304, -2147483648, -2147450880, 1048576, 32, -2146435040, 1081344, 1048608, -2147450848, 0, -2147483648, 32768, 1081376, -2146435072, 1048608, -2147483616, 0, 1081344, 32800, -2146402304, -2146435072, 32800, 0, 1081376, -2146435040, 1048576, -2147450848, -2146435072, -2146402304, 32768, -2146435072, -2147450880, 32, -2146402272, 1081376, 32, 32768, -2147483648, 32800, -2146402304, 1048576, -2147483616, 1048608, -2147450848, -2147483616, 1048608, 1081344, 0, -2147450880, 32800, -2147483648, -2146435040, -2146402272, 1081344),
        w = new Array(520, 134349312, 0, 134348808, 134218240, 0, 131592, 134218240, 131080, 134217736, 134217736, 131072, 134349320, 131080, 134348800, 520, 134217728, 8, 134349312, 512, 131584, 134348800, 134348808, 131592, 134218248, 131584, 131072, 134218248, 8, 134349320, 512, 134217728, 134349312, 134217728, 131080, 520, 131072, 134349312, 134218240, 0, 512, 131080, 134349320, 134218240, 134217736, 512, 0, 134348808, 134218248, 131072, 134217728, 134349320, 8, 131592, 131584, 134217736, 134348800, 134218248, 520, 134348800, 131592, 8, 134348808, 131584),
        x = new Array(8396801, 8321, 8321, 128, 8396928, 8388737, 8388609, 8193, 0, 8396800, 8396800, 8396929, 129, 0, 8388736, 8388609, 1, 8192, 8388608, 8396801, 128, 8388608, 8193, 8320, 8388737, 1, 8320, 8388736, 8192, 8396928, 8396929, 129, 8388736, 8388609, 8396800, 8396929, 129, 0, 0, 8396800, 8320, 8388736, 8388737, 1, 8396801, 8321, 8321, 128, 8396929, 129, 1, 8192, 8388609, 8193, 8396928, 8388737, 8193, 8320, 8388608, 8396801, 128, 8388608, 8192, 8396928),
        y = new Array(256, 34078976, 34078720, 1107296512, 524288, 256, 1073741824, 34078720, 1074266368, 524288, 33554688, 1074266368, 1107296512, 1107820544, 524544, 1073741824, 33554432, 1074266112, 1074266112, 0, 1073742080, 1107820800, 1107820800, 33554688, 1107820544, 1073742080, 0, 1107296256, 34078976, 33554432, 1107296256, 524544, 524288, 1107296512, 256, 33554432, 1073741824, 34078720, 1107296512, 1074266368, 33554688, 1073741824, 1107820544, 34078976, 1074266368, 256, 33554432, 1107820544, 1107820800, 524544, 1107296256, 1107820800, 34078720, 0, 1074266112, 1107296256, 524544, 33554688, 1073742080, 524288, 0, 1074266112, 34078976, 1073742080),
        z = new Array(536870928, 541065216, 16384, 541081616, 541065216, 16, 541081616, 4194304, 536887296, 4210704, 4194304, 536870928, 4194320, 536887296, 536870912, 16400, 0, 4194320, 536887312, 16384, 4210688, 536887312, 16, 541065232, 541065232, 0, 4210704, 541081600, 16400, 4210688, 541081600, 536870912, 536887296, 16, 541065232, 4210688, 541081616, 4194304, 16400, 536870928, 4194304, 536887296, 536870912, 16400, 536870928, 541081616, 4210688, 541065216, 4210704, 541081600, 0, 541065232, 16, 16384, 541065216, 4210704, 16384, 4194320, 536887312, 0, 541081600, 536870912, 4194320, 536887312),
        A = new Array(2097152, 69206018, 67110914, 0, 2048, 67110914, 2099202, 69208064, 69208066, 2097152, 0, 67108866, 2, 67108864, 69206018, 2050, 67110912, 2099202, 2097154, 67110912, 67108866, 69206016, 69208064, 2097154, 69206016, 2048, 2050, 69208066, 2099200, 2, 67108864, 2099200, 67108864, 2099200, 2097152, 67110914, 67110914, 69206018, 69206018, 2, 2097154, 67108864, 67110912, 2097152, 69208064, 2050, 2099202, 69208064, 2050, 67108866, 69208066, 69206016, 2099200, 0, 2, 69208066, 0, 2099202, 69206016, 2048, 67108866, 67110912, 2048, 2097154),
        B = new Array(268439616, 4096, 262144, 268701760, 268435456, 268439616, 64, 268435456, 262208, 268697600, 268701760, 266240, 268701696, 266304, 4096, 64, 268697600, 268435520, 268439552, 4160, 266240, 262208, 268697664, 268701696, 4160, 0, 0, 268697664, 268435520, 268439552, 266304, 262144, 266304, 262144, 268701696, 4096, 64, 268697664, 4096, 266304, 268439552, 64, 268435520, 268697600, 268697664, 268435456, 262144, 268439616, 0, 268701760, 262208, 268435520, 268697600, 268439552, 268439616, 0, 268701760, 266240, 266240, 4160, 4160, 262208, 268435456, 268701696),
        C = des_createKeys(a),
        D = 0,
        E = b.length,
        F = 0,
        G = 32 == C.length ? 3 : 9;
    for (n = 3 == G ? c ? new Array(0, 32, 2) : new Array(30, -2, -2) : c ? new Array(0, 32, 2, 62, 30, -2, 64, 96, 2) : new Array(94, 62, -2, 32, 64, 2, 30, -2, -2), 2 == f ? b += "        " : 1 == f ? (i = 8 - E % 8, b += String.fromCharCode(i, i, i, i, i, i, i, i), 8 == i && (E += 8)) : f || (b += "\x00\x00\x00\x00\x00\x00\x00\x00"), result = "", tempresult = "", 1 == d && (o = e.charCodeAt(D++) << 24 | e.charCodeAt(D++) << 16 | e.charCodeAt(D++) << 8 | e.charCodeAt(D++), q = e.charCodeAt(D++) << 24 | e.charCodeAt(D++) << 16 | e.charCodeAt(D++) << 8 | e.charCodeAt(D++), D = 0); E > D;) {
        for (l = b.charCodeAt(D++) << 24 | b.charCodeAt(D++) << 16 | b.charCodeAt(D++) << 8 | b.charCodeAt(D++), m = b.charCodeAt(D++) << 24 | b.charCodeAt(D++) << 16 | b.charCodeAt(D++) << 8 | b.charCodeAt(D++), 1 == d && (c ? (l ^= o, m ^= q) : (p = o, r = q, o = l, q = m)), i = 252645135 & (l >>> 4 ^ m), m ^= i, l ^= i << 4, i = 65535 & (l >>> 16 ^ m), m ^= i, l ^= i << 16, i = 858993459 & (m >>> 2 ^ l), l ^= i, m ^= i << 2, i = 16711935 & (m >>> 8 ^ l), l ^= i, m ^= i << 8, i = 1431655765 & (l >>> 1 ^ m), m ^= i, l ^= i << 1, l = l << 1 | l >>> 31, m = m << 1 | m >>> 31, h = 0; G > h; h += 3) {
            for (s = n[h + 1], t = n[h + 2], g = n[h]; g != s; g += t) j = m ^ C[g], k = (m >>> 4 | m << 28) ^ C[g + 1], i = l, l = m, m = i ^ (v[j >>> 24 & 63] | x[j >>> 16 & 63] | z[j >>> 8 & 63] | B[63 & j] | u[k >>> 24 & 63] | w[k >>> 16 & 63] | y[k >>> 8 & 63] | A[63 & k]);
            i = l, l = m, m = i
        }
        l = l >>> 1 | l << 31, m = m >>> 1 | m << 31, i = 1431655765 & (l >>> 1 ^ m), m ^= i, l ^= i << 1, i = 16711935 & (m >>> 8 ^ l), l ^= i, m ^= i << 8, i = 858993459 & (m >>> 2 ^ l), l ^= i, m ^= i << 2, i = 65535 & (l >>> 16 ^ m), m ^= i, l ^= i << 16, i = 252645135 & (l >>> 4 ^ m), m ^= i, l ^= i << 4, 1 == d && (c ? (o = l, q = m) : (l ^= p, m ^= r)), tempresult += String.fromCharCode(l >>> 24, l >>> 16 & 255, l >>> 8 & 255, 255 & l, m >>> 24, m >>> 16 & 255, m >>> 8 & 255, 255 & m), F += 8, 512 == F && (result += tempresult, tempresult = "", F = 0)
    }
    return result += tempresult, result = result.replace(/\0*$/g, "")
}

function des_createKeys(a) {
    pc2bytes0 = new Array(0, 4, 536870912, 536870916, 65536, 65540, 536936448, 536936452, 512, 516, 536871424, 536871428, 66048, 66052, 536936960, 536936964), pc2bytes1 = new Array(0, 1, 1048576, 1048577, 67108864, 67108865, 68157440, 68157441, 256, 257, 1048832, 1048833, 67109120, 67109121, 68157696, 68157697), pc2bytes2 = new Array(0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272, 0, 8, 2048, 2056, 16777216, 16777224, 16779264, 16779272), pc2bytes3 = new Array(0, 2097152, 134217728, 136314880, 8192, 2105344, 134225920, 136323072, 131072, 2228224, 134348800, 136445952, 139264, 2236416, 134356992, 136454144), pc2bytes4 = new Array(0, 262144, 16, 262160, 0, 262144, 16, 262160, 4096, 266240, 4112, 266256, 4096, 266240, 4112, 266256), pc2bytes5 = new Array(0, 1024, 32, 1056, 0, 1024, 32, 1056, 33554432, 33555456, 33554464, 33555488, 33554432, 33555456, 33554464, 33555488), pc2bytes6 = new Array(0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746, 0, 268435456, 524288, 268959744, 2, 268435458, 524290, 268959746), pc2bytes7 = new Array(0, 65536, 2048, 67584, 536870912, 536936448, 536872960, 536938496, 131072, 196608, 133120, 198656, 537001984, 537067520, 537004032, 537069568), pc2bytes8 = new Array(0, 262144, 0, 262144, 2, 262146, 2, 262146, 33554432, 33816576, 33554432, 33816576, 33554434, 33816578, 33554434, 33816578), pc2bytes9 = new Array(0, 268435456, 8, 268435464, 0, 268435456, 8, 268435464, 1024, 268436480, 1032, 268436488, 1024, 268436480, 1032, 268436488), pc2bytes10 = new Array(0, 32, 0, 32, 1048576, 1048608, 1048576, 1048608, 8192, 8224, 8192, 8224, 1056768, 1056800, 1056768, 1056800), pc2bytes11 = new Array(0, 16777216, 512, 16777728, 2097152, 18874368, 2097664, 18874880, 67108864, 83886080, 67109376, 83886592, 69206016, 85983232, 69206528, 85983744), pc2bytes12 = new Array(0, 4096, 134217728, 134221824, 524288, 528384, 134742016, 134746112, 16, 4112, 134217744, 134221840, 524304, 528400, 134742032, 134746128), pc2bytes13 = new Array(0, 4, 256, 260, 0, 4, 256, 260, 1, 5, 257, 261, 1, 5, 257, 261);
    for (var b, c, d, e = a.length > 8 ? 3 : 1, f = new Array(32 * e), g = new Array(0, 0, 1, 1, 1, 1, 1, 1, 0, 1, 1, 1, 1, 1, 1, 0), h = 0, j = 0, k = 0; e > k; k++)
        for (left = a.charCodeAt(h++) << 24 | a.charCodeAt(h++) << 16 | a.charCodeAt(h++) << 8 | a.charCodeAt(h++), right = a.charCodeAt(h++) << 24 | a.charCodeAt(h++) << 16 | a.charCodeAt(h++) << 8 | a.charCodeAt(h++), d = 252645135 & (left >>> 4 ^ right), right ^= d, left ^= d << 4, d = 65535 & (right >>> -16 ^ left), left ^= d, right ^= d << -16, d = 858993459 & (left >>> 2 ^ right), right ^= d, left ^= d << 2, d = 65535 & (right >>> -16 ^ left), left ^= d, right ^= d << -16, d = 1431655765 & (left >>> 1 ^ right), right ^= d, left ^= d << 1, d = 16711935 & (right >>> 8 ^ left), left ^= d, right ^= d << 8, d = 1431655765 & (left >>> 1 ^ right), right ^= d, left ^= d << 1, d = left << 8 | right >>> 20 & 240, left = right << 24 | right << 8 & 16711680 | right >>> 8 & 65280 | right >>> 24 & 240, right = d, i = 0; i < g.length; i++) g[i] ? (left = left << 2 | left >>> 26, right = right << 2 | right >>> 26) : (left = left << 1 | left >>> 27, right = right << 1 | right >>> 27), left &= -15, right &= -15, b = pc2bytes0[left >>> 28] | pc2bytes1[left >>> 24 & 15] | pc2bytes2[left >>> 20 & 15] | pc2bytes3[left >>> 16 & 15] | pc2bytes4[left >>> 12 & 15] | pc2bytes5[left >>> 8 & 15] | pc2bytes6[left >>> 4 & 15], c = pc2bytes7[right >>> 28] | pc2bytes8[right >>> 24 & 15] | pc2bytes9[right >>> 20 & 15] | pc2bytes10[right >>> 16 & 15] | pc2bytes11[right >>> 12 & 15] | pc2bytes12[right >>> 8 & 15] | pc2bytes13[right >>> 4 & 15], d = 65535 & (c >>> 16 ^ b), f[j++] = b ^ d, f[j++] = c ^ d << 16;
    return f
}
var Aes = {};
Aes.cipher = function(a, b) {
    for (var c = 4, d = b.length / c - 1, e = [
            [],
            [],
            [],
            []
        ], f = 0; 4 * c > f; f++) e[f % 4][Math.floor(f / 4)] = a[f];
    e = Aes.addRoundKey(e, b, 0, c);
    for (var g = 1; d > g; g++) e = Aes.subBytes(e, c), e = Aes.shiftRows(e, c), e = Aes.mixColumns(e, c), e = Aes.addRoundKey(e, b, g, c);
    e = Aes.subBytes(e, c), e = Aes.shiftRows(e, c), e = Aes.addRoundKey(e, b, d, c);
    for (var h = new Array(4 * c), f = 0; 4 * c > f; f++) h[f] = e[f % 4][Math.floor(f / 4)];
    return h
}, Aes.keyExpansion = function(a) {
    for (var b = 4, c = a.length / 4, d = c + 6, e = new Array(b * (d + 1)), f = new Array(4), g = 0; c > g; g++) {
        var h = [a[4 * g], a[4 * g + 1], a[4 * g + 2], a[4 * g + 3]];
        e[g] = h
    }
    for (var g = c; b * (d + 1) > g; g++) {
        e[g] = new Array(4);
        for (var i = 0; 4 > i; i++) f[i] = e[g - 1][i];
        if (g % c == 0) {
            f = Aes.subWord(Aes.rotWord(f));
            for (var i = 0; 4 > i; i++) f[i] ^= Aes.rCon[g / c][i]
        } else c > 6 && g % c == 4 && (f = Aes.subWord(f));
        for (var i = 0; 4 > i; i++) e[g][i] = e[g - c][i] ^ f[i]
    }
    return e
}, Aes.subBytes = function(a, b) {
    for (var c = 0; 4 > c; c++)
        for (var d = 0; b > d; d++) a[c][d] = Aes.sBox[a[c][d]];
    return a
}, Aes.shiftRows = function(a, b) {
    for (var c = new Array(4), d = 1; 4 > d; d++) {
        for (var e = 0; 4 > e; e++) c[e] = a[d][(e + d) % b];
        for (var e = 0; 4 > e; e++) a[d][e] = c[e]
    }
    return a
}, Aes.mixColumns = function(a) {
    for (var b = 0; 4 > b; b++) {
        for (var c = new Array(4), d = new Array(4), e = 0; 4 > e; e++) c[e] = a[e][b], d[e] = 128 & a[e][b] ? a[e][b] << 1 ^ 283 : a[e][b] << 1;
        a[0][b] = d[0] ^ c[1] ^ d[1] ^ c[2] ^ c[3], a[1][b] = c[0] ^ d[1] ^ c[2] ^ d[2] ^ c[3], a[2][b] = c[0] ^ c[1] ^ d[2] ^ c[3] ^ d[3], a[3][b] = c[0] ^ d[0] ^ c[1] ^ c[2] ^ d[3]
    }
    return a
}, Aes.addRoundKey = function(a, b, c, d) {
    for (var e = 0; 4 > e; e++)
        for (var f = 0; d > f; f++) a[e][f] ^= b[4 * c + f][e];
    return a
}, Aes.subWord = function(a) {
    for (var b = 0; 4 > b; b++) a[b] = Aes.sBox[a[b]];
    return a
}, Aes.rotWord = function(a) {
    for (var b = a[0], c = 0; 3 > c; c++) a[c] = a[c + 1];
    return a[3] = b, a
}, Aes.sBox = [99, 124, 119, 123, 242, 107, 111, 197, 48, 1, 103, 43, 254, 215, 171, 118, 202, 130, 201, 125, 250, 89, 71, 240, 173, 212, 162, 175, 156, 164, 114, 192, 183, 253, 147, 38, 54, 63, 247, 204, 52, 165, 229, 241, 113, 216, 49, 21, 4, 199, 35, 195, 24, 150, 5, 154, 7, 18, 128, 226, 235, 39, 178, 117, 9, 131, 44, 26, 27, 110, 90, 160, 82, 59, 214, 179, 41, 227, 47, 132, 83, 209, 0, 237, 32, 252, 177, 91, 106, 203, 190, 57, 74, 76, 88, 207, 208, 239, 170, 251, 67, 77, 51, 133, 69, 249, 2, 127, 80, 60, 159, 168, 81, 163, 64, 143, 146, 157, 56, 245, 188, 182, 218, 33, 16, 255, 243, 210, 205, 12, 19, 236, 95, 151, 68, 23, 196, 167, 126, 61, 100, 93, 25, 115, 96, 129, 79, 220, 34, 42, 144, 136, 70, 238, 184, 20, 222, 94, 11, 219, 224, 50, 58, 10, 73, 6, 36, 92, 194, 211, 172, 98, 145, 149, 228, 121, 231, 200, 55, 109, 141, 213, 78, 169, 108, 86, 244, 234, 101, 122, 174, 8, 186, 120, 37, 46, 28, 166, 180, 198, 232, 221, 116, 31, 75, 189, 139, 138, 112, 62, 181, 102, 72, 3, 246, 14, 97, 53, 87, 185, 134, 193, 29, 158, 225, 248, 152, 17, 105, 217, 142, 148, 155, 30, 135, 233, 206, 85, 40, 223, 140, 161, 137, 13, 191, 230, 66, 104, 65, 153, 45, 15, 176, 84, 187, 22], Aes.rCon = [
    [0, 0, 0, 0],
    [1, 0, 0, 0],
    [2, 0, 0, 0],
    [4, 0, 0, 0],
    [8, 0, 0, 0],
    [16, 0, 0, 0],
    [32, 0, 0, 0],
    [64, 0, 0, 0],
    [128, 0, 0, 0],
    [27, 0, 0, 0],
    [54, 0, 0, 0]
], Aes.Ctr = {}, Aes.Ctr.encrypt = function(a, b, c) {
    var d = 16;
    if (128 != c && 192 != c && 256 != c) return "";
    a = Utf8.encode(a), b = Utf8.encode(b);
    for (var e = c / 8, f = new Array(e), g = 0; e > g; g++) f[g] = isNaN(b.charCodeAt(g)) ? 0 : b.charCodeAt(g);
    var h = Aes.cipher(f, Aes.keyExpansion(f));
    h = h.concat(h.slice(0, e - 16));
    for (var i = new Array(d), j = (new Date).getTime(), k = j % 1e3, l = Math.floor(j / 1e3), m = Math.floor(65535 * Math.random()), g = 0; 2 > g; g++) i[g] = k >>> 8 * g & 255;
    for (var g = 0; 2 > g; g++) i[g + 2] = m >>> 8 * g & 255;
    for (var g = 0; 4 > g; g++) i[g + 4] = l >>> 8 * g & 255;
    for (var n = "", g = 0; 8 > g; g++) n += String.fromCharCode(i[g]);
    for (var o = Aes.keyExpansion(h), p = Math.ceil(a.length / d), q = new Array(p), r = 0; p > r; r++) {
        for (var s = 0; 4 > s; s++) i[15 - s] = r >>> 8 * s & 255;
        for (var s = 0; 4 > s; s++) i[15 - s - 4] = r / 4294967296 >>> 8 * s;
        for (var t = Aes.cipher(i, o), u = p - 1 > r ? d : (a.length - 1) % d + 1, v = new Array(u), g = 0; u > g; g++) v[g] = t[g] ^ a.charCodeAt(r * d + g), v[g] = String.fromCharCode(v[g]);
        q[r] = v.join("")
    }
    var w = n + q.join("");
    return w = Base64.encode(w)
}, Aes.Ctr.decrypt = function(a, b, c) {
    var d = 16;
    if (128 != c && 192 != c && 256 != c) return "";
    a = Base64.decode(a), b = Utf8.encode(b);
    for (var e = c / 8, f = new Array(e), g = 0; e > g; g++) f[g] = isNaN(b.charCodeAt(g)) ? 0 : b.charCodeAt(g);
    var h = Aes.cipher(f, Aes.keyExpansion(f));
    h = h.concat(h.slice(0, e - 16));
    var i = new Array(8);
    ctrTxt = a.slice(0, 8);
    for (var g = 0; 8 > g; g++) i[g] = ctrTxt.charCodeAt(g);
    for (var j = Aes.keyExpansion(h), k = Math.ceil((a.length - 8) / d), l = new Array(k), m = 0; k > m; m++) l[m] = a.slice(8 + m * d, 8 + m * d + d);
    a = l;
    for (var n = new Array(a.length), m = 0; k > m; m++) {
        for (var o = 0; 4 > o; o++) i[15 - o] = m >>> 8 * o & 255;
        for (var o = 0; 4 > o; o++) i[15 - o - 4] = (m + 1) / 4294967296 - 1 >>> 8 * o & 255;
        for (var p = Aes.cipher(i, j), q = new Array(a[m].length), g = 0; g < a[m].length; g++) q[g] = p[g] ^ a[m].charCodeAt(g), q[g] = String.fromCharCode(q[g]);
        n[m] = q.join("")
    }
    var r = n.join("");
    return r = Utf8.decode(r)
};
var Base64 = {};
Base64.code = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=", Base64.encode = function(a, b) {
    b = "undefined" == typeof b ? !1 : b;
    var c, d, e, f, g, h, i, j, k, l, m, n = [],
        o = "",
        p = Base64.code;
    if (l = b ? a.encodeUTF8() : a, k = l.length % 3, k > 0)
        for (; k++ < 3;) o += "=", l += "\x00";
    for (k = 0; k < l.length; k += 3) c = l.charCodeAt(k), d = l.charCodeAt(k + 1), e = l.charCodeAt(k + 2), f = c << 16 | d << 8 | e, g = f >> 18 & 63, h = f >> 12 & 63, i = f >> 6 & 63, j = 63 & f, n[k / 3] = p.charAt(g) + p.charAt(h) + p.charAt(i) + p.charAt(j);
    return m = n.join(""), m = m.slice(0, m.length - o.length) + o
}, Base64.decode = function(a, b) {
    b = "undefined" == typeof b ? !1 : b;
    var c, d, e, f, g, h, i, j, k, l, m = [],
        n = Base64.code;
    l = b ? a.decodeUTF8() : a;
    for (var o = 0; o < l.length; o += 4) f = n.indexOf(l.charAt(o)), g = n.indexOf(l.charAt(o + 1)), h = n.indexOf(l.charAt(o + 2)), i = n.indexOf(l.charAt(o + 3)), j = f << 18 | g << 12 | h << 6 | i, c = j >>> 16 & 255, d = j >>> 8 & 255, e = 255 & j, m[o / 4] = String.fromCharCode(c, d, e), 64 == i && (m[o / 4] = String.fromCharCode(c, d)), 64 == h && (m[o / 4] = String.fromCharCode(c));
    return k = m.join(""), b ? k.decodeUTF8() : k
};
var Utf8 = {};
Utf8.encode = function(a) {
    var b = a.replace(/[\u0080-\u07ff]/g, function(a) {
        var b = a.charCodeAt(0);
        return String.fromCharCode(192 | b >> 6, 128 | 63 & b)
    });
    return b = b.replace(/[\u0800-\uffff]/g, function(a) {
        var b = a.charCodeAt(0);
        return String.fromCharCode(224 | b >> 12, 128 | b >> 6 & 63, 128 | 63 & b)
    })
}, Utf8.decode = function(a) {
    var b = a.replace(/[\u00e0-\u00ef][\u0080-\u00bf][\u0080-\u00bf]/g, function(a) {
        var b = (15 & a.charCodeAt(0)) << 12 | (63 & a.charCodeAt(1)) << 6 | 63 & a.charCodeAt(2);
        return String.fromCharCode(b)
    });
    return b = b.replace(/[\u00c0-\u00df][\u0080-\u00bf]/g, function(a) {
        var b = (31 & a.charCodeAt(0)) << 6 | 63 & a.charCodeAt(1);
        return String.fromCharCode(b)
    })
}, window.Aes = Aes, window.Base64 = Base64, window.Utf8 = Utf8;
(function() {
    var n, e, r, t, u, o, i, l, a, s, c, f, p, h, d = [].slice;
    u = "2.4.0", e = "pending", t = "resolved", r = "rejected", a = function(n, e) {
        return null != n ? n.hasOwnProperty(e) : void 0
    }, c = function(n) {
        return a(n, "length") && a(n, "callee")
    }, f = function(n) {
        return a(n, "promise") && "function" == typeof(null != n ? n.promise : void 0)
    }, l = function(n) {
        return c(n) ? l(Array.prototype.slice.call(n)) : Array.isArray(n) ? n.reduce(function(n, e) {
            return Array.isArray(e) ? n.concat(l(e)) : (n.push(e), n)
        }, []) : [n]
    }, o = function(n, e) {
        return 0 >= n ? e() : function() {
            return --n < 1 ? e.apply(this, arguments) : void 0
        }
    }, p = function(n, e) {
        return function() {
            var r;
            return r = [n].concat(Array.prototype.slice.call(arguments, 0)), e.apply(this, r)
        }
    }, i = function(n, e, r) {
        var t, u, o, i, a;
        for (i = l(n), a = [], u = 0, o = i.length; o > u; u++) t = i[u], a.push(t.call.apply(t, [r].concat(d.call(e))));
        return a
    }, n = function() {
        var u, o, a, s, c, p;
        return p = e, s = [], c = [], a = {}, this.promise = function(u) {
            var o, h;
            return u = u || {}, u.state = function() {
                return p
            }, h = function(n, r) {
                return function() {
                    return p === e && r.push.apply(r, l(arguments)), n() && i(arguments, a), u
                }
            }, u.done = h(function() {
                return p === t
            }, s), u.fail = h(function() {
                return p === r
            }, c), u.always = function() {
                var n;
                return (n = u.done.apply(u, arguments)).fail.apply(n, arguments)
            }, o = function(e, r) {
                var t, o;
                return o = new n, t = function(n, e, r) {
                    return r ? u[n](function() {
                        var n, t;
                        return n = 1 <= arguments.length ? d.call(arguments, 0) : [], t = r.apply(null, n), f(t) ? t.done(o.resolve).fail(o.reject) : o[e](t)
                    }) : u[n](o[e])
                }, t("done", "resolve", e), t("fail", "reject", r), o
            }, u.pipe = o, u.then = o, null == u.promise && (u.promise = function() {
                return u
            }), u
        }, this.promise(this), u = this, o = function(n, r, t) {
            return function() {
                return p === e ? (p = n, a = arguments, i(r, a, t), u) : this
            }
        }, this.resolve = o(t, s), this.reject = o(r, c), this.resolveWith = function(n, e) {
            return o(t, s, n).apply(null, e)
        }, this.rejectWith = function(n, e) {
            return o(r, c, n).apply(null, e)
        }, this
    }, h = function() {
        var e, r, t, u, i, a, s;
        if (r = l(arguments), 1 === r.length) return f(r[0]) ? r[0] : (new n).resolve(r[0]).promise();
        if (i = new n, !r.length) return i.resolve().promise();
        for (u = [], t = o(r.length, function() {
                return i.resolve.apply(i, u)
            }), r.forEach(function(n, e) {
                return f(n) ? n.done(function() {
                    var n;
                    return n = 1 <= arguments.length ? d.call(arguments, 0) : [], u[e] = n.length > 1 ? n : n[0], t()
                }) : (u[e] = n, t())
            }), a = 0, s = r.length; s > a; a++) e = r[a], f(e) && e.fail(i.reject);
        return i.promise()
    }, s = function(e) {
        return e.Deferred = function() {
            return new n
        }, e.ajax = p(e.ajax, function(e, r) {
            var t, u, o, i;
            return null == r && (r = {}), u = new n, t = function(n, e) {
                return p(n, function() {
                    var n, r;
                    return r = arguments[0], n = 2 <= arguments.length ? d.call(arguments, 1) : [], r && r.apply(null, n), e.apply(null, n)
                })
            }, r.success = t(r.success, u.resolve), r.error = t(r.error, u.reject), i = e(r), o = u.promise(), o.abort = function() {
                return i.abort()
            }, o
        }), e.when = h
    }, "undefined" != typeof exports ? (exports.Deferred = function() {
        return new n
    }, exports.when = h, exports.installInto = s) : "function" == typeof define && define.amd ? define(function() {
        return "undefined" != typeof Zepto ? s(Zepto) : (n.when = h, n.installInto = s, n)
    }) : "undefined" != typeof Zepto ? s(Zepto) : (this.Deferred = function() {
        return new n
    }, this.Deferred.when = h, this.Deferred.installInto = s)
}).call(this);
(function() {
    "use strict";

    function a() {}

    function b(a, b) {
        for (var c = a.length; c--;)
            if (a[c].listener === b) return c;
        return -1
    }

    function c(a) {
        return function() {
            return this[a].apply(this, arguments)
        }
    }
    var d = a.prototype,
        e = this,
        f = e.EventEmitter;
    d.getListeners = function(a) {
        var b, c, d = this._getEvents();
        if ("object" == typeof a) {
            b = {};
            for (c in d) d.hasOwnProperty(c) && a.test(c) && (b[c] = d[c])
        } else b = d[a] || (d[a] = []);
        return b
    }, d.flattenListeners = function(a) {
        var b, c = [];
        for (b = 0; b < a.length; b += 1) c.push(a[b].listener);
        return c
    }, d.getListenersAsObject = function(a) {
        var b, c = this.getListeners(a);
        return c instanceof Array && (b = {}, b[a] = c), b || c
    }, d.addListener = function(a, c) {
        var d, e = this.getListenersAsObject(a),
            f = "object" == typeof c;
        for (d in e) e.hasOwnProperty(d) && -1 === b(e[d], c) && e[d].push(f ? c : {
            listener: c,
            once: !1
        });
        return this
    }, d.on = c("addListener"), d.addOnceListener = function(a, b) {
        return this.addListener(a, {
            listener: b,
            once: !0
        })
    }, d.once = c("addOnceListener"), d.defineEvent = function(a) {
        return this.getListeners(a), this
    }, d.defineEvents = function(a) {
        for (var b = 0; b < a.length; b += 1) this.defineEvent(a[b]);
        return this
    }, d.removeListener = function(a, c) {
        var d, e, f = this.getListenersAsObject(a);
        for (e in f) f.hasOwnProperty(e) && (d = b(f[e], c), -1 !== d && f[e].splice(d, 1));
        return this
    }, d.off = c("removeListener"), d.addListeners = function(a, b) {
        return this.manipulateListeners(!1, a, b)
    }, d.removeListeners = function(a, b) {
        return this.manipulateListeners(!0, a, b)
    }, d.manipulateListeners = function(a, b, c) {
        var d, e, f = a ? this.removeListener : this.addListener,
            g = a ? this.removeListeners : this.addListeners;
        if ("object" != typeof b || b instanceof RegExp)
            for (d = c.length; d--;) f.call(this, b, c[d]);
        else
            for (d in b) b.hasOwnProperty(d) && (e = b[d]) && ("function" == typeof e ? f.call(this, d, e) : g.call(this, d, e));
        return this
    }, d.removeEvent = function(a) {
        var b, c = typeof a,
            d = this._getEvents();
        if ("string" === c) delete d[a];
        else if ("object" === c)
            for (b in d) d.hasOwnProperty(b) && a.test(b) && delete d[b];
        else delete this._events;
        return this
    }, d.removeAllListeners = c("removeEvent"), d.emitEvent = function(a, b) {
        var c, d, e, f, g = this.getListenersAsObject(a);
        for (e in g)
            if (g.hasOwnProperty(e))
                for (d = g[e].length; d--;) c = g[e][d], c.once === !0 && this.removeListener(a, c.listener), f = c.listener.apply(this, b || []), f === this._getOnceReturnValue() && this.removeListener(a, c.listener);
        return this
    }, d.trigger = c("emitEvent"), d.emit = function(a) {
        var b = Array.prototype.slice.call(arguments, 1);
        return this.emitEvent(a, b)
    }, d.setOnceReturnValue = function(a) {
        return this._onceReturnValue = a, this
    }, d._getOnceReturnValue = function() {
        return this.hasOwnProperty("_onceReturnValue") ? this._onceReturnValue : !0
    }, d._getEvents = function() {
        return this._events || (this._events = {})
    }, a.noConflict = function() {
        return e.EventEmitter = f, a
    }, "function" == typeof define && define.amd ? define(function() {
        return a
    }) : "object" == typeof module && module.exports ? module.exports = a : this.EventEmitter = a
}).call(this);

function hexmd5(a) {
    return rstr2hex(rstr_md5(str2rstr_utf8(a)))
}

function b64_md5(a) {
    return rstr2b64(rstr_md5(str2rstr_utf8(a)))
}

function any_md5(a, b) {
    return rstr2any(rstr_md5(str2rstr_utf8(a)), b)
}

function hex_hmac_md5(a, b) {
    return rstr2hex(rstr_hmac_md5(str2rstr_utf8(a), str2rstr_utf8(b)))
}

function b64_hmac_md5(a, b) {
    return rstr2b64(rstr_hmac_md5(str2rstr_utf8(a), str2rstr_utf8(b)))
}

function any_hmac_md5(a, b, c) {
    return rstr2any(rstr_hmac_md5(str2rstr_utf8(a), str2rstr_utf8(b)), c)
}

function md5_vm_test() {
    return "900150983cd24fb0d6963f7d28e17f72" == hexmd5("abc").toLowerCase()
}

function rstr_md5(a) {
    return binl2rstr(binl_md5(rstr2binl(a), 8 * a.length))
}

function rstr_hmac_md5(a, b) {
    var c = rstr2binl(a);
    c.length > 16 && (c = binl_md5(c, 8 * a.length));
    for (var d = Array(16), e = Array(16), f = 0; 16 > f; f++) d[f] = 909522486 ^ c[f], e[f] = 1549556828 ^ c[f];
    var g = binl_md5(d.concat(rstr2binl(b)), 512 + 8 * b.length);
    return binl2rstr(binl_md5(e.concat(g), 640))
}

function rstr2hex(a) {
    try {} catch (b) {
        hexcase = 0
    }
    for (var c, d = hexcase ? "0123456789ABCDEF" : "0123456789abcdef", e = "", f = 0; f < a.length; f++) c = a.charCodeAt(f), e += !zerofill && 16 > c ? d.charAt(c) : d.charAt(c >>> 4 & 15) + d.charAt(15 & c);
    return e
}

function rstr2b64(a) {
    try {} catch (b) {
        b64pad = ""
    }
    for (var c = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/", d = "", e = a.length, f = 0; e > f; f += 3)
        for (var g = a.charCodeAt(f) << 16 | (e > f + 1 ? a.charCodeAt(f + 1) << 8 : 0) | (e > f + 2 ? a.charCodeAt(f + 2) : 0), h = 0; 4 > h; h++) d += 8 * f + 6 * h > 8 * a.length ? b64pad : c.charAt(g >>> 6 * (3 - h) & 63);
    return d
}

function rstr2any(a, b) {
    var c, d, e, f, g, h = b.length,
        i = Array(Math.ceil(a.length / 2));
    for (c = 0; c < i.length; c++) i[c] = a.charCodeAt(2 * c) << 8 | a.charCodeAt(2 * c + 1);
    var j = Math.ceil(8 * a.length / (Math.log(b.length) / Math.log(2))),
        k = Array(j);
    for (d = 0; j > d; d++) {
        for (g = Array(), f = 0, c = 0; c < i.length; c++) f = (f << 16) + i[c], e = Math.floor(f / h), f -= e * h, (g.length > 0 || e > 0) && (g[g.length] = e);
        k[d] = f, i = g
    }
    var l = "";
    for (c = k.length - 1; c >= 0; c--) l += b.charAt(k[c]);
    return l
}

function str2rstr_utf8(a) {
    for (var b, c, d = "", e = -1; ++e < a.length;) b = a.charCodeAt(e), c = e + 1 < a.length ? a.charCodeAt(e + 1) : 0, b >= 55296 && 56319 >= b && c >= 56320 && 57343 >= c && (b = 65536 + ((1023 & b) << 10) + (1023 & c), e++), 127 >= b ? d += String.fromCharCode(b) : 2047 >= b ? d += String.fromCharCode(192 | b >>> 6 & 31, 128 | 63 & b) : 65535 >= b ? d += String.fromCharCode(224 | b >>> 12 & 15, 128 | b >>> 6 & 63, 128 | 63 & b) : 2097151 >= b && (d += String.fromCharCode(240 | b >>> 18 & 7, 128 | b >>> 12 & 63, 128 | b >>> 6 & 63, 128 | 63 & b));
    return d
}

function str2rstr_utf16le(a) {
    for (var b = "", c = 0; c < a.length; c++) b += String.fromCharCode(255 & a.charCodeAt(c), a.charCodeAt(c) >>> 8 & 255);
    return b
}

function str2rstr_utf16be(a) {
    for (var b = "", c = 0; c < a.length; c++) b += String.fromCharCode(a.charCodeAt(c) >>> 8 & 255, 255 & a.charCodeAt(c));
    return b
}

function rstr2binl(a) {
    for (var b = Array(a.length >> 2), c = 0; c < b.length; c++) b[c] = 0;
    for (var c = 0; c < 8 * a.length; c += 8) b[c >> 5] |= (255 & a.charCodeAt(c / 8)) << c % 32;
    return b
}

function binl2rstr(a) {
    for (var b = "", c = 0; c < 32 * a.length; c += 8) b += String.fromCharCode(a[c >> 5] >>> c % 32 & 255);
    return b
}

function binl_md5(a, b) {
    a[b >> 5] |= 128 << b % 32, a[(b + 64 >>> 9 << 4) + 14] = b;
    for (var c = 1732584193, d = -271733879, e = -1732584194, f = 271733878, g = 0; g < a.length; g += 16) {
        var h = c,
            i = d,
            j = e,
            k = f;
        c = md5_ff(c, d, e, f, a[g + 0], 7, -680876936), f = md5_ff(f, c, d, e, a[g + 1], 12, -389564586), e = md5_ff(e, f, c, d, a[g + 2], 17, 606105819), d = md5_ff(d, e, f, c, a[g + 3], 22, -1044525330), c = md5_ff(c, d, e, f, a[g + 4], 7, -176418897), f = md5_ff(f, c, d, e, a[g + 5], 12, 1200080426), e = md5_ff(e, f, c, d, a[g + 6], 17, -1473231341), d = md5_ff(d, e, f, c, a[g + 7], 22, -45705983), c = md5_ff(c, d, e, f, a[g + 8], 7, 1770035416), f = md5_ff(f, c, d, e, a[g + 9], 12, -1958414417), e = md5_ff(e, f, c, d, a[g + 10], 17, -42063), d = md5_ff(d, e, f, c, a[g + 11], 22, -1990404162), c = md5_ff(c, d, e, f, a[g + 12], 7, 1804603682), f = md5_ff(f, c, d, e, a[g + 13], 12, -40341101), e = md5_ff(e, f, c, d, a[g + 14], 17, -1502002290), d = md5_ff(d, e, f, c, a[g + 15], 22, 1236535329), c = md5_gg(c, d, e, f, a[g + 1], 5, -165796510), f = md5_gg(f, c, d, e, a[g + 6], 9, -1069501632), e = md5_gg(e, f, c, d, a[g + 11], 14, 643717713), d = md5_gg(d, e, f, c, a[g + 0], 20, -373897302), c = md5_gg(c, d, e, f, a[g + 5], 5, -701558691), f = md5_gg(f, c, d, e, a[g + 10], 9, 38016083), e = md5_gg(e, f, c, d, a[g + 15], 14, -660478335), d = md5_gg(d, e, f, c, a[g + 4], 20, -405537848), c = md5_gg(c, d, e, f, a[g + 9], 5, 568446438), f = md5_gg(f, c, d, e, a[g + 14], 9, -1019803690), e = md5_gg(e, f, c, d, a[g + 3], 14, -187363961), d = md5_gg(d, e, f, c, a[g + 8], 20, 1163531501), c = md5_gg(c, d, e, f, a[g + 13], 5, -1444681467), f = md5_gg(f, c, d, e, a[g + 2], 9, -51403784), e = md5_gg(e, f, c, d, a[g + 7], 14, 1735328473), d = md5_gg(d, e, f, c, a[g + 12], 20, -1926607734), c = md5_hh(c, d, e, f, a[g + 5], 4, -378558), f = md5_hh(f, c, d, e, a[g + 8], 11, -2022574463), e = md5_hh(e, f, c, d, a[g + 11], 16, 1839030562), d = md5_hh(d, e, f, c, a[g + 14], 23, -35309556), c = md5_hh(c, d, e, f, a[g + 1], 4, -1530992060), f = md5_hh(f, c, d, e, a[g + 4], 11, 1272893353), e = md5_hh(e, f, c, d, a[g + 7], 16, -155497632), d = md5_hh(d, e, f, c, a[g + 10], 23, -1094730640), c = md5_hh(c, d, e, f, a[g + 13], 4, 681279174), f = md5_hh(f, c, d, e, a[g + 0], 11, -358537222), e = md5_hh(e, f, c, d, a[g + 3], 16, -722521979), d = md5_hh(d, e, f, c, a[g + 6], 23, 76029189), c = md5_hh(c, d, e, f, a[g + 9], 4, -640364487), f = md5_hh(f, c, d, e, a[g + 12], 11, -421815835), e = md5_hh(e, f, c, d, a[g + 15], 16, 530742520), d = md5_hh(d, e, f, c, a[g + 2], 23, -995338651), c = md5_ii(c, d, e, f, a[g + 0], 6, -198630844), f = md5_ii(f, c, d, e, a[g + 7], 10, 1126891415), e = md5_ii(e, f, c, d, a[g + 14], 15, -1416354905), d = md5_ii(d, e, f, c, a[g + 5], 21, -57434055), c = md5_ii(c, d, e, f, a[g + 12], 6, 1700485571), f = md5_ii(f, c, d, e, a[g + 3], 10, -1894986606), e = md5_ii(e, f, c, d, a[g + 10], 15, -1051523), d = md5_ii(d, e, f, c, a[g + 1], 21, -2054922799), c = md5_ii(c, d, e, f, a[g + 8], 6, 1873313359), f = md5_ii(f, c, d, e, a[g + 15], 10, -30611744), e = md5_ii(e, f, c, d, a[g + 6], 15, -1560198380), d = md5_ii(d, e, f, c, a[g + 13], 21, 1309151649), c = md5_ii(c, d, e, f, a[g + 4], 6, -145523070), f = md5_ii(f, c, d, e, a[g + 11], 10, -1120210379), e = md5_ii(e, f, c, d, a[g + 2], 15, 718787259), d = md5_ii(d, e, f, c, a[g + 9], 21, -343485551), c = safe_add(c, h), d = safe_add(d, i), e = safe_add(e, j), f = safe_add(f, k)
    }
    return Array(c, d, e, f)
}

function md5_cmn(a, b, c, d, e, f) {
    return safe_add(bit_rol(safe_add(safe_add(b, a), safe_add(d, f)), e), c)
}

function md5_ff(a, b, c, d, e, f, g) {
    return md5_cmn(b & c | ~b & d, a, b, e, f, g)
}

function md5_gg(a, b, c, d, e, f, g) {
    return md5_cmn(b & d | c & ~d, a, b, e, f, g)
}

function md5_hh(a, b, c, d, e, f, g) {
    return md5_cmn(b ^ c ^ d, a, b, e, f, g)
}

function md5_ii(a, b, c, d, e, f, g) {
    return md5_cmn(c ^ (b | ~d), a, b, e, f, g)
}

function safe_add(a, b) {
    var c = (65535 & a) + (65535 & b),
        d = (a >> 16) + (b >> 16) + (c >> 16);
    return d << 16 | 65535 & c
}

function bit_rol(a, b) {
    return a << b | a >>> 32 - b
}
var zerofill = 1,
    hexcase = 0,
    b64pad = "";
//     Underscore.js 1.5.2
//     http://underscorejs.org
//     (c) 2009-2013 Jeremy Ashkenas, DocumentCloud and Investigative Reporters & Editors
//     Underscore may be freely distributed under the MIT license.
(function() {
    var n = this,
        t = n._,
        r = {},
        e = Array.prototype,
        u = Object.prototype,
        i = Function.prototype,
        a = e.push,
        o = e.slice,
        c = e.concat,
        l = u.toString,
        f = u.hasOwnProperty,
        s = e.forEach,
        p = e.map,
        h = e.reduce,
        v = e.reduceRight,
        g = e.filter,
        d = e.every,
        m = e.some,
        y = e.indexOf,
        b = e.lastIndexOf,
        x = Array.isArray,
        w = Object.keys,
        _ = i.bind,
        j = function(n) {
            return n instanceof j ? n : this instanceof j ? (this._wrapped = n, void 0) : new j(n)
        };
    "undefined" != typeof exports ? ("undefined" != typeof module && module.exports && (exports = module.exports = j), exports._ = j) : n._ = j, j.VERSION = "1.5.2";
    var A = j.each = j.forEach = function(n, t, e) {
        if (null != n)
            if (s && n.forEach === s) n.forEach(t, e);
            else if (n.length === +n.length) {
            for (var u = 0, i = n.length; i > u; u++)
                if (t.call(e, n[u], u, n) === r) return
        } else
            for (var a = j.keys(n), u = 0, i = a.length; i > u; u++)
                if (t.call(e, n[a[u]], a[u], n) === r) return
    };
    j.map = j.collect = function(n, t, r) {
        var e = [];
        return null == n ? e : p && n.map === p ? n.map(t, r) : (A(n, function(n, u, i) {
            e.push(t.call(r, n, u, i))
        }), e)
    };
    var E = "Reduce of empty array with no initial value";
    j.reduce = j.foldl = j.inject = function(n, t, r, e) {
        var u = arguments.length > 2;
        if (null == n && (n = []), h && n.reduce === h) return e && (t = j.bind(t, e)), u ? n.reduce(t, r) : n.reduce(t);
        if (A(n, function(n, i, a) {
                u ? r = t.call(e, r, n, i, a) : (r = n, u = !0)
            }), !u) throw new TypeError(E);
        return r
    }, j.reduceRight = j.foldr = function(n, t, r, e) {
        var u = arguments.length > 2;
        if (null == n && (n = []), v && n.reduceRight === v) return e && (t = j.bind(t, e)), u ? n.reduceRight(t, r) : n.reduceRight(t);
        var i = n.length;
        if (i !== +i) {
            var a = j.keys(n);
            i = a.length
        }
        if (A(n, function(o, c, l) {
                c = a ? a[--i] : --i, u ? r = t.call(e, r, n[c], c, l) : (r = n[c], u = !0)
            }), !u) throw new TypeError(E);
        return r
    }, j.find = j.detect = function(n, t, r) {
        var e;
        return O(n, function(n, u, i) {
            return t.call(r, n, u, i) ? (e = n, !0) : void 0
        }), e
    }, j.filter = j.select = function(n, t, r) {
        var e = [];
        return null == n ? e : g && n.filter === g ? n.filter(t, r) : (A(n, function(n, u, i) {
            t.call(r, n, u, i) && e.push(n)
        }), e)
    }, j.reject = function(n, t, r) {
        return j.filter(n, function(n, e, u) {
            return !t.call(r, n, e, u)
        }, r)
    }, j.every = j.all = function(n, t, e) {
        t || (t = j.identity);
        var u = !0;
        return null == n ? u : d && n.every === d ? n.every(t, e) : (A(n, function(n, i, a) {
            return (u = u && t.call(e, n, i, a)) ? void 0 : r
        }), !!u)
    };
    var O = j.some = j.any = function(n, t, e) {
        t || (t = j.identity);
        var u = !1;
        return null == n ? u : m && n.some === m ? n.some(t, e) : (A(n, function(n, i, a) {
            return u || (u = t.call(e, n, i, a)) ? r : void 0
        }), !!u)
    };
    j.contains = j.include = function(n, t) {
        return null == n ? !1 : y && n.indexOf === y ? n.indexOf(t) != -1 : O(n, function(n) {
            return n === t
        })
    }, j.invoke = function(n, t) {
        var r = o.call(arguments, 2),
            e = j.isFunction(t);
        return j.map(n, function(n) {
            return (e ? t : n[t]).apply(n, r)
        })
    }, j.pluck = function(n, t) {
        return j.map(n, function(n) {
            return n[t]
        })
    }, j.where = function(n, t, r) {
        return j.isEmpty(t) ? r ? void 0 : [] : j[r ? "find" : "filter"](n, function(n) {
            for (var r in t)
                if (t[r] !== n[r]) return !1;
            return !0
        })
    }, j.findWhere = function(n, t) {
        return j.where(n, t, !0)
    }, j.max = function(n, t, r) {
        if (!t && j.isArray(n) && n[0] === +n[0] && n.length < 65535) return Math.max.apply(Math, n);
        if (!t && j.isEmpty(n)) return -1 / 0;
        var e = {
            computed: -1 / 0,
            value: -1 / 0
        };
        return A(n, function(n, u, i) {
            var a = t ? t.call(r, n, u, i) : n;
            a > e.computed && (e = {
                value: n,
                computed: a
            })
        }), e.value
    }, j.min = function(n, t, r) {
        if (!t && j.isArray(n) && n[0] === +n[0] && n.length < 65535) return Math.min.apply(Math, n);
        if (!t && j.isEmpty(n)) return 1 / 0;
        var e = {
            computed: 1 / 0,
            value: 1 / 0
        };
        return A(n, function(n, u, i) {
            var a = t ? t.call(r, n, u, i) : n;
            a < e.computed && (e = {
                value: n,
                computed: a
            })
        }), e.value
    }, j.shuffle = function(n) {
        var t, r = 0,
            e = [];
        return A(n, function(n) {
            t = j.random(r++), e[r - 1] = e[t], e[t] = n
        }), e
    }, j.sample = function(n, t, r) {
        return arguments.length < 2 || r ? n[j.random(n.length - 1)] : j.shuffle(n).slice(0, Math.max(0, t))
    };
    var k = function(n) {
        return j.isFunction(n) ? n : function(t) {
            return t[n]
        }
    };
    j.sortBy = function(n, t, r) {
        var e = k(t);
        return j.pluck(j.map(n, function(n, t, u) {
            return {
                value: n,
                index: t,
                criteria: e.call(r, n, t, u)
            }
        }).sort(function(n, t) {
            var r = n.criteria,
                e = t.criteria;
            if (r !== e) {
                if (r > e || r === void 0) return 1;
                if (e > r || e === void 0) return -1
            }
            return n.index - t.index
        }), "value")
    };
    var F = function(n) {
        return function(t, r, e) {
            var u = {},
                i = null == r ? j.identity : k(r);
            return A(t, function(r, a) {
                var o = i.call(e, r, a, t);
                n(u, o, r)
            }), u
        }
    };
    j.groupBy = F(function(n, t, r) {
        (j.has(n, t) ? n[t] : n[t] = []).push(r)
    }), j.indexBy = F(function(n, t, r) {
        n[t] = r
    }), j.countBy = F(function(n, t) {
        j.has(n, t) ? n[t] ++ : n[t] = 1
    }), j.sortedIndex = function(n, t, r, e) {
        r = null == r ? j.identity : k(r);
        for (var u = r.call(e, t), i = 0, a = n.length; a > i;) {
            var o = i + a >>> 1;
            r.call(e, n[o]) < u ? i = o + 1 : a = o
        }
        return i
    }, j.toArray = function(n) {
        return n ? j.isArray(n) ? o.call(n) : n.length === +n.length ? j.map(n, j.identity) : j.values(n) : []
    }, j.size = function(n) {
        return null == n ? 0 : n.length === +n.length ? n.length : j.keys(n).length
    }, j.first = j.head = j.take = function(n, t, r) {
        return null == n ? void 0 : null == t || r ? n[0] : o.call(n, 0, t)
    }, j.initial = function(n, t, r) {
        return o.call(n, 0, n.length - (null == t || r ? 1 : t))
    }, j.last = function(n, t, r) {
        return null == n ? void 0 : null == t || r ? n[n.length - 1] : o.call(n, Math.max(n.length - t, 0))
    }, j.rest = j.tail = j.drop = function(n, t, r) {
        return o.call(n, null == t || r ? 1 : t)
    }, j.compact = function(n) {
        return j.filter(n, j.identity)
    };
    var M = function(n, t, r) {
        return t && j.every(n, j.isArray) ? c.apply(r, n) : (A(n, function(n) {
            j.isArray(n) || j.isArguments(n) ? t ? a.apply(r, n) : M(n, t, r) : r.push(n)
        }), r)
    };
    j.flatten = function(n, t) {
        return M(n, t, [])
    }, j.without = function(n) {
        return j.difference(n, o.call(arguments, 1))
    }, j.uniq = j.unique = function(n, t, r, e) {
        j.isFunction(t) && (e = r, r = t, t = !1);
        var u = r ? j.map(n, r, e) : n,
            i = [],
            a = [];
        return A(u, function(r, e) {
            (t ? e && a[a.length - 1] === r : j.contains(a, r)) || (a.push(r), i.push(n[e]))
        }), i
    }, j.union = function() {
        return j.uniq(j.flatten(arguments, !0))
    }, j.intersection = function(n) {
        var t = o.call(arguments, 1);
        return j.filter(j.uniq(n), function(n) {
            return j.every(t, function(t) {
                return j.indexOf(t, n) >= 0
            })
        })
    }, j.difference = function(n) {
        var t = c.apply(e, o.call(arguments, 1));
        return j.filter(n, function(n) {
            return !j.contains(t, n)
        })
    }, j.zip = function() {
        for (var n = j.max(j.pluck(arguments, "length").concat(0)), t = new Array(n), r = 0; n > r; r++) t[r] = j.pluck(arguments, "" + r);
        return t
    }, j.object = function(n, t) {
        if (null == n) return {};
        for (var r = {}, e = 0, u = n.length; u > e; e++) t ? r[n[e]] = t[e] : r[n[e][0]] = n[e][1];
        return r
    }, j.indexOf = function(n, t, r) {
        if (null == n) return -1;
        var e = 0,
            u = n.length;
        if (r) {
            if ("number" != typeof r) return e = j.sortedIndex(n, t), n[e] === t ? e : -1;
            e = 0 > r ? Math.max(0, u + r) : r
        }
        if (y && n.indexOf === y) return n.indexOf(t, r);
        for (; u > e; e++)
            if (n[e] === t) return e;
        return -1
    }, j.lastIndexOf = function(n, t, r) {
        if (null == n) return -1;
        var e = null != r;
        if (b && n.lastIndexOf === b) return e ? n.lastIndexOf(t, r) : n.lastIndexOf(t);
        for (var u = e ? r : n.length; u--;)
            if (n[u] === t) return u;
        return -1
    }, j.range = function(n, t, r) {
        arguments.length <= 1 && (t = n || 0, n = 0), r = arguments[2] || 1;
        for (var e = Math.max(Math.ceil((t - n) / r), 0), u = 0, i = new Array(e); e > u;) i[u++] = n, n += r;
        return i
    };
    var R = function() {};
    j.bind = function(n, t) {
        var r, e;
        if (_ && n.bind === _) return _.apply(n, o.call(arguments, 1));
        if (!j.isFunction(n)) throw new TypeError;
        return r = o.call(arguments, 2), e = function() {
            if (!(this instanceof e)) return n.apply(t, r.concat(o.call(arguments)));
            R.prototype = n.prototype;
            var u = new R;
            R.prototype = null;
            var i = n.apply(u, r.concat(o.call(arguments)));
            return Object(i) === i ? i : u
        }
    }, j.partial = function(n) {
        var t = o.call(arguments, 1);
        return function() {
            return n.apply(this, t.concat(o.call(arguments)))
        }
    }, j.bindAll = function(n) {
        var t = o.call(arguments, 1);
        if (0 === t.length) throw new Error("bindAll must be passed function names");
        return A(t, function(t) {
            n[t] = j.bind(n[t], n)
        }), n
    }, j.memoize = function(n, t) {
        var r = {};
        return t || (t = j.identity),
            function() {
                var e = t.apply(this, arguments);
                return j.has(r, e) ? r[e] : r[e] = n.apply(this, arguments)
            }
    }, j.delay = function(n, t) {
        var r = o.call(arguments, 2);
        return setTimeout(function() {
            return n.apply(null, r)
        }, t)
    }, j.defer = function(n) {
        return j.delay.apply(j, [n, 1].concat(o.call(arguments, 1)))
    }, j.throttle = function(n, t, r) {
        var e, u, i, a = null,
            o = 0;
        r || (r = {});
        var c = function() {
            o = r.leading === !1 ? 0 : new Date, a = null, i = n.apply(e, u)
        };
        return function() {
            var l = new Date;
            o || r.leading !== !1 || (o = l);
            var f = t - (l - o);
            return e = this, u = arguments, 0 >= f ? (clearTimeout(a), a = null, o = l, i = n.apply(e, u)) : a || r.trailing === !1 || (a = setTimeout(c, f)), i
        }
    }, j.debounce = function(n, t, r) {
        var e, u, i, a, o;
        return function() {
            i = this, u = arguments, a = new Date;
            var c = function() {
                    var l = new Date - a;
                    t > l ? e = setTimeout(c, t - l) : (e = null, r || (o = n.apply(i, u)))
                },
                l = r && !e;
            return e || (e = setTimeout(c, t)), l && (o = n.apply(i, u)), o
        }
    }, j.once = function(n) {
        var t, r = !1;
        return function() {
            return r ? t : (r = !0, t = n.apply(this, arguments), n = null, t)
        }
    }, j.wrap = function(n, t) {
        return function() {
            var r = [n];
            return a.apply(r, arguments), t.apply(this, r)
        }
    }, j.compose = function() {
        var n = arguments;
        return function() {
            for (var t = arguments, r = n.length - 1; r >= 0; r--) t = [n[r].apply(this, t)];
            return t[0]
        }
    }, j.after = function(n, t) {
        return function() {
            return --n < 1 ? t.apply(this, arguments) : void 0
        }
    }, j.keys = w || function(n) {
        if (n !== Object(n)) throw new TypeError("Invalid object");
        var t = [];
        for (var r in n) j.has(n, r) && t.push(r);
        return t
    }, j.values = function(n) {
        for (var t = j.keys(n), r = t.length, e = new Array(r), u = 0; r > u; u++) e[u] = n[t[u]];
        return e
    }, j.pairs = function(n) {
        for (var t = j.keys(n), r = t.length, e = new Array(r), u = 0; r > u; u++) e[u] = [t[u], n[t[u]]];
        return e
    }, j.invert = function(n) {
        for (var t = {}, r = j.keys(n), e = 0, u = r.length; u > e; e++) t[n[r[e]]] = r[e];
        return t
    }, j.functions = j.methods = function(n) {
        var t = [];
        for (var r in n) j.isFunction(n[r]) && t.push(r);
        return t.sort()
    }, j.extend = function(n) {
        return A(o.call(arguments, 1), function(t) {
            if (t)
                for (var r in t) n[r] = t[r]
        }), n
    }, j.pick = function(n) {
        var t = {},
            r = c.apply(e, o.call(arguments, 1));
        return A(r, function(r) {
            r in n && (t[r] = n[r])
        }), t
    }, j.omit = function(n) {
        var t = {},
            r = c.apply(e, o.call(arguments, 1));
        for (var u in n) j.contains(r, u) || (t[u] = n[u]);
        return t
    }, j.defaults = function(n) {
        return A(o.call(arguments, 1), function(t) {
            if (t)
                for (var r in t) n[r] === void 0 && (n[r] = t[r])
        }), n
    }, j.clone = function(n) {
        return j.isObject(n) ? j.isArray(n) ? n.slice() : j.extend({}, n) : n
    }, j.tap = function(n, t) {
        return t(n), n
    };
    var S = function(n, t, r, e) {
        if (n === t) return 0 !== n || 1 / n == 1 / t;
        if (null == n || null == t) return n === t;
        n instanceof j && (n = n._wrapped), t instanceof j && (t = t._wrapped);
        var u = l.call(n);
        if (u != l.call(t)) return !1;
        switch (u) {
            case "[object String]":
                return n == String(t);
            case "[object Number]":
                return n != +n ? t != +t : 0 == n ? 1 / n == 1 / t : n == +t;
            case "[object Date]":
            case "[object Boolean]":
                return +n == +t;
            case "[object RegExp]":
                return n.source == t.source && n.global == t.global && n.multiline == t.multiline && n.ignoreCase == t.ignoreCase
        }
        if ("object" != typeof n || "object" != typeof t) return !1;
        for (var i = r.length; i--;)
            if (r[i] == n) return e[i] == t;
        var a = n.constructor,
            o = t.constructor;
        if (a !== o && !(j.isFunction(a) && a instanceof a && j.isFunction(o) && o instanceof o)) return !1;
        r.push(n), e.push(t);
        var c = 0,
            f = !0;
        if ("[object Array]" == u) {
            if (c = n.length, f = c == t.length)
                for (; c-- && (f = S(n[c], t[c], r, e)););
        } else {
            for (var s in n)
                if (j.has(n, s) && (c++, !(f = j.has(t, s) && S(n[s], t[s], r, e)))) break;
            if (f) {
                for (s in t)
                    if (j.has(t, s) && !c--) break;
                f = !c
            }
        }
        return r.pop(), e.pop(), f
    };
    j.isEqual = function(n, t) {
        return S(n, t, [], [])
    }, j.isEmpty = function(n) {
        if (null == n) return !0;
        if (j.isArray(n) || j.isString(n)) return 0 === n.length;
        for (var t in n)
            if (j.has(n, t)) return !1;
        return !0
    }, j.isElement = function(n) {
        return !(!n || 1 !== n.nodeType)
    }, j.isArray = x || function(n) {
        return "[object Array]" == l.call(n)
    }, j.isObject = function(n) {
        return n === Object(n)
    }, A(["Arguments", "Function", "String", "Number", "Date", "RegExp"], function(n) {
        j["is" + n] = function(t) {
            return l.call(t) == "[object " + n + "]"
        }
    }), j.isArguments(arguments) || (j.isArguments = function(n) {
        return !(!n || !j.has(n, "callee"))
    }), "function" != typeof / . / && (j.isFunction = function(n) {
        return "function" == typeof n
    }), j.isFinite = function(n) {
        return isFinite(n) && !isNaN(parseFloat(n))
    }, j.isNaN = function(n) {
        return j.isNumber(n) && n != +n
    }, j.isBoolean = function(n) {
        return n === !0 || n === !1 || "[object Boolean]" == l.call(n)
    }, j.isNull = function(n) {
        return null === n
    }, j.isUndefined = function(n) {
        return n === void 0
    }, j.has = function(n, t) {
        return f.call(n, t)
    }, j.noConflict = function() {
        return n._ = t, this
    }, j.identity = function(n) {
        return n
    }, j.times = function(n, t, r) {
        for (var e = Array(Math.max(0, n)), u = 0; n > u; u++) e[u] = t.call(r, u);
        return e
    }, j.random = function(n, t) {
        return null == t && (t = n, n = 0), n + Math.floor(Math.random() * (t - n + 1))
    };
    var I = {
        escape: {
            "&": "&amp;",
            "<": "&lt;",
            ">": "&gt;",
            '"': "&quot;",
            "'": "&#x27;"
        }
    };
    I.unescape = j.invert(I.escape);
    var T = {
        escape: new RegExp("[" + j.keys(I.escape).join("") + "]", "g"),
        unescape: new RegExp("(" + j.keys(I.unescape).join("|") + ")", "g")
    };
    j.each(["escape", "unescape"], function(n) {
        j[n] = function(t) {
            return null == t ? "" : ("" + t).replace(T[n], function(t) {
                return I[n][t]
            })
        }
    }), j.result = function(n, t) {
        if (null == n) return void 0;
        var r = n[t];
        return j.isFunction(r) ? r.call(n) : r
    }, j.mixin = function(n) {
        A(j.functions(n), function(t) {
            var r = j[t] = n[t];
            j.prototype[t] = function() {
                var n = [this._wrapped];
                return a.apply(n, arguments), z.call(this, r.apply(j, n))
            }
        })
    };
    var N = 0;
    j.uniqueId = function(n) {
        var t = ++N + "";
        return n ? n + t : t
    }, j.templateSettings = {
        evaluate: /<%([\s\S]+?)%>/g,
        interpolate: /<%=([\s\S]+?)%>/g,
        escape: /<%-([\s\S]+?)%>/g
    };
    var q = /(.)^/,
        B = {
            "'": "'",
            "\\": "\\",
            "\r": "r",
            "\n": "n",
            "   ": "t",
            "\u2028": "u2028",
            "\u2029": "u2029"
        },
        D = /\\|'|\r|\n|\t|\u2028|\u2029/g;
    j.template = function(n, t, r) {
        var e;
        r = j.defaults({}, r, j.templateSettings);
        var u = new RegExp([(r.escape || q).source, (r.interpolate || q).source, (r.evaluate || q).source].join("|") + "|$", "g"),
            i = 0,
            a = "__p+='";
        n.replace(u, function(t, r, e, u, o) {
            return a += n.slice(i, o).replace(D, function(n) {
                return "\\" + B[n]
            }), r && (a += "'+\n((__t=(" + r + "))==null?'':_.escape(__t))+\n'"), e && (a += "'+\n((__t=(" + e + "))==null?'':__t)+\n'"), u && (a += "';\n" + u + "\n__p+='"), i = o + t.length, t
        }), a += "';\n", r.variable || (a = "with(obj||{}){\n" + a + "}\n"), a = "var __t,__p='',__j=Array.prototype.join," + "print=function(){__p+=__j.call(arguments,'');};\n" + a + "return __p;\n";
        try {
            e = new Function(r.variable || "obj", "_", a)
        } catch (o) {
            throw o.source = a, o
        }
        if (t) return e(t, j);
        var c = function(n) {
            return e.call(this, n, j)
        };
        return c.source = "function(" + (r.variable || "obj") + "){\n" + a + "}", c
    }, j.chain = function(n) {
        return j(n).chain()
    };
    var z = function(n) {
        return this._chain ? j(n).chain() : n
    };
    j.mixin(j), A(["pop", "push", "reverse", "shift", "sort", "splice", "unshift"], function(n) {
        var t = e[n];
        j.prototype[n] = function() {
            var r = this._wrapped;
            return t.apply(r, arguments), "shift" != n && "splice" != n || 0 !== r.length || delete r[0], z.call(this, r)
        }
    }), A(["concat", "join", "slice"], function(n) {
        var t = e[n];
        j.prototype[n] = function() {
            return z.call(this, t.apply(this._wrapped, arguments))
        }
    }), j.extend(j.prototype, {
        chain: function() {
            return this._chain = !0, this
        },
        value: function() {
            return this._wrapped
        }
    })
}).call(this);
/**
 * Cross platform js library for Hybrid Video clients.
 * @class eBase
 */
var eBase = (function(core) {
    /**
     * Config eBase parameters.
     *
     * @param {Object} config
     * @param {String} config.mac MAC address of terminal.
     * @param {String} config.ip IP address of termianl.
     * @param {String} config.terminalType Terminal type, like 'iPad', 'PC', etc.
     * @param {String} config.clientVersion Client version for upgradation, like '1.0.0'
     * @param {String} config.cnonce Encrypt key for MEM backend
     * @param {eBase.data.CADeviceInfo} [config.caDeviceInfo] CA device info.
     *
     * @param {String} [config.vodListProperties] Properties when vod list is queried.
     * @param {String} [config.channelListProperties] Properties when channel list is queried.
     * @param {String} [config.playbillListProperties] Properties when playbill list is queried.
     *
     * @param {String} config.edsUrl URL of EDS server.
     * @param {String} [config.socialUrl] URL of social server. (Active if set.)
     * @param {Number} [config.aggregateDuration=50] Aggregate duration (ms) of multiple request. 0 to disable.
     * @param {Object} [config.errorCodeMap] Map of internal code to union code.
     * @param {Number} [config.reminderLeadingTime=1] Leading time (min) before reminder is fired
     * @param {Number} [config.historyLimit=10] The limit of history record
     * @param {Boolean} [config.isGuest=false] Is current profile GUEST
     * @param {Number} [config.ajaxTimeout=5000] Ajax timeout length (ms)
     * @param {Boolean} [config.supportCors=false] Support CORS or not.
     *
     * @return {eBase}
     */
    core.configure = function(cfg) {
        if (cfg.errorCodeMap) {
            // Extend default errorCodeMap with configure one.
            core.utils.extend(core.config.errorCodeMap, cfg.errorCodeMap);
            delete cfg.errorCodeMap;
        }
        if (cfg.ignoredErrorMap) {
            // Extend default ignoredErrorMap with configure one.
            core.utils.extend(core.config.ignoredErrorMap, cfg.ignoredErrorMap);
            delete cfg.ignoredErrorMap;
        }
        _.extend(core.config, cfg);

        return core;
    };

    /**
     * Start eBase service.
     *
     * @return {eBase}
     */
    core.start = function() {
        return core;
    };

    return core;
}(eBase || {}));

/**
 * [jQuery Deferred-like](http://api.jquery.com/category/deferred-object/) Object for promise programming.
 * @class eBase.core.Deferred
 */
var eBase = (function(core) {
    var utils = core.utils = core.utils || {};

    var injectCors = function(client) {
        if (core.config.supportCors) {
            client.withCredentials = true;
        }
    };

    utils.ajax = function(req) {
        console.log("utils.ajax begin" + req.url);
        var retDfd = new Deferred(),
            client = new XMLHttpRequest(),
            response = {},
            handler = function() {
                if (this.readyState === 4) {
                    if (this.status === 200) {
                        if (req.dataType === 'json') {
                            try {
                                response = JSON.parse(this.responseText);
                            } catch (exception) {
                                retDfd.resolve({});
                            }
                            console.log("aa" + req.url);
                            retDfd.resolve(response);
                        } else {
                            console.log("bb" + this.responseText);
                            retDfd.resolve(this.responseText);
                        }
                    } else if (this.status === 0) {
                        retDfd.reject({
                            status: '504',
                            statusText: 'connection timeout'
                        });
                    } else {
                        retDfd.reject(this);
                    }
                }
            };

        injectCors(client);

        req.dataType = req.dataType || 'json';
        req.type = req.type || 'POST';
        req.type = req.type.toUpperCase();

        if (req.type === 'GET' && req.data) {
            req.url += '?' + _.map(JSON.parse(req.data), function(val, name) {
                return name + '=' + val;
            }).join('&');
        }

        client.open(req.type, req.url);
        client.timeout = req.timeout || core.config.ajaxTimeout || 0;
        client.onreadystatechange = handler;

        if (req.headers) {
            _.each(req.headers, function(val, name) {
                client.setRequestHeader(name, val);
            });
        }

        if (req.type === 'GET') {
            client.send();
        } else {
            client.send(req.data || '{}');
        }

        return retDfd;
    };

    return core;
}(eBase));
/**
 * Configurations for eBase library.
 * @class eBase.config
 * @singleton
 */
var eBase = (function(core) {
    core.config = {
        // Aggregate duration in ms (0 to disable)
        aggregateDuration: 50,
        // Error code map to convert inner error code to union code
        errorCodeMap: {},
        // Leading time before reminder is fired (minutes)
        reminderLeadingTime: 1,
        // The limit count of history recoreded
        historyLimit: 10,
        // Whether enable UTC time (0 for disable, 1 for enable)
        utcEnable: 1,
        // Whether domestic server
        domestic: false,
        // Ajax request timeout length
        ajaxTimeout: 30000,
        // Should eBase Core support CORS
        supportCors: false,

        failoverSize: 10
    };

    return core;
}(eBase));
/**
 * Utilities for eBase library.
 * @class eBase.utils
 * @singleton
 */
var eBase = (function(core) {
    var utils = core.utils = core.utils || {};

    /**
     * Format date string.
     *
     * @param {Date} date
     * @param {String} format like 'yyyyMMddHHmmss'
     */
    utils.dateToString = function(date, fmt) {
        var o = {
            'M+': date.getMonth() + 1,
            'd+': date.getDate(),
            'H+': date.getHours(),
            'm+': date.getMinutes(),
            's+': date.getSeconds(),
            'S': date.getMilliseconds()
        };

        if (/(y+)/.test(fmt)) {

            fmt = fmt.replace(RegExp.$1, (date.getFullYear() + '').substr(4 - RegExp.$1.length));
        }
        for (var k in o) {
            if (new RegExp('(' + k + ')').test(fmt)) {
                fmt = fmt.replace(RegExp.$1, (RegExp.$1.length === 1) ? (o[k]) : (('00' + o[k]).substr(('' + o[k]).length)));
            }
        }

        return fmt;
    };

    utils.stringToDate = function(dateString, format) {
        var year, month, day, hour, minute, second;

        switch (format) {
            case 'yyyyMMddHHmmss':
                year = dateString.substring(0, 4);
                month = dateString.substring(4, 6);

                if (month[0] === '0') {
                    month = parseInt(month[1], 10) - 1;
                } else {
                    month = parseInt(month, 10) - 1;
                }
                day = dateString.substring(6, 8);
                hour = dateString.substring(8, 10);
                minute = dateString.substring(10, 12);
                second = dateString.substring(12, 14);

                return new Date(year, month, day, hour, minute, second);
            case 'yyyy-MM-dd HH:mm:ss':
                year = dateString.substring(0, 4);
                month = dateString.substring(5, 7);
                if (month[0] === '0') {
                    month = parseInt(month[1], 10) - 1;
                } else {
                    month = parseInt(month, 10) - 1;
                }
                day = dateString.substring(8, 10);
                hour = dateString.substring(11, 13);
                minute = dateString.substring(14, 16);
                second = dateString.substring(17, 19);

                return new Date(year, month, day, hour, minute, second);
            default:
                return new Date();
        }
    };

    utils.utcStringToDate = function(dateString) {
        var year = dateString.substring(0, 4);
        var month = dateString.substring(4, 6);

        if (month[0] === '0') {
            month = parseInt(month[1], 10) - 1;
        } else {
            month = parseInt(month, 10) - 1;
        }

        var day = dateString.substring(6, 8);
        var hour = dateString.substring(8, 10);
        var minute = dateString.substring(10, 12);
        var second = dateString.substring(12, 14);

        return new Date(Date.UTC(year, month, day, hour, minute, second));
    };

    utils.dateToUTCString = function(date) {
        var year = date.getUTCFullYear();
        var month = date.getUTCMonth() + 1;
        var day = date.getUTCDate();
        var hour = date.getUTCHours();
        var minute = date.getUTCMinutes();
        var second = date.getUTCSeconds();

        if (Math.floor(day) < 10) {
            day = '0' + day;
        }
        if (Math.floor(month) < 10) {
            month = '0' + month;
        }
        if (Math.floor(hour) < 10) {
            hour = '0' + hour;
        }
        if (Math.floor(minute) < 10) {
            minute = '0' + minute;
        }
        if (Math.floor(second) < 10) {
            second = '0' + second;
        }

        return year + '' + month + '' + day + '' + hour + '' + minute + '' + second;
    };

    utils.isNull = function(param) {
        if(_.isUndefined(param) || _.isNull(param) || _.isNaN(param) || param === 'undefined' || param === 'null' || param === 'NaN' || param === '') {
            return true;
        }
        return false;
    };

    return core;
}(eBase));
/**
 * Event message bus
 *
 * Core Event (events from api)
 *
 * - CORE_AUTHENTICATE_SUCCESS: Subscriber logged in system.
 * - CORE_LOGOUT_SUCCESS: Subscriber logged out system.
 * - CORE_PROFILE_LOGIN_SUCCESS: Profile logged in system.
 * - CORE_ERROR_MESSAGE: The unified error message.
 * - CORE_SUBSCRIBE_VERSION_CHANGED: Subscribe relationship changed.
 * - CORE_BOOKMARK_VERSION_CHANGED: Backend bookmark version changed.
 * - CORE_CHANNEL_VERSION_CHANGED: Backend channel version changed.
 * - CORE_FAVORITE_VERSION_CHANGED: Backend favorite version changed.
 * - CORE_REMINDER_VERSION_CHANGED: Backend reminder version changed.
 * - CORE_BOOKMARK_CHANGE: User's bookmark changed.
 * - CORE_LOCK_CHANGED: User's lock changed.
 * - CORE_FAVORITE_CHANGED: User's favorite changed.
 * - CORE_REMINDER_CHANGED: User's reminder changed.
 * - CORE_LOCK_VERSION_CHANGED: User's lock changed.
 * - CORE_INVALID_USER: User's session is invalided.
 *
 * App Event (events from applications)
 *
 * - APP_ALL_DATA_READY: All applications in system is ready.
 * - APP_PROFILE_DATA_READY: Profile app is ready to use.
 * - APP_PROFILE_DATA_CHANGED: Profile data is changed.
 * - APP_REMINDER_ALARM: One reminder is alarmed.
 * - APP_REMINDER_DATA_READY: Reminder app is ready to use.
 * - APP_REMINDER_DATA_CHANGED: Reminder data is changed.
 * - APP_BOOKMARK_DATA_READY: Bookmark app is ready to use.
 * - APP_BOOKMARK_DATA_CHANGED: Bookmark data is changed.
 * - APP_CHANNEL_DATA_READY: Channel app is ready to use.
 * - APP_FAVORITE_DATA_READY: Favorite app is ready to use.
 * - APP_FAVORITE_DATA_CHANGED: Favorite data is changed.
 * - APP_LOCK_DATA_READY: Lock app is ready to use.
 * - APP_LOCK_DATA_CHANGED: Lock data is changed.
 * - APP_SUBSCRIBE_DATA_READY: Subscribe app is ready to use.
 * - APP_HISTORY_DATA_READY: History app is ready to use.
 * - APP_DROPBOX_DATA_READY: Dropbox app is ready to use.
 * @class eBase.event
 * @singleton
 */
var eBase = (function(core) {
    var eventEmitter = new EventEmitter();

    core.event = {
        /**
         * Adds a listener function to the specified event.
         * The listener will not be added if it is a duplicate.
         * If the listener returns true then it will be removed after it is called.
         * If you pass a regular expression as the event name then the listener will be added to all events that match it.
         * @param {String} evt - Name of the event to attach the listener to.
         * @param {Function} listener - Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
         * @return {Object} - Current instance of EventEmitter for chaining.
         */
        on: function(evt, listener) {
            return eventEmitter.addListener(evt, listener);
        },
        /**
         * Semi-alias of addListener. It will add a listener that will be
         * automatically removed after it's first execution.
         * @param {String} evt - Name of the event to attach the listener to.
         * @param {Function} listener - Method to be called when the event is emitted. If the function returns true then it will be removed after calling.
         * @return {Object} - Current instance of EventEmitter for chaining.
         */
        once: function(evt, listener) {
            return eventEmitter.addOnceListener(evt, listener);
        },
        /**
         * Removes a listener function from the specified event.
         * When passed a regular expression as the event name, it will remove the listener from all events that match it.
         * @param {String} evt - Name of the event to remove the listener from.
         * @param {Function} listener - Method to remove from the event.
         * return {Object} - Current instance of EventEmitter for chaining.
         */
        off: function(evt, listener) {
            return eventEmitter.removeListener(evt, listener);
        },
        /**
         * Emits an event of your choice.
         * When emitted, every listener attached to that event will be executed.
         * If you pass the optional argument array then those arguments will be passed to every listener upon execution.
         * Because it uses apply, your array of arguments will be passed as if you wrote them out separately.
         * So they will not arrive within the array on the other side, they will be separate.
         * You can also pass a regular expression to emit to all events that match it.
         * @param {String} evt - Name of the event to emit and execute listeners for.
         * @param {Object} arg - Optional arguments to be passed to each listener.
         * @return {Object} - Current instance of EventEmitter for chaining.
         **/
        trigger: function(evt, args) {
            return eventEmitter.emitEvent(evt, [{
                eventName: evt,
                data: args
            }]);
        }
    };

    return core;
}(eBase));
/**
 * @class eBase.i18n
 * @singleton
 * Internalizatize resources.
 */
var eBase = (function(core) {
    var currentCulture = 'en',
        resource = {
            en: {
                messages: {}
            }
        };

    core.i18n = {
        /**
         * Add string resouce for culture.
         * @param {String} culture 'en'/'zh'
         * @param {Object} info
         * @param {Object} info.messages String resource
         *
         *        eBase.i18n.addCultureInfo('en', {
         *            messages: {
         *                'welcome': 'Welcome to eBase Core!'
         *            }
         *        });
         */
        addCultureInfo: function(culture, info) {
            if (!resource[culture]) {
                resource[culture] = {
                    messages: {}
                };
            }
            _.extend(resource[culture].messages, info.messages);
        },
        /**
         * Set current culture.
         * @param  {String} culture 'en'/'zh'
         */
        culture: function(culture) {
            currentCulture = culture;
        },
        /**
         * Localize string according to current culture.
         * @param  {String} key Key to string resource.
         * @return {String} Localized string resource.
         */
        localize: function(key) {
            if (resource[currentCulture] && resource[currentCulture].messages && resource[currentCulture].messages[key]) {
                return resource[currentCulture].messages[key];
            } else {
                return null;
            }
        }
    };

    return core;
}(eBase));
/**
 * @class eBase.utils
 */
var eBase = (function(core) {
    var utils = core.utils = core.utils || {};

    var dateFormat = function(o) {
        return o.getFullYear() + '-' + (o.getMonth() + 1) + '-' + o.getDate() + 'T' + o.getHours() + ':' + o.getMinutes() + ':' + o.getSeconds();
    };

    var toXml = function(o, sb) {
        switch (typeof o) {
            case 'object':
                for (var p in o) {
                    if (o.hasOwnProperty(p)) {
                        var value = o[p];
                        var isArray = _.isArray(value);

                        for (var i = 0, l = isArray ? value.length : 1; i < l; i++) {
                            var v = isArray ? value[i] : value;

                            sb[sb.length] = '<';
                            sb[sb.length] = p;
                            sb[sb.length] = '>';

                            toXml(v, sb);

                            sb[sb.length] = '</';
                            sb[sb.length] = p;
                            sb[sb.length] = '>';
                        }
                    }
                }
                break;
            case 'string':
                sb[sb.length] = o;
                break;
            case 'date':
                sb[sb.length] = dateFormat(o);
                break;
            default:
                sb[sb.length] = o;
                break;
        }
    };
    /**
     * Convert json to xml string.
     *
     * @param {Object} json JSON object.
     *
     * @return {String} xml XML string.
     */
    utils.jsonToXml = function(obj) {
        var sb = [];

        toXml(obj, sb);

        return sb.join('');
    };

    return core;
}(eBase));
/**
 * eBase log utility.
 * @class eBase
 * @singleton
 */
var eBase = (function(core) {
    /**
     * Level of log to print.
     *
     * - debug
     * - log
     * - error
     * @property logLevel
     * @type {String}
     */
    core.logLevel = 'log';

    /**
     * Print debug info.
     * @param {String} info debug info
     */
    core.debug = function() {
        if (core.logLevel === 'debug') {
            var args = Array.prototype.slice.call(arguments);

            args.unshift('eBase[debug]:');

            console.debug.apply(console, args);
        }
    };

    /**
     * Print log info.
     * @param {String} info log info
     */
    core.log = function() {
        if (core.logLevel === 'debug' || core.logLevel === 'log') {
            var args = Array.prototype.slice.call(arguments);

            args.unshift('eBase[log]:');

            console.log.apply(console, args);
        }
    };

    /**
     * Print error info.
     * @param {String} info error info
     */
    core.error = function() {
        if (core.logLevel === 'debug' || core.logLevel === 'log' || core.logLevel === 'error') {
            var args = Array.prototype.slice.call(arguments);

            args.unshift('eBase[error]:');

            console.error.apply(console, args);
        }
    };

    return core;
}(eBase));
/**
 * Utilities for eBase library.
 * @class eBase.utils
 * @singleton
 */
var eBase = (function(core) {
    var utils = core.utils = core.utils || {};


    /**
     * Change property name.
     * @param {Object} req
     * @param {String} oldName
     * @param {String} newName
     */
    utils.rename = function(req, oldName, newName) {
        if (!req) {
            return;
        }

        if (_.has(req, oldName)) {
            req[newName] = req[oldName];
            delete req[oldName];
        }
    };

    utils.rstr2hex = rstr2hex;
    utils.des = des;
    utils.aes = Aes;
    utils.base64 = Base64;

    utils.sha256 = function(plainText) {
        var hash = CryptoJS.SHA256(plainText);
        return hash.toString(CryptoJS.enc.Hex);
    };
    utils.aesEncrypt = function(plainKey) {
        var key = CryptoJS.enc.Hex.parse(eBase.session.get('signatureKey').substring(0, 32));
        // 16 bytes.
        var iv = CryptoJS.lib.WordArray.random(16); //CryptoJS.enc.Hex.parse(eBase.session.get('signatureKey').substring(0, 32));
        return CryptoJS.AES.encrypt(plainKey, key, {
            iv: iv,
            mode: CryptoJS.mode.ECB
        }).toString();
    };

    utils.aesDecrypt = function(data) {
        var key = CryptoJS.enc.Hex.parse(eBase.session.get('signatureKey').substring(0, 32));
        var iv = CryptoJS.lib.WordArray.random(16); //CryptoJS.enc.Hex.parse(eBase.session.get('signatureKey').substring(0, 32));
        return CryptoJS.AES.decrypt(data, key, {
            iv: iv,
            mode: CryptoJS.mode.ECB
        }).toString(CryptoJS.enc.Utf8);

    };

    /**
     * Huawei MD5 encryption
     * @param {String} str String to encrypt
     * @return {String}
     */
    utils.md5 = function(str) {
        var encrypt = hexmd5(str + '99991231');

        return _.map(_.compact(encrypt.split(/(.{2})/g)), function(c) {
            return c.replace(/^0/, '');
        }).join('').substr(0, 8);
    };

    /**
     * Add paginating parameters to req
     * @param {Object} req The request parameter
     * @param {String} [type='jsonapi'] API of this method
     */
    utils.paginate = function(req, type) {
        req = req || {};

        var offset = req.offset || 0;
        var count = req.count || 1000;

        switch (type) {
            case 'openapi':
                req['start-index'] = offset + 1;
                req['max-results'] = count;
                delete req.offset;
                delete req.count;
                break;

            case 'social':
                if (_.has(req, 'offset')) {
                    req.startNum = offset;
                    delete req.offset;
                }

                if (_.has(req, 'count')) {
                    req.length = count;
                    delete req.count;
                }

                if (req.length) {
                    // Convert length to string so _.map won't be wrong.
                    req.length += '';
                }
                break;

            default:
                req.offset = offset;
                req.count = count;
        }

        return req;
    };

    /**
     * Simple string template function.
     * @param {String} template The string template
     * @param {Object} [parameters] Parameters for template.
     */
    utils.template = function(str) {
        str = core.i18n.localize(str) || str;

        if (/{.*}/.test(str)) {
            var args = _.toArray(arguments).slice(1);
            _.each(_.isObject(args[0]) ? args[0] : args, function(arg, index) {
                str = str.replace(new RegExp('\\{' + index + '\\}', 'g'), arg);
            });
        }

        return str;
    };

    /**
     * Parse picture in multiple sizes.
     * @param {String} picture string
     * @return {Object} picture
     * @return {String} picture.xl Picture in XL size
     * @return {String} picture.l Picture in L size
     * @return {String} picture.m Picture in M size
     * @return {String} picture.s Picture in S size
     * @return {String} picture.xs Picture in XS size
     * @return {String} picture[0] Picture in normal size
     */
    utils.parsePicture = function(pictureStr) {
        var picture = {},
            list = (pictureStr || '').split(',');

        _.each(list, function(url) {
            var matchs = /_(\d*)_(XL|L|M|S|XS|0)\.\w{2,5}$/i.exec(url);
            if (matchs) {
                var sizeKey = matchs[2].toLowerCase();

                picture[sizeKey] = picture[sizeKey] || [];
                picture[sizeKey].push(url);
            }
        });

        picture[0] = picture[0] || list[0] || '';

        return picture;
    };

    utils.extend = function(target, source) {
        for (var prop in source) {
            if (prop in target && typeof(target[prop]) === 'object' && typeof(source[prop]) === 'object') {
                utils.extend(target[prop], source[prop]);
            } else {
                target[prop] = source[prop];
            }
        }

        return target;
    };

    return core;
}(eBase));
/**
 * @class eBase.session
 * Storage for user
 * @singleton
 */
var eBase = (function(core) {
    var utils = core.utils,
        json = {};

    core.session = {
        /**
         * obtain value by key
         *
         * @method get
         * @param key {String} key
         *
         * @return {Object}
         */
        get: function(key) {
            if (!_.has(json, key)) {
                var valueStr = sessionStorage.getItem(key) || localStorage.getItem(key);
                if (valueStr) {
                    json[key] = JSON.parse(utils.aes.Ctr.decrypt(valueStr, key, 128));
                }
            }

            return json[key];
        },

        /**
         * save value to session
         *
         * @method put
         * @param key {String} Key
         * @param value {Object} value
         * @param [isPersistence=false] {Boolean} whether to store value to local
         *
         */
        put: function(key, value, isPersistence) {
            isPersistence = isPersistence || false;

            if (_.isNull(value) || _.isUndefined(value) || _.isNaN(value)) {
                this.remove(key);

                return;
            }

            json[key] = value;

            var encryptValue = utils.aes.Ctr.encrypt(JSON.stringify(value), key, 128);

            if (isPersistence) {
                localStorage.setItem(key, encryptValue);
            } else {
                sessionStorage.setItem(key, encryptValue);
            }
        },

        /**
         * clear values
         *
         * @method clear
         * @param [isPersistent=false] {Boolean} if true, will clear all values include stored in local
         *
         */
        clear: function(isAlsoClearPersistent) {
            json = {};
            sessionStorage.clear();
            if (isAlsoClearPersistent) {
                localStorage.clear();
            }
        },

        /**
         * remove value by key
         *
         * @method remove
         * @param key {String} key
         */
        remove: function(key) {
            delete json[key];
            sessionStorage.removeItem(key);
            localStorage.removeItem(key);
        }
    };

    return core;
}(eBase));
/**
 * eBase's apps are heavily using cache. Every app will send app ready event when cache is ready.
 * But we want user only be care about one 'APP_ALL_DATA_READY' event, which will be triggered when every app is ready.
 *
 * @class eBase.appReadyTrigger
 * @singleton
 * @private
 */
var eBase = (function(core) {
    var event = core.event,
        eventList = [];

    core.appReadyTrigger = {
        /**
         * Register application's data ready event.
         *
         * @param {String} eventName Application's data ready event.
         *
         * @return {eBase.appReadyTrigger}
         */
        register: function(eventName) {
            eventList.push(eventName);
        },
        resetData: function() {
            // After all event are received, event will be triggered
            eBase.session.put('isDataReady', false, false);
            var delayTrigger = _.after(eventList.length, function() {
                event.trigger('APP_ALL_DATA_READY');
            });

            _.each(eventList, function(eventName) {
                event.once(eventName, delayTrigger);
            });
        }
    };

    return core;
})(eBase);
/**
 * Common error handler.
 *
 * @class eBase.common.error
 * @singleton
 * @private
 */
var eBase = (function(core) {
    var event = core.event,
        config = core.config,
        DEFAULT_KEY = '',
        RELOAD_RETCODES = ['-2', '-3'];

    var queryUnionCode = function(categoryName, url, retCode) {
        var methodName,
            unionCode,
            category;

        if (categoryName === 'iptv') {
            methodName = /\/JSON\/(\w*)/.exec(url)[1];
        }

        category = config.errorCodeMap[categoryName];

        if (category) {
            if (category[methodName]) {
                unionCode = category[methodName][retCode] || category[methodName][DEFAULT_KEY];
            }

            unionCode = unionCode || category[retCode] || category[DEFAULT_KEY];
        }

        return unionCode || config.errorCodeMap[retCode] || retCode;
    };

    // Convert internal error code to union error code.
    var triggerErrorMessage = function(categoryName, args) {
        var retCode = args.resp.retcode || args.resp.resultCode;

        if (categoryName === 'iptv' && retCode === '-1') {
            retCode = args.resp.errorCode || '-1';
        }

        var unionCode = queryUnionCode(categoryName, args.req.url, retCode),
            message = eBase.i18n.localize(unionCode);

        if (message) {
            event.trigger('CORE_ERROR_MESSAGE', _.extend({
                code: unionCode || retCode,
                unionCode: unionCode,
                reload: _.contains(RELOAD_RETCODES, retCode)
            }, args, message || {}));
        }
    };
    var isneedshowmessage = function(error) {
        if (error.methodName == 'Authenticate') {
            return false;
        } else if (error.methodName == 'AuthorizeAndPlay' && error.args.req && error.args.req.isnoshowerror) {
            return false;
        }
        return true;
    };

    var triggerCoreErrorMessage = function(error) {
        var uniMessage;
        var methodMessages = config.errorCodeMap.iptv[error.methodName];
        var ignoredError = config.ignoredErrorMap.iptv[error.methodName];

        if (ignoredError && (ignoredError === 'x' || ignoredError.indexOf(error.errorCode) >= 0)) {
            return;
        }

        if (!methodMessages) {
            error.methodName = 'Response';
            methodMessages = config.errorCodeMap.iptv.Response;
        }

        // Find uniMessage
        if (methodMessages.indexOf(error.errorCode) >= 0) {
            uniMessage = error.methodName + '.' + error.errorCode;
            uniMessage = core.i18n.localize(uniMessage);
        }
        if (!uniMessage) {
            uniMessage = error.methodName + '.default';
            uniMessage = _.clone(core.i18n.localize(uniMessage));
            //uniMessage.m += error.errorCode;
        }
        if (!uniMessage) {
            uniMessage = core.i18n.localize('http.default');
        }

        if (isneedshowmessage(error)) { //&&error.args.resp&&error.args.resp.isFirstLogin=="1")) {
            event.trigger('CORE_ERROR_MESSAGE', _.extend({
                code: error.errorCode,
                unionCode: error.uniCode,
                reload: false
            }, error.args, {
                description: uniMessage.m,
                suggestion: uniMessage.s
            } || {}));
        }
    };


    var triggerSessionErrorMessage = function(error) {
        var methodMessages = config.errorCodeMap.iptv.Response,
            uniMessage;

        if (methodMessages.indexOf(error.uniCode) >= 0) {
            uniMessage = 'Response.' + error.uniCode;
        } else {
            uniMessage = 'Response.' + error.errorCode;
        }

        uniMessage = core.i18n.localize(uniMessage);

        event.trigger('CORE_ERROR_MESSAGE', _.extend({
            code: error.errorCode,
            unionCode: error.uniCode,
            reload: true
        }, error.args, {
            description: uniMessage.m,
            suggestion: uniMessage.s
        } || {}));
    };

    var triggerIPTVMessage = function(args) {
        var methodName = /\/JSON\/(\w*)/.exec(args.req.url)[1];
        var errorCode = args.resp.errorCode || args.resp.retcode;

        // Filter myself
        if (methodName !== 'QueryErrorCode') {
            /*if (args.resp.retcode === '-1') {
                errorCode = args.resp.errorCode || '-1';
            } else*/
            if (args.resp.retcode === '-2' || args.resp.retcode === '-3') {
                // triggerSessionErrorMessage({
                //     errorCode: errorCode,
                //     uniCode: args.resp.errorCode || errorCode,
                //     args: args
                // });

                //errorCode = args.resp.errorCode || args.resp.retcode;

                // Query uniCode
                core.iptv.getErrorCode({
                    scenarioId: methodName,
                    interCode: errorCode
                }).always(function(resp) {

                    // 新UI：queryErrorCode不返回错误码或者为空，展示内码
                    var uniCode = args.resp.errorCode || args.resp.retcode;
                    if (resp.retcode === '0' && resp.errorCode) {
                        uniCode = resp.errorCode;
                    }

                    triggerSessionErrorMessage({
                        errorCode: errorCode,
                        uniCode: uniCode,
                        args: args
                    });
                });
                return;
            }
            var retcode = args.resp.retcode === '-1' ? args.resp.errorCode || '-1' : args.resp.retcode;
            if (config.isGuest) {
                triggerCoreErrorMessage({
                    errorCode: retcode,
                    uniCode: retcode,
                    methodName: methodName,
                    args: args
                });
            } else {
                isAlertErrorMessage({
                    args: args,
                    methodName: methodName,
                    errorCode: errorCode,
                    retcode : retcode
                });
            }
        }
    };

    var isAlertErrorMessage = function(params){
        var errorCode = params.errorCode;
        var args = params.args;
        var methodName = params.methodName;
        var retcode = params.retcode;
        var now = new Date().getTime();
        var pressKeyTimer = localStorage.getItem('pressKeyTimer');
        if(now - pressKeyTimer < gConfig.pressKeyTime)
        {
                // Query uniCode
            core.iptv.getErrorCode({
                scenarioId: methodName,
                interCode: errorCode
            }).always(function(resp) {
                // 新UI：queryErrorCode不返回错误码或者为空，展示内码
                var uniCode = args.resp.errorCode || args.resp.retcode;
                if (resp.retcode === '0' && resp.errorCode) {
                    uniCode = resp.errorCode;
                }

                triggerCoreErrorMessage({
                    errorCode: retcode,
                    //uniCode: resp.errorCode || errorCode,
                    uniCode: uniCode,
                    methodName: methodName,
                    args: args
                });
            });
        }
        else
        {
            return;
        }
    };

    var triggerHTTPMessage = function(args) {
        var httpCode = args.resp.resultCode;
        var uniCode = '145' + httpCode.replace('http_', '');
        var description = '',suggestion = '';
        if(httpCode === 'http_503'){
            description = eBase.i18n.localize('http.503').m;
            suggestion = eBase.i18n.localize('http.503').s;
        }else {
            description = eBase.i18n.localize('http.default').m;
            suggestion = eBase.i18n.localize('http.default').s;
        }
        event.trigger('CORE_ERROR_MESSAGE', _.extend({
            code: httpCode,
            unionCode: uniCode,
            reload: args.resp.reload
        }, args, {
            description: description,
            suggestion: suggestion
        } || {}));
    };
    
    core.event.on('IPTV_REQUEST_FAIL', function(event) {
        console.debug("=========resultCode = "+ event.data.resp.resultCode);
        if (event.data.resp.resultCode && event.data.resp.resultCode.indexOf('http') === 0) {
            var now = new Date().getTime();
            var pressKeyTimer = localStorage.getItem('pressKeyTimer');
            console.debug("=========now = "+ now + "=pressKeyTimer==" + pressKeyTimer + "==gConfig.pressKeyTime==" + gConfig.pressKeyTime);
            if(now - pressKeyTimer <= gConfig.pressKeyTime || !pressKeyTimer){
                triggerHTTPMessage(event.data);
            }
        } else {
            triggerIPTVMessage(event.data);
        }
    });

    var triggerSocialMessage = function(args) {
        var errorCode = args.resp.resultCode;
        var methodMessages = config.errorCodeMap.social;
        var uniMessage;
        var ignoredError = config.ignoredErrorMap.social;

        if (ignoredError && (ignoredError === 'x' || ignoredError.indexOf(errorCode) >= 0)) {
            return;
        }


        if (methodMessages.indexOf(errorCode) >= 0) {
            uniMessage = core.i18n.localize('social.' + errorCode);
        }
        if (!uniMessage) {
            uniMessage = _.clone(core.i18n.localize('social.default'));
            //uniMessage.m += errorCode;
        }

        event.trigger('CORE_ERROR_MESSAGE', _.extend({
            code: errorCode,
            unionCode: errorCode,
            reload: false
        }, args, {
            description: uniMessage.m,
            suggestion: uniMessage.s
        } || {}));
    };

    core.event.on('SOCIAL_REQUEST_FAIL', function(event) {
        triggerSocialMessage(event.data);
    });

    core.event.on('DROPBOX_REQUEST_FAIL', function(event) {
        triggerErrorMessage('dropbox', event.data);
    });

    core.event.on('RICE_REQUEST_FAIL', function(event) {
        triggerErrorMessage('rice', event.data);
    });

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;


    iptv.getErrorCode = function(req) {
        return utils.forceJsonAjax({
            url: '/EPG/JSON/QueryErrorCode?scenarioId='+req.scenarioId+'&interCode='+req.interCode,
            data: JSON.stringify(req)
        });
    };

    /**
     * This interface is used to query information about all channels.
     * The MEM filters channels based on the regional node, area, and domain to which a subscriber belongs.
     * Channels unavailable to a profile are not filtered out.
     * @param {Object} req
     * @param {Number} [req.domain] Content domain type.
     *
     * - 0: OTT
     * - 1: Mobile
     * - 2: IPTV
     * If this parameter is not carried, switch to the domain to which the login terminal belongs by default.
     * @param {String} [req.channelNamespace] Naming space used for querying a channel.
     * If this parameter is not specified, the MEM uses the defaultChannelNS parameter during subscriber authentication.
     * @param {String} [req.properties] The properties in returned {eBase.data.Channel}. Example: 'id,type,name'
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Channel}
     */
    iptv.allChannel = function(req) {
        if (req && req.properties) {
            req.properties = [{
                name: 'channel',
                include: req.properties
            }];
        }

        return utils.jsonAjax({
            url: '/EPG/JSON/AllChannel',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'channellist', 'list');
        });
    };

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * This interface is used to query information about mashup weatherInfo.
     */
    iptv.getWeatherInfo = function(req) {
        return utils.jsonAjaxMashup({
            url: '/WeatherServlet',
            data: req
        }).done(function(resp) {

        });
    };

    iptv.getFlightInfo = function(req) {
        return utils.jsonAjaxMashup({
            url: '/FlightServlet',
            data: req
        }).done(function(resp) {

        });
    };

    iptv.getNewsCategory = function(req) {
        return utils.jsonAjaxMashup({
            url: '/getNewsCategory',
            data: req
        });
    };

    iptv.getNewsByCategory = function(req) {
        return utils.jsonAjaxMashup({
            url: '/getNewsByCategory',
            data: req
        });
    };

    iptv.getNewsContext = function(req) {
        return utils.jsonAjaxMashup({
            url: '/getNewsContext',
            data: req
        });
    };

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        config = core.config,
        utils = core.utils,
        event = core.event,
        session = core.session;

    // Format system's current time to GMT string.
    var getTimeZoneString = function() {
        var timeStr = utils.dateToUTCString(new Date(), 'yyyyMMddHHmmss');

        if (timeStr) {

            return timeStr;
        } else {
            return null;
        }
    };

    // Huawei's authenticator algorithm
    var calculateAuthenticator = function(o) {
        var data = [_.random(99999999), o.encryToken, o.userId, o.mac, o.ip, o.mac, 'Reserved', 'OTT'].join('$'),
            key = o.password;

        var authenticator = utils.rstr2hex(utils.des(key, data, 1, 0, null, 1));
        while(authenticator.length % 8 !== 0){
            authenticator += "0";
        }
        return authenticator;
    };

    var injectFailover = function(authenticateReq, req) {
        var failoverList = eBase.session.get('core.failover') || [];
        var lastInfo = _.findWhere(failoverList, {
            userId: req.userId
        });

        if (lastInfo) {
            _.extend(authenticateReq, _.omit(lastInfo, 'userId'));
        }
    };

    var saveFailover = function(req, resp) {
        var failoverList = eBase.session.get('core.failover') || [];
        var lastInfo = _.findWhere(failoverList, {
            userId: req.userId
        });
        var json = _.pick(resp, 'areaid', 'templatename', 'usergroup', 'packageid', 'subnetId');

        json.userId = req.userId;

        if (lastInfo) {
            _.extend(lastInfo, json);
        } else {
            while (failoverList.length >= config.failoverSize) {
                failoverList.shift();
            }
            failoverList.push(json);
        }

        eBase.session.put('core.failover', failoverList, true);
    };
    /**AntiTamper start */
    var saveAntiTamperURI = function(antiTamperURI) {
        eBase.session.put('antiTamperURI', antiTamperURI);
    };
    /**AntiTamper end */

    /**
     * A client uses this interface to authenticate subscribers.
     *
     * eBase.config should contain some parameters like
     * {
     *     mac: '00:11:22:33:44:55',
     *     ip: '10.138.235.75',
     *     cnonce: '1234567',
     *     caDeviceInfo: {}
     * }
     *
     * @param {Object} req
     * @param {String} req.userId Subscriber ID.
     * @param {String} req.password Password.
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Profile[]} return.list List of profile.
     * @return {String} return.retcode Error code. For details, see sections of error codes of the object.
     * @return {String} return.retmsg Description of an error code.
     * @return {String} return.upgradedomain Domain name of the upgrade server. Format: http://IP or domain name:port/
     * @return {String} return.upgradedomainbackup Domain name of the backup upgrade server. Format:http://IP or domain name:port/
     * @return {String} return.mgmtdomain Domain name of the terminal management server. The response contains this parameter when the authentication is successful.
     * @return {String} return.mgmtdomainbackup Domain name of the backup network management server. The response contains this parameter when the authentication is successful.
     * @return {String} return.ntpdomain Domain name of the NTP server. The response contains this parameter when the authentication is successful.
     * @return {String} return.ntpdomainbackup Domain name of the backup NTP server. The response contains this parameter when the authentication is successful.
     * @return {String} return.areaid Area ID of a subscriber.
     * @return {String} return.templatename Subscriber template, which needs to be recorded in terminals after successful subscriber authentication.
     * @return {String} return.usergroup User group ID.
     * @return {String} return.packageid Bundle ID.
     * @return {String} return.epgurl EPG server URL.
     * @return {String} return.transportprotocol When subscribers watch unicast programs, terminals receive media streams using the protocol specified by TransportProtocol. Terminals that do not support TransportProtocol use the factory settings. The response contains this parameter when the authentication is successful.
     *
     * - 0: TCP
     * - 1: UDP
     * - 2: RTP over TCP
     * - 3: RTP over UDP
     * @return {Object} return.ca CAInfo The IPTV system can connect to the CA provided by different vendors. The CA vendors deliver different parameters to terminals.
     * @return {String} return.sessionid Session ID.
飦?  * For terminals that meet the HTTP standard, the value can be obtained from the protocol header in the response.
飦?  * For terminals that do not meet the HTTP standard, the value of this parameter is used as the session ID.
     * @return {String} return.currenttime Time on the EPG server.
     * @return {Array} return.parameters Subscriber-related system parameters.
     * @return {Array} return.users List of profiles a subscriber has.
     * @return {String} return.isTriplePlay Indicates whether a subscriber is a TriplePlay subscriber.
     *
     * - 0: no
     * - 1: yes
     * @return {String} return.fccIP IP address of the Fast Channel Change (FCC) server.
     * @return {String} return.defaultProfile ID of the default profile using the device. If this attribute is left blank, no default profile exists and the system returns the profile list for subscriber to select one profile.
     * @return {String} return.STBRCUsubscribed Indicates whether a subscriber has subscribed to the iPad remote controller service.
     *
     * - 0: no
     * - 1: yes
     * @return {String} return.bandwidth Bandwidth available to a subscriber.
     * @return {String} return.isFirstLogin Indicates whether a subscriber has logged in to the IPTV system for the first time if userID is set to a subscriber ID or a device ID.
     *
飦?  * - 0: no
飦?  * - 1: yes
     * @return {String} return.dsmdomain Domain name of the digital shopping mall (DSM) server.
     * @return {String} return.dsmdomainbackup Domain name of the backup DSM server.
     * @return {String} return.timezone Time zone to which a terminal belongs.
     * @return {String} return.dstTime DST of the time zone to which the terminal belongs.
     * @return {String} return.subnetId ID of the regional node carrier to which the subscriber belongs.
     * @return {String} return.userID Subscriber ID.
     * @return {String} return.loginOccasion Subscriber login status.
     *
     * - 0: normal
     * - 1: login request bypassed. If the bypass is supported by the system and the database or the ACS is abnormal, the login request bypasses the EPG server.
     * @return {String} return.deviceId Logic device ID of the physical device used by the subscriber.
     * @return {String} return.profileId If the input parameter userType is 1, the profileId value corresponding to the subscriber is returned.
     * @return {String} return.loginName Login ID of a profile.
     * @return {String} return.needSignEULA Indicates whether EULA information must be signed.
     *
     * - 0: no
     * - 1: yes
     * @return {String} return.lockedNum Maximum number of failed consecutive login attempts. The system locks the subscriber account if the number of failed consecutive login attempts reaches the maximum. If a site does not require this parameter, the value 0 will be returned.
     * @return {String} return.waitUnLockTime Time for waiting for unlocking.
     * @return {String} return.remainLockedNum Number of remaining login attempts.
     * @return {String} return.pwdResetTime Time when a password is reset. The format is yyyyMMddHHmmss.
     * @return {Array} return.triggers The trigger is delivered upon successful authentication and first login to the PlayReady client.
     */
    iptv.authenticate = function(req) {
        var mac = config.mac || '00:00:00:00:00:00',
            ip = config.ip || '0.0.0.0',
            stbModel = core.stb.read('STB_model'),
            chipID = core.stb.read('ChipSeriaNumber'),
            authenticator = calculateAuthenticator({
                encryptToken: config.encryptToken,
                userId: req.userId,
                mac: mac,
                ip: ip,
                password: req.password
            }),
            authenticateReq = _.extend({
                userid: req.userId,
                authenticator: authenticator,
                terminalid: mac,
                mac: mac,
                terminaltype: req.terminalType,
                terminalVendor: req.terminalVendor,
                utcEnable: config.utcEnable,
                timezone: req.timeZone
            }, req);

        if (config.cnonce && config.isSupportconce) {
            authenticateReq.cnonce = config.cnonce;
        } //utils.md5("" + (Math.random() * 1234567890));
        //Q11盒子传入CHIPID需求
        if('Q11' === stbModel && chipID){
            var caDeviceTypeNum;
			if(gConfig.isIPTV) {
				caDeviceTypeNum = 3;
			} else {
				caDeviceTypeNum = 4;
			}
            authenticateReq.caDeviceInfo = [{
                caDeviceType: caDeviceTypeNum,
                caDeviceId: chipID
            }];
        }else if(config.caDeviceInfo) {
            authenticateReq.caDeviceInfo = config.caDeviceInfo;
        }

        injectFailover(authenticateReq, req);

        return utils.forceJsonAjax({
            url: '/EPG/JSON/Authenticate',
            data: JSON.stringify(authenticateReq)
        }).done(function(resp) {
            // TODO: Need unionify session content
            session.put('userId', req.userId);
            //session.put('core.isAuthenticated', true);
            utils.rename(resp, 'profiles', 'list');
            saveFailover(req, resp);
            /**AntiTamper start */
            // save the antiTamperURI
            saveAntiTamperURI(resp.antiTamperURI);
            // default the cccasion status
            console.log("plainKey authenticate start");
            var plainKey = '99991231' + req.userId + config.encryptToken + authenticateReq.cnonce;
            if (resp.loginOccasion === '0') {
                // normal situation
                plainKey = req.password + plainKey;
            }
            eBase.stb.write('loginOccasion', resp.loginOccasion);

            // put the signatureKey into the session
            eBase.session.put('signatureKey', utils.sha256(plainKey));
            console.log("plainKey" + plainKey);
            console.log("signatureKey" + utils.sha256(plainKey));
            /**AntiTamper end */
            event.trigger('CORE_AUTHENTICATE_SUCCESS', resp);
        });
    };

    return core;
}(eBase));



/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to authenticate services before playing content or using services.
     * The client uses this interface to check whether a subscriber has rights to use content or services.
     * If the authentication succeeds, the subscriber can use the content or services.
     * If the authentication fails, the EPG server sends back the list of subscribable products, and the client provides guidance for the subscriber to subscribe to products online based on this list. For details, see the output parameters monthlist and timelist of this interface.
     *
     * @param {Object} req
     * @param {String} [req.contentId] Content ID.
     * @param {String} [req.contentType] contentType Content type.
     *
     * - VOD
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - CHANNEL
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - SUBJECT
     * - PROGRAM
     * @param {String} req.businessType Service type.
     *
     * - 0: mixed
     * - 1: VOD
     * - 2: live TV
     * - 3: PPV
     * - 4: TSTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 7: VAS
     * - 8: nPVR
     * - 9: local TSTV
     * - 10: caller ID
     * - 11: DVB
     * When bussinesstype is set to 10 or 11, other parameters are not required.
     * @param {String} [req.playType] Program type.
     *
     * - 1: VOD
     * - 2: live TV
     * - 4: Catch-up TV
     * - 5: clip
     * - 6: bookmark
     * - 7: TSTV
     * - 8: non-PVR programs
     * - 9: PVR programs
     * - 10: VAS
     * @param {String} [req.categoryId] ID of the category where content belongs to.
     * @param {String} [req.fatherVodId] The options are as follows:
     * Set the parameter to the series ID if the content is an episode. The subscription to both series and episode is authenticated.
     * -2: indicates only the subscription to an episode is authenticated.
     * -3: indicates only the subscription to series to which an episode belongs is authenticated.
     * @param {String} [req.islocalAction=0] Indicates whether local authentication is performed.
     * Local authentication refers to the authentication on the EPG server based on the cached subscription relationship. Local authentication can be used to reduce the interaction between the EPG server and MEM on occasions when certain operations may undermine the system performance, for example, displaying the product information on the VOD details page.
     *
     * - 0: no
     * - 1: yes
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Authentication return code.
     * @return {String} return.retmsg Response message.
     * @return {String} return.point Number of IPTV bonus points that are available.
     * @return {String} return.telecompoint Number of carrier bonus points that are available.
     * @return {eBase.data.Product} return.monthlist Returned information about the subscribable monthly package products after an authentication failure.
     * @return {eBase.data.Product} return.timelist Returned information about the subscribable times-based products after an authentication failure.
     * @return {eBase.data.Product} return.preorderlist Returned information about the subscribed products that have not been activated after an authentication failure.
     * @return {String} return.productId Product ID used for content authentication.
     * @return {String} return.restriction Restrictions of a product. The parameter is returned only when the authentication is successful.
     * @return {String} return.inCondition Applicable conditions of a product. The parameter is returned only when the authentication is successful.
     */
    iptv.authorize = function(req) {
        utils.rename(req, 'productId', 'productid');
        utils.rename(req, 'continueType', 'continuetype');
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'businessType', 'businesstype');
        utils.rename(req, 'playType', 'playtype');

        return utils.jsonAjax({
            url: '/EPG/JSON/Authorization',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to authenticate services before playing content or using services.
     * The client uses this interface to check whether a subscriber has rights to use content or services.
     * If the authentication succeeds, the subscriber can use the content or services.
     * If the authentication fails, the EPG server sends back the list of subscribable products, and the client provides guidance for the subscriber to subscribe to products online based on this list. For details, see the output parameters monthlist and timelist of this interface.
     *
     * @param {Object} req
     * @param {String} [req.contentId] Content ID.
     * @param {String} [req.contentType] contentType Content type.
     *
     * - VOD
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - CHANNEL
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - SUBJECT
     * - PROGRAM
     * @param {String} req.businessType Service type.
     *
     * - 0: mixed
     * - 1: VOD
     * - 2: live TV
     * - 3: PPV
     * - 4: TSTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 7: VAS
     * - 8: nPVR
     * - 9: local TSTV
     * - 10: caller ID
     * - 11: DVB
     * When bussinesstype is set to 10 or 11, other parameters are not required.
     * @param {String} [req.mediaId] ID of media file.
     * @param {String} [req.categoryId] ID of the category where content belongs to.
     * @param {String} [req.fatherVodId] The options are as follows:
     * Set the parameter to the series ID if the content is an episode. The subscription to both series and episode is authenticated.
     * -2: indicates only the subscription to an episode is authenticated.
     * -3: indicates only the subscription to series to which an episode belongs is authenticated.
     * @param {String} [req.islocalAction=0] Indicates whether local authentication is performed.
     * Local authentication refers to the authentication on the EPG server based on the cached subscription relationship. Local authentication can be used to reduce the interaction between the EPG server and MEM on occasions when certain operations may undermine the system performance, for example, displaying the product information on the VOD details page.
     *
     * - 0: no
     * - 1: yes
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Authentication return code.
     * @return {String} return.retmsg Response message.
     * @return {String} return.url Play URL.
     * @return {String} return.point Number of IPTV bonus points that are available.
     * @return {String} return.telecompoint Number of carrier bonus points that are available.
     * @return {eBase.data.Product[]} return.monthlist Returned information about the subscribable monthly package products after an authentication failure.
     * @return {eBase.data.Product[]} return.timelist Returned information about the subscribable times-based products after an authentication failure.
     * @return {eBase.data.Product[]} return.preorderlist Returned information about the subscribed products that have not been activated after an authentication failure.
     * @return {String} return.productId Product ID used for content authentication.
     * @return {String} return.restriction Restrictions of a product. The parameter is returned only when the authentication is successful.
     * @return {String} return.inCondition Applicable conditions of a product. The parameter is returned only when the authentication is successful.
     */
    iptv.authorizeAndPlay = function(req, isnoshowerror) {
        var jsonreq = {
            url: '/EPG/JSON/AuthorizeAndPlay',
            data: JSON.stringify(req)
        };
        if (isnoshowerror) {
            jsonreq.isnoshowerror = isnoshowerror;
        }
        return utils.jsonAjax(jsonreq);
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    /**
     * A client uses this interface to obtain the bookmark information.
     * For example, when a subscriber exits the VOD content playback, the client can invoke this interface to obtain bookmark information and cache the information to a local PC after adding a bookmark.
     * When a subscriber goes to the bookmark management page, the client invokes this interface to obtain and display all bookmarks of the subscriber. The client can also obtain the list of bookmarks of a specified type.
     *
     * @param {Object} req
     * @param {String} [req.bookmarkType=0] Bookmark type.
     *
     * - 0: VOD bookmark
     * - 1: cPVR bookmark
     * - 2: nPVR bookmark
     * - 3: Catch-up TV bookmark
     * - 4: channel bookmark
     * - -1: all
     * @param {String} [req.orderType=1] Sorting mode.
     *
     * - 0: by update time in ascending order
     * - 1: by update time in descending order
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.Bookmark}
     */
    iptv.queryBookmark = function(req) {
        utils.rename(req, 'bookMarkType', 'bookmarkType');

        // 支持运营商配置内容过滤级别
        var contentLevel = eBase.session.get('ContentFilterLevel');
        if (!contentLevel) {
            var contentFilerLevel = gUtils.getConfiguration({
                key: 'CONTENT_FILTER_LEVEL'
            });

            console.debug('ebase-core-stb---iptv.queryBookmark---contentFilerLevel = ' + contentFilerLevel);

            if (contentFilerLevel) {
                eBase.session.put('ContentFilterLevel', contentFilerLevel, false);
                contentLevel = contentFilerLevel;
            }
        }

        console.debug('ebase-core-stb---iptv.queryBookmark---contentLevel = ' + contentLevel);

        // recently watched（EPG）和watch history（OTT）中的过滤级别入参，取当前profile级别和CONTENT_FILTER_LEVEL的较小值；            
        var curProfile = eBase.session.get('currentProfile');

        console.debug('ebase-core-stb---iptv.queryBookmark---core.app.profileApp.currentProfile.levels = ' + core.app.profileApp.currentProfile.levels);
        // currentProfile在CORE_PROFILE_LOGIN_SUCCESS事件之后赋值，而在此事件中触发APP_PROFILE_DATA_READY事件刷新refreshBookmark
        // 此处增加这个处理仅仅是防止从session中获取currentProfile失败。
        // 注意core.app.profileApp.currentProfile中的levels在用户改变级别后，不会变化。
        if (!curProfile) {
            curProfile = core.app.profileApp.currentProfile;
        }

        if (curProfile) {
            console.debug('ebase-core-stb---iptv.queryBookmark---curProfile.levels = ' + curProfile.levels);
            if (isNaN(contentLevel)) {
                contentLevel = curProfile.levels;
            } else {
                contentLevel = Math.min(parseInt(contentLevel), parseInt(curProfile.levels)).toString();
            }
        }

        console.debug('ebase-core-stb---iptv.queryBookmark---filterlist---contentLevel = ' + contentLevel);
        /**
         *播放流不需要根据内容过滤
         */
        if (contentLevel && req.needFilter) {
            if (!req.filterlist) {
                req.filterlist = [];
            }
            req.filterlist.push({
                'key': 'RatingId',
                'value': contentLevel
            });
        }
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryBookmark',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'bookmarkList', 'list');
        });
    };

    /**
     * A client uses this interface to add bookmark
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     *     For a PVR bookmark, this parameter indicates the PVR task ID.
     * @param {String} [req.fatherVodId] Series ID.
     *     This parameter is valid only when bookMarkType is set to 0.
     *     Note: The MEM saves only one episode bookmark for the same series. If the series IDs of VOD program bookmarks are the same, the MEM will automatically delete other VOD program bookmarks of the same series.
     * @param {String} [req.time] Time when a bookmark is recorded.
     *     If bookMarkType is set to 0, 1, 2, or 3, the data type must be int. The unit is second.
飦?  *     If bookMarkType is set to 4, this parameter value is a string of specific time. The format is yyyyMMddHHmmss.
     * @param {Number} [req.bookmarkType=0] Bookmark type.
     *
     * - 0: VOD bookmark
     * - 1: cPVR bookmark
     * - 2: nPVR bookmark
     * - 3: Catch-up TV bookmark
     * - 4: channel bookmark
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.addBookmark = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'fatherVodId', 'fathervodid');
        utils.rename(req, 'bookmarkType', 'bookMarkType');

        req.action = 'ADD';

        return utils.jsonAjax({
            url: '/EPG/JSON/BookmarkManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_BOOKMARK_CHANGED');
        });
    };
    /**
     * A client uses this interface to delete bookmark
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     *     For a PVR bookmark, this parameter indicates the PVR task ID.
     * @param {String} [req.fatherVodId] Series ID.
     *     This parameter is valid only when bookMarkType is set to 0.
     *     Note: The MEM saves only one episode bookmark for the same series. If the series IDs of VOD program bookmarks are the same, the MEM will automatically delete other VOD program bookmarks of the same series.
     * @param {Number} [req.bookmarkType=0] Bookmark type.
     *
     * - 0: VOD bookmark
     * - 1: cPVR bookmark
     * - 2: nPVR bookmark
     * - 3: Catch-up TV bookmark
     * - 4: channel bookmark
     * @return {eBase.core.Deferred}
     * @return {String} Response.retcode Return code.
     * @return {String} [Response.retmsg] Response message.
     */
    iptv.deleteBookmark = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'fatherVodId', 'fathervodid');
        utils.rename(req, 'bookmarkType', 'bookMarkType');

        req.action = 'DELETE';

        return utils.jsonAjax({
            url: '/EPG/JSON/BookmarkManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_BOOKMARK_CHANGED');
        });
    };

    /**
     * Clear all bookmarks.
     *
     * @param {Object} req
     * @param {Number} [req.bookmarkType=0] Bookmark type.
     *
     * - 0: VOD bookmark
     * - 1: cPVR bookmark
     * - 2: nPVR bookmark
     * - 3: Catch-up TV bookmark
     * - 4: channel bookmark
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.clearBookmark = function(req) {
        req = req || {
            bookmarkType: 0
        };
        utils.rename(req, 'bookmarkType', 'bookMarkType');
        req.action = 'CLEAR';

        return utils.jsonAjax({
            url: '/EPG/JSON/BookmarkManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_BOOKMARK_CHANGED');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    var parseReq = function(req) {
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'businessType', 'businesstype');
        utils.rename(req, 'promotionCode', 'promoCode');
        utils.rename(req, 'productId', 'productid');
        utils.rename(req, 'continueType', 'continuetype');
        utils.rename(req, 'contentId', 'contentid');

        return req;
    };
    /**
     * A client uses the interface to cancel the product subscription.
     * For duration-based products, the interface is used to cancel the automatic subscription renewal.
     * For PPV products, the interface is used to cancel the subscription to the PPV product.
     *
     * @param {Object} req
     * @param {String} req.productId Product ID.
     * @param {String} req.continueType Cancellation type:
     *
     * - 0: Cancel the current subscription (for the PPV subscription or duration-based product subscription).
     * - 1: Cancel the automatic subscription renewal and the cancellation takes effect in the next cycle (for duration-based product subscription).
     * - 2: Cancel the automatic subscription renewal and the cancellation takes effect by the end of the month (for duration-based product subscription).
     * @param {String} [req.contentId] Content ID. The parameter needs to be input when the times-based PPV product is unsubscribed from.
     * @param {String} [req.contentType] Content type. The parameter needs to be input when the times-based PPV product is unsubscribed from.
     * @param {String} [req.businessType] Service type.
     * The request does not contain this parameter when continuetype is set to 1.
     * The request contains this parameter when continuetype is set to 0.
     * The options are as follows:
     *
     * - 0: combination of several service types
     * - 1: VOD
     * - 2: LIVE TV
     * - 3: PPV
     * - 4: PLTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 7: VAS
     * The parameter is required only by the PPV service.
     * @param {String} [req.startTime] Start time of subscription. The parameter must be input when a PPV product is unsubscribed from.
     * If a monthly-package product can be unsubscribed from by time segment, the parameter needs to be carried.
     * Format: yyyyMMddHHmmss
     * @param {String} [req.deviceId] ID of a logical device bound to an unsubscribed non-shared product, which comes from the deviceId attribute of Product. If the parameter is not carried, the non-shared product bound to the current device is unsubscribed from on the MEM by default.
     * The parameter is invalid for shared products.
     *
     * @return {eBase.core.Deferred} Promising response.
     */
    iptv.cancelSubscribe = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/CancelSubscribe',
            data: JSON.stringify(parseReq(req))
        }).done(function() {
            event.trigger('CORE_CANCELSUBSCRIBE_SUCCESS');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * This interface is used to query whether a subscriber has rights to the specified content in a specified geographical location.
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     * @param {String} req.contentType Content type.
     * Only VOD programs are supported.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} return.retmsg Response message.
     * @return {String} return.location User location. Use the country code defined in the ISO 3166-1 standard.
     * @return {String} return.IP Subscriber's IP address.
     * @return {String} return.region Geographical location to which the content copyright applies. Use the country code defined in the ISO 3166-1 standard. Separate multiple values with a comma (,). The value is returned when the operation is successful.
     */
    iptv.checkLocationAuthorization = function(req) {
        utils.rename(req, 'contentId', 'contentID');

        return utils.jsonAjax({
            url: '/EPG/JSON/CheckLocationAuthorization',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * This interface is used to query all products
     */
    iptv.getAllProducts = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetAllProducts',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'productlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Check content ratings or content lock when a subscriber plays a content item.
     * @param {Object} req
     * @param {String} req.contentType Content type.The options are as follows:
     *
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - SUBJECT
     * - PROGRAM
     *
     * @param {String} req.contentId Content id.
     * @param {String} [req.isLocked=1] Indicates whether to check the content lock status.The options are as follows:
     *
     * - 1: yes
     * - 0: no
     *
     * @param {String} [req.isControlled=1] Indicates whether to check the content ratings.The options are as follows:
     *
     * - 1: yes
     * - 0: no
     *
     * @return {String}
     * Operation return code.The options are as follows:
     *
     * - 0: Content is locked or the content rating is higher than the profile's parental control level.
     * - 1: content not restricted.
     *
     */
    iptv.checkLock = function(req) {
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'isLocked', 'islocked');
        utils.rename(req, 'isControlled', 'iscontrolled');

        req.islocked = req.islocked || 1;
        req.iscontrolled = req.iscontrolled || 1;

        return core.utils.jsonAjax({
            url: '/EPG/JSON/ContentLocked',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {};
    /**
     * A client uses this interface to check passwords.
     * @param {Object} req
     * @param {String} req.password Password, in encrypted text. The password must be encrypted using the encryption mode.
     * @param {String} [req.type=3] Password type.The options are as follows:
     *
     * - 0: profile's password.
     * - 1: subscription password.
     * - 2: subscriber's login password.
     * - 3: super profile password.
     * - 4: parental control password.
     * - 5: remote EPG password used to log in to the client on a tablet. This value is not recommended.
     * @return {eBase.core.Deferred}
     */
    iptv.checkPassword = function(req) {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/CheckPassword',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils,
        config = core.config;

    var asciiCheckSum = function(obj) {
        var sum = 0,
            i;

        for (i = 0; i < obj.length; i++) {
            sum = sum + obj.charCodeAt(i);
        }

        return sum + 1;
    };

    // UPGRADE url needs a checksum in the end
    var calculateUrl = function(prefixUrl, req) {
        req = req || {};

        var url = prefixUrl + '/jsp/upgrade.jsp?' + [
            'TYPE=' + (req.terminalType || config.terminalType),
            'MAC=' + config.mac,
            'USER=' + (req.userId || 'userId'),
            'VER=' + config.clientVersion,
            'RedirectType=1',
            'CHECKSUM='
        ].join('&');

        url = url + asciiCheckSum(url);

        return url;
    };

    /**
     * Check with UPGRADE server for upgrade info.
     * @param {Object} req
     * @param {String} req.userId Subscriber ID.
     * @return {eBase.core.Deferred} The upgrade file on server.
     */
    iptv.checkUpgrade = function(req) {
        var retDfd = new Deferred();
        var edsUrl = calculateUrl(req.edsUrl || config.edsUrl + '/EDS', req);

        // Step1: Check with EDS for UPGRADE server address
        utils.ajax({
            url: edsUrl,
            type: 'GET',
            dataType: 'text'
        }).done(function(_upgradeUrl) {
            var urls = /(^http.*)\/jsp\//.exec(_upgradeUrl.trim());

            if (urls && urls.length > 1) {
                var upgradeUrl = urls[1];

                core.session.put('upgradeUrl', upgradeUrl);

                // Step2: Check with UPGRADE for upgrade info file.
                utils.ajax({
                    url: calculateUrl(upgradeUrl, req),
                    type: 'GET',
                    dataType: 'text'
                }).then(retDfd.resolve, retDfd.reject);
            } else {
                retDfd.reject();
            }
        }).fail(retDfd.reject);

        return retDfd.promise();
    };

    return core;
})(eBase);
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    /**
     * This interface is used to query content in favorites by category or to query content in all favorites categories.
     * The queried content items are sorted in descending order of the time when a content item is added to favorites.
     * When a subscriber opens favorites, the client invokes this interface to obtain content of the specified type in favorites;
     * if the content type is not specified, the client obtains all content in favorites.
     * The favorites content obtained by a client using this interface contains only the content ID and type,
     * the client needs to invoke the {eBase.iptv.queryDetail} to obtain content details.
     *
     * @param {Object} req
     * @param [req.contentType] Content type. If the request does not contain this parameter, all records in favorites are queried.
     *
     * - VAS
     * - CHANNEL
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - WEB_CHANNEL
     * - VOD
     * - AUDIO_VOD
     * - VIDEO_VOD
     * @param [req.favoId] Favorites category ID. If the request does not contain this parameter, content items in all favorites categories are queried.
     *
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.Favorite}
     */
    iptv.queryFavorite = function(req) {
        utils.rename(req, 'contentType', 'contenttype');

        return utils.jsonAjax({
            url: '/EPG/JSON/GetFavorite',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'favoritelist', 'list');
        });
    };

    /**
     * Add content to favorites.
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     *     Only one content item can be added at a time.
     * @param {String} [req.contentType] Content type.
     *     The options are as follows: VAS, AUDIO_CHANNEL, VIDEO_CHANNEL, WEB_CHANNEL, AUDIO_VOD, VIDEO_VOD
     *     If contentid does not carry values, all favorites of the specified content type are deleted. Use commas (,) to separate multiple content types.
     * @param {String} [req.favoId] Favorites category ID.
     *     If the request does not contain this parameter, content is added to the default favorites category.
     *     If the request contains this parameter, content is added to a specified favorites category.
     * @param {String} [req.autoCover=0] Indicates whether the earliest content item in favorites can be overwritten when the number of content items in favorites reaches the maximum.
     *     The options are as follows, 0: yes, 1: no
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     *     The options are as follows, 0: Operation succeeded, -1: Operation failed.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.addFavorite = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');

        req.action = 'ADD';
        req.autoCover = 1;

        return utils.jsonAjax({
            url: '/EPG/JSON/FavoriteManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_FAVORITE_CHANGED');
        });
    };

    /**
     * Delete content from favorites.
     *
     * @param {Object} req
     * @param {String} [req.contentId] Content ID.
     *     Multiple content items can be deleted at a time. Separate multiple IDs with a comma (,).
     * @param {String} [req.contentType] Content type.
     *     The options are as follows: VAS, AUDIO_CHANNEL, VIDEO_CHANNEL, WEB_CHANNEL, AUDIO_VOD, VIDEO_VOD
     *     If contentid does not carry values, all favorites of the specified content type are deleted. Use commas (,) to separate multiple content types.
     * @param {String} [req.favoId] Favorites category ID.
     *     If the request does not contain this parameter, specified content items in all favorites categories are deleted.
     *     If the request contains this parameter, content is deleted to a specified favorites category.
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     *     The options are as follows, 0: Operation succeeded, -1: Operation failed.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.deleteFavorite = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');

        req.action = 'DELETE';
        return utils.jsonAjax({
            url: '/EPG/JSON/FavoriteManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_FAVORITE_CHANGED');
        });
    };
    /**
     * Clear all favorites.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     *     The options are as follows, 0: Operation succeeded, -1: Operation failed.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.clearFavorite = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/FavoriteManagement',
            data: JSON.stringify({
                action: 'CLEAR'
            })
        }).done(function() {
            event.trigger('CORE_FAVORITE_CHANGED');
        });
    };
    /**
     * Sort favorites.
     *
     * @param {Object} req
     * @param {String} req.contentType Content type.
     *     The options are as follows: VAS, CHANNEL, AUDIO_CHANNEL, VIDEO_CHANNEL, WEB_CHANNEL, VOD, AUDIO_VOD, VIDEO_VOD
     * @param {String} req.contentIds Content ID list after content items are sorted. Separate multiple values with a comma (,).
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     *     The options are as follows, 0: Operation succeeded, -1: Operation failed.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.sortFavorite = function(req) {
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'contentIds', 'contentids');

        return utils.jsonAjax({
            url: '/EPG/JSON/SortFavorite',
            data: JSON.stringify(req)
        });
    };
    /**
     * Create favorites categories.
     *
     * @param {Object} req
     * @param {String} req.favoName Name of the favorites category.
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     * @return {String} [return.favoId] Favorites category ID. The value is returned only when the operation is successful.
     */
    iptv.addFavoCatalog = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/AddFavoCatalog',
            data: JSON.stringify(req)
        });
    };
    /**
     * Delete favorites categories.
     *
     * @param {Object} req
     * @param {String} [req.favoId] Favorites category ID.
     *     If the request does not contain this parameter, all favorites categories are deleted.
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     */
    iptv.deleteFavoCatalog = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/DeleteFavoCatalog',
            data: JSON.stringify(req)
        });
    };
    /**
     * Modify favorites categories.
     *
     * @param {Object} req
     * @param {String} req.favoId Favorites category ID
     * @param {String} req.favoName Favorites category name after the modification.
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     * @return {String} [return.favoId] Favorites category ID. The value is returned only when the operation is successful.
     */
    iptv.updateFavoCatalog = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/UpdateFavoCatalog',
            data: JSON.stringify(req)
        });
    };
    /**
     * Query subscribers' favorites categories.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Return code.
     * @return {String} [return.retmsg] Response message.
     * @return {Array} [return.favolist] Favorites category information.
     *     The value is returned only when the operation is successful.
     *     This is array, and the properties of the object in array are as follows:
     *     {String} id Favorites category ID.
     *     {String} name Favorites category name.
     */
    iptv.queryFavoCatalog = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryFavoCatalog'
        });
    };
    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Get AdPolicy by id
     * @param {Object} req
     * @param {String} [req.id] AD position ID. Default is startup AD.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.AdPolicy}
     */
    iptv.getAdPolicyById = function(req) {
        utils.rename(req, 'id', 'poId');

        return utils.jsonAjax({
            url: '/EPG/JSON/GetAdPolicyByID',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'adPolicies', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 * queryVodDetail
 * modifyVodDetail
 * 支持用户对VOD的评分和展示
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    iptv.QueryContentScore = function(req) {

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryContentScore',
            data: JSON.stringify(req || {})
        });
    };

    iptv.ContentScoreMgmt = function(req) {

        return utils.jsonAjax({
            url: '/EPG/JSON/ContentScoreMgmt',
            data: JSON.stringify(req || {})
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query the content information based on the content foreign key.
     * @param {Object} req
     * @param {String} req.foreignSn Content foreign key.
     * @param {String} [req.contentType] Content type.
     * The options are as follows:
     *
     * - VOD
     * - CHANNEL
     * - VAS
     * - SUBJECT
     * - PROGRAM
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Content} return.content
     */
    iptv.getContentByForeignSn = function(req) {
        utils.rename(req, 'contentType', 'contenttype');

        return utils.jsonAjax({
            url: '/EPG/JSON/GetContentByForeignSn',
            data: JSON.stringify(req || {})
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {};
    /**
     * A client invokes this interface to query a subscriber's credit limit.
     * A subscriber can query the remaining credit limit and total credit limit.
     *
     * @param {Object} req
     * @param {String} [req.queryType=0] Query type.The options are as follows:
     *
     * - 0: current user's consumption limit
     * - 1: subscriber's credit limit
     *
     * @return {eBase.core.Deferred}
     * @return return.resp {eBase.data.CreditBalance}
     */
    iptv.getCreditBalance = function(req) {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/QueryCreditBalance',
            data: JSON.stringify(req || {})
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Obtain the currency rate and display unit of the regional node to which a subscriber belongs.
     * Result sample:

         {currencyRate: "100", currencyAlphCode: "CNY"}

     * @return {eBase.data.CurrencyInfo}
     */
    iptv.queryCurrency = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetCurrencyInfo'
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Query the account and password of a guest user.
     * Result sample:

         {subscriberId: "GUEST", password: "db90e7eb"}

     * @return {eBase.core.Deferred|eBase.data.GuestInfo}
     */
    iptv.getGuestInfo = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetGuestInfo'
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client invokes this interface to obtain the playback URL before a program plays.
     * The playback URL to be obtained and parameters to be set when the client invokes this interface vary depending on the program type. For example, to obtain the VOD program playback URL, the client sets the input parameter playtype to 1, and sets the input parameter contentid to the VOD content ID.
     * If the CP at a site limits the number of sessions of the VOD content that can be watched by multiple users concurrently, the terminal can input the profile ID and device ID to stop playing the VOD content for the profile.
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     * This parameter indicates a VOD program ID, channel ID, or program ID. If this parameter indicates a series ID, playtype can be only set to 5.
     * @param {String} [req.mediaId] Media file ID. If the request does not contain the parameter, the MEM obtains the ID of the first media file that matches the terminal domain.
     * @param {String} [req.playbillId] Program ID. The request contains this parameter when a program is played.
     * @param {String} [req.beginTime] Bookmark start time or program start time.
     * For bookmarks, the value is the period before the program start time, in seconds.
     * For programs, the format is yyyyMMddHHmmss. If this parameter is not carried, it indicates the default start time of the programs.
     * @param {String} [req.endTime] Program end time. The request contains this parameter when a program is played. If this parameter is not carried, it indicates the default end time of the programs.
     * Format: yyyyMMddHHmmss
     * @param {Number} req.playType Program type.
     * The options are as follows:
     *
     * - 1: VOD
     * - 2: live TV
     * - 3: NVOD
     * - 4: Catch-up TV
     * - 5: VOD clip
     * - 6: VOD bookmark
     * - 9: PVR program
     * @param {String} [req.productId] Product ID used for content authentication. The value of this parameter is the same as the value of output parameter productId of 3.4 "Service Authentication Interface."
     * @param {String} [req.profileId] ID of the profile for which the playback stops. The parameter must be input together with deviceId.
     * @param {String} [req.deviceId] ID of the device for which the playback stops. The parameter must be input together with profileId.
     * @param {String} [req.catchupMediaId] Media ID used during Catch-up TV program playback. This parameter is valid only when playtype is set to 4 or 9.
     * @param {String} [req.authContentId] ID of content whose authorization is checked. If this parameter is carried, the MEM checks the authorization of the content whose ID is the value of contentid.
     * This parameter is used for the skippable channel service, and is set to the skippable channel VAS ID.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.url URL for playing a program. The parameter is returned when retcode is 0.
     * @return {String} return.retcode Return code.
     * @return {String} return.retmsg Response message.
     * @return {String[]} return.profileIds If the number of sessions for concurrent access to VOD content exceeds the threshold. The value of this parameter is the ID of the profile that is playing the VOD content.
     * @return {String[]} return.deviceIds If the MEM sends a response indicating that the number of sessions for concurrent access to VOD content exceeds the threshold. The value of this parameter is the ID of the virtual device that is playing the VOD content.
     */
    iptv.getPlayUrl = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'mediaId', 'mediaid');
        utils.rename(req, 'playbillId', 'playbillid');
        utils.rename(req, 'beginTime', 'begintime');
        utils.rename(req, 'endTime', 'endtime');
        utils.rename(req, 'playType', 'playtype');

        return utils.jsonAjax({
            url: '/EPG/JSON/Play',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Obtain information about the current program, previous programs, and following programs.
     *
     * @param {Object} req
     * @param {String} req.channelId Channel ID.
     * @param {String} req.date Time configured for querying a program. Format: yyyyMMddHHmmss
     * @param {String} req.type Program type.The options are as follows:
     *
     * - 0: live TV programs, excluding expired programs
     * - 1: recorded program
     * - 2: all live TV programs, including expired programs
     *
     * @param {String} [req.preNumber] If the value of preNumber is greater than or equal to 0, a number of (the value of preNumber) programs before a specified time point is obtained.Default 5.
     * @param {String} [req.nextNumber] Same with preNumber.
     *
     * @return {eBase.data.Playbill}
     */
    iptv.getPlaybillContext = function(req) {
        utils.rename(req, 'channelId', 'channelid');

        req.preNumber = req.preNumber || 5;
        req.nextNumber = req.nextNumber || 5;

        return utils.jsonAjax({
            url: '/EPG/JSON/PlayBillContextEx',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Obtain information about the current program, previous programs, and following programs.
     *
     * @param {Object} req
     * @param {String} req.channelId Channel ID.
     * @param {String} req.date Time configured for querying a program. Format: yyyyMMddHHmmss
     * @param {String} req.type Program type.The options are as follows:
     *
     * - 0: live TV programs, excluding expired programs
     * - 1: recorded program
     * - 2: all live TV programs, including expired programs
     *
     * @param {String} [req.preNumber] If the value of preNumber is greater than or equal to 0, a number of (the value of preNumber) programs before a specified time point is obtained.Default 5.
     * @param {String} [req.nextNumber] Same with preNumber.
     *
     * @return {eBase.data.Playbill}
     */
    iptv.GetProductsByContent = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetProductsByContent',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query information about the regional node carrier to which the subscriber belongs.
     * Result sample:

         {creditType: "1"}

     * @return {eBase.data.SubnetInfo}
     */
    iptv.getSubnetInfo = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetSubNetInfo'
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Get XMPP token for TVMS push.
     *
     * @param {Object} req
     * @param {String} req.deviceType Device type:
     *
     * - IOS
     * - ANDROID
     * - WP
     * - LINUX
     * - PC
     * - MAC
     * @param {String} [req.deviceToken] Token of 3rd push platform (APNS/GCM/MPNS)
     * @param {String} req.physicalDeviceId Physical device address.
     *
     * - for STB, it's mac
     * - for OTT, it's clientId of CA
     * @param {String} [req.version] Version of XMPP interface:
     *
     * - 1.0: in key:value or key:{key:value,key:value} format
     * - 1.1: in json {key:value} or {key:{key:value,key:value}}
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.xmppToken XMPP token.
     */
    iptv.getXmppToken = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetXMPPToken',
            data: JSON.stringify(req)
        });
    };

    iptv.pushMsgByTag = function(req) {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/PushMsgByTag',
            data: JSON.stringify(req)
        });
    };


    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to send heartbeat messages to the EPG server.
     * The client must periodically invoke this interface to send heartbeat requests to the EPG server to keep the HTTP session with the EPG server. The output parameter nextcallinterval specifies the interval.
     * The client must cache various data such as the channel list, favorite list, and bookmark list. The client invokes this interface to check whether these data is updated. If data is updated, the client invokes the relevant interface to update the data cached in the local disk. For details, see the output parameters of this interface.
     * If the EPG server is deactivated, the MEM returns the HTTP 303 error code. The client requests the MEM to schedule another EPG server.
     *
     * @param userId Subscriber ID.
     * The ID contains digits, letters, dot (.), underscore (_), and @.
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Heartbit} return.resp
     */
    iptv.heartbeat = function(req) {
        utils.rename(req, 'userId', 'userid');

        return utils.jsonAjax({
            url: '/EPG/JSON/HeartBit',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.utils
 */
var eBase = (function(core) {
    var utils = core.utils,
        config = core.config;
    var NumOfhttp504;
    /*Modified begin by [wWX169568] [wuchengren]at[2014.5.6]*/

    var SECURITY_INTERFACES = [
        '/EPG/JSON/Authorization',
        '/EPG/JSON/Authenticate',
        '/EPG/JSON/Subscribe',
        '/EPG/JSON/CheckPassword',
        '/EPG/JSON/UpdatePassword',
        '/EPG/JSON/ResetPassword',
        '/EPG/JSON/AddProfile',
        '/EPG/JSON/ModifyProfile',
        '/EPG/JSON/QueryProfile',
        '/EPG/JSON/SwitchProfile',
        '/EPG/JSON/Play',
        '/EPG/JSON/GetFavorite',
        '/EPG/JSON/GetLock',
        '/EPG/JSON/QueryPVR',
        '/EPG/JSON/queryOrder',
        '/EPG/JSON/queryPPV',
        '/EPG/JSON/QueryBookmark',
        '/EPG/JSON/QueryMyContent',
        '/EPG/JSON/GetCustomizeConfig'
    ];
    /*Modified end by [wWX169568] [wuchengren]at[2014.5.6]*/

    var isSecurityInterface = function(url) {
        return SECURITY_INTERFACES.indexOf(url) >= 0;
    };
    /**AntiTamper start */
    var getAntiTamperInterface = function(url) {
        var antiTamperURI = eBase.session.get('antiTamperURI');
        if (!antiTamperURI) {
            return null;
        }
        var methodName = url.replace('/EPG/JSON/', '');
        var antiTamperInterface = _.find(antiTamperURI, function(antiTamper) {
            return antiTamper.key === methodName;
        });

        return antiTamperInterface;
    };

    var createInputChecksum = function(option) {
        var jsonParameter = JSON.parse(option.data);
        var targetParameters = option.parameters.split(';');
        if (_.isEmpty(targetParameters) || targetParameters.length < 2 || targetParameters[0] === '') {
            return null;
        }
        var inputParameters = targetParameters[0].split(',');
        var keyContent = '';
        _.each(inputParameters, function(parameter) {
            keyContent = keyContent + jsonParameter[parameter];
        });
        console.log("keyContent", keyContent);
        console.log("signatureKey" + eBase.session.get('signatureKey'));
        console.log("signatureKeyUP" + eBase.session.get('signatureKey').toUpperCase());
        return utils.sha256(keyContent + eBase.session.get('signatureKey').toUpperCase()).toUpperCase();
    };

    var createOutputChecksum = function(option) {
        var targetParameters = option.parameters.split(';');
        if (_.isEmpty(targetParameters) || targetParameters.length < 2 || targetParameters[1] === '') {
            return null;
        }
        var outputParameters = targetParameters[1].split(',');
        var keyContent = '';
        if (targetParameters[1].indexOf('.') < 0) {
            _.each(outputParameters, function(parameter) {
                keyContent = keyContent + option.resp[parameter];
            });
            console.log("keyContent1", keyContent);
        } else {
            var firstLevelParam = {};
            var secondLevelParams = [];
            _.each(outputParameters, function(parameter) {
                var methodName = parameter.split('.')[0];
                var prameterName = parameter.split('.')[2];
                if (firstLevelParam[methodName]) {
                    firstLevelParam[methodName].push(prameterName);
                } else {
                    secondLevelParams = [];
                    secondLevelParams.push(prameterName);
                    firstLevelParam[methodName] = secondLevelParams;
                }
            });
            _.each(firstLevelParam, function(value, key) {
                _.each(option.resp[key], function(targetObj) {
                    _.each(value, function(parameter) {
                        keyContent = keyContent + targetObj[parameter];
                    });
                });
            });
            console.log("keyContent2", keyContent);
        }
        console.log("keyContent3" + keyContent);
        console.log("signatureKey3" + eBase.session.get('signatureKey').toUpperCase());

        return utils.sha256(keyContent + eBase.session.get('signatureKey').toUpperCase()).toUpperCase();
    };
    /**AntiTamper end */

    // Aggregate ajax requests
    var aggregate = function(handler, duration) {
        var argList = [],
            dfdList = [],
            intervalId = null;

        return function() {
            console.log('ebase-core-stb---aggregate == ' + core.session.get('core.isAuthenticated'));
            console.log('arguments.url == ' + arguments[0].url);
            //console.log('arguments.url.indexOf("GetDiscountInfos") == ' + arguments[0].url.indexOf("GetDiscountInfos"));
            if (core.session.get('core.isAuthenticated') && arguments[0].url && arguments[0].url.indexOf("GetDiscountInfos") < 0) {
                var dfd = new Deferred();

                argList.push(_.toArray(arguments));
                dfdList.push(dfd);

                if (!intervalId) {
                    intervalId = setTimeout(function() {
                        intervalId = null;
                         if(argList.length==1&&argList[0][0]){
                            var arg=argList[0][0];
                            var dfdAjx=dfdList[0];
                            argList=[];
                            dfdList=[];
                            utils.forceJsonAjax(arg).then(dfdAjx.resolve, dfdAjx.reject);
                        }else{
                            var args = [argList, dfdList];
                            argList = [];
                            dfdList = [];
                            handler.apply(null, args);
                        }
                    }, duration);
                }

                return dfd;
            } else {
                return utils.forceJsonAjax(arguments[0]);
            }
        };
    };

    // Send batch ajax request to backend.
    var sendBatchAjax = function(subReqList) {
        var urlList = [],
            needHttps = false;

        var requestList = _.map(subReqList, function(settings) {
            var url = settings.url;
            var inf = /\/JSON\/(\w*)/.exec(url)[1];

            urlList.push(inf);
            if (isSecurityInterface(url)) {
                needHttps = true;
            }
            /**AntiTamper start */
            var antiTamper = getAntiTamperInterface(settings.url);
            var inputChecksum = null;
            if (antiTamper) {
                inputChecksum = createInputChecksum({
                    data: settings.data,
                    parameters: antiTamper.value
                })
            }
            // add the input checksum into the request 
            if (inputChecksum) {
                var inputJsonObj = JSON.parse(settings.data);
                inputJsonObj.checksum = inputChecksum;
                settings.data = JSON.stringify(inputJsonObj);
            }
            /**AntiTamper end */

            return '{"name": "' + inf + '", "param":' + (settings.data || '{}') + '}';
        });

        return utils.ajax({
            url: (needHttps ? config.epgHttpsUrl : config.epgUrl) + '/EPG/JSON/ExecuteBatch?' + _.uniq(urlList).join(','),
            data: '{"requestList": [' + requestList.join(',') + ']}'
        });
    };

    // Aggregate requests and dispatch responses
    var executeBatch = function(reqList, dfdList) {
        reqList = _.flatten(reqList);
        var dfd = sendBatchAjax(reqList);

        dfd.done(function(resp) {
            if (resp.responseList) {
                _.each(resp.responseList, function(resp, index) {
                    if (resp.msg && !_.isUndefined(resp.msg.retcode) && resp.msg.retcode !== '0' && resp.msg.retcode !== 0) {
                        core.event.trigger('IPTV_REQUEST_FAIL', {
                            req: reqList[index],
                            resp: resp.msg
                        });
                        dfdList[index].reject(resp.msg);
                    } else {
                        /**AntiTamper start */
                        var antiTamper = getAntiTamperInterface(resp.name);
                        if (resp.msg && antiTamper && antiTamper.value && antiTamper.value.split(';')[1]) {
                            var outputChecksum = createOutputChecksum({
                                resp: resp.msg,
                                parameters: antiTamper.value
                            });
                            if (resp.msg.checksum && outputChecksum && outputChecksum !== resp.msg.checksum) {
                                var httpResp = {
                                    resultCode: 'http_401',
                                    description: 'Data Error',
                                    retcode: 'http_401',
                                    retmsg: 'Data Error'
                                };
                                core.event.trigger('IPTV_REQUEST_FAIL', {
                                    req: reqList[index],
                                    resp: httpResp
                                });
                                dfdList[index].reject(httpResp);
                                return;
                            }
                        }
                        /**AntiTamper end */
                        // Set list to [] if not.
                        if (!resp.msg) {
                            resp.msg = {
                                list: []
                            };
                        } else {
                            resp.msg.list = resp.msg.list || [];
                        }
                        dfdList[index].resolve(resp.msg);
                    }
                });
            } else {
                _.times(reqList.length, function(index) {
                    dfdList[index].reject(resp);
                });
                core.event.trigger('IPTV_REQUEST_FAIL', {
                    req: {
                        url: '/EPG/JSON/ExecuteBatch'
                    },
                    resp: resp
                });
            }
        });

        dfd.fail(function(http) {
            _.times(reqList.length, function(index) {
                dfdList[index].reject({
                    resultCode: 'http_' + http.status,
                    description: http.statusText
                });
            });
        });
    };

    /**
     * Ajax request with MEM backend in JSON format.
     *
     * @private
     * @param {Object} req
     * @param {String} req.url Interface url
     * @param {String} [req.type] HTTP type: 'get', 'post', etc.
     * @param {String} [req.dataType] Should be 'json'
     * @param {String} [req.data] Request data
     *
     * @return {eBase.core.Deferred}
     */
    utils.jsonAjax = function(req) {
        var retDfd = new Deferred(),
            httpResp;
        var errorMessageOption;

        //core.debug('ajax request ' + JSON.stringify(req));
        /**AntiTamper start */
        var antiTamper = getAntiTamperInterface(req.url);
        var inputChecksum = null;
        if (antiTamper) {
            inputChecksum = createInputChecksum({
                data: req.data,
                parameters: antiTamper.value
            })
        }
        //console.log("ajax begin:", JSON.stringify(req));
        // add the input checksum into the request 
        if (inputChecksum) {
            var inputJsonObj = JSON.parse(req.data);
            inputJsonObj.checksum = inputChecksum;
            console.log("inputChecksum", inputChecksum);
            req.data = JSON.stringify(inputJsonObj);
        }
        /**AntiTamper end */
        utils.ajax({
            url: (isSecurityInterface(req.url) ? config.epgHttpsUrl : config.epgUrl) + req.url,
            data: req.data || '{}'
        }).done(function(resp) {
            NumOfhttp504 = 0;
            console.log("req.url" + req.url);
            if (resp && !_.isUndefined(resp.retcode) && resp.retcode !== '0' && resp.retcode !== 0) {
                core.event.trigger('IPTV_REQUEST_FAIL', {
                    req: req,
                    resp: resp
                });
                retDfd.reject(resp);
            } else {
                // Set list to [] if not.
                resp.list = resp.list || [];
                /**AntiTamper start */
                if (antiTamper && antiTamper.value && antiTamper.value.split(';')[1]) {
                    console.log("bbreq.url" + req.url);
                    var outputChecksum = createOutputChecksum({
                        resp: resp,
                        parameters: antiTamper.value
                    });
                    console.log("resp.checksum" + resp.checksum);
                    console.log("outputChecksum" + outputChecksum);

                    if (resp.checksum && outputChecksum && outputChecksum !== resp.checksum) {
                        console.log("ccreq.url", req.url);

                        httpResp = {
                            resultCode: 'http_401',
                            description: 'Data Error',
                            retcode: 'http_401',
                            retmsg: 'Data Error'
                        };
                        core.event.trigger('IPTV_REQUEST_FAIL', {
                            req: req,
                            resp: httpResp
                        });
                        retDfd.reject(httpResp);
                        return;
                    }
                }
                /**AntiTamper end */
                console.log("qqreq.url", req.url);
                retDfd.resolve(resp);
            }
        }).fail(function(http) {
            httpResp = {
                resultCode: 'http_' + http.status,
                description: http.statusText
            };
            var option = {
                req: req,
                resp: httpResp
            };

            if (http.status == '504') {
                NumOfhttp504++;
                if (NumOfhttp504 > 2) {
                    option.reload = true
                }
            }

            core.event.trigger('IPTV_REQUEST_FAIL', option);
            retDfd.reject(httpResp);
        }).always(function(resp) {
            core.debug('ajax response ' + JSON.stringify(resp));
        });

        return retDfd;
    };

    /**
     * Ajax request with Mashup backend in JSON format.
     *
     * @private
     * @param {Object} req
     * @param {String} req.url Interface url
     * @param {String} [req.type] HTTP type: 'get', 'post', etc.
     * @param {String} [req.dataType] Should be 'json'
     * @param {String} [req.data] Request data
     *
     * @return {eBase.core.Deferred}
     */
    utils.jsonAjaxMashup = function(req) {
        var retDfd = new Deferred(),
            httpResp;

        core.debug('ajaxMashup request ' + JSON.stringify(req));

        utils.ajax({
            url: eBase.session.get('mashupaddress') + '/mGear' + req.url,
            data: req.data || '{}'
        }).done(function(resp) {
            if (resp && !_.isUndefined(resp.retcode) && resp.retcode !== '0' && resp.retcode !== 0) {
                core.event.trigger('IPTV_REQUEST_FAIL', {
                    req: req,
                    resp: resp
                });
                retDfd.reject(resp);
            } else {
                // Set list to [] if not.
                resp.list = resp.list || [];
                retDfd.resolve(resp);
            }
        }).fail(function(http) {
            httpResp = {
                resultCode: 'http_' + http.status,
                description: http.statusText
            };
            core.event.trigger('IPTV_REQUEST_FAIL', {
                req: req,
                resp: httpResp
            });
            retDfd.reject(httpResp);
        }).always(function(resp) {
            core.debug('ajax response ' + JSON.stringify(resp));
        });

        return retDfd;
    };

    // The backup interface when aggregate is replacing
    utils.forceJsonAjax = utils.jsonAjax;

    if (config.aggregateDuration > 0) {
        utils.jsonAjax = aggregate(executeBatch, config.aggregateDuration);
    }

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    /**
     * A client uses this interface to obtain information about locked content.
     * When a subscriber goes to the lock management page, the client invokes this interface to obtain all locked content, and can also add a lock icon to the locked content.
     *
     * @param {Object} req
     * @param {String} [req.profileId] Profile ID.
     * If the request contains this parameter, information about a specified profile's locked content is queried. If the request does not contain this parameter, information about the current profile's locked content is queried.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Lock}
     */
    iptv.queryLock = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetLock',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'locklist', 'list');
        });
    };
    /**
     * Add a lock.
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     * Only one content item can be added at a time.
     * @param {String} req.contentType Content type.
     * The options are as follows: VAS, CHANNEL, AUDIO_CHANNEL, VIDEO_CHANNEL, WEB_CHANNEL, SUBJECT, VOD, AUDIO_VOD, VIDEO_VOD
     *
     * @return {eBase.core.Deferred}
     */
    iptv.addLock = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');
        req.action = 'ADD';

        return utils.jsonAjax({
            url: '/EPG/JSON/LockManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_LOCK_CHANGED');
        });
    };
    /**
     * Delete a lock.
     *
     * @param {Object} req
     * @param {String} [req.contentId] Content ID.
     * Multiple content items can be deleted at a time. Separate multiple IDs with a comma (,).
     * @param {String} req.contentType Content type.
     * The options are as follows: VAS, CHANNEL, AUDIO_CHANNEL, VIDEO_CHANNEL, WEB_CHANNEL, SUBJECT, VOD, AUDIO_VOD, VIDEO_VOD
     * If contentid does not carry values, all locks of the specified content type are deleted. Use commas (,) to separate multiple content types.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.deleteLock = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');
        req.action = 'DELETE';

        return utils.jsonAjax({
            url: '/EPG/JSON/LockManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_LOCK_CHANGED');
        });
    };
    /**
     * Clear all locks.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.clearLock = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/LockManagement',
            data: JSON.stringify({
                action: 'CLEAR'
            })
        }).done(function() {
            event.trigger('CORE_LOCK_CHANGED');
        });
    };
    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        session = core.session,
        utils = core.utils,
        config = core.config;
    /**
     * Login system
     * @param {Object} req
     * @param {String} req.userId User id to login.
     * @param {String} [req.edsUrl=config.edsUrl] EDS url to login.
     * @return {eBase.core.Deferred}
     */
    iptv.login = function(req) {
        req.mac = config.mac || '00:00:00:00:00:00';
        session.put('core.isAuthenticated', false);

        return utils.ajax({
            url: (req.epgUrl || config.epgUrl || '') + '/EPG/JSON/Login?UserID=' + req.userId
        }).done(function(resp) {
            var epgUrl = resp.epgurl;
            var matchList = /^(.*)\/EPG\//.exec(epgUrl);

            if (!_.isEmpty(matchList)) {
                config.epgUrl = matchList[1] || config.epgUrl;
            } else {
                config.epgUrl = epgUrl || config.epgUrl;
            }
            /*Modified begin by [wWX169568] [wuchengren]at[2014.5.6]*/
            var epgHttpsUrl = config.isHttpUrl ? config.epgUrl : resp.epghttpsurl;
            /*Modified end by [wWX169568] [wuchengren]at[2014.5.6]*/
            matchList = /^(.*)\/EPG\//.exec(epgHttpsUrl);

            if (!_.isEmpty(matchList)) {
                config.epgHttpsUrl = config.epgHttpsUrl || matchList[1];
            } else {
                config.epgHttpsUrl = config.epgHttpsUrl || epgHttpsUrl;
            }

            config.epgHttpsUrl = config.epgHttpsUrl || config.epgUrl;
            eBase.stb.write('epghttpsurl', resp.epghttpsurl);
            config.encryptToken = resp.enctytoken;
        }).fail(function(resp) {
            eBase.stb.write('ShowPic', 2);
            core.event.trigger('IPTV_REQUEST_FAIL', {
                req: {
                    url: '/EDS/JSON/Login'
                },
                resp: {
                    resultCode: 'http_' + resp.status,
                    description: resp.statusText
                }
            });
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        session = core.session,
        utils = core.utils,
        event = core.event;

    /**
     * Logout from system
     * @param {Object} req
     * @param {Number} [req.type=0] Exit type.
     *
     * - 0: subscriber deregistration
     * - 1: subscriber exit, indicating PC power-off
     *
     * @return {eBase.core.Deferred}
     */
    iptv.logout = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/Logout',
            data: JSON.stringify(req || {})
        }).done(function() {
            event.trigger('CORE_LOGOUT_SUCCESS');
        }).always(function() {
            session.put('core.isAuthenticated', false);
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * This interface is used to change device names.
     *
     * @param {Object} req
     * @param {String} req.deviceId Device logical ID.
     * @param {String} req.deviceName New name of a device.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.modifyDeviceName = function(req) {
        utils.rename(req, 'deviceId', 'deviceid');

        return utils.jsonAjax({
            url: '/EPG/JSON/ModifyDeviceName',
            data: JSON.stringify(req || {})
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Report the playback history when the client plays or stops a live TV program.
     * @param {Object} req
     * @param {Number} req.recordType Record type.The options are as follows:
     *
     * - 0: to play a live TV program
     * - 1: to stop playing
     * - 2: to switch between live TV and TSTV states
     * - 3: to switch channels
     * - 4: to watch a VOD program, Catch-up TV program, experience a VAS, or an nPVR task
     * - 5: to exit the playback of the VOD , Catch-up TV program, or an nPVR task
     *
     * @param {String} req.channel Content id.
     *
     * - If playtype is set to 1 or 2, the parameter indicates channel number.
     * - If playtype is set to 3, the parameter indicates VOD program ID.
     * - If playtype is set to 4, the parameter indicates program ID.
     * - If playtype is set to 5, the parameter indicates VAS content ID.
     * - If playtype is set to 6, the parameter indicates nPVR task ID
     *
     * @param {String} req.playType Type of a service.The options are as follows:
     *
     * - 1: to play a live TV program
     * - 2: to play a TSTV-enabled live TV program
     * - 3: to play a VOD program
     * - 4: to play a Catch-up TV program
     * - 5: to experience a VAS
     * - 6: to play an nPVR program
     *
     * @param {String} [req.nextchannel] Number of the next channel.The request contains this parameter when a subscriber switches channels.The parameter is valid when recordtype is set to 3.
     * @param {String} [req.productId] Product ID after authentication.
     */
    iptv.playRecord = function(req) {
        utils.rename(req, 'recordType', 'recordtype');
        utils.rename(req, 'playType', 'playtype');

        return utils.jsonAjax({
            url: '/EPG/JSON/PlayRecord',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 *SetGlobalFilterCond
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    iptv.SetGlobalFilterCond = function(req) {
        utils.rename(req, 'filterList', 'filterlist');
        return utils.jsonAjax({
            url: '/EPG/JSON/SetGlobalFilterCond',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils,
        event = core.event;
    /**
     * A client uses this interface to query the information about profiles.
     *
     * @param {Object} req
     * @param {Number} req.type Type of profiles to be queried.
     *
     * - 0: current profile
     * - 1: all profiles
     * - 2: super profile
     * - 3: all profiles in the system (precise query based on loginName of a profile)
     * - 4: all profiles in the system (precise query based on the ID of a profile)
     * - 5: all profiles in the system (fuzzy query based on the profile name)
     * @param {Array} [req.condition] Search criteria.
     * Indicates loginName when type is 3.
     * Indicates profile ID when type is 4.
     * Indicates profile name when type is 5.
     * @param {Number} [req.count] Total number of profiles obtained for one query. If the parameter is not carried, the value is -1 by default, indicating that all profiles are obtained.
     * @param {Number} [req.offset] Start position for the query. The value starts from 0, indicating the first query. If the parameter is not carried, the default value 0 is used.
     * @param {Number} [req.isReturnPassword=0] Indicates whether to return the profile password. The options are as follows:
     *
     * - 0: yes
     * - 1: no
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Profile[]} return.list
     * @return {String} return.subscriberId ID of the current login subscriber.
     * @return {String} return.userToken Token of the current login user, which is automatically generated after the user is authenticated on the MEM.
     * @return {Number} return.counttotal Total number of profiles.
     */
    iptv.queryProfile = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryProfile',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'profilelist', 'list');
        });
    };
    /**
     * A client uses this interface to switch from a profile to anther profile.
     * After a subscriber starts the STB and logs in to the system or requests to switch to another profile,
     * the subscriber can select a common profile in the profile list to log in if multiple profiles exist in the system. The client invokes this interface to implement this function.
     *
     * @param {Object} req
     * @param {String} req.id ID of the profile to be switched.
     * @param {String} req.password Password of the profile to be switched. The password must be encrypted using the encryption mode specified by encryptiontype in section 3.2 "Login Interface."
     * @param {Number} [req.forceLogin=1] Indicates whether allows a profile to log in forcibly.
     *
     * - 0: no
     * - 1: yes
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Configuration}
     * @return {Number} [return.lastChannelNo] Number of the channel played when the profile exit last time.
     */
    iptv.switchProfile = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/SwitchProfile',
            data: JSON.stringify(req)
        }).done(function(resp) {
            gEventManager.off('EVENT_STB_HEARTBEAT', core.app.heartbeatApp.heartbeatCallback);
            core.session.put('core.isAuthenticated', true);
            eBase.stb.write('Tools.Record.AddRecord', JSON.stringify({
                key: 'currentPlayingPIP',
                value: -1,
                type: 1
            }));
            utils.rename(resp, 'configurations', 'list');
            core.app.vodApp.resetVodlist();
            core.app.playbillApp.cleanPlaybillList();
            core.appReadyTrigger.resetData();

            event.trigger('CORE_PROFILE_LOGIN_SUCCESS', _.extend(resp, {
                profileId: req.id
            }));
        });
    };

    /**
     * Create new profile
     * @param {Object} req
     * @param {String} req.name Profile name.
     * @param {String} [req.identityid] Profile ID, specifies a common profile. If the request does not contain this parameter, the system assigns one automatically.
     * @param {String} [req.introduce] Profile introduce.
     * @param {String} [req.password] Profile password.If the request does not contain this parameter, the MEM assigns the password.
     * @param {Number} [req.quota] Consumption limit. The value -1 indicates that the consumption is not limited.If the request does not contain this parameter, the default value -1 is used.
     * @param {String} [req.logourl] Avatar URL. The URL contains only the avatar's file name, such as 0.png.
     * @param {String} [req.levels] Parental control level of a profile. The value -1 indicates the lowest level. Separate multiple values with a comma (,).A larger value indicates a higher level. The value varies depending on the content rating systems in different countries.If the request does not contain this parameter, the default value -1 is used.
     * @param {String} [req.profileid] ID of a shared profile. The value can be the MultiProfile object ID of other profiles, which allows the profile to share the MultiProfile object data of the other profiles. If the value is not specified, the system creates a MultiProfile object for the profile.This parameter is reserved.
     * @param {Number} [req.hidemessage] Indicates whether TVMS messages will be displayed.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - Default value 0 is used.
     *
     * @param {String} [req.template] Template name. Leave this parameter blank if no module is involved.
     * @param {Number} [req.lang] Language. If the request does not contain this parameter, the default value 1 is used.
     * @param {String} [req.mobilePhone] Subscriber's contact information (mobile number).
     * @param {Number} [req.reviceSMS] Indicates whether SMS messages will be received.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - Default value 0 is used.
     *
     * @param {Number} [req.needSubscriberCnfmFlag] Indicates whether the subscription password is required during product subscription.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - Default value 1 is used.
     *
     * @param {String} [req.email] Profile email address.
     * @param {Number} [req.isDisplayInfoBar] Indicates whether Info Bar is displayed on the client during channel switch.The channel details information is displayed on the info bar, for example, channel name, channel number, program start time, and program end time.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - Default value 0 is used.
     *
     * @param {Number} [req.channellistType] Default channel list type.The options are as follows:
     *
     * - 1: system channel list
     * - 2: self-defined channel (subscribers define the channel number) list
     * - Default value 1 is used.
     *
     * @param {Number} [req.profilePINEnable] Indicates whether the profile password is required during login.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - Default value is 1.
     *
     * @param {Number} [req.multiscreenEnable] Indicates whether a profile is allowed to log in to multiple terminals at the same time.The options are as follows:
     *
     * - 0: yes
     * - 1: no
     * - The default value is 0.
     *
     * @param {Number} [req.purchaseEnable] Indicates whether subscription is supported.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - The default value is 1.
     *
     * @param {String} [req.loginName] Globally unique registered subscriber name.The value consists of letters, digits, and underscores.The value is defined by the subscriber.If the subscriber does not define the value, the value is the same as the profile ID by default.
     * @param {String} [req.location] Profile location.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.addProfile = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/AddProfile',
            data: JSON.stringify(req)
        });
    };

    /**
     * Update/Modify profile information.
     * @param {Object} Param refer to {eBase.iptv.addProfile}
     *
     * @return {eBase.core.Deferred}
     */
    iptv.updateProfile = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/ModifyProfile',
            data: JSON.stringify(req)
        });
    };

    /**
     * Delete a profile
     * @param {Object} req
     * @param {String} req.id Profile ID. Separate multiple values with a comma (,).
     *
     * @return {eBase.core.Deferred}
     */
    iptv.deleteProfile = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/DelProfile',
            data: JSON.stringify(req)
        });
    };
    /**
     * Configure the default profile bound to a device, If the default profile has been created, the subscriber logs in to the client using the default profile after the device is authenticated.
     * @param {Object} req
     * @param {String} req.deviceid Device logical ID.
     * @param {String} [req.userIdentityId] Profile ID.
     *
     * - If the request contains this parameter, the client uses this interface to create a default profile. The value is the ID of the profile to be bound to the device.
     * - If the request does not contain this parameter, the binding relationship between the device and the default profile is deleted.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.setDefaultProfile = function(req) {
        utils.rename(req, 'userIdentityId', 'useridentityid');

        return utils.jsonAjax({
            url: '/EPG/JSON/SetDefaultProfile',
            data: JSON.stringify(req)
        });
    };
    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * This interface is used to add Personal Video Recording (PVR) tasks, including Client Personal Video Recording (cPVR) and Network Personal Video Recording (nPVR) tasks.
     *
     * @param {Object} req
     * @param {Number} req.pvrType Type of the supported recording service.
     *
     * - 1: cPVR
     * - 2: nPVR
     * @param {Number} [req.checkExist=1] Indicates whether to check whether a duplicate task exists.
     *
     * - 0: no
     * - 1: yes
     * @param {String} [req.channelNo] Channel number, which comes from the channo attribute of the Channel object.
     * If a client in the OTT domain creates a cPVR task, the MEM finds the channel media resources of the first IPTV domain to which a channel belongs based on the channelNo parameter.
     * If the channel does not belong to the IPTV domain, the cPVR task fails to be created.
     * The channelNo and mediaId parameters are exclusive. If both parameters exist, the MEM uses the mediaId parameter preferentially and recommends the mediaId parameter to the client.
     * @param {String} [req.mediaId] ID of a channel media resource, which comes from the mediaId attribute of the Channel object.
     * If a cPVR task is created for the channel media resource whose ID belongs to the OTT domain, the MEM will display a message indicating that the task fails. Ensure that the ID of the input media resource must belong to the IPTV domain.
     * The channelNo and mediaId parameters are exclusive. If both parameters exist, the MEM uses the mediaId parameter preferentially.
     * @param {String} [req.programId=-1] Program ID.
     * If a program PVR task is added, the parameter is contained.
     * If a channel PVR task (by time segment or immediate) is added, the parameter is not contained. The default value is -1.
     * @param {String} [req.beginTime] Start time of a recording task.
     * If the task is a program recording task and the request does not contain this parameter, the program start time is the recording start time. If a program has started, the current time is the recording start time.
     * If the recording task is set by time segment, the recording start time is specified by this parameter.
     * @param {String} [req.endTime] End time of a recording task.
     * If the task is a program recording task and the request does not contain this parameter, the program end time is the recording end time.
     * If the recording task is set by time segment, the recording start time is specified by this parameter.
     * @param {String} [req.deviceId] Device logical ID. This parameter is valid only for cPVR tasks.
     * If the request does not contain this parameter, the cPVR task is created on current local device.
     * If the request contains this parameter, the cPVR task is created on a remote device.
     * @param {Number} [req.endOffset=0] Duration that can be delayed for a program recording task after a program ends.
     * Unit: minute
     * @param {String} [req.stbPvrId] Internal task ID generated by the STB.
     * This parameter is used when a cPVR task created in the Digital Video Broadcasting (DVB) mode on a hybrid STB is synchronized to the EPG server.
     * @param {String} [req.stbPeriodId] Series or periodic parent task ID generated by the STB.
     * Currently, this parameter is available only for hybrid STBs. A parent task created in the DVB mode on an STB must be synchronized to the EPG server. Separate multiple parent tasks with a comma (,).
     * @param {String} [req.profileId] ID of the profile for adding a PVR task.
     * If the request does not contain this parameter, the PVR task is created by the current profile.
     * @param {Number} [req.isPltvtoPVR=0] Indicates whether the cPVR task is converted from a local TSTV task. This parameter is used in the Hybrid TV mode.
     *
     * - 0: no
     * - 1: yes
     * @param {Number} [req.status] Recording status. This parameter is used when a cPVR task created in the DVB mode on a hybrid STB is synchronized to the background.
     *
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * 8: recorded partially
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.pvrId PVR task ID. This parameter value is returned only when a task is added successfully.
     */
    iptv.addPvr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/AddPVR',
            data: JSON.stringify(req)
        });
    };
    /**
     * This interface is used to update PVR tasks, including the start time or end time.
     * @param {Object} req
     *
     * @param {String} req.pvrId PVR task ID.
     * For a cPVR task, either pvrId or stbPvrId is mandatory.
     * For an nPVR task, this parameter is mandatory.
     * @param {String} req.beginTime Start time of the PVR task. The format is yyyyMMddHHmmss.
     * Either beginTime or endTime is mandatory.
     * If the request does not contain this parameter, only the end time is updated.
     * @param {String} req.endTime End time of the PVR task. The format is yyyyMMddHHmmss.
     * Either beginTime or endTime is mandatory.
     * If the request does not contain this parameter, only the start time is updated.
     * @param {Number} [req.type] Recording type. Indicates whether to query nPVR tasks or cPVR tasks.
     *
     * - 1: cPVR
     * - 2: nPVR
     * @param {String} [req.stbPvrId] Internal task ID generated by the STB. This parameter is valid only for cPVR tasks.
     */
    iptv.updatePvr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/UpdatePVR',
            data: JSON.stringify(req)
        });
    };
    /**
     * This interface is used to delete PVR tasks. Multiple tasks can be deleted at the same time.
     * If deviceId corresponding to the cPVR task to be deleted is the same as that corresponding to the current device, the MEM will delete the cPVR task directly from the MEM database.
     * If deviceId corresponding to the cPVR task to be deleted is inconsistent with that corresponding to the current device, the MEM invokes this interface to delete the cPVR task when the client finds that the PVR version number is changed after a terminal sends heartbeat messages.
     *
     * @param {Object} req
     * @param {String} [req.pvrId] PVR task ID.
     * The tasks specified by this parameter are to be deleted. Separate multiple IDs with a comma (,).
     * If the request contains pvrId, other parameters are invalid.
     * If the request does not contain pvrId, the deletion is performed based on the stbPvrId value.
     * If request does not contain pvrId and stbPvrId, the deletion is performed based on the status and type values.
     * @param {String} [req.status] Recording task status.
     * Separate multiple values with a comma (,).
     * The options are as follows:
     *
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * If the request does not contain this parameter, all tasks are deleted.
     * @param {Number} [req.type] Indicates whether the task is an nPVR task or cPVR task. This parameter is valid only when the request contains only status but does not contain pvrId and stbPvrId.
     *
     * - 1: cPVR
     * - 2: nPVR
     * If the request does not contain this parameter, both cPVR and nPVR tasks are deleted.
     * @param {String} [req.stbPvrId]    O    String(<=64)    Internal task ID generated by the STB. The request contains this parameter only for a cPVR task.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.deletePvr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/DeletePVR',
            data: JSON.stringify(req)
        });
    };
    /**
     * If an nPVR product is priced based on network storage, this interface is used to query nPVR network space, including the total space, used space, and available space.
     * If an nPVR product is priced based on recording duration, this interface is used to query nPVR recording duration, including the total purchased duration, used duration, and available duration.
     *
     * @param {Object} req
     * @param {Number} [req.type=0] Information to be queried.
     *
     * - 0: total space
     * - 1: used space
     * - 2: available space
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.space If an nPVR product is priced based on network storage, the value is the storage space in MB.
     * If an nPVR product is priced based on recording duration, the value is the duration in minutes.
     */
    iptv.getPvrSpace = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryPVRSpace',
            data: JSON.stringify(req || {
                type: 0
            })
        });
    };
    /**
     * This interface is used to query PVR tasks by filter criteria such as the recording time range, recording task name, device, and profile ID.
     *
     * @param {Object} req
     * @param {String} [req.status] Recording task status.
     * Separate multiple values with a comma (,).
     *
     * - -3: to be deleted (This status is valid only for cPVR tasks.)
     * - -2: not delivered (This status is valid only for cPVR tasks.)
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * If the request does not contain this parameter, all tasks are queried.
     * The options -3 and -2 are available only for remote PVR tasks. For example, if a cPVR task is added or deleted on an OTT device, the MEM must provide this interface for the STB to obtain this remote PVR task.
     * @param {Number} [req.type] Type of tasks to be queried.
     *
     * - 0: PVR tasks added by the current profile. If the current profile is the super profile, PVR tasks added by all profiles are queried.
     * If filterlist is not set to a logic device ID, cPVR tasks of all devices are returned.
     * - 1: PVR tasks added by all profiles are queried.
     * If filterlist is not set to a logic device ID, all cPVR tasks of the login device are returned.
     * This parameter is mandatory if pvrType is set to 1.
     * If pvrType is set to 2 and type is not specified, the MEM returns PVR tasks based on nPVR sharing policies. If nPVR tasks are shared, return nPVR tasks created by all profiles. If nPVR tasks are not shared, return nPVR tasks created by all profiles for the super profile, and nPVR tasks created by common profiles for common profiles.
     * If pvrType is set to 2 and nPVR tasks are not shared, the MEM ignores the value of type input by the terminal, and returns nPVR tasks created by all profiles for the super profile, and nPVR tasks created by common profiles for common profiles.
     * @param {Number} req.count Total number of tasks obtained in one query.
     * If all tasks are obtained, the value is -1.
     * @param {Number} req.offset Start position for querying tasks.
     * The value starts from 0.
     * @param {Number} [req.pvrType] Recording type.
     * Indicates whether to query nPVR tasks or cPVR tasks.
     * The options are as follows:
     *
     * - 1: cPVR
     * - 2: nPVR
     * If the request does not contain this parameter, all tasks are queried.
     * @param {Number} [req.expandSubTask=1] Indicates whether to query series or periodic cPVR an nPVR subtasks.
     * The options are as follows:
     *
     * - 0: no. That is, series or periodic cPVR an nPVR parent tasks are queried first. When the subscriber clicks a parent task, the client queries the subtasks of the parent task.
     * - 1: yes.
     * @param {String} [req.orderType] Sorting mode of found tasks.
     *
     * - 0: ascending order of start time
     * - 1: descending order of start time
     * - 2: ascending order of task names
     * - 3: descending order of task names
     * - 4: ascending order of genre names
     * - 5: descending order of genre names
     * If the request does not contain the parameter, the PVR tasks are sorted by task start time in ascending order.
     * @param {Array} [req.filterlist] List of {eBase.data.NamedParameter} Filter criteria.
     * The options are as follows for the key parameter:
     *
     * - STARTTIME: recording start time
     * - ENDTIME: recording end time
     * - PVRNAME: task name
     * - DEVICE: device
     * - PROFILEID: profile ID
     * - GENRE: genre name
     * - COUNTRY: country where a program is produced
     * If STARTTIME and ENDTIME are selected, the PVR tasks whose recording time range overlaps with the time segment from the STARTTIME value to the ENDTIME value are queried.
     * If only STARTTIME is selected, the PVR tasks after the start time, including those being recorded (that is, the PVR tasks whose end time is later than the STARTTIME value) are queried.
     * If only ENDTIME is selected, the PVR tasks before the end time (that is, the PVR tasks whose end time is earlier than or equal to the ENDTIME value) are queried.
     * If DEVICE is selected, the cPVR tasks stored on the devices are queried. Multiple devices are separated with a comma (,).
     * If PROFILEID is selected, the PVR tasks created by the profile are queried.
     * If GENRE is selected, recorded programs can be filtered by genre. Separate multiple values with a comma (,).
     * If COUNTRY is selected, recorded programs can be filtered by production country or region. The values are codes in the ISO3166-1 standard. Separate multiple values with a comma (,).
     * Remarks:
     * The STARTTIME and ENDTIME values are in the yyyyMMddHHmmSS format.
     * If the values of the filterlist and excluderlist parameters contain the same key value, the key value in the filterlist value is used and the one is the excluderlist value is ignored.
     * @param {Array} [req.excluderlist] Exclusive conditions. The key parameter of NamedParameter indicates the search criteria.
     * The options are as follows:
     *
     * - COUNTRY: country where a program is produced
     * If COUNTRY is selected, recorded programs can be filtered by production country or region. The values are codes in the ISO3166-1 standard.
     * Separate multiple values with a comma (,).
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PVRTask}
     * @return {Number} return.counttotal Total number of tasks.
     */
    iptv.queryPvr = function(req) {
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryPVR',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'pvrlist', 'list');
        });
    };
    /**
     * This interface is used to query PVR tasks by task ID. Multiple tasks can be queried at the same time.
     *
     * @param req {Object}
     * @param req.pvrIds {String} PVR task ID. Separate multiple values with a comma (,).
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PVRTask}
     */
    iptv.queryPvrByIds = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryPVRById',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'pvrlist', 'list');
        });
    };
    /**
     * This interface is used to query series or periodic cPVR and nPVR subtasks. A client can use this interface to query specified series or periodic cPVR and nPVR subtasks by recording status.
     *
     * @param {Object} req
     * @param {String} req.periodPVRId Periodic PVR task ID.
     * @param {String} [req.status] Recording task status.
     * Separate multiple values with a comma (,).
     * The options are as follows:
     *
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * If the request does not contain this parameter, tasks in all statuses are queried.
     * @param {Number} req.count Total number of tasks obtained in one query.
     * If all tasks are obtained, the value is -1.
     * @param {Number} req.offset Start position for querying tasks. The value starts from 0.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PVRTask}
     */
    iptv.querySubPvr = function(req) {
        req = req || {};
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/QuerySubPVR',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'tasklist', 'list');
        });
    };
    /**
     * This interface is used to delete series or periodic cPVR and nPVR subtasks. A client can use this interface to delete only subtasks but not the parent tasks.
     *
     * @param {Object} req
     * @param {String} req.periodPVRId Periodic PVR task ID.
     * @param {String} [req.status] Recording task status.
     * Separate multiple values with a comma (,).
     * The options are as follows:
     *
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * If the request does not contain this parameter, tasks in all statuses are deleted.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.deleteSubPvr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/DeleteSubPVR',
            data: JSON.stringify(req)
        });
    };
    /**
     * This interface is used to query series or periodic cPVR and nPVR tasks.
     *
     * @param {Object} req
     * @param {Number} [req.type] Indicates whether to query nPVR tasks or cPVR tasks.
     *
     * - 1: cPVR
     * - 2: nPVR
     * If the request does not contain this parameter, all tasks are queried.
     * @param {Number} [req.seriesType]  Type of the series PVR task.
     *
     * - 0: recording by time segment
     * - 1: recording by keywords
     * If the request does not contain this parameter, all tasks are queried.
     * @param {String} [req.status] Parent task status.
     *
     * - -3: to be deleted
     * - -2: not delivered
     * - 0: delivered
     * If the request does not contain this parameter, all tasks are queried.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PVRTask}
     */
    iptv.queryPeriodPvr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryPeriodPVR',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'tasklist', 'list');
        });
    };
    /**
     * This interface is used to add, modify, and delete PVR tasks.
     *
     * @param {Object} req
     * @param {String} req.action Operation type.
     * The options are as follows:
     *
     * - ADD: Add a task.
     * - UPDATE: Modify a task.
     * - DELETE: Delete a task. Only parent tasks are deleted.
     * - PREADD: Add a scheduled task. Parent tasks and subtasks are not generated.
     * @param {eBase.data.PeriodPVRTask} [req.task] Periodic PVR tasks.
     * When action is ADD or UPDATE, this parameter is mandatory.
     * When action is DELETE, all PVR tasks are deleted if the request does not contain this parameter, and specified tasks are deleted if the request contains this parameter.
     * @param {Number} [req.programSerialEnable=0] Indicates whether to add a series PVR task based on the relationships between series and episodes on a program.
     * This parameter is valid only when action is ADD.
     * The options are as follows:
     *
     * - 0: no. A series PVR task is added only when the program name contains the entire keyword.
     * - 1: yes. A series PVR task is added only when the program name contains the entire keyword.
     * @param {Number} [req.checkExist=1] Indicates whether to check whether a duplicate task exists when a PVR subtask is created.
     * The options are as follows:
     *
     * - 0: no
     * - 1: yes (by default)
     * @param {Number} [req.strategyType=1] Policy for creating a parent nPVR task. The options are as follows:
     *
     * - 0: If the available space or duration is insufficient for the recording of a program, the parent task and subtasks are not created.
     * - 1: If the available space or duration is insufficient for the recording of a program, the parent task and subtasks are created.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.managePeriodPvr = function(req) {
        utils.rename(req.task, 'seriesId', 'seriesID');
        utils.rename(req.task, 'channelNo', 'channelno');

        return utils.jsonAjax({
            url: '/EPG/JSON/PeriodPVRMgmt',
            data: JSON.stringify(req)
        });
    };

    /**
     * This interface used to update only cPVR task statuses.
     *
     * @param {Object} req
     * @param {String} [req.pvrId] Task ID.
     * Either pvrId or stbPvrId is mandatory.
     * @param {Number} req.status Recording task status.
     *
     * The options are as follows:
     *
     * - -1: not recorded
     * - 0: recorded
     * - 1: recording
     * - 2: recording failed
     * - 3: delivered
     * @param {String} [req.endTime] Task end time.
     * This parameter is mandatory only when status is 3.
     * The Media Entertainment Middleware (MEM) compares the endTime value reported by the STB with the end time in the database. If the two end time is different.
     * The format is yyyyMMddHHmmss.
     * @param {String} [req.stbPvrId] Internal task ID generated by the STB.
     * Either pvrId or stbPvrId is mandatory.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.updatePvrStatus = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/UpdatePVRStatus',
            data: JSON.stringify(req)
        });
    };

    return core;
})(eBase);
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Search hot keywords that are most frequently searched by subscribers.
     * @param {Object} req
     * @param {String} req.key Search keyword.
     * @return {String[]}
     */
    iptv.queryAssociatedKeyword = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetAssociatedKeywords',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'keylist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query historical consumption records of a profile.
     * If the super profile sends a query request, historical consumption records of all family members will be returned.
     * If a common profile sends a query request, only historical consumption records of the common profile will be returned.
     *
     * @param {Object} req
     * @param {String} req.month Month for querying historical consumption records. The format is yyyyMM.
     * @param {String} [req.profileId] IDs of profiles whose consumption records are queried.
     * Separate multiple IDs with a comma (,).
     * If the request does not contain this parameter, consumption records of all profiles are queried.
     * @param {String} [req.orderMode=0] Subscription mode.
     *
     * - 0: all
     * - 1: postpaid
     * - 2: using IPTV bonus points
     * - 10: prepaid
     * @param {Number} [req.productType] Product type.
     *
     * - 0: duration-based
     * - 1: times-based
     * If the request does not contain this parameter, all products are queried.
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Bill[]} return.list List of bill.
     */
    iptv.queryBill = function(req) {
        utils.rename(req, 'profileId', 'profileID');

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryBill',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'billlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Before the Silverlight terminal plays content, this interface needs to be invoked to obtain the license trigger of the PlayReady CA.
     *
     * @param {Object} req
     * @param {String} req.contentId Content ID.
     * @param {String} req.contentType Content type.
     *
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - SUBJECT
     * - PROGRAM
     * @param {Number} req.businessType Type of a service.
     *
     * - 0: combination of several service types
     * - 1: VOD
     * - 2: LIVETV
     * - 3: PPV
     * - 4: TSTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 8: NPVR
     * - 9: local TSTV
     * @param {String} req.mediaId Content media ID.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Trigger}
     */
    iptv.queryCATrigger = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'businessType', 'businesstype');
        utils.rename(req, 'mediaId', 'mediaid');

        return utils.jsonAjax({
            url: '/EPG/JSON/GetCATrigger',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'triggers', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Get cast detail by castIds.
     * @param {Object} req
     * @param {Array} req.castIds castIds array.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.CastInfo}
     */
    iptv.queryCast = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetCastDetail',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'casts', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Get category List under category
     * @param {Object} req
     * @param {String} req.categoryId Category ID.
     * @param {String} req.type EPG category type.
     * Separate multiple values with a comma (,).
     * The options are as follows:
     *
     * - VOD
     * - AUDIO_VOD
     * - VIDEO_VOD
     * - CHANNEL
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - MIX
     * - VAS
     * - PROGRAM
     * @param {Number} [req.offset=0] Record index offset, which starts from 0.
     * @param {Number} [req.count=1000] Number of returned records.
     * @param {Object}[req.properties] Extend property.The extended parameter properties in a request specifies the list of content metadata fields that need to be returned or that do not need to be returned through the interface.

     categoryId: '-1',
     properties: [{
        name: 'category',
        include: 'id,type,name'
     }]

     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.Category}
     */
    iptv.queryCategory = function(req) {
        utils.paginate(req);
        utils.rename(req, 'categoryId', 'categoryid');

        req.categoryid = req.categoryid || -1;
        req.type = req.type || ['VOD', 'AUDIO_VOD', 'VIDEO_VOD', 'CHANNEL', 'AUDIO_CHANNEL', 'VIDEO_CHANNEL', 'MIX', 'VAS', 'PROGRAM'].join(';');

        return utils.jsonAjax({
            url: '/EPG/JSON/CategoryList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'categorylist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query CDRs. CDRs can be queried by month.
     *
     * @param {Object} req
     * @param {String} req.month Month for querying CDRs. The format is yyyyMM.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.CDR}
     */
    iptv.queryCdr = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryCDR',
            data: JSON.stringify(req)
        }).done(function(resp) {
            resp.list = [];
            if (resp.namelist || resp.timelist || resp.feelist) {
                resp.list = [];
                var _namelist = resp.namelist.split(',');
                var _timelist = resp.timelist.split(',');
                var _feelist = resp.feelist.split(',');
                var _contenttypelist = resp.contenttypelist.split(',');
                var _typelist = resp.typelist.split(',');

                for (var i = 0; i < _namelist.length; i++) {
                    resp.list.push({
                        name: _namelist[i],
                        time: _timelist[i],
                        fee: _feelist[i],
                        contenttype: _contenttypelist[i],
                        type: _typelist[i]
                    });
                }

                delete resp.namelist;
                delete resp.timelist;
                delete resp.feelist;
                delete resp.contenttypelist;
                delete resp.typelist;
            }
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * A client uses this interface to query channel lists.
     * When a subscriber browses the channel category, the client can invoke this interface to obtain and display the list of channels under this EPG category. Generally, the client requests the list of all channels, and does not input the category ID or sets the input category ID to -1.
     *
     * @param {Object} req
     * @param {String} [req.categoryId] Category ID, which can be used to query channel lists from this category. If the request does not contain this parameter, all channels are queried.
     * @param {Number} [req.count] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {Number} [req.offset] Start position for querying content.
     * The default value is 0, indicating the content item.
     * @param {String} [req.contentType] Content type. The options are as follows:
     *
     * - CHANNEL
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * @param {Number} [req.domain] Type of a domain to which the content belongs. The options are as follows:
     *
     * - 0: OTT
     * - 1: Mobile
     * - 2: IPTV
     * If a client does not carry this parameter, this parameter is set to the domain to which the current client belongs by default.
     * @param {String} [req.channelNamespace] Naming space used for querying a channel. If this parameter is not specified, the MEM uses the defaultChannelNS parameter during subscriber authentication.
     * @param {String} [req.properties] The properties in returned {eBase.data.Channel}. Example: 'id,type,name'
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Channel}
     */
    iptv.queryChannel = function(req) {
        utils.rename(req, 'categoryId', 'categoryid');
        utils.rename(req, 'contentType', 'contenttype');

        if (req && req.properties) {
            req.properties = [{
                name: 'channel',
                include: req.properties
            }];
        }

        return utils.jsonAjax({
            url: '/EPG/JSON/ChannelList',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'channellist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client invokes this interface to obtain content in a category.
     * The Interface to Query VOD Lists, Interface to Query Channel Lists, or Interface to Query VAS Lists can be invoked to obtain the specific content.
     * If hybrid content items (for example, VOD content and live TV channels) are bound to a category, a client needs to invoke this interface to obtain all content items in the category.
     *
     * @param {Object} req
     * @param {String} req.categoryId ID of a leaf category or UMS category.
     * @param {Number} [req.count=-1] Total number of content items obtained for one query. To obtain all content items, set this parameter to -1.
     * @param {Number} [req.offset=0] Start position for querying content items. The value starts from 0, indicating the first content item.
     * @param {Number} [orderType=0] Mode by which the content items are sorted.
     *
     * - 0: by content name in ascending order
     * - 1: by the operator-defined content number in ascending order
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of content.
     */
    iptv.queryContent = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetContentListByCategory',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client invokes this interface to obtain content in a category.
     * The Interface to Query VOD Lists, Interface to Query Channel Lists, or Interface to Query VAS Lists can be invoked to obtain the specific content.
     * If hybrid content items (for example, VOD content and live TV channels) are bound to a category, a client needs to invoke this interface to obtain all content items in the category.
     *
     * @param {Object} req
     * @param {String} req.categoryId ID of a leaf category or UMS category.
     * @param {Number} [req.count=-1] Total number of content items obtained for one query. To obtain all content items, set this parameter to -1.
     * @param {Number} [req.offset=0] Start position for querying content items. The value starts from 0, indicating the first content item.
     * @param {Number} [orderType=0] Mode by which the content items are sorted.
     *
     * - 0: by content name in ascending order
     * - 1: by the operator-defined content number in ascending order
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of content.
     */
    iptv.queryChoosedContent = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetChoosedContents',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Get CustomConfig info of system.
     * @param {Object} req
     * @param {String} [req.key] key parameter, for 2 or more keys using <b>"key1,key2,ke3"</b>. default is query all of the key info.
     * @param {String} req.queryType query type.
     *
     * - 0: Terminal config
     * - 1: Template config
     * - 2: EPG system config
     *
     * @param {String} [req.isFuzzyQuery] when queryType is 0 or 1, this parameter works.<br>
     *
     * - 0: precision query
     * - 1: fuzzy query
     *
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.CustomConfig}
     */
    iptv.queryCustomConfig = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetCustomizeConfig',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'extensionInfo', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {};

    /**
     * Obtain content details based on content ID in batches.The content includes VOD programs, channels, categories, VASs, and programs.
     * @param {Object} req
     * @param {String} [req.vod] VOD ID. Separate multiple values with a comma (,).
     * @param {String} [req.channel] Channel ID. Separate multiple values with a comma (,).
     * @param {String} [req.category] Category ID.Separate multiple values with a comma (,).The default value is -1, indicating root category.
     * @param {String} [req.vas] VAS ID. Separate multiple values with a comma (,).
     * @param {String} [req.playbill] Playbill ID. Separate multiple values with a comma (,).

     * @param {String} [req.idType] Type of content ID.The options are as follows:
     *
     * - 0: content internal key, for example, VOD indicates the id attribute of a VOD object, whose value is generated by the MEM.
     * - 1: content external key, for example, VOD indicates the foreignsn attribute of a VOD object, whose value is generated by the UMS.
     * - The default value is 0.
     *
     * @param {String} [req.filterType] Filtering type of VOD programs and programs. The options are as follows:
     *
     * - 0: by subscriber attribute (for example, area and regional node) and terminal capability.
     * - 1: do not set filtering types. If no physical media resource is available due to subscriber attributes and terminal capabilities, the MEM returns mediafiles information of the VOD object, but the value of isPlayable of the VOD program is 0.
     * - If the parameter is not carried, the default value 0 is used.
     *
     * @param {Object}[req.properties] Extend property.The extended parameter properties in a request specifies the list of content metadata fields that need to be returned or that do not need to be returned through the interface.

                vod: '1001,1002',
                channel: '1,2',
                properties: [{
                    name: 'vod',
                    include: 'id,name'
                }, {
                    name: 'channel',
                    include: 'id,channo,name'
                }]

     * @return {eBase.data.Vod[]|eBase.data.Channel[]|eBase.data.Category[]|eBase.data.Playbill[]}
     */
    iptv.queryDetail = function(req) {
        req.filterType = 2; //需求变更
        return core.utils.jsonAjax({
            url: '/EPG/JSON/ContentDetail',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query lists of devices bound to subscribers' accounts.
     * @param {Object} req
     * @param {String} req.userId Subscriber ID or profile ID. If the value is a profile ID, the client obtains the subscriber to which the profile belongs then obtains the devices to which the subscriber is bound.
     * @param {String} [req.deviceType] Device type group.The options are as follows:
     *
     * - 0: STB
     * - 1: PC Client
     * - 2: OTT device, including PC plug-in, iOS, and Android devices
     * - 3: mobile terminals for MTV solution (not supported)
     * - 4: all
     * - If the request does not contain this parameter, the device type group where the subscriber has logged in is used.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Device}
     */
    iptv.queryDevice = function(req) {
        utils.rename(req, 'userId', 'userid');

        return utils.jsonAjax({
            url: '/EPG/JSON/GetDeviceList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'deviceList', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query the discounted product price. The original price and discounted price are returned.
     * @param {Object} req
     * @param {String[]} req.productIds Product IDs. Sample:productIds: ['1', '4']
     * @return {eBase.data.ProductDiscountInfo[]}
     */
    iptv.queryDiscount = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetDiscountInfos',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'discountInfos', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.data.Genre
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query the genre of specified content
     * Result sample:

         list: Array[25]

         0: Object
         genreId: "1901"
         genreName: "Action"
         genreType: "7"

         1: Object
         genreId: "1902"
         genreName: "Comedy"
         genreType: "7"

     * @param {Object} req
     * @param {String} [req.genreType] Specified type of genre.The options are as follows:
     *
     * - 1: music, which is used by audio content
     * - 2: movie and TV, which is used by video content
     * - 3: application
     * - 4: image
     * - 5: theme
     * - 6: information, which is used by VAS content
     * - 7: programs in a channel, which is used when program genre information is queried
     * - default All
     *
     * @param {String} [req.genreIds] Query genre information. If the parameter is specified, ignore genreType.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Genre}
     */
    iptv.queryGenre = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetGenreList',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'genres', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * When a subscriber searches for content, the system can list relevant hot search keywords.
     * For example, if key in the request is friend, the interface returns the hottest search keywords starting with friend, such as friends, friendship, and friendster.
     *
     * @param {Object} req
     * @param {Number} req.count Total number of tasks obtained in one query.
     * If all tasks are obtained, the value is -1. It is recommended that the value not exceed 50.
     * @param {String} req.key Search keywords.
     *
     * @return {eBase.core.Deferred}
     * @return {String[]} return.list List of hot keys.
     */
    iptv.queryHotKey = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryHotKey',
            data: JSON.stringify(req)
        }).done(function(resp) {
            if (resp.result) {
                resp.list = resp.result.split(',');
            } else {
                resp.list = [];
            }
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Search hot keywords that are most frequently searched by subscribers.
     *
     * @param {Object} req
     * @param {String} [req.count] Number of keywords obtained. Default is 10.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {String}
     */
    iptv.queryHotKeyword = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetHotKeywords',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'keylist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A subscriber uses this interface to query the hottest Catch-up TV programs.
     * A client uses this interface to display the hottest Catch-up TV programs.
     *
     * @param {Object} req
     * @param {Number} req.count Total number of content items obtained for one query.
     * If all content items are obtained, the value is -1.
     * @param {Number} req.offset Start position for querying content. The value starts from 0, indicating the first content item.
     * @param {Number} [req.boardType=0] Rankings type.
     * The options are as follows:
     *
     * - 0: total rankings
     * - 1: monthly rankings
     * - 2: weekly rankings
     * - 3: daily rankings
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Playbill[]} return.list
     */
    iptv.queryHotProgram = function(req) {
        req = req || {};
        req.boardType = req.boardType || 0;
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryHotProgram',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'playbilllist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query system Mosaic channels
     * @param {Object} req
     * @param {String} [req.offset] Record index offset, which starts from 0. Default is 0.
     * @param {String} [req.count] Number of returned records. Default is -1(All).
     * @param {String} [req.mosaicChannelId] ID of a Mosaic channel. If the parameter is specified, search for only the Mosaic channel matching the ID.
     * @return {eBase.data.MosaicChannel[]}
     */
    iptv.queryMosaic = function(req) {
        req = req || {};
        req.count = req.count || -1;
        req.offset = req.offset || 0;

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryMosaicChannel',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'mosaicChannels', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to obtain program lists of a specified channel.
     * When a subscriber views the channel information, the client can invoke this interface to obtain and display program lists of this channel.
     *
     * @param {Object} req
     * @param {String} req.channelId Channel ID.
     * @param {String} req.beginTime Start time. Format: yyyyMMddhhmmss
     * @param {String} req.endTime End time. Format: yyyyMMddhhmmss
     * @param {Number} req.type Program type.
     *
     * - 0: live TV programs, excluding expired programs
     * - 1: recorded program
     * - 2: all live TV programs, including expired programs
     * @param {Number} req.count Total number of content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {Number} req.offset Start position for querying content.
     * @param {Number} [req.isFillProgram=0] Indicates whether to fill in programs.
     *
     * - 0: no
     * - 1: yes
     * @param {Array} [req.filterList] Search criterion.
     * @param {Number} [req.orderType] Sorting mode.
     *
     * - 0: by click-through rate in descending order
     * - 1: by program name in ascending lexicographic order
     * - 2: by program name in descending lexicographic order
     * - 3: by program start time in descending order
     * If the parameter is not set, programs are sorted by start time by default.
     * If program start times are the same, programs are sorted by program ID in descending order.
     */
    iptv.queryPlaybill = function(req) {
        utils.rename(req, 'channelId', 'channelid');
        utils.rename(req, 'beginTime', 'begintime');
        utils.rename(req, 'endTime', 'endtime');
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/PlayBillList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'playbilllist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query the remaining bonus points and bonus point details of a subscriber.
     *
     * @param {Object} req
     * @param {String} [req.fromDate] Query start date.The format is yyyyMMDDHHmmss.
     * @param {String} [req.toDate] Query end date.The format is yyyyMMDDHHmmss.The start date must be earlier than the end date.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PointBalance}
     */
    iptv.queryPointBalance = function(req) {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/QueryPointbalance',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'detaillist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query Pay Per View (PPV) records for live TV programs of a profile.
     * This interface supports only programs subscribed by times.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.PPV}
     */
    iptv.queryPpv = function() {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/QueryPPV'
        }).done(function(resp) {
            utils.rename(resp, 'ppvlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query the information about the regions where the content is produced.
     * The value returned by the interface is a string of characters containing the production regions.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.ProduceZone}
     */
    iptv.queryProduceZone = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryProduceZone'
        }).done(function(resp) {
            var idList = resp.idList && resp.idList.split(','),
                nameList = resp.result && resp.result.split(','),
                zoneList = _.map(idList, function(id, index) {
                    return {
                        id: id,
                        name: nameList[index]
                    };
                });

            resp.list = zoneList || [];
            delete resp.result;
            delete resp.idList;
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * A client invokes this interface to query product details based on the product ID.
     *
     * @param {Object} req
     * @param {Array} req.productIds Product IDs. ['1', '4']
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Product}
     */
    iptv.queryProduct = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetProductsByID',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'productlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to obtain the parental control level list used in the system.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Rating}
     */
    iptv.queryRating = function() {
        return utils.jsonAjax({
            url: '/EPG/JSON/RatingList'
        }).done(function(resp) {
            var idList = resp.idlist.split(','),
                nameList = resp.namelist.split(','),
                ratingList = _.map(idList, function(id, index) {
                    return {
                        id: id,
                        name: nameList[index]
                    };
                });

            resp.list = ratingList || [];
            delete resp.namelist;
            delete resp.idlist;
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Obtain the list of recommended VOD programs.
     * @param {Object} req
     * @param {String} req.action Recommendation type.
     *
     * - 1 newly updated
     * - 2 newly brought online
     * - 3 to be offline
     * - 4 hottest
     * - 5 weekly rankings
     * - 6 recommended
     *
     * @param {String} req.offset Start position for querying content.The value starts from 0.
     * @param {String} req.count Total number of content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {String} [req.orderType] Mode of sorting recommended VOD programs.This parameter is valid only when action is set to 4 or 5.
     *
     * - 0 by click-through rate in descending order
     * - 1 by average rating in descending order
     *
     * If the request does not contain this parameter, VOD programs are sorted by release time in descending order.
     * @param {String} [req.categoryid] Category ID.If the request contains this parameter, all recommended VOD programs in a specified category are queried. This category can be a parent category.
     * @param {String} [req.position] Position for displaying recommended VOD programs.If the value of this parameter is not empty, the MEM queries recommended VOD programs based on the values of position and action and sends the results to the client.
     * @param {String} [req.vodtype] VOD type.
     *
     * - 0 common VOD
     * - 1 VOD series
     * - 2 all types
     *
     * default value is 2.
     * @param {String} [req.genre] VOD genre.If the request does not contain this parameter, VOD programs of all genres are queried.
     * @return {eBase.data.Vod[]}
     */
    iptv.queryRecommendVod = function(req) {
        req = req || {};
        req.offset = req.offset || 0;
        req.count = req.count || 1000;

        return utils.jsonAjax({
            url: '/EPG/JSON/RecmVodList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'vodlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Obtain series episode lists.
     * @param {Object} req
     * @param {String} req.vodId Series ID.
     * @param {String} [req.offset] Record index offset, which starts from 0.
     * @param {String} [req.count] Number of returned records. -1 means All.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Vod}
     */
    iptv.querySitcom = function(req) {
        utils.rename(req, 'vodId', 'vodid');
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/SitcomList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'vodlist', 'list');
        });
    };

    return core;

}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        session = core.session,
        utils = core.utils;
    /**
     * Query content list of SNS recommended.
     *
     * @param {Object} req
     * @param {String} req.recType Recommend type:
     *
     * - topshare_total All top share
     * @param {Number} req.count
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Content[]} return.list
     */
    iptv.querySnsHot = function(req) {
        req.authorization = 'Basic ' + utils.base64.encode(session.get('authId') + ',token=' + session.get('userToken'));

        return utils.jsonAjax({
            url: '/EPG/JSON/QuerySNSHotContent',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        session = core.session,
        utils = core.utils;
    /**
     * Query content list of SNS recommended.
     *
     * @param {Object} req
     * @param {String} req.contentType Content type.
     *
     * - VOD
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - CHANNEL
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - PROGRAM
     * @param {String} req.recType Recommend type:
     *
     * - friend_share Shared by friend.
     * - my_follow Followed by user.
     * @param {Number} req.count
     *
     * @return {eBase.core.Deferred}
     * @return {eBase.data.Content[]} return.list
     */
    iptv.querySnsRecommend = function(req) {
        req.authorization = 'Basic ' + utils.base64.encode(session.get('authId') + ',token=' + session.get('userToken'));

        return utils.jsonAjax({
            url: '/EPG/JSON/QuerySNSRecContent',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query subscribed content of a profile.
     * @param {Object} req
     * @param {String} [req.contenttype] Content type. Separate multiple values with a comma (,).The options are as follows:
     *
     * - VAS
     * - CHANNEL
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - WEB_CHANNEL
     * - SUBJECT
     * - VOD
     * - AUDIO_VOD
     * - VIDEO_VOD
     * - PROGRAM
     * - Default is All.
     *
     * @param {String} [req.offset] Record index offset, default is 0.
     * @param {String} [req.count] Number of returned records,default is -1(All).
     * @param {String} [req.order] Mode by which the results are sorted. Separate multiple values with a comma (,).The options are as follows:
     *
     * - 1: by name in ascending order
     * - 2: by name in descending order
     * - 3: by time in ascending order
     * - 4: by time in descending order
     * - 5: by type in ascending order
     * - 6: by type in descending order
     * - 7: by channel number in ascending order
     * - 8: by subscription expiration time in ascending order
     * - 9: by subscription expiration time in descending order
     * - If the request does not contain this parameter, the results are not sorted.
     *
     * @param {String} [req.productType] Type of a product.The options are as follows:
     *
     * - 0: duration-based product
     * - 1: times-based product
     * - If the request does not contain this parameter, content items of all types of subscribed products are queried.
     *
     * @return {eBase.data.SubscribedContent[]}
     */
    iptv.querySubscribedContent = function(req) {
        req = req || {};
        utils.paginate(req);
        req.contenttype = req.contenttype || 'VAS,CHANNEL,AUDIO_CHANNEL,VIDEO_CHANNEL,WEB_CHANNEL,SUBJECT,VOD,AUDIO_VOD,VIDEO_VOD,PROGRAM';

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryMyContent',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query subscribed content of a profile.
     * @param {Object} req
     * @param {String} [req.profileId] ID of the profile whose subscription relationships will be obtained.
     *
     * - If the request does not contain this parameter, subscription relationships of the current login profile are obtained. If the current user is the super profile, all subscribed products are queried.
     * - If the super profile logs in, all subscription relationships are obtained.
     * - If a common profile logs in, only subscription relationships of the common profile are obtained.
     * - If the value is -1, all subscribed products are queried.
     *
     * @param {String} [req.fromDate] Start time. The format is YYYYMMDDHHMMSS.A request must contain both fromDate and toDate or either of them.
     * @param {String} [req.toDate] End time. The format is YYYYMMDDHHMMSS.A request must contain both fromDate and toDate
     * @param {String} [req.deviceId] ID of a logical device bound to a product.If the request does not contain this parameter, all products are queried.
     * @param {String} [req.isMain] Indicates whether to query basic packages.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     * - -1: all
     *
     * @param {String} [req.subscriptionKey] Key value of the subscription relationship, which is the unique primary key of the subscription relationship. If the terminal carries this parameter, the MEM does not focus on the preceding parameters.
     * @param {String} [req.contenttype] Content type. Separate multiple values with a comma (,).The options are as follows:
     *
     * - VAS
     * - CHANNEL
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - WEB_CHANNEL
     * - SUBJECT
     * - VOD
     * - AUDIO_VOD
     * - VIDEO_VOD
     * - PROGRAM
     * - Default is All.
     *
     * @param {String} [req.offset] Record index offset, default is 0.
     * @param {String} [req.count] Number of returned records,default is -1(All).
     * @param {String} [req.orderType] Mode of sorting subscription relationships.The options are as follows:
     *
     * - 0: by subscription time in ascending order
     * - 1: by subscription time in descending order
     * - 2: by subscription relationship expiration time in ascending order
     * - 3: by subscription relationship expiration time in descending order
     * - 4: by pricing object name in ascending order
     * - 5: by pricing object name in descending order
     * - The default value is 1.
     *
     * @param {String} [req.productType] Type of a product.The options are as follows:
     *
     * - 0: duration-based product
     * - 1: times-based product
     * - If the request does not contain this parameter, content items of all types of subscribed products are queried.
     *
     * @return {eBase.data.SubscribedContent[]}
     */
    iptv.querySubscribedProduct = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryOrder',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'productlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query extended attributes of a profile or subscriber.
     *
     * @param {Object} req
     * @param {Number} [req.userType = 0] User type.
     *
     * - 0: profile
     * - 1: subscriber
     * @param {String} [req.profileId] ID of the profile whose extended attributes will be obtained.
     * When userType is set to 1, you do not need to set profileId.
     * When userType is set to 0, if you leave profileId blank, extended attributes of the current login profile will be obtained; if you set profileId, extended attributes of the common profile specified by profileId will be obtained.
     *
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.CustomConfig}
     */
    iptv.querySubscriberEx = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QuerySubscriberEx',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'extensionInfo', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Get video AD policy info by content type and id.
     * @param {Object} req
     * @param {String} req.contentType Content type,The options are as follows:
     *
     * - VOD
     * - TVOD
     * - NPVR
     * - AUDIO_VOD
     * - VIDEO_VOD
     * @param {String} req.contentId ContentId, For VOD contentid is vodid; For NPVR or TVOD contentid is playbillid.
     *
     * @return {eBase.data.AdPolicy}
     */
    iptv.queryVideoAdPolicy = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetVideoAdPolicy',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'adPolicies', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query the VOD list under a specified EPG category.
     * When a subscriber browses a VOD category, the client invokes this interface to display the VOD list under this category to the subscriber.
     *
     * @param {Object} req
     * @param {String} [req.categoryId] Category ID, which can be used to query VOD lists from this category.
     * If the request does not contain this parameter, all VOD lists are queried.
     * @param {Number} [req.count=-1] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {Number} [req.offset=0] Start position for querying content.
     * @param {String} [req.properties] Properties be returned, seperated by ','. Like, 'id,type,name,averagescore,picture'
     * @param {Number} [req.orderType=3] Mode of sorting VOD programs.
     *
     * - 0: by release time in descending order
     * - 1: by average rating in descending order
     * - 2: by click-through rate in descending order
     * - 3: by content arrangement in a category
     * - 4: by VOD program name in ascending lexicographic order
     * - 5: by VOD program name in descending lexicographic order
     * @param {Array} filterlist List of {key: 'key', value: 'value'} Search criterion.
     * The options are as follows for the key parameter:
     *
     * - Producezone: country or region where a VOD program is produced
     * - Publishdate: date when a VOD program is released
     * - Genre: genre name for a VOD program
     * - SubscriptionType: subscription mode. The options are as follows:
     *
     * - -1: all
     * 鈭?0: subscription by month
     * 鈭?1: subscription by times
     * 鈭?2: the VOD program is not subscribed to
     * - Initial: filtering content by the initial letter of content name. The options include letters of subscribers' languages and ten digits from 0 to 9.
     * @param {Array} excluderlist List of {key: 'key', value: 'value'} Exclusive conditions.
     * The options are as follows for the key parameter:
     * - Producezone: country or region where a VOD program is produced
     * - Publishdate: date when a VOD program is released
     * - Genre: genre name for a VOD program
     * - SubscriptionType: subscription mode. The options are as follows:
     * 鈭?   -1: all
     * 鈭?   0: subscription by month
     * 鈭?   1: subscription by times
     * 鈭?   2: the VOD program is not subscribed to
     *
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.Vod}
     * @return return.counttotal Total count in list.
     */
    iptv.queryVod = function(req) {
        req = req || {};

        utils.rename(req, 'categoryId', 'categoryid');
        utils.paginate(req);

        if (_.has(req, 'properties')) {
            req.properties = [{
                name: 'vod',
                include: req.properties
            }];
        }

        //console.error("===Fangshenglong=== : Starting query vod via (/EPG/JSON/VodList) interface. Request is (" + JSON.stringify(req) + ").");
        return utils.jsonAjax({
            url: '/EPG/JSON/VodList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            //console.error("===Fangshenglong=== : Query vod via (/EPG/JSON/VodList) interface success. Request is (" + JSON.stringify(req) + ").");
            utils.rename(resp, 'vodlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client can invoke this interface only when the IPTV system connects to the recommendation engines such as the PRS.
     *
     * @param {Object} req
     * @param {String} req.vodId ID of the VOD content the subscriber is watching.
     * @param {Number} [req.type=0] Recommendation type.
     *
     * - 0: correlation recommendation
     * - 1: similarity recommendation
     *
     * @return {eBase.core.Deferred}
     */
    iptv.activeDynamicRecmFilm = function(req) {
        utils.rename(req, 'vodId', 'vodid');

        return utils.jsonAjax({
            url: '/EPG/JSON/ActiveDynamicRecmFilm',
            data: JSON.stringify(req)
        });
    };

    /**
     * This interface is used to query the dynamically recommended content.
     *
     * @param {Object} req
     * @param {String} req.vodId ID of the VOD content the subscriber is watching.
     * The ID can be obtained from the VOD content list.
     * @param {Number} req.count Total number of recommended content items.
     * @param {Number} req.offset Start position for querying recommended content. The value starts from 0.
     * @param {Number} [req.type=0] Recommendation type.
     *
     * - 0: correlation recommendation
     * - 1: similarity recommendation
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Vod}
     */
    iptv.dynamicRecmFilm = function(req) {
        utils.rename(req, 'vodId', 'vodid');
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/DynamicRecmFilm',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    /**
     * A client uses this interface to query the dynamically recommended content. Before invoking this interface, the client does not need to invoke the interface to trigger dynamic recommendation.
     *
     * @param {Object} req
     * @param {String} req.vodId VOD content ID.
     * @param {Number} req.count Total number of recommended content items.
     * @param {Number} req.offset Start position for querying recommended content. The value starts from 0.
     * @param {Number} [req.type=0] Recommendation type.
     *
     * - 0: correlation recommendation
     * - 1: similarity recommendation
     * - 2: prefer recommendation
     * - 3: user correlation recommendation
     * @param {String} [req.scenario] Recommend scenario.
     * @param {Number} [req.network] Network of terminal.
     *
     * - 1: WLAN
     * - 2: Cellular
     * @param {String} [req.recmdRegionID] Region id of recommendation.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Vod}
     */
    iptv.queryDynamicRecmFilm = function(req) {
        utils.rename(req, 'vodId', 'vodid');
        utils.paginate(req);
        if (_.has(req, 'properties')) {
            req.properties = [{
                name: 'vod',
                include: req.properties
            }];
        }
        if (eBase.session.get('currentProfile').profiletype === '0') {
            var r18LevelId = parseInt(eBase.session.get('currentProfile').levels, 10);
            for (var i = 0; i < gConfig.parentControlList.length; i++) {
                if ('R18' === gConfig.parentControlList[i].name) {
                    r18LevelId = parseInt(gConfig.parentControlList[i].id, 10);
                    break;
                }
            }
            req.ratingId = r18LevelId;
        }

        return utils.jsonAjax({
            url: '/EPG/JSON/QueryDynamicRecmFilm',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    /**
     * Write MEM the recommend log.
     *
     * @param {Object} req
     * @param {String} req.entrance Entrance of recommend page, defined by template.
     * @param {String} req.contentId ID of recommend VOD.
     * @param {Number} req.recmType Recommend type:
     *
     * - 0: Coordination recommend
     * - 1: Similar recommend
     * - 2: Prefeer recommend
     * - 3: User coordination recommend
     * - 4: Other recommend
     * @param {Number} req.actionType Action type:
     *
     * - 0: Watch
     * - 1: Subscribe
     * - 2: Favorite
     * @param {String} [req.productId] Product id if subscribed
     *
     * @return {eBase.core.Deferred}
     */
    iptv.writeRecmLog = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/WriteRecmLog',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));


/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    /**
     * This interface is used to query subscribers' program reminders. The system supports the function of querying reminders by type.
     * If a schedule is deleted, the platform automatically deletes reminders of this schedule.
     *
     * @param req
     * @param {Number} [req.type] Reminder type.
     *
     * - 1: program reservation
     * - 2: notice
     * @param {Number} [req.count=-1] Total number of content items obtained for one query. If all content items are obtained, the value is -1. The default value is -1.
     * @param {Number} [req.offset=0] Start position for querying content, starting from 0 (the first record).
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Reminder}
     */
    iptv.queryReminder = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/ReminderQuery',
            data: JSON.stringify(req || {})
        }).done(function(resp) {
            utils.rename(resp, 'reminderContents', 'list');
        });
    };
    /**
     * This interface is used to create reminders.
     *
     * @param {Object} req
     * @param {Array} req.reminderContents List of {eBase.data.Reminder}
     * @return {eBase.core.Deferred}
     */
    iptv.addReminder = function(req) {
        req.action = 'ADD';

        return utils.jsonAjax({
            url: '/EPG/JSON/ReminderManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_REMINDER_CHANGED');
        });
    };
    /**
     * This interface is used to delete reminders.
     *
     * @param {Object} req
     * @param {Array} req.reminderContents List of {eBase.data.Reminder}
     * @return {eBase.core.Deferred}
     */
    iptv.deleteReminder = function(req) {
        req.action = 'DELETE';

        return utils.jsonAjax({
            url: '/EPG/JSON/ReminderManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_REMINDER_CHANGED');
        });
    };
    /**
     * This interface is used to clear reminders.
     *
     * @param {Object} req
     * @param {Array} [req.reminderContents] List of {eBase.data.Reminder}
     * @return {eBase.core.Deferred}
     */
    iptv.clearReminder = function(req) {
        req = req || {};
        req.action = 'CLEAR';

        return utils.jsonAjax({
            url: '/EPG/JSON/ReminderManagement',
            data: JSON.stringify(req)
        }).done(function() {
            event.trigger('CORE_REMINDER_CHANGED');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * When a subscriber fails to log in to a device because the number of devices bound to a subscriber account reaches the upper limit, this interface is used to replace a bound device (referred as old device) with the device the subscriber attempts to log in to (referred as new device).
     *
     * @param {Object} req
     * @param {String} req.userId Subscriber ID. Subscriber ID. The ID contains digits, letters, dot (.), underscore (_), and @.
     * @param {String} req.orgDeviceId Logical ID of an old device. The ID is the same as the value of deviceId in the Device table. For details, see section 2.47 "Device."
     * @param {String} [req.destDeviceId] Physical address of a target device.
     * For an STB, this parameter is set to the MAC address of a device.
     * For an OTT device, this parameter is set to the value of caDeviceId of the CA client plug-in.
     * @return {eBase.core.Deferred}
     */
    iptv.replaceDevice = function(req) {
        utils.rename(req, 'userId', 'userid');

        return utils.jsonAjax({
            url: '/EPG/JSON/ReplaceDevice',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Rate and score content.
     * @param {Object} req
     * @param {String} req.contentType Content type.The options are as follows:
     *
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     *
     * @param {String} req.contentId Content id.
     * @param {String} req.score Content rating score. Value range: 1 to 10
     *
     * @return {eBase.core.Deferred}
     */
    iptv.scoreContent = function(req) {
        utils.rename(req, 'contentId', 'contentid');
        utils.rename(req, 'contentType', 'contenttype');

        return utils.jsonAjax({
            url: '/EPG/JSON/ScoreContent',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client invokes this interface to search for specified content.
     * The search function enables a subscriber to easily locate specific programs by entering keywords or setting filtering criteria.
     * After the subscriber sets search criteria on the content search page, the client invokes this interface to obtain the list of specified content.
     *
     * @param {Object} req
     * @param {String} req.key Search keywords.
     * @param {String} req.contentType Content type.
     *
     * - VOD
     * - TELEPLAY_VOD
     * - CHANNEL
     * - PROGRAM
     * - TVOD
     * - VAS
     * - AUDIO_CHANNEL
     * - VIDEO_CHANNEL
     * - WEB_CHANNEL
     * - AUDIO_VOD
     * - VIDEO_VOD
     * - Separate multiple values with a comma (,).
     * @param {Number} req.type Search type.
     *
     * - 1: name
     * - 2: genre. This option is supported only for VOD programs, schedules, and the VAS.
     * - 4: VOD tag
     * - 8: actor or actress
     * - 16: director
     * - 32: author
     * - 64: singer
     * - 128: producer
     * - 256: playwright
     * - 512: other cast and crew members
     * - 1023: all fields
     * - 2050: keyword
     * @param {Number} req.count Total number of content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {Number} req.offset Start position for querying content.
     * @param {Number} [req.order] Sorting mode.
     * - 1: by name in ascending order
     * - 2: by name in descending order
     * - 3: by release time in ascending order
     * - 4: by release time in descending order
     * - 5: by type in ascending order
     * - 6: by type in descending order
     * - 7: by number of playback times in ascending order
     * - 8: by number of playback times in descending order
     * - 9: by average rating in ascending order
     * - 10: by average rating in descending order
     * - 11: by genre in ascending order
     * - 12: by genre in descending order
     * Separate multiple values with a comma (,).
     * If the request does not contain this parameter, the results are not sorted.
     * @param {String} [req.categoryId] Category ID.
     * @param {String} [req.detailedVODType=0] Type of VOD content to be searched out.
     *
     * - 0: all (including series and movies)
     * - 1: common VOD (including the VOD program and episode of series)
     * - 2: series (including series and season)
     * - 3: VOD program
     * @param {Array} [req.filterList] Filter criteria. {eBase.data.NamedParameter}
     * The options are as follows for the key parameter:
     * -Genre: genre name for a program
     * -Country: country where a program is produced
     * -SubscriptionType: subscription mode. The options are as follows:
     * - 鈭?   -1: all
     * - 鈭?   0: subscription by month
     * - 鈭?   1: subscription by times
     * - 鈭?   2: the VOD program is not subscribed to
     * -Initial: filtering content by the initial letter of content name. The options include letters of subscribers' languages and ten digits from 0 to 9.
     * @param {Array} [req.excluderList] Exclusive conditions. {eBase.data.NamedParameter}
     * The options are as follows for the key parameter:
     *
     * - Genre: genre name for a program
     * - Country: country where a program is produced
     * - SubscriptionType: subscription mode. The options are as follows:
     * 鈭?   -1: all
     * 鈭?   0: subscription by month
     * 鈭?   1: subscription by times
     * 鈭?   2: the VOD program is not subscribed to
     *
     * @return {eBase.core.Deferred}
     * @return return.list {Array} List of {eBase.data.SearchContent}
     */
    iptv.search = function(req) {
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'categoryId', 'categoryid');
        utils.rename(req, 'filterList', 'filterlist');
        utils.rename(req, 'excluderList', 'excluderList');
        utils.paginate(req);

        return core.utils.jsonAjax({
            url: '/EPG/JSON/Search',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'contentlist', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Search live TV programs.
     * @param {Object} req
     * @param {String} req.channelId Channel ID.
     * @param {String} req.key Search keywords.
     * @param {String} [req.type] Search type.The options are as follows:
     *
     * - 1: program name.If the parameter is set to 1, programs are queried by name in leftmost match mode, which applies to series recording based on program name query in leftmost match mode.
     * - 2: series name.If the parameter is set to 2, series are queried by name in exact match mode, which applies to series recording based on series name and episode name query.
     *
     * @param {String} [req.businesstype] Service type.The options are as follows:
     *
     * - 6: cPVR
     * - 8: nPVR
     *
     * If the request does not contain this parameter, live TV programs that do not support cPVR and nPVR can be queried.
     * @return {eBase.data.Playbill[]}
     */
    iptv.searchPlaybill = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/QueryPlayBill',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'playbilllist', 'list');
        });
    };
    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * The terminal sends a self-defined name to the MEM, and the MEM processes the service, for example, Remote PVR, based on the defined command word.
     *
     * @param {Object} req
     * @param {String} req.receiver ID of a receiving user. This parameter indicates a subscriber ID or profile ID.
     * @param {Number} req.receiverType Type of a receiving user. The options are as follows:
     *
     * - 0: subscriber
     * - 1: UserProfile
     * @param {String} req.title Message title.
     * @param {Array} [req.contents] List of {eBase.data.NamedParameter} Message content. The client can transmit multiple self-defined key-value pairs. The maximum length of value in the key-value pairs can extend to 1024.
     * @param {String} [req.sendTime] Time when the message is sent. The format is yyyyMMddHHmmss.
     * If this parameter is not carried, the MEM uses the current time by default.
     * @param {String} [req.invalidTime] Time when the message expires. The format is yyyyMMddHHmmss.
     * If this parameter is not carried, the MEM uses the value of sendTime plus 1 as the default value.
     * @param {String} req.domain Domain type. The options are as follows:
     *
     * - 0: OTT
     * - 1: Mobile
     * - 2: IPTV
     * Separate multiple values with a comma (,).
     *
     * @return {eBase.core.Deferred}
     */
    iptv.sendCustomCommand = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/SendCustomCmd',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * The terminal reports device information to the MEM. The MEM sends the device information to the Push Server.
     *
     * @param {Object} req
     * @param {String} req.deviceType Device type. The options are as follows:
     *
     * - IOS: iOS device
     * - ANDROID: Android device
     * - WP: Windows Phone device (for subsequent extension)
     * - HUAWEI: push channel created by Huawei
     * - LINUX: Linux device
     * - PC: PC
     * - MAC: Apple computer
     * @param {String} req.deviceToken
     * For the XMPP mode, this parameter indicates a device token obtained from the Push Server.
飦?  * For the proxy mode, this parameter indicates a token assigned by the APNS, GCM, or MPNS.
     * @param {String} [req.terminalAppId] Device application ID, which is valid only on iOS devices.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.submitDeviceInfo = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/SubmitDeviceInfo',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;

    var parseReq = function(req) {
        utils.rename(req, 'contentType', 'contenttype');
        utils.rename(req, 'businessType', 'businesstype');
        utils.rename(req, 'promotionCode', 'promoCode');
        utils.rename(req, 'productId', 'productid');
        utils.rename(req, 'continueType', 'continuetype');
        utils.rename(req, 'contentId', 'contentid');

        return req;
    };
    /**
     * A client uses this interface to subscribe to products for a subscriber.
     * Generally, when a subscriber requests to use content or services, the client uses the {eBase.iptv.authorize} to authenticate the subscriber.
     * If the authentication fails, the EPG server sends back the list of subscribable products.
     * When the subscriber subscribes to a product, the client asks for the subscription password.
     * If the password is correct, the client invokes the product subscription interface to subscribe to the product.
     * @param {Object} req
     * @param {String} req.productid Product ID.
     * @param {String} [req.serviceid] ID of the pricing object, which comes from the priceobjectid attribute of the Product object.
     * @param {String} [req.servicetype] Type of a priced object.
     *
     * - 1: service
     * - 2: content
     * - 3: EPG category
     * - 4: bundle
     * The parameter comes from the priceobjecttype attribute of the Product object.
     * @param {String} req.continueType Indicates whether the automatic subscription renewal is supported.
     * - 0: no. When the product validity period ends, the subscription relationship expires. The subscriber must subscribe to the product again.
     * - 1: yes. When the product validity period ends, the system automatically renews the subscription to the product.
     * @param {String} [req.contentId] Content ID. The request contains this parameter for the subscription to a times-based product.
     * @param {String} [req.contentType] Content type. The request contains this parameter for the subscription to a times-based product.
     *
     * - VOD
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - CHANNEL
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - PROGRAM
     * @param {String} [req.businessType] Service type.
     *
     * - 0: combination of several content types
     * - 1: VOD
     * - 2: LIVE TV
     * - 3: PPV
     * - 4: TSTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 7: VAS
     * The parameter is required only by the PPV service.
     * @param {String} [req.fatherVodId] Set the parameter to the series ID if the subscriber wants to subscribe to a series.
     * @param {String} [req.orderMode=1] Subscription mode.
     *
     * - 1: subscription by money
     * - 2: subscription by IPTV bonus points
     * - 3: subscription by carrier bonus points
     * - 10: prepaid subscription
     * @param {String} [req.password] Subscription password.
     * @param {String} [req.promotionCode] Promo code.
     * This parameter is valid only when a subscriber uses the promo code to subscribe products.
     * @param {String} [req.quotaDeductFlag=0] Indicates whether subscribers' credit limit can exceed the maximum.
     *
     * - 0: no
     * - 1: yes. If consumption limit of a profile is insufficient, the consumption limit is deducted forcibly (the consumption limit can be a negative value).
     * @param {String} [req.deviceId] If a product is experienced only on the same device, the product subscription relationship is bound to the logical device. If this parameter is not specified, the product subscription relationship is bound to the current logical device by default.
     * @param {String} [req.discountFee] Discounted price. This parameter is valid only for times-based products.
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Subscription return code.
     * @return {String} return.retmsg Description of the subscription result.
     * @return {String} return.fee Product price.
     * @return {String} return.begintime Time when the subscription starts.
     * The value is returned only when the operation is successful.
     * Format: yyyyMMddHHmmss
     * @return {String} return.expiretime Time when the subscription ends.
     * The value is returned only when the operation is successful.
     * Format: yyyyMMddHHmmss
     * @return {String} return.purchaseType Subscription mode.
     *
     * - 0: monthly package product
     * - 1: times-based product
     *
     * @return {String} return.reliantPackageId ID of a bundle associated with the subscribed product.
     * The subscriber must subscribe to the bundle before subscribing to the product.
     * @return {String} return.remanentCredit Remaining quota.
     * @return {String} return.remanentCreditDuration Remaining time of the quota validity time.
     * @return {String} return.totalCredit
     * @return {String} return.totalCreditDuration Quota validity period.
     * @return {String} return.reliantProductIDList ID list of dependent products.
     * If multiple dependent policies exist, separate multiple policies with double vertical bars (||).
     * If multiple products IDs are contained in a dependent policy, separate multiple product IDs with vertical bars (|).
     * @return {String} reliantProductNameList Name list of dependent products.
     * The data encapsulation structure maps reliantProductIDList.
     * If multiple dependent policies exist, separate multiple policies with double vertical bars (||).
     * If multiple products IDs are contained in a dependent policy, separate multiple product IDs with vertical bars (|).
     * @return {String} return.transactionID Transaction ID.
     * @return {Object} return.triggers The triggers is delivered only when the client is PlayReady.
     * @return {Object} return.promoCodes  List of promo codes obtained by a subscriber. This parameter is returned upon successful subscription.
     * Only one promo code is rewarded upon one subscription. The interface is in the array format.
     */
    iptv.subscribe = function(req) {
        // UI订购节目单PPV时传入订购生效时间
        if (req && (parseInt(req.businesstype) === 3)) {
            req.effectiveTime = utils.dateToUTCString(new Date(), 'yyyyMMddHHmmss');
        }
        eBase.subscribeReq = req;
        return utils.jsonAjax({
            url: '/EPG/JSON/Subscribe',
            data: JSON.stringify(parseReq(req))
        }).done(function() {
            if (eBase.subscribeReq.contenttype === 'PROGRAM'){
                eBase.app.playbillApp.clearPlaybillByChaId(eBase.subscribeReq.contentid);
            }
            /**if (req.type !== 'vod') {*/
            event.trigger('CORE_SUBSCRIBE_SUCCESS');
            /**}*/
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.blackoutPeriod}
     */
    iptv.queryBlackoutPeriod = function() {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/GetBlackoutPeriod'
        }).done(function(resp) {
            utils.rename(resp, 'blackoutPeriod', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        event = core.event,
        utils = core.utils;
    /**
     * A client uses this interface to subscribe to products for a subscriber.
     * Generally, when a subscriber requests to use content or services, the client uses the {eBase.iptv.authorize} to authenticate the subscriber.
     * If the authentication fails, the EPG server sends back the list of subscribable products.
     * When the subscriber subscribes to a product, the client asks for the subscription password.
     * If the password is correct, the client invokes the product subscription interface to subscribe to the product.
     * @param {Object} req
     * @param {String} req.productid Product ID.
     * @param {String} req.mediaId ID of media file.
     * @param {String} [req.serviceid] ID of the pricing object, which comes from the priceobjectid attribute of the Product object.
     * @param {String} [req.servicetype] Type of a priced object.
     *
     * - 1: service
     * - 2: content
     * - 3: EPG category
     * - 4: bundle
     * The parameter comes from the priceobjecttype attribute of the Product object.
     * @param {String} req.continueType Indicates whether the automatic subscription renewal is supported.
     * - 0: no. When the product validity period ends, the subscription relationship expires. The subscriber must subscribe to the product again.
     * - 1: yes. When the product validity period ends, the system automatically renews the subscription to the product.
     * @param {String} [req.contentId] Content ID. The request contains this parameter for the subscription to a times-based product.
     * @param {String} [req.contentType] Content type. The request contains this parameter for the subscription to a times-based product.
     *
     * - VOD
     * - VIDEO_VOD
     * - AUDIO_VOD
     * - CHANNEL
     * - VIDEO_CHANNEL
     * - AUDIO_CHANNEL
     * - WEB_CHANNEL
     * - VAS
     * - PROGRAM
     * @param {String} [req.businessType] Service type.
     *
     * - 0: combination of several content types
     * - 1: VOD
     * - 2: LIVE TV
     * - 3: PPV
     * - 4: TSTV
     * - 5: Catch-up TV
     * - 6: PVR
     * - 7: VAS
     * The parameter is required only by the PPV service.
     * @param {String} [req.fatherVodId] Set the parameter to the series ID if the subscriber wants to subscribe to a series.
     * @param {String} [req.orderMode=1] Subscription mode.
     *
     * - 1: subscription by money
     * - 2: subscription by IPTV bonus points
     * - 3: subscription by carrier bonus points
     * - 10: prepaid subscription
     * @param {String} [req.password] Subscription password.
     * @param {String} [req.promotionCode] Promo code.
     * This parameter is valid only when a subscriber uses the promo code to subscribe products.
     * @param {String} [req.quotaDeductFlag=0] Indicates whether subscribers' credit limit can exceed the maximum.
     *
     * - 0: no
     * - 1: yes. If consumption limit of a profile is insufficient, the consumption limit is deducted forcibly (the consumption limit can be a negative value).
     * @param {String} [req.deviceId] If a product is experienced only on the same device, the product subscription relationship is bound to the logical device. If this parameter is not specified, the product subscription relationship is bound to the current logical device by default.
     * @param {String} [req.discountFee] Discounted price. This parameter is valid only for times-based products.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.retcode Subscription return code.
     * @return {String} return.retmsg Description of the subscription result.
     * @return {String} return.url Play URL.
     * @return {String} return.fee Product price.
     * @return {String} return.begintime Time when the subscription starts.
     * The value is returned only when the operation is successful.
     * Format: yyyyMMddHHmmss
     * @return {String} return.expiretime Time when the subscription ends.
     * The value is returned only when the operation is successful.
     * Format: yyyyMMddHHmmss
     * @return {String} return.purchaseType Subscription mode.
     *
     * - 0: monthly package product
     * - 1: times-based product
     *
     * @return {String} return.reliantPackageId ID of a bundle associated with the subscribed product.
     * The subscriber must subscribe to the bundle before subscribing to the product.
     * @return {String} return.remanentCredit Remaining quota.
     * @return {String} return.remanentCreditDuration Remaining time of the quota validity time.
     * @return {String} return.totalCredit
     * @return {String} return.totalCreditDuration Quota validity period.
     * @return {String} return.reliantProductIDList ID list of dependent products.
     * If multiple dependent policies exist, separate multiple policies with double vertical bars (||).
     * If multiple products IDs are contained in a dependent policy, separate multiple product IDs with vertical bars (|).
     * @return {String} reliantProductNameList Name list of dependent products.
     * The data encapsulation structure maps reliantProductIDList.
     * If multiple dependent policies exist, separate multiple policies with double vertical bars (||).
     * If multiple products IDs are contained in a dependent policy, separate multiple product IDs with vertical bars (|).
     * @return {String} return.transactionID Transaction ID.
     * @return {Object} return.triggers The triggers is delivered only when the client is PlayReady.
     * @return {Object} return.promoCodes  List of promo codes obtained by a subscriber. This parameter is returned upon successful subscription.
     * Only one promo code is rewarded upon one subscription. The interface is in the array format.
     */
    iptv.subscribeAndPlay = function(req) {
        // UI订购节目单PPV时传入订购生效时间
        if (req && (parseInt(req.businesstype) === 3)) {
            req.effectiveTime = utils.dateToUTCString(new Date(), 'yyyyMMddHHmmss');
        }

        return utils.jsonAjax({
            url: '/EPG/JSON/SubscribeAndPlay',
            data: JSON.stringify(req)
        }).done(function() {
            /**if (req.type !== 'vod') {*/
            event.trigger('CORE_SUBSCRIBE_SUCCESS');
            /**}*/
        });
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var utils = core.utils,

        PLAY_TYPE_MAP = {
            vod: '1',
            channel: '2',
            playbill: '4',
            clip: '5',
            bookmark: '6',
            timeshift: '7',
            cpvr: '9',
            npvr: '9'
        },

        BUSINESS_TYPE_MAP = {
            mix: '0',
            vod: '1',
            clip: '1',
            channel: '2',
            timeshift: '4',
            ppv: '3',
            playbill: '5',
            pvr: '6',
            cpvr: '6',
            vas: '7',
            npvr: '8'
        },

        SERVER_TYPE_MAP = {
            'VOD': 'vod',
            'VIDEO_VOD': 'vod',
            'AUDIO_VOD': 'vod',
            'CREDIT_VOD': 'vod',
            'TELEPLAY_VOD': 'vod',
            'CHANNEL': 'channel',
            'VIDEO_CHANNEL': 'channel',
            'AUDIO_CHANNEL': 'channel',
            'PROGRAM': 'playbill'
        };

    utils.convertToClientType = function(type) {
        return SERVER_TYPE_MAP[type] || type;
    };

    utils.convertToServerType = function(type) {
        switch (type) {
            case 'vod':
                return 'VOD';
            case 'channel':
                return 'CHANNEL';
            case 'AUDIO_CHANNEL': //Modified by [A001] [chenyao] at [2014.05.06]
                return 'AUDIO_CHANNEL';
            case 'playbill':
                return 'PROGRAM';
            case 'tvod':
                return 'TVOD';
            default:
                return type;
        }
    };

    utils.convertToPlayType = function(type) {
        return PLAY_TYPE_MAP[type];
    };

    utils.convertToBusinessType = function(type) {
        return BUSINESS_TYPE_MAP[type];
    };

    return core;
})(eBase);
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    /**
     * Change the common profile password, subscription password, subscriber password, super profile password, and parental control password.
     * @param {Object} req
     * @param {String} req.oldPassword Old password. The old password must be encrypted using the encryption mode.
     * @param {String} req.newPassword New password. The new password must be encrypted using the encryption mode.
     * @param {String} req.confirmPassword Confirm password. The confirm password must be the same as the new password and be encrypted using the encryption mode.
     * @param {String} [req.type] Password type.The options are as follows:
     *
     * - 0: profile's password.
     * - 1: subscription password.
     * - 2: subscriber's login password.
     * - 3: super profile password.
     * - 4: parental control password.
     * - 5: remote EPG password used to log in to the client on a tablet. This value is not recommended.
     * - 6: logic device password.
     * - The default value 3 is used.
     *
     * @return {String}
     *
     * - 0: Password modification succeeded.
     * - 87031808: Password modification failed.
     * - 85983235: The old password is incorrect.
     * - 85983237: The new password and confirm password are different.
     * - 85983236: Only the super profile has the right to change the password.
     * - 85983233: The user does not exist.
     * - 85983302: The logic device ID does not exist.
     *
     */
    iptv.updatePassword = function(req) {
        utils.rename(req, 'oldPassword', 'oldpwd');
        utils.rename(req, 'newPassword', 'newpwd');
        utils.rename(req, 'confirmPassword', 'confirmpwd');

        return core.utils.jsonAjax({
            url: '/EPG/JSON/UpdatePassword',
            data: JSON.stringify(req || {})
        });
    };
    iptv.resetPassword = function(req) {
        return core.utils.jsonAjax({
            url: '/EPG/JSON/ResetPassword',
            data: JSON.stringify(req || {})
        });
    };
    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to modify extended attributes of a profile or subscriber.
     *
     * @param {Object} req
     * @param {eBase.data.CustomConfig[]} [req.extensionInfo] Extended attribute.
     * In NamedParameter,
     *
     * - If key exists but value is empty, extended attributes will be deleted.
     * - If key exists and value is not empty, extended attributes will be modified.
     * - If key does not exist, extended attributes will be added.
     * @param {Number} [req.userType=0] User type.
     *
     * - 0: profile
     * - 1: subscriber
     * @param {String} [req.profileId] ID of the profile whose extended attributes will be modified.
     * When userType is set to 1, you do not need to set profileId.
     * When userType is set to 0, if you leave profileId blank, extended attributes of the current login profile will be modified; if you set profileId, extended attributes of the common profile specified by profileId will be modified.
     *
     * @return {eBase.core.Deferred}
     */
    iptv.updateSubscriberEx = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/UpdateSubscriberEx',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * This interface is to report user action to backend.
     *
     * @param {Object} req
     * @param {String} req.action Action type:
     *
     * - browsePage
     * - browseContent
     * - playStart
     * - playEnd
     * - playHold
     * - login
     * - logout
     * - active
     * - disactive
     * @param {String} req.timestamp Action time, in yyyyMMddHHmmss format.
     * @param {String} [req.divFrom] Last page/region's name.
     * @param {String} [req.divTo] Current page/region's name.
     * @param {String} [req.contentId] Content ID.
     * @param {String} [req.contentType] Content type.
     * @param {Number} [req.businessType] Business type:
     *
     * - 1: VOD
     * - 2: LIVETV
     * - 4: PLTV
     * - 5:TVOD
     * @return {eBase.core.Deferred}
     */
    iptv.userTrace = function(req) {
        utils.rename(req, 'userId', 'userid');

        return utils.jsonAjax({
            url: '/EPG/JSON/UserTrace',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        utils = core.utils;

    /**
     * Authenticate related business logic.
     *
     * @class eBase.app.authenticateApp
     * @singleton
     */
    app.authenticateApp = function() {
        return {
            /**
             * Login system as guest.
             *
             * @return {eBase.core.Deferred}
             */
            guestLogin: function() {
                var retDfd = new Deferred();

                core.iptv.login({
                    userId: 'Guest'
                }).done(function() {
                    core.iptv.getGuestInfo().done(function(resp) {
                        core.iptv.authenticate({
                            userId: resp.subscriberId,
                            password: resp.password
                        }).then(retDfd.resolve, retDfd.reject);
                    }).fail(retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * Authenticate client to backend server
             *
             * @param {Object} req
             * @param {String} req.userId Subscriber ID.
             * @param {String} req.password Password (in plaintext).
             * @param {String} [req.encrypted=false] Whether password is encrypted.
             *
             * @return {eBase.core.Deferred}
             */
            authenticate: function(req) {
                var retDfd = new Deferred();

                req.password = req.encrypted ? req.password : utils.md5(req.password);

                core.iptv.login(req).done(function() {
                    core.iptv.authenticate(req).pipe(retDfd.resolve, retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * Logout from system
             *
             * @param {Object} req
             * @param {Number} [req.type=0] Exit type.
             *
             * - 0: subscriber deregistration
             * - 1: subscriber exit, indicating PC power-off
             *
             * @return {eBase.core.Deferred}
             */
            logout: function(req) {
                return core.iptv.logout(req);
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        utils = core.utils;
    /**
     * Authorization related business logic.
     *
     * @class eBase.app.authorizeApp
     * @singleton
     */
    app.authorizeApp = function() {
        return {
            /**
             * Authorize content
             *
             * @param {Object} req
             * @param {String} req.id Content ID
             * @param {String} req.type Content type
             *
             * - vod
             * - channel
             * - playbill
             * - vas
             * - npvr
             * - cpvr
             * @param {String} req.mediaId ID of media file
             * @param {String} [req.businessType] Service type
             *
             * - vod
             * - channel
             * - playbill
             * - timeshift
             * @param {String} [req.fatherVodId] Superset VOD ID
             * @param {String} [req.categoryId] Category ID
             *
             * @return {eBase.core.Deferred}
             * @return {String} return.retcode Authentication return code.
             * @return {String} return.retmsg Response message.
             * @return {String} return.url Play URL.
             * @return {String} return.point Number of IPTV bonus points that are available.
             * @return {String} return.telecompoint Number of carrier bonus points that are available.
             * @return {eBase.data.Product[]} return.list Returned information about the subscribable package products after an authentication failure.
             * @return {eBase.data.Product[]} return.monthlist Returned information about the subscribable monthly package products after an authentication failure.
             * @return {eBase.data.Product[]} return.timelist Returned information about the subscribable times-based products after an authentication failure.
             * @return {eBase.data.Product[]} return.preorderlist Returned information about the subscribed products that have not been activated after an authentication failure.
             * @return {String} return.productId Product ID used for content authentication.
             * @return {String} return.restriction Restrictions of a product. The parameter is returned only when the authentication is successful.
             * @return {String} return.inCondition Applicable conditions of a product. The parameter is returned only when the authentication is successful.
             */
            authorize: function(req, isnoshowerror) {
                utils.rename(req, 'id', 'contentId');
                utils.rename(req, 'type', 'contentType');

                req.businessType = req.businessType || req.contentType;
                req.businessType = utils.convertToBusinessType(req.businessType);
                req.contentType = utils.convertToServerType(req.contentType);

                return core.iptv.authorizeAndPlay(req, isnoshowerror).fail(function(resp) {
                    resp.list = (resp.monthlist || []).concat(resp.timelist || []);
                });
            },
            /**
             * Interface to Subscribe a Product
             *
             * @param {Object} req
             * @param {String} req.productId Product ID.
             * @param {String} req.mediaId Media ID.
             * @param {String} [req.continueType=1] Continue Type.
             * @param {String} [req.contentId] Content ID. The parameter is required if a times-based product has been subscribed to.
             * @param {String} [req.contentType] Content type.
             *
             * - vod
             * - channel
             * - playbill
             * @param {String} [req.fatherVodId] Set the parameter to the series ID if the subscriber wants to subscribe to a series.
             *
             * @return {eBase.core.Deferred}
             */
            subscribe: function(req) {
                utils.rename(req, 'id', 'contentId');
                utils.rename(req, 'type', 'contentType');

                req.businessType = req.businessType || req.contentType;
                req.businessType = utils.convertToBusinessType(req.businessType);
                req.contentType = utils.convertToServerType(req.contentType);
                if (req.sitcomnum && req.sitcomnum > 0) {
                    return core.iptv.subscribe(req);
                } else {

                    return core.iptv.subscribeAndPlay(req);
                }
            },

            GetDiscountInfos: function(req) {
                req.ids = req.ids.split(',');
                utils.rename(req, 'ids', 'productIds');
                return core.iptv.queryDiscount(req);
            },
            /**
             * Interface to Cancel the Product Renewal
             *
             * @param {Object} req
             * @param {String} req.productId Product ID.
             * @param {String} req.continueType Cancellation type.
             *
             * - 0: canceling the current subscription (PPV subscription and subscription by period)
             * - 1: canceling automatic renewal, which takes effect in next period (subscription by period)
             * - 2: canceling automatic renewal, which takes effect at the end of a month (subscription by period)
             * @param {String} [req.contentId] Content ID. The parameter is required when a PPV product is unsubscribed from.
             * @param {String} [req.contentType] Content type. The parameter is required when a PPV product is unsubscribed from.
             * @param {Date} [req.startTime] Start time of subscription. The parameter is required when a PPV product is unsubscribed from. If a by-month product
             * can be unsubscribed from by time segment, the parameter must be carried.
             * @param {String} [req.deviceId] ID of a logical device bound to an unsubscribed non-shared product, which comes from the deviceId attribute of
             * Product. If the parameter is not carried, the non-shared product bound to the current device is unsubscribed from on the MEM by default.
             *
             * @return {eBase.core.Deferred}
             */
            cancelSubscribe: function(req) {
                return core.iptv.cancelSubscribe(req);
            },
            /**
             * A client invokes this interface to query product details based on the product ID.
             *
             * @param {Object} req
             * @param {String} req.ids Product IDs separated by ','. '1, 4'
             *
             * @return {eBase.core.Deferred}
             * @return {eBase.data.Product[]} return.list
             */
            queryProductByIds: function(req) {
                req.ids = req.ids.split(',');
                utils.rename(req, 'ids', 'productIds');

                return core.iptv.queryProduct(req);
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        event = core.event;

    var BOOKMARK_SERVER_TYPE = {
            'vod': 0,
            'cpvr': 1,
            'npvr': 2,
            'tvod': 3,
            'channel': 4,
            'all': -1
        },
        BOOKMARK_CLIENT_TYPE = ['vod', 'cpvr', 'npvr', 'tvod', 'channel'],
        ignoreFatherVodIdList = ['-1', '-2'];

    core.appReadyTrigger.register('APP_BOOKMARK_DATA_READY');

    // Query bookmark when the profile login.
    event.on('APP_PROFILE_DATA_READY', function() {
        if (config.isGuest) {
            app.bookmarkApp.bookmarkList = [];
            event.trigger('APP_BOOKMARK_DATA_READY');
        } else {
            app.bookmarkApp.refreshBookmark().always(function() {
                console.log('bookmark  ready');
                event.trigger('APP_BOOKMARK_DATA_READY');
            });
        }
    });

    // Query bookmark when the bookmark-version changed.
    event.on('CORE_BOOKMARK_VERSION_CHANGED', function() {
        app.bookmarkApp.refreshBookmark().done(function() {
            event.trigger('APP_BOOKMARK_DATA_CHANGED');
        });
    });

    /**
     * Bookmark related business logic.
     * @class eBase.app.bookmarkApp
     * @singleton
     */
    var self = app.bookmarkApp = function() {
        return {
            /**
             * Cached bookmark list.
             * @private
             */
            bookmarkList: [],
            /**
             * This interface is used to query bookmarks.
             * @param {Object} req
             * @param {String} [req.type=all] Bookmark type
             *
             * - vod
             * - cpvr
             * - npvr
             * - tvod
             * - channel
             * - all
             * @return {Array} List of {eBase.data.Bookmark}
             */
            queryBookmark: function(req) {
                if (req && req.type) {
                    return new Deferred().resolve({
                        list: _.filter(self.bookmarkList, function(item) {
                            return BOOKMARK_CLIENT_TYPE[item.bookmarkType] === req.type;
                        })
                    });
                }

                return new Deferred().resolve({
                    list: self.bookmarkList
                });
            },
            /**
             * This interface is used to get one bookmark.
             *
             * @param {Object} req
             * @param {String} [req.id] content id
             * @param {String} [req.type=vod] Bookmark type
             *
             * - vod
             * - cpvr
             * - npvr
             * - tvod
             * - channel
             * @param {String} [req.fatherVodId] Farther vod id
             * @return {eBase.data.Bookmark}
             */
            getBookmark: function(req) {
                req.type = req.type || 'vod';

                if (req.fatherVodId && !_.contains(ignoreFatherVodIdList, req.fatherVodId)) {
                    if (req.id) {
                        return _.findWhere(self.bookmarkList, {
                            id: req.id,
                            fathervodid: req.fatherVodId
                        });
                    } else {
                        return _.findWhere(self.bookmarkList, {
                            fathervodid: req.fatherVodId
                        });
                    }
                } else {
                    return _.findWhere(self.bookmarkList, {
                        id: req.id
                    });
                }
            },
            /**
             * This interface is used to create one bookmark.
             *
             * @param {Object} req
             * @param {String} req.id Content ID.
             * @param {String} [req.fatherVodId] Father vod id.
             * @param {String} [req.type=vod] Content type.
             *
             * - vod
             * - cpvr
             * - npvr
             * - tvod
             * - channel
             *
             * @param {Number} req.time Bookmark breakpoint, in seconds.
             *
             * @return {eBase.core.Deferred}
             */
            addBookmark: function(req) {
                var retDfd = new Deferred(),
                    request = {
                        contentId: req.id,
                        time: req.time,
                        bookmarkType: BOOKMARK_SERVER_TYPE[req.type || 'vod']
                    };

                if (req.fatherVodId) {
                    request.fatherVodId = req.fatherVodId;
                }

                core.iptv.addBookmark(request).done(function() {
                    // succeed to create bookmark, refresh cached bookmarks
                    self.refreshBookmark().done(function(resp) {
                        retDfd.resolve(resp);
                        event.trigger('APP_BOOKMARK_DATA_CHANGED');
                    }).fail(retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * This interface is used to delete one bookmark.
             *
             * @param {Object} req
             * @param {String} req.id Content ID.
             * @param {String} [req.fatherVodId] Father vod id.
             * @param {String} [req.type=vod] Content type.
             *
             * - vod
             * - cpvr
             * - npvr
             * - tvod
             * - channel
             *
             * @return {eBase.core.Deferred}
             */
            deleteBookmark: function(req) {
                req.type = req.type || 'vod';

                var request = {
                    contentId: req.id,
                    bookmarkType: BOOKMARK_SERVER_TYPE[req.type]
                };

                if (req.fatherVodId) {
                    request.fatherVodId = req.fatherVodId;
                }

                return core.iptv.deleteBookmark(request).done(function() {
                    // delete the bookmark in cache
                    self.bookmarkList = _.reject(self.bookmarkList, function(bookmark) {
                        return bookmark.contentId === req.id && BOOKMARK_CLIENT_TYPE[bookmark.bookmarkType] === req.type;
                    });
                    event.trigger('APP_BOOKMARK_DATA_CHANGED');
                });
            },
            /**
             * This interface is to clear user bookmarks.
             * @param {Object} req
             * @param {String} [req.type=vod] type of bookmark
             *
             * - vod
             * - cpvr
             * - npvr
             * - tvod
             * - channel
             *
             * @return {eBase.core.Deferred}
             */
            clearBookmark: function(req) {
                req = req || {
                    type: 'vod'
                };

                return core.iptv.clearBookmark({
                    bookmarkType: BOOKMARK_SERVER_TYPE[req.type]
                }).done(function() {
                    // clear bookmarks in cache
                    if (req.type === 'all') {
                        self.bookmarkList = [];
                    } else {
                        self.bookmarkList = _.reject(self.bookmarkList, function(bookmark) {
                            return BOOKMARK_CLIENT_TYPE[bookmark.bookmarkType] === req.type;
                        });
                    }
                    event.trigger('APP_BOOKMARK_DATA_CHANGED');
                });
            },
            /**
             * This interface is to refresh bookmark cache.
             * @private
             */
            refreshBookmark: function() {

                //console.log('refreshBookmark-------------------------------------01');
                //eBase.session.put('ContentFilterLevel', '1', false);

                // 支持运营商配置内容过滤级别
                // var contentLevel = eBase.session.get('ContentFilterLevel');
                // if (!contentLevel) {
                //     var contentFilerLevel = gUtils.getConfiguration({
                //         key: 'CONTENT_FILTER_LEVEL'
                //     });

                //     if (contentFilerLevel) {
                //         eBase.session.put('ContentFilterLevel', contentFilerLevel, false);

                //         contentLevel = contentFilerLevel;
                //     }
                // }

                var req = {
                    orderType: 1
                };
                // if (contentLevel) {
                //     req = {
                //         orderType: 1,
                //         filterlist: [{
                //             'key': 'RatingId',
                //             'value': contentLevel
                //         }]
                //     }
                // }

                return core.iptv.queryBookmark(req).done(function(resp) {
                    // refresh the bookmark in cache
                    self.bookmarkList = resp.list;
                })
            }
        };
    }();

    return core;
}(eBase));


var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        event = core.event;

    /**
     * Channel related business logic.
     * @class eBase.app.channelApp
     * @singleton
     */
    var mashupFlightList = {};
    var mashupWeatherList = {};
    var mashupWeatherReq = '';
    var mashupFlightReq = '';

    var self = app.vasApp = function() {
        return {
            getMashupWeatherInfo: function(req) {
                var retDfd = new Deferred();
                core.iptv.getWeatherInfo(req).done(function(resp) {
                    retDfd.resolve(resp[0]);
                }).fail(retDfd.reject);
                return retDfd;
            },

            getMashupFlightInfo: function(req) {
                var retDfd = new Deferred();
                core.iptv.getFlightInfo(req).done(function(resp) {
                    retDfd.resolve(resp[0]);
                }).fail(retDfd.reject);
                return retDfd;
            }
        }
    }();

    return core;
}(eBase));

var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        utils = core.utils,
        event = core.event;

    core.appReadyTrigger.register('APP_FAVORITE_DATA_READY');

    // Query favorite when the profile login.
    event.on('APP_CHANNEL_DATA_READY', function() {
        if (config.isGuest) {
            app.favoriteApp.favoriteChannels = [];
            app.favoriteApp.favoriteVods = [];
            event.trigger('APP_FAVORITE_DATA_READY');
        } else {
            app.favoriteApp.refreshFavorite().always(function() {
                console.log('favorite ready');
                event.trigger('APP_FAVORITE_DATA_READY');
            });
        }
    });

    // Query favorite when the favorite-version changed.
    event.on('CORE_FAVORITE_VERSION_CHANGED', function() {
        app.favoriteApp.refreshFavorite().done(function() {
            event.trigger('APP_FAOVIRTE_DATA_CHANGED');
        });
    });

    /**
     * Favorite related business logic.
     * @class eBase.app.favoriteApp
     * @singleton
     */
    var self = app.favoriteApp = function() {
        return {
            /**
             * This interface is used to query content in favorites.
             * @param {Object} req
             * @param {String} [req.type=vod] Type of favorited contents.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Favorite}
             */
            queryFavorite: function(req) {
                var dfd = new Deferred();

                var _callback = function() {
                    if (req && req.type === 'channel') {
                        dfd.resolve({
                            list: self.favoriteChannels
                        });
                    } else {
                        dfd.resolve({
                            list: self.favoriteVods
                        });
                    }
                };
                // if (!self.favoriteChannels) {
                //     self.refreshFavorite().done(function() {
                //         _callback();
                //     });
                // } else {
                //     _callback();
                // }
                self.refreshFavorite().done(function() {
                    _callback();
                });
                return dfd;
            },
            /**
             * This interface is used to add content to favorites.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             */
            addFavorite: function(req) {
                var dfd = new Deferred();

                core.iptv.addFavorite({
                    contentId: req.id,
                    contentType: utils.convertToServerType(req.type)
                }).done(function() {
                    // refresh favorite in cache
                    self.refreshFavorite().done(function(resp) {
                        dfd.resolve(resp);
                        if (req.type === 'channel') {
                            event.trigger('APP_FAVCHANNEL_DATA_CHANGED');
                        } else {
                            event.trigger('APP_FAVORITE_DATA_CHANGED');
                        }
                    }).fail(dfd.reject);
                }).fail(dfd.reject);

                return dfd;
            },
            /**
             * This interface is used to delete a favorite.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             */
            deleteFavorite: function(req) {
                return core.iptv.deleteFavorite({
                    contentId: req.id,
                    contentType: utils.convertToServerType(req.type)
                }).done(function() {
                    var deletedIds = req.id.split(',');
                    // delete the favorite in cache
                    self.favoriteVods = _.reject(self.favoriteVods, function(favorite) {
                        return deletedIds.indexOf(favorite.id) >= 0;
                    });
                    self.favoriteChannels = _.reject(self.favoriteChannels, function(favorite) {
                        return deletedIds.indexOf(favorite.id) >= 0;
                    });
                    event.trigger('APP_FAVORITE_DATA_CHANGED', req.id);
                });
            },
            /**
             * This interface is used to clear all favorites.
             *
             * @return {eBase.core.Deferred}
             */
            clearFavorite: function() {
                return core.iptv.clearFavorite().done(function() {
                    // clear favorites in cache
                    self.favoriteVods = [];
                    self.favoriteChannels = [];
                    event.trigger('APP_FAVORITE_DATA_CHANGED');
                });
            },
            /**
             * Refresh favorite cache.
             * @private
             */
            refreshFavorite: function() {
                var retDfd = new Deferred();

                core.iptv.queryFavorite().done(function(resp) {
                    // refresh the favorite in cache
                    var group = _.groupBy(resp.list, function(favorite) {
                        return utils.convertToClientType(favorite.type);
                    });
                    var channelIds = _.map(group.channel, function(favorite) {
                        return favorite.id;
                    });

                    Deferred.when(app.channelApp.getChannelSortDetail({
                        ids: channelIds
                    }), app.vodApp.queryVodByIds({
                        ids: group.vod ? _.pluck(group.vod, 'id').join(',') : ''
                    })).done(function(channelResp, vodResp) {
                        self.favoriteChannels = _.compact(channelResp.list);
                        self.favoriteVods = _.compact(vodResp.list);
                        retDfd.resolve();
                    }).fail(retDfd.reject);
                });

                return retDfd;
            },
            /**
             * This interface is to check whether content is favorited.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {Boolean}
             */
            isFavorited: function(content) {
                if (utils.convertToClientType(content.type) === 'channel') {
                    return !!_.find(self.favoriteChannels, function(favorite) {
                        return favorite.id === content.id;
                    });
                } else {
                    return !!_.find(self.favoriteVods, function(favorite) {
                        return favorite.id === content.id;
                    });
                }
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        event = core.event,
        session = core.session,
        versionWatchingPool;
    //heartbeatInterval,
    //heartbeatHandler;

    // First beat after profile logged.
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function() {
        //heartbeatInterval = core.config.heartbeatInterval || 15 * 60 * 1000;
        var self = this;
        versionWatchingPool = {
            channelVersion: {
                id: 'channelversion',
                event: 'CORE_CHANNEL_VERSION_CHANGED'
            },
            reminderVersion: {
                id: 'reminderversion',
                event: 'CORE_REMINDER_VERSION_CHANGED'
            },
            favoriteVersion: {
                id: 'favoversion',
                event: 'CORE_FAVORITE_VERSION_CHANGED'
            },
            lockVersion: {
                id: 'lockversion',
                event: 'CORE_LOCK_VERSION_CHANGED'
            },
            bookmarkversion: {
                id: 'bookmarkversion',
                event: 'CORE_BOOKMARK_VERSION_CHANGED'
            },
            subscribeversion: {
                id: 'subscribeversion',
                event: 'CORE_SUBSCRIBE_VERSION_CHANGED'
            },
            ppvversion: {
                id: 'ppvversion',
                event: 'APP_PPV_DATA_CHANGED'
            },
            pvrversion: {
                id: 'pvrversion',
                event: 'APP_NPVR_DATA_CHANGED'
            },
            profileversion: {
                id: 'profileversion',
                event: 'CORE_PROFILE_VERSION_CHANGED'
            },
            blackoutversion: {
                id: 'blackoutversion',
                event: 'CORE_BLACKOUT_VERSION_CHANGED'
            }
        };

        //app.heartbeatApp.heartbeat();
        // prepare next heartbeat
        /*gEventManager.on('EVENT_STB_HEARTBEAT', function() {
            //self.heartbeat();
            self.httpisFrist = true;
            app.heartbeatApp.heartbeat();

            //app.playbillApp.cleanPlaybillList();//清空节目单数据
        });*/
        gEventManager.on('EVENT_STB_HEARTBEAT', app.heartbeatApp.heartbeatCallback);
    });

    event.on('CORE_LOGOUT_SUCCESS', function() {
        //clearTimeout(heartbeatHandler);
        gEventManager.off('EVENT_STB_HEARTBEAT', app.heartbeatApp.heartbeatCallback);
    });

    event.on('CORE_PROFILE_VERSION_CHANGED', function() {
        console.log('CORE_PROFILE_VERSION_CHANGED');

        app.profileApp.refreshProfile();
    });

    /**
     * Heartbeat related business logic.
     * @class eBase.app.heartbeatApp
     * @singleton
     */
    var self = app.heartbeatApp = function() {
        var self = this;
        return {
            setChannelVersion: function(newVersion) {
                versionWatchingPool['channelVersion'].current = newVersion;
            },

            heartbeat: function() {
                return core.iptv.heartbeat({
                    userId: session.get('userId')
                }).done(function(resp) {
                    if (resp.uservalid !== 'true') {
                        event.trigger('CORE_INVALID_USER');
                    } else {
                        // parse subscribeversion from channelversion
                        var versions = resp.channelversion.split('|');
                        var isFreshChannel = false;
                        // 标记频道版本号是否发生变化
                        var isChannelVersionChanged = false;
                        var isFreshNpvr = false;
                        var isFirstTime = false;
                        resp.channelversion = versions[0];
                        resp.subscribeversion = versions[1];
                        _.each(versionWatchingPool, function(version) {
                            // First time, no need to cache data.
                            if (!version.current) {
                                isFirstTime = true;
                                if ('pvrversion' === version.id) {
                                    var npvrversion = _.filter(resp.personalDataVersions, function(_npvrversion) {
                                        return _npvrversion.key === 'NPVR';
                                    });
                                    version.current = npvrversion[0].value;

                                } else if ('blackoutversion' === version.id) {
                                    var blackoutversion = _.filter(resp.personalDataVersions, function(_blackoutversion) {
                                        return _blackoutversion.key === 'BLACKOUTPERIOD';
                                    });
                                    version.current = blackoutversion[0].value;
                                } else {
                                    version.current = resp[version.id];
                                }
                            } else {
                                // Version changed, need refresh data.
                                if (version.current !== resp[version.id]) {
                                    if ('pvrversion' === version.id) {
                                        var npvrversion = _.filter(resp.personalDataVersions, function(_npvrversion) {
                                            return _npvrversion.key === 'NPVR';
                                        });
                                        version.current = npvrversion[0].value;
                                    } else if ('blackoutversion' === version.id) {
                                        var blackoutversion = _.filter(resp.personalDataVersions, function(_blackoutversion) {
                                            return _blackoutversion.key = 'BLACKOUTPERIOD';
                                        });
                                        version.current = blackoutversion[0].value;
                                    } else if ('channelversion' !== version.id){
                                        version.current = resp[version.id];
                                    }
                                    //判断当前改变的版本号，如果是channelversion 则说明频道列表发生了变化
                                    if (version.id === 'channelversion') {
                                        isChannelVersionChanged = true;
                                        if (!isFreshChannel) {
                                            isFreshChannel = true;
                                            event.trigger(version.event, {
                                                version: resp[version.id]
                                            });
                                        }
                                    } else if (version.id === 'subscribeversion') {
                                        if (!isFreshChannel) {
                                            isFreshChannel = true;
                                            event.trigger(version.event);
                                        }
                                    } else {
                                        if ('pvrversion' === version.id) {
                                            isFreshNpvr = true;
                                        }
                                        event.trigger(version.event);
                                    }
                                }
                                /**
                                 *EPG在每次心跳中，都更新所有最新的录制中和未来录制的任务列表（QueryPVR，status：-1,1，filterlist建议传入STARTTIME和ENDTIME，为当前时间到下一个心跳时间点，以减少缓存数据，pvrType为2，npvr），缓存到本地。在UI展示中，根据当前时间点遍历缓存中的任务列表判断当前内容是否在录；
                                 */
                                if ('pvrversion' === version.id && !isFreshNpvr && !isFirstTime) {
                                    event.trigger('REFRESH_ONE_HEATBIT_NPVR');
                                }
                            }
                        });
                        if (!isFirstTime) {
                            //如果频道版本号未变化，则用原先的缓存中的数据获取节目版本号,如果发生了变化则在接受事件CORE_CHANNEL_VERSION_CHANGED的地方做处理
                            if (!isChannelVersionChanged) {
                                 app.playbillApp.refreshPlayBill(false);
                            }

                            app.channelApp.refreshBlackoutPeriod();
                            localStorage.removeItem('ControlledCategory');
                            /*心跳刷新，重新刷新配置查询受控的成人栏目ID*/
                            core.iptv.queryCustomConfig({
                                key: 'ControlledCategory',
                                type: 0
                            }).done(function(resp) {
                                if (resp.list && resp.list.length !== 0) {
                                    localStorage.setItem('ControlledCategory', resp.list[0].value);
                                }
                            });
                        }

                        // 每次心跳都触发获取VOD栏目，并比较数据是否有更新。有更新则刷新界面
                        gEventManager.trigger('APP_VOD_DATA_CHANGED');
                    }

                    // if (resp.nextcallinterval) {
                    //  heartbeatInterval = parseInt(resp.nextcallinterval, 10) * 1000;
                    // }



                    // heartbeatHandler = setTimeout(function() {
                    //  self.heartbeat();
                    // }, heartbeatInterval);
                }).fail(function(resp){
                    if(self.httpisFrist){
                        self.httpisFrist = false;
                        setTimeout(function(){
                            if(resp.httpResp.resultCode === 'http_503'){
                                app.heartbeatApp.heartbeat();
                            } 
                        },100)
                    }

                });
            },
            heartbeatCallback: function() {
                event.httpisFrist = true;
                app.heartbeatApp.heartbeat();
            }
        };
    }();

    return core;
}(eBase));
/**
 * Save user's play history.
 * @class eBase.app.historyApp
 * @singleton
 */
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        utils = core.utils,
        session = core.session,
        event = core.event,
        historyList = {
            'channel': [],
            'radioChannel': []
        },
        // Serialize history list to localStorage
        saveHistory = function() {
            /**session.put('history', JSON.stringify(historyList), true);*/
            console.log("saveHistory" + JSON.stringify(historyList.channel));
            localStorage.setItem('history', JSON.stringify(historyList.channel));
            var radioHistoryObj = localStorage.getItem('radioHistory') ? JSON.parse(localStorage.getItem('radioHistory')) : [];
            var curProfileRadioList = new Object();
            curProfileRadioList = _.find(radioHistoryObj, function(item) {
                if (item.profileId === eBase.session.get('currentProfile').id) {
                    item.radioHistoryList = historyList.radioChannel;
                    return true;
                }
            });
            if (!curProfileRadioList) {
                var req = {
                    profileId: eBase.session.get('currentProfile').id,
                    radioHistoryList: []
                };
                radioHistoryObj.push(req);
            }
            localStorage.setItem('radioHistory', JSON.stringify(radioHistoryObj));
            event.trigger('APP_HISTORY_DATA_CHANGED');
        },
        // Load history list to localStorage
        loadHistory = function(resp) {
            /**var historyString = session.get('history');*/
            var historyString = localStorage.getItem('history');
            var radioHistoryString = localStorage.getItem('radioHistory');
            if (historyString) 
            {
                try {
                    var _historyList = JSON.parse(historyString);
                    historyList.channel = _historyList;
                } catch (ex) {
                    var _tmp = [{
                        "id": "9999",
                        "channo": "9999",
                        "timestamp": "20140830224352"
                    }];
                    localStorage.setItem('history', JSON.stringify(_tmp));
                    console.error('[STBService] JSON.parse error: ' + ex.toString());
                }

            }
            if(radioHistoryString)
            {
                try {
                    var _radioHistoryList = JSON.parse(radioHistoryString);
                    //查找对应profile的radioList
                    var proRadioHistoryList = _.find(_radioHistoryList, function(proRadioHistory) {
                        return proRadioHistory.profileId === resp.data.profileId;
                    });
                    if (!proRadioHistoryList) {
                        var _tmp = {
                            "profileId": resp.data.profileId,
                            "radioHistoryList": []
                        };
                        var localRadioHistoryList = localStorage.getItem('radioHistory') ? JSON.parse(localStorage.getItem('radioHistory')) : [];
                        localRadioHistoryList.push(_tmp);
                        proRadioHistoryList = _tmp.radioHistoryList;
                        localStorage.setItem('radioHistory', JSON.stringify(localRadioHistoryList));
                    } else {
                        proRadioHistoryList = proRadioHistoryList.radioHistoryList;
                    }
                    historyList.radioChannel = proRadioHistoryList;
                } catch (ex) {
                    var _tmp = {
                            "profileId": resp.data.profileId,
                            "radioHistoryList": []
                        };
                    localStorage.setItem('radioHistory', JSON.stringify(_tmp));
                    console.error('[STBService] JSON.parse error: ' + ex.toString());
                }
            }
        };


    core.appReadyTrigger.register('APP_HISTORY_DATA_READY');

    // Query reminder when the profile login.
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function(resp) {
        loadHistory(resp);
        event.trigger('APP_HISTORY_DATA_READY');
        console.log('history ready');
    });

    app.historyApp = function() {
        return {
            /**
             * Add one history record.
             *
             * @param {Object} history History record (like channel).
             * @param {String} [type='channel'] History record type.
             *
             * @return {eBase.app.historyApp}
             */
            addHistory: function(history, type) {
                type = type || 'channel';

                historyList[type] = _.reject(historyList[type], function(record) {
                    return record.id === history.id;
                });

                historyList[type].unshift(_.extend(history, {
                    timestamp: utils.dateToString(new Date(), 'yyyyMMddHHmmss')
                }));

                while (historyList[type].length > config.historyLimit) {
                    historyList[type].pop();
                }

                saveHistory();

                return this;
            },
            /**
             * Delete one history record.
             *
             * @param {Object} history History record (like channel).
             * @param {String} [type='channel'] History record type.
             *
             * @return {eBase.app.historyApp}
             */
            deleteHistory: function(history, type) {
                type = type || 'channel';

                historyList[type] = _.reject(historyList[type], function(record) {
                    return record.id === history.id;
                });

                saveHistory();

                return this;
            },
            /**
             * Delete all history records.
             *
             * @param {String} [type='channel'] History record type.
             *
             * @return {eBase.app.historyApp}
             */
            clearHistory: function(type) {
                historyList[type || 'channel'] = [];

                saveHistory();

                return this;
            },
            /**
             * Query history records.
             *
             * @param {String} [type='channel'] History record type.
             *
             * @return {Array}
             */
            queryHistory: function(type) {
                console.log("queryHistory" + JSON.stringify(historyList[type || 'channel']));
                return historyList[type || 'channel'];
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        utils = core.utils,
        config = core.config,
        event = core.event;

    core.appReadyTrigger.register('APP_LOCK_DATA_READY');

    // Query lock when the profile login.
    event.on('APP_CHANNEL_DATA_READY', function() {
        console.debug("lixinying core_profile_login  refresh Lock start");
        if (config.isGuest) {
            app.lockApp.lockChannels = [];
            app.lockApp.lockVods = [];
            event.trigger('APP_LOCK_DATA_READY');
        } else {
            app.lockApp.refreshLock().always(function() {
                console.log('lock ready');
                event.trigger('APP_LOCK_DATA_READY');
            });
        }
    });
    // Query lock when the lock-version changed.
    event.on('MSG_LOCK_VERSION_CHANGE', function() {
        app.lockApp.refreshLock();
    });

    event.on('CORE_LOCK_VERSION_CHANGED', function() {
        app.lockApp.refreshLock();
    });

    /**
     * Lock related business logic.
     * @class eBase.app.lockApp
     * @singleton
     */
    var self = app.lockApp = function() {
        return {
            /**
             * This interface is used to query content in locks.
             * @param {Object} req
             * @param {String} [req.type=vod] Type of lockd contents.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Lock}
             */
            queryLock: function(req) {
                if (req && req.type === 'channel') {
                    return new Deferred().resolve({
                        list: self.lockChannels
                    });
                } else {
                    return new Deferred().resolve({
                        list: self.lockVods
                    });
                }
            },
            /**
             * This interface is used to add a lock.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             */
            addLock: function(req) {
                var dfd = new Deferred();

                core.iptv.addLock({
                    contentId: req.id,
                    contentType: utils.convertToServerType(req.type)
                }).done(function() {
                    // refresh lock in cache
                    self.refreshLock().done(function(resp) {
                        dfd.resolve(resp);
                        event.trigger('APP_LOCK_DATA_CHANGED');
                    }).fail(dfd.reject);
                }).fail(dfd.reject);

                return dfd;
            },
            /**
             * This interface is used to delete a lock.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             */
            deleteLock: function(req) {
                return core.iptv.deleteLock({
                    contentId: req.id,
                    contentType: utils.convertToServerType(req.type)
                }).done(function() {
                    // delete the lock in cache
                    self.lockVods = _.reject(self.lockVods, function(lock) {
                        return lock.id === req.id;
                    });
                    self.lockChannels = _.reject(self.lockChannels, function(lock) {
                        return lock.id === req.id;
                    });
                    event.trigger('APP_LOCK_DATA_CHANGED');
                });
            },
            /**
             * This interface is used to clear all locks.
             *
             * @return {eBase.core.Deferred}
             */
            clearLock: function() {
                return core.iptv.clearLock().done(function() {
                    // clear locks in cache
                    self.lockVods = [];
                    self.lockChannels = [];
                    event.trigger('APP_LOCK_DATA_CHANGED');
                });
            },
            /**
             * Refresh lock cache.
             * @private
             */
            refreshLock: function() {
                console.debug("lixinying refreshLock start");
                var retDfd = new Deferred();

                core.iptv.queryLock().done(function(resp) {
                    // refresh the lock in cache
                    var group = _.groupBy(resp.list, function(lock) {
                        return utils.convertToClientType(lock.type);
                    });
                    var channelIds = _.map(group.channel, function(lock) {
                        return lock.id;
                    });

                    Deferred.when(app.channelApp.getChannelDetail({
                        ids: channelIds
                    }), app.vodApp.queryVodByIds({
                        ids: group.vod ? _.pluck(group.vod, 'id').join(',') : ''
                    })).done(function(channelResp, vodResp) {
                        console.debug("lixinying refreshLock done");
                        self.lockChannels = [];
                        for (var i = 0; i < channelIds.length; i++) {
                            for (var k = 0; k < channelResp.list.length; k++) {
                                if (channelIds[i] === channelResp.list[k].id) {
                                    self.lockChannels.push(channelResp.list[k]);
                                }
                            }
                        }

                        self.lockVods = vodResp.list;
                        retDfd.resolve();
                    }).fail(retDfd.reject);
                });

                return retDfd;
            },
            /**
             * This interface is to check whether content is locked.
             * @param {Object} req
             * @param {String} req.id Content id.
             * @param {String} req.type Content type.
             *
             * - vod
             * - channel
             * @return {eBase.core.Deferred}
             */
            isLocked: function(content) {
                if (utils.convertToClientType(content.type) === 'channel') {
                    return !!_.find(self.lockChannels, function(lock) {
                        return lock.id === content.id;
                    });
                } else {
                    return !!_.find(self.lockVods, function(lock) {
                        return lock.id === content.id;
                    });
                }
            }
        };
    }();


    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * A client uses this interface to query extended attributes of a profile or subscriber.
     *
     * @param {Object} req
     * @param {Number} [req.userType = 0] User type.
     *
     * - 0: profile
     * - 1: subscriber
     * @param {String} [req.profileId] ID of the profile whose extended attributes will be obtained.
     * When userType is set to 1, you do not need to set profileId.
     * When userType is set to 0, if you leave profileId blank, extended attributes of the current login profile will be obtained; if you set profileId, extended attributes of the common profile specified by profileId will be obtained.
     *
     * @return {eBase.core.Deferred}
     * @return return.list List of {eBase.data.CustomConfig}
     */
    iptv.getDataVersion = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetDataVersion',
            data: JSON.stringify(req)
        });
    };

    return core;
}(eBase));

var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    iptv.getDeviceGroup = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetDeviceGroup',
            data: JSON.stringify(req)
        }).done(function(resp) {
        });
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    iptv.GetDeviceGroupByType = function(req) {
        return utils.jsonAjax({
            url: '/EPG/JSON/GetDeviceGroupByType',
            data: JSON.stringify(req)
        }).done(function(resp) {
        });
    };

    return core;
}(eBase));

var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        utils = core.utils;

    var PLAYBILL_LIST_PROPERTIES = ['id', 'type', 'name', 'starttime', 'endtime'].join(',');

    var injectFilter = function(req) {
        var filterList = [];

        if (_.has(req, 'country')) {
            filterList.push({
                key: 'Country',
                value: req.country
            });
            delete req.country;
        }

        if (_.has(req, 'genre')) {
            filterList.push({
                key: 'Genre',
                value: req.genre
            });
            delete req.genre;
        }

        if (!_.isEmpty(filterList)) {
            req.filterlist = filterList;
        }
    };

    var injectDate = function(req, queryPlaybillFlag) {
        if (!req.productPPV) {
            req.date.setHours(0);
            req.date.setMinutes(0);
            req.date.setSeconds(0);
            var beginTime = req.date;
            if (config.domestic) {
                req.beginTime = utils.dateToString(beginTime, 'yyyyMMddHHmmss');
            } else {
                req.beginTime = utils.dateToUTCString(beginTime, 'yyyyMMddHHmmss');
            }
        } else {
            req.beginTime = utils.dateToUTCString(req.date, 'yyyyMMddHHmmss');
        }

        if (!req.productPPV) {
            req.date.setHours(23);
            req.date.setMinutes(59);
            req.date.setSeconds(59);
            var endTime = req.date;
            if (config.domestic) {
                req.endTime = utils.dateToString(endTime, 'yyyyMMddHHmmss');
            } else {
                req.endTime = utils.dateToUTCString(endTime, 'yyyyMMddHHmmss');
            }
        }

        delete req.productPPV;
        delete req.date;
    };

    var injectProperties = function(req, type) {
        switch (type) {
            case 'list':
                req.properties = [{
                    name: 'playbill',
                    include: config.playbillListProperties || PLAYBILL_LIST_PROPERTIES
                }];
                break;
            case 'detail':
                if (config.playbillDetailProperties) {
                    req.properties = [{
                        name: 'playbill',
                        include: config.playbillDetailProperties
                    }];
                }
                break;
            default:
                break;
        }
    };

    var playbillList = {};

    var getCachedList = function(req, type) {
        var dateStamp = req.beginTime.substr(0, 8);
        if (type === 'list') {
            var playbillArr = [];
            var channel = eBase.app.channelApp.getChannelById(req.channelId);
            playbillArr = playbillList[req.channelId] && playbillList[req.channelId][dateStamp];
            if (playbillArr && channel) { // && channel.istvod === '1'
                playbillArr = _.filter(playbillArr, function(playbill) {
                    return !((playbill.endDate <= new Date() && playbill.id === '-1') || playbill.id === 'NAN');
                });
            }
            return playbillArr;
        } else {
            return null;
        }
    };
    var setCachedList = function() {
        playbillList = {};
    };
    var cacheList = function(req, resp) {
        var dateStamp = req.begintime.substr(0, 8);
        var channel = eBase.app.channelApp.getChannelById(req.channelid);
        var playbillArr = [];
        if (channel) { //&& channel.istvod === '1'
            playbillArr = _.filter(resp.list, function(playbill) {
                return !(gUtils.utcStringToDate(playbill.endtime) <= new Date() && playbill.id === '-1');
            });

            /**if (playbillArr && playbillArr.length > 0 && gUtils.dateToString(gUtils.utcStringToDate(playbillArr[0].starttime), 'yyyyMMdd') === dateStamp) {
                playbillArr.splice(0, 1);
            }*/
        }
        playbillList[req.channelid] = playbillList[req.channelid] || {};
        playbillList[req.channelid][dateStamp] = playbillArr.length > 0 ? playbillArr : resp.list;

        var versionList = resp.playbillVersion;
        if (versionList) {
            //versionList = JSON.parse("[" + versionList + "]");
            _.each(versionList, function(version) {
                var key = version.date + "version";
                playbillList[version.channelId][key] = version.version;
            });
        }
        resp.list = playbillArr.length > 0 ? playbillArr : resp.list;
        return resp;
    };
    var freshCachedList = function(req) {
        var dateStamp = req.begintime.substr(0, 8);
        if (playbillList && playbillList[req.channelid]) {
            var ret;
            var stampArr = [dateStamp];
            if (req.playbill) {
                var startStamp = utils.dateToString(req.playbill.startDate, 'yyyyMMddHHmmss').substr(0, 8);
                var endStamp = utils.dateToString(req.playbill.endDate, 'yyyyMMddHHmmss').substr(0, 8);
                if (startStamp != dateStamp) {
                    stampArr.push(startStamp);
                }
                if (startStamp != endStamp) {
                    stampArr.push(endStamp);
                }
            }

            for (var i = 0; i < stampArr.length; i++) {
                ret = _.find(playbillList[req.channelid][stampArr[i]], function(playbill) {
                    if (playbill.id === req.playbill.id) {
                        playbill.issubscribed = '1';
                        return true;
                    } else {
                        return false;
                    }
                });
            }
            return ret;
        }

    };
    var checkVersion = function(version) {
        var date = version.date;
        var versionNum = version.version;
        var key = date + "version";
        if (playbillList[version.channelId] && playbillList[version.channelId][key] != versionNum) {
            playbillList[version.channelId] = null;
        }
    };

    var checkExpired = function() {
        var curDate = new Date().getDate();
        var timeList = [];
        for (var i = 0; i < 11; i++) {
            timeList.push(utils.dateToString(new Date(new Date().setDate(curDate + i - 3)), 'yyyyMMddHHmmss').substr(0, 8));
            timeList.push(utils.dateToString(new Date(new Date().setDate(curDate + i - 3)), 'yyyyMMddHHmmss').substr(0, 8) + 'version');
        }
        for (var key in playbillList) {
            if (!eBase.app.channelApp.getChannelById(key) && _.has(playbillList, key)) {
                delete playbillList[key];
                continue;
            }
            for (var _key in playbillList[key]) {
                if (-1 === timeList.indexOf(_key) && _.has(playbillList, key)) {
                    delete playbillList[key][_key];
                }
            }
        }

    };

    var clearPlaybillByChaId = function(channelId) {
        delete playbillList[channelId];
    };

    var getAllPlayBillCache = function() {
        return playbillList;
    };



    /**
     * Playbill related business logic.
     * @class eBase.app.playbillApp
     * @singleton
     */
    var self = app.playbillApp = function() {
        return {
            /**
             * A client uses this interface to obtain program lists of a specified channel.
             * When a subscriber views the channel information, the client can invoke this interface to obtain and display program lists of this channel.
             *
             * @param {Object} req
             * @param {String} req.channelId Channel ID.
             * @param {Date} req.date Program date.
             * @param {Number} [req.count] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
             * @param {Number} [req.offset] Start position for querying content.
             * If the parameter is not set, programs are sorted by start time by default.
             * If program start times are the same, programs are sorted by program ID in descending order.
             */
            queryPlaybill: function(req, type) {
                injectDate(req, true);

                var retDfd = new Deferred(),
                    cachedList = getCachedList(req, type);

                if (typeof(req.type) === 'undefined') {
                    req.type = 2;
                }

                injectFilter(req);
                injectProperties(req, type);
                if (cachedList) {
                    retDfd.resolve({
                        list: cachedList
                    });
                } else {
                    core.iptv.queryPlaybill(req).done(function(resp) {
                        if (type === 'list') {
                            cacheList(req, resp);
                        }
                        retDfd.resolve(resp);
                    }).fail(retDfd.reject);
                }

                return retDfd;
            },

            clearPlaybillByChaId: function(programId) {
                self.queryPlaybillByIds({
                    ids: programId
                }).done(function(resp) {
                    clearPlaybillByChaId(resp.list[0] && resp.list[0].channelid);
                })
            },

            refreshPlayBill: function(isFreshChannel) {
                console.log('--ebase-core-stb---refreshPlayBill--Start');
                if (isFreshChannel) {
                    checkExpired();
                }
                var playbillCache = getAllPlayBillCache();
                var cachedChannelidList = _.keys(playbillCache);
                console.log('--ebase-core-stb---isFreshChannel--isFreshChannel'+isFreshChannel);
                //过滤数组中的undefined
                cachedChannelidList = _.filter(cachedChannelidList, function(id) {
                    if (!utils.isNull(id)) {
                        return true;
                    }
                    return false;
                });
                // 当频道id为空的时候，不需要获取program version
                if (cachedChannelidList.length > 0) {
                    self.getProgVersion(cachedChannelidList);
                }
                // self.getProgVersion(cachedChannelidList);
            },

            getProgVersion: function(ids) {
                var idStr = ids.join(",");
                console.log('----ebase-core-stb------idStr---'+JSON.stringify(idStr));
                core.iptv.getDataVersion({
                    contentType: "PROGRAM",
                    contentIds: idStr
                }).done(function(resp) {
                    var versions = resp.version;
                    //versions = JSON.parse("[" + versions + "]");
                    _.each(versions, function(version) {
                        checkVersion(version);
                    });
                });
            },

            queryPlaybillForMytv: function(req, type) {
                injectDate(req);
                var retDfd = new Deferred();

                if (typeof(req.type) === 'undefined') {
                    req.type = 2;
                }

                injectFilter(req);
                injectProperties(req, type);

                core.iptv.queryPlaybill(req).done(function(resp) {

                    retDfd.resolve(resp);
                }).fail(retDfd.reject);


                return retDfd;
            },
            /**
             * Get playbill list with 'list' properties.
             *
             * @param {Object} req
             * @param {String} req.channelId Channel ID.
             * @param {Date} req.date Program date.
             * @param {Number} [req.count] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
             * @param {Number} [req.offset] Start position for querying content.
             * If the parameter is not set, programs are sorted by start time by default.
             * If program start times are the same, programs are sorted by program ID in descending order.
             */
            queryPlaybillList: function(req) {
                return self.queryPlaybill(req, 'list');
            },
            cleanPlaybillList: function() {
                setCachedList();
            },
            freshPlaybillList: function(req) {
                freshCachedList(req);
            },
            /**
             * Get playbill list with 'detail' properties.
             *
             * @param {Object} req
             * @param {String} req.channelId Channel ID.
             * @param {Date} req.date Program date.
             * @param {Number} [req.count] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
             * @param {Number} [req.offset] Start position for querying content.
             * If the parameter is not set, programs are sorted by start time by default.
             * If program start times are the same, programs are sorted by program ID in descending order.
             */
            queryPlaybillDetail: function(req) {
                return self.queryPlaybill(req, 'detail');
            },
            /**
             * Get playbill list with ids.
             *
             * @param {Object} req
             * @param {String} req.ids Playbill id list seperated with ','
             *
             * @return {eBase.core.Deferred}
             * @return {eBase.data.Playbill[]} return.list
             */
            queryPlaybillByIds: function(req) {
                return core.iptv.queryDetail({
                    playbill: req.ids,
                    properties: [{
                        name: 'playbill',
                        properties: config.playbillListProperties || PLAYBILL_LIST_PROPERTIES
                    }]
                }).done(function(resp) {
                    utils.rename(resp, 'playbilllist', 'list');
                });
            },
            queryPlaybillByIdsForNoFilter: function(req) {
                return core.utils.jsonAjax({
                    url: '/EPG/JSON/ContentDetail',
                    data: JSON.stringify({
                        filterType: 1,
                        playbill: req.ids,
                        properties: [{
                            name: 'playbill',
                            properties: config.playbillListProperties || PLAYBILL_LIST_PROPERTIES
                        }]
                    })
                }).done(function(resp) {
                    utils.rename(resp, 'playbilllist', 'list');
                });
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        event = core.event;
    /**
     * PPV related business logic.
     * @class eBase.app.ppvApp
     * @singleton
     */
    app.ppvApp = function() {
        return {
             /**
             * get ppv channel list
             */
            queryPPVCategory: function() {
                eBase.iptv.queryCustomConfig({
                    key: 'CHANNEL_PPV_CATEGORY',
                    type: 0
                }).done(function(resp) {
                    var ppvCategoryId = null;
                    if (resp.list.length > 0) {
                        ppvCategoryId = resp.list[0].value;
                    }
                    if (ppvCategoryId) {
                        var request = {"offset":0,"count":5,"filterlist":[{"key":"SubscriptionType","value":"2"}]};
                        request.categoryid = ppvCategoryId;           
                        eBase.app.channelApp.queryChannel(_.extend(request, {
                                type: 'category'
                        })).done(function(resp) {
                            var channelList = resp.list || [];
                            if (channelList.length) {
                               app.channelApp.isExistPPVChannel = true;
                            } else {
                                app.channelApp.isExistPPVChannel = false;
                            }
                        }).always(function() {
                            event.trigger('APP_PPV_DATA_READY');
                        });
                    }else {
                    	event.trigger('APP_PPV_DATA_READY');
                    }
                }).fail(function(){
                    event.trigger('APP_PPV_DATA_READY');
                    console.log('iptv.queryCustomConfig CHANNEL_PPV_CATEGORY execute failed.');
                });
            }
        };
    }();
    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        utils = core.utils,
        event = core.event;

    // Map password type to MEM
    var PASSWORD_TYPE_MAP = {
        profile: 0,
        subscription: 1,
        account: 2,
        master: 3,
        parent: 4
    };

    core.appReadyTrigger.register('APP_PROFILE_DATA_READY');

    // 切换profile
    core.appReadyTrigger.register('LOGIN_PROFILE_CHANGED');

    event.on('CORE_AUTHENTICATE_SUCCESS', function(event) {
        app.profileApp.profileList = event.data.list;
        var req = {
                queryType: 0
            };
        eBase.iptv.queryCustomConfig(req).done(function(resp){
            _.each(resp.list, function(item){
                if(item.key == 'vas_root_id'){
                    eBase.session.put(item.key, item.value);
                } else if (item.key == 'flight_id'){
                    eBase.session.put(item.key, item.value);
                } else if (item.key == 'weather_id'){
                    eBase.session.put(item.key, item.value);
                } else if (item.key == 'youtube_id'){
                    eBase.session.put(item.key, item.value);
                } else if (item.key == 'extra_root_id'){
                    eBase.session.put(item.key, item.value);
                } else if (item.key == 'news_id'){
                    eBase.session.put(item.key, item.value);
                }
            });
            
        }).fail(function(){
            console.log('iptv.queryCustomConfig VASCategory execute failed.');
        });
    });
    //Get custom config and current profile when the profile login.
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function(event) {
        // Refresh cache
        var oldProfile = app.profileApp.currentProfile;
        app.profileApp.currentProfile = _.find(app.profileApp.profileList, function(profile) {
            return profile.id === event.data.profileId;
        });
        app.profileApp.masterProfile = _.find(app.profileApp.profileList, function(profile) {
            return profile.profiletype === '0';
        });
        app.profileApp.customConfig = {};

        _.each(event.data.list, function(configuration) {
            _.each(configuration.extensionInfo, function(extention) {
                var key = extention.key;
                var value = extention.value;

                app.profileApp.customConfig[key] = value;
            });
        });

        if (oldProfile && oldProfile.id !== app.profileApp.currentProfile.id) {
            console.debug('loginProfileChanged-------------------------');
            core.event.trigger('LOGIN_PROFILE_CHANGED');
        }

        core.event.trigger('APP_PROFILE_DATA_READY');
        console.log('profile ready');

        eBase.iptv.getDeviceGroup().done(function(req){
            eBase.session.put('deviceGroups',req.deviceGroups);
        }).fail(function(){
              console.log('iptv.getDeviceGroup execute failed.');
        });
    });

    /**
     * Business logic for profiles.
     *
     * @class eBase.app.profileApp
     * @singleton
     */
    var self = app.profileApp = function() {
        return {
            /**
             * Configurations of current profile.
             * @property customConfig
             * @type {Object}
             */
            customConfig: {},
            /**
             * A client uses this interface to query the information about profiles.
             *
             * @param {Object} req
             * @param {String} [req.type=all] Type of profiles to be queried.
             *
             * - all
             * - current
             * - master
             *
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Profile}
             */
            queryProfile: function(req) {
                req = req || {
                    type: 'all'
                };

                switch (req.type) {
                    case 'all':
                        return self.queryAllProfile();
                    case 'current':
                        return self.queryCurrentProfile();
                    case 'master':
                        return self.queryMasterProfile();
                    default:
                        break;
                }
            },
            queryAllProfile: function() {
                if (self.profileList) {
                    return new Deferred().resolve({
                        list: self.profileList
                    });
                } else {
                    return core.iptv.queryProfile({
                        type: 1
                    }).done(function(resp) {
                        self.profileList = resp.list;
                        self.masterProfile = _.find(self.profileList, function(profile) {
                            return profile.profiletype === '0';
                        });
                    });
                }
            },
            queryMasterProfile: function() {
                if (self.masterProfile) {
                    return new Deferred().resolve({
                        list: [self.masterProfile]
                    });
                } else {
                    return core.iptv.queryProfile({
                        type: 2
                    }).done(function(resp) {
                        self.masterProfile = resp.list[0];
                    });
                }
            },
            queryCurrentProfile: function() {
                if (self.currentProfile) {
                    return new Deferred().resolve({
                        list: [self.currentProfile]
                    });
                } else {
                    return core.iptv.queryProfile({
                        type: 0
                    }).done(function(resp) {
                        self.currentProfile = resp.list[0];
                    });
                }
            },
            /**
             * Search profile with keyword.
             *
             * @param {Object} req
             * @param {String} req.key Keyword of profile name.
             *
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Profile}
             */
            searchProfile: function(req) {
                return core.iptv.queryProfile({
                    type: 5,
                    condition: [req.key]
                });
            },
            addProfile: function(req) {
                var retDfd = new Deferred();

                req.password = core.config.isSuuportaes ? utils.aesEncrypt(utils.md5(req.password)) : utils.md5(req.password); //utils.aes.Ctr.encrypt(req.password, eBase.session.get('signatureKey').substring(0, 32), 128);// utils.md5(req.password);

                core.iptv.addProfile(req).done(function() {
                    self.profileList = null;
                    self.queryAllProfile().done(function() {
                        retDfd.resolve();
                        event.trigger('APP_PROFILE_DATA_CHANGED');
                    }).fail(retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            modifyProfile: function(req) {
                var retDfd = new Deferred();

                if (req.password) {
                    req.password = core.config.isSuuportaes ? utils.aesEncrypt(utils.md5(req.password)) : utils.md5(req.password); //utils.aes.Ctr.encrypt(req.password, eBase.session.get('signatureKey').substring(0, 32), 128);//utils.md5(req.password);
                }

                core.iptv.updateProfile(req).done(function() {
                    self.profileList = null;
                    self.queryAllProfile().done(function() {
                        retDfd.resolve();
                        event.trigger('APP_PROFILE_DATA_CHANGED');
                    }).fail(retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            deleteProfile: function(req) {
                var retDfd = new Deferred();
                var deleteProfileId = req;
                core.iptv.deleteProfile(req).done(function() {
                    self.profileList = null;
                    self.queryAllProfile().done(function() {
                        retDfd.resolve();
                        event.trigger('APP_PROFILE_DATA_CHANGED', deleteProfileId);
                    }).fail(retDfd.reject);
                });

                return retDfd;
            },
            /**
             * Switch to another Profile
             * @param {Object} req
             * @param {String} req.id profile id
             * @param {String} req.password password
             * @param {Boolean} [req.encrypted=false] Whether password is encrypted.
             *
             * @return {eBase.core.Deferred}
             */
            switchProfile: function(req) {
                var password = core.config.isSuuportaes ? utils.aesEncrypt(utils.md5(req.password)) : utils.md5(req.password);
                req.password = req.encrypted ? req.password : password; //utils.aes.Ctr.encrypt(req.password, eBase.session.get('signatureKey').substring(0, 32), 128);//utils.md5(req.password);
                delete req.encrypted;

                return core.iptv.switchProfile(req);
            },
            /**
             * A client uses this interface to check passwords.
             *
             * @param {Object} req
             * @param {String} req.password Password in plaintext.
             * @param {String} [req.type=master] Password type
             *
             * - profile
             * - subscription
             * - account
             * - master
             * - parent
             *
             * @return {eBase.core.Deferred}
             */
            checkPassword: function(req) {
                req.type = req.type || 'master';
                req.type = PASSWORD_TYPE_MAP[req.type];

                req.password = core.config.isSuuportaes ? utils.aesEncrypt(utils.md5(req.password)) : utils.md5(req.password); //utils.md5(req.password); //utils.aes.Ctr.encrypt(req.password, eBase.session.get('signatureKey').substring(0, 32), 128);//utils.md5(req.password);

                return core.iptv.checkPassword(req);
            },
            /**
             * Change the common profile password, subscription password, subscriber password, super profile password, and parental control password.
             *
             * @param {Object} req
             * @param {String} req.oldPassword Old password. The old password must be encrypted using the encryption mode.
             * @param {String} req.newPassword New password. The new password must be encrypted using the encryption mode.
             * @param {String} req.confirmpwd Confirm password. The confirm password must be the same as the new password and be encrypted using the encryption mode.
             * @param {String} [req.type=master] Password type
             *
             * - profile
             * - subscription
             * - account
             * - master
             * - parent
             *
             * @return {eBase.core.Deferred}
             */
            updatePassword: function(req) {
                req.type = req.type || 'master';
                req.type = PASSWORD_TYPE_MAP[req.type];
                if (core.config.isSuuportaes) {
                    var signatureKey = utils.sha256('Vpti12_' + 'Dhome_NMSKey12').substring(0, 32);
                    var plaintText = CryptoJS.enc.Utf8.parse(req.newPassword);
                    var key = CryptoJS.enc.Hex.parse(signatureKey);

                    var encryptedData = CryptoJS.AES.encrypt(plaintText, key, {
                        mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.Pkcs7
                    });
                    req.encryptpwd = encryptedData.ciphertext.toString()
                };
                req.oldPassword = utils.md5(req.oldPassword);
                req.newPassword = utils.md5(req.newPassword);
                req.confirmPassword = req.newPassword;
                //req.encryptpwd = utils.aes.Ctr.encrypt(req.newPassword, signatureKey, 128);

                return core.iptv.updatePassword(req);
            },
            resetPassword: function(req) {
                req.type = req.type || 'profile';
                req.type = PASSWORD_TYPE_MAP[req.type];
                if (core.config.isSuuportaes) {
                    var signatureKey = utils.sha256('Vpti12_' + 'Dhome_NMSKey12').substring(0, 32);
                    var plaintText = CryptoJS.enc.Utf8.parse(req.resetpwd);
                    var key = CryptoJS.enc.Hex.parse(signatureKey);

                    var encryptedData = CryptoJS.AES.encrypt(plaintText, key, {
                        mode: CryptoJS.mode.ECB,
                        padding: CryptoJS.pad.Pkcs7
                    });
                    req.encryptpwd = encryptedData.ciphertext.toString()
                };
                req.resetpwd = core.config.isSuuportaes ? utils.aesEncrypt(utils.md5(req.resetpwd)) : utils.md5(req.resetpwd);
                return core.iptv.resetPassword(req);
            },

            refreshProfile: function() {

                console.log('CORE_PROFILE_VERSION_CHANGED---refreshProfile');

                core.iptv.queryProfile({
                    type: 1
                }).done(function(resp) {

                    console.log('CORE_PROFILE_VERSION_CHANGED---queryProfile---all---ok');

                    self.profileList = resp.list;
                    self.masterProfile = _.find(self.profileList, function(profile) {
                        return profile.profiletype === '0';
                    });

                    core.appReadyTrigger.register('APP_REFRESH_PROFILE_UI');
                    core.event.trigger('APP_REFRESH_PROFILE_UI');
                });
            }

        };

    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {};

    /**
     * PVR related business logic.
     * @class eBase.app.pvrApp
     * @singleton
     */
    app.pvrApp = function() {
        return {};
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        event = core.event,
        config = core.config,
        utils = core.utils,
        reminderHandler = -1;

    var REMINDER_CHECK_INTERVAL = 1 * 60 * 1000;

    var checkReminder = function() {
        if ('-1' === eBase.session.get('currentProfile')) {
            clearInterval(reminderHandler);
            return;
        }
        /**app.reminderApp.refreshReminder().always(function() {*/
        var leadTime = new Date(),
            now = new Date();
        leadTime.setSeconds("00");
        now.setSeconds("00");

        //leadTime.setMinutes(leadTime.getMinutes() + config.reminderLeadingTime);

        /**DTS2015081204543,提醒时间判断以分钟级别*/
        var reminderList = _.filter(app.reminderApp.reminderList, function(reminder) {
            var startTime = utils.stringToDate(reminder.reminderTime, 'yyyyMMddHHmmss');
            startTime.setSeconds("00"); /**DTS2015091003559 program starttime with second time*/
            console.log("ebase--checkReminder---startTime==" + startTime + "---now==" + now + "--leadTime==" + leadTime);
            console.log("ebase--checkReminder---contentID==" + reminder.contentID);
            return Date.parse(startTime) == Date.parse(now);
        });

        if (!_.isEmpty(reminderList)) {
            event.trigger('APP_REMINDER_ALARM', reminderList);
        }

        var _ppvChannelList = app.channelApp.getPpvChannelList();
        var _orderTime = {
            startTime: '',
            endTime: ''
        };
        for (var i = 0; i < _ppvChannelList.length; i++) {
            _orderTime.startTime = eBase.utils.utcStringToDate(_ppvChannelList[i].serstarttime, 'yyyyMMddHHmmss');
            _orderTime.endTime = eBase.utils.utcStringToDate(_ppvChannelList[i].serendtime, 'yyyyMMddHHmmss');
            if (new Date() > _orderTime.endTime) {
                event.trigger('CORE_SUBSCRIBE_VERSION_CHANGED');
                app.playbillApp.refreshPlayBill(false);
                break;
            }
        }
        /**});*/

    };

    core.appReadyTrigger.register('APP_REMINDER_DATA_READY');

    // Query reminder when the profile login.
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function() {
        if (config.isGuest) {
            app.reminderApp.reminderList = [];
            event.trigger('APP_REMINDER_DATA_READY');
        } else {
            if ('-1' === eBase.session.get('currentProfile')) {
                clearInterval(reminderHandler);
                return;
            }
            app.reminderApp.refreshReminder().always(function() {
                clearInterval(reminderHandler);
                reminderHandler = setInterval(checkReminder, REMINDER_CHECK_INTERVAL);
                console.log('reminder ready');
                event.trigger('APP_REMINDER_DATA_READY');
            });
        }
    });

    // Query reminder when the reminder-version changed.
    event.on('CORE_REMINDER_VERSION_CHANGED', function() {
        app.reminderApp.refreshReminder();
    });

    /**
     * Reminder related business logic.
     * @class eBase.app.reminderApp
     * @singleton
     */
    var self = app.reminderApp = function() {
        return {
            checkReminder: checkReminder,
            /**
             * This interface is used to query subscribers' program reminders. The system supports the function of querying reminders by type.
             * If a schedule is deleted, the platform automatically deletes reminders of this schedule.
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Reminder}
             */
            queryReminder: function() {
                return new Deferred().resolve({
                    list: self.reminderList
                });
            },
            /**
             * This interface is used to create a reminder.
             *
             * @param {Object} req
             * @param {String} req.id Program ID.
             * @return {eBase.core.Deferred}
             */
            addReminder: function(req) {
                var dfd = new Deferred();

                core.iptv.addReminder({
                    reminderContents: [{
                        type: 1,
                        contentID: req.id,
                        contentType: 'PROGRAM'
                    }]
                }).done(function() {
                    // succeed to create reminder, refresh cached reminders
                    self.refreshReminder().done(function(resp) {
                        event.trigger('APP_REMINDER_DATA_CHANGED');
                        dfd.resolve(resp);
                    }).fail(dfd.reject);
                }).fail(dfd.reject);

                return dfd;
            },
            /**
             * This interface is used to delete a reminder.
             *
             * @param {Object} req
             * @param {String} req.id Program ID.
             * @return {eBase.core.Deferred}
             */
            deleteReminder: function(req) {
                console.debug('chenboyu reminder deleteReminder start');
                return core.iptv.deleteReminder({
                    reminderContents: [{
                        type: 1,
                        contentID: req.id,
                        contentType: 'PROGRAM'
                    }]
                }).done(function() {
                    // delete the reminder in cache
                    self.reminderList = _.reject(self.reminderList, function(reminder) {
                        return reminder.contentID === req.id;
                    });
                    event.trigger('APP_REMINDER_DATA_CHANGED');
                    console.debug('chenboyu reminder deleteReminder end');
                });
            },
            /**
             * This interface is used to clear all reminders.
             *
             * @return {eBase.core.Deferred}
             */
            clearReminder: function() {
                return core.iptv.clearReminder().done(function() {
                    // succeed to clear reminder
                    // clear reminders in cache
                    self.reminderList = [];
                    event.trigger('APP_REMINDER_DATA_CHANGED');
                });
            },
            /**
             * Refresh reminder cache.
             * @private
             */
            refreshReminder: function() {
                return core.iptv.queryReminder({
                    ordertype: 1
                }).done(function(resp) {
                    // refresh the reminder in cache
                    self.reminderList = resp.list;
                    self.reminderList = _.sortBy(self.reminderList, 'reminderTime');
                    if (!config.domestic) {
                        _.each(self.reminderList, function(reminder) {
                            var time = utils.utcStringToDate(reminder.reminderTime);
                            reminder.reminderTime = utils.dateToString(time, 'yyyyMMddHHmmss');
                        });
                    }
                });
            },
            /**
             * Check if program has reminder.
             * @param {Object} req
             * @param {String} req.id Program ID
             * @return {Boolean} isReminder
             */
            isReminder: function(req) {
                return !!_.find(self.reminderList, function(reminder) {
                    return reminder.contentID === req.id;
                });
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        utils = core.utils;

    var injectFilter = function(req) {
        var filterList = [];

        if (_.has(req, 'country')) {
            filterList.push({
                key: 'Country',
                value: req.country
            });
            delete req.country;
        }

        if (_.has(req, 'subscriptionType')) {
            filterList.push({
                key: 'SubscriptionType',
                value: req.subscriptionType
            });
            delete req.subscriptionType;
        }

        if (_.has(req, 'genre')) {
            filterList.push({
                key: 'Genre',
                value: req.genre
            });
            delete req.genre;
        }

        if (_.has(req, 'initial')) {
            filterList.push({
                key: 'Initial',
                value: req.initial
            });
            delete req.initial;
        }

        if (!_.isEmpty(filterList)) {
            req.filterList = filterList;
        }
    };

    /**
     * Search related business logic.
     * @class eBase.app.searchApp
     * @singleton
     */
    app.searchApp = function() {
        return {
            /**
             * A client invokes this interface to search for specified content.
             * The search function enables a subscriber to easily locate specific programs by entering keywords or setting filtering criteria.
             * After the subscriber sets search criteria on the content search page, the client invokes this interface to obtain the list of specified content.
             *
             * @param {Object} req
             * @param {String} req.key Search keywords.
             * @param {String} req.contentType Content type.
             *
             * - vod
             * - channel
             * - playbill
             * - tvod
             * @param {String} req.searchType Search type.
             *
             * - name
             * - genre
             * - tag
             * - actor
             * - director
             * - author
             * - singer
             * - producer
             * - playwright
             * - other
             * - all
             * - keyword
             * @param {Number} [req.count] Total number of content items obtained for one query. If all content items are obtained, the value is -1.
             * @param {Number} [req.offset] Start position for querying content.
             *
             * @return {eBase.core.Deferred}
             * @return return.list {Array} List of {eBase.data.SearchContent}
             */
            search: function(req) {
                var searchTypeMap = {
                    name: 1,
                    genre: 2,
                    tag: 4,
                    actor: 8,
                    director: 16,
                    author: 32,
                    singer: 64,
                    producer: 128,
                    playwright: 256,
                    other: 512,
                    all: 1023,
                    keyword: 2050
                };

                injectFilter(req);

                req.contentType = utils.convertToServerType(req.contentType);
                req.type = searchTypeMap[req.searchType];
                delete req.searchType;

                return core.iptv.search(req);
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config;

    var split = function(str, character) {
        var point = str.indexOf(character);

        if (point > 0) {
            return [str.substring(0, point), str.substring(point + character.length)];
        } else {
            return [str];
        }
    };

    // Convert upgrade info file to JSON object.
    var iniToJson = function(str) {
        str = (str || '').replace(/\n[\s| ]*\r/g, '').trim();

        var list = _.compact(str.split('\n')),
            json = {},
            item = {};
        for (var i = 0; i < list.length; i++) {
            var line = list[i];
            if (line.trim()) {
                var match = /\[(\w*)\]/.exec(line);
                if (match) {
                    item = {};
                    json[match[1]] = item;
                } else {
                    var splits = split(line, '=');
                    if (splits.length === 2) {
                        item[splits[0].trim()] = splits[1].trim();
                    }
                }
            }
        }

        return json;
    };

    // Calculate version (like '1.0.1') to number.
    var convertVersonToNumber = function(verStr) {
        var match = /(\d*)\.(\d*)\.(\d*)/.exec(verStr);
        var list = _.map(match.slice(1), function(s) {
            if (s.length === 1) {
                return '00' + s;
            } else if (s.length === 2) {
                return '0' + s;
            } else {
                return s;
            }
        });

        return parseInt(list.join(''), 10);
    };

    // Concat server address to upgrade file.
    var fillUpgradeUrl = function(fileName) {
        if (fileName && fileName.indexOf('http') !== 0) {
            var param = '/jsp/upgrade.jsp?TYPE=' + config.terminalType + '&FILENAME=' + fileName;

            return core.session.get('upgradeUrl') + param;
        } else {
            return fileName;
        }
    };

    /**
     * Communicate with UPGRADE for upgrade info.
     * @class eBase.app.upgradeApp
     * @singleton
     */
    app.upgradeApp = function() {
        return {
            /**
             * Get upgrade info
             * @param {Object} req
             * @param {String} req.userId ID of user account.
             * @return {Object}
             * @return {Boolean} return.isNeedUpgrade Whether upgrade is needed. (Local version is lower than server version)
             * @return {Boolean} return.isForceUpgrade Whether this upgrade is forced.
             * @return {Object} return.upgradeInfo Upgrade info in JSON format.
             */
            checkUpgrade: function(req) {
                var retDfd = new Deferred();

                core.iptv.checkUpgrade(req).done(function(resp) {
                    var upgradeInfo = iniToJson(resp);
                    var needUpgrade = false;

                    if (upgradeInfo.UPDATEAPP) {
                        upgradeInfo.UPDATEAPP.FileName = fillUpgradeUrl(upgradeInfo.UPDATEAPP.FileName);
                        upgradeInfo.UPDATEAPP.AppId = fillUpgradeUrl(upgradeInfo.UPDATEAPP.AppId);
                    }

                    try {
                        var localVersion = convertVersonToNumber(config.clientVersion);
                        var currVersion = convertVersonToNumber(upgradeInfo.UPDATEAPP.Version);

                        core.debug('compare version [ localVersion, currVersion ]', [localVersion, currVersion]);

                        if (localVersion < currVersion) {
                            needUpgrade = true;
                        }
                    } catch (e) {
                        core.error('upgrade information format error: ', resp);
                    }

                    retDfd.resolve({
                        isNeedUpgrade: needUpgrade,
                        isForceUpgrade: upgradeInfo.UPDATEAPP.isForceUpgrade === 'true',
                        upgradeInfo: upgradeInfo
                    });
                }).fail(retDfd.reject);

                return retDfd;
            }
        };
    }();

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        utils = core.utils;

    var VOD_BASE_PROPERTIES = ['id', 'type'].join(','),
        VOD_LIST_PROPERTIES = ['id', 'type', 'name', 'picture','deviceGroups','bizdomain'].join(',');

    var injectExcluder = function(req) {
        var excluderlist = [];

        if (_.has(req, 'excludeProduceZone')) {
            excluderlist.push({
                key: 'Producezone',
                value: req.excludeProduceZone
            });
            delete req.excludeProduceZone;
        }

        if (_.has(req, 'excludePublishDate')) {
            excluderlist.push({
                key: 'Publishdate',
                value: req.excludePublishDate
            });
            delete req.excludePublishDate;
        }

        if (_.has(req, 'excludeGenre')) {
            excluderlist.push({
                key: 'Genre',
                value: req.excludePublishDate
            });
            delete req.excludePublishDate;
        }

        if (!_.isEmpty(excluderlist)) {
            req.excluderlist = excluderlist;
        }
    };

    var injectFilter = function(req) {
        var filterList = [];

        if (_.has(req, 'produceZone')) {
            filterList.push({
                key: 'Producezone',
                value: req.produceZone
            });
            delete req.produceZone;
        }

        if (_.has(req, 'publishDate')) {
            filterList.push({
                key: 'Publishdate',
                value: req.publishDate
            });
            delete req.publishDate;
        }

        if (_.has(req, 'genre')) {
            filterList.push({
                key: 'Genre',
                value: req.genre
            });
            delete req.genre;
        }

        if (!_.isEmpty(filterList)) {
            req.filterlist = filterList;
        }
    };

    var injectOrder = function(req) {
        if (_.has(req, 'orderBy')) {
            switch (req.orderBy) {
                case 'latest':
                    req.orderType = 0;
                    break;
                case 'rating':
                    req.orderType = 1;
                    break;
                case 'popular':
                    req.orderType = 2;
                    break;
                default:
                    req.orderType = 0;
                    break;
            }
            delete req.orderBy;
        }
    };

    var injectProperties = function(req, type) {
        switch (type) {
            case 'list':
                req.properties = [{
                    name: 'vod',
                    include: config.vodListProperties || VOD_LIST_PROPERTIES
                }];
                break;
            case 'detail':
                if (config.vodDetailProperties) {
                    req.properties = [{
                        name: 'vod',
                        include: config.vodDetailProperties
                    }];
                }
                break;
            default:
                break;
        }
    };

    var vodList = [];

    var filterCachedIds = function(idList) {
        var cachedIds = _.map(vodList, function(vod) {
            return vod.id;
        });

        return _.difference(idList, cachedIds);
    };

    var getCachedVods = function(ids) {
        return _.map(ids, function(id) {
            return _.find(vodList, function(vod) {
                return vod.id === id;
            });
        });
    };

    var vodCategories = [];
    var getCachedVodCategories = function(id) {
        return _.find(vodCategories, function(fatherVodCategory) {
            return fatherVodCategory.id === id;
        });
    };

    var dynamicRecmFilmList = {};
    var dynamicRecmFilmListReq = {};

    /**
     * Vod related business logic.
     * @class eBase.app.vodApp
     * @singleton
     */
    var self = app.vodApp = function() {
        return {
            /**
             * Get Vods under certain category.
             * @param {Object} req
             * @param {String} [req.categoryId] Category ID.
             * @param {String} [req.produceZone] Filtering by area. This parameter can be set to multiple values separated by (,).
             * @param {String} [req.publishDate] yyyy Release time that is separated with commas (,).
             * @param {String} [req.genre] Genres that is separated with commas (,).
             * @param {String} [req.orderBy] Sorting mode
             *
             * - latest
             * - rating
             * - popular
             * @param {Number} [req.count=-1] Number of returned records.
             * @param {Number} [req.offset=0] Record index offset, which starts from 0.
             * @param {String} properties
             *
             * @return {eBase.core.Deferred}
             * @return return.list List of {eBase.data.Vod}
             */
            queryCategory: function(req) {
                var retDfd = new Deferred();
                var _tmpVodCategories = getCachedVodCategories(req.categoryId);
                if (_tmpVodCategories) {
                    retDfd.resolve({
                        list: _tmpVodCategories.list
                    });
                } else {
                    utils.paginate(req);
                    utils.rename(req, 'categoryId', 'categoryid');

                    req.categoryid = req.categoryid || -1;
                    req.type = req.type || ['VOD', 'AUDIO_VOD', 'VIDEO_VOD', 'CHANNEL', 'AUDIO_CHANNEL', 'VIDEO_CHANNEL', 'MIX', 'VAS', 'PROGRAM'].join(';');

                    return utils.jsonAjax({
                        url: '/EPG/JSON/CategoryList',
                        data: JSON.stringify(req)
                    }).done(function(resp) {
                        utils.rename(resp, 'categorylist', 'list');
                        var _tmpVodCategoryObj = {
                            id: req.categoryid,
                            list: resp.list
                        };
                        vodCategories.push(_tmpVodCategoryObj);
                    });
                }
                return retDfd;
            },
            queryDynamicRecmFilm: function(req, type) {
                var retDfd = new Deferred();
                dynamicRecmFilmListReq.properties = dynamicRecmFilmListReq.properties && dynamicRecmFilmListReq.properties[0].include;
                if (_.isEqual(req, dynamicRecmFilmListReq) && dynamicRecmFilmList.list && dynamicRecmFilmList.list.length > 0) {
                    retDfd.resolve(dynamicRecmFilmList);
                } else {
                    core.iptv.queryDynamicRecmFilm(req).done(function(resp) {
                        dynamicRecmFilmListReq = req;
                        dynamicRecmFilmList = resp;
                        retDfd.resolve(resp);
                    }).fail(retDfd.reject);
                }

                return retDfd;
            },
            queryVod: function(req) {
                return self.queryVodByCategory(req);
            },
            queryVodByCategory: function(req) {
                var retDfd = new Deferred();

                injectExcluder(req);
                injectFilter(req);
                injectOrder(req);

                core.iptv.queryVod(_.extend(req, {
                    properties: VOD_BASE_PROPERTIES
                })).done(function(resp) {
                    var countTotal = resp.counttotal;

                    self.queryVodByIds({
                        ids: _.pluck(resp.list, 'id').join(',')
                    }).then(function(resp) {
                        resp.counttotal = countTotal;
                        retDfd.resolve(resp);
                    }, retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            queryVodByCategoryForHome: function(req) {
                var retDfd = new Deferred();
                // core.iptv.queryVod(_.extend(req, {
                //     properties: ['id', 'type', 'picture', 'mediafiles','name'].join(','),
                // })).done(function(resp) {
                //     var countTotal = resp.counttotal;
                //     retDfd.resolve(resp);
                // }).fail(retDfd.reject);

                core.iptv.queryRecommendVod(_.extend(req, {
                    properties: ['id', 'type', 'picture', 'mediafiles', 'name'].join(','),
                })).done(function(resp) {
                    var countTotal = resp.counttotal;
                    retDfd.resolve(resp);
                }).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * Query vod with id list.
             * @param {Object} req
             * @param {String} req.ids Ids separated by ','
             *
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.data.Vod}
             */
            queryVodByIds: function(req) {
                var retDfd = new Deferred();

                if (req.ids === '') {
                    retDfd.resolve({
                        list: []
                    });
                } else {
                    var idList = req.ids.split(',');
                    var reqIdList = filterCachedIds(idList);

                    if (reqIdList.length > 0) {
                        if (!req.properties) {
                            req = {
                                vod: reqIdList.join(','),
                                filterType: 1
                            };
                            injectProperties(req, 'list');
                        } else {
                            var properties = req.properties;
                            req = {
                                properties: properties,
                                vod: reqIdList.join(','),
                                filterType: 1
                            };
                        }

                        //console.error("===Fangshenglong=== : Starting query vod detail infomation via (/EPG/JSON/ContentDetail) interface. Request is (" + JSON.stringify(req) + ").");
                        core.iptv.queryDetail(req).done(function(resp) {
                            utils.rename(resp, 'vodlist', 'list');
                            //console.error("===Fangshenglong=== : Query vod detail infomation via (/EPG/JSON/ContentDetail) interface success. Request is (" + JSON.stringify(req) + ").");
                            // Merge in cache
                            vodList = _.union(vodList, resp.list);
                            resp.list = getCachedVods(idList);

                            retDfd.resolve(resp);
                        }).fail(retDfd.reject);
                    } else {
                        retDfd.resolve({
                            list: getCachedVods(idList)
                        });
                    }
                }

                return retDfd;
            },
            /**
             * Get vod detail info.
             * @param {Object} req
             * @param {String} req.id
             *
             * @return {eBase.core.Deferred}
             * @return resp.vod {eBase.data.Vod} return.resp
             */
            getVodDetail: function(req) {
                var retDfd = new Deferred();

                utils.rename(req, 'id', 'vod');
                injectProperties(req, 'detail');

                core.iptv.queryDetail(req).done(function(resp) {
                    var vod = resp.vodlist[0];
                    if (vod) {
                        for (var i = 0; i < vodList.length; i++) {
                            if (vodList[i].id === vod.id) {
                                vodList[i] = vod;
                                break;
                            }
                        }
                    }
                    retDfd.resolve(vod);
                }).fail(retDfd.reject);

                return retDfd;
            },

            /**
             * reset vodlist
             */
            resetVodlist: function(req) {
                var retDfd = new Deferred();

                vodList = [];

                return retDfd;
            },
            /**
             * A client uses this interface to obtain series episode lists.
             * When a subscriber views the details of a series, the client can invoke this interface to obtain the episode list
             * of this series for the subscriber to select an episode to play.
             * @param {Object} req
             * @param {String} req.id Series ID.
             * @param {String} [req.offset] Record index offset, which starts from 0.
             * @param {String} [req.count] Number of returned records. -1 means All.
             *
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.core.Vod}
             */
            querySitcom: function(req) {
                var retDfd = new Deferred();

                utils.rename(req, 'id', 'vodId');

                req.properties = [{
                    name: 'vod',
                    properties: VOD_BASE_PROPERTIES
                }];

                // core.iptv.querySitcom(req).done(function(resp) {
                //     self.queryVodByIds({
                //         ids: _.pluck(resp.list, 'id').join(',')
                //     }).then(retDfd.resolve, retDfd.reject);
                // }).fail(retDfd.reject);
                core.iptv.querySitcom(req).done(retDfd.resolve).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * Obtain the list of recommended VOD programs.
             *
             * @return {eBase.core.Deferred}
             * @return {Array} return.list List of {eBase.core.Vod}
             */
            queryRecommend: function(req) {
                var retDfd = new Deferred();

                req = req || {
                    action: 6
                };
                req.properties = [{
                    name: 'vod',
                    properties: VOD_BASE_PROPERTIES
                }];

                core.iptv.queryRecommendVod(req).done(function(resp) {
                    self.queryVodByIds({
                        ids: _.pluck(resp.list, 'id').join(',')
                    }).then(retDfd.resolve, retDfd.reject);
                }).fail(retDfd.reject);

                return retDfd;
            },
            /**
             * Query content providers of Vod.
             *
             * @param {Object} req
             * @param {String} req.id Vod ID
             *
             * @return {eBase.core.Deferred}
             * @return {eBase.data.ContentProvider[]} return.list
             */
            queryContentProvider: function(req) {
                var retDfd = new Deferred();

                self.getVodDetail({
                    id: req.id
                }).done(function(resp) {
                    var medias = resp.mediafiles;
                    var dfds = _.map(medias, function(media) {
                        return core.iptv.getPlayUrl({
                            contentId: req.id,
                            mediaId: media.id,
                            playType: 1
                        });
                    });

                    Deferred.when.apply(Deferred, dfds).done(function() {
                        retDfd.resolve({
                            list: _.map(arguments, function(resp, index) {
                                return {
                                    name: medias[index].spId4VIP,
                                    playUrl: resp.url
                                };
                            })
                        }).fail(retDfd.reject);
                    }).fail(retDfd.reject);
                });

                return retDfd;
            }
        };
    }();

    return core;
}(eBase));
/**
 * AdPolicy
 * @class eBase.data.AdPolicy
 */
/**
 * AD position id.
 * @property poId
 * @type {String}
 */
/**
 * AD type.
 *
 * - 0锛歋tartup AD
 * - 1锛欵PG AD
 * - 2锛歏ideo insert AD
 * - 3锛歏ideo pause AD
 *
 * @property placementType
 * @type {String}
 */
/**
 * Video insert AD type.
 * @property insertType
 * @type {String}
 */
/**
 * AD effective time.Format: yyyyMMddHHmmss.
 * @property startTime
 * @type {String}
 */
/**
 * AD disabled time.Format: yyyyMMddHHmmss.
 * @property endTime
 * @type {String}
 */
/**
 * When play the AD,only for video insert AD.Format: hh:mm:ss.
 * @property playTime
 * @type {String}
 */
/**
 * AD resource type.
 *
 * - 1锛歏OD
 * - 2锛歅icture
 * - 3锛欸OOGLE CODE
 *
 * @property mediaType
 * @type {String}
 */
/**
 * AD resource contentid or URL.
 * @property mediaContent
 * @type {String}
 */
/**
 * Picture URL,only for picutre AD.
 * @property adLink
 * @type {String}
 */
/**
 * AssociatedKeyword
 * @class eBase.data.AssociatedKeyword
 */
/**
 * Bill
 * @class eBase.data.Bill
 */
/**
 * Bill name.
 *
 * - For a monthly package product, the value is the product name.
 * - For a times-based product, the value is the content name.
 * @property name
 * @type {String}
 */
/**
 * Fee in a bill. The unit is the currency unit configured in the UMS when a product is released.
 * @property fee
 * @type {String}
 */
/**
 * Time when a bill is generated.
 * @property time
 * @type {String}
 */
/**
 * Content type.
 * For details about the value options, see section 18.3 "Content Type Enumeration Values."
 *
 * - If a bundle is subscribed to, the value is MIX.
 * - If a category is subscribed to, the value is SUBJECT.
 * - If a content item is subscribed to, the value is the specific content type.
 * @property contenttype
 * @type {String}
 */
/**
 * Number of episodes of a series.
 * This parameter is reserved.
 * @property volume
 * @type {Number}
 */
/**
 * Product type.
 * The options are as follows:
 *
 * - 0: duration-based product
 * - 1: times-based product
 * @property producttype
 * @type {String}
 */
/**
 * Fee type.
 * The options are as follows:
 *
 * - 1: cash
 * - 2: bonus points
 * - 10: prepaid
 * @property feetype
 * @type {String}
 */
/**
 * ID of a profile who subscribes to a product.
 * @property profileId
 * @type {String}
 */
/**
 * Operation result. The options are as follows:
 *
 * - 0: success
 * - 1: failure
 * @property operResult
 * @type {Number}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Bookmark
     * @class eBase.data.Bookmark
     */
    data.Bookmark = function(json) {
        return json;
    };
    /**
     * Content ID.
     *     If the bookmark is for PVR content and a program is recorded, the type of the program is returned.
     *     If a channel is recorded, the ID of the channel is returned.
     * @property id
     * @type {String}
     */
    /**
     * Content type. The options are as follows:
飦?  *     SUBJECT: category
飦?  *     VOD: on-demand content
飦?  *     AUDIO_VOD: audio on demand
飦?  *     VIDEO_VOD: video on demand
飦?  *     TELEPLAY_VOD: episode of a series
飦?  *     CREDIT_VOD: ad content
飦?  *     CHANNEL: channel
飦?  *     AUDIO_CHANNEL: audio channel
飦?  *     VIDEO_CHANNEL: video channel
飦?  *     WEB_CHANNEL: web channel
飦?  *     MIX: combination of several content types
飦?  *     VAS: value-added service
飦?  *     PROGRAM: live TV program
飦?  *     TVOD: Catch-up TV program
飦?  *     If the bookmark is for PVR content and a program is recorded, the type of the program is returned.
飦?  *     If a channel is recorded, the type of the channel is returned.
     * @property type
     * @type {String}
     */
    /**
     * Content name. If the bookmark is for PVR content, the parameter indicates the PVR task name.
     * @property name
     * @type {String}
     */
    /**
     * Series name. The parameter is invalid if the content is a movie.
     * @property seriesname
     * @type {String}
     */
    /**
     * Content description.
     * @property introduce
     * @type {String}
     */
    /**
     * Bookmark time.
飦?  *     If bookMarkType is set to 0, 1, 2, or 3, the data type must be int. The unit is second.
飦?  *     If bookMarkType is set to 4, the data type must be string. The format is yyyyMMddHHmmss.
     * @property time
     * @type {String}
     */
    /**
     * Content rating. A larger value indicates a higher rating. The value varies depending on the content rating systems in different countries.
     * @property ratingid
     * @type {String}
     */
    /**
     * Path for storing a poster of the bookmark.
     *     Use the absolute path (HTTP URL), for example, http://12.24.11.120:8080/EPG/jsp/image/12.jpg.
     *     Separate multiple values with a comma (,). The properties of the object are as follows:
     *     deflate:     Path for storing thumbnails.
     *     poster:      Path for storing posters.
     *     still:       Path for storing stills.
     *     icon:        Path for storing icons.
     *     title:       Path for storing titles pictures.
     *     ad:          Path for storing ad pictures.
     *     draft:       Path for storing draft pictures.
     *     background:  Path for storing background pictures.
     *     channelpic:  Path for storing channel pictures.
     *     blackwhite:  Path for storing channel black and white pictures.
     *     channame:    Path for storing channel name pictures.
     *     other:       Path for storing other pictures.
     * @property picture
     * @type {Object}
     */
    /**
     * Series ID. The value is -1 if the content is a movie.
     * @property fathervodid
     * @type {String}
     */
    /**
     * Bookmark type. The options are as follows:
飦?  *     0: VOD bookmark
飦?  *     1: cPVR bookmark
飦?  *     2: nPVR bookmark
飦?  *     3: Catch-up TV bookmark
飦?  *     4: channel bookmark
     * @property bookMarkType
     * @type {String}
     */
    /**
     * PVR task ID. The parameter is valid if the bookmark is a PVR bookmark.
     * @property pvrId
     * @type {String}
     */
    /**
     * Bookmark update time. Format is yyyyMMddHHmmss
     * @property updatetime
     * @type {String}
     */
    return core;
}(eBase));
/**
 * CA device info.
 * @class eBase.data.CADeviceInfo
 */
/**
 * CA device type
 * Separate multiple values with a comma (,).
 *
 * 1: DTV hard card STB
 * 2: DTV soft cardSTB
 * 3: IPTV STB
 * 4: OTT STB
 * 5: HLS OTT client
 * 6: Playready OTT client
 * @property caDeviceType
 * @type {String}
 */
/**
 * CA device ID
 * For the first login at Nagra OTT client, the value is not generated. The value is left blank.
 * @property caDeviceId
 * @type {String}
 */
/**
 * CDR
 * @class eBase.data.CDR
 */
/**
 * Content name.
 * @property name
 * @type {String}
 */
/**
 * Playback time.
 * @property time
 * @type {String}
 */
/**
 * Fee charged for watching programs.
 * @property fee
 * @type {String}
 */
/**
 * Content type.The options are as follows:
 *
 * - VIDEO_VOD
 * - AUDIO_VOD
 * - VIDEO_CHANNEL
 * - AUDIO_CHANNEL
 * - WEB_CHANNEL
 * - VAS
 * - SUBJECT
 * - PROGRAM
 *
 * @property contenttype
 * @type {String}
 */
/**
 * CDR type.The options are as follows:
 *
 * - 0: CDR recording consumption information
 * - 1: CDR recording unsubscription from services
 *
 * @property type
 * @type {String}
 */
/**
 * CastInfo of system.
 * @class eBase.data.CastInfo
 */
/**
 * castId of CastInfo.
 * @property castId
 * @type {String}
 */
/**
 * name of CastInfo.
 * @property name
 * @type {String}
 */
/**
 * firstName of CastInfo.
 * @property firstName
 * @type {Object}
 */
/**
 * sex of CastInfo.
 * @property sex
 * @type {String}
 */
/**
 * birthday of CastInfo.
 * @property birthday
 * @type {String}
 */
/**
 * hometown of CastInfo.
 * @property hometown
 * @type {String}
 */
/**
 * picture of CastInfo.
 * @property picture
 * @type {Object}
 */
/**
 * Category
 * @class eBase.data.Category
 */
/**
 * Category id.
 * @property id
 * @type {String}
 */
/**
 * Category name.
 * @property name
 * @type {String}
 */
/**
 * Category type.
 * VOD: on-demand content
 * AUDIO_VOD: audio on demand
 * VIDEO_VOD: video on demand
 * CREDIT_VOD: ad content
 * CHANNEL: channel
 * AUDIO_CHANNEL: audio channel
 * VIDEO_CHANNEL: video channel
 * WEB_CHANNEL: web channel
 * MIX: combination of several content types
 * VAS: value-added service
 * PROGRAM: program
 * @property type
 * @type {String}
 */
/**
 * Category introduce.
 * @property introduce
 * @type {String}
 */
/**
 * Category ratingid.
 * @property ratingid
 * @type {String}
 */
/**
 * Category issubscribed.
 * @property issubscribed
 * @type {String}
 */
/**
 * Category foreignsn.
 * @property foreignsn
 * @type {String}
 */
/**
 * Category parentCategoryId.
 * @property parentCategoryId
 * @type {String}
 */
/**
 * No subcategories or content.
 * @property haschildren Indicates whether a category contains subcategories or content.
 *
 * - -1: no subcategories or content
 * - 0: content contained
 * - 1: subcategories contained
 * @type {String}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Channel
     * @class eBase.data.Channel
     */
    data.Channel = function(json) {
        return json;
    };
    /**
     * Content ID.
     * @property id
     * @type {String}
     */
    /**
     * Content name.
     * @property name
     * @type {String}
     */
    /**
     * Content type.
     * - SUBJECT: category
     * - VOD: on-demand content
     * - AUDIO_VOD: audio on demand
     * - VIDEO_VOD: video on demand
     * - TELEPLAY_VOD: episode of a series
     * - CREDIT_VOD: ad content
     * - CHANNEL: channel
     * - AUDIO_CHANNEL: audio channel
     * - VIDEO_CHANNEL: video channel
     * - WEB_CHANNEL: web channel
     * - MIX: combination of several content types
     * - VAS: value-added service
     * - PROGRAM: live TV program
     * - TVOD: Catch-up TV program
     * @property type
     * @type {String}
     */
    /**
     * Content description.
     * @property introduce
     * @type {String}
     */
    /**
     * Indicates whether the preview is supported. The options are as follows, 0: no, 1: yes
     * @property previewenable
     * @type {String}
     */
    /**
     * Preview duration. If preview is not supported, the value is 0. Unit: second
     * @property previewlength
     * @type {String}
     */
    /**
     * Number of previews being performed. If preview is not supported, the value is 0.
     * @property previewcount
     * @type {String}
     */
    /**
     * IP address of multicast streams source.
     * @property multicastsourceip
     * @type {String}
     */
    /**
     * Indicates whether PIP channels are contained. The options are as follows, 0: no, 1: yes
     * @property haspip
     * @type {String}
     */
    /**
     * Multicast IP address of the PIP channel.
     * @property pipmulticastip
     * @type {String}
     */
    /**
     * Multicast port of the PIP channel.
     * @property pipmulticastport
     * @type {String}
     */
    /**
     * PIP multicast source address.
     * @property pipmulticastsourceip
     * @type {String}
     */
    /**
     * Indicates whether the PIP channel supports Fast Channel Change (FCC) and Retransmission (RET).
     *     The options are as follows:
飦?  *     0: does not support FCC and RET
飦?  *     1: supports FCC and RET
飦?  *     2: supports FCC only
飦?  *     3: supports RET only
     * @property pipfccenable
     * @type {String}
     */
    /**
     * Channel status. This parameter determines whether a channel can be played on a terminal.
     *     The options are as follows:
飦?  *     1: authorized and unlocked
飦?  *     2: authorized and locked
飦?  *     3: unauthorized
     *     If the parameter is null, the channel status is unknown.
     * @property status
     * @type {String}
     */
    /**
     * Channel number.
     * @property channo
     * @type {String}
     */
    /**
     * Indicates whether a channel supports FCC and RET.
     *     The options are as follows:
飦?  *     0: does not support FCC and RET
飦?  *     1: supports FCC and RET
飦?  *     2: supports FCC only
飦?  *     3: supports RET only
     * @property fccenable
     * @type {String}
     */
    /**
     * Indicates whether a channel is encrypted. The options are as follows, 0: no, 1: yes
     * @property encrypt
     * @type {String}
     */
    /**
     * Channel bandwidth. Unit: kbit/s
     * @property bitrate
     * @type {String}
     */
    /**
     * Play URL. For the IPTV multicast channels and OTT channels, the format is "live TV URL|network TSTV URL".
     *     For the IPTV unicast channels, the URL contains only live TV URL.
     *     In the URL: Live TV URL for a multicast channel starts with igmp. For example, igmp://230.2.2.2:22222
飦?  *     Live TV URL for an IPTV unicast channel starts with rtsp. For example, rtsp://125.88.67.43/88888888/16/20100619/268435541/4.3ts?icpid=&amp;accounttype=1&amp;limitflux=-1&amp;limitdur=-1&amp;accountinfo=:20100621134646,18925160006,,20100621134646,00000100000000010000000000000058,,-1,0,3,-1,,1,,,,1,END&amp;icpid=88888888
飦?  *     In the OTT domain, the live TV URL starts with HTTP.
     *     For example, http://125.88.67.43/88888888/16/20110422/12321434/a.m3u8?icpid=&amp;accounttype=1&amp;limitflux=-1&amp;limitdur=-1&amp;accountinfo=:20100621134646,18925160006,,20100621134646,00000100000000010000000000000058,,-1,0,3,-1,,1,,,,1,END&amp;icpid=88888888
     *     This parameter is not returned if the channel is not subscribed to.
     * @property playurl
     * @type {String}
     */
    /**
     * Definition flag. The options are as follows, 1: high definition, 0: standard definition
     * @property definition
     * @type {String}
     */
    /**
     * Poster picture.
     *     Use the absolute path (HTTP URL), for example, http://12.24.11.120:8080/EPG/jsp/image/12.jpg.
     *     Separate multiple values with a comma (,). The properties of the object are as follows:
     *     deflate:     Path for storing thumbnails.
     *     poster:      Path for storing posters.
     *     still:       Path for storing stills.
     *     icon:        Path for storing icons.
     *     title:       Path for storing titles pictures.
     *     ad:          Path for storing ad pictures.
     *     draft:       Path for storing draft pictures.
     *     background:  Path for storing background pictures.
     *     channelpic:  Path for storing channel pictures.
     *     blackwhite:  Path for storing channel black and white pictures.
     *     channame:    Path for storing channel name pictures.
     *     other:       Path for storing other pictures.
     * @property picture
     * @type {Object}
     */
    /**
     * Indicates whether a channel supports cPVR. The options are as follows, 0: no, 1: yes
     * @property iscpvr
     * @type {String}
     */
    /**
     * Indicates whether a channel supports time shift. The options are as follows, 0: no, 1: yes
     * @property ispltv
     * @type {String}
     */
    /**
     * Time shift duration. If the network TSTV function is not supported, the value is 0. Unit: second
     * @property pltvlength
     * @type {String}
     */
    /**
     * Indicates whether a channel supports Catch-up TV. The options are as follows, 0: no, 1: yes
     * @property istvod
     * @type {String}
     */
    /**
     * Indicates whether a channel supports local time shift. The options are as follows, 0: no, 1: yes
     * @property islocaltimeshift
     * @type {String}
     */
    /**
     * Channel logo. The properties of the object are as follows:
     *     url:      Channel logo URL. For example, http://10.12.2.25:8080/EPG/jsp/image/a.jpg
     *     display:  Channel logo display duration. The format is Display duration,Hide duration. Unit: second
     *     location: Channel logo location, in the format X,Y.
     *     size:     Channel logo size, in the format Height,Width. Unit: pixel
     * @property logo
     * @type {Object}
     */
    /**
     * Indicates whether a channel is added to favorites. The options are as follows, 1: yes, 0: no
     * @property isfavorited
     * @type {String}
     */
    /**
     * Content rating. A larger value indicates a higher rating. The value varies depending on the content rating systems in different countries.
     * @property ratingid
     * @type {String}
     */
    /**
     * Indicates whether a channel has been subscribed to. The options are as follows, 0: no, 1: yes
     * @property issubscribed
     * @type {String}
     */
    /**
     * Indicates whether a channel supports PPV. The options are as follows, 0: no, 1: yes
     * @property isppv
     * @type {String}
     */
    /**
     * Indicates whether a subscriber has subscribed to a cPVR product. The options are as follows, 0: no, 1: yes
     * @property cpvrsubscribed
     * @type {String}
     */
    /**
     * Channel recording duration. If the recording function is not supported. The parameter value is 0. Unit: second
     * @property recordlength
     * @type {String}
     */
    /**
     * Near Video on Demand (NVOD) channel type.
     *     The options are as follows:
飦?  *     0: non-NVOD channel
飦?  *     1: NVOD channel
飦?  *     2: BarkerChannel
飦?  *     3: PushVOD
     * @property slstype
     * @type {String}
     */
    /**
     * Indicates whether a subscriber has subscribed to a Time Shift TV (TSTV) product. The options are as follows, 0: no, 1: yes
     * @property pltvsubscribed
     * @type {String}
     */
    /**
     * Indicates whether a subscriber has subscribed to a local TSTV product. The options are as follows, 0: no, 1: yes
     * @property localtimeshiftsubscribed
     * @type {String}
     */
    /**
     * Indicates whether a subscriber has subscribed to a Catch-up TV product. The options are as follows, 0: no, 1: yes
     * @property tvodsubscribed
     * @type {String}
     */
    /**
     * PIP streams bit rate.
     * @property pipbitrate
     * @type {String}
     */
    /**
     * Indicates whether a channel supports HDCP. The options are as follows, 0: no, 1: yes
     * @property HDCPEnable
     * @type {String}
     */
    /**
     * Languages supported by a channel.
     *     The values are codes for the representation of language names specified in the ISO 639-1 standard.
     *     Separate multiple values with a comma (,).
     * @property languages
     * @type {String}
     */
    /**
     * Macrovision attribute of a channel.
     *     The options are as follows:
飦?  *     0: Macrovision off
飦?  *     1: AGC
飦?  *     2: AGC + 2-stripe
飦?  *     3: AGC + 4-stripe
     *     The default value is 0.
     * @property macrovision
     * @type {String}
     */
    /**
     * Total rating times.
     * @property statictimes
     * @type {String}
     */
    /**
     * Average rating of rating.
     * @property averagescore
     * @type {String}
     */
    /**
     * Indicates whether a channel supports nPVR. The options are as follows, 0: no, 1: yes
     * @property isnpvr
     * @type {String}
     */
    /**
     * nPVR recording duration. If nPVR is not supported, the value is 0. Unit: second
     * @property npvrlength
     * @type {String}
     */
    /**
     * Indicates whether a subscriber has subscribed to an nPVR product. The options are as follows, 0: no, 1: yes
     * @property npvrsubscribed
     * @type {String}
     */
    /**
     * Information about a DVB channel. The parameter is contained only for DVB channels.
     *     The properties of the object are as follows:
     *     serviceId:         Service ID of a DVB channel. The value is a string of hexadecimal characters, for example, 0x0002.
     *     bearerType:        Bearer type. The options are as follows, 1: DVB-S, 2: DVB-C
     *     frequency:         Frequency of a DVB channel. Unit: MHz.
     *     symborate:         Symbol rate of a DVB channel. Unit: MS/s.
     *     transportStreamId: Transmission stream ID of a DVB channel. The value is a string of hexadecimal characters, for example, 0x0003.
     *     originalNetworkId: Original transmission network ID of a DVB channel. The value is a string of hexadecimal characters, for example, 0x0004.
     *     polarity:          Polarization of a DVB channel. The options are as follows, 1: Automatic, 2: Vertical, 3: Horizontal
     *     satelliteName:     Satellite name.
     *     satellitePosition: Satellite position.
     *         If the satellite is located west of the Greenwich, the value is a negative number.
     *         If the satellite is located east of the Greenwich, the value is a positive number.
     *         For example, -160 indicates 160 degrees west longitude, and 150 indicates 150 degrees east longitude.
     *     demodulation:      Demodulating frequency (DVB-C parameter). The options are as follows, 0x00: not defined, 0x01: 16 QAM, 0x02: 32 QAM, 0x03: 64 QAM, 0x04: 128 QAM, 0x05: 256 QAM, 0x06: to 0xFF reserved for future use
     * @property dvbInfo
     * @type {Object}
     */
    /**
     * Media ID.
     * @property mediaid
     * @type {String}
     */
    /**
     * Price of a channel. The parameter is contained when the channel is priced. The unit is the currency unit configured in the UMS when a product is released.
     * @property price
     * @type {String}
     */
    /**
     * Type of terminals supported by live TV media files.
     * For IPTV solution, the options are as follows, 1: STB, 2: PC, 3: LG-TV
     * For OTT solution, the options are as follows, 1: Browser, 2: iPhone/iPad, 3: Android
     * Separate multiple values with a comma (,).
     * @property supportTerminal
     * @type {String}
     */
    /**
     * Content format of a live TV media file.
     *     The options are as follows, 1: TS, 2: HLS, 3: HSS, The default value is 1.
     * @property fileFormat
     * @type {String}
     */
    /**
     * Content CGMS-A attribute.
     *     The options are as follows:
飦?  *     0: Copy Freely
飦?  *     1: Copy No More
飦?  *     2: Copy Once
飦?  *     3: CopyNever
     * @property CGMSA
     * @type {String}
     */
    /**
     * Inaccessible network. The options are as follows, 1: WLAN, 2: Cellular, Separate multiple values with a comma (,).
     * @property blockedNetwork
     * @type {String}
     */
    /**
     * Indicates whether the playback is allowed.
     *     The options are as follows, 0: no, 1: yes
     *     If the device group of a subscriber overlaps with the device group of a channel, the content playback is allowed. Otherwise, the content playback is disallowed.
     * @property playEnable
     * @type {String}
     */
    /**
     * Device group corresponding to VOD content.
     * The properties of the object in the array are as follows:
     *     groupId:    Device group ID.
     *     groupName:  Device group name.
     *     groupType:  Device group type. The options are as follows, 0: CP group, 1: SP group
     * @property deviceGroups
     * @type {Array}
     */
    /**
     * Restrictions of a product. The restrictions vary depending on products authorized by different services, because channels support the service-based authorization.
     *     The value of key of the NamedParameter type is the attribute name indicating whether the specific service of a channel is subscribed to. The value of value is the restriction of a product.
     *     The options of the key parameter are as follows: issubscribed, pltvsubscribed, localtimeshiftsubscribed, tvodsubscribed, cpvrsubscribed, npvrsubscribed
     *     The preceding information indicates the attribute names indicating whether the specific service of a channel is subscribed to. Only when the values of the preceding attributes are 1, indicating that the service is subscribed to, the value parameter of the NamedParameter type is valid. Use ampersands (&) to separate multiple restrictions. The format is as follows:
     *     &lt;attribute name 1&gt;=&lt;attribute value 1&gt;&amp;&lt;attribute name 2&gt;=&lt;attribute value 2&gt;......
     *     Use commas (,) to separate multiple attribute values.
     *     The network access mode and terminal type are limited.
     *     The attribute for the network access mode is Net. The options are as follows, 1: WLAN, 2: Cellular
     *     For example, if a product cannot be used in the WLAN network, the value of restriction is Net=1.
     *     The attribute for the terminal type is deviceModels, which can be set to multiple values.
     * @property restrictions
     * @type {Array}
     */
    /**
     * Applicable conditions of a product. The applicable conditions vary depending on products authorized by different services, because channels support the service-based authorization.
     *     The value of key of the NamedParameter type is the attribute name indicating whether the specific service of a channel is subscribed to. The value of value is the applicable condition of a product.
     *     The options of the key parameter are as follows: issubscribed, pltvsubscribed, localtimeshiftsubscribed, tvodsubscribed, cpvrsubscribed, npvrsubscribed
     *     The preceding information indicates the attribute names indicating whether the specific service of a channel is subscribed to. Only when the values of the preceding attributes are 1, indicating that the service is subscribed to, the value parameter of the NamedParameter type is valid. Use ampersands (&) to separate multiple applicable conditions. The format is as follows:
     *     &lt;attribute name 1&gt;=&lt;attribute value 1&gt;&amp;&&lt;attribute name 2&gt;=&lt;attribute value 2&gt;......
     *     Use commas (,) to separate multiple attribute values.
     *     The network access mode and terminal type are limited.
     *     The attribute for the network access mode is Net. The options are as follows, 1: WLAN, 2: Cellular
     *     The attribute for the terminal type is deviceModels, which can be set to multiple values.
     *     The restriction condition and applicable condition will not be set at the same time.
     * @property inConditions
     * @type {Array}
     */
    /**
     * Channel type ID.
     * @property genreIds
     * @type {Array}
     */
    /**
     * ID of a physical media used during Catch-up TV program playback.
     * @property catchupMediaId
     * @type {String}
     */
    /**
     * Video encoding format. The options are H.263 and H.264.
     * @property videoCodec
     * @type {String}
     */
    /**
     * Whether to a channel supports the FEC function. The options are as follows, 0: No, 1: Yes
     * @property FECEnable
     * @type {String}
     */
    /**
     * Whether to a PIP channel supports the FEC function. The options are as follows, 0: No, 1: Yes
     * @property pipFECEnable
     * @type {String}
     */
    return core;
}(eBase));
/**
 * Configuration of system.
 * @class eBase.data.Configuration
 */
/**
 * Type of Configuration.
 *
 * - 0: Terminal configuration
 * - 1: Template configuration
 * - 2: EPG configuration
 * @property cfgType
 * @type {String}
 */
/**
 * List of {eBase.data.CustomConfig}
 * @property extensionInfo
 * @type {Array}
 */
/**
 * Content
 * @class eBase.data.Content
 */
/**
 * Content ID.
 * @property id
 * @type {String}
 */
/**
 * Content type.
 *
 * SUBJECT: category
 * VOD: on-demand content
 * AUDIO_VOD: audio on demand
 * VIDEO_VOD: video on demand
 * TELEPLAY_VOD: episode of a series
 * CREDIT_VOD: ad content
 * CHANNEL: channel
 * AUDIO_CHANNEL: audio channel
 * VIDEO_CHANNEL: video channel
 * WEB_CHANNEL: web channel
 * MIX: combination of several content types
 * VAS: value-added service
 * PROGRAM: live TV program
 * TVOD: Catch-up TV program
 * @property type
 * @type {String}
 */
/**
 * Info of content provider.
 * @class eBase.data.ContentProvider
 */
/**
 * Name of content provider.
 * @property name
 * @type {String}
 */
/**
 * Play url of media.
 * @property playUrl
 * @type {String}
 */
/**
 * CreditBalance
 * @class eBase.data.CreditBalance
 */
/** Credit type
 *
 * - 1: subscriber's consumption limit in a fixed bill cycle
 * - 2: subscriber's experience times limit in a flexible bill cycle
 * - 3: subscriber's consumption limit in a flexible bill cycle
 *
 * @property creditType
 * @type {String}
 */
/**
 * The value is returned only when the query succeeds.
 *
 * - This parameter indicates the total credit limit when creditType is set to 1 or 3.
 * - This parameter indicates the total number of limited times when creditType is set to 2.
 *
 * @property creditTotal
 * @type {String}
 */
/**
 * The value is returned only when the query succeeds.
 *
 * - This parameter indicates the remaining credit limit when creditType is set to 1 or 3.
 * - This parameter indicates the remaining number of limited times when creditType is set to 2.
 *
 * @property creditBalance
 * @type {String}
 */
/**
 * Start time of the credit bill cycle. The format is YYYYMMDDHHMMSS.
 * @property cycleStartTime
 * @type {String}
 */
/**
 * End time of the credit bill cycle. The format is YYYYMMDDHHMMSS.
 * @property cycleEndTime
 * @type {String}
 */
/**
 * The currency rate and display unit
 * @class eBase.data.CurrencyInfo
 */
/**
 * Currency rate between the minimum currency unit and display unit.The default value is 100.
 * @property currencyRate
 * @type {String}
 */
/**
 * Code of the currency type, defined in the ISO 4217 standard.The options are as follows:<br>
 *
 * SGD: Singapore dollar<br>
 * THB: Thai baht<br>
 * PHP: Philippine pesos<br>
 * USD: United States dollar<br>
 * CNY: Renminbi<br>
 *
 * @property currencyAlphCode
 * @type {String}
 */
/**
 * CustomConfig of system.
 * @class eBase.data.CustomConfig
 */
/**
 * key of CustomConfig.
 * @property key
 * @type {String}
 */
/**
 * value of CustomConfig.
 * @property value
 * @type {String}
 */
/**
 * Device
 * @class eBase.data.Device
 */
/**
 * Device logical ID, which is created on the UMS.
 * @property deviceId
 * @type {String}
 */
/**
 * ID of a device group.The options are as follows:
 *
 * - 0: STB
 * - 1: PC Client
 * - 2: OTT device, including PC plug-in, iOS, and Android devices
 * - 3: mobile terminals for MTV solution
 * - Values greater than or equal to 5: self-defined device
 *
 * @property deviceType
 * @type {String}
 */
/**
 * Indicates whether a device is online.The options are as follows:
 *
 * - 0: no
 * - 1: yes
 *
 * @property isonline
 * @type {String}
 */
/**
 * Device name.
 * @property deviceName
 * @type {String}
 */
/**
 * Device physical address.
 *
 * - For STBs, the parameter value is the STB MAC address.
 * - For OTT devices, the parameter is set to the value of clientId generated by Verimatrix plug-in.
 *
 * @property physicalDeviceId
 * @type {String}
 */
/**
 * Last time when the subscriber logs out of the device.Format: yyyyMMddHHmmss.If the subscriber never logs in to the device, the parameter is not contained.
 * @property lastOfflineTime
 * @type {String}
 */
/**
 * CA info.
 * @property caDeviceInfos
 * @type {String}
 */
/**
 * Terminal type. The parameter is valid only when a subscriber logs in to a terminal and the terminal type is reported.
 * @property terminalType
 * @type {String}
 */
/**
 * Whether a device supports the PVR service. The options are as follows:
 *
 * - 1: yes
 * - 0: no
 * - The default value is 0.
 *
 * @property isSupportPVR
 * @type {String}
 */
/**
 * Naming space of a channel associated with a device.
 * @property channelNamespace
 * @type {String}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Favorite
     * @class eBase.data.Favorite
     */
    data.Favorite = function(json) {
        return json;
    };
    /**
     * Content ID.
     * @property id
     * @type {String}
     */
    /**
     * Content type. The options are as follows:
飦?  *     SUBJECT: category
飦?  *     VOD: on-demand content
飦?  *     AUDIO_VOD: audio on demand
飦?  *     VIDEO_VOD: video on demand
飦?  *     TELEPLAY_VOD: episode of a series
飦?  *     CREDIT_VOD: ad content
飦?  *     CHANNEL: channel
飦?  *     AUDIO_CHANNEL: audio channel
飦?  *     VIDEO_CHANNEL: video channel
飦?  *     WEB_CHANNEL: web channel
飦?  *     MIX: combination of several content types
飦?  *     VAS: value-added service
飦?  *     PROGRAM: live TV program
飦?  *     TVOD: Catch-up TV program
     * @property type
     * @type {String}
     */
    /**
     * Favorites category ID. The value -1 indicates the default favorites category.
     * @property favoId
     * @type {String}
     */
    /**
     * Time when content is added to favorites. The format is yyyyMMddHHmmss.
     * @property collectTime
     * @type {String}
     */
    return core;
}(eBase));
/**
 * Genre of system.
 * @class eBase.data.Genre
 */
/**
 * Id of Genre.
 * @property genreId
 * @type {String}
 */
/**
 * Type of Genre.
 * @property genreType
 * @type {String}
 */
/**
 * Name of Genre.
 * @property genreName
 * @type {String}
 */
/**
 * GuestInfo of system.The guest user can be used to log in to the IPTV system, browse content information and posters. The guest user cannot subscribe to or use the IPTV service.
 * @class eBase.data.GuestInfo
 */
/**
 * subscriberId of GuestInfo.
 * @property subscriberId
 * @type {String}
 */
/**
 * password of GuestInfo.
 * @property password
 * @type {String}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Hearbeat
     * @class eBase.data.Heartbit
     */
    data.Hearbit = function(json) {
        return json;
    };
    /**
     * Indicates whether a subscriber is valid.
     *     The options are as follows, true: valid, false: invalid
     * @property uservalid
     * @type {String}
     */
    /**
     * Interval for sending heartbeat messages. Unit: second
     * @property nextcallinterval
     * @type {Number}
     */
    /**
     * Version of the channel metadata and subscription relationship on the EPG server.
     *     After a client receives a response, it compares the channel version with that stored in the cache. If they are different, the client sends a request to obtain the channel list.
     *     Request format: channelversion|subscribeversion, In the format, channelversion is the channel metadata version, and subscribeversion is the subscriber subscription relationship version.
     * @property channelversion
     * @type {String}
     */
    /**
     * Subscriber-related system parameters.
     * @property parameters
     * @type {Object}
     *     The properties of the object are as follows:
     *     favouritelimit:        Upper limit number of content a profile can add to favorites. Each profile has their upper limit.
     *     bookmarklimit:         Upper limit number of bookmarks a profile can add. Each profile has their upper limit.
     *     locklimit:             Upper limit number locks all profiles can add.
     *     profilelimit:          Upper limit number of profiles a subscriber can have.
     *     mashupaddress:         Address of the Mashup platform.
     *     tvmsheartbiturl:       URL for the client to send heartbeat messages to the TVMS. Example: http://1.0.0.88:35220/gateway/queryXML.do
     *     tvmsvodheartbiturl:    URL for the client to send VOD messages to the TVMS. Example: http://1.0.0.88:35220/gateway/queryXML.do
     *     tvmsheartbitinterval:  Period for the client to send heartbeat messages to the TVMS. Unit: second
     *     tvmsdelaylength:       Display delay of the TV messages due to a message conflict. Unit: second
     *     issupportpublicad:     Indicates whether public advertising is supported. The options are as follows, 1: yes, 0: no
     *     adplatformurl:         URL of the ad platform.
     *     adpublicstrategyurl:   URL for obtaining the ad policy.
     *     adplayovernotifyurl:   URL for notifying the ad ending.
     *     bitband:               Subscriber's bandwidth information. Unit: kbit/s
     *     giftLoyaltyByBrowseAd: Bonus points awarded to a subscriber for viewing ad details.
     *     repeatTVLength:        Repeat TV rewind interval. Unit: second, Repeat TV is for TMNL only.
     *     restartTVOffset:       Restart TV time shift offset. Unit: second, Restart TV is for TMNL only.
     *     pltvDelay:             Delay for enabling the local TSTV service. Unit: second
     *     DVBEanble:             Indicates whether the IPTV platform supports the maintenance of DVB channels. The options are as follows, 0: no, 1: yes
     *     sqmurl:                URL of the Service Quality Management (SQM) system.
     *     giftLoyaltyByReceiveAdWithSMS:      Bonus points awarded to a subscriber for receiving ads by SMS messages.
     *     giftLoyaltyByReceiveAdWithEmail:    Bonus points awarded to a subscriber for receiving ads by email.
     */
    /**
     * List of content items to be reminded of in the heartbeat period.
     *     The parameter is returned after a subscriber performs the reminder operation.
     * @property reminderContents
     * @type {eBase.data.Reminder}
     */
    /**
     * Favorites version.
     *     The response contains this parameter if the subscriber performs favorites-related operations. If the version is changed, the client can invoke the Interface to Query Content in Favorites to obtain content list in favorites again.
     * @property favoversion
     * @type {String}
     */
    /**
     * Lock version.
     *     The response contains this parameter if the subscriber performs lock-related operations. If the version is changed, the client can invoke the Interface to Query Information About Locked Content to obtain the locked content list again.
     * @property lockversion
     * @type {String}
     */
    /**
     * Bookmark version.
     *     The response contains this parameter if the subscriber performs bookmark-related operations. If the version is changed, the client can invoke the Interface to Query Bookmark Content to obtain the bookmark list again.
     * @property bookmarkversion
     * @type {String}
     */
    /**
     * Profile version.
     *     The response contains this parameter if the subscriber performs profile-related operations. If the version is changed, the client can invoke the Interface to Query Profiles to obtain the profile list again.
     * @property profileversion
     * @type {String}
     */
    /**
     * CPVR version.
     *     The response contains this parameter if the subscriber performs CPVR-related operations. If the version is changed, the client can invoke the Interface to Query PVR Tasks by Filter Criteria to obtain PVR task list again.
     * @property pvrversion
     * @type {String}
     */
    /**
     * PPV version.
     *     The response contains this parameter if the subscriber subscribes to a program or cancels the subscription to a program. If the version is changed, the client can invoke the Interface to Query Program Lists again.
     * @property ppvversion
     * @type {String}
     */
    /**
     * Reminder version.
     *     The response contains this parameter if the subscriber creates a program reminder. If the version is changed, the client can invoke the Interface to Query Program Reminders to obtain the program reminder list again.
     * @property reminderversion
     * @type {String}
     */
    /**
     * Remote series/period task version.
     *     The response contains this parameter if the subscriber performs Remote PVR parent task-related operations. If the version is changed, the client can invoke the Interface to Query Series or Periodic PVR Tasks to obtain the PVR task list again.
     * @property remotepvrversion
     * @type {String}
     */
    /**
     * Indicates whether a subscriber is valid.
     * @property personalDataVersions
     * @type {Array}
     *     Version number for personalized data.
     *     The options of key of NamedParameter are as follows, NPVR: network recording, SERIESNPVR: series or periodical network recording.
     *     The value parameter of NamedParameter indicates the version number corresponding to personalized data.
     *     This version number is returned only when the personalized data is processed.
     */
    return core;
}(eBase));
/**
 * HotKeyword
 * @class eBase.data.HotKeyword
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Lock
     * @class eBase.data.Lock
     */
    data.Lock = function(json) {
        return json;
    };
    /**
     * Content ID.
     * @property id
     * @type {String}
     */
    /**
     * Content type. The options are as follows:
飦?  *     SUBJECT: category
飦?  *     VOD: on-demand content
飦?  *     AUDIO_VOD: audio on demand
飦?  *     VIDEO_VOD: video on demand
飦?  *     TELEPLAY_VOD: episode of a series
飦?  *     CREDIT_VOD: ad content
飦?  *     CHANNEL: channel
飦?  *     AUDIO_CHANNEL: audio channel
飦?  *     VIDEO_CHANNEL: video channel
飦?  *     WEB_CHANNEL: web channel
飦?  *     MIX: combination of several content types
飦?  *     VAS: value-added service
飦?  *     PROGRAM: live TV program
飦?  *     TVOD: Catch-up TV program
     * @property type
     * @type {String}
     */
    return core;
}(eBase));
/**
 * MosaicChannel
 * @class eBase.data.MosaicChannel
 */
/**
 * Id of MosaicChannel.
 * @property id
 * @type {String}
 */
/**
 * Name of MosaicChannel.
 * @property name
 * @type {String}
 */
/**
 * description of MosaicChannel.
 * @property description
 * @type {String}
 */
/**
 * Sequence number of a Mosaic channel.
 * @property channelIndex
 * @type {String}
 */
/**
 * Name of the background channel played on a Mosaic channel.
 * @property bgChannelNo
 * @type {String}
 */
/**
 * Height of a background channel.Unit: pixel.
 * @property bgHeight
 * @type {String}
 */
/**
 * Width of a background channel.Unit: pixel.
 * @property bgWidth
 * @type {String}
 */
/**
 * Information about each cell on a Mosaic channel.Oject is MosaicConfig.
 * @property pipChannels
 * @type {Object[]}
 */
/**
 * MosaicConfig
 * @class eBase.data.MosaicConfig
 */
/**
 * Cell position (distance between the top of the background channel and the cell).Unit: pixel.
 * @property top
 * @type {String}
 */
/**
 * Cell position (distance between the left side of the background channel and the cell).Unit: pixel.
 * @property left
 * @type {String}
 */
/**
 * Cell height.Unit: pixel.
 * @property height
 * @type {String}
 */
/**
 * Cell width.Unit: pixel.
 * @property width
 * @type {String}
 */
/**
 * Number of the channel played in a cell.
 * @property channelno
 * @type {String}
 */
/**
 * Audio PID for the channel played in a cell.
 * @property pid
 * @type {String}
 */
/**
 * Channel ID of Mosaic streams.
 * @property channelId
 * @type {String}
 */
/**
 * PPV
 * @class eBase.data.PPV
 */
/**
 * Channel ID.
 * @property channelid
 * @type {String}
 */
/**
 * Program ID.
 * @property programid
 * @type {String}
 */
/**
 * Content name.
 * @property ppvname
 * @type {String}
 */
/**
 * Content type. Currently, only the value PROGRAM is returned.
 * @property contenttype
 * @type {String}
 */
/**
 * PPV product start time.Format: yyyyMMddHHmmss.
 * @property starttime
 * @type {String}
 */
/**
 * PPV product end time.Format: yyyyMMddHHmmss.
 * @property endtime
 * @type {String}
 */
/**
 * Product ID.
 * @property productid
 * @type {String}
 */
/**
 * PVR task info
 * @class eBase.data.PVRTask
 */
/**
 * PVR task ID. The parameter is not contained for adding operation.
 * @property pvrId
 * @type {String}
 */
/**
 *Channel ID. The parameter is not contained for adding operation.
 * @property channelId
 * @type {String}
 */
/**
 * Media file ID. The parameter is not contained for adding operation.
 * @property mediaId
 * @type {String}
 */
/**
 * Channel number.
 * @property channelNo
 * @type {Number}
 */
/**
 * Program ID.
 * If a program PVR task is added, the parameter is contained.
 * If a channel PVR task (by time segment or immediate) is added, the parameter is not contained. The default value is -1.
 * @property programId
 * @type {String}
 */
/**
 * PVR task start time.
 * Format: yyyyMMddHHmmss
 * If the client sends a request to create a program PVR task, the parameter is not contained in the request.
 * @property beginTime
 * @type {String}
 */
/**
 * PVR task end time.
 * Format: yyyyMMddHHmmss If the client sends a request to create a program PVR task, the parameter is not contained in the request.
 * @property endTime
 * @type {String}
 */
/**
 * Name of a PVR task. If the client sends a request to create a PVR task, the parameter is not contained in the request.
 * @property pvrName
 * @type {String}
 */
/**
 * Status of a PVR task. If the client sends a request to create a PVR task, the parameter is not contained in the request.
 * The options are as follows:
 *
 * - -1: not starting
 * - -3: to be deleted
 * - -2: to be sent
 * - 0: recording success
 * 1: recording in progress
 * 2: recording failed
 * The causes are as follows:
 *
 * - Insufficient network space
 * - Content expired and deleted
 * - No live TV channel available
 * - Content not allowed to be recorded
 * - Failure to obtain live TV content
 * - Program expired.
 * The parameter is required only when the cPVR task created by the Hybrid STB in DVB mode is synchronized to the MEM.
 * @property status
 * @type {Number}
 */
/**
 * Device logical ID. The parameter is valid for Remote PVR task.
 * @property deviceId
 * @type {String}
 */
/**
 * Duration that a program has been played before a bookmark is added. The parameter is contained only when a bookmark is added to the PVR task.
 * Unit: second
 * @property bookmarkTime
 * @type {String}
 */
/**
 * Type of the supported recording service.
 * The options are as follows:
 *
 * - 1: cPVR
 * - 2: nPVR
 * @property type
 * @type {Number}
 */
/**
 * Internal task ID generated by the STB.
 * This parameter is valid only for cPVR tasks.
 * @property stbPvrId
 * @type {String}
 */
/**
 * ID of a parent task generated by the STB.
 * The parameter is used only for the Hybrid STB. The STB creates a parent task and synchronizes the task ID to the server in the DVB mode.
 * @property stbPeriodId
 * @type {String}
 */
/**
 * Profile ID used for creating a PVR task.
 * If the parameter is not contained, the current profile is used.
 * @property profileId
 * @type {String}
 */
/**
 * ID of a parent task. The parameter is valid for series/periodic recording tasks. Separate multiple values with a comma (,).
 * @property periodPVRTaskId
 * @type {String}
 */
/**
 * Recording time after a program ends.
 * Unit: minute
 * The default value is 0.
 * @property endOffset
 * @type {Number}
 */
/**
 * Indicates whether to transfer time shift content to PVR content.
 * This parameter is used in the Hybrid TV mode.
 * The options are as follows:
 *
 * - 0: no
 * - 1: yes
 * The default value is 0.
 * @property isPltvtoPVR
 * @type {Number}
 */
/**
 * Indicates whether PVR tasks conflict with each other.
 * The options are as follows:
 *
 * - 1: yes
 * - 0: no
 * If two PVR tasks conflict with each other, the task that starts first is recorded first.
 * The parameter is not required for adding or deleting a recording task.
 * @property isConflict
 * @type {Number}
 */
/**
 * Language of the soundtrack the subscriber prefers.
 * The values are codes for the representation of language names specified in the ISO 639-1 standard.
 * @property language
 * @type {String}
 */
/**
 * Language of the subtitle the subscriber prefers.
 * The values are codes for the representation of language names specified in the ISO 639-1 standard.
 * @property subtitle
 * @type {String}
 */
/**
 * Content source. The options are as follows:
 *
 * - DVB: PVR in the DVB domain
 * - IP: PVR in the IPTV domain
 * The default value is IP.
 * @property contentPath
 * @type {String}
 */
/**
 * Name of a program episode. The parameter is valid when the series has episodes.
 * @property subName
 * @type {String}
 */
/**
 * ID of a program episode. The parameter is valid when the series has episodes.
 * @property subNum
 * @type {Number}
 */
/**
 * Period PVR task info
 * @class eBase.data.PeriodPVRTask
 */
/**
 * Type of a PVR task series.
 * The options are as follows:
 *
 * - 0: recording by time segment. The task is set to recording programs played at the specified time.
 * - 1: recording by keywords. The task is set to recording a series of related programs, for example, a series.
 * The default value is 0.
 * @property seriesType
 * @type {Number}
 */
/**
 * Channel number.
 * @property channelno
 * @type {Number}
 */
/**
 * Type of the supported recording service.
 * The options are as follows:
 *
 * - 1: cPVR
 * - 2: nPVR
 * @property type
 * @type {Number}
 */
/**
 * Task ID.
 * The attribute is generated automatically in new task requests and is contained in task update or deletion requests.
 * @property periodPVRTaskId
 * @type {Number}
 */
/**
 * Device ID.
 * This parameter is valid only for cPVR tasks. If this attribute is left blank, the current login device is used.
 * @property deviceId
 * @type {String}
 */
/**
 * ID of a parent task generated by the STB.
 * The parameter is used only for the Hybrid STB. The STB creates a parent task and synchronizes the task ID to the server in the DVB mode.
 * @property stbPeriodId
 * @type {String}
 */
/**
 * ID of a profile who creates a parent task. If the parameter is not contained, it indicates the current profile (already logged in) creates the parent task.
 * @property profileId
 * @type {String}
 */
/**
 * Status of a parent task.
 * The options are as follows:
 *
 * - 0: normal
 * - -2: not sent
 *  -3: to be deleted
 * The default value is 0.
 * @property status
 * @type {Number}
 */
/**
 * Preferred soundtrack language.
 * The values are codes for the representation of language names specified in the ISO 639-1 standard.
 * @property language
 * @type {String}
 */
/**
 * Preferred subtitle language.
 * The values are codes for the representation of language names specified in the ISO 639-1 standard.
 * @property subtitle
 * @type {String}
 */
/**
 * Content source. The options are as follows:
 *
 * - DVB: PVR in the DVB domain
 * - IP: PVR in the IPTV domain
 * The default value is IP.
 * @property contentPath
 * @type {String}
 */
/**
 * Time when a task is created. The format is yyyyMMddHHmmss.
 * @property createTime
 * @type {String}
 */
/**
 * Time when a series or periodical recording parent task takes effect. The format is yyyyMMddHHmmss.
 * If this parameter is not specified, the time when the MEM receives a request is used as the time when the parent task takes effect.
 * The rules for processing effectivetime of a serial recording task are as follows:
 * If a program starts after the time specified by effectivetime (effectivetime <= PlayBill.begintime), a subtask is directly generated.
 * If a program ends before the time specified by effectivetime (PlayBill.endtime<= effectivetime), no subtask is generated.
 * If the time specified by effectivetime is in the process of the program playback, the start time of the generated subtask is the value of effectivetime, and the end time of the generated subtask is the program end time.
 * @property effectivetime
 * @type {String}
 */
/**
 * End time of a recording task.
 * Format: yyyyMMddHHmmss
 * If this parameter is not carried, the recording task is in process.
 * @property overtime
 * @type {String}
 */
/**
 * Time when recording starts.
 * Format: hhmmss
 * The attribute is valid only when seriesType is set to 0.
 * @property starttime
 * @type {String}
 */
/**
 * Time when recording stops.
 * Format: hhmmss
 * The attribute is valid only when seriesType is set to 0.
 * @property endtime
 * @type {String}
 */
/**
 * Recording period (days involved). If the recording lasts for multiple days, separate multiple values with commas (,).
 * For example, 1,2,3
 * 1-7: Monday to Sunday
 * @property days
 * @type {String}
 */
/**
 * Program keyword.The attribute is valid only when seriesType is set to 1.
 * @property keyword
 * @type {String}
 */
/**
 * Recording time after a program ends.The attribute is valid only when seriesType is set to 1.
 * Unit: minute
 * The default value is 0.
 * @property endOffset
 * @type {Number}
 */
/**
 * Picture
 * @class eBase.data.Picture
 */
/**
 * Path for storing thumbnails.
 * Use the absolute path (HTTP URL), for example, http://12.24.11.120:8080/EPG/jsp/image/12.jpg.
 * Separate multiple values with a comma (,).
 * @property deflate
 * @type {String}
 */
/**
 * Path for storing posters.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property poster
 * @type {String}
 */
/**
 * Path for storing stills.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property still
 * @type {String}
 */
/**
 * Path for storing icons.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property icon
 * @type {String}
 */
/**
 * Path for storing titles pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property title
 * @type {String}
 */
/**
 * Path for storing ad pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property ad
 * @type {String}
 */
/**
 * Path for draft pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property draft
 * @type {String}
 */
/**
 * Path for storing background pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property background
 * @type {String}
 */
/**
 * Path for channel pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property channelpic
 * @type {String}
 */
/**
 * Path for channel black and white pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property blackwhite
 * @type {String}
 */
/**
 * Path for channel name pictures.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property channame
 * @type {String}
 */
/**
 * Path for other icons.
 * Separate multiple values with a comma (,). Use the absolute path.
 * @property other
 * @type {String}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Playbill
     * @class eBase.data.Playbill
     */
    data.Playbill = function(json) {
        return json;
    };
    /**
     * Cast information of content.
     * @property casts
     * @type {Object}
     */
    /**
     * Channel id.
     * @property channelid
     * @type {String}
     */
    /**
     * For a Barker channel, the parameter indicates the ID of the content associated with the Barker Channel.
     * @property contentId
     * @type {String}
     */
    /**
     * Rating.
     * @property contentRating
     * @type {String}
     */
    /**
     * For a Barker channel, the parameter indicates the type of the content associated with the Barker Channel.The options are as follows:
     *
     *- 0: video on demand
     * - 1: video channel
     * - 2: audio channel
     * - 4: audio on demand
     * - 5: web channel
     * - 100: VAS
     * - 300: program
     *
     * For non-Barker channels, the parameter is invalid.
     * @property contentType
     * @type {String}
     */
    /**
     * Country.
     * @property country
     * @type {String}
     */
    /**
     * Program end time.Format: yyyyMMddHHmmss.Example: 20120426170534
     * @property endtime
     * @type {String}
     */
    /**
     * Program start time.Format: yyyyMMddHHmmss.Example: 20120426170534.
     * @property starttime
     * @type {String}
     */
    /**
     * Content code. Specifies the foreign serial number of the content.
     * @property foreignsn
     * @type {String}
     */
    /**
     * Genre name of a program.Separate multiple values with a comma (,).
     * @property genres
     * @type {String}
     */
    /**
     * Playbill introduce.
     * @property introduce
     * @type {String}
     */
    /**
     * Indicates whether a channel supports nPVR.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     *
     * @property isnpvr
     * @type {String}
     */
    /**
     * Indicates whether a playbill supports PPV.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     *
     * @property isppv
     * @type {String}
     */
    /**
     * Indicates whether a program supports the Catch-up TV function:
     *
     * - 0: no
     * - 1: yes
     *
     * @property istvod
     * @type {String}
     */
    /**
     * Indicates whether a program has been subscribed to:The options are as follows:
     *
     * 0: no
     * 1: yes
     *
     * @property issubscribed
     * @type {String}
     */
    /**
     * Playbill keyword
     * @property keyword
     * @type {String}
     */
    /**
     * Playbill name
     * @property name
     * @type {String}
     */
    /**
     * Playbill poster.
     * @property picture
     * @type {Object}
     */
    /**
     * Indicates whether a subscriber has subscribed to the program PPV product.The options are as follows:
     *
     * - 0: no
     * - 1: yes
     *
     * @property ppvsubscribed
     * @type {String}
     */
    /**
     * Production date.The format is yyyy-MM-dd.
     * @property producedate
     * @type {String}
     */
    /**
     * Program type.The options are as follows:
     *
     * - movie: movie
     * - episode: episode
     * - program: common program
     *
     * @property programType
     * @type {String}
     */
    /**
     * Content rating. A larger value indicates a higher rating. The value varies depending on the content rating systems in different countries.
     * @property ratingid
     * @type {String}
     */
    /**
     * Season ID of a program, which is valid only when the program belongs to a seasons.
     * @property seasonNum
     * @type {String}
     */
    /**
     * ID of a program episode. The parameter is valid when the series has episodes.
     * @property subNum
     * @type {String}
     */
    /**
     * Content type.Set the parameter to PROGRAM.
     * @property type
     * @type {String}
     */
    return core;
}(eBase));
/**
 * PointBalance
 * @class eBase.data.PointBalance
 */
/**
 * Points get time.
 * @property getTime
 * @type {String}
 */
/**
 * Points expire time.
 * @property expireTime
 * @type {String}
 */
/**
 * Points.
 * @property points
 * @type {String}
 */
/**
 * The information about the regions.
 * @class eBase.data.ProduceZone
 */
/**
 * Name of produceZone.
 * @property name
 * @type {String}
 */
/**
 * id of produceZone.
 * @property id
 * @type {String}
 */
/**
 * Product
 * @class eBase.data.Product
 */
/**
 * Content or category ID of the priced object.The parameter is valid when priceobjecttype is set to 2 or 3.If a node is associated with multiple categories, use commas (,) to separate them.
 * @property contentid
 * @type {String}
 */
/**
 * Content type of a priced object.The parameter is valid when priceobjecttype is set to 2 or 3.
 *
 * - VIDEO_VOD
 * - AUDIO_VOD
 * - VIDEO_CHANNEL
 * - AUDIO_CHANNEL
 * - WEB_CHANNEL
 * - VAS
 * - SUBJECT
 * - PROGRAM
 *
 * @property contenttype
 * @type {String}
 */
/**
 * Indicates whether the automatic subscription renewal is supported.The options are as follows:
 *
 * - 0: no
 * - 1: yes.
 *
 * @property continueable
 * @type {String}
 */
/**
 * Subscription end time.The format is yyyyMMddHHmmss.
 * @property endtime
 * @type {String}
 */
/**
 * Subscription start time.The format is yyyyMMddHHmmss.
 * @property starttime
 * @type {String}
 */
/**
 * id of Product.
 * @property id
 * @type {String}
 */
/**
 * introduce of Product.
 * @property introduce
 * @type {String}
 */
/**
 * Indicates whether a product can be subscribed to online. The options are as follows:
 *
 * - 0: no
 * - 1: yes.
 *
 * @property isEnableOnlinePurchase
 * @type {String}
 */
/**
 * Indicates whether the package is a basic package.The options are as follows:
 *
 * - 0: non-basic package
 * - 1: basic package
 *
 * @property isMain
 * @type {String}
 */
/**
 * name of Product.
 * @property name
 * @type {String}
 */
/**
 * When the subscriber order the product.The format is yyyyMMddHHmmss.
 * @property ordertime
 * @type {String}
 */
/**
 * Bundle information. The parameter is contained when priceobjecttype is set to 4.
 * @property package
 * @type {String}
 */
/**
 * Bonus points required for subscribing to content.The parameter is valid when priceobjecttype is set to 2 and the content supports subscription by bonus point.
 * @property points
 * @type {String}
 */
/**
 * Number of IPTV bonus points.It is not included in the subscribed product list. If the value is -1, the product does not support subscription by bonus points.
 * @property point
 * @type {String}
 */
/**
 * Price of Product.
 * @property price
 * @type {String}
 */
/**
 * ID of a priced object.
 * @property priceobjectid
 * @type {String}
 */
/**
 * Type of a priced object.The options are as follows:
 *
 * - 1: service
 * - 2: content
 * - 3: node
 * - 4: bundle
 *
 * @property priceobjecttype
 * @type {String}
 */
/**
 * Rental period for times-based product.If the subscribable products are queried, the parameter is contained.Unit: hour.
 * @property rentPeriod
 * @type {String}
 */
/**
 * Restrictions of a product. Use ampersands (&) to separate multiple restrictions. The format is similar to [attribute name 1]=[attribute value 1]&[attribute name 2]=[attribute value 2].
 * @property restriction
 * @type {String}
 */
/**
 * Indicates whether a subscriber has subscribed the product. The options are as follows:
 *
 * - 0: no
 * - 1: yes
 *
 * @property subscriptionState
 * @type {String}
 */
/**
 * Product type.The options are as follows:
 *
 * - 1: times-based product
 * - 0: monthly package product
 *
 * @property type
 * @type {String}
 */
/**
 * ProductDiscountInfo
 * @class eBase.data.ProductDiscountInfo
 */
/**
 * Id of Product.
 * @property productId
 * @type {String}
 */
/**
 * Price before discount. The unit is the currency unit configured in the UMS.
 * @property preDiscountFee
 * @type {String}
 */
/**
 * Price after discount.
 * @property discountFee
 * @type {String}
 */
/**
 * Time when the discount expires.The format is yyyyMMddHHmmss.
 * @property discountExpireTime
 * @type {String}
 */
/**
 * Currency code,like 'USD' , 'CNY'.
 * @property currency
 * @type {String}
 */
/**
 * User's personal info.
 * @class eBase.data.Profile
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};

    data.Profile = function(json) {
        /**
         * Profile id.ID of an object. Normally, the parameter is set to the profile ID, whose value is the same as that of identityid in the UserIdentity metadata.
         * @property id
         * @type {String}
         */
        /**
         * Profile name.
         * @property name
         * @type {String}
         */
        /**
         * Profile password (encrypted).
         * @property password
         * @type {String}
         */
        /**
         * Profile introduce.
         * @property introduce
         * @type {String}
         */
        /**
         * Consumption limit. The value -1 indicates that the profile does not have consumption limit but is restricted by the subscriber's credit limit, the default value -1.
         * @property quota
         * @type {String}
         */
        /**
         * Avatar URL. The URL contains only the avatar's file name, such as 0.png.
         * @property logourl
         * @type {String}
         */
        /**
         * Parental control level.
         * @property levels
         * @type {String}
         */
        /**
         * IDs of channels restricted by a profile. Separate multiple values with a comma (,).The profile cannot access the channels in the list.
         * @property channelids
         * @type {String}
         */
        /**
         * IDs of categories restricted by a profile. Separate multiple values with a comma (,).The profile cannot access the categories in the list.
         * @property categoryids
         * @type {String}
         */
        /**
         * IDs of VASs restricted by a profile. Separate multiple values with a comma (,).The profile cannot use the VASs in the list.
         * @property vasids
         * @type {String}
         */
        /**
         * Profile type.The options are as follows:
         *
         * - 0: super profile
         * - 1: common profile
         * - When the SubscriberMgmt interface is used to create a subscriber, the parameter is optional and the value 0 is used.
         *
         * @property profiletype
         * @type {String}
         */
        /**
         * Indicates whether TVMS messages will be displayed.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         * - The default value is 0.
         *
         * @property hidemessage
         * @type {String}
         */
        /**
         * EPG template name.
         * @property template
         * @type {String}
         */
        /**
         * Language.The options are as follows:
         *
         * - 1: language 1
         * - 2: language 2
         * - The default value is 1.
         *
         * @property lang
         * @type {String}
         */
        /**
         * Profile mobilePhone
         * @property mobilePhone
         * @type {String}
         */
        /**
         * Indicates whether the subscription password is required during product subscription.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         * - The default value is 1.
         *
         * @property needSubscriberCnfmFlag
         * @type {String}
         */
        /**
         * Email address.
         * @property email
         * @type {String}
         */
        /**
         * Indicates whether Info Bar is displayed on the client during channel switch.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         * - The default value is 0.
         *
         * @property isDisplayInfoBar
         * @type {String}
         */
        /**
         * Default channel list type.The options are as follows:
         *
         * - 1: system channel list
         * - 2: self-defined channel list
         * - The default value is 1.
         *
         * @property channellistType
         * @type {String}
         */
        /**
         * ID of a device bound to a profile. If this parameter is contained, it indicates that the profile is the default profile for the device.
         * @property deviceid
         * @type {String}
         */
        /**
         * Subscriber's birthday. The format is YYYYMMdd.
         * @property birthday
         * @type {String}
         */
        /**
         * Indicates whether the profile password is required during login.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         * - The default value is 1.
         *
         * @property profilePINEnable
         * @type {String}
         */
        /**
         * Indicates whether a profile is allowed to log in to multiple terminals at the same time.The options are as follows:
         *
         * - 0: yes
         * - 1: no
         * - The default value is 0.
         *
         * @property multiscreenEnable
         * @type {String}
         */
        /**
         * Indicates whether subscription is supported.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         * - The default value is 1.
         *
         * @property purchaseEnable
         * @type {String}
         */
        /**
         * Globally unique registered subscriber name. The value consists of letters, digits, and underscores. The value is defined by the subscriber.If the subscriber does not define the value, the value is the same as the profile ID by default.
         * @property loginName
         * @type {String}
         */
        /**
         * Indicates whether a profile is online.The options are as follows:
         *
         * - 0: no
         * - 1: yes
         *
         * @property isonline
         * @type {String}
         */
        /**
         * ID of the subscriber using a profile.
         * @property subscriberId
         * @type {String}
         */
        return json;
    };

    return core;
}(eBase));
/**
 * Rating/Parental control level list of system.
 * @class eBase.data.Rating
 */
/**
 * Parental control level ID.
 * @property id
 * @type {String}
 */
/**
 * Parental control level name.
 * @property name
 * @type {String}
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Reminder
     * @class eBase.data.Reminder
     */
    data.Reminder = function(json) {
        return json;
    };
    /**
     * Reminder type. The options are as follows, 1: program reservation, 2: notice
     * @property type
     * @type {String}
     */
    /**
     * IPTV content ID. This parameter is optional for only the clear operation, indicating deleting that the program reminders of a content type.
     * @property contentID
     * @type {String}
     */
    /**
     * IPTV content type. The options are as follows: AUDIO_VOD, VIDEO_VOD, PROGRAM
     * @property contentType
     * @type {String}
     */
    /**
     * Time when the reminder is displayed. The format is yyyyMMddHHmmss.
     * For a program reminder, the parameter is specified in the request sent by the client to the system. If the request does not contain the information, the MEM generates a value (program start time minus the value of leadTimeForSendReminder in the MultiProfile table) automatically.
     * For a VOD reminder, the parameter must be specified by the client.
     * @property reminderTime
     * @type {String}
     */
    return core;
}(eBase));
/**
 * SearchContent
 * @class eBase.data.SearchContent
 */
/**
 * Content id.
 * @property id
 * @type {String}
 */
/**
 * Content type.The options are as follows:
 *
 * - SUBJECT: category
 * - VOD: on-demand content
 * - AUDIO_VOD: audio on demand
 * - VIDEO_VOD: video on demand
 * - TELEPLAY_VOD: episode of a series
 * - CREDIT_VOD: ad content
 * - CHANNEL: channel
 * - AUDIO_CHANNEL: audio channel
 * - VIDEO_CHANNEL: video channel
 * - WEB_CHANNEL: web channel
 * - MIX: combination of several content types
 * - VAS: value-added service
 * - PROGRAM: live TV program
 * - TVOD: Catch-up TV program
 *
 * @property type
 * @type {String}
 */
/**
 * Indicates whether a content item is added to favorites.The options are as follows:
 *
 * - 1: yes
 * - 0: no
 *
 * @property isfavorited
 * @type {String}
 */
/**
 * Start time.Format: yyyyMMddHHmmss
 *
 * - For a live TV program, this parameter indicates the start time of the live TV program.
 * - For other content, this parameter indicates the time when the content is released.
 *
 * @property starttime
 * @type {String}
 */
/**
 * End time.Format: yyyyMMddHHmmss
 *
 * - For a live TV program, this parameter indicates the end time of the live TV program.
 * - For other content, this parameter indicates the time when the content is deleted.
 *
 * @property endtime
 * @type {String}
 */
/**
 * Content name.
 * @property name
 * @type {String}
 */
/**
 * Content description.
 * @property introduce
 * @type {String}
 */
/**
 * Category poster.
 * @property picture
 * @type {String}
 */
/**
 * Content rating.A larger value indicates a higher rating. The value varies depending on the content rating systems in different countries.
 * @property ratingid
 * @type {String}
 */
/**
 * Country or area where a program is produced. The values are codes in the ISO3166-1 standard. Separate multiple values with a comma (,).
 * @property country
 * @type {String}
 */
/**
 * Subscriber's consumption type in the regional node.
 * @class eBase.data.SubnetInfo
 */
/**
 * Subscriber's consumption type in the regional node.
 *
 * - 1: subscriber's consumption limit in a fixed bill cycle
 * - 2: subscriber's experience times limit in a flexible bill cycle
 * - 3: subscriber's consumption limit in a flexible bill cycle
 *
 * @property creditType <br>
 * @type {String}
 */
/**
 * SubscribedContent
 * @class eBase.data.SubscribedContent
 */
/**
 * Start time.The format is yyyyMMddHHmmss.
 * @property starttime
 * @type {String}
 */
/**
 * End time.The format is yyyyMMddHHmmss.
 * @property endtime
 * @type {String}
 */
/**
 * Subscription start time.The format is yyyyMMddHHmmss.
 * @property serstarttime
 * @type {String}
 */
/**
 * Subscription end time.The format is yyyyMMddHHmmss.
 * @property serendtime
 * @type {String}
 */
/**
 * Service type.The options are as follows:
 *
 * - 1: monthly package
 * - 3: PPV
 * - 5: Catch-up TV
 *
 * @property businesstype
 * @type {Number}
 */
/**
 * SubscriptionInfo
 * @class eBase.data.SubscriptionInfo
 */
/**
 * Content or category ID of the priced object.The parameter is valid when priceobjecttype is set to 2 or 3.If a node is associated with multiple categories, use commas (,) to separate them.
 * @property contentid
 * @type {String}
 */
/**
 * Content type of a priced object.The parameter is valid when priceobjecttype is set to 2 or 3.
 *
 * - VIDEO_VOD
 * - AUDIO_VOD
 * - VIDEO_CHANNEL
 * - AUDIO_CHANNEL
 * - WEB_CHANNEL
 * - VAS
 * - SUBJECT
 * - PROGRAM
 *
 * @property contenttype
 * @type {String}
 */
/**
 * Indicates whether the automatic subscription renewal is supported.The options are as follows:
 *
 * - 0: no
 * - 1: yes.
 *
 * @property continueable
 * @type {String}
 */
/**
 * ID of a logical device bound to a product.
 * @property deviceId
 * @type {String}
 */
/**
 * Subscription end time.The format is yyyyMMddHHmmss.
 * @property endtime
 * @type {String}
 */
/**
 * Subscription start time.The format is yyyyMMddHHmmss.
 * @property starttime
 * @type {String}
 */
/**
 * id of Product.
 * @property id
 * @type {String}
 */
/**
 * introduce of Product.
 * @property introduce
 * @type {String}
 */
/**
 * Indicates whether a product can be subscribed to online. The options are as follows:
 *
 * - 0: no
 * - 1: yes.
 *
 * @property isEnableOnlinePurchase
 * @type {String}
 */
/**
 * Indicates whether the package is a basic package.The options are as follows:
 *
 * - 0: non-basic package
 * - 1: basic package
 *
 * @property isMain
 * @type {String}
 */
/**
 * name of Product.
 * @property name
 * @type {String}
 */
/**
 * When the subscriber order the product.The format is yyyyMMddHHmmss.
 * @property ordertime
 * @type {String}
 */
/**
 * Bundle information. The parameter is contained when priceobjecttype is set to 4.
 * @property package
 * @type {String}
 */
/**
 * Bonus points required for subscribing to content.The parameter is valid when priceobjecttype is set to 2 and the content supports subscription by bonus point.
 * @property points
 * @type {String}
 */
/**
 * Number of IPTV bonus points.It is not included in the subscribed product list. If the value is -1, the product does not support subscription by bonus points.
 * @property point
 * @type {String}
 */
/**
 * Price of Product.
 * @property price
 * @type {String}
 */
/**
 * ID of a priced object.
 * @property priceobjectid
 * @type {String}
 */
/**
 * Type of a priced object.The options are as follows:
 *
 * - 1: service
 * - 2: content
 * - 3: node
 * - 4: bundle
 *
 * @property priceobjecttype
 * @type {String}
 */
/**
 * Rental period for times-based product.If the subscribable products are queried, the parameter is contained.Unit: hour.
 * @property rentPeriod
 * @type {String}
 */
/**
 * Restrictions of a product. Use ampersands (&) to separate multiple restrictions. The format is similar to [attribute name 1]=[attribute value 1]&[attribute name 2]=[attribute value 2].
 * @property restriction
 * @type {String}
 */
/**
 * Indicates whether a subscriber has subscribed the product. The options are as follows:
 *
 * - 0: no
 * - 1: yes
 *
 * @property subscriptionState
 * @type {String}
 */
/**
 * Product type.The options are as follows:
 *
 * - 1: times-based product
 * - 0: monthly package product
 *
 * @property type
 * @type {String}
 */
/**
 * Profile which has subscribed the product
 *
 * - 1: times-based product
 * - 0: monthly package product
 *
 * @property userIdentityId
 * @type {String}
 */
/**
 * The UserIdentity metadata stores the basic information about a profile, including ID, name, and password.
 * The Profile metadata stores the detailed information about a profile, including consumption quota and restricted channel list.
 * In the UserIdentity table, identityid identifies a profile.
 * @class eBase.data.UserIdentity
 */
var eBase = (function(core) {
    var data = core.data = core.data || {};

    data.UserIdentity = function(json) {
        /**
         * Profile ID.
         * @property identityid
         * @type {String}
         */
        /**
         * Profile name.
         * @property username
         * @type {String}
         */
        /**
         * Profile password, in encrypted text.
         * @property password
         * @type {String}
         */
        /**
         * Profile role.
         *
         * - 0: super profile
         * - 1: common profile
         * @property role
         * @type {String}
         */
        /**
         * Template.
         * @property template
         * @type {String}
         */
        /**
         * Language.
         * @property lang
         * @type {String}
         */
        /**
         * Avatar URL.
         * @property logo
         * type {String}
         */
        /**
         * Profile description.
         * @property description
         * type {String}
         */
        return json;
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var data = core.data = core.data || {};
    /**
     * Video on demond, including Movies, Series, Epesolides.
     * @class eBase.data.Vod
     */
    data.Vod = function(json) {
        return json;
    };
    /**
     * On-demand content ID
     * @property id
     * @type {String}
     */
    /**
     * Name of on-demand content. The name in the current language of the profile is returned.
     * @property name
     * @type {String}
     */
    /**
     * On-demand content type.
     *
     * - The options are as follows:
     * - AUDIO_VOD: audio on demand
     * - VIDEO_VOD: video on demand
     * - TELEPLAY_VOD: episode of a series
     * - CREDIT_VOD: ad content
     *
     * @property type
     * @type {String}
     */
    /**
     * Vod introduce.
     * @property introduce
     * @type {String}
     */
    /**
     * Vod mediafiles.
     * @property mediafiles
     * @type {Object[]}
     */
    /**
     * Vod rentperiod.Remaining playback duration of VOD content, that is, the interval between the current time and the expiration time of the subscription relationship for which the subscriber is authenticated.
     * @property rentperiod
     * @type {String}
     */
    /**
     * Vod ratingid.
     * @property ratingid
     * @type {String}
     */
    /**
     * Vod starttime.Format: yyyyMMddHHmmss
     * @property starttime
     * @type {String}
     */
    /**
     * Vod endtime.Format: yyyyMMddHHmmss
     * @property endtime
     * @type {String}
     */
    /**
     * Vod isfavorited.
     *
     * - 1: yes
     * - 0: no
     *
     * @property isfavorited
     * @type {String}
     */
    /**
     * Vod vodtype.Type of a VOD series.
     *
     * - 0: non-TV series
     * - 1: series
     * - 2: seasons
     *
     * @property vodtype
     * @type {String}
     */
    /**
     * Vod issubscribed.Indicates whether a VOD program has been subscribed to.
     *
     * - 1: yes
     * - 0: no
     *
     * @property issubscribed
     * @type {String}
     */
    /**
     * Vod foreignsn.Foreign key of VOD content.
     * @property foreignsn
     * @type {String}
     */
    /**
     * Vod statictimes.Total rating times.
     * @property statictimes
     * @type {String}
     */
    /**
     * Vod averagescore.Average rating of a VOD program.
     * @property averagescore
     * @type {String}
     */
    /**
     * Vod casts.Cast information of on-demand content.
     * @property casts
     * @type {String}
     */

    return core;
}(eBase));
/**
 * Hybrid Video MEM interface
 * @class eBase.iptv
 * @singleton
 */
var eBase = (function(core) {
    core.isOversea = function() {
        return true;
    };

    core.isDomestic = function() {
        return false;
    };

    return core;
}(eBase));
/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;

    /**
     * Obtain the information about the user account that is bound to the external accounts of all or specified social networking sites
     * @param {Object} req
     * @param {String} req.snsId the id of the social networking site
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be '',
     * - 411050001: Parameter [%s] format error
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050039: Tag[%s] not exist
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.Person[]} [return.list] List of accounts bound to a third-party social networking site.
     */
    social.queryBind = function(req) {
        var url = '/netrixsocial/account/@all';

        if (req && req.snsId) {
            url = utils.template('/netrixsocial/account/@all?snsId={snsId}', req);
        }

        return utils.socialAjax({
            url: url,
            type: 'GET'
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    /**
     * Bind a user to the account of the third-party social networking site.
     * @param {Object} req
     * @param {String} req.snsId  Id of the third-party social networking site.
     * @param {String} req.codeInfo Access token provided by the third party social.
     * @param {String} req.isSync Whether to synchronize the user information from the third party social.
     * - 0: no.
     * - 1: yes. Default value.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be '',
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050004: Not support this third party platform
     * - 411050005: Binding record already exist
     * - 411050006: Failed to access the third party network platform [%s]
     * - 411050013: Max bind account limit
     * - 411050029: Request authorization failed
     * - 411050050: Bind unknown error, please try again
     * - 411050099: System inner error:%s
     * - 0: Operation succeeded. Default value
     * @return {String} [return.description] Operation result description. For example, "Operation succeeded."
     * @return {eBase.social.data.Person} [return.data] Information about the account bound to a third-party social networking site.
     */
    social.createBind = function(req) {
        req.isSync = req.isSync || 1;

        return utils.socialAjax({
            url: utils.template('/netrixsocial/account/@self?isSync={isSync}', req),
            data: _.omit(req, 'isSync')
        });
    };

    /**
     * Unbind the internal account of the Social Platform from the account of the third-party social networking site.
     * @param {Object} req
     * @param {String} req.authId Currently logged in user.
     * @param {String} req.snsAccountId ocial Platform (user ID returned when the account is bound)
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be '',
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050004: Not support this third party platform
     * - 411050099: System inner error:%s
     * - 0: Operation succeeded. Default value
     * @return {String} [return.description] Operation result description. For example, "Operation succeeded."
     */
    social.deleteBind = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/account/{snsAccountId}/@self', req),
            type: 'delete'
        });
    };

    /**
     * Obtain the authentication URL of the third-party social networking site during the binding process.
     * @param {Object} req
     * @param {String} req.snsId Id of the social networking site.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be '',
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050004: Not support this third party platform
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Operation succeeded. Default value
     * @return {String} [return.description] Operation result description. For example, "Operation succeeded."
     * @return {String} [return.data] Authentication URL.
     * Example:
     * - OAuth 1.0(twitter):
     *   https://api.twitter.com/oauth/authorize?oauth_token=pjtGXfgQAIjND5RlbqQGK17zP0sq9GfXjV0rio9o8
     * - OAuth 2.0(facebook):
     *   https://graph.facebook.com/oauth/authorize?response_type=code&client_id=246822735413036&redirect_uri=http%3A%2F%2Fsocialtv.com%3A9800%2F
     * The parameter is returned to the client by the service application. The client accesses the authentication URL for authentication on the third-party social networking site.
     */
    social.getAuthURL = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/account/{snsId}/authURL/@self', req),
            type: 'get'
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;

    /**
     * This interface is to publish media content to internal system or target SNS platform.
     *
     * @alias {eBase.social.publishContent}
     * @param {eBase.social.data.Content} req Media content to be published.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050004: Not support this third party platform
     * - 411050010: Content %s not exist
     * - 411050017: Operation partial failed
     * - 411050018: Operation failed
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.ResultDetail[]} [return.list] Operation result details.
     */
    social.addContent = social.publishContent = function(req) {
        req.type = utils.convertToSocialType(req.type);

        return utils.socialAjax({
            url: '/netrixsocial/content/@self',
            type: 'post',
            data: req
        });
    };

    /**
     * This interface is to cancel published media content of internal system. (Content published to SNS won't be canceled.)
     *
     * @alias {eBase.social.cancelContent}
     * @param {Object} req
     * @param {String} req.contentIdList Content ids separated by ','.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.ResultDetail[]} [return.list] Operation result details.
     */
    social.deleteContent = social.cancelContent = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/@all?contentIdList={contentIdList}', req),
            type: 'delete'
        });
    };

    /**
     * This interface is to query media contents.
     *
     * @param {Object} req
     * @param {String} req.contentIdList Content ids separated by ','.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050010: Content %s not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.Content[]} [return.list] Content object.
     */
    social.queryContent = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/@all?contentIdList={contentIdList}', req),
            type: 'get'
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    /**
     * The interface is to create social action between user and content, including share, comment, like, dislike.
     *
     * @param {Object} req
     * @param {String} req.contentId Id of media content.
     * @param {String} req.action User action.
     *
     * - share
     * - like
     * - dislike
     * @param {String} [req.message] Message when sharing.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050010: Content %s not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {String} [return.data] Connection ID after the connection is created (feed ID or comment ID generated by the social action).
     */
    social.addAction = social.createAction = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/{contentId}/{action}/@self', req),
            data: _.pick(req, 'message'),
            type: 'post'
        });
    };

    /**
     * This interface is to query connection list of certain cretieal
     *
     * @param {Object} req
     * @param {String} req.contentIdList List of content id, separated by ','
     * @param {String} req.q Query string.
     * 'action:xxx,yyy;mode:xxx;scope:xxx;show:xxx'
     *
     * - action: social actions separated by ','
     * - mode: 0: count by user, 1 count by feed
     * - scope: all: All user, friend: only friends, me: myself
     * - show: 0: don't show detail, 1: show detail
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050045: UnSupport action type [%s]
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.Connection[]} [return.list]  Social relationships.
     */
    social.queryConnection = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/connection/@all?contentIdList={contentIdList}&q={q}', req),
            type: 'get'
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    /**
     * This interface is to delete social action.
     *
     * @param {Object} req
     * @param {String} req.contentId Id of media content.
     * @param {String} req.action Action User action.
     * @param {String} req.connectionId Id of connection.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050021:Operation forbidden
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     */
    social.deleteAction = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/{contentId}/{action}/@self?connectionId={connectionId}', req),
            type: 'delete'
        });
    };

    /**
     * This inteface is to query action list of media content.
     *
     * @param {Object} req
     * @param {String} req.action
     * @param {String} req.scope 'local'
     * @param {Number} req.startNum
     * @param {Number} req.length
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050010: Content %s not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return [Number] {return.totalCount} Total number of feeds in the query result.
     * @return {eBase.social.Feed[]} [return.list] Feed list, which is sorted by time. The feed list generated earliest ranks first.
     */
    social.queryAction = social.queryActivity = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/content/{contentId}/activity/@all', req),
            type: 'get',
            data: _.omit(req, 'contentId')
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;


    /**
     * Query the feeds of the internal friends or all friends (including the friends bound to the external account) of a specified user.
     *
     * @param {Object} req
     * @param {String} req.q Query string.
     * Search condition. Format: key:value.
     *  key: search scope. The options are as follows:
     *     鈭?action: social action. For details, see section A.5 "Social Action Type."
     *     鈭?link: social object type. For details, see section A.4 "Social Object Types."
     *  value: search value
     *  Only the internal friend feeds can be queried.
     * @param {String} req.scope Query scope.
     * - local: on the Social Platform. Default value is local.
     * @param {String} req.startId If this parameter is specified, the feed whose ID is smaller than or equal to the value of startId (social feed generated earlier than the social feed whose ID is the same as the value of startId) is returned. The default value is 0.
     * @param {String} req.endId If this parameter is specified, the feed whose ID is greater than the value of endId (social feed generated later than the social feed whose ID is the same as the value of endId) is returned. The default value is 0.
     * @param {Number} req.length Total number of friend feeds in the query result.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.Feed[]} [return.list] Friend feed list.
     */
    social.queryFriendFeed = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: '/netrixsocial/home/@all',
            type: 'get',
            data: req
        }).done(function(resp) {
            if (gConfig.isIPTV) {
                _.each(resp.data, function(item) {
                    var tempAvatarURL = item.from.avatarURL ? item.from.avatarURL : 'default.png';
                    tempAvatarURL = tempAvatarURL.indexOf('http') === 0 ? 'default.png' : tempAvatarURL;
                    item.avatarURL = tempAvatarURL;
                    //item.from.avatarURL = item.from.localAvatarURL;
                });
            } else {
                _.each(resp.data, function(item) {
                    // var _urlList = item.from.outerAvatars;
                    // var _tempUrl;
                    // for (var i = 0; i < _urlList.length; i++) {
                    //     if (_urlList[i].avatarURL !== '') {
                    //         _tempUrl = _urlList[i].avatarURL;
                    //     }
                    // }
                    //     item.avatarURL = _tempUrl ? _tempUrl : item.from.avatarURL ? item.from.avatarURL : 'default.png';
                    item.avatarURL = core.social.getSocialUrl(item.from);
                    //     //item.from.avatarURL = item.from.avatarURL ? item.from.avatarURL : item.from.localAvatarURL;
                });
            }
            utils.rename(resp, 'data', 'list');


        });
    };

    /**
     * Obtain all feed news of the specified user.
     *
     * @param {Object} req
     * @param {String} req.userId Unique ID of a user.
     * @param {String} req.q Query string.
     * Search criterion, which is in the format of key:value.
     *  key: search scope. An option is action, which indicates a social action. For details about social action types, see section A.5 "Social Action Type."
     *  value: search value
     * @param {String} req.startId If this parameter is specified, the feed whose ID is smaller than or equal to the value of startId (social feed generated earlier than the social feed whose ID is the same as the value of startId) is returned. The default value is 0.
     * @param {String} req.endId If this parameter is specified, the feed whose ID is greater than the value of endId (social feed generated later than the social feed whose ID is the same as the value of endId) is returned. The default value is 0.
     * @param {Number} req.length Total number of friend feeds in the query result.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.Feed[]} [return.list] Friend feed list.
     */
    social.queryUserFeed = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/{userId}/profile/@all', req),
            type: 'get',
            data: _.omit(req, 'userId')
        }).done(function(resp) {
            if (gConfig.isIPTV) {
                _.each(resp.data, function(item) {
                    var tempAvatarURL = item.from.avatarURL ? item.from.avatarURL : 'default.png';
                    tempAvatarURL = tempAvatarURL.indexOf('http') === 0 ? 'default.png' : tempAvatarURL;
                    item.avatarURL = tempAvatarURL;
                    //item.from.avatarURL = item.from.localAvatarURL;
                });
            } else {
                _.each(resp.data, function(item) {
                    // var _urlList = item.from.outerAvatars;
                    // var _tempUrl;
                    // for (var i = 0; i < _urlList.length; i++) {
                    //     if (_urlList[i].avatarURL !== '') {
                    //         _tempUrl = _urlList[i].avatarURL;
                    //     }
                    // }
                    // item.avatarURL = _tempUrl ? _tempUrl : item.from.avatarURL ? item.from.avatarURL : 'default.png';
                    item.avatarURL = core.social.getSocialUrl(item.from);
                    //item.from.avatarURL = item.from.avatarURL ? item.from.avatarURL : item.from.localAvatarURL;
                });
            }
            utils.rename(resp, 'data', 'list');
        });
    };
    return core;
}(eBase));

var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;
    social.getSocialUrl = function(str) {
        var icon = str.avatarURL || "default.png";
        if (str.outerAvatars && str.outerAvatars.length > 0) {
            var avatar = _.find(str.outerAvatars, function(obj) {
                if (obj.outerId && obj.outerId.indexOf('facebook') !== -1) {
                    return obj.avatarURL;
                } else {
                    return false;
                }
            }) || _.find(str.outerAvatars, function(obj) {
                if (obj.outerId && obj.outerId.indexOf('twitter') !== -1) {
                    return obj.avatarURL;
                } else {
                    return false;
                }
            });
            icon = avatar ? avatar.avatarURL : icon;
        }
        return icon;
    };

    return core;
}(eBase));

/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;
    /**
     * Add the friend of local social user.
     *
     * @param {Object} req
     * @param {String} req.userId User ID.
     * @param {String} req.friendId Friend ID.
     * @param {String} [req.message] the message send to the friend for request.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050016: Can't add self as friend
     * - 411050021: Operation forbidden
     * - 411050029: Request authorization failed
     * - 411050040: Friend relationship already exists
     * - 411050043: The auth user is not the same as the user
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     */
    social.addFriend = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/{userId}/friend/{friendId}/@self', req),
            data: {
                'message': req.message || ''
            }
        });
    };
    /**
     * Accept or deny the request of adding friend.
     *
     * @param {Object} req
     * @param {String} req.userId currently logged in user.
     * @param {String} req.friendId the friend id.
     * @param {String} req.status
     * - accept: accept
     * - deny: deny
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050008: Friendship %s not exit
     * - 411050016: Can't add self as friend
     * - 411050029: Request authorization failed
     * - 411050038: Friend request not exist
     * - 411050043: The auth user is not the same as the user
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded.
     */
    social.confirmFriend = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/{userId}/friend/{friendId}/status/@self', req),
            data: {
                'status': req.status
            },
            type: 'PUT'
        });
    };
    /**
     * Delete the friend of local social user.
     *
     * @param {Object} req
     * @param {String} req.userId User ID.
     * @param {String} req.friendId IDs to be deleted, separated by ','
     * Example:
     *        friendId1,friendId2,friendId3.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not existd
     * - 411050029: Request authorization failed
     * - 411050043: The auth user is not the same as the user
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded.
     */
    social.deleteFriend = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/{userId}/friend/@all?friendIdList={friendId}', req),
            type: 'DELETE'
        });
    };
    /**
     * Query user's friend list.
     *
     * @param {Object} req
     * @param {String} req.userId User ID.
     * @param {String} [req.scope] Query scope, could be 'local'.
     * @param {String} [req.status=active] Friend status:
     *
     * - pending_request: Request sent and waiting for confirmation
     * - pending_response: Request received and waiting to confirim
     * - active: Already friend
     * @param {String} [req.orderBy=default] Order type:
     *
     * - default: Sorted by ID
     * - activity: Sorted by activity
     * - time: Sorted by time
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not existd
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded.
     * @return {Number} [return.totalCount] Total number of friends.
     * @return {eBase.social.data.Person[]} [return.list] Friend list.
     */
    social.queryFriend = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/{userId}/friend/@all', req),
            type: 'GET',
            data: _.omit(req, 'userId')
        }).done(function(resp) {
            if (gConfig.isIPTV) {
                _.each(resp.data, function(item) {
                    var tempAvatarURL = item.avatarURL ? item.avatarURL : 'default.png';
                    tempAvatarURL = tempAvatarURL.indexOf('http') === 0 ? 'default.png' : tempAvatarURL;
                    item.avatarURL = tempAvatarURL;
                });
            } else {
                _.each(resp.data, function(item) {
                    //     var _urlList = item.outerAvatars;
                    //     var _tempUrl;
                    //     for (var i = 0; i < _urlList.length; i++) {
                    //         if (_urlList[i].avatarURL !== '') {
                    //             _tempUrl = _urlList[i].avatarURL;
                    //         }
                    //     }
                    //     item.avatarURL = _tempUrl ? _tempUrl : item.avatarURL ? item.avatarURL : 'default.png';
                    item.avatarURL = core.social.getSocialUrl(item);
                    //     //item.avatarURL = item.avatarURL ? item.avatarURL : item.localAvatarURL;
                });
            }
            utils.rename(resp, 'data', 'list');
        });
    };

    return core;
}(eBase));
/**
 * @class eBase.utils
 */
var eBase = (function(core) {
    var utils = core.utils,
        session = core.session,
        config = core.config;

    utils.socialAjax = function(req) {
        var retDfd = new Deferred();

        core.debug('social ajax request ' + JSON.stringify(req));
        utils.ajax({
            url: (req.socialUrl || config.socialUrl || '') + req.url,
            type: req.type || 'post',
            dataType: req.dataType || 'json',
            data: JSON.stringify(req.data) || null,
            headers: {
                'Authorization': 'Basic ' + utils.base64.encode(session.get('authId') + ',token=' + session.get('userToken')),
                'Content-Type': 'application/json',
                'Accept': 'application/json'
            }
        }).done(function(resp) {
            if (!_.isUndefined(resp.resultCode) && resp.resultCode !== '0') {
                /** core.event.trigger('SOCIAL_REQUEST_FAIL', {
                    req: req,
                    resp: resp
                });*/
                retDfd.reject(resp);
            } else {
                // Set data to [] for empty result.
                resp.data = resp.data || [];
                retDfd.resolve(resp);
            }
        }).fail(retDfd.reject).always(function(resp) {
            core.debug('social ajax response ' + JSON.stringify(resp));
            retDfd.reject(resp);
        });
        return retDfd;
    };


    return core;
}(eBase));
/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;
    /**
     * follow the tag by the local social user.
     * @param {Object} req
     * @param {Array} req.tag the text of tag.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050039: Tag[%s] not exist
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     */
    social.addFollow = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/tag/{tag}/follow/@self', req),
            type: 'post'
        });
    };

    /**
     * unfollow the tag.
     * @param {Object} req
     * @param {String} req.tag the text of the tag.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050039: Tag[%s] not exist
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     */
    social.deleteFollow = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/tag/{tag}/follow/@self', req),
            type: 'delete'
        });
    };

    /**
     * Query tag objects in the system, and obtain the number of social relationships that are associated with the tag objects.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050039: Tag[%s] not exist
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.data.Tag[]} [return.list] Tag object.
     */
    social.queryFollow = social.queryTag = function(req) {
        return utils.socialAjax({
            url: '/netrixsocial/tag/follow/@all',
            type: 'get',
            data: req
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var utils = core.utils;

    utils.convertToSocialType = function(type) {
        switch (type) {
            case 'vod':
                return 'movie';
            case 'playbill':
                return 'program';
            default:
                return type;
        }
    };

    return core;
})(eBase);
/**
 * @class eBase.social
 */
var eBase = (function(core) {
    var social = core.social = core.social || {},
        utils = core.utils;

    /**
     * Search sns users.
     *
     * @param {Object} req
     * @param {String} req.q Query string. like: 'name=SOL506'
     * @param {Number} req.startNum Start number of a specified search friend list.
     * @param {Number} req.length Total number of friends in the query result.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {Number} [return.totalCount] Total number of users.
     * @return {eBase.social.Person[]} [return.data] Detailed user information.
     */
    social.searchUser = function(req) {
        utils.paginate(req, 'social');

        return utils.socialAjax({
            url: utils.template('/netrixsocial/person/search/@all?q={q}', req),
            type: 'GET'
        }).done(function(resp) {
            utils.rename(resp, 'data', 'list');
        });
    };

    /**
     * Query the list of users that the system recommends.
     *
     * @param {Object} req
     * @param {String} req.q Recommendation rules.
     * - external_friend: external friends
     * @param {Number} req.startNumStart number of the list of the specified users. If the parameter value is not specified, the returned list starts from the first record.
     * @param {Number} req.length Total number of users in the query list.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     * @return {eBase.social.RecommendObject[]} [return.list] List of the recommended users. For this parameter, the value of obj in RecommendObject is Person.
     */
    social.queryRecommendUser = function(req) {
        console.log('--ebase--queryRecommendUser');
        return utils.socialAjax({
            url: '/netrixsocial/person/recommend/@all',
            type: 'get',
            data: req
        }).done(function(resp) {
            console.log('--ebase--queryRecommendUser  person--done--:' + resp.data);
            if (gConfig.isIPTV) {
                console.log('--ebase--queryRecommendUser  isiptv:');


                _.each(resp.data, function(item) {
                    var tempAvatarURL = item.obj.avatarURL ? item.obj.avatarURL : 'default.png';
                    tempAvatarURL = tempAvatarURL.indexOf('http') === 0 ? 'default.png' : tempAvatarURL;
                    item.avatarURL = tempAvatarURL;
                    //item.obj.avatarURL = item.obj.localAvatarURL;
                });
            } else {
                _.each(resp.data, function(item) {
                    //     var _urlList = item.obj.outerAvatars;
                    //     var _tempUrl;
                    //     for (var i = 0; i < _urlList.length; i++) {
                    //         if (_urlList[i].avatarURL !== '') {
                    //             _tempUrl = _urlList[i].avatarURL;
                    //         }
                    //     }
                    //     item.avatarURL = _tempUrl ? _tempUrl : item.obj.avatarURL ? item.obj.avatarURL : 'default.png';
                    item.avatarURL = core.social.getSocialUrl(item.obj);
                    //     //item.obj.avatarURL = item.obj.avatarURL ? item.obj.avatarURL : item.obj.localAvatarURL;
                });
            }
            utils.rename(resp, 'data', 'list');
        });
    };

    /**
     * Query the information about multiple users.
     *
     * @param {Object} req
     * @param {String} req.userIdList User Id, separated by ',' if query multiple users one time.
     * Example:
     *        userId1,userId2,userId3.
     * @param {Number} req.startNum Start number of a specified search friend list.
     * @param {Number} req.length Total number of friends in the query result.
     *
     * @return {eBase.core.Deferred}
     * @return {String} return.resultCode Operation result code, can't be left blank
     * - 411050001: Parameter [%s] format error
     * - 411050002: Access forbidden by strategy
     * - 411050003: User[%s] does not exist
     * - 411050029: Request authorization failed
     * - 411050099: System inner error:%s
     * - 0: Sucess, default value
     * @return {String} [return.description] Operation result description.For example, "Operation succeeded."
     */
    social.queryUser = function(req) {
        return utils.socialAjax({
            url: utils.template('/netrixsocial/person/@all?userIdList={userIdList}', req),
            type: 'get'
        }).done(function(resp) {
            if (gConfig.isIPTV) {
                _.each(resp.data, function(item) {
                    var tempAvatarURL = item.avatarURL ? item.avatarURL : 'default.png';
                    tempAvatarURL = tempAvatarURL.indexOf('http') === 0 ? 'default.png' : tempAvatarURL;
                    item.avatarURL = tempAvatarURL;
                    //item.avatarURL = item.localAvatarURL;
                });
            } else {
                _.each(resp.data, function(item) {
                    //     var _urlList = item.outerAvatars;
                    //     var _tempUrl;
                    //     for (var i = 0; i < _urlList.length; i++) {
                    //         if (_urlList[i].avatarURL !== '') {
                    //             _tempUrl = _urlList[i].avatarURL;
                    //         }
                    //     }
                    //     item.avatarURL = _tempUrl ? _tempUrl : item.avatarURL ? item.avatarURL : 'default.png';
                    item.avatarURL = core.social.getSocialUrl(item);
                });
            }
            utils.rename(resp, 'data', 'list');
        });
    };

    return core;
}(eBase));
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config;

    var queryDetail = function(resp, retDfd) {
        var vodIds = [],
            playbillIds = [],
            validList = [];

        _.each(resp.list, function(feed) {
            if (feed.link && feed.link.obj) {
                if (feed.link.obj.type === 'movie') {
                    vodIds.push(feed.link.obj.id);
                    validList.push(feed);
                } else {
                    playbillIds.push(feed.link.obj.id);
                    validList.push(feed);
                }
            }
        });

        if (validList.length === 0) {
            return retDfd.resolve({
                list: []
            });
        }

        core.iptv.queryDetail({
            vod: vodIds.join(','),
            playbill: playbillIds.join(','),
            idType: 1,
            filterType: 1,
            properties: [{
                name: 'vod',
                include: config.vodListProperties
            }, {
                name: 'playbill',
                include: config.playbillListProperties
            }]
        }).done(function(contentResp) {
            var vodList = contentResp.vodlist;
            var playbillList = contentResp.playbilllist;

            _.each(validList, function(feed) {
                if (feed.link.obj.type === 'movie') {
                    feed.content = _.find(vodList, function(vod) {
                        return vod.foreignsn === feed.link.obj.id;
                    });
                } else {
                    feed.content = _.find(playbillList, function(playbill) {
                        return playbill.foreignsn === feed.link.obj.id;
                    });
                }
            });
            retDfd.resolve({
                list: validList
            });
        }).fail(retDfd.reject);
    };

    /**
     * Social related business logic.
     * @class eBase.app.socialApp
     * @singleton
     */
    app.socialApp = function() {
        return {
            /**
             * Obtain all feed news of the specified user.
             *
             * @param {Object} req
             * @param {String} req.userId Unique ID of a user.
             * @param {String} req.q Query string.
             * Search criterion, which is in the format of key:value.
             *  key: search scope. An option is action, which indicates a social action. For details about social action types, see section A.5 "Social Action Type."
             *  value: search value
             * @param {String} req.startId If this parameter is specified, the feed whose ID is smaller than or equal to the value of startId (social feed generated earlier than the social feed whose ID is the same as the value of startId) is returned. The default value is 0.
             * @param {String} req.endId If this parameter is specified, the feed whose ID is greater than the value of endId (social feed generated later than the social feed whose ID is the same as the value of endId) is returned. The default value is 0.
             * @param {Number} req.length Total number of friend feeds in the query result.
             *
             * @return {eBase.core.Deferred}
             * @return return.list {eBase.data.Feed[]} with content of {eBase.data.Vod|eBase.data.Playbill}
             */
            queryUserFeed: function(req) {
                var retDfd = new Deferred();

                core.social.queryUserFeed(req).done(function(resp) {
                    queryDetail(resp, retDfd);
                }).fail(retDfd.reject);

                return retDfd;
            },

            queryFriendFeed: function(req) {
                var retDfd = new Deferred();

                core.social.queryFriendFeed(req).done(function(resp) {
                    queryDetail(resp, retDfd);
                }).fail(retDfd.rejct);

                return retDfd;
            }
        };
    }();

    return core;
}(eBase));
/**
 * Comment
 * @class eBase.social.data.Comment
 */
/**
 * id of the comment
 * @property id
 * @type {String}
 */
/**
 * Comment sponsor
 * @property from
 * @type {eBase.social.data.Person}
 */
/**
 * detail information of this comment
 * @property message
 * @type {String}
 */
/**
 * create time of this comment
 * @property created_time
 * @type {Date}
 */
/**
 * id of the third party server where this comment published
 * @property sns_source
 * @type {String}
 */
/**
 * Connection
 * @class eBase.social.data.Connection
 */
/**
 * Type of a target social object.
 * @property objType
 * @type {String}
 */
/**
 * Unique ID of a target social object.
 * @property objId
 * @type {String}
 */
/**
 * Social action type.
 * @property action
 * @type {String}
 */
/**
 * Total number of social relationship records in the query result
 * @property count
 * @type {Number}
 */
/**
 * Social object type.
 * @property type
 * @type {String}
 */
/**
 * List of social relationship records in the query result.
 * @property ids
 * @type {String[]}
 */
/**
 * Media content on social platform.
 * @class eBase.social.data.Content
 */
/**
 * Content id of media content.
 * @property id
 * @type {String}
 */
/**
 * Content type of media content.
 *
 * - photo
 * - video
 * - movie
 * - program
 * - article
 * @property type
 * @type {String}
 */
/**
 * Name of media content.
 * @property name
 * @type {String}
 */
/**
 * Url of media content.
 * @property url
 * @type {String}
 */
/**
 * Description of media content.
 * @property description
 * @type {String}
 */
/**
 * Attachments of media content, like icons.
 * @property attachs
 * @type {eBase.social.data.MediaItem[]}
 */
/**
 * Tags of media content, like genre, director, actor
 * @property tags
 * @type {String[]}
 */
/**
 * Activities of media content, like, dislike, share, comment.
 * @property connects
 * @type {eBase.social.data.Count[]}
 */
/**
 * List of sns platforms.
 * @property snsIds
 * @type {String[]}
 */
/**
 * Account id of sns when published.
 * @property snsAccountId
 * @type {String}
 */
/**
 * Expire date.
 * @property expire
 * @type {Date}
 */
/**
 * Count
 * @class eBase.social.data.Count
 */
/**
 * Specified social relationship. For example, like, dislike, share, friend, and follow.
 * @property name
 * @type {String}
 */
/**
 * Total number of feeds of a specified type.
 * @property count
 * @type {Number}
 */
/**
 * Feed
 * @class eBase.social.data.Feed
 */
/**
 * id of a feed
 * @property id
 * @type {String}
 */
/**
 * information of the user that trigger this feed
 * Notice: return nothing if the feed is generated by the system
 * @property from
 * @type {eBase.social.data.Person}
 */
/**
 * List of users that are associated with the social action.
 * @property to
 * @type {eBase.social.data.Person[]}
 */
/**
 * System description of the feed.
 * @property story
 * @type {String}
 */
/**
 * Social action that generates the feed
 * @property action
 * @type {String}
 */
/**
 * Time when the feed is generated.
 * @property created_time
 * @type {String}
 */
/**
 * Statistics on the number of social actions, including share, like, dislike, and comment.
 * @property connects
 * @type {eBase.social.data.Count[]}
 */
/**
 * Source (social networking site) of a feed, used as the unique ID of the social networking site on the Social Platform.
 * @property snsId
 * @type {String}
 */
/**
 * Object of the feed.
 * @property link
 * @type {eBase.social.data.SocialObject}
 */
/**
 * Description of the social action.
 * @property message
 * @type {String}
 */
/**
 * URL of the current feed resource, used to obtain the feed description.
 * @property url
 * @type {String}
 */
/**
 * Picture URL of a feed.
 * @property picture
 * @type {String}
 */
/**
 * FriendInfo
 * @class eBase.social.data.FriendInfo
 */
/**
 * Friendship status.
 * - pending_request: The request to add a friend has been sent, and is to be confirmed by the friend.
 * - pending_response: The request to add a friend has been received, and is to be confirmed by the user.
 * - active: The friendship has been created.
 * - null: No request to add a friend has been sent.
 * @property status
 * @type {String}
 */
/**
 * Flag of prohibiting a friend from speaking.
 * - 1: Feed information about the friend is not received.
 * - 0: Feed information about the friend is received. Default value.
 * @property isMute
 * @type {String}
 */
/**
 * Info of media item.
 * @class eBase.social.data.MediaItem
 */
/**
 * Name of media item.
 * @property name
 * @type {String}
 */
/**
 * Type of media item.
 * @property type
 * @type {String}
 */
/**
 * Url of media item.
 * @property url
 * @type {String}
 */
/**
 * Content of media item (encoded in Base64)
 * @property content
 * @type {String}
 */
/**
 * Person
 * @class eBase.social.data.Person
 */
/**
 * Id of a user account.
 * Notice:
 * - for an account on the Social Platform (internal account), the value is the user ID.
 * - for an account on the social networking site (external account), the value consists of snsId and extAccountId.
 *   snsId: Unique ID of the social networking site on the Social Platform.
 *   extAccountId: Account ID on the social networking site.
 * @property id
 * @type {String}
 */
/**
 * User's nickname.
 * @property name
 * @type {String}
 */
/**
 * Id of the third party networking site.
 * @property snsId
 * @type {String}
 */
/**
 * User's email.
 * @property email
 * @type {String}
 */
/**
 * User's sex
 * - Female
 * - Male
 * @property gender
 * @type {String}
 */
/**
 * User's birthday.
 * @property birthday
 * @type {String}
 */
/**
 * URL of a user's image.
 * @property avatarURL
 * @type {String}
 */
/**
 * city name of the userCity where a user is located.
 * @property location
 * @type {String}
 */
/**
 * Statistics on the number of times a user performs social actions, including share, friend, and follow.
 * @property connects
 * @type {eBase.social.data.Count[]}
 */
/**
 * Friendships between the request sender and the current user.
 * @property frdWithMe
 * @type {eBase.social.data.FriendInfo}
 */
/**
 * RecommendReason
 * @class eBase.social.data.RecommendReason
 */
/**
 * Reason of recommending a social relationship.
 * @property name
 * @type {String}
 */
/**
 * Recommendation weight.
 * @property score
 * @type {Number}
 */
/**
 * Type of the data elements of recommendation reason. For details, see section A.4 "Social Object Types."
 * @property type
 * @type {String}
 */
/**
 * Evidence for recommendation reason.
 * @property ids
 * @type {String[]}
 */
/**
 * RecommendObject
 * @class eBase.social.data.RecommendObject
 */
/**
 * Social object. The values of T include person, content, and tag.
 * @property obj
 * @type {eBase.social.data.Person|eBase.social.data.Content|eBase.social.data.Tag}
 */
/**
 * Recommendation reason.
 * @property reason
 * @type {eBase.social.data.RecommendReason[]}
 */
/**
 * ResultDetail
 * @class eBase.social.data.ResultDetail
 */
/**
 * Operation description, used to identify the object of a social action. For example, a certain social networking site or a certain friend.
 * @property target
 * @type {String}
 */
/**
 * Operation result code, value 0 indicates sucess. Can't be left blank
 * @property resultCode
 * @type {String}
 */
/**
 * Operation result description, can be left blank
 * @property description
 * @type {String}
 */
/**
 * Additional information, used to carry the operation results. For example, ID of the external comment or feed.
 * @property returnValue
 * @type {String}
 */
/**
 * binding list between user and third party social server
 * @class eBase.social.data.SnsInfo
 */
/**
 * id of the third party social server
 * @property snsId
 * @type {String}
 */
/**
 * http address of the third party social server
 * @property snsUrl
 * @type {String}
 */
/**
 * binding list between user and third party social server
 * @class eBase.social.data.SocialObject
 */
/**
 * Type of a social object.
 * @property type
 * @type {String}
 */
/**
 * Unique ID of a social object. For example, the user ID.
 * @property id
 * @type {String}
 */
/**
 * Social object.
 * @property obj
 * @type {eBase.social.data.Person|eBase.social.data.Content|eBase.social.data.Tag}
 */
/**
 * Tag
 * @class eBase.social.data.Tag
 */
/**
 * Tag description.
 * @property text
 * @type {String}
 */
/**
 * Tag description.
 * @property connects
 * @type {eBase.social.data.Count[]}
 */
/**
 * Hybrid Video Social interface
 * @class eBase.social
 * @singleton
 */
var eBase = (function(core) {
    var config = core.config;

    core.event.on('CORE_PROFILE_LOGIN_SUCCESS', function(event) {
        // Save social url
        _.each(event.data.list, function(configuration) {
            _.each(configuration.extensionInfo, function(extension) {
                if (extension.key === 'SocialUrl') {
                    config.socialUrl = extension.value;
                }
            });
        });
        // Refresh token
        core.iptv.queryProfile({
            type: 0
        }).done(function(resp) {
            core.session.put('authId', resp.list[0].id);
            core.session.put('userToken', resp.userToken);
            core.event.trigger('CORE_QUERY_PROFILE_INFO_SUCCESS');
        });
    });

    return core;
}(eBase));
/**
 * @class eBase.dropbox
 */
var eBase = (function(core) {
    var dropbox = core.dropbox = core.dropbox || {},
        config = core.config,
        utils = core.utils;

    var TYPE_MAP = {
        0: 'folder',
        1: 'image',
        2: 'video',
        3: 'audio',
        4: 'other'
    };

    dropbox.authorize = function(req) {
        return utils.ajax({
            url: config.mashupUrl + '/mGear/OauthDropBoxAPIs',
            type: 'post',
            data: utils.jsonToXml({
                RequestParams: req
            })
        });
    };

    dropbox.getFileList = function(req) {
        return utils.ajax({
            url: config.mashupUrl + '/mGear/GetMetadataWithChildren',
            type: 'post',
            data: utils.jsonToXml({
                RequestParams: req
            })
        }).done(function(resp) {
            resp.list = _.map(resp.childList, function(file) {
                return _.extend(file, {
                    type: TYPE_MAP[file.contentType]
                });
            });
            delete resp.childList;
        });
    };

    dropbox.getThumbnail = function(req) {
        return utils.ajax({
            url: config.mashupUrl + '/mGear/GetThumbnail',
            type: 'post',
            data: utils.jsonToXml({
                RequestParams: req
            })
        });
    };

    dropbox.getPlayUrl = function(req) {
        return utils.ajax({
            url: config.mashupUrl + '/mGear/CreateTemporaryDirectUrl',
            type: 'post',
            data: utils.jsonToXml({
                RequestParams: req
            })
        });
    };

    return core;
}(eBase));
/**
 * DropboxApp
 * Browser content in Dropbox
 * @class eBase.app.dropboxApp
 * @singleton
 */
var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        event = core.event,
        profileId,
        token;

    core.appReadyTrigger.register('APP_DROPBOX_DATA_READY');

    event.on('APP_PROFILE_DATA_READY', function() {
        var customConfig = core.app.profileApp.customConfig;

        if (!_.isEmpty(customConfig.MashupUrl)) {
            config.mashupUrl = customConfig.MashupUrl;
        }

        profileId = core.app.profileApp.currentProfile.id;
        token = null;

        core.iptv.querySubscriberEx().done(function(resp) {
            var tokenConfig = _.find(resp.list, function(config) {
                return config.key === 'DROPBOX_ACCESSTOKEN';
            });

            token = tokenConfig ? tokenConfig.value : '';
        }).always(function() {
            event.trigger('APP_DROPBOX_DATA_READY');
            console.log('dropbox ready');
        });
    });

    event.on('CORE_ERROR_MESSAGE', function(msg) {
        gEventManager.trigger('CORE_ERROR_MESSAGE_FOR_ERRORMANAGER', msg);
        if (msg.data.code === 'dropbox_token_expire') {
            app.dropboxApp.unbind();
        }
    });

    app.dropboxApp = function() {
        return {
            /**
             * Is user authorized by Dropbox
             *
             * @return {eBase.core.Deferred}
             */
            isAuthorized: function() {
                var retDfd = new Deferred();

                if (token) {
                    retDfd.resolve();
                } else {
                    core.iptv.querySubscriberEx().done(function(resp) {
                        var tokenConfig = _.find(resp.list, function(config) {
                            return config.key === 'DROPBOX_ACCESSTOKEN';
                        });

                        token = tokenConfig ? tokenConfig.value : '';
                        if (!_.isEmpty(token)) {
                            retDfd.resolve();
                        } else {
                            retDfd.reject();
                        }
                    }).fail(retDfd.reject);
                }

                return retDfd;
            },
            /**
             * Unbind current user with Dropbox.
             *
             * @return {eBase.core.Deferred}
             */
            unbind: function() {
                token = null;

                return core.iptv.updateSubscriberEx({
                    extensionInfo: [{
                        key: 'DROPBOX_ACCESSTOKEN',
                        value: ''
                    }]
                });
            },
            /**
             * OAuth user to Dropbox
             * @param {Object} req
             * @param {String} req.type 'email' or 'barcode'
             * @param {String} [req.email] user's email address
             * @return {eBase.core.Deferred} resp
             * @return {String} resp.retcode
             *
             * - 0: Success
             * - 1: Email Fail
             * - 2: Barcode Fail
             * - 3: Other Fail
             * @return {String} [resp.picStream] Barcode in BASE64 image
             */
            authorize: function(_req) {
                var req = {
                    methodName: 'oauthRequest',
                    // JSON format
                    format: 0,
                    profileId: profileId,
                    email: _req.email || '',
                    sendType: _req.type === 'email' ? 1 : 2
                };

                return core.dropbox.authorize(req);
            },
            /**
             * File list for path.
             * @param {Object} req
             * @param {String} req.path Directory path.
             * @param {String} req.type
             *
             * - 0: All
             * - 1: Image & Video
             * - 2: Image Only
             * - 3: Video Only
             * @return {eBase.core.Deferred} resp
             * @return {Array} resp.list List of files
             *
             * - 0: Folder
             * - 1: Image
             * - 2: Video
             * - 3: Audio
             * - 4: Other
             */
            getFileList: function(_req) {
                var req = {
                    methodName: 'getMetadataWithChildren',
                    // JSON format
                    format: 0,
                    path: _req.path,
                    accesstoken: token,
                    contentType: _req.type || 0,
                    beginIndex: (_req.offset || 0) + 1,
                    number: _req.count || 1000
                };

                return core.dropbox.getFileList(req);
            },
            /**
             * Thumbnail of Image
             *
             * @param {Object} req
             * @param {Number} req.size
             *
             * - 1锛?2x32 pixels
             * - 2锛?4x64 pixels
             * - 3锛?28x128 pixels
             * - 4锛?40x480 pixels
             * - 5锛?024x768 pixels
             * @return {eBase.core.Deferred} resp
             * @return {String} resp.get('picStream') thumbnail in BASE64
             */
            getThumbnail: function(_req) {
                var req = {
                    methodName: 'getThumbnail',
                    // JSON format
                    format: 0,
                    path: _req.path,
                    accesstoken: token,
                    // TODO: Add Size Filter
                    size: _req.size || 1
                };

                return core.dropbox.getThumbnail(req);
            },
            /**
             * Play url for Video or Image
             *
             * @param {Object} req
             * @param {String} req.path Url of file
             *
             * @return {eBase.core.Deferred}
             * @return {String} return.url Play URL.
             */
            getPlayUrl: function(_req) {
                var req = {
                    methodName: 'createTemporaryDirectUrl',
                    // JSON format
                    format: 0,
                    path: _req.path,
                    accesstoken: token
                };

                return core.dropbox.getPlayUrl(req);
            }
        };
    }();

    return core;
}(eBase));
/**
 * eBase's DropBox extension
 *
 * @class eBase.dropbox
 * @singleton
 */
/**
 * @class eBase.rice
 */
var eBase = (function(core) {
    var rice = core.rice = core.rice || {},
        config = core.config,
        utils = core.utils;

    rice.getMediaInfo = function(req) {
        return utils.ajax({
            url: config.riceUrl + '/MCP/api/mapping/' + req.id,
            type: 'post'
        });
    };

    rice.queryFeed = function(req) {
        var url = config.riceUrl + '/MCP/api/mapping/' + req.id + '/' + req.type;

        if (req.count) {
            url += '?page=' + (req.offset + 1) + '&pagesize=' + req.count;
        }

        return utils.ajax({
            url: url,
            type: 'get'
        }).done(function(resp) {
            utils.rename(resp, 'records', 'list');
        });
    };

    return core;
}(eBase));
/**
 * DropboxApp
 * Browse content in Dropbox
 * @class eBase.app.dropboxApp
 * @singleton
 */
var eBase = (function(core) {
    var app = core.app = core.app || {},
        rice = core.rice,
        config = core.config,
        event = core.event;

    core.appReadyTrigger.register('APP_RICE_DATA_READY');

    event.on('APP_PROFILE_DATA_READY', function() {
        var customConfig = core.app.profileApp.customConfig;

        if (!_.isEmpty(customConfig.RiceUrl)) {
            config.riceUrl = customConfig.RiceUrl;
        }

        event.trigger('APP_RICE_DATA_READY');
        console.log('rice ready');
    });

    app.riceApp = function() {
        return {
            /**
             * Get extended info of media.
             *
             *
             * @param {Object} req
             * @param {String} req.id Media ID
             * @return {eBase.core.Deferred}
             *
             * <pre><code>
             *     // extended score
             *     resp.list[0].userRating
             *     // extended posters
             *     resp.list[0].scenes
             *     // extended trailers
             *     resp.list[0].videos
             * </code></pre>
             */
            getMediaInfo: function(req) {
                return rice.getMediaInfo(req);
            },

            /**
             * Get reviews of media.
             *
             * @param {Object} req
             * @param {String} req.id Media ID
             * @param {Number} [req.offset] Record index offset, which starts from 0.
             * @param {Number} [req.count] Number of returned records.
             * @return {eBase.core.Deferred}
             */
            queryReview: function(req) {
                return rice.queryFeed(_.extend(req, {
                    type: 'reviews'
                }));
            },

            /**
             * Get comments of media.
             *
             * @param {Object} req
             * @param {String} req.id Media ID
             * @param {Number} [req.offset] Record index offset, which starts from 0.
             * @param {Number} [req.count] Number of returned records.
             * @return {eBase.core.Deferred}
             */
            queryComment: function(req) {
                return rice.queryFeed(_.extend(req, {
                    type: 'comments'
                }));
            }
        };
    }();

    return core;
}(eBase));
/**
 * eBase's RICE extension
 *
 * @class eBase.rice
 * @singleton
 */
/**
 * @class eBase.stb
 */
var eBase = (function(core) {
    var stb = core.stb = core.stb || {};

    /**
     * Get details of the virtual key events.
     *
     * @return {eBase.data.STBEvent}
     */
    stb.getEvent = function() {
        var eventStr = Utility.getEvent() + '';

        core.debug('Utility.getEvent() <-- ' + eventStr);

        try {
            return JSON.parse(eventStr);
        } catch (ex) {
            core.error('[STBService] JSON.parse error: ' + ex.toString());
        }

        return null;
    };

    return core;
}(eBase));
/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;
    /**
     * Query lists of devices bound to subscribers' accounts.
     * @param {Object} req
     * @param {String} req.categoryId Category ID, which can be used to query VAS lists from this category. A category can be a leaf category or a non-leaf category. If the category is a non-leaf category, query VAS content in all leaf categories.
     * @param {Number} req.count Total VAS content items obtained for one query. If all content items are obtained, the value is -1.
     * @param {Number} req.offset Start position for the query. The value starts from 0, indicating the first query.
     * @param {Number} [req.orderType] Sorting mode of VAS content items.
     *
     * - 0: by click-through rate in descending order
     * - 1: by release time in descending order
     * If the parameter is not specified, VAS content items are sorted by default mode. If a category is a root category, sort VAS content items by content ID in descending order. If a category is not a root category, sort VAS content items by the content sorting mode in the category.
     *
     * @return {eBase.core.Deferred}
     * @return {Array} return.list List of {eBase.data.Vas}
     */
    iptv.queryVas = function(req) {
        utils.rename(req, 'categoryId', 'categoryid');
        utils.paginate(req);

        return utils.jsonAjax({
            url: '/EPG/JSON/VasList',
            data: JSON.stringify(req)
        }).done(function(resp) {
            utils.rename(resp, 'vaslist', 'list');
        });
    };

    return core;
}(eBase));

/**
 * @class eBase.iptv
 */
var eBase = (function(core) {
    var iptv = core.iptv = core.iptv || {},
        utils = core.utils;

    iptv.GetHotelUIInfo = function(req) {

        return utils.jsonAjax({
            url: '/EPG/JSON/GetHotelUIInfo',
            data: JSON.stringify(req)
        }).done(function(resp) {

        });
    };

    return core;
}(eBase));
/**
 * @class eBase.stb
 */
var eBase = (function(core) {
    var stb = core.stb = core.stb || {};

    /**
     * Get the value of parameter according to the key
     *
     * @param {String} key The key of the parameter.
     *
     * @return {String} value The value of the parameter.
     * - '':  when fail to get the value return ''
     */
    stb.read = function(key) {
        var ret = Utility.getValueByName(key) || '';

        return ret;
    };

    return core;
}(eBase));
/**
 * @class eBase.stb
 */
var eBase = (function(core) {
    var stb = core.stb = core.stb || {};

    /**
     * Set the value of parameter according to the key
     *
     * @param {String} key The key of the parameter.
     * @param {String} value The key of the parameter.
     *
     * @return {String} retString
     *  - 0: success to set the value
     *  - other number: fail to set the value
     */
    stb.write = function(key, value) {
        var ret = Utility.setValueByName(key, value + '');

        return ret;
    };

    return core;
}(eBase));

var eBase = (function(core) {
    var app = core.app = core.app || {},
        event = core.event;

    core.appReadyTrigger.register('APP_MOSAIC_DATA_READY');

    //Query channel when the profile login.
    /***/
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function() {
        app.mosaicApp.refreshMosaic().always(function() {
            event.trigger('APP_MOSAIC_DATA_READY');
            eBase.session.put('isFirstLoginSuccess',true,false);
            console.log('mosaic ready');
        });
    });
    /***/

    /**
     * Mosaic channel infomation
     * @class eBase.app.mosaicApp
     * @singleton
     */
    app.mosaicApp = function() {
        return {
            mosaicList: [],
            /**
             * Query mosaic channel list.
             *
             * @return {eBase.core.Deferred}
             * @return {eBase.data.MosaicChannel[]} return.list
             */
            queryMosaic: function() {
                return new Deferred().resolve({
                    list: this.mosaicList
                });
            },
            /**
             * Return mosaic channel info.
             *
             * @param {Object} req
             * @param {String} req.channo Channel number
             *
             * @return {eBase.data.MosaicChannel}
             */
            getMosaicInfo: function(req) {
                if (this.mosaicList.length === 0) {
                    return;
                }

                return _.find(this.mosaicList, function(mosaic) {
                    return mosaic.bgChannelNo === req.channo;
                });
            },
            /**
             * Refresh cache.
             * @private
             */
            refreshMosaic: function() {
                var self = this;

                return core.iptv.queryMosaic().done(function(resp) {
                    self.mosaicList = resp.list;
                });
            }
        };
    }();

    return core;
}(eBase));


var eBase = (function(core) {
    var app = core.app = core.app || {},
        config = core.config,
        event = core.event;

    var ppvChannelList = [];
    var subscribedChanIds = [];
    var DEFAULT_PROPERTIES = ['id', 'type', 'name', 'channo', 'issubscribed', 'pltvsubscribed'].join(',');

    core.appReadyTrigger.register('APP_CHANNEL_DATA_READY');

    //Query channel when the profile login.
    event.on('CORE_PROFILE_LOGIN_SUCCESS', function() {
        console.debug("lixinying rereshChannel start ");
        /**开机缓存blackoutPeriod信息*/
        app.channelApp.refreshBlackoutPeriod().always(function() {
            console.debug("zangjing blackoutPeriod done ");
            /**event.trigger('APP_BLACKOUTPERIOD_DATA_READY');*/
        });
        app.channelApp.refreshChannel().always(function() {
            console.log('channel  ready');
            console.debug("lixinying rereshChannel done ");
            event.trigger('APP_CHANNEL_DATA_READY');
            /**var channelContentVideo = gUtils.getConfiguration({
                key: 'ChannelCategoryID'
            });
            channelContentVideo = (channelContentVideo && gConfig.isIPTV) ? channelContentVideo : '-1';
            var request = {
                categoryid: channelContentVideo,
                offset: 0,
                count: 5
            };

            app.channelApp.queryChannel(request).done(function(resp) {
                var channelList = resp.list || [];
                var date = new Date();
                var i = 0;
                var callback = function(channelid) {
                    eBase.app.playbillApp.queryPlaybillList({
                        channelId: channelid,
                        date: date,
                        type: 2,
                        isFillProgram: '1'
                    }).done(function() {
                        i++;
                        if (i < 5) {
                            callback(channelList[i].id);
                        }
                    });
                };
                channelList[i] && callback(channelList[i].id);
            });*/
        });

        app.ppvApp.queryPPVCategory();
    });

    event.on('CORE_BLACKOUT_VERSION_CHANGED', function() {
        app.channelApp.refreshBlackoutPeriod().always(function() {
            event.trigger('APP_CHANNEL_BLACKOUT_READY');
            gEventManager.trigger('APP_CHANNEL_BLACKOUT_READY');
        });
    });

    event.on('CORE_CHANNEL_VERSION_CHANGED', function(obj) {
        app.channelApp.refreshChannel().done(function() {
            if (obj.data) {
                app.heartbeatApp.setChannelVersion(obj.data.version);
            }
            //频道版本号变化 在获取节目版本号之前，过滤掉不存在的频道号
            app.playbillApp.refreshPlayBill(true);
        }).always(function() {
            event.trigger('APP_CHANNEL_SUBSCRIBE_READY');
            gEventManager.trigger('APP_CHANNEL_SUBSCRIBE_READY');
        });
    });

    event.on('CORE_SUBSCRIBE_VERSION_CHANGED', function() {
        app.channelApp.refreshChannel().always(function() {
            event.trigger('APP_CHANNEL_SUBSCRIBE_READY');
            // app.playbillApp.refreshPlayBill(true);
            gEventManager.trigger('APP_CHANNEL_SUBSCRIBE_READY');
        });
    });

    // TODO: NEED TEST HERE!!!
    event.on('CORE_SUBSCRIBE_SUCCESS', function() {
        ppvChannelList = [];
        if (subscribedChanIds.length === 0 && typeof subscribedChanIds !== 'string') {
            return;
        }
        /**return new Deferred.when(core.iptv.querySubscribedContent({
            contenttype: 'CHANNEL,AUDIO_CHANNEL,VIDEO_CHANNEL',
            productType: 1
        }), core.iptv.allChannel({
            properties: config.channelListProperties
        })).done(function(presp, cresp) {
            ppvChannelList = presp.list;
            _.each(cresp.list, function(channel, index) {
                app.channelApp.channelList[index].issubscribed = channel.issubscribed;
                app.channelApp.channelList[index].restrictions = channel.restrictions;
                app.channelApp.channelList[index].inConditions = channel.inConditions;
                app.channelApp.channelList[index].subscribeType = channel.subscribeType;
                app.channelApp.channelList[index].tvodsubscribed = channel.tvodsubscribed;
                app.channelApp.channelList[index].pltvsubscribed = channel.pltvsubscribed;
            });
            event.trigger('APP_CHANNEL_SUBSCRIBE_READY');
            //gEventManager.trigger('APP_CHANNEL_SUBSCRIBE_READY');
        });*/

        return new Deferred.when(core.iptv.querySubscribedContent({
            contenttype: 'CHANNEL,AUDIO_CHANNEL,VIDEO_CHANNEL',
            productType: 1
        }), core.iptv.queryDetail({
            properties: [{
                name: 'channel',
                include: 'id,issubscribed,restrictions,inConditions,subscribeType,tvodsubscribed,pltvsubscribed'
            }],
            channel: _.pluck(subscribedChanIds, 'contentid').join(','),
            filterType: 1
        })).done(function(presp, cresp) {
            ppvChannelList = presp.list;
            var tmpChanLen = app.channelApp.channelList.length;
            for (var i = 0; i < cresp.channellist.length; i++) {
                for (var k = 0; k < tmpChanLen; k++) {
                    if (app.channelApp.channelList[k].id === cresp.channellist[i].id) {
                        app.channelApp.channelList[k].issubscribed = cresp.channellist[i].issubscribed;
                        app.channelApp.channelList[k].restrictions = cresp.channellist[i].restrictions;
                        app.channelApp.channelList[k].inConditions = cresp.channellist[i].inConditions;
                        app.channelApp.channelList[k].subscribeType = cresp.channellist[i].subscribeType;
                        app.channelApp.channelList[k].tvodsubscribed = cresp.channellist[i].tvodsubscribed;
                        app.channelApp.channelList[k].pltvsubscribed = cresp.channellist[i].pltvsubscribed;
                        continue;
                    }
                }
            }
            /**只刷新需要订购的频道的订购关系，刷新完之后重置subscribedChanIds*/
            event.trigger('APP_CHANNEL_SUBSCRIBE_READY', {programId: subscribedChanIds});
            subscribedChanIds = [];
            //gEventManager.trigger('APP_CHANNEL_SUBSCRIBE_READY');
        });
    });

    var categoryChannellist = {};
    var cacheChannelCategorys = [];

    /**
     * Channel related business logic.
     * @class eBase.app.channelApp
     * @singleton
     */
    var self = app.channelApp = function() {
        return {
            /**
             * Get channel by channel id.
             * @param {String} id Channel id.
             *
             * @return {eBase.data.Channel}
             */
            getChannelById: function(id) {
                return _.find(self.channelList, function(channel) {
                    return channel.id === id;
                });
            },
            refreshChannelSubscribed: function(channel) {
                var retDfd = new Deferred();
                ppvChannelList = [];
                core.iptv.querySubscribedContent({
                    contenttype: 'CHANNEL,AUDIO_CHANNEL,VIDEO_CHANNEL',
                    productType: 1
                }).done(function(presp) {
                    ppvChannelList = presp.list;
                    core.iptv.allChannel({
                        properties: config.channelListProperties
                    }).done(function(cresp) {
                        _.each(cresp.list, function(channel, index) {
                            self.channelList[index].issubscribed = channel.issubscribed;
                            self.channelList[index].restrictions = channel.restrictions;
                            self.channelList[index].inConditions = channel.inConditions;
                            self.channelList[index].subscribeType = channel.subscribeType;
                            self.channelList[index].tvodsubscribed = channel.tvodsubscribed;
                            self.channelList[index].pltvsubscribed = channel.pltvsubscribed;
                        });
                        event.trigger('APP_CHANNEL_SUBSCRIBE_READY');
                        retDfd.resolve();
                    });
                });


                return retDfd;
            },
            getChannelByContentdetail: function(req) {
                ///aaaaaaaa
                var retDfd = new Deferred();

                //utils.rename(req, 'id', 'channel');
                //injectProperties(req, 'detail');
                core.iptv.queryDetail(req).done(function(resp) {
                    var channel = resp.channellist;
                    retDfd.resolve({
                        list: channel
                    });
                }).fail(retDfd.reject);

                return retDfd;
            },

            refreshBlackoutPeriod: function() {
                self.blackoutPeriod = [];
                return core.iptv.queryBlackoutPeriod().done(function(resp) {
                    self.blackoutPeriod = resp.blackoutPeriods;
                });
            },

            queryBlackoutPeriod: function() {
                return self.blackoutPeriod;
            },

            /**
             * Get channel by channel no.
             * @param {String} no Channel number.
             *
             * @return {eBase.data.Channel}
             */
            getChannelByNo: function(no) {
                /* DTS2018061413514 使用channelup/down切换到添加录制的频道,导致NPVR录制无法播放*/
                console.log('==test======ebase core stb===no==' + no);
                return _.find(self.channelList, function(channel) {
                    console.log('==test======ebase core stb===channel.channo==' + channel.channo);
                    return parseInt(channel.channo) === parseInt(no);
                });
            },
            /**
             * This interface is to query channel list.
             *
             * @param {Object} req
             * @param {String} [req.type=all]
             *
             * - all: Return all channels
             * - favorited: return favorited channels
             * - category: return channels under category
             * @param {String} [req.categoryId] Channel category id.
             * @param {String} [req.properties] Fields in returned {eBase.data.Channel}. Ex. 'id,type,name'
             *
             * @return {eBase.core.Deferred}
             * @return {eBase.data.Channel[]} return.list List of channel.
             */
            queryAllChannel: function(req) {
                var list = [];

                if (req.count) {
                    list = (self.channelList || []).slice(req.offset, req.offset + req.count);
                } else {
                    list = self.channelList || [];
                }

                return new Deferred().resolve({
                    list: list
                });
            },
            queryChannel: function(req) {
                req = req || {};
                req.type = req.type || 'all';

                switch (req.type) {
                    case 'all':
                        return self.queryAllChannel(req);
                    case 'category':
                        req.properties = config.channelListProperties;
                        if (categoryChannellist && categoryChannellist[req.categoryid] && categoryChannellist[req.categoryid].counttotal > 0) {
                            var _tmpList = categoryChannellist[req.categoryid].list;
                            if (req.count > 0 && req.offset >= 0) {
                                _tmpList = [];
                                for (var i = req.offset; i < req.offset + req.count; i++) {
                                    if (categoryChannellist[req.categoryid].list[i]) {
                                        _tmpList.push(categoryChannellist[req.categoryid].list[i]);
                                    }
                                }
                            }
                            var list = [];
                            if (req.filterlist && req.filterlist[0].key === 'SubscriptionType' && req.filterlist[0].value === '2') {
                                var _tmpPpvChanlist = _.map(_tmpList, function(channel) {
                                    return self.getChannelById(channel.id);
                                });
                                list = _.filter(_tmpPpvChanlist, function(channel) {
                                    return channel.issubscribed !== '1';
                                });
                                categoryChannellist[req.categoryid].counttotal = list.length;
                            } else {
                                list = _.map(_tmpList, function(channel) {
                                    return self.getChannelById(channel.id);
                                });
                            }

                            return new Deferred().resolve({
                                counttotal: categoryChannellist[req.categoryid].counttotal,
                                list: list
                            });
                        } else {
                            var isHasChanCategoryId = _.find(categoryChannellist, function(_category) {
                                return req.categoryid === _category.id && _category.counttotal === _category.list.length;
                            });
                            req.count = (req.count && req.count > 0 && isHasChanCategoryId) ? req.count : -1;
                            req.offset = (req.offset && req.offset > 0 && isHasChanCategoryId) ? req.offset : 0;

                            return core.iptv.queryChannel(req).done(function(resp) {
                            // 修改问题单DTS2017011107316--后台操作UMS，如混排频道（添加或删除一个频道），不等心跳，Guide页面按Filter选择HBO过滤，右下角页码显示有误，上下翻页异常
                                var clist = [];
                                var channel;
                                _.each(resp.list, function(item) {
                                    channel = self.getChannelById(item.id);
                                    if (channel) {
                                        clist.push(channel);
                                    }
                                });
                                resp.list = clist;
                                if (req.count === -1) {
                                    resp.counttotal = clist.length;
                                }
                                categoryChannellist[req.categoryid] = resp;
                            });
                        }

                    case 'favorited':
                        return new Deferred().resolve({
                            list: _.filter(self.channelList, function(channel) {
                                return app.favoriteApp.isFavorited(channel);
                            })
                        });
                    default:
                        break;
                }
            },

            refreshChannel: function() {
                /**
                 *  DTS2016031007467 接口返回成功后再刷新
                 */
                return new Deferred.when(core.iptv.allChannel({
                    properties: config.channelListProperties || DEFAULT_PROPERTIES
                }), core.iptv.querySubscribedContent({
                    contenttype: 'CHANNEL,AUDIO_CHANNEL,VIDEO_CHANNEL',
                    productType: 1
                })).done(function(cresp, presp) {
                    categoryChannellist = {};
                    self.channelList = [];
                    ppvChannelList = [];
                    self.channelList = cresp.list;
                    ppvChannelList = presp.list;

                    // 重新获取PPV栏目内容
                    core.app.ppvApp.queryPPVCategory();
                });
            },

            getPpvChannelList: function() {
                return ppvChannelList;
            },

            getSubscribedChanIds: function() {
                return subscribedChanIds;
            },

            setSubscribedChanIds: function(tmpChanIds) {
                subscribedChanIds = tmpChanIds;
            },

            getChannelSortDetail: function(req) {
                var channelList = _.filter(self.channelList, function(channel) {
                    return _.indexOf(req.ids, channel.id) >= 0;
                });
                var channelList1 = [];
                for (var i = 0; i < req.ids.length; i++) {
                    for (var j = 0; j < channelList.length; j++)
                        if (req.ids[i] === channelList[j].id) {
                            channelList1.push(channelList[j]);
                        }
                }
                return new Deferred().resolve({
                    list: channelList1
                });
            },
            getChannelDetail: function(req) {
                var channelList = _.filter(self.channelList, function(channel) {
                    return _.indexOf(req.ids, channel.id) >= 0;
                });
                return new Deferred().resolve({
                    list: channelList
                });
            },

            findChannelListByCategoryId: function(req) {
                return categoryChannellist[req.categoryid];
            }
        };
    }();

    return core;
}(eBase));



/**
 * STBEvent
 * @class eBase.data.STBEvent
 */
/**
 * STBEvent type.
 * @property type
 * @type {String}
 */
/**
 * Other params.
 * expand other properties according to actual needs.
 * Different definitions for different events.
 * @property params
 * @type {String}
 */
/**
 * Value-Added Service
 * @class eBase.data.Vas
 */
/**
 * Content ID.
 * @property id
 * @type {String}
 */
/**
 * Content name.
 * @property name
 * @type {String}
 */
/**
 * Content type.
 * The options are as follows: VAS: value - added service
 * @property type
 * @type {String}
 */
/**
 * Content description.
 * @property introduce
 * @type {String}
 */
/**
 * VAS URL.
 * @property url
 * @type {String}
 */
/**
 * Content rating.
 * A larger value indicates a higher rating.The value varies depending on the content rating systems in different countries.
 * @property ratingid
 * @type {Number}
 */
/**
 * Indicates whether a VAS has been subscribed to: The options are as follows: 飦?: no飦?1: yes
 * @property issubscribed
 * @type {Number}
 */
/**
 * Poster picture.
 * @property picture
 * @type {eBase.data.Picture}
 */
/**
 * VAS start time.
 *Format: yyyyMMddHHmmss
 * Example: 20120426170534
 * @property starttime
 * @type {String}
 */
/**
 * VAS end time.
 * Format: yyyyMMddHHmmss
 * Example: 20120426170534
 * @property endtime
 * @type {String}
 */
/**
 * ID of a category associated with a VAS.
 * @property categoryIds
 * @type {String[]}
 */
/**
 * Genre ID of a VAS.
 * @property genreIds
 * @type {String[]}
 */
/**
 * ID of a UMS category associated with a VAS.
 * @property nodeId
 * @type String
 */
/**
 * eBase's STB extension
 *
 * @class eBase.stb
 * @singleton
 */
(function(core) {
    'use strict';

    var config = core.config;

    config.ignoredErrorMap = {
        iptv: {
            'ContentLocked': [
                '1'
            ],
            'Logout': 'x'
        },
        social: [
            '411050007'
        ]
    };

    config.errorCodeMap = {
        'default': 'unknown.error',
        '-2': 'network_error',
        '-3': 'network_error',

        iptv: {
            'Authenticate': [
                '67174404',
                '33620481',
                '33620231',
                '33620232',
                '33619970',
                '85983373',
                '33619984',
                '85983545'
            ],

            'Authorization': [
                '117571585',
                '85983382',
                '85983383',
                '33619970'
            ],

            'GetDiscountInfos': [],

            'ReplaceDevice': [
                '85983300',
                '85983272',
                '85983406',
                '83886088'
            ],

            'Subscribe': [
                '117637892',
                '85983264',
                '117637888',
                '85983451',
                '33619970',
                '85983299'
            ],

            'Play': [],

            'AuthorizeAndPlay': [
                '117571585',
                '85983382',
                '85983383',
                '33619970',
                '67242120'
            ],

            'SubscribeAndPlay': [
                '117637892',
                '85983264',
                '117637888',
                '85983451',
                '33619970',
                '85983299'
            ],

            'AddProfile': [
                '3',
                '85983401'
            ],

            'SwitchProfile': [
                '5',
                '33619970',
                '85983269'
            ],

            'ModifyProfile': [
                '85983401',
                '85983542'
            ],

            'DelProfile': [
                '85983266'
            ],

            'SubscriberMgmt': [],

            'QuerySubscriber': [],

            'UpdatePassword': [
                '85983235',
                '85983237'
            ],

            'QueryOrder': [],

            'FavoriteManagement': [
                '85983254'
            ],

            'ReminderManagement': [
                '85983254',
                '85983284',
                '85983283',
                '83886088'
            ],


            'LockManagement': [
                '85983254',
                '83886088'
            ],

            'BookmarkManagement': [],

            'AddPVR': [
                '85983274',
                '85983282',
                '85983286',
                '85983305',
                '85983285',
                '85983287',
                '85983288',
                '85983281',
                '85983606'
            ],

            'UpdatePVR': [
                '85983263',
                '85983282'
            ],

            'DeletePVR': [],

            'DeleteSubPVR': [],

            'PeriodPVRMgmt': [
                '85983287',
                '85983280',
                '85983495',
                '85983496',
                '85983285',
                '85983286'
            ],

            'SubmitDeviceInfo': [],

            'pushMsgByTag': [],

            'ReminderQuery': [],

            'ScoreContent': [
                '85983462'
            ],

            'CheckLocationAuthorization': [
                '85983372'
            ],

            'CheckPassword': [
                '1'
            ],

            'Response': [
                '-2',
                '-3',
                '85983520',
                '85983521',
                '85983522',
                '85983523',
                '85983524',
                '85983525',
                '85983526',
                '85983527',
                '85983539',
                '85983549'
            ]
        },

        rice: {
            '': 'rice_other_error'
        },

        dropbox: {
            '': 'dropbox_other_error',
            authorize: {
                '1': 'dropbox_mail_error',
                '2': 'dropbox_barcode_error'
            },
            getPlayUrl: {
                '1': 'dropbox_token_expire'
            },
            getThumbnail: {
                '1': 'dropbox_token_expire'
            },
            getFileList: {
                '1': 'dropbox_token_expire'
            }
        },

        social: [
            '411050005',
            '411050040',
            '411050003',
            '411050054',
            'default'
        ]
    };


    return core;
})(eBase);

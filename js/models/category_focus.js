define(["underscore", "backbone"], function (_, Backbone) {
  var category_focus = Backbone.Model.extend();

  var focus = new category_focus();
  focus.set({ id: 1001, name: "Svi kanali" });
  return focus;
});

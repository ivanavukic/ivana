define(["underscore", "backbone"], function (_, Backbone) {
  var epg_focus = Backbone.Model.extend({
    validate: function (item) {
      //console.log(item.max_number);
      if (item.id < 0 || item.id > item.max_number) return "id ne smije biti manji od 0 i veci od 9";
    },
    setData: function (data) {
      var data_item = {
        idOriginal: data.channelID,
        id_epg: data.ID,
        mediaId: data.mediaId,
        channelID: data.channelID,
        contentType: data.contentType,
        slsType: data.slsType,
        CUTVStatus: data.CUTVStatus,
        icons: data.icons,
        isFillProgram: data.isFillProgram,
        isCPVR: data.isCPVR,
        name: data.name,
        startTime: data.startTime,
        endTime: data.endTime,
        isCUTV: data.isCUTV,
        isNPVR: data.isNPVR,
        purchaseEnable: data.purchaseEnable,
        st_live: this.get_status_live(data)
      };

      console.log("##############", data_item);

      this.set(data_item);
    },
    get_status_live: function (data) {
      if (data.startTime <= Date.now() && Date.now() <= data.endTime) return true;
      else return false;
    }
  });

  var focus = new epg_focus();
  focus.set({
    id: 1,
    idOriginal: "",
    id_epg: "",
    mediaId: "",
    channelID: "",
    contentType: "",
    slsType: "",
    CUTVStatus: "",
    icons: "",
    isFillProgram: "",
    isCPVR: "",
    name: "",
    startTime: "",
    endTime: "",
    isCUTV: "",
    isNPVR: "",
    purchaseEnable: "",
    max_number: 50,
    st_live: true
  });

  return focus;
});

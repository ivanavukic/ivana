define(["underscore", "backbone", "collections/channelCollections"], function (_, Backbone, ChannelCollections) {
  var channel_focus = Backbone.Model.extend({
    validate: function (item) {
      if (item.id < 0 || item.id > item.max_number) return "id ne smije biti manji od 0 i veci od 9";
    },
    setData: function (data) {
      console.log(data);
      var data_item = {
        id_select: this.get("id"),
        idKanala: data.idKanala,
        idOriginal: data.idOriginal,
        mediaId: data.mediaId,
        omiljeni: data.omiljeni,
        zakljucan: data.zakljucan,
        slika: data.slika,
        kategorija: data.kategorija,
        ime: data.ime,
        ChanStatus: data.ChanStatus,
        PreviewEnable: data.PreviewEnable,
        ChanKey: data.ChanKey,
        PLTVEnable: data.PLTVEnable,
        PauseLength: data.PauseLength,
        ChanURL: data.ChanURL,
        subscribed: data.subscribed,
        adult: data.adult,
        catchupDuration: data.catchupDuration,
        catchupStatus: data.catchupStatus
      };

      //console.log(data_item);
      this.set(data_item);
    },

    getIndexId: function (idOriginal) {
      var id = _.findIndex(ChannelCollections.toJSON(), { idOriginal: idOriginal });
      return id;
    }
  });

  var focus = new channel_focus();
  console.log(localStorage.getItem("kanalFokus"));
  focus.set(
    JSON.parse(localStorage.getItem("kanalFokus")) || {
      id: 0,
      id_select: 1,
      idKanala: 1,
      idOriginal: "",
      mediaId: "",
      omiljeni: 0,
      zakljucan: 0,
      slika: "",
      kategorija: "",
      ime: "",
      ChanStatus: "",
      PreviewEnable: "",
      ChanKey: "1",
      PLTVEnable: "",
      PauseLength: "",
      ChanURL: "",
      subscribed: "",
      max_number: 50
    }
  );

  return focus;
});

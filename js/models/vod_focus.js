define(["underscore", "backbone", "collections/channelCollections"], function (_, Backbone, ChannelCollections) {
  var vod_focus = Backbone.Model.extend({
    setData: function (data) {
      this.set(data);
    },
  });

  var focus = new vod_focus();
  /*
  focus.set({
    id_channel: "",
    id_media: "",
    id_item: "",
    vr_media: "",
    name: "",
    description: "",
    startTime: "",
    endTime: "",
    duration: "",
    "year-group": "",
    "maturity-rating": "",
    creators: "",
    genres: [
      {
        genreID: "",
        genreType: "",
        genreName: "",
      },
    ],
    picture: {
      icons: "",
      ads: "",
      stills: "",
      posters: "",
      backgrounds: "",
    },
  });
  */
  focus.set({
    id_channel: "1212",
    id_media: "",
    id_item: "576644198",
    vr_media: "catchup",
    name: "Ubice moga oca",
    description: "",
    startTime: "1641672000000",
    endTime: "1641675600000",
    duration: "00:00",
    "year-group": "",
    "maturity-rating": "",
    creators: "",
    genres: "",
    picture: {
      icons: "http://93.86.36.81:33200/CPS/images/universal/film/program/202201/20220108/63/20220108010236449828.jpg",
      ads: "http://93.86.36.81:33200/CPS/images/universal/film/program/202201/20220108/57/20220108010236449k44.jpg",
      stills: "http://93.86.36.81:33200/CPS/images/universal/film/program/202201/20220108/98/20220108010236449pm1.jpg",
      posters: "",
      backgrounds: "http://93.86.36.81:33200/CPS/images/universal/film/program/202201/20220108/51/202201080102364499ft.jpg",
    },
  });
  return focus;
});

define(["underscore", "backbone"], function (_, Backbone) {
  var menu_focus = Backbone.Model.extend({
    validate: function (item) {
      if (item.id < 0 || item.id > 6) return "id ne smije biti manji od 0 i veci od 9";
    },
  });

  var focus = new menu_focus();
  focus.set({ id: 2, name: "Početna", routes: "showStart", menu: "#menu_start" });

  /*
    focus.set({ id: 18 }, { validate: true });
    console.log('status', focus.isValid());
    console.log('vrijednos', focus.get("id"));
    console.log('focus', focus);
    */

  return focus;
});

define([
    'underscore',
    'backbone'
], function (_, Backbone) {
    var dynamic_view_model = Backbone.Model.extend({
        defaults: {
            id_stack: [],
            list_function_stack: [],
            list_function: [],
        },

        initialize: function (args) {
            this.id_stack = [];
            this.list_function_stack = [];
            this.list_function = args.list_function;
            this.list_function_args = args.list_function_args;
        }
    });

    return dynamic_view_model;
});
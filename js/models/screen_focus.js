define(["underscore", "backbone"], function (_, Backbone) {
  var screen_model = Backbone.Model.extend({
    setData: function (data) {
      var data_item = {
        view: data.view,
        focus: data.focus,
      };

      this.set(data_item);
    },
  });

  var screen = new screen_model();

  return screen;
});

define(["underscore", "backbone", "collections/channelCollections"], function (_, Backbone, ChannelCollections) {
  var vod_content_focus = Backbone.Model.extend({
    setData: function (data) {
      if (data[0].category.size == "plg") {
        data[0].category.size = "pmd";
        data[0].category.left_shift = -7.8125;
      }

      if (data[0].category.size == "lg") {
        data[0].category.size = "md";
        data[0].category.left_shift = -21.25;
      }

      if (
        data[0].list[data[0].list.length - 1].hasOwnProperty("hasChildren") &&
        data[0].list[data[0].list.length - 1].hasChildren == -1
      ) {
        data[0].list.pop();
      }

      console.log(data);
      this.set(data);
    }
  });

  var focus = new vod_content_focus();

  return focus;
});

define(["underscore", "backbone"], function (_, Backbone) {
  var radio_list = Backbone.Model.extend();

  var RadioList = Backbone.Collection.extend({
    model: radio_list
  });

  var radioList = new RadioList([
    {
      id: "0",
      name: "Popularno",
      id_list: 0,
      vod_id: 0,
      position: "radio_row_0",
      size: "lg",
      left_shift: -19.53125,
      progress: 0,
      scroll: 0,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "1",
      name: "Rock",
      id_list: 1,
      vod_id: 1,
      position: "radio_row_1",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -350,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "2",
      name: "Klasika",
      id_list: 2,
      vod_id: 2,
      position: "radio_row_2",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -585,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "3",
      name: "Zabavna",
      id_list: 3,
      vod_id: 3,
      position: "radio_row_3",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -585,
      duration: true,
      video_preview: false,
      st_header: 0
    }
  ]);

  return radioList;
});

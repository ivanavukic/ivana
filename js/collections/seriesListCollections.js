define(["underscore", "backbone"], function (_, Backbone) {
  var series_list = Backbone.Model.extend();

  var SeriesList = Backbone.Collection.extend({
    model: series_list,
  });

  // lista grupa

  var seriesList = new SeriesList();

  return seriesList;
});

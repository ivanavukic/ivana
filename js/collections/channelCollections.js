define(["underscore", "backbone", "models/channel_item"], function (_, Backbone, channel_item) {
  var Channel = Backbone.Collection.extend({
    model: channel_item,
  });

  var channel = new Channel();

  /*
  console.log("111111111111111111");
  console.time("start");
  new Promise.delay(2000)
    .then(function () {
      return tv.getKanale("", "");
    })
    .then(function (res) {
      channel.push(res);
      console.timeEnd("start");
      console.log("222222222222");
    });
*/

  return channel;
});

define(["underscore", "backbone"], function (_, Backbone, menu_item) {
  var menu_mov_item = Backbone.Model.extend();

  var MenuMov = Backbone.Collection.extend({
    model: menu_mov_item,
  });

  var menu_mov = new MenuMov([
    new menu_mov_item({ id: 0, name: "Pogledaj trejler", routes: "#", menu: "#mov_trailer" }),
    new menu_mov_item({ id: 1, name: "Pusti film", routes: "#", menu: "#mov_play" }),
    new menu_mov_item({ id: 2, name: "Označi kao odgledano", routes: "#", menu: "#mov_bookmark" }),
  ]);

  return menu_mov;
});

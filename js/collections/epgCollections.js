define(["underscore", "backbone", "models/epg_item"], function (_, Backbone, epg_item) {
  var Epg = Backbone.Collection.extend({
    model: epg_item,
  });

  var epg = new Epg();

  return epg;
});

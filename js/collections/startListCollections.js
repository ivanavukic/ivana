define(["underscore", "backbone"], function (_, Backbone) {
  var start_list = Backbone.Model.extend();

  var StartList = Backbone.Collection.extend({
    model: start_list
  });

  var startList = new StartList([
    {
      id: "0",
      name: "Popularno",
      id_list: "0",
      vod_id: 0,
      position: "start_row_0",
      size: "lg",
      left_shift: -19.53125,
      progress: 0,
      scroll: 0,
      duration: true,
      video_preview: true,
      st_header: 0
    },
    {
      id: "1",
      name: "Trenutno",
      id_list: "1",
      vod_id: 1,
      position: "start_row_1",
      size: "md",
      left_shift: 0,
      progress: 1,
      scroll: -180,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "2",
      name: "Promo",
      id_list: "2",
      vod_id: 2,
      position: "start_row_2",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -415,
      duration: true,
      video_preview: false,
      st_header: 0
    }
    /*
        {
            "id": "3",
            "name": "Trenutno 2",
            "id_list": "3",
            "vod_id": 3,
            "position": "start_row_3",
            "size": "md",
            "left_shift": 0,
            "progress": 1,
            "scroll": -650,
            "duration": true,
            "st_header": 0
        }
        */
  ]);

  return startList;
});

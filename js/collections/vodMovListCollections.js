define(["underscore", "backbone"], function (_, Backbone) {
  var vod_list = Backbone.Model.extend();

  var VodtList = Backbone.Collection.extend({
    model: vod_list
  });

  var vodtList = new VodtList([
    {
      id: "0",
      name: "Akcijski trileri",
      id_list: "0",
      vod_id: 0,
      position: "vod_row_1",
      size: "pmd",
      left_shift: 5.46875,
      progress: 0,
      scroll: 0,
      duration: true,
      video_preview: false,
      st_header: 0
    }
  ]);

  return vodtList;
});

define(["underscore", "backbone", "coreSearch", "coreVods", "bluebird"], function (_, Backbone, search, vod, Promise) {
  /* constructor */
  function SearchController(options) {
    this.groups = [
      {
        code: "1",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Trenutno na TV",
        ID: "1",
        id_list: 0,
        size: "md",
        left_shift: "-21.25",
        progress: 1,
        position_back: { left: "key_keyboard" },
        action_type: "searchList",
        position: "vod_row_0",
        image: "stills"
      },
      {
        code: "2",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Najgledaniji kanali",
        ID: "2",
        id_list: 1,
        size: "md",
        left_shift: "-21.25",
        progress: 0,
        position_back: { left: "key_keyboard" },
        action_type: "searchList",
        position: "vod_row_1",
        image: "stills"
      },
      {
        code: "3",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Filmovi",
        ID: "3",
        id_list: 2,
        size: "md",
        left_shift: "-21.25",
        progress: 0,
        position_back: { left: "key_keyboard" },
        action_type: "searchList",
        position: "vod_row_2",
        image: "backgrounds"
      },
      {
        code: "4",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Serije",
        ID: "4",
        id_list: 3,
        size: "md",
        left_shift: "-21.25",
        progress: 0,
        position_back: { left: "key_keyboard" },
        action_type: "searchList",
        position: "vod_row_3",
        image: "backgrounds"
      }
    ];
  }

  SearchController.prototype.init = function () {
    var group = [];
    var self = this;

    return new Promise(function (resolve, reject) {
      vod.get_mts_trenutno_na_tv().then(function (res) {
        group.push({
          category: self.groups[0],
          list: mts_trenutno_na_tv_Translate(res)
        });

        vod.get_mts_najgledaniji_kanali().then(function (res) {
          var list = [];

          group.push({
            category: self.groups[1],
            list: mts_najgledaniji_kanali_Translate(res)
          });

          vod.get_mts_najgledanije_u_bioskopu().then(function (res) {
            //console.log(vodTranslate(res));
            group.push({
              category: self.groups[2],
              list: mts_najgledanije_u_bioskopu_filmovi_Translate(res)
            });

            group.push({
              category: self.groups[3],
              list: mts_najgledanije_u_bioskopu_serije_Translate(res)
            });

            console.log(group);
            resolve(group);
          });
        });
      });
    });
    /*
    return vod
      .getPopularnoOveNedelje()
      .then(function (res) {
        group.push({
          category: self.groups[0],
          list: res,
        });
        return true;
      })
      .then(function (res) {
        return vod.getCatchupRecm().then(function (res) {
          group.push({
            category: self.groups[1],
            list: res,
          });
        });
      })
      .then(function (res) {
        return vod.getVodRecm().then(function (res) {
          return group.push({
            category: self.groups[2],
            list: res,
          });
        });
      });
*/

    //return this.getFirst: function (4);
  };

  SearchController.prototype.slice = function (from, to) {
    return true;
  };

  SearchController.prototype.getGroupLength = function () {
    return this.groups.length;
  };

  SearchController.prototype.keyboardSearch = function (data) {
    var self = this;
    var searchVal = data.value;

    // kolekcije koje vracamo
    var collections = [];

    return new Promise(function (resolve, reject) {
      search.search(searchVal).then(function (res) {
        console.log(res);

        collections[0] = {
          category: self.groups[0],
          list: mts_search_program_Translate(res.content[1])
        };
        collections[1] = {
          category: self.groups[1],
          list: mts_search_kanali_Translate(res.content[0])
        };

        collections[2] = {
          category: self.groups[2],
          list: mts_search_filmovi_Translate(res.content[2])
        };

        collections[3] = {
          category: self.groups[3],
          list: mts_search_serije_Translate(res.content[2])
        };

        resolve(this.validateCollections(collections));
      });
    });
  };

  SearchController.prototype.searchSuggestions = function (data) {
    return new Promise(function (resolve, reject) {
      resolve([]);
    });
  };

  // validacija, tj. uklanjanje praznih grupa
  validateCollections = function (collections) {
    var out = [];
    var group_tmp = [];
    this.groups = [];
    if (collections.length) {
      for (i = 0; i < collections.length; i++) {
        if (collections[i].list.length > 0) {
          out.push(collections[i]);
          var index = out.length - 1;
          // prepisivanje, korekcija...
          out[index].category.code = index + 1;
          out[index].category.parentSubjectID = index;
          out[index].category.ID = index + 1;
          out[index].category.id_list = index;
          out[index].category.position = "vod_row_" + index;
        }
      }
    }

    return out;
  };

  return SearchController;
});

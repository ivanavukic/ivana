define(["underscore", "backbone"], function (_, Backbone) {
  var search_list = Backbone.Model.extend();

  var SearchList = Backbone.Collection.extend({
    model: search_list
  });

  var searchList = new SearchList([
    {
      id: "0",
      name: "Preporučeno",
      id_list: "0",
      vod_id: 0,
      position: "start_row_0",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: 0,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "1",
      name: "Propušteno",
      id_list: "1",
      vod_id: 1,
      position: "start_row_1",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -235,
      duration: true,
      video_preview: false,
      st_header: 0
    },
    {
      id: "2",
      name: "Filmovi & Serije",
      id_list: "2",
      vod_id: 2,
      position: "start_row_2",
      size: "md",
      left_shift: 0,
      progress: 0,
      scroll: -235,
      duration: false,
      video_preview: false,
      st_header: 0
    }
  ]);

  return searchList;
});

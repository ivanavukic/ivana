define(["underscore", "backbone"], function (_, Backbone) {
  //-------------------------------------
  var vod_list = Backbone.Model.extend();

  var VodtList = Backbone.Collection.extend({
    model: vod_list,
  });
  //-------------------------------------

  var vodtLista = new VodtList();

  //console.log('vodtLista',vodtLista);
  return vodtLista;
});

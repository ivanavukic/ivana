define(["underscore", "backbone", "coreSearch", "coreVods", "bluebird"], function (_, Backbone, search, vod, Promise) {
  /* constructor */
  function SearchController(options) {
    this.groups = [
      {
        code: "1",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Promo",
        ID: "1",
        id_list: 0,
        size: "lg",
        left_shift: "-60.15625",
        progress: 0,
        position_back: {},
        action_type: "player",
        position: "vod_row_0",
        image: "backgrounds"
      },

      {
        code: "2",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Trenutno na TV",
        ID: "2",
        id_list: 1,
        size: "md",
        left_shift: "-21.25",
        progress: 1,
        position_back: {},
        action_type: "player",
        position: "vod_row_1",
        image: "stills"
      },

      {
        code: "3",
        parentSubjectID: "0",
        hasChildren: "-1",
        name: "Najgledanije u bioskopu",
        ID: "3",
        id_list: 2,
        size: "md",
        left_shift: "-21.25",
        progress: 0,
        position_back: {},
        action_type: "vod_content",
        position: "vod_row_2",
        image: "stills"
      }
    ];
  }

  SearchController.prototype.init = function () {
    var group = [];
    var self = this;

    //vod.getPopularnoOveNedelje().then(function (res) {

    return new Promise(function (resolve, reject) {
      vod.get_mts_propusteno().then(function (res) {
        console.log(res);

        group.push({
          category: self.groups[0],
          list: mts_propusteno_Translate(res)
        });

        vod.get_mts_trenutno_na_tv().then(function (res) {
          //console.log(res);

          group.push({
            category: self.groups[1],
            list: mts_trenutno_na_tv_Translate(res)
          });

          vod.get_mts_najgledanije_u_bioskopu().then(function (res) {
            //console.log(vodTranslate(res));
            console.log("usao u init", res);
            group.push({
              category: self.groups[2],
              list: mts_najgledanije_u_bioskopu_Translate(res)
            });

            console.log(group);
            resolve(group);
          });
        });
      });
    });
  };

  SearchController.prototype.slice = function (from, to) {
    return true;
  };

  SearchController.prototype.getGroupLength = function () {
    return this.groups.length;
  };

  return SearchController;
});

define(["underscore", "backbone"], function (_, Backbone, menu_item) {
  var menu_item = Backbone.Model.extend();

  var Menu = Backbone.Collection.extend({
    model: menu_item,
  });

  //new menu_item({ id: 3, name: "TV Vodič", routes: "showGuideTV", menu: "#menu_guideTV" }),

  var menu = new Menu([
    new menu_item({ id: 0, name: "Pretraga", routes: "showSearch", menu: "#menu_search" }),
    new menu_item({ id: 1, name: "Početna", routes: "showStart", menu: "#menu_start" }),
    new menu_item({ id: 2, name: "TV Uživo", routes: "showLiveTV", menu: "#menu_liveTV" }),
    new menu_item({ id: 3, name: "Filmovi", routes: "showMovies", menu: "#menu_movies" }),
    new menu_item({ id: 4, name: "Serije", routes: "showSeries", menu: "#menu_series" }),
    new menu_item({ id: 5, name: "Podešavanja", routes: "showSettings", menu: "#menu_settings" }),
    new menu_item({ id: 6, name: "User", routes: "showUser", menu: "#menu_user" }),
  ]);

  return menu;
});

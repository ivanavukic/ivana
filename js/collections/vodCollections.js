define(["underscore", "backbone", "coreVods", "bluebird"], function (_, Backbone, vod, Promise) {
  /* constructor */
  function VodController(options) {
    this.ID = options.ID;
    this.config = options.config;
    this.groups = [];
  }

  VodController.prototype.debug = function () {
    //this.getGroupItem({ ID: "APO" });
    //console.log();

    console.log(this.groups);

    /*
            this.slice(2, 4).then(function (res) {
              console.log(res);
            });
            */
    //Promise.all([this.getFirst(4)]).then(function (res) {
    //console.log(res);
    //});
  };

  VodController.prototype.init = function () {
    var self = this;
    this.groups = [];
    console.log(this.ID);
    return vod.getVodCatalogue("movie", this.ID).then(function (res) {
      console.log(this.ID, res);
      self.groups = res;

      //self.debug();
      return self.groups;
    });

    //return this.getFirst: function (4);
  };

  VodController.prototype.getGroupItem = function (category) {
    var group = {};

    // sadrzaj po ID-ju kategorije
    return new Promise(function (resolve) {
      resolve(
        vod.getVodCategory("movie", String(category.ID), true).then(function (res) {
          console.log(res);

          group.category = category;
          group.list = vodTranslate(res);

          //console.log(group);
          //resolve(group); //return group
          return group;
        })
      );
    });
  };

  VodController.prototype.getFirst = function (max) {
    var self = this;
    return new Promise(function (resolve) {
      resolve(self.slice(0, max));
    });
  };

  VodController.prototype.slice = function (from, to) {
    var self = this;
    var i = from;
    //console.log(from + " -------------", this.groups.length - 1);
    if (from > this.groups.length - 1) {
      return new Promise(function (resolve, reject) {
        console.log("NEMA VISE !!!!!!!!!!!!!");
        resolve(undefined);
      });
    } else {
      var coll_slice = this.groups.slice(from, to);
      return Promise.map(coll_slice, function (item, index) {
        return self.getGroupItem(item).then(function (res) {
          console.log(res);
          /*
                              action_type    
                              1 - player
                              2 - show preview
                              3 - children
                              */

          //console.log("from, to, i: ", from, to, i);
          //console.log("index: ", index);

          //-------------------------------------------------------------
          /* 
                                 To do:
                                 PROBLEM: SORTIRANJE / ORDER, Promise.map fcija ne garantuje tacan order 
                                 pa zato moramo da koristimo "index" umjesto "i".
                                 Npr. red koji se naknadno dodaje, renderuje se na pogresnoj poziciji (vod_row_0).
                              */
          if (to - from === 1) {
            index = i;
          }
          //-------------------------------------------------------------

          if (index == 0) {
            res.category["id_list"] = index;
            res.category["size"] = self.config.first.size;
            res.category["left_shift"] = self.config.first.left_shift;
            res.category["progress"] = self.config.first.progress;
            res.category["position_back"] = self.config.first.position_back;
            res.category["action_type"] = self.config.first.action_type;
            res.category["position"] = "vod_row_" + res.category["id_list"];
            res.category["image"] = self.config.first.image;
          } else {
            res.category["id_list"] = index;
            res.category["size"] = self.config.other.size;
            res.category["left_shift"] = self.config.other.left_shift;
            res.category["progress"] = self.config.other.progress;
            res.category["position_back"] = self.config.other.position_back;
            res.category["action_type"] = self.config.other.action_type;
            res.category["position"] = "vod_row_" + res.category["id_list"];
            res.category["image"] = self.config.other.image;
          }

          if (res.category.hasOwnProperty("hasChildren")) {
            if (res.category.hasChildren == "1") {
              //res.list.unshift(self.getChildrenCard(res.category));
              // res.list.push(self.getChildrenCard(res.category));
            } else {
              if (!res.list.length) {
                res.list.push(self.getEmptyCard(res.category));
              }
            }
          }

          //console.log(i);
          //console.log(res);
          i++;
          return res;
        });
      }).then(function (result) {
        if (result == undefined) {
          console.log("self.groups", self.groups);
          self.groups.splice(from + index, 1);
          console.log("Poslije self.groups", self.groups);
        }

        return result;
      });
    }
  };

  VodController.prototype.getEmptyCard = function (item) {
    return {
      ID: -1,
      code: 0,
      name: "Empty",
      vr_media: "vod",
      hasChildren: "-1",
      action_type: "empty",
      parentSubjectID: null,
      picture: {
        posters: ["./image/" + item.size + "_empty_posters.png"],
        icons: ["./image/" + item.size + "_empty_icons.png"],
        ads: ["./image/" + item.size + "_empty_ads.png"],
        backgrounds: ["./image/" + item.size + "_empty_backgrounds.png"],
        stills: ["./image/" + item.size + "_empty_stills.png"]
      }
    };
  };

  VodController.prototype.getChildrenCard = function (item) {
    return {
      ID: item.ID,
      code: item.code,
      name: "Prikaži sve",
      hasChildren: "-1",
      action_type: "children",
      parentSubjectID: item.parentSubjectID,
      picture: {
        posters: ["./image/" + item.size + "_posters.png"],
        icons: ["./image/" + item.size + "_icons.png"],
        ads: ["./image/" + item.size + "_ads.png"],
        backgrounds: ["./image/" + item.size + "_backgrounds.png"],
        stills: ["./image/" + item.size + "_stills.png"]
      }
    };
  };

  VodController.prototype.getGroupLength = function () {
    return this.groups.length;
  };

  return VodController;
});

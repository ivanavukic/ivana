require.config({
  paths: {
    jquery: "lib/jquery-min",
    "jquery-ui": "lib/jquery-ui",
    underscore: "lib/underscore-min",
    backbone: "lib/backbone-min",
    bluebird: "lib/bluebird",
    velocity: "lib/velocity",
    scrollintoview: "lib/jquery.scrollintoview.min",
    text: "lib/plugins/text",

    //===== CORE ==============
    core: "assets/core",
    coreLogin: "assets/corejs/coreLogin",
    coreChanEpg: "assets/corejs/coreChanEpg",
    coreSearch: "assets/corejs/coreSearch",
    coreSettings: "assets/corejs/coreSettings",
    coreVods: "assets/corejs/coreVods",
    coreHeartbeat: "assets/corejs/coreHeartbeat"
  }
});

define([
  "jquery",
  "app",
  "coreLogin",
  "coreChanEpg",
  "coreSearch",
  "coreVods",
  "coreSettings",
  "coreHeartbeat"
], function ($, App, login, coreChanEpg, coreSearch, coreVods, coreSettings, coreHeartbeat) {
  $(document).ready(function () {
    login
      .loginNonDynamicDevice()
      .then(res => login.getCustomerProfiles())
      .then(res => {
        App.initialize({});
      });
    coreChanEpg.createFavorite(399).then(res => console.log(res));
  });
});

define([
  "underscore",
  "backbone",
  "models/menu_item",
  "views/menuView",
  "models/channel_item",
  "views/channelView",
  "models/category_item",
  "views/menucategView",
  "views/categoryView",
  "models/epg_focus",
  "models/channel_focus",
  "views/sliderView",
  "collections/epgItemCollections",
  "views/routerView",
  "views/vodView",
  "views/searchView",
  "views/suggestView",
  "views/startView",
  "coreVods",
  "views/sliderVodView",
  "models/screen_focus"
], function (
  _,
  Backbone,
  menu_item,
  MenuView,
  channel_item,
  ChannelView,
  category_item,
  MenucategView,
  CategoryView,
  epg_focus,
  channel_focus,
  SliderView,
  EpgCollections,
  RouterView,
  VodView,
  SearchView,
  SuggestView,
  StartView,
  vod,
  SliderVodView,
  screen_focus
) {
  var bus = _.extend({}, Backbone.Events);

  var initialize = function (options) {
    this.bus = options.bus;

    var menuView = new MenuView({
      el: "#menu_container",
      model: menu_item,
      bus: bus
    });

    var menucategView = new MenucategView({
      el: "#menucateg_container",
      model: category_item,
      bus: bus
    });

    var categoryView = new CategoryView({
      el: "#category_container",
      model: category_item,
      bus: bus
    });

    var channelView = new ChannelView({
      el: "#channel_container",
      model: channel_item,
      bus: bus
    });

    /* Vratiti kada stavimop na pocetak*/
    var routerView = new RouterView({
      bus: bus
    });

    /*
    var b = new VodCollections({ ID: "7000" });
    b.init().then(function () {
      b.getFirst(10).then(function (res) {
        console.log(res);
      });
    });
    */

    /*
    b.getFirst(2).then(function (res) {
      console.log(res);
    });
    */
    //b.init();

    //a.VodController.init();

    /*
    var a = VodCollections.init("7000")
      .getFirst(4)
      .then(function (res) {
        console.log(res);
      });
*/
    /*
    a.getFirst(4).then(function (res) {
      console.log(res);
    }); //console.log(a.init("7000"));
    */

    //var testView = new TestView({ el: "#other_container", model: channel_item, bus: bus });
    //var sliderView = new SliderView({ el: "#player_container", id_item: "1", bus: bus });
    //var vodContent = new VodContentView({ el: "#other_container", id_item: "1", bus: bus });

    //var vodView = new VodView({ el: "#other_container", bus: bus });

    //var searchView = new SearchView({ el: "#other_container", bus: bus });

    /*
        var startView = new StartView({ el: "#other_container", bus: bus });
        var searchView = new SearchView({ el: "#other_container", bus: bus });
        var guideTVView = new GuideTVView({ el: "#other_container", bus: bus });
        var vodView = new VodView({ el: "#other_container", bus: bus });
        var seriesView = new SeriesView({ el: "#other_container", bus: bus });
    */

    //var vodView = new VodView({ el: "#other_container", bus: bus });
    //var radioView = new RadioView({ el: "#other_container", bus: bus });

    //var seriesView = new SeriesView({ el: "#other_container", bus: bus });

    //var startView = new StartView({ el: "#other_container", bus: bus });

    //var channel_epgView = new Channel_epgView({ el: "#other_container", model: channel_epg_focus, bus: bus });

    //var guideTVView = new GuideTVView({ el: "#other_container", bus: bus });

    var AplicationRouter = Backbone.Router.extend({
      routes: {
        "": "showLiveTV",
        view_live_tv: "showLiveTV"
      },

      showLiveTV: function () {
        routerView.render();
        menuView.render();
        channelView.create();
        menucategView.create();
        categoryView.render();

        //console.log("SSSSSSSSSSSSSSS");
        //console.log(vod);

        //screen_focus.setData({ view: "vodcontent", focus: "mov_play" });

        //vod.getVodCatalogue("TRI3").then(function (aaa) {});
        //vod.getVodCategory("TRI3").then(function (aaa) {});
        /*
        vod.getEpisodesAndSeasons("549791990").then(function (aaa) {
          console.log(aaa);
        });
        */
        /*
        vod.getSingleVod("549791990").then(function (aaa) {
          console.log(aaa);
        });
        */

        //var vodView = new VodView({ el: "#other_container", bus: bus });
        //vodView.create({ ID: "7000" });

        //var searchView = new SearchView({ el: "#other_container", bus: bus });
        //searchView.create();

        //var startView = new StartView({ el: "#other_container", bus: bus });
        //startView.create();

        //var suggestView = new SuggestView({ el: "#other_container", bus: bus });
        //suggestView.create({ search: "kuc" });

        /*
        $("#screen_view_container").removeClass("active");
        $("#screen_view_container").addClass("inactive");
        $("#id_router").focus();
        */
        //testView.render();
        //sliderView.render();
        //vodContent.render();
        //seriesView.render();
        //searchView.render();
        //vodView.render();
        //radioView.render();
        //startView.render();
        //console.log('aaaaaa');
        //seriesView.render();
        //guideTVView.render();
        //channel_epgView.render();
        //searchView.render();
        //startView.render();
        //console.log(StartCollections.where({ id_category: "8" }));
        /*
                var tag = StartCollections.get(2);
                console.log(tag);
                tag.set({ image: "something" });
                StartCollections.set(tag);
                */
        //console.log(StartCollections);
        /*

                var list = new Backbone.Collection(StartCollections.filter(function(model) {
                    return model.get('id_list') == '1';
                }));

                console.log(StartCollections.get(2));

                var listView = new ListView({
                    el: "#start_container",
                    id_list: 1,
                    name: "Preporučeno",
                    collection: list,
                    bus: this.bus
                });
                listView.render();
                */
        //console.log("Prikazi showLiveTV");
        /******************************************************************************** */
        /*
        var self = this;
        tv.getKanalItem(1208).then(function (chan_res) {
          self.channel = chan_res;
          tv.getTrenutniEpg(1208).then(function (epg_res) {
            EpgCollections.reset();
            EpgCollections.push(epg_res);
            self.epg = epg_res;
            tv.getTrenutno(1208).then(function (live_res) {
              self.live = live_res;

              var epg_index = _.findIndex(self.epg, { ID: self.live.ID });

              var data = self.epg[epg_index];
              console.log("this.epg", self.epg);
              console.log("epg_index", epg_index);

              var data_item = {
                idOriginal: self.channel.idOriginal,
                id_epg: data.ID,
                mediaId: data.mediaId,
                channelID: data.channelID,
                contentType: data.contentType,
                slsType: data.slsType,
                CUTVStatus: data.CUTVStatus,
                icons: self.channel.slika,
                isFillProgram: data.isFillProgram,
                isCPVR: data.isCPVR,
                name: data.name,
                startTime: data.startTime,
                endTime: data.endTime,
                isCUTV: data.isCUTV,
                isNPVR: data.isNPVR,
                purchaseEnable: data.purchaseEnable,
              };

              console.log(data_item);
              epg_focus.set(data_item);

              var data = self.channel;
              var data_item = {
                idKanala: data.idKanala,
                idOriginal: data.idOriginal,
                mediaId: data.mediaId,
                omiljeni: data.omiljeni,
                slika: data.slika,
                ime: data.ime,
                ChanStatus: data.ChanStatus,
                PreviewEnable: data.PreviewEnable,
                ChanKey: data.ChanKey,
                PLTVEnable: data.PLTVEnable,
                PauseLength: data.PauseLength,
                ChanURL: data.ChanURL,
              };

              channel_focus.set(data_item);

              console.log(data_item);

              var sliderView = new SliderView({ el: "#player_container", bus: bus });
              sliderView.create();
            });
          });
        });
        */
        /********************************************************************************** */
      }
    });

    var AppRouter = new AplicationRouter();
    Backbone.history.start();
  };

  return {
    initialize: initialize
  };
});

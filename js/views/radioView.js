define([
    'jquery',
    'underscore',
    'backbone',
    'collections/radioListCollections',
    'collections/radioCollections',
    'views/listRowsView',
    'text!page/radio.html'
], function($, _, Backbone, RadioListCollections, RadioCollections, ListRowsView, template) {


    var st_complete = "";

    var StartView = Backbone.View.extend({

        tagName: "span",

        initialize: function(options) {
            this.y = 0;
            this.bus = options.bus;
            this.bus.on("RadioClose", this.onClose, this);
        },

        render: function() {

            console.log('Radio');
            console.log(RadioListCollections);
            console.log(RadioCollections);


            var listRowsView = new ListRowsView({
                el: "#other_container",
                template: template,
                LstCollect: RadioListCollections,
                Collect: RadioCollections,
				show_content: { st_show: false, id_item: "" },
                position_back: "",
                shift_y: 245,
				set_focus_name:1,
                bus: this.bus
            });
            //listRowsView.render();

            $('#other').append(listRowsView.render().el);

            //$('#series_row_0').css({"margin-left":"-265px"});
            //$('#control_0').css({"left":"-497px"});
            /*
           
            listRowsView.remove();
            */

            var tag = RadioListCollections.at(0);
            this.$el.find('#control_' + tag.get("id_list")).addClass('focus');
            this.$el.find('#control_' + tag.get("id_list")).focus();


            return this;
        },

        onClose: function() {

            this.undelegateEvents();
            this.$el.removeData().unbind();

            this.$el.empty();
            this.unbind();
            this.stopListening();
            return this;


            //this.remove();




        }

    });

    return StartView;
});
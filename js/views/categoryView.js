define([
  "jquery",
  "underscore",
  "backbone",
  "velocity",
  "models/category_focus",
  "collections/categoryCollections",
  "text!page/category.html"
], function ($, _, Backbone, Velocity, category_focus, category, template) {
  /*
    var x = 0;
    var y = 0;

    var x_focus_pos = 0;
    var x_shift = 230;
    var count = 0;

   
    var st_open_menu = false
    */

  var st_complete = "";

  var CategoryView = Backbone.View.extend({
    initialize: function (options) {
      this.x = 1;
      this.y = 0;

      this.x_focus_pos = 0;

      this.count = 0;

      if ($(window).height() <= 720) {
        this.x_shift = 150;
      } else {
        //this.x_shift = 230;
        this.x_shift = 150;
      }

      //this.st_complete = "";
      this.st_open_menu = false;

      this.bus = options.bus;
      this.bus.on("SelectCategory", this.onSelectCategory, this);
    },

    onSelectCategory: function () {
      console.log("onSelectCategory");
      $("#channel").addClass("remove_focus");
      this.$el.find("#category").addClass("focus");
      this.$el.find("#category").focus();
    },

    events: {
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i

      //dbgLog("ChannelView - key: " + e.keyCode);

      if (e.keyCode == KEY_RED) {
        clearEpgCache();
        return false;
      } else if (e.keyCode == KEY_BLUE) {
        location.reload();
        return false;
      } else if (e.keyCode == KEY_SETTINGS) {
        openSettings();
        return false;
      }

      this.x = category_focus.get("id");
      this.count = category.size();

      if (e.keyCode === KEY_RIGHT) {
        if (this.st_open_menu !== false) {
          if (st_complete !== "1") {
            if (this.x < this.count) {
              this.x = this.x + 1;
              console.log("count--", this.count, "x", this.x);
              this.onCategoryShift(e.keyCode);
            }
          }
        }
      } else if (e.keyCode === KEY_LEFT) {
        if (this.st_open_menu !== false) {
          if (st_complete !== "1") {
            if (this.x > 1) {
              this.x = this.x - 1;
              this.onCategoryShift(e.keyCode);
            }
          }
        }
      } else if (e.keyCode === KEY_HOME) {
        //this.bus.trigger("ExpandMenu");
      } else if (e.keyCode === KEY_ENTER) {
        if (this.st_open_menu === false) {
          this.onShowMenuCategory();
        } else {
          this.onReturnChannel();
          console.log("this.x-1", this.x - 1);
          console.log(category.at(this.x - 1));
          return;
        }
      } else if (e.keyCode === KEY_DOWN) {
        this.onReturnChannel();
        return;
      }
      category_focus.set({ id: this.x, name: category.at(this.x - 1).get("name") }, { validate: true });
      this.$el.find("#category").focus();
    },

    onCategoryShift: function (key) {
      console.log("onCategoryShift");

      if (key === KEY_RIGHT) {
        this.x_focus_pos = this.x_focus_pos - this.x_shift;
        st_complete = "1";
        console.log("animiram", this.x_focus_pos);
        $("#category-content").animate(
          { left: this.x_focus_pos + "px" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
            }
          }
        );
      } else if (key === KEY_LEFT) {
        this.x_focus_pos = this.x_focus_pos + this.x_shift;
        st_complete = "1";
        console.log("animiram levo", this.x_focus_pos);
        $("#category-content").animate(
          { left: this.x_focus_pos + "px" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
            }
          }
        );
      }

      this.$el.find("#category").focus();
    },

    onShowMenuCategory: function () {
      this.st_open_menu = true;
      this.x = 1;
      this.x_focus_pos = 0;
      this.render();
    },

    onReturnChannel: function () {
      this.st_open_menu = false;
      this.x_focus_pos = 0;
      this.x = 1;
      this.render();
      $("#channel").removeClass("remove_focus");
      $("#channel").focus();
    },

    render: function () {
      //console.log('render');

      var templ = _.template(template);

      var html = templ({
        result: category.toJSON(),
        st_open_menu: this.st_open_menu,
        name: category_focus.get("name")
      });
      this.$el.html(html);
      if (this.st_open_menu === true) {
        this.$el.find("#category").addClass("focus");
        this.$el.find("#category").focus();
      }
      return this;
    }
  });

  return CategoryView;
});

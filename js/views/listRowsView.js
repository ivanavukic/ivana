define(["jquery", "underscore", "backbone", "bluebird", "views/listView"], function (
  $,
  _,
  Backbone,
  Promise,
  ListView,
  template
) {
  var st_complete = "";
  var std = true;

  var ListRowsView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      //console.log(options);
      this.y = 0;
      this.id_list = 0;

      this.template = options.template;
      this.position_back = options.position_back;
      this.collObj = options.collObj;
      this.config = options.config;
      this.screen = options.screen;

      // setovanje roots za Back
      this.parentID = options.collObj.ID;
      this.roots = options.roots;
      this.setRoots();

      //--------------------------------------------------------
      var lists = new Backbone.Collection();
      lists.push(options.collect);
      //console.log(options.collect);

      this.Collections = lists;

      //dodao zbog searc i ostalih stana koje imaju fiksno redove
      if (options.hasOwnProperty("max_rows")) this.max_rows = options.max_rows;
      else this.max_rows = 3;

      console.log(this.max_rows);

      var rows = Backbone.Model.extend();
      this.TempCollections = new Backbone.Collection(this.Collections.first(this.max_rows), { model: rows });

      //console.log(this.TempCollections);

      //this.listenTo(this.TempCollections, 'add', this.render);
      //---------------------------------------------------------

      this.height_first = getRowSizeAnimate(this.config.first.size).height;
      this.height_other = getRowSizeAnimate(this.config.other.size).height;

      console.log(this.config.first.size);
      console.log(this.height_first);

      this.bus = options.bus;

      this.bus.off("StartListUp").on("StartListUp", this.scrollUp, this);
      this.bus.off("StartListDown").on("StartListDown", this.scrollDown, this);

      this.bus.on("SearchListUp", this.onStartListUp, this);
      this.bus.on("SearchListDown", this.onStartListDown, this);
      this.bus.on("RowsViewClose", this.onClose, this);
    },

    // potrebno za Back
    setRoots: function () {
      if (this.roots.indexOf(this.parentID) === -1) {
        this.roots.push(this.parentID);
      }
    },

    getListID: function () {
      return this.id_list;
    },

    afterRender: function () {
      this.container = document.getElementById("middle_list");

      this.appendRowFake(this.height_other + "px");
      this.appendRowFake(this.height_other + "px");
      var height_css = "translateY(" + Number(-2 * this.height_other) + "px)";

      this.container.style["-webkit-transform"] = height_css;
      this.setFocus();
    },

    appendRow: function (id) {
      var row = document.createElement("div");
      row.setAttribute("id", "vod_row_" + id);
      row.setAttribute("class", "rowxx");
      return this.container.appendChild(row);
    },

    prependRow: function (id) {
      console.log("prependRow", id);
      var row = document.createElement("div");
      row.setAttribute("id", "vod_row_" + id);
      row.setAttribute("class", "rowxx");
      row.style.height = "0px";
      return this.container.insertBefore(row, this.container.children[0]);
    },

    appendRowFake: function (height) {
      var row = document.createElement("div");
      row.setAttribute("id", "vod_row_xx");
      row.setAttribute("class", "rowxx");
      row.style.height = height;
      return this.container.insertBefore(row, this.container.children[0]);
    },

    // skroll dolje
    scrollDown: function () {
      var self = this;

      //console.log("DOWN", this.id_list);
      if (this.id_list < this.Collections.length - 1 && std) {
        ++this.id_list;
        std = false;

        //console.log("this.Collections.length", this.Collections.length);
        //if (this.Collections.length - this.id_list <= 2 && this.id_list < this.collObj.getGroupLength() - 2) {

        console.log(this.collObj);
        if (this.Collections.length - this.id_list <= 2 && this.id_list < this.collObj.groups.length - 2) {
          this.collObj.slice(this.Collections.length, this.Collections.length + 1).then(function (res) {
            if (res != undefined) {
              console.log(res);
              self.Collections.push(res);
              console.log("doljeeeeee", self.id_list);
              var append_id = self.id_list + 2;
              if (!document.getElementById("vod_row_" + append_id)) {
                if (self.Collections.at(append_id)) {
                  self.appendRow(append_id);
                  self.renderList(self.Collections.at(append_id));
                }
              }

              // ne treba animacija reda, ako je zadnji vidljiv
              if (self.animateRowsDown()) {
                self.goDown();
              } else {
                self.setFocus();
                std = true;
              }
            } else {
              std = true;
            }
          });
        } else {
          //dodavanje reda (div)
          var append_id = self.id_list + 2;
          if (!document.getElementById("vod_row_" + append_id)) {
            if (self.Collections.at(append_id)) {
              self.appendRow(append_id);
              self.renderList(self.Collections.at(append_id));
            }
          }
          // std kontrolise iteracije (tek kad se zavrsi animacija)
          if (self.animateRowsDown()) self.goDown();
          else {
            self.setFocus();
            std = true;
          }
        }
      }
    },

    goDown: function () {
      //console.log("animateFirst");
      var row = this.container.children[0];

      $(this.container).addClass("setYpos");

      var self = this;
      //console.log("### this.id_list ###", this.id_list, std);
      if (this.id_list != 3) {
        $(row).animate({ height: 0 }, 300, function () {
          /*
						console.log(self.container.children[0]);
						var dom=self.container.children[0];
						dom.remove();				
					*/
          var del = self.container.removeChild(self.container.children[0]);
          del = null;

          self.setFocus();
          std = true;
        });
      } else {
        $(row).animate({ height: this.height_other }, 300, function () {
          self.setFocus();
          std = true;
          /*
						console.log(self.container.children[0]);
						var dom=self.container.children[0];
						dom.remove();				
						*/
          //self.container.removeChild(self.container.children[0]);
        });
      }
    },

    // skroll gore
    scrollUp: function () {
      console.log("scroll down");

      if (this.id_list > 0 && std) {
        std = false;
        --this.id_list;

        var append_id = this.id_list - 3;

        //console.log('id_list',this.id_list,'append_id',append_id);
        if (append_id >= 0) {
          //Dodavanje normal

          console.log("append_id", append_id);
          if (!document.getElementById("vod_row_" + append_id)) {
            if (this.Collections.at(append_id)) {
              this.prependRow(append_id);
              this.renderList(this.Collections.at(append_id));
            }
          }
          // std kontrolise iteracije (tek kad se zavrsi animacija)
          this.goUp(append_id);
        } else if (append_id >= -3 && append_id < -1) {
          //Dodavanje na vrhu fake ako je -3 i -2
          this.appendRowFake("0px");

          if (this.animateRowsUp()) {
            this.goUp(append_id);
          } else {
            this.setFocus();
            std = true;
          }
        } else if (append_id == -1) {
          //Nije dodavanje fake nego sirenje velike sa 285px na 425px

          //if (this.animateRows()) self.goDown();
          if (this.animateRowsUp()) {
            this.goUp(append_id);
          } else {
            this.setFocus();
            std = true;
          }

          /*
					var row = this.container.children[2];
					console.log(row);
					document.getElementById('control_0').focus();
					*/
        }
      }
    },

    goUp: function (append_id) {
      //console.log("animateFirst");
      var row = this.container.children[0];

      $(this.container).addClass("setYpos");

      var self = this;

      console.log("### this.id_list ###", this.id_list, std);

      if (this.id_list != 2) {
        $(row).animate({ height: this.height_other }, 300, function () {
          var row_pred_sub_last = self.container.children.length - 3;
          var row_sub_last = self.container.children.length - 2;
          var row_last = self.container.children.length - 1;
          var row_in_focus = document.getElementById("vod_row_" + self.id_list);

          console.log(self.container.children[row_sub_last]);
          console.log(self.container.children[row_last]);
          console.log(row_in_focus);

          if (
            self.container.children[row_sub_last] == row_in_focus ||
            self.container.children[row_last] == row_in_focus ||
            self.container.children[row_pred_sub_last] == row_in_focus
          ) {
            console.log("Predzadnji...");
          } else {
            var del = self.container.removeChild(self.container.lastElementChild);
            del = null;
          }
          self.setFocus();
          console.log("UP", self.id_list);
          std = true;
        });
      } else {
        $(row).animate({ height: this.height_first }, 300, function () {
          self.setFocus();
          console.log("UP", self.id_list);
          std = true;
        });
      }
    },

    animateRowsDown: function () {
      //this.firstRow = this.container.children[0];
      //this.secondRow = this.container.children[1];
      this.lastRow = this.container.lastElementChild;

      if (window.innerHeight > this.lastRow.getBoundingClientRect().bottom) {
        console.log("zadnji red se vidi 100%");
        return false;
      } else {
        if (window.innerHeight > this.lastRow.getBoundingClientRect().top) {
          console.log(
            "zadnji red se vidi " + Math.floor(window.innerHeight - this.lastRow.getBoundingClientRect().top) + "px"
          );
        } else {
          console.log("zadnji red se ne vidi");
        }
        return true;
      }
    },

    animateRowsUp: function () {
      //this.firstRow = this.container.children[0];
      //this.secondRow = this.container.children[1];
      this.firstRow = document.getElementById("vod_row_0");

      if (this.firstRow) {
        if (this.firstRow.getBoundingClientRect().top > 0) {
          console.log("Prvi red se vidi 100%");
          return false;
        } else {
          return true;
        }
      }
    },

    setFocus: function () {
      //console.log(this.id_list);
      if (document.getElementById("control_" + this.id_list)) {
        document.getElementById("control_" + this.id_list).focus();
      } else {
        console.log("GRESKA GRESKA !!!!!!!!!!");
      }
    },

    // prikaz jednog reda(liste)
    renderList: function (data) {
      //console.log("render reda");

      //console.log("************************", data);
      var category = data.get("category");
      var list = data.get("list");

      console.log(category);

      console.log(list);
      if (list.length == 0) {
        return false;
      }
      /*
      console.log("position", category.position);
      console.log("id_list", category.id_list);
      console.log("name", category.name);
      console.log("size", category.size);
      console.log("left_shift", category.left_shift);
      console.log("show_content", category.show_content);
      console.log("position_back", category.position_back);
      console.log("progress", category.progress);
      console.log("id_l", category.id_list);
      console.log("image", category.image);
      */

      console.log(category);

      listView = new ListView({
        el: "#" + category.position,
        category: category,
        collection: list,
        display_back: "yes",
        bus: this.bus,
        roots: this.roots // potrebno za Back
      });

      this.$el.append(listView.render());
      this.$el.find("#list-content_" + category.id_list).css("left", category.left_shift + "vw");
      return true;
    },

    render: function () {
      var templ = _.template(this.template);
      var html = templ();
      this.$el.html(html);

      //-------------------------------------------------
      this.TempCollections.forEach(this.renderList, this);
      //-------------------------------------------------

      this.afterRender();
      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();
    }
  });

  return ListRowsView;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "models/channel_focus",
  "models/epg_focus",
  "models/vod_focus",
  "models/vod_content_focus",
  "models/screen_focus",
  "views/listboxView",
  "views/vodContentView",
  "text!page/list.html"
], function (
  $,
  _,
  Backbone,
  chanEpg,
  channel_focus,
  epg_focus,
  vod_focus,
  vod_content_focus,
  screen_focus,
  ListboxView,
  VodContentView,
  template
) {
  var st_complete = true;

  var prevKey = 0;
  var longPress = 0;
  var longPressCnt = 0;
  var speed_change = 0;
  var duration = 300;

  var ListView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      //console.log(this);

      this.x = 0;

      //-------
      this.pointer = 0;
      //-------

      var tmp_list = new Backbone.Collection();
      tmp_list.push(options.collection);
      this.collection = tmp_list;

      this.category = options.category;
      this.list = options.collection;

      this.collect = options.collect;
      this.id_content = 0;
      this.count = this.collection.size();
      this.id_list = options.category.id_list;
      this.name = options.category.name;
      this.size = options.category.size;
      this.progress = options.category.progress;
      this.action_type = options.category.action_type;
      this.bus = options.bus;

      this.position_back = options.category.position_back;
      this.display_back = options.display_back;
      this.image = options.category.image;
      //this.el = document.getElementById(options.el);

      // potrebno za back
      this.type = options.type;
      this.roots = options.roots;

      this.col_width = getRowSizeAnimate(this.size).width;
      console.log(this.col_width);

      this.bus.on("setFocusListContent", this.setFocusListContent, this);
      this.bus.on("ListViewVodClose", this.onClose, this);
      //this.bus.on("setVisibleItems", this.setVisibleItems, this);
    },

    afterRender: function () {
      this.container = this.el.getElementsByClassName("list-content")[0];

      console.log(this.container);
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown"
    },

    keyActionUp: function (e) {
      this.handleKey(e.keyCode, 1);
    },

    keyActionDown: function (e) {
      e.stopPropagation();
      default_key(e.keyCode);

      //dbgLog(e.keyCode);

      switch (e.keyCode) {
        case KEY_RIGHT:
          this.scroll_right();
          this.handleKey(e.keyCode, 0);
          return false;
          break;
        case KEY_LEFT:
          this.scroll_left();
          this.handleKey(e.keyCode, 0);
          return false;
          break;
        case KEY_OK:
        case KEY_ENTER:
          //console.log(this.show_content.st_show);
          var item = this.collection.at(this.id_content).toJSON();
          console.log(item);

          vod_focus.setData(item);
          this.bus.trigger("playVodContent");

          return false;
          break;

        case KEY_DOWN:
          return false;
          break;
        case KEY_UP:
          this.bus.trigger("onSetMovPlayFocus");
          return false;
          break;
        case KEY_HOME:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
        case KEY_BACK:
          console.log(this.roots);
          // uklanjanje zadnjeg ID iz roots (root parent ove liste)
          //this.roots.pop();

          console.log(this.type);

          if (this.roots.length > 0) {
            if (this.type == "movie") {
              console.log("XXXXXXXXXXXXXX");
              var rootID = this.roots[this.roots.length - 1];
              this.bus.trigger("RenderChildren", { ID: rootID, roots: this.roots });
            } else {
              var rootID = this.roots[this.roots.length - 1];
              this.bus.trigger("RenderSezons", { ID: rootID, roots: this.roots });
            }
          } else {
            this.bus.trigger("ExpandMenu");
          }

          return false;
          break;
        default:
      }

      //this.$el.find('#control_' + this.id_list).focus();
    },

    scroll_right: function () {
      //console.log(this.el.getElementsByClassName('list-content')[0]);
      //console.log(this.el.children[1].find);

      //console.log(this.id_content);
      //console.log(this.count);
      //console.log(st_complete);

      if (this.id_content < this.count - 1 && st_complete) {
        st_complete = false;

        var col = this.container.children[0];
        var self = this;
        $(col).animate(
          { width: "0px", marginLeft: "0px", marginRight: "0px" },
          {
            duration: duration,
            complete: function () {
              $(col).remove();
              if (self.id_content + 8 < self.collection.size()) {
                self.appendColumn(self.collection.at(self.id_content + 8));
              }

              st_complete = true;
              self.id_content = self.id_content + 1;
              self.bus.trigger("onSetItemContent", self.collection.at(self.id_content).toJSON());
            }
          }
        );
      } else {
        if (this.id_content <= 0 && this.position_back.right != undefined) $("#" + this.position_back.right).focus();
      }
    },

    scroll_left: function (data) {
      //console.log(this.id_content);
      //console.log(this.count);
      //console.log(st_complete);

      if (this.id_content > 0 && st_complete) {
        this.id_content = this.id_content - 1;

        if (this.id_content - 2 >= 0) {
          this.prependColumn(this.collection.at(this.id_content - 2), "no");
        } else {
          this.prependColumn(this.collection.at(0), "yes");
        }

        var col = this.container.children[0];
        st_complete = false;

        var self = this;

        $(col).animate(
          { width: this.col_width + "vw", marginLeft: "10px", marginRight: "10px" },
          {
            duration: duration,
            complete: function () {
              var row_last = self.container.children.length - 1;

              // brisanje

              if (row_last > 9) {
                //console.log(col_del);
                var col_del = self.container.lastElementChild;
                $(col_del).remove();
              }

              self.bus.trigger("onSetItemContent", self.collection.at(self.id_content).toJSON());

              st_complete = true;
            }
          }
        );
      } else {
        console.log(this.position_back);
        if (this.id_content <= 0 && this.position_back.left != undefined) $("#" + this.position_back.left).focus();
      }
    },

    appendColumn: function (data) {
      var listbox = new ListboxView({
        model: data,
        id_list: this.id_list,
        size: this.size,
        image: this.image,
        progress: this.progress,
        box_empty: "no",
        bus: this.bus
      });
      $(this.container).append(listbox.render().$el);
    },

    prependColumn: function (data, stats) {
      //console.log(data);

      var listbox = new ListboxView({
        model: data,
        id_list: this.id_list,
        size: this.size,
        image: this.image,
        progress: this.progress,
        box_empty: stats,
        bus: this.bus
      });
      //listbox.className="listbox";

      //console.log(listbox);
      //getElementsByClassName('item')[0]
      //$(this.container).prepend(listbox.render().$el.find('item').css({'width':'0px'}));

      var root = listbox.render().$el;
      $(root).css({ width: "1px", marginLeft: "0px", marginRight: "0px" });
      $(this.container).prepend(root);
    },

    setFocusListContent: function (data) {
      var id_c = $("#control_" + data.id_list).attr("select");

      //$("div[name = 'start_name']").removeClass('active');
      //$('#id_start_name_' + id_c).addClass('active');

      this.setFocusContent(id_c);
    },

    setFocusContent: function (id_content) {
      $("div[name = 'start_name']").removeClass("active");
      $("#id_start_name_" + id_content).addClass("active");
    },

    handleKey: function (code, isKeyUp) {
      //console.log('############################');
      if (isKeyUp) {
        if (prevKey == code) {
          prevKey = 0;
          longPress = 0;
          longPressCnt = 0;
          duration = 300;
        }
        return;
      }

      if (prevKey == code) {
        longPress = 1;
        longPressCnt += 1;
      }
      prevKey = code;

      // RAZRADA
      if (longPress == 1) {
        if (longPressCnt == 3) {
          duration = 150;
        } else if (longPressCnt == 5) {
          duration = 100;
        } else if (longPressCnt == 7) {
          duration = 50;
        }

        //dbgLog("KEYDOWN NOW: " + code + " LONG PRESS: " + longPressCnt + '   speed-' + speed_change + '  duration-' + duration);
        //console.log("LONG PRESS FOR KEY: " + code + " LONG PRESS COUNT: " + longPressCnt + '   x-' + speed_change);
      } else {
        if (longPress == 3) {
          duration = 150;
        } else if (longPress == 5) {
          duration = 100;
        } else if (longPress == 7) {
          duration = 50;
        }
        //dbgLog("KEYDOWN NOW: " + code + " LONG PRESS: " + longPress + ' x');
        //console.log("KEYDOWN NOW: " + code + " LONG PRESS: " + longPress);
      }
    },

    render: function () {
      var templ = _.template(template);
      var html = templ({ id_list: this.id_list, size: this.size, name: this.name, x_shift: -50 });
      this.$el.html(html);

      //console.log(this.$el);

      //create empty
      var listboxe = new ListboxView({
        model: this.collection.at(0),
        id_list: this.id_list,
        size: this.size,
        image: this.image,
        progress: this.progress,
        box_empty: "yes",
        bus: this.bus
      });
      this.$el.find(".list-content").append(listboxe.render().$el);

      var listboxe = new ListboxView({
        model: this.collection.at(0),
        id_list: this.id_list,
        size: this.size,
        image: this.image,
        progress: this.progress,
        box_empty: "yes",
        bus: this.bus
      });
      this.$el.find(".list-content").append(listboxe.render().$el);

      //this.tmp_collection=this.collection.first(8);

      var tmp_rows = Backbone.Model.extend();
      this.tempCollections = new Backbone.Collection(this.collection.first(8), { model: tmp_rows });

      this.tempCollections.each(function (data) {
        var listbox = new ListboxView({
          model: data,
          id_list: this.id_list,
          size: this.size,
          image: this.image,
          progress: this.progress,
          box_empty: "no",
          bus: this.bus
        });
        //console.log(listbox);
        this.$el.find(".list-content").append(listbox.render().$el);
      }, this);

      this.$el.find("#control_" + this.id_list).attr("select", this.collection.at(0).get("id"));

      //---
      //this.setFocus();
      //---

      //this.setVisibleItems({ "event": "init" });

      //console.log(this.$el);
      //console.log(this.tempCollections.toJSON());

      this.afterRender();

      return this;

      /*
            var templ = _.template(template);
            var html = templ();
            this.$el.html(html);
            return this;
            */
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      //this.remove();
    }
  });

  return ListView;
});

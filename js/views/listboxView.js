define(["jquery", "underscore", "backbone", "text!page/listbox.html"], function($, _, Backbone, template) {
    var ListboxView = Backbone.View.extend({
        tagName: "span",
        className: "listbox",

        initialize: function(options) {
            //console.log('opcije', options);
            this.model = options.model;

            this.box_empty = options.box_empty;
            this.progress = options.progress;
            this.size = options.size;
            this.id_list = options.id_list;
            this.image = options.image;
            this.bus = options.bus;

            //console.log("@@@@@@@@@@@@image", this.image);

            this.$el.addClass("list-box-" + options.size);
            //console.log(this.$el);

            // setovanje id-ja na parentu/elementu
            /* if(this.box_empty == 'no'){
			  this.$el.attr('id', 'box-' + this.model.get('id')); 	
			}  */
        },

        render: function() {
            /* console.log(this.model);
                  console.log('this.size', this.size);			
                  console.log(this.model.toJSON());
                  console.log('this.progress', this.progress); */

            //console.log('model u listbox', this.model.toJSON());

            var result = this.model.toJSON();
            //console.log(this.image);
            //console.log(result);

            //console.log(result.picture.hasOwnProperty(this.image));

            if (result.picture.hasOwnProperty(this.image)) {
                //console.log("*************", result.picture[this.image]);
                image = result.picture[this.image];
            } else {
                result.picture[this.image] = "#";
                image = result.picture[this.image];
                //console.log("nema nema nema nema");
            }

            //console.log("XXXXXXXXXXXXXXXXXXXXXXXXXXXX");
            //console.log(result);

            var progress_val = 0;

            if (this.progress != 0) {

                var t_end = (this.model.get("endTime") - this.model.get("startTime")) / 1000;
                var t_cur = (getCurentEpoch() - this.model.get("startTime")) / 1000;

                console.log(this.model.get("name"));
                console.log('t_end', t_end);
                console.log('t_cur', t_cur);
                console.log('Math.floor(t_cur / t_end * 100)', Math.floor(t_cur / t_end * 100));

                progress_val = Math.floor(t_cur / t_end * 100);


            }


            console.log('progress_valprogress_valprogress_val', progress_val);

            var templ = _.template(template);
            var html = templ({ result: this.model.toJSON(), id_list: this.id_list, size: this.size, image: image, progress: this.progress, progress_val: progress_val, box_empty: this.box_empty });
            this.$el.html(html);
            return this;
        },
    });

    return ListboxView;
});
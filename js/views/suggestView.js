define(["jquery", "underscore", "backbone", "text!page/suggest.html"], function ($, _, Backbone, template) {
  var st_complete = "";

  var SuggestView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.suggest = options.suggest;
      this.count = this.suggest.length - 1;
      this.x = 0;
      this.bus = options.bus;
      this.bus.on("SetFocusSuggest", this.onSetFocus, this);
    },

    events: {
      keydown: "keyActionDown",
    },

    keyActionDown: function (e) {
      //e.stopPropagation();

      console.log("down");

      default_key(e.keyCode);

      //dbgLog(e.keyCode);

      switch (e.keyCode) {
        case KEY_DOWN:
          if (this.x < this.count) {
            this.x++;
            this.onSetFocus();
          }
          return false;
          break;
        case KEY_UP:
          if (this.x > 0) {
            this.x--;
            this.onSetFocus();
          } else $("#key_keyboard").focus();

          return false;
          break;
        case KEY_OK:
        case KEY_ENTER:
          var $focused = $(":focus");
          $("#id_search").text($focused.text());
          this.bus.trigger("RenderKeyboardSearch", { value: $("#id_search").text() });
          return false;
          break;
        case KEY_HOME:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_BACK:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        default:
      }

      console.log(this.id_key);

      //this.$el.find('#control_' + this.id_list).focus();
    },
    onSetFocus: function () {
      this.$el.find("div[tabindex=" + this.x + "]").focus();
    },

    render: function () {
      console.log(this.suggest);
      var templ = _.template(template);
      var html = templ({ result: this.suggest });

      this.$el.html(html);
      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      //this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;
    },
  });

  return SuggestView;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "collections/seriesCollections",
  "models/screen_focus",
  "views/listRowsView",
  "text!page/vod.html"
], function ($, _, Backbone, SeriesCollections, screen_focus, ListRowsView, template) {
  var st_complete = "";
  var seriesColl = {};

  var SeriesView = Backbone.View.extend({
    tagName: "span",

    /*
         action_type    
         1 - player
         2 - preview
         3 - children
         4 - vod_content


         vr_media 
         1 - live
         2 - catchup
         3 - catchup_show
         4 - vod
         5 - vod_show
         
        */

    initialize: function (options) {
      // potrebno za Back
      this.roots = [];

      this.bus = options.bus;
      this.bus.on("RenderSeriesParent", this.create, this);
      this.bus.on("RenderSeriesChildren", this.onRenderVodChildren, this);
      this.bus.on("RenderSezons", this.onRenderSezons, this);

      //this.bus.on("RenderChildren", this.onRenderVodChildren, this);
      this.bus.on("SeriesClose", this.onClose, this);
    },

    onRenderVodChildren: function (item) {
      this.config = {
        first: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "series_episodes",
          image: "posters"
        },
        other: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "series_episodes",
          image: "posters"
        }
      };

      console.log(item);
      this.onClose();
      this.create(item);
    },

    onRenderSezons: function (item) {
      this.config = {
        first: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "series_content",
          image: "posters"
        },
        other: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "series_content",
          image: "posters"
        }
      };

      console.log(item);
      this.onClose();
      this.createEpisodes(item);
    },

    create: function (item) {
      // potrebno za Back
      // typeof item.roots !== "undefined"

      this.config = {
        first: {
          size: "plg",
          left_shift: -23.4375,
          progress: 0,
          position_back: "",
          action_type: "series_episodes",
          image: "posters"
        },
        other: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "series_episodes",
          image: "posters"
        }
      };

      console.log("SERIJA");

      loaderShow(true);

      if (item.hasOwnProperty("roots")) this.roots = item.roots;

      seriesColl = new SeriesCollections({ ID: item.ID, config: this.config });
      //console.log(seriesColl);
      var self = this;
      seriesColl.init().then(function () {
        seriesColl.getFirst(3).then(function (res) {
          self.collection = res;
          console.log(self.collection);
          self.render();
          loaderShow(false);
        });
      });
    },

    createEpisodes: function (item) {
      // potrebno za Back
      // typeof item.roots !== "undefined"

      console.log(item);
      loaderShow(true);

      if (item.hasOwnProperty("roots")) this.roots = item.roots;

      seriesColl = new SeriesCollections({ ID: item.ID, config: this.config });
      //console.log(seriesColl);
      var self = this;
      seriesColl.initEpisodes().then(function (res) {
        console.log(res);

        //if()
        var max_num = res.length <= 3 ? res.length : 3;

        console.log(max_num);

        seriesColl.getFirstSeasons(max_num).then(function (res) {
          self.collection = res;
          console.log(self.collection);
          self.render();
          loaderShow(false);
        });
      });
    },

    setAttribute: function (data) {},

    render: function () {
      console.log(seriesColl);
      console.log(this.collection);

      var listRowsView = new ListRowsView({
        el: "#other_container",
        template: template,
        collObj: seriesColl,
        collect: this.collection,
        config: this.config,
        bus: this.bus,
        roots: this.roots // potrebno za Back
      });

      $("#other").append(listRowsView.render().el);
      /*
            var tag = StartListCollections.at(0);
            this.$el.find("#control_" + tag.get("id_list")).addClass("focus");
            this.$el.find("#control_" + tag.get("id_list")).focus();
            */

      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return SeriesView;
});

define([
    'jquery',
    'underscore',
    'backbone',
    'collections/menuCollections',
    'models/menu_focus',
    'text!page/temporarySettings.html'
], function($, _, Backbone, MenuCollections, MenuFocus,Template){

    var st_complete = "0";
    
    var SettingsView = Backbone.View.extend({
        
        initialize: function(options){
            this.template = Template;
            this.el = options.el;
            this.bus = options.bus;
            this.bus.on("SettingsClose", this.onClose, this);
            this.activeOptionId = 0;
            this.idMap = ['#upArrow', '#rightArrow', '#downArrow', '#leftArrow','#changeTheme'];
        },

        render: function(){
            var template = _.template(this.template);
            var html = template();
            this.$el.html(html);
            this.$el.find("#setting_page").focus();
            this.selectOption();
        },

        onClose : function(){
            this.undelegateEvents();
            this.$el.removeData().unbind();

            this.$el.empty();
            this.unbind();
            this.stopListening();
            return this;
        },

        selectOption : function(){
            $(this.idMap[this.activeOptionId]).css("color","white");
        },

        deselectOption : function(){
            $(this.idMap[this.activeOptionId]).css("color", "grey");
        },

        events : {
            'keydown': 'keyAction'
        },


       keyAction: function(e) {
            e.stopPropagation();
            default_key(e.keyCode);
            switch (e.keyCode) {
                case KEY_UP:
                        if(this.activeOptionId == 0) break;
                        this.deselectOption();
                        if(this.activeOptionId == 4) this.activeOptionId = 0;
                        if(this.activeOptionId == 2) this.activeOptionId = 4;
                        this.selectOption();
                break;
                case KEY_RIGHT:
                    if(this.activeOptionId == 1) break;
                    this.deselectOption();
                    if(this.activeOptionId == 4) this.activeOptionId = 1;
                    if(this.activeOptionId == 3) this.activeOptionId = 4;
                    this.selectOption();
                break;
                case KEY_DOWN:
                        if(this.activeOptionId == 2) break;
                        this.deselectOption();
                        if(this.activeOptionId == 4) this.activeOptionId = 2;
                        if(this.activeOptionId == 0) this.activeOptionId = 4;
                        this.selectOption();
                    return false;
                    break;
                case KEY_LEFT:
                    if(this.activeOptionId == 3) break;
                    this.deselectOption();
                    if(this.activeOptionId == 4) this.activeOptionId = 3;
                    if(this.activeOptionId == 1) this.activeOptionId = 4;
                    this.selectOption();
                    return false;
                    break;
                case KEY_OK:                    
                case KEY_ENTER:
                    if(this.activeOptionId == 0){
                        console.log("upArrow");
                        epgScale(0,1);
                    }else if(this.activeOptionId == 1){
                        console.log("rightArrow");
                        epgScale(1,0);
                    }else if(this.activeOptionId == 2){
                        console.log("downArrow");
                        epgScale(0,-1);
                    }else if(this.activeOptionId == 3){
                        console.log("leftArrow");
                        epgScale(-1,0);
                    }else if(this.activeOptionId == 4){
                        console.log("change theme");
                        if ($('link[href="css/style.css"]').attr('href') == 'css/style.css') {
                            $('link[href="css/style.css"]').attr('href', 'css/style_dark.css');
                            $("#btn_logo").attr("src", "image/logo_dark.png");
                        } else if ($('link[href="css/style_dark.css"]').attr('href') == 'css/style_dark.css') {
                            $('link[href="css/style_dark.css"]').attr('href', 'css/style_dark_orange.css');
                            $("#btn_logo").attr("src", "image/logo_dark.png");
                        } else if ($('link[href="css/style_dark_orange.css"]').attr('href') == 'css/style_dark_orange.css') {
                            $('link[href="css/style_dark_orange.css"]').attr('href', 'css/style.css');
                            $("#btn_logo").attr("src", "image/logo_gray.png");
                        }
                    }else{
                        console.log("Greska koja ne bi trebala da se javlja.");
                    }
                    return false;
                    break;
                case KEY_HOME:
                    if (st_complete !== "1") this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_MENU:
                    if (st_complete !== "1") this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_RETURN:
                    if (st_complete !== "1") this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_BACK:
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                default:
            }
            return false;
        },
    });
    return SettingsView;
});
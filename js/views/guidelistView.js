define([
    'jquery',
    'underscore',
    'backbone',
    'collections/epgCollections',
    'views/guideboxView',
    'text!page/guidelist.html'
], function($, _, Backbone, EpgCollections, GuideboxView, template) {


    var st_complete = "";

    var GuideRow = Backbone.View.extend({

        tagName: "span",

        initialize: function(options) {

            this.x = 300;

            this.id_epg = options.epg_min + 10;
            this.id_epg_now = (options.epg_min + 10);

            this.id_channel = options.id_channel;
            this.epg_min = options.epg_min;
            this.epg_max = options.epg_max;
            this.channel = options.channel;
            this.bus = options.bus;

            this.bus.off('setFooteritem').on("setFooteritem", this.setFooteritem, this);

        },

        events: {
            'keyup': 'keyActionUp',
            'keydown': 'keyActionDown',
        },

        keyActionDown: function(e) {
            e.stopPropagation();

            default_key(e.keyCode);

            //this.x = channel_focus.get("id");
            //this.count = channel.size() - 1;

            switch (e.keyCode) {
                case KEY_LEFT:

                    if (st_complete !== "1") {
                        if (this.id_epg > this.epg_min) {
                            this.id_epg = this.id_epg - 1;

                            this.x = 660;

                            /*
                            st_complete = "1";
                            var id_c = this.id_channel;
                            var id_e = this.id_epg;
                            $("#epg_live_content_" + this.id_channel).animate({ left: this.x + 'px' }, {
                                duration: 100,
                                complete: function() {
                                    $("#epg_live_content_" + id_c).removeAttr("style");
                                    st_complete = "";
                                }
                            });
                            */

                            console.log(this.id_epg);

                            var epg_tpm = this.getEpgList(this.id_channel, this.id_epg);
                            this.setEpgList('left', this.id_channel, epg_tpm);

                        }
                    }
                    return false;
                    break;
                case KEY_RIGHT:


                    if (st_complete !== "1") {


                        //console.log('id_epg', this.id_epg, 'max', this.epg_max - 3);

                        if (this.id_epg <= this.epg_max - 3) {
                            this.id_epg = this.id_epg + 1;
                            this.x = -660;

                            /*
                                st_complete = "1";
                                var id_c = this.id_channel;
                                var id_e = this.id_epg;
                            
                                $("#epg_live_content_" + this.id_channel).animate({ left: this.x + 'px' }, {
                                    duration: 2000,
                                    complete: function() {
                                        $("#epg_live_content_" + id_c).removeAttr("style");
                                        st_complete = "";
                                    }
                                });
                                */
                            console.log(this.id_epg);

                            var epg_tpm = this.getEpgList(this.id_channel, this.id_epg);
                            this.setEpgList('right', this.id_channel, epg_tpm);

                        }
                    }

                    return false;
                    break;
                case KEY_DOWN:
                    this.bus.trigger("GuideListDown");
                    return false;
                    break;
                case KEY_UP:
                    this.bus.trigger("GuideListUp");
                    return false;
                    break;
                case KEY_HOME:
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_MENU:
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_RETURN:
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_BACK:
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_OK:
                    var chId = Number(channel_focus.get("id") + 1);
                    //dbgLog(chId);
                    playChan(1);
                    return false;
                    break;
            }

            this.$el.find('#ch_epg_1').focus();
            //console.log('fokusirano');

            return false;
        },



        getEpgList: function(id_c, id_e) {

            var list = new Backbone.Collection(EpgCollections.filter(function(model) {
                return model.get('id_channel') == id_c && model.get('id') > id_e - 2;
            })).first(5);

            return list;
        },


        render: function() {

            var templ = _.template(template);
            var html = templ({ id_channel: this.id_channel });
            this.$el.html(html);


            var guideboxView = new GuideboxView({
                channel: this.channel,
                bus: this.bus
            });

            this.$el.find('.channel-epg-data').append(guideboxView.render().$el);
            this.setEpgList('right', this.id_channel, this.getEpgList(this.id_channel, this.id_epg));

            $('#ch_epg_1').focus();

            return this;
        },


        setEpgList: function(shift, id_c, lst) {

            //console.log(lst);
            //console.log('shift', shift);
            //console.log('lst.length', lst.length);

            if (lst.length > 4) {
                this.setEpgitem(0, id_c, lst[0]);
                this.setEpgitem(1, id_c, lst[1]);
                this.setEpgitem(2, id_c, lst[2]);
                this.setEpgitem(3, id_c, lst[3]);
                this.setEpgitem(4, id_c, lst[4]);
            } else if (shift == 'left' && lst.length == 2) {
                this.setEpgitem(1, id_c, lst[0]);
                this.setEpgitem(2, id_c, lst[1]);
                this.setEpgitem("");
            } else if (shift == 'left' && lst.length == 1) {
                this.setEpgitem(1, id_c, "");
                this.setEpgitem(2, id_c, lst[0]);
                this.setEpgitem("");
            } else if (shift == 'right' && lst.length == 4) {
                this.setEpgitem(0, id_c, lst[0]);
                this.setEpgitem(1, id_c, lst[1]);
                this.setEpgitem(2, id_c, lst[2]);
                this.setEpgitem(3, id_c, "");
                this.setEpgitem(4, id_c, "");
            } else if (shift == 'right' && lst.length == 1) {
                this.setEpgitem(1, id_c, "");
                this.setEpgitem(2, id_c, lst[0]);
                this.setEpgitem(3, id_c, "");
            }

            return false;
        },


        setEpgitem: function(num, id_c, row) {


            if (row != "") {

                var s_date = new Date(Number(row.get("startTime")));
                var e_date = new Date(Number(row.get("endTime")));


                $('#ch_epg_' + row.get("id_channel") + '_name_' + num).text(row.get("name"));
                $('#ch_epg_' + row.get("id_channel") + '_data_' + num).text(formatTime(s_date) + " - " + formatTime(e_date));
                $('#ch_epg_' + row.get("id_channel") + '_time_' + num).text(formatDate(s_date));

                $('#ch_epg_' + row.get("id_channel") + '_blue_' + num).removeClass(function(index, className) {
                    return (className.match(/\bw-\S+/g) || []).join(' ');
                });

                $('#ch_epg_' + id_c + '_blue_' + num).removeAttr("style");
                $('#ch_epg_' + row.get("id_channel") + '_blue_' + num).addClass('w-0');


                if (num == 2) {
                    $('#ch_epg_' + row.get("id_channel") + '_id_' + num).val(row.get("id"));
                    $("#id_epg_footer_img").attr("src", row.get("image"));
                    $("#id_epg_footer_name").text(row.get("name"));
                    $("#id_epg_footer_date").text(formatTime(s_date) + " - " + formatTime(e_date) + " / " + formatDate(s_date));
                }

                /*
                if (this.id_epg > this.id_epg_now) $('#ch_epg_' + row.get("id_channel") + '_blue_' + num).addClass('w-0');
                else if (this.id_epg <= this.id_epg_now) $('#ch_epg_' + row.get("id_channel") + '_blue_' + num).addClass('w-100');
                */

            } else {
                $('#ch_epg_' + id_c + '_name_' + num).text("");
                $('#ch_epg_' + id_c + '_data_' + num).text("");
                $('#ch_epg_' + id_c + '_time_' + num).text("");

                /*
                $('#ch_epg_' + id_c + '_blue_' + num).removeClass(function(index, className) {
                    return (className.match(/\bw-\S+/g) || []).join(' ');
                });
                */

                $('#ch_epg_' + id_c + '_blue_' + num).css("display", "none");

            }

            return false;
        },


        setFooteritem: function(data) {

            console.log(data.id_channel);
            console.log(this.id_epg);

            item_data = EpgCollections.get(Number($('#ch_epg_' + data.id_channel + '_id_2').val()));

            var s_date = new Date(Number(item_data.get("startTime")));
            var e_date = new Date(Number(item_data.get("endTime")));
            $("#id_epg_footer_img").attr("src", item_data.get("image"));
            $("#id_epg_footer_name").text(item_data.get("name"));
            $("#id_epg_footer_date").text(formatTime(s_date) + " - " + formatTime(e_date) + " / " + formatDate(s_date));

        },



    });

    return GuideRow;
});
define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "velocity",
  "models/menu_focus",
  "models/channel_focus",
  "collections/menuCollections",
  "views/startView",
  "views/searchView",
  "views/guideTVView",
  "views/vodView",
  "views/seriesView",
  "views/radioView",
  "views/vodContentView",
  "views/sliderView",
  "text!page/menu.html"
], function (
  $,
  _,
  Backbone,
  tv,
  velocity,
  menu_focus,
  channel_focus,
  menu,
  StartView,
  SearchView,
  GuideTVView,
  VodView,
  SeriesView,
  RadioView,
  VodContentView,
  SliderView,
  template
) {
  var st_complete = "";

  var MenuView = Backbone.View.extend({
    initialize: function (options) {
      this.id_menu = menu_focus.get("id");
      //this.startView = new StartView({ el: "#other_container", bus: options.bus });

      this.bus = options.bus;
      this.bus.on("ExpandMenu", this.onExpandMenu, this);
      this.bus.on("CollapsMenu", this.onCollapsMenu, this);
      this.bus.on("setVodContent", this.setVodContent, this);

      this.bus.on("CloseAll", this.closeView, this);
      this.bus.on("setSliderView", this.setSliderView, this);
      this.bus.on(
        "GoHome",
        function () {
          this.id_menu = 1;
          menu_focus.set({ id: this.id_menu }, { validate: true });
          this.onSetFocus(menu.at(menu_focus.get("id")));
          this.showStart();
        },
        this
      );
    },

    setVodContent: function (data) {
      this.closeView();
      $("#other_container").empty();

      var vodContent = new VodContentView({
        el: "#other_container",
        id_item: data.id.toString(),
        item_data: data.item_data,
        bus: this.bus
      });
      vodContent.render();

      console.log(data);
    },

    onExpandMenu: function () {
      if (st_complete !== "1") {
        console.log("SIRIM MENI", $(menu.at(menu_focus.get("id")).get("menu")));
        st_complete = "";
        set_previous_element_focus();

        $("#epg-list").hide();
        $("#category-list").hide();
        $("#channel-list").hide();
        $("#menucateg_container").hide();
        //dbgLog('meni: ' + menu.at(menu_focus.get("id")).get("menu"))
        $(menu.at(menu_focus.get("id")).get("menu")).focus();
        this.onAnimateMenu();
      }

      /*
                  $("#menu").addClass('active');
                  $('#menu_liveTV').focus();
                  */
    },

    onCollapsMenu: function () {
      if (st_complete !== "1") {
        st_complete = "";
        this.onAnimateMenu();
      }
    },

    setSliderView: function () {
      var sliderView = new SliderView({ el: "#other_container", id_item: "1", bus: this.bus });
      sliderView.render();
    },

    onAnimateMenu: function () {
      if (st_complete !== "1") {
        st_complete = "1";

        if ($("#menu").hasClass("active")) {
          $("#menu_name_search").css({ left: -300 });
          $("#menu_name_start").css({ left: -300 });
          $("#menu_name_liveTV").css({ left: -300 });
          //$("#menu_name_guideTV").css({ left: -300 });
          $("#menu_name_movies").css({ left: -300 });
          $("#menu_name_series").css({ left: -300 });
          $("#menu_name_radio").css({ left: -300 });
          $("#menu_name_settings").css({ left: -300 });
          $("#menu_name_user").css({ left: -300 });
          $("#menu").removeClass("active");
          st_complete = "";
        } else {
          $("#menu").addClass("active");
          $("#menu_name_search").animate({ left: 50 }, 100);
          $("#menu_name_start").animate({ left: 50 }, 130);
          $("#menu_name_liveTV").animate({ left: 50 }, 160);
          //$("#menu_name_guideTV").animate({ left: 50 }, 200);
          $("#menu_name_movies").animate({ left: 50 }, 200);
          $("#menu_name_series").animate({ left: 50 }, 250);
          $("#menu_name_radio").animate({ left: 50 }, 300);
          $("#menu_name_settings").animate({ left: 50 }, 350);
          $("#menu_name_user").animate(
            { left: 50 },
            {
              duration: 500,
              complete: function () {
                st_complete = "";
              }
            }
          );
        }
      }

      return false;
    },

    events: {
      keydown: "keyAction",
      "click #menu_search": "showSearch",
      "click #menu_start": "showStart",
      "click #menu_liveTV": "showLiveTV",
      "click #menu_guideTV": "showGuideTV",
      "click #menu_movies": "showMovies",
      "click #menu_series": "showSeries",
      "click #menu_radio": "showRadio",
      "click #menu_settings": "showSettings",
      "click #menu_user": "showUser"
    },

    keyAction: function (e) {
      var code = e.keyCode || e.which;
      e.stopPropagation();

      default_key(e.keyCode);

      this.id_menu = menu_focus.get("id");

      switch (e.keyCode) {
        case KEY_UP:
          if (this.$el.find("#menu").hasClass("active")) {
            this.id_menu = this.id_menu - 1;
            menu_focus.set({ id: this.id_menu }, { validate: true });
            this.onSetFocus(menu.at(menu_focus.get("id")));
          }
          break;
        case KEY_DOWN:
          if (this.$el.find("#menu").hasClass("active")) {
            this.id_menu = this.id_menu + 1;
            menu_focus.set({ id: this.id_menu }, { validate: true });
            this.onSetFocus(menu.at(menu_focus.get("id")));
          }
          break;
        case KEY_BACK:
          this.bus.trigger("GoHome");
          break;
        case KEY_HOME:
          if ($("#menu").hasClass("active")) {
            this.onCollapsMenu();
            $("#" + $("#id_previous_focus").attr("focus_element")).focus();
          } else {
            this.onExpandMenu();
          }
          break;
        case KEY_MENU:
          if ($("#menu").hasClass("active")) {
            this.onCollapsMenu();
            $("#" + $("#id_previous_focus").attr("focus_element")).focus();
          } else {
            this.onExpandMenu();
          }
          break;
        case KEY_ENTER:
          var focused = document.activeElement;

          console.log(focused);

          if (focused.id == "menu_search") {
            this.showSearch();
          } else if (focused.id == "menu_start") {
            this.showStart();
          } else if (focused.id == "menu_guideTV") {
            this.showGuideTV();
          } else if (focused.id == "menu_liveTV") {
            this.showLiveTV();

            $("#epg-list").show();
            $("#category-list").show();
            $("#channel-list").show();
            $("#menucateg_container").show();

            $("#channel").focus();

            /*
                        var chId = Number(channel_focus.get("id"));
                        var ChanKey = tv.tvChannels[chId].ChanKey;
                        */
            dbgLog(channel_focus.get("ChanKey"));
            if (!channel_focus.get("subscribed")) {
              console.log("Niste pretplaceni");
              $("#chanPin").hide();
              $("#chanSub").show();
              // fokusKanala = false;
            } else if (channel_focus.get("adult")) {
              console.log("Adult");
              $("#chanSub").hide();
              $("#chanPin").show();
            } else {
              $("#chanSub").hide();
              $("#chanPin").hide();
              player.playV3(channel_focus.get("ChanKey"), channel_focus.get("mediaId"), 0, 0);
              //player.joinLive(channel_focus.get("mediaId"));
            }
          } else if (focused.id == "menu_movies") {
            this.showMovies();
          } else if (focused.id == "menu_series") {
            this.showSeries();
          } else if (focused.id == "menu_radio") {
            this.showRadio();
          } else if (focused.id == "menu_settings") {
            this.showSettings();
          }

          break;
      }

      return false;
    },

    onSetFocus: function (itme) {
      this.$el.find("#menu").parent().find("div[tabindex]").removeClass("focus");
      this.$el.find(itme.get("menu")).addClass("focus");
      this.$el.find(itme.get("menu")).focus();
    },

    render: function () {
      var templ = _.template(template);
      this.$el.html(templ);

      _.defer(function () {});

      this.onSetFocus(menu.at(menu_focus.get("id")));

      return this;
    },

    showSearch: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }

      this.closeView();
      player.stop();
      this.searchView = new SearchView({ el: "#other_container", bus: this.bus });
      this.searchView.create();

      /*
                  $('.id_loading').show();

                  var $images = $('div#search_page img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
                  */
    },

    showStart: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }
      this.closeView();
      player.stop();
      this.startView = new StartView({ el: "#other_container", bus: this.bus });
      this.startView.create();

      /*
                  $('.id_loading').show();

                  var $images = $('div#start_page img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
              */
    },

    showLiveTV: function () {
      this.closeView();

      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }
    },

    showGuideTV: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }

      this.closeView();
      player.stop();

      this.guideTVView = new GuideTVView({ el: "#other_container", bus: this.bus });
      this.guideTVView.render();

      /*
                  $('.id_loading').show();

                  var $images = $('div#guidetv img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
                  */
    },

    showMovies: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }

      this.closeView();
      player.stop();
      this.vodView = new VodView({ el: "#other_container", bus: this.bus });
      this.vodView.create({ ID: "7000" });

      /*
                  $('.id_loading').show();

                  var $images = $('div#vod_page img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
                  */
    },

    showSeries: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }

      this.closeView();
      player.stop();
      this.seriesView = new SeriesView({ el: "#other_container", bus: this.bus });
      this.seriesView.create({ ID: "7000" });

      /*
                  $('.id_loading').show();

                  var $images = $('div#series_page img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
                  */
    },

    showRadio: function () {
      if ($("#menu").hasClass("active")) {
        this.onCollapsMenu();
      } else {
        this.onExpandMenu();
      }

      this.closeView();
      player.stop();
      this.radioView = new RadioView({ el: "#other_container", bus: this.bus });
      this.radioView.render();

      /*
                  $('.id_loading').show();

                  
                  var $images = $('div#series_page img'),
                      preloaded = 0,
                      total = $images.length;

                  $images.on('load', function() {
                      if (++preloaded === total) {
                          $('.id_loading').fadeOut(100);
                      }
                  });
                  */
    },

    showSettings: function () {
      console.log($('link[href="css/style.css"]').attr("href"));

      if ($('link[href="css/style.css"]').attr("href") == "css/style.css") {
        $('link[href="css/style.css"]').attr("href", "css/style_dark.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      } else if ($('link[href="css/style_dark.css"]').attr("href") == "css/style_dark.css") {
        $('link[href="css/style_dark.css"]').attr("href", "css/style.css");
        $("#btn_logo").attr("src", "image/logo.svg");
      }

      console.log("Prikazi showSettings x");
    },

    showUser: function () {
      console.log("Prikazi showUser x");
    },

    closeView: function () {
      this.bus.trigger("ListViewClose");
      this.bus.trigger("RowsViewClose");
      this.bus.trigger("SearchClose");
      this.bus.trigger("StartClose");
      this.bus.trigger("StartClose");
      this.bus.trigger("GuideTVClose");
      this.bus.trigger("VodClose");
      this.bus.trigger("SeriesClose");

      if (typeof this.guideTVView !== "undefined") {
        this.guideTVView.remove();
        //$("#other_view_container").append("<div id='other_container'></div>");
      }

      $("#other_view_container").append("<div id='other_container'></div>");

      /*
                  if (typeof this.searchView === "undefined") {
                      this.searchView.remove();
                      $("#other_view_container").append("<div id='other_container'></div>");

                  }

                  if (typeof this.startView === "undefined") {
                      this.startView.remove();
                      $("#other_view_container").append("<div id='other_container'></div>");
                  }
                  */
    }
  });

  return MenuView;
});

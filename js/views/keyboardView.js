define(["jquery", "underscore", "backbone", "text!page/keyboard.html"], function ($, _, Backbone, template) {
  var st_complete = "";

  var KeyboardView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.search_val = options.search_val;
      this.form = options.form;

      this.keyboard = [
        [
          {
            id: "keyb_1",
            value: "a",
            ico: "a",
            tag: "span1",
            left: "",
            right: "keyb_2",
            top: "",
            down: "keyb_7",
            x: 0,
            y: 0,
            width: 3.515625
          },
          {
            id: "keyb_2",
            value: "b",
            ico: "b",
            tag: "span1",
            left: "keyb_1",
            right: "keyb_3",
            top: "",
            down: "keyb_8",
            x: 3.515625,
            y: 0,
            width: 3.515625
          },
          {
            id: "keyb_3",
            value: "c",
            ico: "c",
            tag: "span1",
            left: "keyb_2",
            right: "keyb_4",
            top: "",
            down: "keyb_9",
            x: 7.03125,
            y: 0,
            width: 3.515625
          },
          {
            id: "keyb_4",
            value: "d",
            ico: "d",
            tag: "span1",
            left: "keyb_3",
            right: "keyb_5",
            top: "",
            down: "keyb_10",
            x: 10.546875,
            y: 0,
            width: 3.515625
          },
          {
            id: "keyb_5",
            value: "e",
            ico: "e",
            tag: "span1",
            left: "keyb_4",
            right: "keyb_6",
            top: "",
            down: "keyb_11",
            x: 14.0625,
            y: 0,
            width: 3.515625
          },
          {
            id: "keyb_6",
            value: "f",
            ico: "f",
            tag: "span1",
            left: "keyb_5",
            right: "call_slid",
            top: "",
            down: "keyb_12",
            x: 17.578125,
            y: 0,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_7",
            value: "g",
            ico: "g",
            tag: "span1",
            left: "",
            right: "keyb_8",
            top: "keyb_1",
            down: "keyb_13",
            x: 0,
            y: 6.25,
            width: 3.515625
          },
          {
            id: "keyb_8",
            value: "h",
            ico: "h",
            tag: "span1",
            left: "keyb_7",
            right: "keyb_9",
            top: "keyb_2",
            down: "keyb_14",
            x: 3.515625,
            y: 6.25,
            width: 3.515625
          },
          {
            id: "keyb_9",
            value: "i",
            ico: "i",
            tag: "span1",
            left: "keyb_8",
            right: "keyb_10",
            top: "keyb_3",
            down: "keyb_15",
            x: 7.03125,
            y: 6.25,
            width: 3.515625
          },
          {
            id: "keyb_10",
            value: "j",
            ico: "j",
            tag: "span1",
            left: "keyb_9",
            right: "keyb_11",
            top: "keyb_4",
            down: "keyb_16",
            x: 10.546875,
            y: 6.25,
            width: 3.515625
          },
          {
            id: "keyb_11",
            value: "k",
            ico: "k",
            tag: "span1",
            left: "keyb_10",
            right: "keyb_12",
            top: "keyb_5",
            down: "keyb_17",
            x: 14.0625,
            y: 6.25,
            width: 3.515625
          },
          {
            id: "keyb_12",
            value: "l",
            ico: "l",
            tag: "span1",
            left: "keyb_11",
            right: "call_slid",
            top: "keyb_6",
            down: "keyb_18",
            x: 17.578125,
            y: 6.25,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_13",
            value: "m",
            ico: "m",
            tag: "span1",
            left: "",
            right: "keyb_14",
            top: "keyb_7",
            down: "keyb_19",
            x: 0,
            y: 11.805555555555555,
            width: 3.515625
          },
          {
            id: "keyb_14",
            value: "n",
            ico: "n",
            tag: "span1",
            left: "keyb_13",
            right: "keyb_15",
            top: "keyb_8",
            down: "keyb_20",
            x: 3.515625,
            y: 11.805555555555555,
            width: 3.515625
          },
          {
            id: "keyb_15",
            value: "o",
            ico: "o",
            tag: "span1",
            left: "keyb_14",
            right: "keyb_16",
            top: "keyb_9",
            down: "keyb_21",
            x: 7.03125,
            y: 11.805555555555555,
            width: 3.515625
          },
          {
            id: "keyb_16",
            value: "p",
            ico: "p",
            tag: "span1",
            left: "keyb_15",
            right: "keyb_17",
            top: "keyb_10",
            down: "keyb_22",
            x: 10.546875,
            y: 11.805555555555555,
            width: 3.515625
          },
          {
            id: "keyb_17",
            value: "q",
            ico: "q",
            tag: "span1",
            left: "keyb_16",
            right: "keyb_18",
            top: "keyb_11",
            down: "keyb_23",
            x: 14.0625,
            y: 11.805555555555555,
            width: 3.515625
          },
          {
            id: "keyb_18",
            value: "r",
            ico: "r",
            tag: "span1",
            left: "keyb_17",
            right: "call_slid",
            top: "keyb_12",
            down: "keyb_24",
            x: 17.578125,
            y: 11.805555555555555,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_19",
            value: "s",
            ico: "s",
            tag: "span1",
            left: "",
            right: "keyb_20",
            top: "keyb_13",
            down: "keyb_25",
            x: 0,
            y: 17.36111111111111,
            width: 3.515625
          },
          {
            id: "keyb_20",
            value: "t",
            ico: "t",
            tag: "span1",
            left: "keyb_19",
            right: "keyb_21",
            top: "keyb_14",
            down: "keyb_26",
            x: 3.515625,
            y: 17.36111111111111,
            width: 3.515625
          },
          {
            id: "keyb_21",
            value: "u",
            ico: "u",
            tag: "span1",
            left: "keyb_20",
            right: "keyb_22",
            top: "keyb_15",
            down: "keyb_27",
            x: 7.03125,
            y: 17.36111111111111,
            width: 3.515625
          },
          {
            id: "keyb_22",
            value: "v",
            ico: "v",
            tag: "span1",
            left: "keyb_21",
            right: "keyb_23",
            top: "keyb_16",
            down: "keyb_28",
            x: 10.546875,
            y: 17.36111111111111,
            width: 3.515625
          },
          {
            id: "keyb_23",
            value: "w",
            ico: "w",
            tag: "span1",
            left: "keyb_22",
            right: "keyb_24",
            top: "keyb_17",
            down: "keyb_29",
            x: 14.0625,
            y: 17.36111111111111,
            width: 3.515625
          },
          {
            id: "keyb_24",
            value: "x",
            ico: "x",
            tag: "span1",
            left: "keyb_23",
            right: "call_slid",
            top: "keyb_18",
            down: "keyb_30",
            x: 17.578125,
            y: 17.36111111111111,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_25",
            value: "y",
            ico: "y",
            tag: "span1",
            left: "",
            right: "keyb_26",
            top: "keyb_19",
            down: "keyb_31",
            x: 0,
            y: 23.61111111111111,
            width: 3.515625
          },
          {
            id: "keyb_26",
            value: "z",
            ico: "z",
            tag: "span1",
            left: "keyb_25",
            right: "keyb_27",
            top: "keyb_20",
            down: "keyb_32",
            x: 3.515625,
            y: 23.61111111111111,
            width: 3.515625
          },
          {
            id: "keyb_27",
            value: "š",
            ico: "š",
            tag: "span1",
            left: "keyb_26",
            right: "keyb_28",
            top: "keyb_21",
            down: "keyb_33",
            x: 7.03125,
            y: 23.61111111111111,
            width: 3.515625
          },
          {
            id: "keyb_28",
            value: "đ",
            ico: "đ",
            tag: "span1",
            left: "keyb_27",
            right: "keyb_29",
            top: "keyb_22",
            down: "keyb_34",
            x: 10.546875,
            y: 23.61111111111111,
            width: 3.515625
          },
          {
            id: "keyb_29",
            value: "ć",
            ico: "ć",
            tag: "span1",
            left: "keyb_28",
            right: "keyb_30",
            top: "keyb_23",
            down: "keyb_35",
            x: 14.0625,
            y: 23.61111111111111,
            width: 3.515625
          },
          {
            id: "keyb_30",
            value: "č",
            ico: "č",
            tag: "span1",
            left: "keyb_29",
            right: "call_slid",
            top: "keyb_24",
            down: "keyb_36",
            x: 17.578125,
            y: 23.61111111111111,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_31",
            value: "ž",
            ico: "ž",
            tag: "span1",
            left: "",
            right: "keyb_32",
            top: "keyb_25",
            down: "keyb_37",
            x: 0,
            y: 29.444444444444446,
            width: 3.515625
          },
          {
            id: "keyb_32",
            value: "dž",
            ico: "dž",
            tag: "span1",
            left: "keyb_31",
            right: "keyb_33",
            top: "keyb_26",
            down: "keyb_38",
            x: 3.515625,
            y: 29.444444444444446,
            width: 3.515625
          },
          {
            id: "keyb_33",
            value: "0",
            ico: "0",
            tag: "span1",
            left: "keyb_32",
            right: "keyb_34",
            top: "keyb_27",
            down: "keyb_39",
            x: 7.03125,
            y: 29.444444444444446,
            width: 3.515625
          },
          {
            id: "keyb_34",
            value: "1",
            ico: "1",
            tag: "span1",
            left: "keyb_33",
            right: "keyb_35",
            top: "keyb_28",
            down: "keyb_40",
            x: 10.546875,
            y: 29.444444444444446,
            width: 3.515625
          },
          {
            id: "keyb_35",
            value: "2",
            ico: "2",
            tag: "span1",
            left: "keyb_34",
            right: "keyb_36",
            top: "keyb_29",
            down: "keyb_41",
            x: 14.0625,
            y: 29.444444444444446,
            width: 3.515625
          },
          {
            id: "keyb_36",
            value: "3",
            ico: "3",
            tag: "span1",
            left: "keyb_35",
            right: "call_slid",
            top: "keyb_30",
            down: "keyb_42",
            x: 17.578125,
            y: 29.444444444444446,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_37",
            value: "4",
            ico: "4",
            tag: "span1",
            left: "",
            right: "keyb_38",
            top: "keyb_31",
            down: "keyb_43",
            x: 0,
            y: 35.41666666666667,
            width: 3.515625
          },
          {
            id: "keyb_38",
            value: "5",
            ico: "5",
            tag: "span1",
            left: "keyb_37",
            right: "keyb_39",
            top: "keyb_32",
            down: "keyb_43",
            x: 3.515625,
            y: 35.41666666666667,
            width: 3.515625
          },
          {
            id: "keyb_39",
            value: "6",
            ico: "6",
            tag: "span1",
            left: "keyb_38",
            right: "keyb_40",
            top: "keyb_33",
            down: "keyb_43",
            x: 7.03125,
            y: 35.41666666666667,
            width: 3.515625
          },
          {
            id: "keyb_40",
            value: "7",
            ico: "7",
            tag: "span1",
            left: "keyb_39",
            right: "keyb_41",
            top: "keyb_34",
            down: "keyb_43",
            x: 10.546875,
            y: 35.41666666666667,
            width: 3.515625
          },
          {
            id: "keyb_41",
            value: "8",
            ico: "8",
            tag: "span1",
            left: "keyb_40",
            right: "keyb_42",
            top: "keyb_35",
            down: "keyb_back",
            x: 14.0625,
            y: 35.41666666666667,
            width: 3.515625
          },
          {
            id: "keyb_42",
            value: "9",
            ico: "9",
            tag: "span1",
            left: "keyb_41",
            right: "call_slid",
            top: "keyb_36",
            down: "keyb_search",
            x: 17.578125,
            y: 35.41666666666667,
            width: 3.515625
          }
        ],
        [
          {
            id: "keyb_43",
            value: " ",
            ico: '<svg x="0px" y="0px" viewBox="0 0 200 25"  class="icon_color" xml:space="preserve"><path d="M131.9,7.9H63.1V3.3h-2.6v7h74v-7h-2.6C131.9,3.3,131.9,7.9,131.9,7.9z"/></svg>',
            tag: "span4",
            left: "",
            right: "keyb_back",
            top: "keyb_37",
            down: "call_slid",
            x: 0,
            y: 40.97222222222222,
            width: 14.0625
          },
          {
            id: "keyb_back",
            value: "",
            ico: '<i class="bx bx-arrow-back"></i>',
            tag: "span1",
            left: "keyb_43",
            right: "keyb_search",
            top: "keyb_41",
            down: "call_slid",
            x: 14.0625,
            y: 40.97222222222222,
            width: 3.515625
          },
          {
            id: "keyb_search",
            value: "",
            ico: '<i class="bx bx-search"></i>',
            tag: "span1",
            left: "keyb_back",
            right: "call_slid",
            top: "keyb_42",
            down: "call_slid",
            x: 17.578125,
            y: 40.97222222222222,
            width: 3.515625
          }
        ]
      ];

      this.id_key = "keyb_1";
      this.value = "a";
      this.bus = options.bus;
    },

    events: {
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      //e.stopPropagation();
      default_key(e.keyCode);

      //dbgLog(e.keyCode);

      switch (e.keyCode) {
        case KEY_RIGHT:
          var res_s = _(this.keyboard).chain().flatten().findWhere({ id: this.id_key }).value();
          if (res_s["right"].match("^keyb_")) {
            var res = _(this.keyboard).chain().flatten().findWhere({ id: res_s["right"] }).value();
            this.id_key = res["id"];
            this.value = res["value"];
            $("#key_keyboard").animate(
              { left: res["x"] + "vw", top: res["y"] + "vh", width: res["width"] + "vw" },
              { duration: 25 }
            );
          } else if (res_s["right"] == "call_slid") {
            if (st_complete !== "1") console.log(this.bus.trigger("SetFocusList"));
            return false;
          }
          return false;
          break;
        case KEY_LEFT:
          var res_s = _(this.keyboard).chain().flatten().findWhere({ id: this.id_key }).value();
          if (res_s["left"].match("^keyb_")) {
            var res = _(this.keyboard).chain().flatten().findWhere({ id: res_s["left"] }).value();
            this.id_key = res["id"];
            this.value = res["value"];
            $("#key_keyboard").animate(
              { left: res["x"] + "vw", top: res["y"] + "vh", width: res["width"] + "vw" },
              { duration: 25 }
            );
          } else if (res_s["right"] == "call_slid") {
            if (st_complete !== "1") console.log(this.bus.trigger("SetFocusList"));
            return false;
          }
          return false;
          break;
        case KEY_DOWN:
          var res_s = _(this.keyboard).chain().flatten().findWhere({ id: this.id_key }).value();
          console.log(res_s);
          if (res_s["down"].match("^keyb_")) {
            var res = _(this.keyboard).chain().flatten().findWhere({ id: res_s["down"] }).value();
            this.id_key = res["id"];
            this.value = res["value"];
            $("#key_keyboard").animate(
              { left: res["x"] + "vw", top: res["y"] + "vh", width: res["width"] + "vw" },
              { duration: 25 }
            );
          } else if (res_s["down"] == "call_slid") {
            if (st_complete !== "1") this.bus.trigger("SetFocusSuggest");
            return false;
          }
          return false;
          break;
        case KEY_UP:
          var res_s = _(this.keyboard).chain().flatten().findWhere({ id: this.id_key }).value();
          if (res_s["top"].match("^keyb_")) {
            var res = _(this.keyboard).chain().flatten().findWhere({ id: res_s["top"] }).value();
            this.id_key = res["id"];
            this.value = res["value"];
            $("#key_keyboard").animate(
              { left: res["x"] + "vw", top: res["y"] + "vh", width: res["width"] + "vw" },
              { duration: 25 }
            );
          } else if (res_s["right"] == "call_slid") {
            if (st_complete !== "1") this.bus.trigger("SetFocusList");
            return false;
          }
          return false;
          break;
        case KEY_OK:
          //if (this.id_key == "keyb_back") $('#id_search').text($('#id_search').text().slice(0, -1));
          //else if (this.id_key == "keyb_search")console.log('search');
          //else $('#id_search').text($('#id_search').text() + this.value);

          if (this.id_key == "keyb_back") {
            $("#id_search").text($("#id_search").text().slice(0, -1));
          } else if (this.id_key == "keyb_search") {
            this.bus.trigger("RenderKeyboardSearch", { value: $("#id_search").text() });
          } else {
            $("#id_search").text($("#id_search").text() + this.value);
            console.log("Sadrzaj searcha: ", $("#id_search").text());
          }

          if (this.form.view == "searchView") {
            if ($("#id_search").text().length >= 3)
              this.bus.trigger("RenderSuggest", { search: $("#id_search").text() });
            else {
              console.log("brisi");
              $(".nav_suggest").empty();
            }
          }

          return false;
          break;
        case KEY_HOME:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_BACK:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        default:
      }

      console.log(this.id_key);

      //this.$el.find('#control_' + this.id_list).focus();
    },

    /* showResult: function(){
			console.log("evo me u keybard");
			return "abcd";
		}, */
    render: function () {
      var templ = _.template(template);
      var html = templ({ keyboard: this.keyboard });
      this.$el.html(html);

      var res = _(this.keyboard).chain().flatten().findWhere({ id: this.id_key }).value();

      this.$el.find("#key_keyboard").css({ left: res["x"] + "vw", top: res["y"] + "vh" });
      $("#id_search").text(this.search_val);
      //console.log(this.search_val);

      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      //this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;
    }
  });

  return KeyboardView;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "velocity",
  "collections/epgItemCollections",
  "models/channel_focus",
  "models/epg_focus",
  "views/sliderView",
  "text!page/channepg.html"
], function ($, _, Promise, tv, velocity, EpgCollections, channel_focus, epg_focus, SliderView, template) {
  var ChannEpgView = Backbone.View.extend({
    initialize: function (options) {
      this.channel = options.channel_data;

      this.epg = {};
      this.live = {};
      var epg_index;
      var id_epg_focus;

      this.bus = options.bus;
      this.bus.on("channEPGClose", this.onClose, this);
    },

    create: function () {
      var self = this;
      var data = {};
      tv.getTrenutniEpg(self.channel.idOriginal).then(function (res) {
        data = res;
        console.log(data);
        tv.getTrenutno(self.channel.idOriginal)
          .then(function (live_res) {
            console.log(live_res);
            EpgCollections.reset();
            EpgCollections.push(data);
            //console.log(EpgCollections.toJSON());
            self.epg = EpgCollections.toJSON();
            self.live = live_res;

            console.log("*** self.epg", self.epg);
            console.log("*** self.live", self.live);

            epg_index = _.findIndex(self.epg, { ID: self.live.ID });
            id_epg_focus = self.epg[epg_index].ID;

            epg_focus.set({ max_number: self.epg.length - 1 });
            epg_focus.set({ id: epg_index }, { validate: true });

            self.set_value_epg_focus();

            console.log("data", data);
            console.log("self.epg", self.epg);
            console.log("self.live", self.live);
            console.log("id_epg_focus", id_epg_focus);
            console.log(
              "count",
              epg_focus.get("max_number"),
              "id",
              epg_focus.get("id"),
              self.epg[epg_focus.get("id")].name
            );
          })
          .then(function (res) {
            console.log("RENDER");
            self.render();
          });
      });
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      e.stopPropagation();

      default_key(e.keyCode);

      //console.log(e.keyCode);

      //console.log("Napred", tv.getNapredEpg(this.channel.idOriginal, false));
      switch (e.keyCode) {
        case KEY_DOWN:
          epg_focus.set({ id: epg_focus.get("id") + 1 }, { validate: true });
          this.update_info(epg_focus.get("id"));

          if (epg_focus.get("id") > epg_focus.get("max_number") - 2) this.getNext();

          break;

        case KEY_UP:
          epg_focus.set({ id: epg_focus.get("id") - 1 }, { validate: true });
          this.update_info(epg_focus.get("id"));
          if (epg_focus.get("id") < 2) this.getPrevious();
          break;

        case KEY_HOME:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
          this.onReturnChannel();
          return false;
          break;
        case KEY_BACK:
          this.onReturnChannel();
          return false;
          break;
        case KEY_LEFT:
          this.onReturnChannel();
          return false;
          break;
        case KEY_ENTER:
        case KEY_OK:
          this.set_value_epg_focus();
          this.set_value_channel_focus();

          $("#screen_view_container").removeClass("active").addClass("inactive");
          $("#player_view_container").removeClass("inactive").addClass("active");
          $("#channel").removeClass("remove_focus");

          /************************************************************************/

          if (epg_focus.get("st_live") == true) {
            console.log("Live *************************************************");
            var mchId = epg_focus.get("idOriginal");
            var mediaId = epg_focus.get("mediaId");
            var playbillId = 0;
            var position = 0;

            player.playV3(mchId, mediaId, playbillId, position);
            dbgLog(
              "Live - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
            console.log(
              "Live - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
          } else {
            console.log("EPG ***************************************************");
            var mchId = epg_focus.get("idOriginal");
            var mediaId = epg_focus.get("mediaId");
            var playbillId = epg_focus.get("id_epg");
            var position = 0;

            player.playV3(mchId, mediaId, playbillId, position);
            dbgLog(
              "EPG - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
            console.log(
              "EPG - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
          }

          /************************************************************************/

          this.$el.empty();
          this.sliderView = new SliderView({ el: "#player_container", bus: this.bus });
          this.sliderView.create();
          this.onClose();

          return false;
          break;
      }

      console.log("count", epg_focus.get("max_number"), "id", epg_focus.get("id"), this.epg[epg_focus.get("id")].name);

      return false;
    },

    getNext: function () {
      console.log("***************** usao za promis getNext");
      self = this;
      id_epg_focus = this.epg[epg_focus.get("id")].ID;
      tv.getNapredEpg(self.channel.idOriginal, false)
        .then(function (res) {
          EpgCollections.reset();
          EpgCollections.push(res);
          self.epg = res;

          epg_focus.set({ max_number: self.epg.length - 1 });
          var epg_index = _.findIndex(self.epg, { ID: id_epg_focus });
          epg_focus.set({ id: epg_index }, { validate: true });
        })
        .then(function () {
          self.update_info(epg_focus.get("id"));
        });
    },

    getPrevious: function () {
      console.log("***************** usao za promis getPrevious");
      id_epg_focus = this.epg[epg_focus.get("id")].ID;
      self = this;

      tv.getNazadEpg(self.channel.idOriginal)
        .then(function (res) {
          EpgCollections.reset();
          EpgCollections.push(res);
          self.epg = res;

          epg_focus.set({ max_number: self.epg.length - 1 });
          var epg_index = _.findIndex(self.epg, { ID: id_epg_focus });
          epg_focus.set({ id: epg_index }, { validate: true });
        })
        .then(function () {
          self.update_info(epg_focus.get("id"));
        });
    },

    set_value_epg_focus: function () {
      var data = this.epg[epg_focus.get("id")];
      epg_focus.setData(data);
    },

    set_value_channel_focus: function () {
      var data = this.channel;
      channel_focus.setData(data);
    },

    onReturnChannel: function () {
      $("#channel").removeClass("remove_focus");
      $("#channel").focus();
      this.onClose();
      //this.bus.trigger("ReturnChannelEPG");
      return false;
    },

    render: function () {
      var templ = _.template(template);
      var html = templ();
      this.$el.html(html);
      this.update_info(epg_focus.get("id"));
      this.$el.find("#lst-epg").focus();

      var top_poz =
        $("#lst-epg-content").offset().top -
        ($("#lst-epg-content").offset().top -
          $("#channel").offset().top +
          $("body").height() / (100 / 7.222222222222221)) -
        $("#epg_arrow_up").height() -
        $("#epg_item_1").height() -
        $("#epg_item_2").height();
      console.log(
        "topPoz",
        top_poz,
        $("body").height(),
        $("#lst-epg-content").offset().top,
        $("#channel").offset().top,
        $("#epg_arrow_up").height(),
        $("#epg_item_1").height(),
        $("#epg_item_2").height()
      );
      $("#lst-epg-content").css("top", top_poz + "px");
      $("#channel").addClass("remove_focus");
      $("#lst-epg").focus();

      return this;
    },

    update_info: function (id) {
      console.log("id", id);

      for (var i = 0; i < 6; i++) {
        this.onSetRowInfo(id + (i - 2), i + 1);
      }
    },

    onSetRowInfo: function (id, poz) {
      var elem = id;
      console.log("elem", elem);

      if (typeof this.epg[elem] !== "undefined") {
        $("#epg_item_" + poz)
          .find(".name")
          .text(this.epg[elem].name);
        $("#epg_item_" + poz)
          .find(".time")
          .text(formatTime(Number(this.epg[elem].startTime)) + " - " + formatTime(Number(this.epg[elem].endTime)));
        $("#epg_item_" + poz)
          .find(".date")
          .text(formatDate(Number(this.epg[elem].startTime)));

        var icon = "";
        if (this.epg[elem].startTime < this.live.startTime) icon = "<i class='bx bx-play-circle'></i>";
        else if (this.epg[elem].startTime > this.live.startTime) icon = "<i class='bx bx-bell'></i>";
        else icon = "<i class='bx bx-broadcast'></i>";

        $("#epg_item_" + poz)
          .find(".lst-icon")
          .empty()
          .append(icon);

        $("#epg_item_" + poz)
          .find(".lst-icon")
          .show();

        $("#epg_item_" + poz)
          .find(".lst-epg-box-border")
          .removeClass("border-none");
      } else {
        $("#epg_item_" + poz)
          .find(".name")
          .text("");
        $("#epg_item_" + poz)
          .find(".time")
          .text("");
        $("#epg_item_" + poz)
          .find(".date")
          .text("");
        $("#epg_item_" + poz)
          .find(".lst-icon")
          .hide();
        $("#epg_item_" + poz)
          .find(".lst-epg-box-border")
          .addClass("border-none");
      }
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return ChannEpgView;
});

function sleep(milliseconds) {
  var start = new Date().getTime();
  for (var i = 0; i < 1e7; i++) {
    if (new Date().getTime() - start > milliseconds) {
      console.log(milliseconds);
      return milliseconds;
      break;
    }
  }
}

define([
  "jquery",
  "underscore",
  "backbone",
  "coreSearch",
  "collections/searchCollections",
  "models/screen_focus",
  "views/listRowsView",
  "views/keyboardView",
  "views/suggestView",
  "text!page/search.html"
], function (
  $,
  _,
  Backbone,
  search,
  SearchCollections,
  screen_focus,
  ListRowsView,
  KeyboardView,
  SuggestView,
  template
) {
  var st_complete = "";
  var searchColl = {};
  var searchGroups = {};
  var listRowsView = {};

  var SearchView = Backbone.View.extend({
    tagName: "span",

    /*
     action_type    
     1 - player
     2 - preview
     3 - children
    */
    initialize: function (options) {
      this.roots = [];

      this.config = {
        first: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "player",
          image: "stills"
        },
        other: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "player",
          image: "stills"
        }
      };

      this.bus = options.bus;
      this.bus.on("RenderSearchChildren", this.onRenderSearchChildren, this);
      this.bus.on("RenderSuggest", this.onRenderSuggest, this);
      this.bus.on("RenderKeyboardSearch", this.onRenderKeyboardSearch, this);
      this.bus.on("SetFocusList", this.onSetFocusList, this);

      this.bus.on("SearchClose", this.onClose, this);

      screen_focus.setData({ view: "search", focus: "control_0" });
    },

    onRenderSearchChildren: function (item) {
      console.log("Ne radi nista");
    },

    create: function () {
      loaderShow(true);
      searchColl = new SearchCollections();
      var self = this;
      searchColl.init().then(function (res) {
        console.log(res);
        self.collection = res;

        searchGroups = {
          groups: []
        };

        for (i = 0; i < self.collection.length - 1; i++) {
          searchGroups.groups.push(self.collection[i].category);
        }

        console.log(self.collection);
        self.render();
        self.afterRender();
        loaderShow(false);
      });
    },

    onRenderKeyboardSearch: function (data) {
      loaderShow(true);
      var self = this;
      searchColl = new SearchCollections();
      searchColl.keyboardSearch(data).then(function (res) {
        self.collection = res;

        searchGroups = {
          groups: []
        };

        for (i = 0; i < self.collection.length - 1; i++) {
          searchGroups.groups.push(self.collection[i].category);
        }

        console.log(self.collection);
        self.render();
        self.afterRender();
        loaderShow(false);
      });
    },

    onSetFocusList: function () {
      $("#control_" + listRowsView.getListID()).focus();
    },

    render: function () {
      console.log(this.collection);
      listRowsView = new ListRowsView({
        el: "#other_container",
        template: template,
        collObj: searchGroups,
        collect: this.collection,
        config: this.config,
        roots: this.roots,
        max_rows: this.collection.length,
        bus: this.bus
      });

      $("#other").append(listRowsView.render().el);

      return this;
    },

    afterRender: function () {
      var keyboardView = new KeyboardView({ el: ".nav_keyboard", form: { view: "searchView" }, bus: this.bus });
      keyboardView.render();
      this.$el.find("#key_keyboard").focus();

      return this;
    },

    onRenderSuggest: function (data) {
      var self = this;
      searchColl = new SearchCollections();
      searchColl.searchSuggestions(data).then(function (res) {
        //console.log(res);
        if (res.hasOwnProperty("suggests")) {
          var suggestView = new SuggestView({ el: ".nav_suggest", suggest: res.suggests.slice(0, 6), bus: self.bus });
          suggestView.render();
        } else $(".nav_suggest").empty();
      });
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return SearchView;
});

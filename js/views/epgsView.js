define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "collections/epgItemCollections",
  "models/channel_focus",
  "models/epg_focus",
  "text!page/epgs.html"
], function ($, _, Backbone, tv, EpgCollections, channel_focus, epg_focus, template) {
  var st_complete = "";
  var duration = 150;

  var EpgsView = Backbone.View.extend({
    tagName: "ul",

    initialize: function (options) {
      this.x_focus_pos = 40.15625;
      this.speed_change = 1;
      this.x_shift = 19.84375;

      this.bus = options.bus;
      this.bus.off("SliderEpgRight").on("SliderEpgRight", this.onRight, this);
      this.bus.off("SliderEpgLeft").on("SliderEpgLeft", this.onLeft, this);
      this.bus.off("ShowEpg").on("ShowEpg", this.onShowEpg, this);
    },

    create: function () {
      this.epg = EpgCollections.toJSON();
      this.live = epg_focus.toJSON();

      //console.log(this.epg);
      //console.log(epg_focus.get("id_epg"));
      //console.log(epg_focus.toJSON());

      var epg_index = _.findIndex(this.epg, { ID: epg_focus.get("id_epg") });
      var id_epg_focus = this.epg[epg_index].ID;

      this.count = this.epg.length - 1;
      epg_focus.set({ max_number: this.epg.length - 1 });
      this.render();
    },

    onShowEpg: function () {
      this.y = 0;
      this.x_focus_pos = 514;
      //epg_focus.set({ id: 3 }, { validate: true });
      this.render();
      $("#channel").addClass("remove_focus");
      $("#epg").focus();
    },

    onReturnChannel: function () {
      this.$el.empty();
      $("#channel").removeClass("remove_focus");
      $("#channel").focus();
      return this;
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      default_key(e.keyCode);

      //this.x = epg_focus.get("id");
      this.x = epg_focus.get("id");

      switch (e.keyCode) {
        case KEY_RIGHT:
          //console.log("right id", epg_focus.get("id"));
          this.onRight();
          this.$el.find("#epg").focus();
          break;
        case KEY_LEFT:
          this.onLeft();
          this.$el.find("#epg").focus();
          break;
        case KEY_HOME:
          this.onClose();
          this.bus.trigger("SliderClose");
          this.bus.trigger("SetFokusMenu");
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
        case KEY_RETURN:
          this.onClose();
          this.bus.trigger("SliderClose");
          this.bus.trigger("SetFokusMenu");
          return false;
          break;
        case KEY_BACK:
          this.onClose();
          this.bus.trigger("SliderClose");
          this.bus.trigger("SetFokusMenu");
          return false;
          break;
        case KEY_OK:
        case KEY_ENTER:
          ////console.log("id", epg_focus.get("id"));
          ////console.log("##### " + this.epg[epg_focus.get("id")].name + " #######");
          ////console.log(this.epg[epg_focus.get("id")]);
          epg_focus.setData(this.epg[epg_focus.get("id")]);

          /************************************************************************/

          if (epg_focus.get("st_live") == true) {
            //console.log("Live *************************************************");
            var mchId = epg_focus.get("idOriginal");
            var mediaId = epg_focus.get("mediaId");
            var playbillId = 0;
            var position = 0;

            player.playV3(mchId, mediaId, playbillId, position);
            console.log(
              "Live - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
            //console.log("Live - " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
          } else {
            //console.log("EPG ***************************************************");
            var mchId = epg_focus.get("idOriginal");
            var mediaId = epg_focus.get("mediaId");
            var playbillId = epg_focus.get("id_epg");
            var position = 0;
            // console.log(this.epg[epg_focus.get("id")]);
            console.log(epg_focus.get("startTime") >= Date.now() - channel_focus.get("catchupDuration") * 3600000);

            if (channel_focus.get("catchupStatus") == "Disabled") {
              console.log("IF");
              var tempImg = this.epg[epg_focus.get("id")].picture.stills;
              this.epg[epg_focus.get("id")].picture.stills = "./image/noEpg.png";

              this.update_epg_info(epg_focus.get("id"));
              this.epg[epg_focus.get("id")].picture.stills = tempImg;
            } else if (epg_focus.get("startTime") <= Date.now() - channel_focus.get("catchupDuration") * 3600000) {
              console.log("ELS IF");
              var tempImg1 = this.epg[epg_focus.get("id")].picture.stills;
              this.epg[epg_focus.get("id")].picture.stills = "./image/noEpgRew.png";

              this.update_epg_info(epg_focus.get("id"));
              this.epg[epg_focus.get("id")].picture.stills = tempImg1;
            } else {
              console.log("ELSE");
              player.playV3(mchId, mediaId, playbillId, position);
            }
            console.log(
              "EPG - " +
                "mchId:" +
                mchId +
                "  mediaId:" +
                mediaId +
                "  playbillId:" +
                playbillId +
                "  position:" +
                position
            );
            //console.log("EPG - " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
          }
          1646974800000;
          1646988721442;
          259200000;
          /************************************************************************/

          var vr_m = "";
          if (epg_focus.get("st_live") == true) vr_m = "live";
          else vr_m = "catchup";
          if (
            channel_focus.get("catchupStatus") == "Enabled" &&
            epg_focus.get("startTime") > Date.now() - channel_focus.get("catchupDuration") * 3600000
          ) {
            this.bus.trigger("HideScreen", { view: "channel", vr_media: vr_m, control_focus: "channel" });
            this.bus.trigger("SliderClose");
          }

          //this.onClose();

          return false;
          break;
        /*
                    case KEY_ENTER:
                      //console.log("Usao Enter");
                      //console.log(this.collection);
                      epg_focus.set({ id_epg: this.collection[this.x - 1].ID, id_media: this.collection[this.x - 1].mediaId, name: this.collection[this.x - 1].name, st_live: 0 });

                      this.onClose();
                      this.bus.trigger("SliderClose");
                      this.bus.trigger("GotoEpgPlay");

                      return false;
                      break;
                    case KEY_OK:
                      if ($("#main_view_container").css("display") == "none") {
                        $("#slider_page").remove();
                        $("#main_view_container").toggle();
                        this.onReturnChannel();
                        this.onClose();
                        return false;
                      } else {
                        this.onReturnChannel();
                        this.onClose();
                        $("#main_view_container").toggle();
                        this.bus.trigger("setSliderView");
                      }

                      return false;
                      break;
                      */
      }
    },

    onRight: function () {
      if (st_complete !== "1") {
        if (epg_focus.get("id") < epg_focus.get("max_number")) {
          epg_focus.set({ id: epg_focus.get("id") + 1 }, { validate: true });
          this.set_value_epg_focus();
          st_complete = 1;

          //console.log("id", epg_focus.get("id"), "count", epg_focus.get("max_number"));
          if (epg_focus.get("id") > epg_focus.get("max_number") - 3) this.getNext();
          else this.Right_EPG_Shift();
        }
      }
    },

    onLeft: function () {
      if (st_complete !== "1") {
        if (epg_focus.get("id") > 0) {
          epg_focus.set({ id: epg_focus.get("id") - 1 }, { validate: true });
          this.set_value_epg_focus();
          st_complete = 1;

          //console.log("id", epg_focus.get("id"), "count", epg_focus.get("max_number"));
          if (epg_focus.get("id") < 3) this.getPrevious();
          else this.Left_EPG_Shift();
        }
      }
    },

    Right_EPG_Shift: function () {
      st_complete = 1;
      this.onSetRowInfo(0, "");

      var dom = $("#epg-content").children(".item").eq(0);

      ////console.log(this.epg.length);
      ////console.log(epg_focus.get("id"));

      var epg_item = this.epg.length - 1 >= epg_focus.get("id") + 3 ? this.epg[epg_focus.get("id") + 3] : "";
      console.log(epg_item);

      self = this;
      dom.animate(
        { width: "0px" },
        {
          duration: duration,
          complete: function () {
            dom.remove();
            self.insertDOM(epg_item, "forward");
            $("#epg-content").find("*").removeClass("focus");
            $("#epg-content").children(".item").eq(3).find(".fill").addClass("focus");
          }
        }
      );
    },

    Left_EPG_Shift: function () {
      st_complete = 1;

      var epg_item = epg_focus.get("id") - 3 >= 0 ? this.epg[epg_focus.get("id") - 3] : "";
      console.log(epg_item);
      this.insertDOM(epg_item, "backward");
      var dom = $("#epg-content").children(".item").eq(0);
      this.onSetRowInfo(0, epg_item);

      dom.animate(
        { width: "19.84375vw" },
        {
          duration: duration,
          complete: function () {
            $("#epg-content").children(".item").last().remove();

            $("#epg-content").find("*").removeClass("focus");
            $("#epg-content").children(".item").eq(3).find(".fill").addClass("focus");

            st_complete = 0;
          }
        }
      );
    },

    getNext: function () {
      //console.log("***************** usao za promis getNext");
      self = this;
      id_epg_focus = this.epg[epg_focus.get("id")].ID;
      tv.getNapredEpg(channel_focus.get("idOriginal"))
        .then(function (res) {
          EpgCollections.reset();
          EpgCollections.push(res);
          self.epg = res;

          epg_focus.set({ max_number: self.epg.length - 1 });
          var epg_index = _.findIndex(self.epg, { ID: id_epg_focus });

          //console.log("*** id ***", epg_focus.get("id"));
          //console.log("*** epg_index ***", epg_index);
          //console.log("*** epg ***", self.epg);

          epg_focus.set({ id: epg_index }, { validate: true });
        })
        .then(function () {
          self.Right_EPG_Shift();
        });
    },

    getPrevious: function () {
      //console.log("***************** usao za promis getPrevious");
      id_epg_focus = this.epg[epg_focus.get("id")].ID;
      self = this;

      tv.getNazadEpg(channel_focus.get("idOriginal"))
        .then(function (res) {
          EpgCollections.reset();
          EpgCollections.push(res);
          self.epg = res;

          epg_focus.set({ max_number: self.epg.length - 1 });
          var epg_index = _.findIndex(self.epg, { ID: id_epg_focus });
          epg_focus.set({ id: epg_index }, { validate: true });
        })
        .then(function () {
          self.Left_EPG_Shift();
        });
    },

    insertDOM: function (data, status) {
      var doc = document.getElementById("epg-content");

      var div_item = document.createElement("div");
      div_item.className = "item epg-box";
      div_item.setAttribute("id_epg", data.id_epg);

      var div_box = document.createElement("div");
      div_box.className = "epg-box-border";

      var div_fill = document.createElement("div");
      div_fill.className = "fill";

      var span_name = document.createElement("span");
      span_name.className = "name";
      span_name.textContent = data.name;

      var span_time = document.createElement("span");
      span_time.className = "time";
      span_time.textContent = formatTime(Number(data.startTime)) + " - " + formatTime(Number(data.endTime));

      var span_data = document.createElement("span");
      span_data.className = "date";
      span_data.textContent = formatDate(Number(data.startTime));

      var img = document.createElement("img");
      console.log(img);
      img.src = data.picture.stills;

      div_fill.appendChild(span_name);
      div_fill.appendChild(span_time);
      div_fill.appendChild(span_data);

      div_box.appendChild(div_fill);
      div_box.appendChild(img);

      div_item.appendChild(div_box);
      //doc.appendChild(div_item);

      if (status == "forward") {
        doc.appendChild(div_item);
        st_complete = 0;
      } else {
        div_item.style.width = "0px";
        doc.insertBefore(div_item, doc.firstChild);
      }
    },

    set_value_epg_focus: function () {
      var data = this.epg[epg_focus.get("id")];
      epg_focus.setData(data);
    },

    render: function () {
      var templ = _.template(template);
      var html = templ();
      this.$el.html(html);

      //console.log(epg_focus.get("id"));
      this.update_epg_info(epg_focus.get("id"));
      return this;
    },

    update_epg_info: function (index_e) {
      var i;
      var index = 0;

      for (var i = index_e - 3; i <= index_e + 3; i++) {
        if (typeof this.epg[i] !== "undefined" && i <= this.count) {
          this.onSetRowInfo(index, this.epg[i]);
        } else {
          this.onSetRowInfo(index, "");
        }
        index = index + 1;
      }
    },

    onSetRowInfo: function (id, data) {
      if (data !== "") {
        $("#epg-content").children(".item").eq(id).find(".name").text(data.name);
        $("#epg-content")
          .children(".item")
          .eq(id)
          .find(".time")
          .text(formatTime(Number(data.startTime)) + " - " + formatTime(Number(data.endTime)));
        $("#epg-content")
          .children(".item")
          .eq(id)
          .find(".date")
          .text(formatDate(Number(data.startTime)));
        $("#epg-content").children(".item").eq(id).find("img").attr("src", data.picture.stills);

        if (id == 3) $("#epg-content").children(".item").eq(id).find(".fill").addClass("focus");
        else $("#epg-content").children(".item").eq(id).find(".fill").removeClass("focus");
      } else {
        $("#epg-content").children(".item").eq(id).find(".name").text("");
        $("#epg-content").children(".item").eq(id).find(".name").text("");
        $("#epg-content").children(".item").eq(id).find(".time").text("");
        $("#epg-content").children(".item").eq(id).find(".date").text("");
        $("#epg-content").children(".item").eq(id).find("img").attr("src", "");
      }
    },

    onClose: function () {
      //this.$el.empty();

      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;
    }
  });

  return EpgsView;
});

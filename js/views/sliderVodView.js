define(["jquery", "underscore", "backbone", "coreChanEpg", "models/vod_focus", "models/epg_focus", "models/screen_focus", "views/searchView", "views/startView", "views/vodContentView", "text!page/slidervod.html", "jquery-ui"], function (
  $,
  _,
  Backbone,
  tv,
  vod_focus,
  epg_focus,
  screen_focus,
  SearchView,
  StartView,
  VodContentView,
  template,
  ui
) {
  var st_complete = "";

  var prevKey = 0;
  var longPress = 0;
  var longPressCnt = 0;
  var status_change_step = false;

  var status_play = false;

  var hide_screen;

  var control_focus = {
    "custom-handle": {
      40: { name: "con_play" }, //KEY_DOWN
      38: { name: "" }, //KEY_UP
      37: { name: "" }, //KEY_LEFT
      39: { name: "" }, //KEY_RIGHT
    },
    con_play: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_rewind" }, //KEY_LEFT
      39: { name: "con_forward" }, //KEY_RIGHT
    },
    con_forward: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_play" }, //KEY_LEFT
      39: { name: "con_reply" }, //KEY_RIGHT
    },
    con_reply: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_forward" }, //KEY_LEFT
      39: { name: "con_info" }, //KEY_RIGHT
    },
    con_info: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_reply" }, //KEY_LEFT
      39: { name: "" }, //KEY_RIGHT
    },
    con_rewind: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "" }, //KEY_LEFT
      39: { name: "con_play" }, //KEY_RIGHT
    },
  };

  var SliderView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.y = 0;
      this.pos = 31;
      this.bus = options.bus;

      this.roots = options.roots;

      this.bus.on("SliderVodClose", this.onClose, this);

      console.log(this.roots);

      //console.log(aa["browsers"]);
      //this.state_live=player.getState();
    },

    create: function () {
      this.render();
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown",
      "slide #slider": "onSlide",
      "keyup #slider": "onSlideKeyUp",
      "keydown #custom-handle": "onKeyDown_customhandle",
      "keydown #con_play": "onKeyDown_con_play",
      "keydown #con_reply": "onKeyDown_con_reply",
      "keydown #con_forward": "onKeyDown_con_forward",
      "keydown #con_info": "onKeyDown_con_info",
      "keydown #con_rewind": "onKeyDown_con_rewind",

      "focus #custom-handle": "TimeOutHideScreen",
      "focus #con_play": "TimeOutHideScreen",
      "focus #con_reply": "TimeOutHideScreen",
      "focus #con_forward": "TimeOutHideScreen",
      "focus #con_info": "TimeOutHideScreen",
      "focus #con_rewind": "TimeOutHideScreen",
    },

    sget: function (key) {
      return $("#slider").slider("option", key);
    },

    ClearTimeOutHideScreen: function () {
      //console.log("hide_screen");
      clearTimeout(hide_screen);
    },

    TimeOutHideScreen: function () {
      //console.log("TimeOutHideScreen");
      var self = this;
      clearTimeout(hide_screen);

      hide_screen = setTimeout(function () {
        console.log(screen_focus);
        console.log("SAKRIJ");
        console.log(self.roots);
        self.bus.trigger("HideScreen", { view: screen_focus.get("view"), vr_media: vod_focus.get("vr_media"), roots: self.roots, control_focus: screen_focus.get("channel") });
      }, 3000);
    },

    onKeyDown_customhandle: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        console.log("on_custom-handle");
      }
    },
    onKeyDown_con_play: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        if (status_play == true) {
          this.onStop();
        } else {
          this.onPlay();
        }
      }
    },
    onKeyDown_con_reply: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        console.log("on_con_reply");
      }
    },
    onKeyDown_con_forward: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        this.onStop();
        var speed = player.fastForward();
        $("#fast_control_name").text(speed);
      }
    },

    onKeyDown_con_rewind: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        this.onStop();
        var speed = player.fastRewind();
        $("#fast_control_name").text(speed);
      }
    },

    onKeyDown_con_info: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        console.log("on_con_info");
      }
    },

    onKeyDown_epg: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        console.log("on_epg");
      }
    },

    onSlideKeyUp: function (event) {
      switch (event.keyCode) {
        case KEY_LEFT:
          if (status_play == false) this.onPlay();
          break;
        case KEY_RIGHT:
          if (status_play == false) this.onPlay();
          break;
      }
      this.TimeOutHideScreen();
      return;
    },

    onSlide: function (event, ui) {
      event.stopPropagation();

      var max = this.sget("max");

      this.ClearTimeOutHideScreen();

      switch (event.keyCode) {
        case KEY_LEFT:
          if (status_play == true) this.onStop();
          //console.log(ui.value / this.sget( "step"), ui.value, this.sget( "step"));

          if (ui.value / this.sget("step") <= 1 && ui.value / this.sget("step") > 0) {
            this.onChangeStep(20, "2x");
            //longPressCnt = 5;
          }

          this.update(ui.value);
          this.handleKey(event.keyCode, 0);
          break;
        case KEY_RIGHT:
          if (status_play == true) this.onStop();
          if ((this.sget("max") - ui.value) / this.sget("step") < 1 && (this.sget("max") - ui.value) / this.sget("step") > 0) {
            this.onChangeStep(20, "2x");
            status_change_step = true;
          }

          this.update(ui.value);
          this.handleKey(event.keyCode, 0);
          break;
        case KEY_DOWN:
          this.onControlPosition("custom-handle", event.keyCode);
          return false;
          break;
      }

      return;
    },

    keyActionUp: function (e) {
      this.handleKey(e.keyCode, 1);
      this.TimeOutHideScreen();
    },

    keyActionDown: function (e) {
      default_key(e.keyCode);
      var focused = document.activeElement;
      //log(focused);

      //console.log(e.keyCode);

      this.ClearTimeOutHideScreen();

      switch (e.keyCode) {
        case KEY_DOWN:
          this.onControlPosition(focused.id, e.keyCode);
          return false;
          break;
        case KEY_UP:
          this.onControlPosition(focused.id, e.keyCode);
          return false;
          break;
        case KEY_LEFT:
          if (focused.id == "custom-handle") {
            var slider_min = this.sget("min");
            var slider_val = this.sget("value");
            if (slider_val == slider_min) {
              console.log("Pocetak min");
              this.handleKey(e.keyCode, 1);
            }
          } else {
            this.onControlPosition(focused.id, e.keyCode);
          }

          return false;
          break;
        case KEY_RIGHT:
          if (focused.id == "custom-handle") {
            var slider_max = this.sget("max");
            var slider_val = this.sget("value");
            if (slider_val == slider_max) {
              console.log("Pocetak max");
              this.handleKey(e.keyCode, 1);
            }
          } else {
            this.onControlPosition(focused.id, e.keyCode);
          }
          return false;
          break;
        case KEY_BACK:
        case KEY_RETURN:
          console.log("KEY_BACK");
          this.onClose();
          this.show_return();
          return false;
          break;
        case KEY_ENTER:
          return false;
          break;
        case KEY_OK:
          return false;
          break;
      }

      return false;
    },

    show_return: function () {
      console.log(screen_focus.get("view"));
      switch (screen_focus.get("view")) {
        case "start":
          player.stop();
          $("#player_view_container").removeClass("active").addClass("inactive");
          $("#screen_view_container").removeClass("inactive").addClass("active");

          var startView = new StartView({ el: "#other_container", bus: this.bus });
          startView.create();

          return false;
          break;
        case "search":
          player.stop();
          $("#player_view_container").removeClass("active").addClass("inactive");
          $("#screen_view_container").removeClass("inactive").addClass("active");

          var searchView = new SearchView({ el: "#other_container", bus: this.bus });
          searchView.create();

          return false;
          break;
        case "vodcontent":
          player.stop();
          $("#player_view_container").removeClass("active").addClass("inactive");
          $("#screen_view_container").removeClass("inactive").addClass("active");

          clearTimeout(hide_screen);

          var vodContent = new VodContentView({ el: "#other_container", bus: this.bus });
          vodContent.create({ item: vod_focus.toJSON(), roots: this.roots });

          return false;
          break;
      }
    },

    onControlPosition: function (control, key) {
      var tmp_control = control_focus[control];
      //console.log("***********************************");
      //console.log(control);
      //console.log(tmp_control);
      //console.log(tmp_control[key]);
      //console.log("***********************************");
      if (tmp_control[key].name !== "") this.onControlSetFocus(tmp_control[key].name);
      else return false;
    },

    onControlSetFocus: function (control) {
      this.$el.find("#slider_page").parent().find("div[tabindex]").removeClass("focus");
      $("#" + control).addClass("focus");
      $("#" + control).focus();
    },

    duration: function (end, start) {
      var diff = (parseInt(end) - parseInt(start)) / 1000;
      dbgLog((parseInt(end) - parseInt(start)) / 1000);
      return parseInt(diff);
    },

    onFillEPGContent: function (status) {
      $("#trc_name_content").text(vod_focus.get("name"));
      $("#trc_date_content").text(formatDate(Number(vod_focus.get("startTime"))));
      $("#trc_time_content").text(formatTime(Number(vod_focus.get("startTime"))) + "-" + formatTime(Number(vod_focus.get("endTime"))));
      //$("#trc_date_content").text("bbbbbbb<br>cccccc");

      var max_val = this.duration(vod_focus.get("endTime"), vod_focus.get("startTime"));

      if (status.start == "start") {
        $("#slider").slider({ value: 0, max: max_val });
      } else if (status.start == "end") {
        $("#slider").slider({ value: max_val, max: max_val });
      }
    },

    render: function () {
      var templ = _.template(template);
      var html = templ({
        //channel: channel_focus.toJSON(),
        channel: {},
      });
      this.$el.html(html);

      this.onFillEPGContent({ start: "start" });

      if (typeof progress !== "undefined") clearInterval(progress);
      status_play = true;
      progress = setInterval(this.tick.bind(this), 1000);

      $("#slider").slider({ value: Math.ceil(player.getCurPos() + 0.1) });
      this.update(this.sget("value"));
      this.TimeOutHideScreen();
      //this.onPlay();

      console.log(Math.ceil(player.getCurDur()));
      $("#slider").slider({ max: Math.ceil(player.getCurDur()) });
      //$("#slider").slider({ max: 5000 });

      $("#custom-handle").focus();
      $("#custom-handle").addClass("focus");
      $("#epg").css("opacity", "0");

      return this;
    },

    update: function (seconds) {
      //var seconds = this.sget( "value");

      hours = Math.floor(seconds / 3600);
      hours = ("0" + hours).slice(-2);

      seconds %= 3600;

      minutes = Math.floor(seconds / 60);
      minutes = ("0" + minutes).slice(-2);

      seconds = seconds % 60;
      seconds = ("0" + seconds).slice(-2);

      $("#slider_time").text(hours + ":" + minutes + ":" + seconds);

      //$("#slider_time").text(seconds);
    },

    handleKey: function (code, isKeyUp, key) {
      //console.log('handleKey');
      //console.log('isKeyUp',isKeyUp);

      if (isKeyUp) {
        if (prevKey == code) {
          prevKey = 0;
          longPress = 0;
          longPressCnt = 0;
          status_change_step = false;

          $("#slider").slider({ step: 5 });
        }
        return;
      }

      if (prevKey == code) {
        longPress = 1;
        longPressCnt += 1;
      }
      prevKey = code;

      //Korekcija da se ne ubrzava za *2 ako je kraj videa

      if (code == KEY_RIGHT) {
        //console.log("*****", (this.sget( "max") - this.sget( "value")) / this.sget( "step"));
        if ((this.sget("max") - this.sget("value")) / this.sget("step") < 4 && (this.sget("max") - this.sget("value")) / this.sget("step") > 0) {
          status_change_step = true;
          //console.log("status_change_step = true;");
        }
      }

      // RAZRADA
      if (longPress == 1) {
        if (longPressCnt == 5 && status_change_step == false) {
          this.onChangeStep(15, "2x");
          //console.log("2x");
        } else if (longPressCnt == 10 && status_change_step == false) {
          this.onChangeStep(30, "4x");
          //console.log("4x");
        } else if (longPressCnt == 15 && status_change_step == false) {
          this.onChangeStep(60, "8x");
          //console.log("8x");
        }
      }

      //console.log("LONG PRESS FOR KEY: " + code + " LONG PRESS COUNT: " + longPressCnt );
    },

    onChangeStep: function (v_step, caption) {
      $("#fast_control_name").text(caption);
      $("#slider").slider({ step: v_step });
    },

    onPlay: function () {
      //console.log("PLAY PLAY PLAY");
      $("#slider").slider({ step: 5 });
      if (this.sget("value") == 0) $("#slider").slider("value", 1);
      var v_seconds = this.sget("value");
      //player.seekToPos(v_seconds);

      /************************************************************************/

      console.log("ZZZZZZZZZZZZZZZZZZZZZZZ");
      console.log(v_seconds);

      if (vod_focus.get("vr_media") == "catchup_show") {
        var mchId = vod_focus.get("id_channel");
        var mediaId = vod_focus.get("id_media");
        var playbillId = vod_focus.get("id_item");
        var position = v_seconds;

        player.playV3(mchId, mediaId, playbillId, position);
        console.log("EPG - " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
        dbgLog("EPG - " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
      } else if (vod_focus.get("vr_media") == "vod") {
        console.log("FILM");
        var position = v_seconds;
        player.playVodV3(vod_focus.get("id_item"), position);
        console.log("VOD - " + "id_item:" + vod_focus.get("id_item") + "  position:" + position);
        dbgLog("VOD - " + "id_item:" + vod_focus.get("id_item") + "  position:" + position);
      } else {
        console.log("SERIJA");
        var position = v_seconds;
        player.playShowV3(vod_focus.get("id_item"), position);
        console.log("VOD - " + "id_item:" + vod_focus.get("id_item") + "  position:" + position);
        dbgLog("VOD - " + "id_item:" + vod_focus.get("id_item") + "  position:" + position);
      }

      /************************************************************************/

      if (typeof progress !== "undefined") clearInterval(progress);

      $("#fast_control_name").text("");
      $("#play_icon").removeClass("bx-play").addClass("bx-pause");
      status_play = true;
      progress = setInterval(this.tick.bind(this), 1000);
    },

    onStop: function () {
      //console.log("STOP STOP STOP");
      player.pause();
      $("#play_icon").removeClass("bx-pause").addClass("bx-play");
      status_play = false;
      if (typeof progress !== "undefined") clearInterval(progress);
    },

    tick: function () {
      if (this.sget("min") < this.sget("value") && this.sget("max") > this.sget("value")) {
        var tmp_slider_val = this.sget("value");
        //console.log("tmp_slider_val", tmp_slider_val);
        $("#slider").slider("value", Number(tmp_slider_val) + 1);
        //console.log(this.sget( "value"));
        //console.log(this.sget( "steap"));

        this.update(this.sget("value"));
      }
    },

    onClose: function () {
      console.log("brisi");

      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    },
  });

  return SliderView;
});

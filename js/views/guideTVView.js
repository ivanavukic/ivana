define([
    'jquery',
    'underscore',
    'backbone',
    'collections/channelCollections',
    'collections/epgCollections',
    'views/guideRowsView',
    'views/epgcategoryView',
    'text!page/guidetv.html'
], function($, _, Backbone, StartListCollections, StartCollections, ListRowsView, EpgcategoryView, template) {


    var st_complete = "";

    var GuideTV = Backbone.View.extend({

        tagName: "span",

        initialize: function(options) {
            this.y = 0;
            this.bus = options.bus;
            this.bus.on("GuideTVClose", this.onClose, this);

        },

        render: function() {


            var templ = _.template(template);
            var html = templ();
            this.$el.html(html);


            //StartListCollections.reset();


            var listRowsView = new ListRowsView({
                el: "#other_container",
                LstCollect: StartListCollections,
                Collect: StartCollections,
                template: template,
                set_focus_name: 1,
                bus: this.bus
            });

            $('#other').append(listRowsView.render().el);


            var epgcategoryView = new EpgcategoryView({
                el: "#id_epg_category",
                bus: this.bus
            });


            epgcategoryView.render();



            return this;
        },

        onClose: function() {

            this.undelegateEvents();
            this.$el.removeData().unbind();

            this.$el.empty();
            this.unbind();
            this.stopListening();

            this.remove();


            return this;


        }

    });

    return GuideTV;
});
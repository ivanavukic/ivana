define([
  "jquery",
  "underscore",
  "backbone",
  "velocity",
  "models/category_focus",
  "collections/categoryCollections",
  "text!page/epgcategory.html"
], function ($, _, Backbone, Velocity, category_focus, category, template) {
  /*
    var x = 0;
    var y = 0;

    var x_focus_pos = 0;
    var x_shift = 230;
    var count = 0;

   
    var st_open_menu = false
    */

  var st_complete = "";

  var EpgCategoryView = Backbone.View.extend({
    initialize: function (options) {
      this.x = 1;
      this.y = 0;

      this.x_focus_pos = 0;

      this.count = 0;

      if ($(window).height() <= 720) {
        this.x_shift = 190;
      } else {
        //this.x_shift = 230;
        this.x_shift = 190;
      }

      //this.st_complete = "";
      this.st_open_menu = false;

      this.bus = options.bus;
      this.bus.on("EpgSelectCategory", this.onSelectCategory, this);
    },

    onSelectCategory: function () {
      console.log("onSelectCategory");

      $("#epg_ch_focus_1").removeClass("epg_ch_focus");
      $("#epg-channel").addClass("remove_focus");
      this.$el.find("#epg-category").addClass("focus");
      this.$el.find("#epg-category").focus();
    },

    events: {
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i

      //dbgLog("ChannelView - key: " + e.keyCode);

      if (e.keyCode == KEY_RED) {
        clearEpgCache();
        return false;
      } else if (e.keyCode == KEY_BLUE) {
        location.reload();
        return false;
      } else if (e.keyCode == KEY_SETTINGS) {
        openSettings();
        return false;
      }

      this.x = category_focus.get("id");
      this.count = category.size();

      if (e.keyCode === KEY_RIGHT) {
        if (this.st_open_menu !== false) {
          if (st_complete !== "1") {
            if (this.x < this.count) {
              this.x = this.x + 1;
              console.log("count--", this.count, "x", this.x);
              this.onCategoryShift(e.keyCode);
            }
          }
        }
      } else if (e.keyCode === KEY_LEFT) {
        if (this.st_open_menu !== false) {
          if (st_complete !== "1") {
            if (this.x > 1) {
              this.x = this.x - 1;
              this.onCategoryShift(e.keyCode);
            }
          }
        }
      } else if (e.keyCode === KEY_HOME) {
        //this.bus.trigger("ExpandMenu");
      } else if (e.keyCode === KEY_ENTER) {
        if (this.st_open_menu === false) {
          this.onShowMenuCategory();
        } else {
          this.onReturnEpgChannel();
          console.log("this.x-1", this.x - 1);
          console.log(category.at(this.x - 1));
          return;
        }
      } else if (e.keyCode === KEY_DOWN) {
        this.onReturnEpgChannel();
        return;
      }

      category_focus.set({ id: this.x, name: category.at(this.x - 1).get("name") }, { validate: true });
      this.$el.find("#epg-category").focus();
    },

    onCategoryShift: function (key) {
      console.log("onCategoryShift");

      if (key === KEY_RIGHT) {
        this.x_focus_pos = this.x_focus_pos - this.x_shift;
        st_complete = "1";
        console.log("ASDASDASDASDASDASDASD");
        $("#epg-category-content").animate(
          { left: this.x_focus_pos + "px" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
            }
          }
        );
      } else if (key === KEY_LEFT) {
        this.x_focus_pos = this.x_focus_pos + this.x_shift;
        st_complete = "1";
        $("#epg-category-content").animate(
          { left: this.x_focus_pos + "px" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
            }
          }
        );
      }

      this.$el.find("#category").focus();
    },

    onShowMenuCategory: function () {
      this.st_open_menu = true;
      this.x = 1;
      this.x_focus_pos = 0;
      this.render();
    },

    onReturnEpgChannel: function () {
      this.st_open_menu = false;
      this.x_focus_pos = 0;
      this.x = 1;
      this.render();
      $("#ch_epg_1").removeClass("remove_focus");
      $("#epg_ch_focus_1").addClass("epg_ch_focus");
      $("#ch_epg_1").focus();
    },

    render: function () {
      console.log("render epg");

      var templ = _.template(template);

      var html = templ({
        result: category.toJSON(),
        st_open_menu: this.st_open_menu,
        name: category_focus.get("name")
      });
      this.$el.html(html);
      if (this.st_open_menu === true) {
        this.$el.find("#epg-category").addClass("focus");
        this.$el.find("#epg-category").focus();
      }
      return this;
    }
  });

  return EpgCategoryView;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "models/channel_focus",
  "models/epg_focus",
  "models/vod_focus",
  "views/searchView",
  "views/startView",
  "views/sliderView",
  "views/sliderVodView",
  "views/vodContentView",
  "text!page/router.html",
], function (
  $,
  _,
  Backbone,
  channel_focus,
  epg_focus,
  vod_focus,
  SearchView,
  StartView,
  SliderView,
  SliderVodView,
  VodContentView,
  template
) {
  var st_complete = "";

  var CategoryView = Backbone.View.extend({
    initialize: function (options) {
      this.bus = options.bus;
      this.bus.on("HideScreen", this.onHideScreen, this);
      this.bus.on("ShowChannels", this.show_up_down, this);
      this.bus.on("ShowPlayer", this.show_left_right, this);
      var self = this;
      document.body.addEventListener("keydown", function (e) {
        console.log("ROUTER VIEW KEY: " + e.keyCode);
        e.preventDefault();
        self.keyActionDown(e);
      });
      document.body.addEventListener("keyup", function (e) {
        console.log("ROUTER VIEW KEY: " + e.keyCode);
        e.preventDefault();
        console.log(self); //IVANA
        self.keyActionUp(e);
      });
    },

    onHideScreen: function (data) {
      if ($("#player_view_container").hasClass("active")) {
        this.bus.trigger("SliderClose");
      }
      $("#screen_view_container").removeClass("active").addClass("inactive");
      $("#player_view_container").removeClass("active").addClass("inactive");

      $("body").focus();
      console.log("SAKRIVAM SVE");
      console.log(data);

      this.view = data.view;
      this.vr_media = data.vr_media;
      this.control_focus = data.control_focus;
      this.roots = data.roots;
    },
    keyActionUp: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation();

      console.log(this.view);
      console.log(e.keyCode);
      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i
      switch (e.keyCode) {
        case KEY_CHDOWN:
          // case KEY_DOWN:
          console.log("dole");
          this.bus.trigger("QuickChanUp", e);
          return false;
          break;
        case KEY_CHUP:
          // case KEY_UP:
          console.log("gore");
          this.bus.trigger("QuickChanDown", e);
          return false;
          break;
      }
    },
    keyActionDown: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation();

      console.log(this.view);
      console.log(e.keyCode);

      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i

      switch (e.keyCode) {
        case KEY_ENTER:
        case KEY_OK:
          console.log("enter/ok");
          // this.show_ok();
          this.show_left_right();
          return false;
          break;
        case KEY_RETURN:
        case KEY_BACK:
          // this.show_return();
          console.log("return");
          this.showChannelsContainer("hide");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          this.bus.trigger("ExpandMenu");

          return false;
          break;
        case KEY_CHDOWN:
        case KEY_DOWN:
          console.log("dole");
          this.bus.trigger("QuickChanUp", e);
          this.show_left_right();
          return false;
          break;
        case KEY_CHUP:
        case KEY_UP:
          console.log("gore");
          this.bus.trigger("QuickChanDown", e);
          this.show_left_right();
          return false;
          break;
        case KEY_LEFT:
        case KEY_RIGHT:
          console.log("levo/desno");
          this.show_up_down();
          return false;
          break;
        case KEY_MENU:
          console.log("menu");
          this.showChannelsContainer("hide");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_HOME:
          console.log("home");
          this.showChannelsContainer("hide");
          $("#menu").removeClass("inactive").addClass("active");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          this.bus.trigger("GoHome");
          return false;
          break;
      }

      console.log(e);

      $("#id_router").focus();
      return false;
    },

    show_up_down: function () {
      switch (this.view) {
        case "channel":
          this.showChannelsContainer("show");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");
          this.bus.trigger("SetPositionchannle", {
            id: channel_focus.get("id_select"),
          });
          $("#channel").focus();
          return false;
          break;
        case "start":
          console.log(this.vr_media);
          if (this.vr_media == "live") {
            console.log(channel_focus.get("id_select"));

            this.showChannelsContainer("show");
            $("#player_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#screen_view_container")
              .removeClass("inactive")
              .addClass("active");
            this.bus.trigger("SetPositionchannle", {
              id: channel_focus.get("id_select"),
            });
            $("#channel").focus();
            return false;
          } else if (this.vr_media == "catchup") {
            console.log("catchup - neznam sta da radi na gore dolje");
          } else if (this.vr_media == "vod" || this.vr_media == "vod_tvshow") {
            console.log("vod -neznam sta da radi na gore dolje");
          }

          return false;
          break;
      }
    },
    show_left_right: function () {
      console.log(this.view);
      switch (this.view) {
        case "channel":
        case "start":
        case "search":
          if (this.vr_media == "live") {
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");

            var sliderView = new SliderView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderView.create();
          } else if (this.vr_media == "catchup") {
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");

            var sliderView = new SliderView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderView.create();
          } else if (this.vr_media == "catchup_show") {
            var sliderVodView = new SliderVodView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderVodView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
          } else if (this.vr_media == "vod" || this.vr_media == "vod_tvshow") {
            var sliderVodView = new SliderVodView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderVodView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
            $("#custom-handle").focus();

            //this.bus.trigger("RouterClose");
            //this.$el.empty();
          }

          return false;
          break;
        case "vodcontent":
          st_complete = "1";
          var sliderVodView = new SliderVodView({
            el: "#player_container",
            roots: this.roots,
            bus: this.bus,
          });
          sliderVodView.create();
          $("#screen_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#player_view_container")
            .removeClass("inactive")
            .addClass("active");
          $("#custom-handle").focus();

          return false;
          break;
      }
    },

    show_ok: function () {
      console.log(this.view);
      switch (this.view) {
        case "channel":
          //player.stop();
          this.showChannelsContainer("show");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");
          this.bus.trigger("SetPositionchannle", {
            id: channel_focus.get("id_select"),
          });
          $("#channel").focus();
          return false;
          break;

        case "start":
        case "search":
        case "vodcontent":
          if (this.vr_media == "live") {
            st_complete = "1";
            var sliderView = new SliderView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
            $("#custom-handle").focus();
          } else if (this.vr_media == "catchup") {
            st_complete = "1";
            var sliderView = new SliderView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
            $("#custom-handle").focus();
          } else if (this.vr_media == "catchup_show") {
            st_complete = "1";
            var sliderVodView = new SliderVodView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderVodView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
            $("#custom-handle").focus();
          } else if (this.vr_media == "vod" || this.vr_media == "vod_tvshow") {
            st_complete = "1";

            var sliderVodView = new SliderVodView({
              el: "#player_container",
              roots: this.roots,
              bus: this.bus,
            });
            sliderVodView.create();
            $("#screen_view_container")
              .removeClass("active")
              .addClass("inactive");
            $("#player_view_container")
              .removeClass("inactive")
              .addClass("active");
            $("#custom-handle").focus();
          }
      }
    },

    show_return: function () {
      console.log(this.view);
      switch (this.view) {
        case "channel":
          //player.stop();
          this.showChannelsContainer("show");
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");
          this.bus.trigger("SetPositionchannle", {
            id: channel_focus.get("id_select"),
          });
          $("#channel").focus();
          return false;
          break;
        case "start":
          player.stop();
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          var startView = new StartView({
            el: "#other_container",
            bus: this.bus,
          });
          startView.create();

          return false;
          break;
        case "search":
          player.stop();
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          var searchView = new SearchView({
            el: "#other_container",
            bus: this.bus,
          });
          searchView.create();

          return false;
          break;
        case "vodcontent":
          player.stop();

          console.log(this.roots);
          $("#player_view_container")
            .removeClass("active")
            .addClass("inactive");
          $("#screen_view_container")
            .removeClass("inactive")
            .addClass("active");

          var vodContent = new VodContentView({
            el: "#other_container",
            bus: this.bus,
          });
          vodContent.create({ item: vod_focus.toJSON(), roots: this.roots });

          return false;
          break;
      }
    },

    showChannelsContainer: function (st) {
      if (st == "show") {
        $("#epg-list").show();
        $("#category-list").show();
        $("#channel-list").show();
        $("#menucateg_container").show();
      } else {
        $("#epg-list").hide();
        $("#category-list").hide();
        $("#channel-list").hide();
        $("#menucateg_container").hide();
      }

      return this;
    },
  });

  return CategoryView;
});

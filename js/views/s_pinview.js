define(["jquery", "underscore", "backbone", "text!page/s_pin.html"], function (
  $,
  _,
  Backbone,
  Template
) {
  var keyboard_container_id = "#keyboard_container";
  var message_field_id = "#message_field";
  var input_field_id = "#pin-input_field";
  var key_foc_id = "#key_foc";
  var animation_duration = 70;

  var init_pin_view = function (args) {
    var new_div = '<div id="keyboard_page_container" tabindex="1"></div>';
    $("#other_container").append(new_div);
    args.el = "#keyboard_page_container";
    return new pin_view(args);
  };

  var pin_view = Backbone.View.extend({
    initialize: function (options) {
      this.bus = options.bus;
      this.func = options.func;
      this.row = 0;
      this.col = 0;
      this.current_text = "";
      this.set_keyboard();
      this.render();
    },

    set_keyboard: function () {
      this.cell_size = 100;
      this.margins = 10;
      this.container_width = this.cell_size * 3 + this.margins * 2;
      this.container_height =
        this.cell_size * 4 + 3 * this.margins + 50 + 50 + 20;
      this.keyboard = [
        [
          {
            id: "keyb_1",
            value: "1",
            ico: "1",
            tag: "col-xs-4 cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 100,
          },
          {
            id: "keyb_2",
            value: "2",
            ico: "2",
            tag: "col-xs-4 cell",
            left: 0,
            right: 2,
            top: 1,
            down: 1,
            x: 10 + 100,
            width: 100,
          },
          {
            id: "keyb_3",
            value: "3",
            ico: "3",
            tag: "col-xs-4 cell",
            left: 1,
            right: 2,
            top: 2,
            down: 2,
            x: 20 + 200,
            width: 100,
          },
        ],
        [
          {
            id: "keyb_4",
            value: "4",
            ico: "4",
            tag: "col-xs-4 cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 100,
          },
          {
            id: "keyb_5",
            value: "5",
            ico: "5",
            tag: "col-xs-4 cell",
            left: 0,
            right: 2,
            top: 1,
            down: 1,
            x: 10 + 100,
            width: 100,
          },
          {
            id: "keyb_6",
            value: "6",
            ico: "6",
            tag: "col-xs-4 cell",
            left: 1,
            right: 2,
            top: 2,
            down: 2,
            x: 20 + 200,
            width: 100,
          },
        ],
        [
          {
            id: "keyb_7",
            value: "7",
            ico: "7",
            tag: "col-xs-4 cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 100,
          },
          {
            id: "keyb_8",
            value: "8",
            ico: "8",
            tag: "col-xs-4 cell",
            left: 0,
            right: 2,
            top: 1,
            down: 1,
            x: 10 + 100,
            width: 100,
          },
          {
            id: "keyb_9",
            value: "9",
            ico: "9",
            tag: "col-xs-4 cell",
            left: 1,
            right: 2,
            top: 2,
            down: 1,
            x: 20 + 200,
            width: 100,
          },
        ],
        [
          {
            id: "keyb_0",
            value: "0",
            ico: "0",
            tag: "col-xs-4 cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 100,
          },
          {
            id: "keyb_back",
            value: "",
            ico: '<i class="bx bx-arrow-back"></i>',
            tag: "col-xs-8 double",
            left: 0,
            right: 1,
            top: 1,
            down: 1,
            x: 10 + 100,
            width: 200 + 10,
          },
        ],
      ];

      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          this.keyboard[i][j].y = i * this.cell_size + i * this.margins;
        }
      }
    },

    animate_selection: function (rowoff, coloff) {
      var that = this;

      function valid_pos(row, col) {
        if (0 <= row && row < that.keyboard.length) return true;
        else return false;
      }

      if (valid_pos(this.row + rowoff)) {
        var last_val_col = that.col;
        var last_val_row = that.row;

        var elem = that.keyboard[that.row][that.col];
        if (rowoff == 1) {
          that.col = elem.down;
        } else if (rowoff == -1) {
          that.col = elem.top;
        }

        that.row += rowoff;

        if (coloff == 1) {
          that.col = elem.right;
        } else if (coloff == -1) {
          that.col = elem.left;
        }
        if (last_val_col != that.col || last_val_row != that.row) {
          elem = that.keyboard[that.row][that.col];
          $(key_foc_id).animate(
            {
              left: elem.x + "px",
              top: elem.y + "px",
              width: elem.width + "px",
            },
            { duration: animation_duration }
          );
        }
      }
    },

    action_button: function () {
      var key = this.keyboard[this.row][this.col];
      if (key.id == "keyb_back") {
        this.current_text = this.current_text.slice(0, -1);
        $("#inp" + this.current_text.length.toString()).html("");
      } else {
        $("#inp" + this.current_text.length.toString()).html("*");
        this.current_text += key.value;
        if (this.current_text.length == 4) {
          this.submit();
        }
      }
    },

    remote_num_press: function (num) {
      $("#inp" + this.current_text.length.toString()).html("*");
      this.current_text += num.toString();
      if (this.current_text.length == 4) {
        this.submit();
      }
    },

    key_action_down: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation(); // zbog bubblinga
      switch (e.keyCode) {
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:
          console.log();
          this.remote_num_press(e.keyCode - 48);
          break;
        case KEY_RIGHT:
          this.animate_selection(0, 1);
          break;
        case KEY_LEFT:
          this.animate_selection(0, -1);
          break;
        case KEY_DOWN:
          this.animate_selection(1, 0);
          break;
        case KEY_UP:
          this.animate_selection(-1, 0);
          break;
        case KEY_OK:
          this.action_button();
          break;
        case KEY_BACK:
          this.on_close();
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
          this.on_close();
          this.bus.trigger("ExpandMenu");
          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },

    events: {
      keydown: "key_action_down",
    },

    submit: function () {
      this.func(this.current_text);
    },

    render: function () {
      // html
      var template = _.template(Template);
      var html = template({ keyboard: this.keyboard });
      this.$el.html(html);
      // cell css
      this.set_cell_css();
      // focus
      this.exfocus = "#" + document.activeElement.id;
      this.$el.focus();
    },

    set_cell_css: function () {
      $(keyboard_container_id).css({
        position: "relative",
        width: this.container_width + "px",
        left: (1280 - this.container_width) / 2 + "px",
        height: this.container_height + "px",
        top: (720 - (20 + this.container_height)) / 2 + "px",
      });

      $(".keyboard").css({
        position: "relative",
        "font-size": "30px",
        "box-sizing": "border-box",
      });

      $(".cell").css({
        width: this.cell_size + "px",
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      $(".double").css({
        width: this.cell_size * 2 + 1 * this.margins + "px",
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          $("#" + this.keyboard[i][j].id).css({
            top: this.keyboard[i][j].y,
            left: this.keyboard[i][j].x,
          });
        }
      }

      $(key_foc_id).css({
        width: this.cell_size + "px",
        height: this.cell_size + "px",
        top: "0px",
        left: "0px",
      });
    },

    on_close: function () {
      $(this.exfocus).focus(); // returns focus
      this.remove();
    },

    show_message: function (message) {
      $(message_field_id).html(message);
    },

    clear_input: function () {
      for (var i = 0; i < 4; i++) {
        $("#inp" + i.toString()).html("");
      }
      this.current_text = "";
    },
  });

  return init_pin_view;
});

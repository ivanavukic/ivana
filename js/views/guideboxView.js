define([
    'jquery',
    'underscore',
    'backbone',
    'collections/epgCollections',
    'text!page/guidebox.html'
], function($, _, Backbone, EpgCollections, template) {

    var st_complete = "";

    var GuideBox = Backbone.View.extend({

        tagName: "span",

        initialize: function(options) {

            this.channel = options.channel;
            this.bus = options.bus;
        },

        render: function() {


            //console.log('##', this.channel);
            //console.log('##', this.channel.get('image'));

            var templ = _.template(template);
            var html = templ({ c: this.channel });
            this.$el.html(html);

            return this;
        }

    });

    return GuideBox;
});
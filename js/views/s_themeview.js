define([
  "jquery",
  "underscore",
  "backbone",
  "text!page/s_theme.html",
], function ($, _, Backbone, Template) {
  var init_theme_view = function (args) {
    var temporary_container =
      "<div id='theme_temporary_container' tabindex='1'></div>";
    $("#right_column").append(temporary_container);
    args.el = "#theme_temporary_container";
    return new theme_view(args);
  };

  var default_style = "css/style.css";
  var custom_style = "css/style_dark.css";

  var theme_view = Backbone.View.extend({
    initialize: function (options) {
      this.el = options.el;
      this.bus = options.bus;
      this.render();
    },

    render: function () {
      var template = _.template(Template);
      var html = template();
      this.$el.html(html);

      this.set_initial_state();
      this.exfocus = "#" + document.activeElement.id;
      this.$el.focus();
    },

    set_initial_state: function () {
      // $('ul[class="options-list"]').attr("style", "display:none"); //CEDA
      if (
        $('link[href="' + default_style + '"]').attr("href") == default_style
      ) {
        $("#toggle_theme").addClass("bx bx-toggle-right");
        $("#sun").css("color", "##ffa500");
      } else {
        $("#toggle_theme").addClass("bx bx-toggle-left");
        $("#moon").css("color", "##ffa500");
      }
    },

    events: {
      keydown: "key_action",
    },

    key_action: function (e) {
      e.stopPropagation();

      switch (e.keyCode) {
        case KEY_LEFT:
          console.log("LEVA TEMA"); //CEDA
          console.log($(this.exfocus).focus());
          console.log('link[href="' + default_style + '"]');
          if (
            $('link[href="' + default_style + '"]').attr("href") ==
            default_style
          ) {
            // set_theme('dark');
            $("#toggle_theme").removeClass();
            $("#sun").css("color", "grey");
            $("#moon").css("color", "#ffa500");
            $("#toggle_theme").addClass("bx bx-toggle-left");
            $('link[href="' + default_style + '"]').attr("href", custom_style);
            console.log("PPPPPPPPPPPromenjena u dark temu"); //IVANA
            console.log('link[href="' + custom_style + '"]'); //IVANA

            $("#btn_logo").attr("src", "image/logo_dark.png");
          }
          break;

        case KEY_RIGHT:
          console.log("DESNO TEMA"); //CEDA
          console.log('link[href="' + default_style + '"]');
          if (
            $('link[href="' + default_style + '"]').attr("href") !=
            default_style
          ) {
            // set_theme('light');
            console.log("DESNO USAO U IF"); //CEDA
            $("#toggle_theme").removeClass();
            $("#moon").css("color", "grey");
            $("#sun").css("color", "#ffa500");
            $("#toggle_theme").addClass("bx bx-toggle-right");
            $('link[href="' + custom_style + '"]').attr("href", default_style);
            $("#btn_logo").attr("src", "image/logo_gray.png");
          }
          break;

        case KEY_BACK:
          //   $('ul[class="options-list"]').attr("style", ""); //CEDA
          this.on_close();
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
          this.on_close();
          this.bus.trigger("ExpandMenu");
          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },

    on_close: function () {
      $(this.exfocus).focus();
      console.log("VVVVVVVVVVVVVVVVVVVVV"); //CEDA
      this.bus.trigger("CancelSettingHighlight");
      // $("#right_column").addClass("bcg");
      this.remove();
    },
  });
  return init_theme_view;
});

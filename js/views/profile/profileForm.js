define([
    'jquery',
    'underscore',
    'backbone',
    "views/profilesView",
    "views/s_keyboardview",
    "coreLogin",
    "coreAccount",
    "models/profile/profile_item",
    'text!page/profile/profileForm.html'
], function ($, _, Backbone, ProfilesView, KeyboardView, CoreLogin, CoreAccount, profile_model, template) {

    var ProfileForm = Backbone.View.extend({

        initialize: function (options) {
            this.el = options.el;
            this.bus = options.bus;
            this.currentIndex = 0;
            this.focussable = [];
            this.profile = options.profile;
            this.isEditMode = options.isEditMode;

            // profile_model.on('change', this.render(), this);
            this.bus.on("ProfileFormClose", this.onClose, this);
        },

        render: function () {
            console.log('Profile Form Page');
            var templ = _.template(template);

            // if edit - pass info about the selected profile;
            var html = templ({ isEditMode: this.isEditMode, profile: this.profile.toJSON() });
            this.$el.html(html);

            document.activeElement.blur();
            this.setFocus(0);

            // set the initial state of the child toggleButton
            this.setInitialChildValue(this.profile.get("ratingID"));
            this.toggleChildMode();

            return this;
        },

        events: {
            keydown: "keyAction"
        },

        keyAction: function (e) {
            e.stopPropagation();
            e.stopImmediatePropagation();
            default_key(e.keyCode);
            switch (e.keyCode) {
                case KEY_UP:
                    console.log("PREVIOUS");
                    this.setFocus(-1);
                    return false;
                    break;
                case KEY_DOWN:
                    console.log("NEXT");
                    this.setFocus(1);
                    return false;
                    break;
                case KEY_ENTER:
                    this.onOkKeydown();
                    return false;
                    break;
                case KEY_RETURN:
                    // console.log("Open profiles");
                    // var profilesView = new ProfilesView({ el: "#other_container", bus: this.bus });
                    // profilesView.create();        
                    // this.onClose();
                    // TODO: remove
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                case KEY_BACK:
                    // console.log("Open profiles");
                    // var profilesView = new ProfilesView({ el: "#other_container", bus: this.bus });
                    // profilesView.create();        
                    // this.onClose();
                    // TODO: remove
                    this.bus.trigger("ExpandMenu");
                    return false;
                    break;
                // case KEY_HOME:
                //     if (st_complete !== "1") this.bus.trigger("ExpandMenu");
                //     return false;
                //     break;
                // case KEY_MENU:
                //     if (st_complete !== "1") this.bus.trigger("ExpandMenu");
                //     return false;
                //     break;
            }

            return false;
        },

        setFocus: function (offset) {
            this.focussable = this.getFocusableElements();

            // set initial focus
            if (offset == 0) {
                this.focussable[this.currentIndex].focus();
                return false;
            }

            // index of currently focused element in the list of focussable elements
            var index = _.findIndex(this.focussable, function (el) {
                if (el.id === document.activeElement.id) return el;
            });

            if (index > -1) {
                // iterating through the circular list, set new index
                this.currentIndex = ((index + offset + this.focussable.length) % this.focussable.length);

                // set new focus
                this.focussable[this.currentIndex].focus();
            }
        },

        getFocusableElements: function () {
            return this.$el.find("button, *[tabindex]").not("[disabled], [tabindex=-1]");
        },

        onOkKeydown: function () {
            var focused = document.activeElement;

            if (focused.id === "profile_name_wrapper") {
                console.log("Enter: profile name");

                // open keyboard
                var that = this;
                var keyboard = new KeyboardView({
                    bus: that.bus,
                    current_text: profile_model.get("name"),
                    func: function (text) {
                        keyboard.on_close();
                        that.onKeyboardClose(text);
                    }
                });

                return false;
            }

            if (focused.id === "profile_pass_wrapper") {
                console.log("Enter: profile pass");

                // if (this.isEditMode) {
                //     this.changePin();
                // }
            }

            if (focused.id === "profile_type_tgl_text") {
                console.log("Enter: toggle child");

                // toggle child option on/off

                this.isChild = !(this.isChild);
                this.toggleChildMode();
            }

            // if focused element is a button, call API SAVE (create/modify) profile method
            if (($("#" + focused.id).is(":button"))) {
                console.log("Enter: add profile");

                var self = this;

                if (!(this.isEditMode)) {
                    console.log("Create Profile.");
                    CoreAccount.createProfile(profile_model.toJSON())
                        .then(function (res) {
                            console.log("Profile saved!");
                            console.log(res);
                        }).catch(function(error) {
                            console.log(error);
                        });
                }
                else {
                    console.log("Edit Profile.");
                    console.log(profile_model.toJSON());
                    CoreAccount.modifyProfile(profile_model.toJSON())
                        .then(function (res) {
                            console.log("Profile saved!");
                            console.log(res);
                        }).catch(function(error) {
                            console.log(error);
                        });
                }

                self.onClose();

                return false;
            }
        },

        setInitialChildValue: function (ratingId) {
            this.isChild = ratingId == "180" ? false : true;

            if (!(this.isChild)) {
                $("#profile_type_button").children().toggleClass("bx bx-toggle-right");
            }
        },

        toggleChildMode: function () {
            var focused = document.activeElement;
            $("#" + focused.id).find(".profile-type-tgl-btn").children().toggleClass("bx-toggle-right");

            var ratingId = "";
            ratingId = this.isChild ? "12" : "180";
            profile_model.set("ratingID", ratingId);
        },

        onKeyboardClose: function (keyboardInput) {
            console.log("Closed keyboard");

            var name = keyboardInput.length > 0 ? keyboardInput : $(":focus").children().text();
            // TODO: remove this line after the model.on("change") is added
            $(":focus").children().text(name);

            profile_model.set("name", name);

            // validate input
            // var inputValid = this.validateInput();

            // enable/disable save button based on validation
            // if (this.buttonEnabled != inputValid) this.toggleButton(inputValid);
        },

        onClose: function () {
            var profilesView = new ProfilesView({ el: "#other_container", bus: this.bus });
            profilesView.create();

            console.log("Close Profile Form.");

            this.undelegateEvents();
            this.$el.removeData().unbind();

            this.$el.empty();
            this.unbind();
            this.stopListening();

            return false;
        }
    });

    return ProfileForm;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "models/s_dynamicviewmodel",
  "text!page/s_rightcolumn.html",
], function ($, _, Backbone, DynamicViewModel, Template) {
  var init_dynamic_view = function (args) {
    var mod = new DynamicViewModel({
      list_function: args.list_function,
      list_function_args: args.list_function_args,
    });
    var view_args = {};
    view_args.el = "#right_column";
    view_args.model = mod;
    view_args.bus = args.bus;
    return new dynamic_view(view_args);
  };

  var line_height = 55;
  var position_of_scroll_start = 250;

  var dynamic_view = Backbone.View.extend({
    initialize: function (options) {
      this.el = options.el;
      this.bus = options.bus;
      this.model = options.model;
      this.bus.on("Execute", this.execute, this);
      this.bus.on("Render", this.render, this);
      this.active_option_id = 0;
      this.execute();
    },

    execute: function () {
      this.list_function = this.model.list_function;
      this.list_function_args = this.model.list_function_args;
      this.list_function(this.list_function_args);
    },

    set_list: function (list) {
      this.list = list;
    },

    render: function () {
      this.title = this.list[0].name;
      this.list = this.list.slice(1);
      this.no_active_element = false;
      this.first_active_option_selection();
      var template = _.template(Template);
      var html = template({ list: this.list, title: this.title });
      this.$el.html(html);
      this.$el.focus();
      this.set_inactive_and_static_option_css();
      if (this.no_active_element == false)
        $("#" + this.list[this.active_option_id].id).addClass("select");
      this.scroll();
    },

    first_active_option_selection: function () {
      while (this.list[this.active_option_id].inactive) {
        this.active_option_id =
          (this.active_option_id + 1 + this.list.length) % this.list.length;
        if (
          this.active_option_id == this.list.length - 1 &&
          this.list[this.active_option_id].inactive
        ) {
          console.log("Nema aktivnih elemenata.");
          this.no_active_element = true;
          this.active_option_id = 0;
          break;
        }
      }
    },

    set_inactive_and_static_option_css: function () {
      for (var i = 0; i < this.list.length; i++) {
        if (this.list[i].inactive) {
          $("#" + this.list[i].id).addClass("inactive-option");
        }
        if (this.list[i].static) {
          $("#" + this.list[i].id).addClass("static-option");
        }
      }
    },

    set_view: function (direction) {
      var ref;
      if (direction == 1) {
        ref = this.list[this.active_option_id];
        if (ref.optionsList) {
          this.model.list_function_stack.push({
            list_function: this.list_function,
            list_function_args: this.list_function_args,
          });
          this.model.id_stack.push(this.active_option_id);
          this.active_option_id = 0;
          this.model.list_function_args = ref.optionsList.list_function_args;
          this.model.list_function = ref.optionsList.list_function;
          this.execute();
          //this.render();
        } else {
          ref.onclick(ref.args);
        }
      } else {
        this.active_option_id = this.model.id_stack.pop();
        var obj = this.model.list_function_stack.pop();
        this.model.list_function_args = obj.list_function_args;
        this.model.list_function = obj.list_function;
        this.execute();
        //this.render();
      }
    },

    events: {
      keydown: "key_action",
    },

    selection_animation: function (offset) {
      if (this.sort_logic) {
        this.sort_animation(offset);
        return;
      } else if (this.reminder_logic) {
        return;
      }
      if (this.no_active_element == true) {
        this.inactive_scroll(offset);
        return;
      }
      $("#" + this.list[this.active_option_id].id).removeClass("select");
      this.active_option_id =
        (this.active_option_id + offset + this.list.length) % this.list.length;
      while (this.list[this.active_option_id].inactive)
        this.active_option_id =
          (this.active_option_id + offset + this.list.length) %
          this.list.length;
      $("#" + this.list[this.active_option_id].id).addClass("select");
      this.scroll();
    },

    inactive_scroll: function (offset) {
      document.getElementById("right_column").children[1].scrollTop +=
        offset * line_height;
    },

    scroll: function () {
      document.getElementById("right_column").children[1].scrollTop =
        -position_of_scroll_start + this.active_option_id * line_height;
    },

    toggle_reminder_logic: function (args) {
      if (typeof this.reminder_logic !== "undefined") {
        console.log("Reminder logic off.");
        if (args.key == "back") {
          $("#" + this.list[this.active_option_id].id).removeClass(
            "delete-reminder"
          );
          $("#" + this.list[this.active_option_id].id).addClass("select");
          document.getElementById(
            this.list[this.active_option_id].id
          ).children[0].innerHTML = this.reminder_text;
        }
        this.active_option_id += -1;
        if (this.active_option_id == -1) this.active_option_id = 0;
        delete this.reminder_logic;
        delete this.delete_reminder;
        delete this.reminder_text;
        if (args.key == "enter") this.trigger_flag();
      } else {
        console.log("Reminder logic on.");
        this.reminder_logic = true;
        this.delete_reminder = args.func;
        this.reminder_text = document.getElementById(
          this.list[this.active_option_id].id
        ).children[0].innerHTML;
        var trashcan = "<i class='bx bx-trash'></i>";
        document.getElementById(
          this.list[this.active_option_id].id
        ).children[0].innerHTML = trashcan + " " + this.reminder_text;
        $("#" + this.list[this.active_option_id].id).removeClass("select");
        $("#" + this.list[this.active_option_id].id).addClass(
          "delete-reminder"
        );
      }
    },

    toggle_sort_logic: function (args) {
      if (typeof this.sort_logic !== "undefined") {
        console.log("Sort logic off.");
        delete this.sort_logic;
        $("#channel_" + this.active_option_id.toString()).removeClass(
          "sort-select"
        );
        $("#channel_" + this.active_option_id.toString()).addClass("select");
        delete this.swap_function;
        var element = document.getElementById(
          this.list[this.active_option_id].id
        ).children[0];
        document
          .getElementById(this.list[this.active_option_id].id)
          .children[0].removeChild(element.firstChild);
      } else {
        console.log("Sort logic on.");
        /* Api? */
        this.sort_logic = true;
        this.swap_function = args.func;
        $("#channel_" + this.active_option_id.toString()).removeClass("select");
        $("#channel_" + this.active_option_id.toString()).addClass(
          "sort-select"
        );
        var chev = "<i class='bx bxs-chevron-right'></i>";
        var current = document.getElementById(
          this.list[this.active_option_id].id
        ).children[0].innerHTML;
        document.getElementById(
          this.list[this.active_option_id].id
        ).children[0].innerHTML = chev + " " + current;
      }
    },

    sort_animation: function (offset) {
      if (
        (offset == 1 && this.active_option_id == this.list.length - 1) ||
        (offset == -1 && this.active_option_id == 0)
      ) {
        return;
      }
      $("#" + this.list[this.active_option_id].id).removeClass("sort-select");
      var temp = this.active_option_id;
      this.active_option_id = this.active_option_id + offset;
      $("#" + this.list[this.active_option_id].id).addClass("sort-select");
      this.swap_function({ prev: temp, next: this.active_option_id });
      this.swap_animation(temp, this.active_option_id);
      this.scroll();
    },

    swap_animation: function (prev, next) {
      var name = document.getElementById(this.list[prev].id).children[0]
        .innerHTML;
      var id = document.getElementById(this.list[prev].id).children[1]
        .innerHTML;
      document.getElementById(this.list[prev].id).children[0].innerHTML =
        document.getElementById(this.list[next].id).children[0].innerHTML;
      document.getElementById(this.list[prev].id).children[1].innerHTML =
        document.getElementById(this.list[next].id).children[1].innerHTML;
      document.getElementById(this.list[next].id).children[0].innerHTML = name;
      document.getElementById(this.list[next].id).children[1].innerHTML = id;
    },

    key_action: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation(); // zbog bubblinga
      switch (e.keyCode) {
        case KEY_UP:
          this.selection_animation(-1);
          break;
        case KEY_DOWN:
          this.selection_animation(1);
          break;
        case KEY_BACK:
          if (this.model.id_stack.length == 0) {
            if (typeof this.reminder_logic !== "undefined") {
              this.toggle_reminder_logic({ key: "back" });
            } else this.on_close();
          } else {
            if (typeof this.sort_logic !== "undefined") {
              this.toggle_sort_logic();
            }
            this.set_view(-1);
          }
          break;
        case KEY_ENTER:
          if (this.reminder_logic) {
            this.delete_reminder({ ind: this.active_option_id });
            this.toggle_reminder_logic({ key: "enter" });
            return;
          }
          this.set_view(1);
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
          this.on_close();
          this.bus.trigger("ExpandMenu");
          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },

    on_close: function () {
      $("#left_column").focus();
      this.bus.trigger("CancelSettingHighlight");
      this.bus.trigger("ClearApidata");
      this.bus.off("Render");
      this.bus.off("Execute");
      this.remove();
      $("#settings")
        .children()
        .append(
          "<div id='right_column' class='col-md-8 rcol bcg' tabindex='3'>"
        );
    },
  });
  return init_dynamic_view;
});

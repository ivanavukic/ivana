define(["jquery", "underscore", "backbone", "text!page/s_scf.html"], function (
  $,
  _,
  Backbone,
  Template
) {
  var init_screen_config_view = function (args) {
    var temporary_container =
      "<div id='screenconfig_temporary_container' tabindex='1'></div>";
    $("#other_container").append(temporary_container);
    args.el = "#screenconfig_temporary_container";
    return new screen_config_view(args);
  };

  var screen_config_view = Backbone.View.extend({
    initialize: function (options) {
      this.bus = options.bus;
      this.zoomW = parseInt(Utility.getValueByName("displayZoomWidth"));
      this.zoomH = parseInt(Utility.getValueByName("displayZoomHeight"));
      this.id_map = ["#sscUp", "#sscDown", "#sscRight", "#sscLeft"];
      this.render();
    },

    render: function () {
      var template = _.template(Template);
      var html = template();
      this.$el.html(html);

      this.exfocus = "#" + document.activeElement.id;
      this.animate_percenage();
      this.$el.focus();
    },

    animate_percenage: function () {
      // var str = "Širina: " + this.zoomW.toString() + "%" + '\n' + "Visina: " + this.zoomH.toString() + "%";
      // var cont = '<pre>'+ str +'</pre>'
      // $("#scfg_percentage").html(cont);
      $("#scfg_percentage").html(
        "Širina: " +
          this.zoomW.toString() +
          "%" +
          "<br>" +
          "Visina: " +
          this.zoomH.toString() +
          "%"
      );
    },

    on_close: function () {
      $(this.exfocus).focus();
      this.bus.trigger("CancelSettingHighlight");
      $("#right_column").addClass("bcg");
      this.remove();
    },

    events: {
      keydown: "key_action",
    },

    animate_arrow: function (flag) {
      $.fn.extend({
        qcss: function (css) {
          return $(this).queue(function (next) {
            $(this).css(css);
            next();
          });
        },
      });

      $(this.id_map[flag])
        .delay(0)
        .qcss({ color: "#ed1a3b" })
        .delay(250)
        .qcss({ color: "" });

      this.animate_percenage();
    },

    key_action: function (e) {
      e.stopPropagation();

      switch (e.keyCode) {
        case KEY_UP:
          this.animate_arrow(0);
          this.zoomH += 1;
          Utility.setValueByName("displayZoomHeight", this.zoomH);
          break;
        case KEY_DOWN:
          this.animate_arrow(1);
          this.zoomH -= 1;
          Utility.setValueByName("displayZoomHeight", this.zoomH);
          break;
        case KEY_RIGHT:
          this.animate_arrow(2);
          this.zoomW += 1;
          Utility.setValueByName("displayZoomWidth", this.zoomW);
          break;
        case KEY_LEFT:
          this.animate_arrow(3);
          this.zoomW -= 1;
          Utility.setValueByName("displayZoomWidth", this.zoomW);
          break;
        case KEY_BACK:
        case KEY_ENTER:
          this.on_close();
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
          this.on_close();
          this.bus.trigger("ExpandMenu");
          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },
  });

  return init_screen_config_view;
});

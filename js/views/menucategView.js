define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "collections/categoryCollections",
  "models/category_focus",
  "text!page/menucateg.html"
], function ($, _, Backbone, tv, CategoryCollections, category_focus, template) {
  var st_complete = "0";

  var MenuCateg = Backbone.View.extend({
    initialize: function (options) {
      this.st_open_menu = false;

      this.bus = options.bus;
      this.bus.on("ExpandCategoryMenu", this.onExpandCategoryMenu, this);
      this.bus.on("FocusCategories", this.onSetPosition, this);
    },

    create: function () {
      var self = this;
      tv.getKategorije()
        .then(function (res) {
          console.log(res);
          CategoryCollections.reset();
          CategoryCollections.push(res);
          self.cat_data = res;
        })
        .then(function () {
          //console.log("##************************************");
          self.x = _.findIndex(self.cat_data, { code: category_focus.get("id").toString() });
          self.render();
        });
    },

    onExpandCategoryMenu: function () {
      $("#channel").css("opacity", "0");
      $("#menucateg_container").css("visibility", "visible");
      $("#menu_category").css("display", "none");
      $("#menu_category").css("visibility", "visible");

      $("#menucateg_container").animate(
        { width: "+=16.796875vw" },
        {
          duration: 150,
          complete: function () {
            $("#menu_category").css("display", "table-cell");
            $("#menu_category").css("visibility", "visible");
          }
        }
      );

      var cat = this;

      $("#channel_group").animate(
        { marginLeft: "+=16.796875vw" },
        {
          duration: 150,
          complete: function () {
            $("#channel").css("opacity", "0");
            $("#menu_category").css("display", "table-cell");
            cat.st_open_menu = true;
            $("#menucateg_container").css("width", $("#menu_category").width());
            cat.onSetPosition();
            $("#id_mc_" + category_focus.get("id")).focus();

            /*
            var focused = document.activeElement;
            console.log(focused);
            */

            //console.log('category_focus.get("id")',category_focus.get("id"));

            //dbgLog("$$$ " + focused.id + " " + focused + " $$$");
            //dbgLog('#id_mc_' + category_focus.get("id"));
          }
        }
      );
    },

    onCollapsCategoryMenu: function (data) {
      $("#channel").css("opacity", "0");
      $("#menu_category").css("visibility", "hidden");

      self = this;
      $("#channel_group").animate(
        { marginLeft: "0px" },
        {
          duration: 150,
          complete: function () {
            self.st_open_menu = false;
            if (data.key == "left" || data.key == "back" || data.key == "return") {
              $("#channel").css("opacity", "1");
              self.bus.trigger("ExpandMenu");
            } else {
              $("#channel").css("opacity", "1");
              $("#channel").focus();
            }
            //$('#menu_category').css('display', 'none');
          }
        }
      );

      $("#menucateg_container").css("visibility", "hidden");

      //$('#menu_category').css('display', 'none');

      $("#menucateg_container").animate(
        { width: "0px" },
        {
          duration: 150,
          complete: function () {
            $("#menucateg_container").css("visibility", "hidden");
            $("#menu_category").css("display", "table-cell");
          }
        }
      );
      this.st_open_menu = false;
    },

    onSetPosition: function () {
      this.$el.find("#menu_category").parent().find("div[tabindex]").removeClass("focus");
      //console.log("#id_mc_" + category_focus.get("id"));
      dbgLog("#id_mc_" + category_focus.get("id"));
      this.$el.find("#id_mc_" + category_focus.get("id")).addClass("focus");
      $("#id_category_1").text(category_focus.get("name"));
      this.$el.find("#id_mc_" + category_focus.get("id")).focus();
    },

    events: {
      keydown: "keyActionDown"
    },

    keyActionDown: function (e) {
      //console.log(e);

      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i

      //console.log(e);

      default_key(e.keyCode);

      console.log(this.x);

      //this.x = Number(category_focus.get("id"));
      this.count = this.cat_data.length;

      switch (e.keyCode) {
        case KEY_DOWN:
          console.log("DOLJE");
          if (this.st_open_menu !== false) {
            if (st_complete !== "1") {
              if (this.x < this.count - 1) {
                this.x = this.x + 1;
              }
            }
            console.log(this.cat_data);
            category_focus.set(
              { id: this.cat_data[this.x].code, name: this.cat_data[this.x].name },
              { validate: true }
            );
            this.onSetPosition();
            this.bus.trigger("CategoryChannel", { code: Number(category_focus.get("id")) });
          }
          break;
        case KEY_UP:
          console.log("GORE");
          if (this.st_open_menu !== false) {
            if (st_complete !== "1") {
              if (this.x > 0) {
                this.x = this.x - 1;
              }
            }

            category_focus.set(
              { id: this.cat_data[this.x].code, name: this.cat_data[this.x].name },
              { validate: true }
            );
            this.onSetPosition();
            this.bus.trigger("CategoryChannel", { code: Number(category_focus.get("id")) });
          }
          break;
        case KEY_RIGHT:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "right" });
          }
          break;
        case KEY_LEFT:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "left" });
          }
          break;
        case KEY_RETURN:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "return" });
          }
          break;
        case KEY_BACK:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "back" });
          }
          break;
        case KEY_ENTER:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "enter" });
          }
          break;
        case KEY_OK:
          if (this.st_open_menu !== false) {
            this.onCollapsCategoryMenu({ key: "ok" });
          }
          break;
      }
      /*
      console.log(this.cat_data);
      console.log("this.x", this.x);
      console.log("this.count", this.count);
      console.log("this.st_open_menu",this.st_open_menu);
      console.log("st_complete",st_complete);

      console.log('get("id")',category_focus.get("id"));
      console.log('code',this.cat_data[this.x].code);
      console.log('name',this.cat_data[this.x ].name);
*/

      return false;
    },

    render: function () {
      //console.log(this.cat_data);

      var templ = _.template(template);

      var html = templ({
        result: this.cat_data,
        st_open_menu: this.st_open_menu,
        name: category_focus.get("name")
      });
      this.$el.html(html);

      return this;
    }
  });

  return MenuCateg;
});

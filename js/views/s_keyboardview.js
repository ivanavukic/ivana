define([
  "jquery",
  "underscore",
  "backbone",
  "text!page/s_keyboard.html",
], function ($, _, Backbone, Template) {
  var keyboard_container_id = "#keyboard_container";
  var message_field_id = "#message_field";
  var input_field_id = "#input_field";
  var key_foc_id = "#key_foc";
  var animation_duration = 70;

  var init_keyboard_view = function (args) {
    var new_div = '<div id="keyboard_page_container" tabindex="1"></div>';
    $("#other_container").append(new_div);
    args.el = "#keyboard_page_container";
    return new keyboard_view(args);
  };

  var keyboard_view = Backbone.View.extend({
    initialize: function (options) {
      this.bus = options.bus;
      this.func = options.func;
      this.current_text = options.current_text;
      if (!options.current_text) {
        this.current_text = "";
      }
      this.shift_on = false;
      this.row = 0;
      this.col = 0;
      this.set_keyboard();
      this.render();
    },

    set_keyboard: function () {
      this.cell_size = 50;
      this.margins = 5;
      this.container_width = this.cell_size * 13 + 12 * this.margins;
      this.container_height =
        this.cell_size * 5 + 4 * this.margins + 50 + 50 + 20; // 50 50 20 su msg i inp field i donja margina

      this.keyboard = [
        [
          {
            id: "keyb_1",
            value: "1",
            ico: "1",
            tag: "cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 50,
          },
          {
            id: "keyb_2",
            value: "2",
            ico: "2",
            tag: "cell",
            left: 0,
            right: 2,
            top: 1,
            down: 1,
            x: 5 + 50,
            width: 50,
          },
          {
            id: "keyb_3",
            value: "3",
            ico: "3",
            tag: "cell",
            left: 1,
            right: 3,
            top: 2,
            down: 2,
            x: 10 + 100,
            width: 50,
          },
          {
            id: "keyb_4",
            value: "4",
            ico: "4",
            tag: "cell",
            left: 2,
            right: 4,
            top: 3,
            down: 3,
            x: 15 + 150,
            width: 50,
          },
          {
            id: "keyb_5",
            value: "5",
            ico: "5",
            tag: "cell",
            left: 3,
            right: 5,
            top: 4,
            down: 4,
            x: 20 + 200,
            width: 50,
          },
          {
            id: "keyb_6",
            value: "6",
            ico: "6",
            tag: "cell",
            left: 4,
            right: 6,
            top: 5,
            down: 5,
            x: 25 + 250,
            width: 50,
          },
          {
            id: "keyb_7",
            value: "7",
            ico: "7",
            tag: "cell",
            left: 5,
            right: 7,
            top: 6,
            down: 6,
            x: 30 + 300,
            width: 50,
          },
          {
            id: "keyb_8",
            value: "8",
            ico: "8",
            tag: "cell",
            left: 6,
            right: 8,
            top: 7,
            down: 7,
            x: 35 + 350,
            width: 50,
          },
          {
            id: "keyb_9",
            value: "9",
            ico: "9",
            tag: "cell",
            left: 7,
            right: 9,
            top: 8,
            down: 8,
            x: 40 + 400,
            width: 50,
          },
          {
            id: "keyb_10",
            value: "0",
            ico: "0",
            tag: "cell",
            left: 8,
            right: 10,
            top: 9,
            down: 9,
            x: 45 + 450,
            width: 50,
          },
          {
            id: "keyb_11",
            value: "&",
            ico: "&",
            tag: "cell",
            left: 9,
            right: 11,
            top: 10,
            down: 10,
            x: 50 + 500,
            width: 50,
          },
          {
            id: "keyb_back",
            value: "",
            ico: '<i class="bx bx-arrow-back"></i>',
            tag: "double",
            left: 10,
            right: 11,
            top: 11,
            down: 11,
            x: 55 + 550,
            width: 50 + 5 + 50,
          },
        ],
        [
          {
            id: "keyb_14",
            value: "a",
            ico: "a",
            tag: "cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 50,
          },
          {
            id: "keyb_15",
            value: "b",
            ico: "b",
            tag: "cell",
            left: 0,
            right: 2,
            top: 1,
            down: 1,
            x: 5 + 50,
            width: 50,
          },
          {
            id: "keyb_16",
            value: "c",
            ico: "c",
            tag: "cell",
            left: 1,
            right: 3,
            top: 2,
            down: 2,
            x: 10 + 100,
            width: 50,
          },
          {
            id: "keyb_17",
            value: "č",
            ico: "č",
            tag: "cell",
            left: 2,
            right: 4,
            top: 3,
            down: 3,
            x: 15 + 150,
            width: 50,
          },
          {
            id: "keyb_18",
            value: "ć",
            ico: "ć",
            tag: "cell",
            left: 3,
            right: 5,
            top: 4,
            down: 4,
            x: 20 + 200,
            width: 50,
          },
          {
            id: "keyb_19",
            value: "d",
            ico: "d",
            tag: "cell",
            left: 4,
            right: 6,
            top: 5,
            down: 5,
            x: 25 + 250,
            width: 50,
          },
          {
            id: "keyb_20",
            value: "đ",
            ico: "đ",
            tag: "cell",
            left: 5,
            right: 7,
            top: 6,
            down: 6,
            x: 30 + 300,
            width: 50,
          },
          {
            id: "keyb_21",
            value: "e",
            ico: "e",
            tag: "cell",
            left: 6,
            right: 8,
            top: 7,
            down: 7,
            x: 35 + 350,
            width: 50,
          },
          {
            id: "keyb_22",
            value: "f",
            ico: "f",
            tag: "cell",
            left: 7,
            right: 9,
            top: 8,
            down: 8,
            x: 40 + 400,
            width: 50,
          },
          {
            id: "keyb_23",
            value: "g",
            ico: "g",
            tag: "cell",
            left: 8,
            right: 10,
            top: 9,
            down: 9,
            x: 45 + 450,
            width: 50,
          },
          {
            id: "keyb_24",
            value: "h",
            ico: "h",
            tag: "cell",
            left: 9,
            right: 11,
            top: 10,
            down: 10,
            x: 50 + 500,
            width: 50,
          },
          {
            id: "keyb_25",
            value: "i",
            ico: "i",
            tag: "cell",
            left: 10,
            right: 12,
            top: 11,
            down: 11,
            x: 55 + 550,
            width: 50,
          },
          {
            id: "keyb_26",
            value: "-",
            ico: "-",
            tag: "cell",
            left: 11,
            right: 12,
            top: 11,
            down: 12,
            x: 60 + 600,
            width: 50,
          },
        ],
        [
          {
            id: "keyb_27",
            value: "j",
            ico: "j",
            tag: "cell",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 50,
          },
          {
            id: "keyb_28",
            value: "k",
            ico: "k",
            tag: "cell",
            left: 0,
            right: 2,
            top: 1,
            down: 0,
            x: 5 + 50,
            width: 50,
          },
          {
            id: "keyb_29",
            value: "l",
            ico: "l",
            tag: "cell",
            left: 1,
            right: 3,
            top: 2,
            down: 1,
            x: 10 + 100,
            width: 50,
          },
          {
            id: "keyb_30",
            value: "m",
            ico: "m",
            tag: "cell",
            left: 2,
            right: 4,
            top: 3,
            down: 2,
            x: 15 + 150,
            width: 50,
          },
          {
            id: "keyb_31",
            value: "n",
            ico: "n",
            tag: "cell",
            left: 3,
            right: 5,
            top: 4,
            down: 3,
            x: 20 + 200,
            width: 50,
          },
          {
            id: "keyb_32",
            value: "o",
            ico: "o",
            tag: "cell",
            left: 4,
            right: 6,
            top: 5,
            down: 4,
            x: 25 + 250,
            width: 50,
          },
          {
            id: "keyb_33",
            value: "p",
            ico: "p",
            tag: "cell",
            left: 5,
            right: 7,
            top: 6,
            down: 5,
            x: 30 + 300,
            width: 50,
          },
          {
            id: "keyb_34",
            value: "q",
            ico: "q",
            tag: "cell",
            left: 6,
            right: 8,
            top: 7,
            down: 6,
            x: 35 + 350,
            width: 50,
          },
          {
            id: "keyb_35",
            value: "r",
            ico: "r",
            tag: "cell",
            left: 7,
            right: 9,
            top: 8,
            down: 7,
            x: 40 + 400,
            width: 50,
          },
          {
            id: "keyb_36",
            value: "s",
            ico: "s",
            tag: "cell",
            left: 8,
            right: 10,
            top: 9,
            down: 8,
            x: 45 + 450,
            width: 50,
          },
          {
            id: "keyb_37",
            value: "š",
            ico: "š",
            tag: "cell",
            left: 9,
            right: 11,
            top: 10,
            down: 9,
            x: 50 + 500,
            width: 50,
          },
          {
            id: "keyb_38",
            value: "t",
            ico: "t",
            tag: "cell",
            left: 10,
            right: 12,
            top: 11,
            down: 10,
            x: 55 + 550,
            width: 50,
          },
          {
            id: "keyb_39",
            value: "_",
            ico: "_",
            tag: "cell",
            left: 11,
            right: 12,
            top: 12,
            down: 11,
            x: 60 + 600,
            width: 50,
          },
        ],
        [
          {
            id: "keyb_shift",
            value: "",
            ico: "SHIFT",
            tag: "double",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 50 + 5 + 50,
          },
          {
            id: "keyb_42",
            value: "u",
            ico: "u",
            tag: "cell",
            left: 0,
            right: 2,
            top: 2,
            down: 0,
            x: 10 + 100,
            width: 50,
          },
          {
            id: "keyb_43",
            value: "v",
            ico: "v",
            tag: "cell",
            left: 1,
            right: 3,
            top: 3,
            down: 1,
            x: 15 + 150,
            width: 50,
          },
          {
            id: "keyb_44",
            value: "w",
            ico: "w",
            tag: "cell",
            left: 2,
            right: 4,
            top: 4,
            down: 1,
            x: 20 + 200,
            width: 50,
          },
          {
            id: "keyb_45",
            value: "x",
            ico: "x",
            tag: "cell",
            left: 3,
            right: 5,
            top: 5,
            down: 1,
            x: 25 + 250,
            width: 50,
          },
          {
            id: "keyb_46",
            value: "y",
            ico: "y",
            tag: "cell",
            left: 4,
            right: 6,
            top: 6,
            down: 1,
            x: 30 + 300,
            width: 50,
          },
          {
            id: "keyb_47",
            value: "z",
            ico: "z",
            tag: "cell",
            left: 5,
            right: 7,
            top: 7,
            down: 1,
            x: 35 + 350,
            width: 50,
          },
          {
            id: "keyb_48",
            value: "ž",
            ico: "ž",
            tag: "cell",
            left: 6,
            right: 8,
            top: 8,
            down: 1,
            x: 40 + 400,
            width: 50,
          },
          {
            id: "keyb_49",
            value: "@",
            ico: "@",
            tag: "cell",
            left: 7,
            right: 9,
            top: 9,
            down: 2,
            x: 45 + 450,
            width: 50,
          },
          {
            id: "keyb_50",
            value: "(",
            ico: "(",
            tag: "cell",
            left: 8,
            right: 10,
            top: 10,
            down: 3,
            x: 50 + 500,
            width: 50,
          },
          {
            id: "keyb_51",
            value: ")",
            ico: ")",
            tag: "cell",
            left: 9,
            right: 11,
            top: 11,
            down: 3,
            x: 55 + 550,
            width: 50,
          },
          {
            id: "keyb_52",
            value: ",",
            ico: ",",
            tag: "cell",
            left: 10,
            right: 11,
            top: 12,
            down: 3,
            x: 60 + 600,
            width: 50,
          },
        ],
        [
          {
            id: "keyb_cancel",
            value: "",
            ico: "Poništi",
            tag: "triple",
            left: 0,
            right: 1,
            top: 0,
            down: 0,
            x: 0,
            width: 10 + 150,
          },
          {
            id: "keyb_space",
            value: " ",
            ico: "Razmak",
            tag: "space",
            left: 0,
            right: 2,
            top: 4,
            down: 1,
            x: 15 + 150,
            width: 25 + 300,
          },
          {
            id: "keyb_62",
            value: ".",
            ico: ".",
            tag: "cell",
            left: 1,
            right: 3,
            top: 8,
            down: 2,
            x: 45 + 450,
            width: 50,
          },
          {
            id: "keyb_ok",
            value: "",
            ico: "OK",
            tag: "triple",
            left: 2,
            right: 3,
            top: 10,
            down: 3,
            x: 50 + 500,
            width: 10 + 150,
          },
        ],
      ];

      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          this.keyboard[i][j].y = i * this.cell_size + i * this.margins;
        }
      }
    },

    set_cell_css: function () {
      $(keyboard_container_id).css({
        position: "relative",
        width: this.container_width + "px",
        left: (1280 - this.container_width) / 2 + "px",
        height: this.container_height + "px",
        top: (720 - (20 + this.container_height)) / 2 + "px",
      });

      $(".keyboard").css({
        position: "relative",
        "font-size": "30px",
        "box-sizing": "border-box",
      });

      $(".cell").css({
        width: this.cell_size + "px",
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      $(".space").css({
        width: 6 * this.cell_size + 5 * this.margins,
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      $(".double").css({
        width: 2 * this.cell_size + 1 * this.margins,
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      $(".triple").css({
        width: 3 * this.cell_size + 2 * this.margins,
        height: this.cell_size + "px",
        "line-height": this.cell_size + "px",
        "text-align": "center",
        padding: "0 !important",
        margin: "0 !important",
        position: "absolute",
      });

      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          $("#" + this.keyboard[i][j].id).css({
            top: this.keyboard[i][j].y,
            left: this.keyboard[i][j].x,
          });
        }
      }

      $(key_foc_id).css({
        width: this.cell_size + "px",
        height: this.cell_size + "px",
        top: "0px",
        left: "0px",
      });
    },

    animate_selection: function (rowoff, coloff) {
      var that = this;

      function valid_pos(row) {
        if (0 <= row && row < that.keyboard.length) return true;
        else return false;
      }

      if (valid_pos(this.row + rowoff)) {
        var last_val_col = that.col;
        var last_val_row = that.row;

        var elem = that.keyboard[that.row][that.col];
        if (rowoff == 1) {
          that.col = elem.down;
        } else if (rowoff == -1) {
          that.col = elem.top;
        }

        that.row += rowoff;

        if (coloff == 1) {
          that.col = elem.right;
        } else if (coloff == -1) {
          that.col = elem.left;
        }

        if (last_val_col != that.col || last_val_row != that.row) {
          elem = that.keyboard[that.row][that.col];
          $(key_foc_id).animate(
            {
              left: elem.x + "px",
              top: elem.y + "px",
              width: elem.width + "px",
            },
            { duration: animation_duration }
          );
        }
      }
    },

    to_upper_case: function () {
      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          if (this.keyboard[i][j].ico.length == 1)
            $("#" + this.keyboard[i][j].id).html(
              this.keyboard[i][j].value.toUpperCase()
            );
        }
      }
    },

    to_lower_case: function () {
      for (var i = 0; i < this.keyboard.length; i++) {
        for (var j = 0; j < this.keyboard[i].length; j++) {
          if (this.keyboard[i][j].ico.length == 1)
            $("#" + this.keyboard[i][j].id).html(
              this.keyboard[i][j].value.toLowerCase()
            );
        }
      }
    },

    action_button: function () {
      var key = this.keyboard[this.row][this.col];
      if (key.id == "keyb_back")
        this.current_text = this.current_text.slice(0, -1);
      else if (key.id == "keyb_ok") {
        this.submit();
        return;
      } else if (key.id == "keyb_shift") {
        this.shift_on = !this.shift_on;
        if (this.shift_on) this.to_upper_case();
        else this.to_lower_case();
      } else if (key.id == "keyb_cancel") {
        this.on_close();
        return;
      } else {
        if (this.shift_on) this.current_text += key.value.toUpperCase();
        else this.current_text += key.value;
      }
      $(input_field_id).html(this.current_text);
    },

    remote_num_press: function (num) {
      this.current_text += num.toString();
      $(input_field_id).html(this.current_text);
    },

    key_action_down: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation(); // zbog bubblinga
      switch (e.keyCode) {
        case 48:
        case 49:
        case 50:
        case 51:
        case 52:
        case 53:
        case 54:
        case 55:
        case 56:
        case 57:
          this.remote_num_press(e.keyCode - 48);
          break;
        case KEY_RIGHT:
          this.animate_selection(0, 1);
          break;
        case KEY_LEFT:
          this.animate_selection(0, -1);
          break;
        case KEY_DOWN:
          this.animate_selection(1, 0);
          break;
        case KEY_UP:
          this.animate_selection(-1, 0);
          break;
        case KEY_OK:
          this.action_button();
          break;
        case KEY_BACK:
          this.on_close();
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
          this.on_close();
          this.bus.trigger("ExpandMenu");
          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },

    events: {
      keydown: "key_action_down",
    },

    submit: function () {
      this.func(this.current_text);
    },

    render: function () {
      // html
      var template = _.template(Template);
      var html = template({ keyboard: this.keyboard });
      this.$el.html(html);
      // cell css
      this.set_cell_css();
      $(input_field_id).html(this.current_text);
      // focus
      this.exfocus = "#" + document.activeElement.id;
      this.$el.focus();
    },

    on_close: function () {
      $(this.exfocus).focus(); // returns focus
      this.remove();
    },

    show_message: function (message) {
      $(message_field_id).html(message);
    },

    clear_input: function () {
      $(input_field_id).html("");
      this.current_text = "";
    },
  });

  return init_keyboard_view;
});

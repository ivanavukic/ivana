define([
  "jquery",
  "underscore",
  "backbone",
  "collections/startCollections",
  "models/screen_focus",
  "views/listRowsView",
  "text!page/start.html"
], function ($, _, Backbone, StartCollections, screen_focus, ListRowsView, template) {
  var st_complete = "";
  var startColl = {};
  var listRowsView = {};

  var StartView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.y = 0;
      this.roots = [];

      this.config = {
        first: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "player",
          image: "stills"
        },
        other: {
          size: "md",
          left_shift: -21.25,
          progress: 0,
          position_back: "",
          action_type: "player",
          image: "stills"
        }
      };

      this.bus = options.bus;
      this.bus.on("StartClose", this.onClose, this);

      screen_focus.setData({ view: "start", focus: "control_0" });
    },

    create: function () {
      loaderShow(true);
      startColl = new StartCollections();

      var self = this;
      startColl.init().then(function (res) {
        self.collection = res;
        console.log(self.collection);
        self.render();
        loaderShow(false);
        console.log(res);
      });
    },

    render: function () {
      listRowsView = new ListRowsView({
        el: "#other_container",
        template: template,
        collObj: startColl,
        collect: this.collection,
        config: this.config,
        roots: this.roots,
        screen: this.screen,
        bus: this.bus
      });

      $("#other").append(listRowsView.render().el);

      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return StartView;
});

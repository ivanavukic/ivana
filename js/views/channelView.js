define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "velocity",
  "collections/channelCollections",
  "models/screen_focus",
  "models/channel_focus",
  "models/epg_focus",
  "views/channEpgView",
  "views/sliderView",
  "text!page/channel.html"
], function (
  $,
  _,
  Backbone,
  tv,
  velocity,
  ChannelCollections,
  screen_focus,
  channel_focus,
  epg_focus,
  ChannEpgView,
  SliderView,
  template
) {
  var st_complete = 0;
  var prevKey = 0;
  var longPress = 0;
  var longPressCnt = 0;
  var speed_change = 0;
  var duration = 300;
  var subOrNot = true;
  var fokusKanala = true;
  var test = "";

  var chan_back = 5;
  var chan_forw = 6;

  var hide_screen;

  var ChannelView = Backbone.View.extend({
    initialize: function (options) {
      this.x = 0;
      this.y = 0;

      // this.y_channel = ["0", "111", "222", "333", "444", "555"];
      this.y_channel = [
        "0",
        "15.416666666666668",
        "30.833333333333336",
        "46.25",
        "61.66666666666667",
        "77.08333333333334"
      ];
      this.y_focus_pos = 0;
      this.count = 0;
      this.delayFlag = true;
      prevKey = 0;
      longPress = 0;
      longPressCnt = 0;
      speed_change = 0;
      self = this;
      this.y_shift = 111;
      this.bus = options.bus;
      this.bus.on("CategoryChannel", this.onCategoryChannel, this);
      this.bus.on("SetFokusMenu", this.onSetFokusMenu, this);
      this.bus.on("SetPositionchannle", this.onSetPositionchannle, this);
      this.bus.on(
        "QuickChanUp",
        function (e) {
          console.log(e);
          if (e.type == "keydown" || e.type == "slide") {
            console.log("Pokusavam da menjam kanal", channel_focus.get("mediaId"));
            // if (fokusKanala) {
            if (st_complete !== 1) {
              if (channel_focus.get("id") < this.count) {
                // Ubravanje kanala (zaustavljam kada dodje do zadnjih 3)
                if (this.count - 4 > channel_focus.get("id") && speed_change == 3)
                  channel_focus.set({ id: channel_focus.get("id") + 2 }, { validate: true });
                else channel_focus.set({ id: channel_focus.get("id") + 1 }, { validate: true });

                st_complete = 1;
                if (between(channel_focus.get("id"), 3, this.count - 3) && speed_change < 3) {
                  this.Down_ChanShift();
                } else if (between(channel_focus.get("id"), 3, this.count - 3) && speed_change == 3) {
                  this.fast_update_channel(channel_focus.get("id") - 2);
                } else {
                  this.handleKey(e.keyCode, 1);
                  this.FocusShift({ st_animate: true });
                }
              }
            }
            console.log(channel_focus.get("id"));
            // } else {
            //   subOrNot = false;
            //   $(".chanSubSpan1").removeClass("orange");
            //   $(".chanSubSpan2").addClass("orange");
            // }
            //playChan(10000000);

            $("#slider_chanPic").attr("src", this.channel_data[channel_focus.get("id")].slika);
            $("#trc_name_content").css({ opacity: 0 });
            $("#custom-handle").css({ opacity: 0 });
            // $("#slider").css({ opacity: 0 });
            // $("#player_controls").css({ opacity: 0 });
            // $("#player_buttons").css({ opacity: 0 });
            $("#trc_slider_epg").css({ opacity: 0, height: 0 });
          } else if (e.type == "keyup") {
            $("#slider_chanPic").attr("src", this.channel_data[channel_focus.get("id")].slika);
            var x = function izvrsi(callback) {
              if (self.intervalId) {
                clearInterval(self.intervalId);
              }
              self.intervalId = setTimeout(function () {
                callback();
              }, 300);
            };

            x(function () {
              // if (fokusKanala) {
              console.log("Pokusavam da pustim kanal", channel_focus.get("mediaId"));
              self.set_value_channel_focus();
              epg_focus.set({ st_live: true });
              if (!channel_focus.get("subscribed")) {
                console.log("Niste pretplaceni");
                $("#chanPin").hide();
                $("#chanSub").show();
                // fokusKanala = false;
              } else if (channel_focus.get("adult")) {
                console.log("Adult");
                $("#chanSub").hide();
                $("#chanPin").show();
              } else {
                $("#chanSub").hide();
                $("#chanPin").hide();
                localStorage.setItem("kanalFokus", JSON.stringify(channel_focus.attributes));
                player.playV3(channel_focus.get("ChanKey"), channel_focus.get("mediaId"), 0, 0);
                //player.joinLive(channel_focus.get("mediaId"));
              }
              // } else {
              //   if (!subOrNot) {
              //     fokusKanala = true;
              //     $("#chanSub").hide();
              //   }
              // }

              //console.log("Live " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
              dbgLog("Live " + "ChanKey:" + channel_focus.get("ChanKey"));
              setTimeout(function () {
                self.bus.trigger("ShowPlayer");
              }, 1000);
              self.intervalId = undefined;
            });
          }
        },
        this
      );
      this.bus.on(
        "QuickChanDown",
        function (e) {
          console.log(e);
          if (e.type == "keydown" || e.type == "slide") {
            // if (fokusKanala) {
            if (st_complete !== 1) {
              if (channel_focus.get("id") > 0) {
                // Ubravanje kanala

                // Ubravanje kanala (zaustavljam kada dodje do prvih 4)
                if (channel_focus.get("id") > 4 && speed_change == 3)
                  channel_focus.set({ id: channel_focus.get("id") - 2 }, { validate: true });
                else channel_focus.set({ id: channel_focus.get("id") - 1 }, { validate: true });

                //channel_focus.set({ id: channel_focus.get("id") - 1 }, { validate: true });

                st_complete = 1;
                if (between(channel_focus.get("id"), 2, this.count - 4) && speed_change < 3) {
                  console.log("PROBA GORE");
                  this.Up_ChanShift();
                } else if (between(channel_focus.get("id"), 2, this.count - 4) && speed_change == 3)
                  this.fast_update_channel(channel_focus.get("id") - 2);
                else {
                  this.handleKey(e.keyCode, 1);
                  this.FocusShift({ st_animate: true });
                }
              }
            }
            console.log(channel_focus.get("id"));
            // } else {
            //   subOrNot = true;
            //   $(".chanSubSpan2").removeClass("orange");
            //   $(".chanSubSpan1").addClass("orange");
            // }
            //playChan(10000000);
            $("#slider_chanPic").attr("src", this.channel_data[channel_focus.get("id")].slika);
            $("#trc_name_content").css({ opacity: 0 });
            $("#custom-handle").css({ opacity: 0 });
            // $("#slider").css({ opacity: 0 });
            // $("#player_controls").css({ opacity: 0 });
            // $("#player_buttons").css({ opacity: 0 });
            $("#trc_slider_epg").css({ opacity: 0, height: 0 });
          } else if (e.type == "keyup") {
            $("#slider_chanPic").attr("src", this.channel_data[channel_focus.get("id")].slika);
            var x = function izvrsi(callback) {
              if (self.intervalId) {
                clearInterval(self.intervalId);
              }
              self.intervalId = setTimeout(function () {
                callback();
              }, 300);
            };

            x(function () {
              // if (fokusKanala) {
              console.log("Pokusavam da pustim kanal", channel_focus.get("mediaId"));
              self.set_value_channel_focus();
              epg_focus.set({ st_live: true });
              if (!channel_focus.get("subscribed")) {
                console.log("Niste pretplaceni");
                $("#chanPin").hide();
                $("#chanSub").show();
                // fokusKanala = false;
              } else if (channel_focus.get("adult")) {
                console.log("Adult");
                $("#chanSub").hide();
                $("#chanPin").show();
              } else {
                $("#chanSub").hide();
                $("#chanPin").hide();
                localStorage.setItem("kanalFokus", JSON.stringify(channel_focus.attributes));
                player.playV3(channel_focus.get("ChanKey"), channel_focus.get("mediaId"), 0, 0);
                //player.joinLive(channel_focus.get("mediaId"));
              }
              // } else {
              //   if (!subOrNot) {
              //     fokusKanala = true;
              //     $("#chanSub").hide();
              //   }
              // }

              //console.log("Live " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
              dbgLog("Live " + "ChanKey:" + channel_focus.get("ChanKey"));
              setTimeout(function () {
                self.bus.trigger("ShowPlayer");
              }, 1000);

              self.intervalId = undefined;
            });
          }
        },
        this
      );
    },

    create: function () {
      screen_focus.setData({ view: "channel", focus: "channel" });

      var self = this;
      tv.getKanale("", "")
        .then(function (res) {
          ChannelCollections.reset();
          ChannelCollections.push(res);

          self.channel_data = ChannelCollections.toJSON();
          console.log(self.channel_data);
          //postavljam defolten vrijednosti za kanal prije poziva (prvi sa liste)
          //this.set_value_channel_focus();
          // console.log(self.channel_data);
        })
        .then(function (res) {
          self.render();

          setTimeout(function () {
            epg_focus.set({ st_live: true });
            // player.joinLive(channel_focus.get("mediaId"));

            // console.log("Pokusavam da pustim kanal", channel_focus.get("mediaId"));
            if (!channel_focus.get("subscribed")) {
              console.log("Niste pretplaceni");
              $("#chanPin").hide();
              $("#chanSub").show();
              // fokusKanala = false;
            } else if (channel_focus.get("adult")) {
              console.log("Adult");
              $("#chanSub").hide();
              $("#chanPin").show();
            } else {
              $("#chanSub").hide();
              $("#chanPin").hide();
              player.playV3(channel_focus.get("ChanKey"), channel_focus.get("mediaId"), 0, 0);
              //player.joinLive(channel_focus.get("mediaId"));
            }
            return false;
          }, 100);

          //self.TimeOutHideScreen();
          $("#chanSub").hide();
          $("#chanPin").hide();
        });
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown",
      "focus #channel": "TimeOutHideScreen",
      "blur #channel": "ClearTimeOutHideScreen"
    },

    ClearTimeOutHideScreen: function () {
      clearTimeout(hide_screen);
    },
    TimeOutHideScreen: function () {
      var self = this;
      clearTimeout(hide_screen);

      //brisi **************************************//
      //channel_focus.set({ id: 3 });
      //this.set_value_channel_focus();
      //*******************************************//

      hide_screen = setTimeout(function () {
        self.bus.trigger("HideScreen", { view: "channel", vr_media: "live", control_focus: "channel" });
      }, 5000);
    },

    onSetPositionchannle: function (d) {
      console.log(d);
      this.update_channel_info(d.id);
      $("#channel").focus();
    },

    keyActionUp: function (e) {
      this.handleKey(e.keyCode, 1);
      this.TimeOutHideScreen();
    },

    keyActionDown: function (e) {
      e.stopPropagation();

      default_key(e.keyCode);
      //dbgLog(e.keyCode);
      if (e.keyCode == 768) {
        return false;
      } // NEKI EVENT-i

      console.log("xxxxxxxxxxxxxxxxxxxx");
      this.ClearTimeOutHideScreen();

      switch (e.keyCode) {
        case KEY_DOWN:
          // if (fokusKanala) {
          if (st_complete !== 1) {
            if (channel_focus.get("id") < this.count) {
              // Ubravanje kanala (zaustavljam kada dodje do zadnjih 3)
              if (this.count - 4 > channel_focus.get("id") && speed_change == 3)
                channel_focus.set({ id: channel_focus.get("id") + 2 }, { validate: true });
              else channel_focus.set({ id: channel_focus.get("id") + 1 }, { validate: true });

              st_complete = 1;
              if (between(channel_focus.get("id"), 3, this.count - 3) && speed_change < 3) {
                this.Down_ChanShift();
              } else if (between(channel_focus.get("id"), 3, this.count - 3) && speed_change == 3) {
                this.fast_update_channel(channel_focus.get("id") - 2);
              } else {
                this.handleKey(e.keyCode, 1);
                this.FocusShift({ st_animate: true });
              }
            }
          }
          console.log(channel_focus.get("id"));
          // } else {
          //   subOrNot = false;
          //   $(".chanSubSpan1").removeClass("orange");
          //   $(".chanSubSpan2").addClass("orange");
          // }

          break;
        case KEY_UP:
          // if (fokusKanala) {
          if (st_complete !== 1) {
            if (channel_focus.get("id") > 0) {
              // Ubravanje kanala

              // Ubravanje kanala (zaustavljam kada dodje do prvih 4)
              if (channel_focus.get("id") > 4 && speed_change == 3)
                channel_focus.set({ id: channel_focus.get("id") - 2 }, { validate: true });
              else channel_focus.set({ id: channel_focus.get("id") - 1 }, { validate: true });

              //channel_focus.set({ id: channel_focus.get("id") - 1 }, { validate: true });

              st_complete = 1;
              if (between(channel_focus.get("id"), 2, this.count - 4) && speed_change < 3) {
                console.log("PROBA GORE");
                this.Up_ChanShift();
              } else if (between(channel_focus.get("id"), 2, this.count - 4) && speed_change == 3)
                this.fast_update_channel(channel_focus.get("id") - 2);
              else {
                this.handleKey(e.keyCode, 1);
                this.FocusShift({ st_animate: true });
              }
            }
          }
          console.log(channel_focus.get("id"));
          // } else {
          //   subOrNot = true;
          //   $(".chanSubSpan2").removeClass("orange");
          //   $(".chanSubSpan1").addClass("orange");
          // }

          break;
        case KEY_HOME:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
          this.bus.trigger("HideScreen", { view: "channel", vr_media: "live", control_focus: "channel" });
          return false;
          break;
        case KEY_BACK:
          this.bus.trigger("HideScreen", { view: "channel", vr_media: "live", control_focus: "channel" });
          return false;
          break;
        case KEY_LEFT:
          // if (fokusKanala) {
          this.bus.trigger("ExpandCategoryMenu");
          // }

          return false;
          break;
        case KEY_RIGHT:
          // if (fokusKanala) {
          if (this.channel_data.length > 0) {
            var channel_data = this.channel_data[channel_focus.get("id")];

            var channEpgView = new ChannEpgView({ el: "#epg_container", channel_data: channel_data, bus: this.bus });
            channEpgView.create();
          }
          // }

          return false;
          break;
        case KEY_ENTER:
        case KEY_OK:
          //playChan(10000000);
          // if (fokusKanala) {
          console.log("Pokusavam da pustim kanal", channel_focus.get("mediaId"));
          this.set_value_channel_focus();
          epg_focus.set({ st_live: true });
          if (!channel_focus.get("subscribed")) {
            console.log("Niste pretplaceni");
            $("#chanPin").hide();
            $("#chanSub").show();
            // fokusKanala = false;
          } else if (channel_focus.get("adult")) {
            console.log("Adult");
            $("#chanSub").hide();
            $("#chanPin").show();
          } else {
            $("#chanSub").hide();
            $("#chanPin").hide();
            localStorage.setItem("kanalFokus", JSON.stringify(channel_focus.attributes));
            player.playV3(channel_focus.get("ChanKey"), channel_focus.get("mediaId"), 0, 0);
            //player.joinLive(channel_focus.get("mediaId"));
          }
          // } else {
          //   if (!subOrNot) {
          //     fokusKanala = true;
          //     $("#chanSub").hide();
          //   }
          // }

          //console.log("Live " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position);
          dbgLog("Live " + "ChanKey:" + channel_focus.get("ChanKey"));

          /************************************************************************/

          return false;
          break;
      }

      //console.log("id: ", channel_focus.get("id"), "count: ", this.count, "nema: ", channel_focus.get("ime"), "STATUS:", st_complete);

      this.handleKey(e.keyCode, 0);
      return false;
    },

    onSetFokusMenu: function () {
      this.TimeOutHideScreen();
      $("#epg-list").show();
      $("#category-list").show();
      $("#channel-list").show();
      $("#menucateg_container").show();
      $("#screen_view_container").removeClass("inactive").addClass("active");
      $("#channel").removeClass("remove_focus");
      $("#channel").focus();

      //this.bus.trigger("SliderClose");
    },

    FocusShift: function (date) {
      console.log("FOKUS SHIFT");
      var id_index = channel_focus.get("id");
      var top_poz;
      if (this.count < 5) top_poz = this.y_channel[id_index];
      else {
        if (id_index < 2) top_poz = this.y_channel[id_index];
        else if (id_index == this.count - 2) top_poz = this.y_channel[3];
        else if (id_index == this.count - 1) top_poz = this.y_channel[4];
        else if (id_index == this.count) top_poz = this.y_channel[5];
        else top_poz = this.y_channel[2];
      }

      dbgLog("id " + channel_focus.get("id"));
      //dbgLog("top_poz " + top_poz);

      if (date.st_animate == true) {
        console.log("ANIMIRAM");
        st_complete = 1;

        $("#channel").animate(
          { top: top_poz + "vh" },
          {
            duration: 150,
            complete: function () {
              st_complete = 0;
            }
          }
        );
      } else {
        $("#channel").css({ top: top_poz + "vh" });
        st_complete = 0;
      }
    },

    Down_ChanShift: function () {
      st_complete = 1;

      var dom = $("#channel-content").children(".channel-box").eq(0);
      dom.children(".channel-box .number").eq(0).text("");

      //console.log($("#channel-content").children(".channel-box").eq(3).offset().top);
      //console.log('channel_focus.get("id")', channel_focus.get("id"));

      //var poz_index = this.count >= channel_focus.get("id") + 4 ? channel_focus.get("id") + 4 : -1;
      var channel_item =
        this.count >= channel_focus.get("id") + 4 ? this.channel_data[channel_focus.get("id") + 4] : "";

      self = this;

      dom.animate(
        { height: "0px" },
        {
          duration: duration,
          complete: function () {
            dom.remove();
            //if (channel_item != "")
            self.insertDOM(channel_item, "forward");
          }
        }
      );
    },

    Up_ChanShift: function () {
      st_complete = 1;

      //var channel_item = this.channel_data[channel_focus.get("id")];
      //var poz_index = channel_focus.get("id") - 2 >= 0 ? channel_focus.get("id") - 2 : -1;
      var channel_item = channel_focus.get("id") - 2 >= 0 ? this.channel_data[channel_focus.get("id") - 2] : "";

      this.insertDOM(channel_item, "backward");
      var dom = $("#channel-content").children(".channel-box").eq(0);
      dom.children(".channel-box .number").eq(0).text(channel_item.ChanKey);

      dom.animate(
        { height: "15.416666666666668vh" },
        {
          duration: duration,
          complete: function () {
            $("#channel-content div").last().remove();
            st_complete = 0;
          }
        }
      );
    },

    onCategoryChannel: function (data) {
      var self = this;
      console.log(data);
      if (data.code == 1001) {
        tv.getKanale("", "")
          .then(function (res) {
            console.log(res);
            self.channel_data = res;
          })
          .then(function () {
            self.updateCategoryChannelInfo();
          });
        //console.log('IF SVE *********',data.code);
        //this.channel_data = tv.getKanale("", "");
      } else if (data.code == 1002) {
        tv.getKanale("", 1)
          .then(function (res) {
            console.log(res);
            self.channel_data = res;
          })
          .then(function () {
            self.updateCategoryChannelInfo();
          });
      } else {
        tv.getKanale(data.code, "")
          .then(function (res) {
            console.log(res);
            self.channel_data = res;
          })
          .then(function () {
            self.updateCategoryChannelInfo();
          });
      }
    },

    updateCategoryChannelInfo: function () {
      this.count = this.channel_data.length - 1;
      channel_focus.set({ max_number: this.count });
      channel_focus.set({ id: 0 });
      this.set_value_channel_focus();
      this.update_channel_info(channel_focus.get("id"));
    },

    set_value_channel_focus: function () {
      //console.log(this.channel_data[channel_focus.get("id")])
      var data = this.channel_data[channel_focus.get("id")];
      channel_focus.setData(data);
    },

    insertDOM: function (date, status) {
      console.log(date);
      var doc = document.getElementById("channel-content");
      var div = document.createElement("div");
      div.className = "channel-box";

      var span = document.createElement("span");
      span.className = "number";
      span.textContent = date.ChanKey;

      var img = document.createElement("img");

      // if (date.subscribed) {
      //   img.src = date.slika;
      // } else img.src = "https://m.media-amazon.com/images/I/51wCFLE371L._SS500_.jpg";
      img.src = date.slika;
      div.appendChild(span);
      div.appendChild(img);

      if (status == "forward") {
        doc.appendChild(div);
        st_complete = 0;
      } else {
        div.style.height = "0px";
        doc.insertBefore(div, doc.firstChild);
      }
    },

    handleKey: function (code, isKeyUp) {
      //console.log("code", code);
      //console.log("isKeyUp", isKeyUp);
      //console.log("prevKey", prevKey);

      if (isKeyUp) {
        if (prevKey == code) {
          prevKey = 0;
          longPress = 0;
          longPressCnt = 0;
          speed_change = 0;
          duration = 300;
          //console.log("Reset **** Reset ");
        }
        return;
      }

      if (prevKey == code) {
        longPress = 1;
        longPressCnt += 1;
      }
      prevKey = code;

      // RAZRADA
      if (longPress == 1) {
        if (longPressCnt == 3) {
          speed_change = 1;
          duration = 100;
        } else if (longPressCnt == 3) {
          speed_change = 2;
          duration = 50;
        } else if (longPressCnt == 5) {
          speed_change = 3;
          duration = 1;
        }

        //dbgLog("KEYDOWN NOW: " + code + " LONG PRESS: " + longPressCnt + '   speed-' + speed_change + '  duration-' + duration);
        //console.log("LONG PRESS FOR KEY: " + code + " LONG PRESS COUNT: " + longPressCnt + '   x-' + speed_change);
      } else {
        if (longPress == 1) {
          speed_change = 1;
          duration = 100;
        } else if (longPress == 3) {
          speed_change = 2;
          duration = 50;
        } else if (longPress == 5) {
          speed_change = 3;
          duration = 1;
        }
        //dbgLog("KEYDOWN NOW: " + code + " LONG PRESS: " + longPress + ' x');
        //console.log("KEYDOWN NOW: " + code + " LONG PRESS: " + longPress);
      }
    },

    render: function () {
      var templ = _.template(template);
      var html = templ();
      this.$el.html(html);
      this.$el.find("#channel").focus();

      //console.log(this.channel_data);

      this.x = channel_focus.get("id");
      this.count = this.channel_data.length - 1;
      channel_focus.set({ max_number: this.count });

      //channel_focus.set({ id: 100 });
      this.set_value_channel_focus();
      this.update_channel_info(channel_focus.get("id"));

      //console.log(this.count);

      return this;
    },

    update_channel_info: function (id_c) {
      var i;
      var id_index;

      id_index = id_c;

      //Postavljam na sredinu ekrana id uzimam dva kanala ispred da bi postavio

      if (id_index < 2 || this.count < 5) id_index = 0;
      else if (id_index > this.count - 5) id_index = this.count - 5;
      else id_index = id_index - 2;

      //console.log(this.count);
      //console.log(id_index);
      //s
      for (var i = 0; i <= 6; i++) {
        if (id_index <= this.count) {
          $("#channel-content").children(".channel-box").eq(i).css("display", "block");
          $("#channel-content")
            .children(".channel-box")
            .eq(i)
            .children("span.number")
            .text(this.channel_data[id_index].ChanKey);
          $("#channel-content")
            .children(".channel-box")
            .eq(i)
            .children("img")
            .attr("src", this.channel_data[id_index].slika);
        } else {
          $("#channel-content").children(".channel-box").eq(i).removeAttr("id_c");
          $("#channel-content").children(".channel-box").eq(i).css("display", "none");
        }
        id_index = id_index + 1;
      }

      this.FocusShift({ st_animate: false });
    },

    fast_update_channel: function (num) {
      //console.log("fast");
      //console.log(num);

      var top_poz = this.y_channel[2];
      $("#channel").css({ top: top_poz + "vh" });

      for (var i = 0; i < 6; i++) {
        $("#channel-content")
          .children(".channel-box")
          .eq(i)
          .children("span.number")
          .text(this.channel_data[num + i].ChanKey);
        $("#channel-content")
          .children(".channel-box")
          .eq(i)
          .children("img")
          .attr("src", this.channel_data[num + i].slika);
      }

      st_complete = 0;
    }
  });

  return ChannelView;
});

function between(x, min, max) {
  return x >= min && x <= max;
}

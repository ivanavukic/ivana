define([
  "jquery",
  "underscore",
  "backbone",
  "coreVods",
  "models/vod_focus",
  "models/vod_content_focus",
  "models/screen_focus",
  "collections/menuMovCollections",
  "collections/vodCollections",
  "views/listVodView",
  "text!page/vodcontent.html"
], function (
  $,
  _,
  Backbone,
  vod,
  vod_focus,
  vod_content_focus,
  screen_focus,
  menu_mov,
  StartCollections,
  ListVodView,
  template
) {
  //'collections/vodMovListCollections'

  var st_complete = "";

  var VodView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.id_menu = 1;

      this.bus = options.bus;
      this.type = options.type;

      console.log(this.type);

      this.bus.on("onSetMovPlayFocus", this.onSetMovPlayFocus, this);
      this.bus.on("playVodContent", this.showPlay, this);

      this.bus.on("onSetItemContent ", this.onSetItemContent, this);
      this.bus.on("VodContentClose", this.onClose, this);

      screen_focus.setData({ view: "vodcontent", focus: "mov_play" });
    },

    create: function (data) {
      this.item = data.item;

      this.id_item = data.item.id_item;
      this.roots = data.roots;

      var groups = vod_content_focus.toJSON();
      this.category = groups[0].category;
      this.list = groups[0].list;

      console.log(data);

      /*
      vod.getSingleVod(this.id_item).then(function (res) {
        this.render();
        this.onSetItemContent(data);
      });
      */

      this.render();
      this.onSetItemContent(this.item);
    },

    events: {
      keydown: "keyAction",
      "click #mov_trailer": "showSearch",
      "click #mov_play": "showStart",
      "click #mov_bookmark": "showLiveTV"
    },

    onSetMovPlayFocus: function () {
      this.id_menu = 1;
      this.onSetFocus(this.id_menu);
    },

    onSetItemContent: function (data) {
      console.log(data);

      if (data.vr_media == "vod") {
        this.type = "movie";
      } else {
        this.type = "tvShow";
      }

      vod_focus.setData(data);

      /*
      if (typeof data.genres !== "undefined") {
        if (data.genres[0].hasOwnProperty("genreName")) {
          $("#year-group").text(data.genres[0].genreName);
        } else {
          $("#year-group").text("");
        }
      }
      */

      if (this.type == "movie") {
        $("#caption-message").text(data.name);
        $("#mov_play span.links_name.tag_2").text(menu_mov.at(1).get("name"));
      } else {
        $("#caption-message").text(data.name_sec);
        $("#mov_play span.links_name.tag_2").text("Pusti: " + data.episode_name);
      }

      //$("#caption-message").text("aaaaaaaaaaaaaaa");

      $("#synopsis").text(data.description);

      $("#mov_image").css("background-image", "url(" + data.picture.backgrounds + ")");
    },

    keyAction: function (e) {
      var code = e.keyCode || e.which;
      e.stopPropagation();
      default_key(e.keyCode);

      switch (e.keyCode) {
        case KEY_UP:
          if (this.id_menu > 0) {
            this.id_menu = this.id_menu - 1;
            this.onSetFocus(this.id_menu);
          }
          break;
        case KEY_DOWN:
          if (this.id_menu < menu_mov.size() - 1) {
            this.id_menu = this.id_menu + 1;
            this.onSetFocus(this.id_menu);
          } else {
            console.log("brisi");
            this.$el.find(".nav_mov_content").parent().find("div[tabindex]").removeClass("focus");

            console.log(this.category);

            $("#control_" + this.category.id_list).focus();

            //this.bus.trigger("SetFocusList");
          }
          break;
        case KEY_HOME:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_MENU:
          if (st_complete !== "1") this.bus.trigger("ExpandMenu");
          return false;
          break;
        case KEY_RETURN:
        case KEY_BACK:
          // uklanjanje zadnjeg ID iz roots (root parent ove liste)
          //this.roots.pop();

          console.log(this.roots);
          if (this.roots[0] != undefined && this.roots.length > 0) {
            if (this.type == "movie") {
              var rootID = this.roots[this.roots.length - 1];
              this.bus.trigger("RenderChildren", { ID: rootID, roots: this.roots });
            } else {
              var rootID = this.roots[this.roots.length - 1];
              this.bus.trigger("RenderSezons", { ID: rootID, roots: this.roots });
            }
          } else {
            this.bus.trigger("ExpandMenu");
          }

          return false;
          break;
        case KEY_ENTER:
          var focused = document.activeElement;

          if (focused.id == "mov_trailer") {
            this.showTrailer();
          } else if (focused.id == "mov_play") {
            this.showPlay();
          } else if (focused.id == "mov_bookmark") {
            this.showBookmark();
          }
          break;
      }

      return false;
    },

    showTrailer: function () {
      console.log("Trailer");
    },

    showPlay: function () {
      console.log("XXXXXXXXXXXXXX", this.roots);

      this.bus.trigger("HideScreen", {
        view: screen_focus.get("view"),
        vr_media: vod_focus.get("vr_media"),
        roots: this.roots,
        control_focus: screen_focus.get("focus")
      });

      if (this.type == "movie") {
        console.log("FILM");
        player.playVodV3(vod_focus.get("id_item"), 0);
      } else {
        console.log("SERIJA");
        player.playShowV3(vod_focus.get("id_item"), 0);
      }
      this.bus.trigger("ListViewVodClose");
      this.bus.trigger("VodContentClose");
      $("#other_container").empty();
    },

    showBookmark: function () {
      console.log("Bookmark");
    },

    onSetFocus: function (id_m) {
      //console.log(menu_mov);
      //console.log('id_m', id_m);
      //console.log('xxx', menu_mov.get(id_m).get("menu"));

      this.$el.find(".nav_mov_content").parent().find("div[tabindex]").removeClass("focus");
      this.$el.find(menu_mov.get(id_m).get("menu")).addClass("focus");
      this.$el.find(menu_mov.get(id_m).get("menu")).focus();
    },

    render: function () {
      console.log("render");
      var templ = _.template(template);
      var html = templ();
      this.$el.html(html);

      this.onSetFocus(this.id_menu);

      //console.log(ListVodView);

      console.log(this.roots);

      listView = new ListVodView({
        el: "#middle_list",
        type: this.type,
        category: this.category,
        collection: this.list,
        display_back: "yes",
        bus: this.bus,
        roots: this.roots // potrebno za Back
      });

      //$("#other").append(listView.render().el);

      this.$el.append(listView.render());
      this.$el.find("#list-content_" + this.category.id_list).css("left", this.category.left_shift + "vw");

      //$("#vod_row_1.rowxx div.list-data-pmd span.name_category").text("Preporučujemo");

      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return VodView;
});

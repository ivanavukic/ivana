define([
  "jquery",
  "underscore",
  "backbone",
  "views/s_scfview",
  "views/s_themeview",
  "views/s_dynamicview",
  "views/s_keyboardview",
  "views/s_pinview",
  "coreSettings",
  "coreLogin",
  "bluebird",
  "text!page/s_settings.html",
], function (
  $,
  _,
  Backbone,
  ScreenConfigView,
  ThemeView,
  DynamicView,
  KeyboardView,
  PinView,
  Core,
  coreLogin,
  Promise,
  Template
) {
  var apidata = undefined;
  var dyn_list = [];

  var init_settings_view = function (options) {
    function my_purchases() {
      // function active_packages(args){
      //     dyn_list = [];
      //     var data_list = apidata.activePackageProducts;
      //     if(data_list.length > 0){
      //         for(var i = 0; i < data_list.length; i++){
      //             dyn_list.push({ name: data_list[i].packageName, id: "package-"+ i.toString(), info: data_list[i].price + " din", onclick: function(){console.log("qwe");}, args: data_list[i]});
      //         }
      //     }else{
      //         dyn_list.unshift({name: "Nema aktivnih paketa", inactive: true, static: true});
      //     }
      //     dyn_list.unshift({name: "Aktivni paketi"});
      //     settings.view.set_list(dyn_list);
      //     options.bus.trigger("Render");
      // }

      function active_VOD(args) {
        dyn_list = [];

        var data_list = apidata.activeVodProducts;
        if (data_list.length > 0) {
          for (var i = 0; i < data_list.length; i++) {
            var prod = data_list[i];
            var format =
              prod.contenttype === "VIDEO_VOD"
                ? "DD.MM.YYYY. HH:mm"
                : "DD.MM.YYYY.";
            text = moment(prod.ordertime, "YYYYMMDDhhmmss")
              .locale(currentLanguage)
              .format(format);
            dyn_list.push({
              name: prod.contentname || prod.name,
              id: "package-" + i.toString(),
              info: text,
              onclick: function () {
                console.log("qwe");
              },
              args: prod,
            });
          }
        } else {
          dyn_list.unshift({
            name: "Nema aktivnih proizvoda",
            inactive: true,
            static: true,
          });
        }
        dyn_list.unshift({ name: "Aktivni paketi" });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      }

      function other(args) {
        dyn_list = [];
        var data_list = apidata.inactiveProducts;
        if (data_list.length > 0) {
          for (var i = 0; i < data_list.length; i++) {
            var prod = data_list[i];
            var format =
              prod.contenttype === "VIDEO_VOD"
                ? "DD.MM.YYYY. HH:mm"
                : "DD.MM.YYYY.";
            text = moment(prod.ordertime, "YYYYMMDDhhmmss")
              .locale("sr")
              .format(format);
            dyn_list.push({
              name: prod.contentname || prod.name,
              id: "package-" + i.toString(),
              info: text,
              onclick: function () {
                console.log("qwe");
              },
              args: prod,
            });
          }
        } else {
          dyn_list.unshift({
            name: "Nema proizvoda",
            inactive: true,
            static: true,
          });
        }
        dyn_list.unshift({ name: "Aktivni paketi" });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      }

      var fill_dyn = function () {
        dyn_list = [
          // { name: "Aktivni Paketi", id: "purchase-1", optionsList : {list_function: active_packages, list_function_args : null}},
          {
            name: "Aktivni VOD",
            id: "purchase-2",
            optionsList: {
              list_function: active_VOD,
              list_function_args: null,
            },
          },
          {
            name: "Ostali",
            id: "purchase-3",
            optionsList: { list_function: other, list_function_args: null },
          },
        ];

        dyn_list.unshift({ name: "Kupovine" });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      };
      dyn_list = [];

      if (apidata) {
        fill_dyn();
      } else {
        // CoreProducts.getAllPurchaseHistory({ profileId: -1 }).then(function (
        //   response
        // ) {
        //   apidata = response;
        // console.log(apidata);
        // *** apidata.activeVodProducts = ["nema proizvoda"];
        // *** apidata.activePackageProducts = ["nema proizvoda"];
        // *** apidata.inactiveProducts = ["nema proizvoda"];
        //   fill_dyn();
        // });
      }
    }

    function change_pin() {
      var customerId = profili[curProfilePosition].customer_id;
      var curProfileId = sys.getItemPermanent("curProfileId");
      var old_pin;
      var new_pin;
      var confirmed_pin;

      var confirm_new = function (text) {
        if (new_pin == text) {
          confirmed_pin = text;
          console.log(curProfileId, "curProfileId za password change");

          console.log("Novi pin 1.unos", new_pin);
          console.log("Confirmed pin", confirmed_pin);
          coreLogin
            .changeProfilePassword(old_pin, confirmed_pin)

            .then(function (res) {
              console.log(res, "Uspesno zamenjen pin");
              keyboard.show_message("Uspesno zamenjen pin");
              keyboard.on_close();
            })
            .catch(function (err) {
              console.log(err);
              keyboard.show_message(
                "Ne bi trebalo da se dešava. Uneti novi pin."
              );
              keyboard.clear_input();
              keyboard.func = enter_new;
            });
        } else {
          keyboard.show_message("Pin se ne poklapa. Uneti novi pin.");
          keyboard.clear_input();
          keyboard.func = enter_new;
        }
      };

      var enter_new = function (text) {
        keyboard.show_message("Potvrditi novi pin.");
        keyboard.clear_input();
        new_pin = text;
        keyboard.func = confirm_new;
      };

      var keyboard = new PinView({
        bus: options.bus,

        func: function (text) {
          // old_pin = text;

          // console.log(curProfileId, "curProfileId za password check");
          console.log(text, " ovo je uneti pin");
          console.log(
            (customer_id = profili[curProfilePosition].customer_id),
            " ovo je customer id za koga proveravamo pass"
          );

          coreLogin
            .checkSubscriberPassword(text)
            .then(function (res) {
              console.log(res);
              old_pin = text;
              keyboard.show_message("Uneti novi pin.");
              keyboard.clear_input();
              keyboard.func = enter_new;
            })
            .catch(function (err) {
              console.log(err);
              keyboard.show_message("Neispravan pin.");
              keyboard.clear_input();
            });
        },
      });
      keyboard.show_message("Uneti admin pin");
    }

    function packages() {
      function packageListing(args) {
        function additionalPackages(args) {
          console.log(args);
        }
        var data_list = args.additionalPackages;
        dyn_list = [];
        for (var i = 0; i < data_list.length; i++) {
          dyn_list.push({
            name: data_list[i].packageName,
            id: "package-" + i.toString(),
            info: data_list[i].price + " din",
            onclick: additionalPackages,
            args: data_list[i],
          });
        }
        dyn_list.unshift({ name: "Dodatni paketi za " + args.packageName });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      }

      function no_package(args) {
        console.log(args);
      }

      var fill_dyn = function () {
        for (var i = 0; i < apidata.length; i++) {
          dyn_list.push({
            name: apidata[i].packageName,
            id: "profile-" + i.toString(),
            info: apidata[i].price + " din",
          });
          if (apidata[i].additionalPackages == null) {
            dyn_list[i].onclick = no_package;
            dyn_list[i].args = apidata[i];
          } else {
            dyn_list[i].optionsList = {
              list_function: packageListing,
              list_function_args: apidata[i],
            };
          }
        }
        dyn_list.unshift({ name: "Paketi" });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      };

      dyn_list = [];
      if (apidata) {
        fill_dyn();
      } else {
        console.log(
          "(Print od značaja, treba API za profil:",
          profili[curProfilePosition].name
        );
        //    Products.getAvailablePackages({ username: "testui" }).then(function (
        //       data
        //     ) {
        //       apidata = data.packages;
        //       fill_dyn();
        //     });
      }
    }

    function profiles() {
      function profile(args) {
        console.log(args);
      }

      var fill_dyn = function () {
        for (var i = 0; i < apidata.length; i++) {
          dyn_list.push({
            name: apidata[i].name,
            id: "profile-" + i.toString(),
            onclick: profile,
            args: apidata[i],
          });
        }
        dyn_list.unshift({ name: "Izbor profila" });
        settings.view.set_list(dyn_list);
        options.bus.trigger("Render");
      };
      dyn_list = [];

      if (apidata) {
        fill_dyn();
      } else {
        coreLogin.getCustomerProfiles().then(function (res) {
          apidata = res;
          fill_dyn();
        });
      }
    }

    function device() {
      function change_device_name() {
        var keyboard = new KeyboardView({
          bus: options.bus,
          current_text: Utility.getValueByName("friendlyName"),
          func: function (text) {
            Utility.setValueByName("friendlyName", text);
            keyboard.on_close();
            // Reprezentativan primjer sta mora kada settings.view postoji
            options.bus.trigger("Execute");
            settings.view.set_list(dyn_list);
            options.bus.trigger("Render");
          },
        });
        keyboard.show_message("Uneti ime uređaja");
      }

      dyn_list = [
        { name: "Uređaj" },
        {
          name: "Korisničko ime",
          id: "username",
          info: localStorage.customerId,
          inactive: true,
        },
        {
          name: "Podešavanja uređaja",
          id: "device-settings-lower",
          inactive: true,
          static: true,
        },
        {
          name: "Ime uređaja",
          id: "change-device-name",
          info: localStorage.deviceId,
          onclick: change_device_name,
        },
        {
          name: "Informacije o uređaju",
          id: "device-information",
          inactive: true,
          static: true,
        },
        {
          name: "STB verzija",
          id: "stb-version",
          info: stbVersion,
          inactive: true,
        },
        {
          name: "IP adresa",
          id: "ip-adress",
          info: "IP adresa", //treba API
          inactive: true,
        },
        {
          name: "MAC adresa",
          id: "mac-adress",
          info: macAddress,
          inactive: true,
        },
      ];
    }

    options.list = [
      {
        name: "Uređaj",
        id: "uredjaj",
        view: DynamicView,
        args: { list_function: device, bus: options.bus },
      },
      {
        name: "Moje kupovine",
        id: "moje-kupovine",
        async: true,
        view: DynamicView,
        args: { list_function: my_purchases, bus: options.bus },
      },
      { name: "Promeni pin", id: "promeni-pin", on_click: change_pin },
      {
        name: "Paketi",
        id: "paketi",
        async: true,
        view: DynamicView,
        args: { list_function: packages, bus: options.bus },
      },
      {
        name: "Profili",
        id: "profili",
        async: true,
        view: DynamicView,
        args: { list_function: profiles, bus: options.bus },
      },
      {
        name: "Ekran",
        id: "ekran",
        view: ScreenConfigView,
        args: { bus: options.bus },
      },
      { name: "Tema", id: "tema", view: ThemeView, args: { bus: options.bus } },
    ];

    options.title = "Podešavanja";

    var settings = new settings_view(options);
    return settings;
  };

  var settings_view = Backbone.View.extend({
    initialize: function (options) {
      this.el = options.el;
      this.bus = options.bus;
      this.list = options.list;
      this.title = options.title;
      this.active_option_id = 0;
      this.bus.on(
        "CancelSettingHighlight",
        function () {
          $("#" + this.list[this.active_option_id].id).removeClass("active");
        },
        this
      );
      this.bus.on(
        "ClearApidata",
        function () {
          apidata = undefined;
          dyn_list = [];
        },
        this
      );
    },

    create: function () {
      this.render();
      $("#" + this.list[this.active_option_id].id).addClass("select");
      $("#left_column").focus();
      this.set_view();
    },

    render: function () {
      var template = _.template(Template);
      var html = template({ title: this.title, list: this.list });
      this.$el.html(html);
    },

    on_close: function () {
      this.bus.off("CancelSettingHighlight");
      this.bus.off("ClearApidata");
      this.remove();
    },

    set_view: function () {
      if (this.list[this.active_option_id].view) {
        $("#" + this.list[this.active_option_id].id).addClass("active");
        $("#right_column").removeClass("bcg");

        this.view = this.list[this.active_option_id].view(
          this.list[this.active_option_id].args
        ); // execute
        if (!this.list[this.active_option_id].async) {
          if (this.list[this.active_option_id].view == DynamicView) {
            this.view.set_list(dyn_list);
            this.view.render();
          }
        }
      } else {
        this.list[this.active_option_id].on_click();
      }
    },

    events: {
      keydown: "key_action",
    },

    selection_animation: function (offset) {
      $("#" + this.list[this.active_option_id].id).removeClass("select");
      $("#" + this.list[this.active_option_id].id).removeClass("active");
      this.active_option_id =
        (this.active_option_id + offset + this.list.length) % this.list.length;
      $("#" + this.list[this.active_option_id].id).addClass("select");
    },

    key_action: function (e) {
      e.stopPropagation();
      e.stopImmediatePropagation(); // zbog bubblinga

      switch (e.keyCode) {
        case KEY_UP:
          console.log("GORE");
          this.selection_animation(-1);
          break;
        case KEY_DOWN:
          console.log("DOLE");
          this.selection_animation(1);
          break;
        case KEY_ENTER:
          $("#" + this.list[this.active_option_id].id).addClass("active");
          this.set_view();
          break;
        case KEY_HOME:
        case KEY_MENU:
        case KEY_RETURN:
        case KEY_BACK:
          console.log("KEY BACK IN SETTINGS");
          $("#main_view_container").show();
          this.on_close();
          this.bus.trigger("ExpandMenu");

          break;
        default:
          console.log("TODO");
          break;
      }
      return false;
    },
  });

  return init_settings_view;
});

define([
  "jquery",
  "underscore",
  "backbone",
  "coreChanEpg",
  "collections/epgItemCollections",
  "models/channel_focus",
  "models/epg_focus",
  "models/screen_focus",
  "views/epgsView",
  "views/searchView",
  "views/startView",
  "text!page/slider.html",
  "jquery-ui"
], function (
  $,
  _,
  Backbone,
  tv,
  EpgCollections,
  channel_focus,
  epg_focus,
  screen_focus,
  EpgsView,
  SearchView,
  StartView,
  template,
  ui
) {
  var st_complete = "";

  var prevKey = 0;
  var longPress = 0;
  var longPressCnt = 0;
  var status_change_step = false;

  var status_play = false;
  var status_right_slide = true;

  var hide_screen;

  var control_focus = {
    "custom-handle": {
      40: { name: "con_play" }, //KEY_DOWN
      38: { name: "" }, //KEY_UP
      37: { name: "" }, //KEY_LEFT
      39: { name: "" } //KEY_RIGHT
    },
    con_play: {
      40: { name: "epg_set_focus" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_rewind" }, //KEY_LEFT
      39: { name: "con_forward" } //KEY_RIGHT
    },
    con_forward: {
      40: { name: "epg_set_focus" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_play" }, //KEY_LEFT
      39: { name: "con_reply" } //KEY_RIGHT
    },
    con_reply: {
      40: { name: "epg_set_focus" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_forward" }, //KEY_LEFT
      39: { name: "con_info" } //KEY_RIGHT
    },
    con_info: {
      40: { name: "epg_set_focus" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "con_reply" }, //KEY_LEFT
      39: { name: "" } //KEY_RIGHT
    },
    con_rewind: {
      40: { name: "epg_set_focus" }, //KEY_DOWN
      38: { name: "custom-handle" }, //KEY_UP
      37: { name: "" }, //KEY_LEFT
      39: { name: "con_play" } //KEY_RIGHT
    },
    epg: {
      40: { name: "" }, //KEY_DOWN
      38: { name: "epg_lost_focus" }, //KEY_UP
      37: { name: "" }, //KEY_LEFT
      39: { name: "" } //KEY_RIGHT
    }
  };

  var SliderView = Backbone.View.extend({
    tagName: "span",

    initialize: function (options) {
      this.y = 0;
      this.pos = 31;
      this.bus = options.bus;

      this.bus.on("SliderClose", this.onClose, this);

      ////console.log(aa["browsers"]);
      //this.state_live=player.getState();
    },

    create: function () {
      var self = this;
      ////console.log(EpgCollections.toJSON());

      if (epg_focus.get("st_live") == true) {
        var self = this;
        var data = {};
        //console.log("Usao Slider");

        tv.getTrenutniEpg(channel_focus.get("idOriginal")).then(function (res) {
          data = res;
          tv.getTrenutno(channel_focus.get("idOriginal"))
            .then(function (live_res) {
              EpgCollections.reset();
              EpgCollections.push(data);
              ////console.log(EpgCollections.toJSON());
              self.epg = EpgCollections.toJSON();
              self.live = live_res;

              ////console.log("*** self.epg", self.epg);
              ////console.log("*** self.live", self.live.ID);

              epg_index = _.findIndex(self.epg, { ID: self.live.ID });
              id_epg_focus = self.epg[epg_index].ID;

              ////console.log(epg_index);
              ////console.log(id_epg_focus);

              epg_focus.set({ max_number: self.epg.length - 1 });
              epg_focus.set({ id: epg_index }, { validate: true });
              self.set_value_epg_focus();

              ////console.log("data", data);
              ////console.log("self.epg", self.epg);
              ////console.log("self.live", self.live);
              ////console.log("id_epg_focus", id_epg_focus);
              ////console.log("count", epg_focus.get("max_number"), "id", epg_focus.get("id"), self.epg[epg_focus.get("id")].name);
            })
            .then(function (res) {
              //console.log("RENDER");
              self.render();
            });
        });
      } else {
        this.epg = EpgCollections.toJSON();
        this.render();
      }
    },

    events: {
      keyup: "keyActionUp",
      keydown: "keyActionDown",
      "slide #slider": "onSlide",
      "stop #slider": "onSlideStop",
      "keyup #slider": "onSlideKeyUp",
      "keydown #custom-handle": "onKeyDown_customhandle",
      "keydown #con_play": "onKeyDown_con_play",
      "keydown #con_reply": "onKeyDown_con_reply",
      "keydown #con_forward": "onKeyDown_con_forward",
      "keydown #con_info": "onKeyDown_con_info",
      "keydown #con_rewind": "onKeyDown_con_rewind",
      "keydown #epg": "onKeyDown_epg",

      "focus #custom-handle": "TimeOutHideScreen",
      "focus #con_play": "TimeOutHideScreen",
      "focus #con_reply": "TimeOutHideScreen",
      "focus #con_forward": "TimeOutHideScreen",
      "focus #con_info": "TimeOutHideScreen",
      "focus #con_rewind": "TimeOutHideScreen",
      "focus #epg": "TimeOutHideScreen"
    },

    onSlideStop: function () {
      //console.log("STOP XXXXXXXXXXXXXXXX");
    },

    ClearTimeOutHideScreen: function () {
      ////console.log("hide_screen");
      clearTimeout(hide_screen);
    },

    sget: function (key) {
      return $("#slider").slider("option", key);
    },

    sset: function (data) {
      return $("#slider").slider(data);
    },

    TimeOutHideScreen: function () {
      ////console.log("TimeOutHideScreen");
      var self = this;
      clearTimeout(hide_screen);

      hide_screen = setTimeout(function () {
        var vr_m = "";
        if (epg_focus.get("st_live") == true) vr_m = "live";
        else vr_m = "catchup";

        self.bus.trigger("HideScreen", { view: "channel", vr_media: vr_m, control_focus: "channel" });
      }, 3000);
    },

    onKeyDown_customhandle: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        //console.log("on_custom-handle");
      }
    },
    onKeyDown_con_play: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        if (status_play == true) {
          this.onStop();
        } else {
          console.log("G1");
          this.onPlay();
        }
      }
    },
    onKeyDown_con_reply: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        $("#slider").slider({ value: 1 });
        epg_focus.set({ st_live: false });
        this.onPlay();
      }
    },
    onKeyDown_con_forward: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        this.onStop();
        //console.log("GRESKA 1");
        var speed = player.fastForward();
        $("#fast_control_name").text(speed);
      }
    },

    onKeyDown_con_rewind: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        this.onStop();
        //console.log("GRESKA 2");
        var speed = player.fastRewind();
        $("#fast_control_name").text(speed);
      }
    },

    onKeyDown_con_info: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        //console.log("on_con_info");
      }
    },

    onKeyDown_epg: function (e) {
      if (KEY_OK == e.keyCode || KEY_ENTER == e.keyCode) {
        //console.log("on_epg");
      }
    },

    onSlideKeyUp: function (event) {
      switch (event.keyCode) {
        case KEY_CHDOWN:
          console.log("MENJAM KANAL Z PLEJERA GORE 1");
          this.bus.trigger("QuickChanUp", event);
          return false;
          break;
        case KEY_CHUP:
          console.log("MENJAM KANAL Z PLEJERA DOLE 1");
          this.bus.trigger("QuickChanDown", event);
          return false;
          break;
        case KEY_LEFT:
          if (status_play == false) this.onPlay();
          break;
        case KEY_RIGHT:
          if (status_play == false) this.onPlay();
          break;
      }
      this.TimeOutHideScreen();
      return;
    },

    onSlide: function (event, ui) {
      event.stopPropagation();

      ////console.log(">>>>>>>>>>>>>>>>>>>");
      //this.onChangeStep(5, "");

      var max = this.sget("max");

      this.ClearTimeOutHideScreen();

      switch (event.keyCode) {
        case KEY_CHDOWN:
          console.log("MENJAM KANAL Z PLEJERA GORE");
          console.log(event);
          this.bus.trigger("QuickChanUp", event);
          return false;
          break;
        case KEY_CHUP:
          console.log("MENJAM KANAL Z PLEJERA DOLE");
          console.log(event);
          this.bus.trigger("QuickChanDown", event);
          return false;
          break;
        case KEY_LEFT:
          status_right_slide = true;
          if (status_play == true) this.onStop();
          ////console.log(ui.value / this.sget( "step"), ui.value, this.sget( "step"));
          epg_focus.set({ st_live: false });
          if (ui.value / this.sget("step") <= 1 && ui.value / this.sget("step") > 0) {
            this.onChangeStep(15, "2x");
            //longPressCnt = 5;
          }

          this.update(ui.value);
          this.handleKey(event.keyCode, 0);

          break;
        case KEY_RIGHT:
          //console.log("RIGHT 3");
          if (status_right_slide == false) return false;
          if (status_right_slide == true) {
            if (status_play == true) this.onStop();
            if (
              (this.sget("max") - ui.value) / this.sget("step") < 1 &&
              (this.sget("max") - ui.value) / this.sget("step") > 0
            ) {
              this.onChangeStep(15, "2x");
              status_change_step = true;
            }

            if (ui.value > positionLive(epg_focus.get("startTime"), getCurentEpoch())) {
              //Korekcija
              //console.log("Korekcija");

              var tmp_fokus = document.activeElement;
              //$("#custom-handle").blur();

              console.log("$$$$$$$$$$$$$", document.activeElement);

              //this.sget("max") > this.sget("value")

              status_right_slide = false;
              epg_focus.set({ st_live: true });
              ////console.log(positionLive(epg_focus.get("startTime"), getCurentEpoch()));
              $("#slider").slider({ value: positionLive(epg_focus.get("startTime"), getCurentEpoch()) - 1 });
              this.handleKey(event.keyCode, -1);

              this.onPlay();
            }
            this.update(ui.value);
            this.handleKey(event.keyCode, 0);
          } else {
            //$("#con_play").focus();
          }
          break;
        case KEY_DOWN:
          this.onControlPosition("custom-handle", event.keyCode);
          return false;
          break;
      }

      return;
    },

    keyActionUp: function (e) {
      this.handleKey(e.keyCode, 1);
      this.TimeOutHideScreen();
      switch (e) {
        case KEY_CHDOWN:
          console.log("MENJAM KANAL Z PLEJERA GORE");
          this.bus.trigger("QuickChanUp", e.keyCode);
          return false;
          break;
        case KEY_CHUP:
          console.log("MENJAM KANAL Z PLEJERA DOLE");
          this.bus.trigger("QuickChanDown", e.keyCode);
          return false;
          break;
      }
    },

    keyActionDown: function (e) {
      default_key(e.keyCode);
      console.log("KEY JE ", e);
      var focused = document.activeElement;
      //log(focused);

      ////console.log(e.keyCode);

      this.ClearTimeOutHideScreen();

      switch (e.keyCode) {
        case KEY_CHDOWN:
          console.log("MENJAM KANAL Z PLEJERA GORE");
          this.bus.trigger("QuickChanUp", e.keyCode);
          return false;
          break;
        case KEY_CHUP:
          console.log("MENJAM KANAL Z PLEJERA DOLE");
          this.bus.trigger("QuickChanDown", e.keyCode);
          return false;
          break;
        case KEY_DOWN:
          this.onControlPosition(focused.id, e.keyCode);
          return false;
          break;
        case KEY_UP:
          this.onControlPosition(focused.id, e.keyCode);
          return false;
          break;
        case KEY_LEFT:
          if (focused.id == "custom-handle") {
            var slider_min = this.sget("min");
            var slider_val = this.sget("value");
            console.log("EPG LEVO");
            if (slider_val == slider_min) {
              this.bus.trigger("SliderEpgLeft");
              //this.set_value_epg_focus();
              this.onFillEPGContent({ start: "end" });
              this.handleKey(e.keyCode, 1);
            }
          } else {
            this.onControlPosition(focused.id, e.keyCode);
          }

          return false;
          break;
        case KEY_RIGHT:
          //console.log("RIGHT 1");
          if (focused.id == "custom-handle") {
            var slider_max = this.sget("max");
            var slider_val = this.sget("value");
            console.log("EPG Desno");
            if (slider_val == slider_max) {
              this.bus.trigger("SliderEpgRight");
              //this.set_value_epg_focus();
              this.onFillEPGContent({ start: "start" });
              this.handleKey(e.keyCode, 1);
            }
          } else {
            this.onControlPosition(focused.id, e.keyCode);
          }
          return false;
          break;
        case KEY_BACK:
        case KEY_RETURN:
          console.log("return is playera");
          var vr_m = "";
          if (epg_focus.get("st_live") == true) vr_m = "live";
          else vr_m = "catchup";
          this.bus.trigger("HideScreen", { view: "channel", vr_media: vr_m, control_focus: "channel" });
          // this.onClose();
          // this.show_return();
          return false;
          break;
        case KEY_ENTER:
          return false;
          break;
        case KEY_OK:
          return false;
          break;
      }

      return false;
    },

    show_return: function () {
      console.log(screen_focus.get("view"));
      switch (screen_focus.get("view")) {
        case "start":
          player.stop();
          $("#player_view_container").removeClass("active").addClass("inactive");
          $("#screen_view_container").removeClass("inactive").addClass("active");

          var startView = new StartView({ el: "#other_container", bus: this.bus });
          startView.create();

          return false;
          break;
        case "search":
          player.stop();
          $("#player_view_container").removeClass("active").addClass("inactive");
          $("#screen_view_container").removeClass("inactive").addClass("active");

          var searchView = new SearchView({ el: "#other_container", bus: this.bus });
          searchView.create();

          return false;
          break;
      }

      this.bus.trigger("SetFokusMenu");
      return false;
    },

    onControlPosition: function (control, key) {
      var tmp_control = control_focus[control];
      ////console.log("***********************************");
      ////console.log(control);
      ////console.log(tmp_control);
      ////console.log(tmp_control[key]);
      ////console.log("***********************************");
      if (tmp_control[key].name !== "") this.onControlSetFocus(tmp_control[key].name);
      else return false;
    },

    onControlSetFocus: function (control) {
      if (control == "epg_set_focus") {
        this.$el.find("#slider_page").parent().find("div[tabindex]").removeClass("focus");
        $("#trc_slider_epg").animate(
          { height: "30.555555555555557vh" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
              $("#epg").css("opacity", "1");
              $("#epg").focus();
            }
          }
        );
      } else if (control == "epg_lost_focus") {
        $("#trc_slider_epg").animate(
          { height: "10.416666666666668vh" },
          {
            duration: 150,
            complete: function () {
              st_complete = "";
              $("#epg").css("opacity", "0");
              $("#custom-handle").focus();
            }
          }
        );

        this.$el.find("#slider_page").parent().find("div[tabindex]").removeClass("focus");
        $("#custom-handle").addClass("focus");
        $("#custom-handle").focus();
      } else {
        this.$el.find("#slider_page").parent().find("div[tabindex]").removeClass("focus");
        $("#" + control).addClass("focus");
        $("#" + control).focus();
      }
    },

    duration: function (end, start) {
      var diff = (parseInt(end) - parseInt(start)) / 1000;
      dbgLog((parseInt(end) - parseInt(start)) / 1000);
      return parseInt(diff);
    },

    onFillEPGContent: function (status) {
      $("#trc_name_content").text(epg_focus.get("name"));
      $("#trc_date_content").text(formatDate(Number(epg_focus.get("startTime"))));
      $("#trc_time_content").text(
        formatTime(Number(epg_focus.get("startTime"))) + "-" + formatTime(Number(epg_focus.get("endTime")))
      );
      //$("#trc_date_content").text("bbbbbbb<br>cccccc");

      var max_val = this.duration(epg_focus.get("endTime"), epg_focus.get("startTime"));

      console.log("max_val", max_val);

      if (status.start == "start") {
        $("#slider").slider({ value: 0, max: max_val });
        console.log(this.sget("max"));
      } else if (status.start == "end") {
        $("#slider").slider({ value: max_val, max: max_val });
        console.log(this.sget("max"));
      }
    },

    set_value_epg_focus: function () {
      this.epg = EpgCollections.toJSON();
      var data = this.epg[epg_focus.get("id")];
      epg_focus.setData(data);
    },

    render: function () {
      var templ = _.template(template);
      var html = templ({
        channel: channel_focus.toJSON()
      });
      this.$el.html(html);

      this.onFillEPGContent({ start: "start" });
      this.onShowPlayerEpg();

      //this.onPlay();

      if (typeof progress !== "undefined") clearInterval(progress);
      status_play = true;
      progress = setInterval(this.tick.bind(this), 1000);

      if (epg_focus.get("st_live") == true) {
        $("#slider").slider({ value: positionLive(epg_focus.get("startTime"), getCurentEpoch()) });
        this.update(this.sget("value"));
      } else {
        //console.log("GRESKA 3");
        $("#slider").slider({ value: Math.ceil(player.getCurPos() + 0.1) });
        this.update(this.sget("value"));
      }

      //$("#custom-handle").focus();
      $("#custom-handle").addClass("focus");
      $("#custom-handle").focus();
      $("#epg").css("opacity", "0");

      return this;
    },

    onShowPlayerEpg: function () {
      var epgsView = new EpgsView({ el: "#trc_slider_epg", bus: this.bus });
      epgsView.create();

      return false;
    },

    update: function (seconds) {
      //var seconds = this.sget( "value");

      hours = Math.floor(seconds / 3600);
      hours = ("0" + hours).slice(-2);

      seconds %= 3600;

      minutes = Math.floor(seconds / 60);
      minutes = ("0" + minutes).slice(-2);

      seconds = seconds % 60;
      seconds = ("0" + seconds).slice(-2);

      $("#slider_time").text(hours + ":" + minutes + ":" + seconds);

      //$("#slider_time").text(seconds);
    },

    handleKey: function (code, isKeyUp, key) {
      ////console.log('handleKey');
      ////console.log('isKeyUp',isKeyUp);

      if (isKeyUp) {
        if (prevKey == code) {
          prevKey = 0;
          longPress = 0;
          longPressCnt = 0;
          status_change_step = false;

          $("#slider").slider({ step: 5 });
        }
        return;
      }

      if (prevKey == code) {
        longPress = 1;
        longPressCnt += 1;
      }
      prevKey = code;

      //Korekcija da se ne ubrzava za *2 ako je kraj videa

      if (code == KEY_RIGHT) {
        ////console.log("*****", (this.sget( "max") - this.sget( "value")) / this.sget( "step"));
        if (
          (this.sget("max") - this.sget("value")) / this.sget("step") < 4 &&
          (this.sget("max") - this.sget("value")) / this.sget("step") > 0
        ) {
          status_change_step = true;
          ////console.log("status_change_step = true;");
        }
      }

      //console.log("************* longPress", longPress);
      //console.log("************* longPressCnt", longPressCnt);

      // RAZRADA
      if (longPress == 1) {
        if (longPressCnt == 5 && status_change_step == false) {
          this.onChangeStep(15, "2x");
          ////console.log("2x");
        } else if (longPressCnt == 10 && status_change_step == false) {
          this.onChangeStep(30, "4x");
          ////console.log("4x");
        } else if (longPressCnt == 15 && status_change_step == false) {
          this.onChangeStep(60, "8x");
          ////console.log("8x");
        }
      }

      ////console.log("LONG PRESS FOR KEY: " + code + " LONG PRESS COUNT: " + longPressCnt );
    },

    onChangeStep: function (v_step, caption) {
      $("#fast_control_name").text(caption);
      $("#slider").slider({ step: v_step });
    },

    onPlay: function () {
      $("#slider").slider({ step: 5 });
      if (this.sget("value") == 0) $("#slider").slider("value", 1);
      var v_seconds = this.sget("value");
      //player.seekToPos(v_seconds);

      /************************************************************************/
      ////console.log("EPG ***************************************************");
      var mchId = epg_focus.get("idOriginal");
      var mediaId = epg_focus.get("mediaId");
      var playbillId = epg_focus.get("id_epg");
      var position = v_seconds;

      //console.log("PLAY PLAY PLAY");
      //console.log(epg_focus.get("st_live"));

      if (epg_focus.get("st_live") == true) {
        //console.log("GRESKA 4");
        player.playV3(mchId, mediaId, playbillId, position);
        dbgLog("LIVE - " + "mchId:" + mchId);
      } else {
        //console.log("GRESKA 5");
        player.playV3(mchId, mediaId, playbillId, position);
        dbgLog(
          "EPG - " + "mchId:" + mchId + "  mediaId:" + mediaId + "  playbillId:" + playbillId + "  position:" + position
        );
      }

      /************************************************************************/

      $("#fast_control_name").text("");
      $("#play_icon").removeClass("bx-play").addClass("bx-pause");

      if (typeof progress !== "undefined") clearInterval(progress);
      status_play = true;
      progress = setInterval(this.tick.bind(this), 1000);
    },

    onStop: function () {
      //console.log("STOP STOP STOP");
      //console.log("GRESKA 7");
      player.pause();
      $("#play_icon").removeClass("bx-pause").addClass("bx-play");
      status_play = false;
      if (typeof progress !== "undefined") clearInterval(progress);
    },

    tick: function () {
      // console.log("tick");

      // console.log('this.sget("value")', this.sget("value"));
      // console.log('this.sget("max")', this.sget("max"));

      if (this.sget("min") < this.sget("value") && this.sget("max") > this.sget("value")) {
        var tmp_slider_val = this.sget("value");

        // console.log(Number(tmp_slider_val) + 1);
        ////console.log("tmp_slider_val", tmp_slider_val);
        $("#slider").slider("value", Number(tmp_slider_val) + 1);
        ////console.log(this.sget( "value"));
        ////console.log(this.sget( "steap"));

        // console.log("XXX", this.sget("value"));
        this.update(this.sget("value"));
      }
    },

    onClose: function () {
      //console.log("brisi");
      if (typeof progress !== "undefined") clearInterval(progress);
      clearTimeout(hide_screen);

      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return SliderView;
});

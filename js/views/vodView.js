define([
  "jquery",
  "underscore",
  "backbone",
  "collections/vodCollections",
  "models/screen_focus",
  "views/listRowsView",
  "text!page/vod.html"
], function ($, _, Backbone, VodCollections, screen_focus, ListRowsView, template) {
  var st_complete = "";
  var vodColl = {};

  var VodView = Backbone.View.extend({
    tagName: "span",

    /*
     action_type    
     1 - player
     2 - preview
     3 - children
     4 - vod_content


     vr_media 
     1 - live
     2 - catchup
     3 - catchup_show
     4 - vod
     5-  vod_tvshow
    */

    initialize: function (options) {
      this.config = {
        first: {
          size: "plg",
          left_shift: -23.4375,
          progress: 0,
          position_back: "",
          action_type: "vod_content",
          image: "posters"
        },
        other: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "vod_content",
          image: "posters"
        }
      };

      // potrebno za Back
      this.roots = [];

      this.bus = options.bus;
      this.bus.on("RenderChildren", this.onRenderVodChildren, this);
      this.bus.on("VodClose", this.onClose, this);
    },

    onRenderVodChildren: function (item) {
      this.config = {
        first: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "vod_content",
          image: "posters"
        },
        other: {
          size: "pmd",
          left_shift: -7.8125,
          progress: 0,
          position_back: "",
          action_type: "vod_content",
          image: "posters"
        }
      };

      console.log(item);
      this.onClose();
      this.create(item);
    },

    create: function (item) {
      // potrebno za Back
      // typeof item.roots !== "undefined"

      loaderShow(true);

      if (item.hasOwnProperty("roots")) this.roots = item.roots;

      vodColl = new VodCollections({ ID: item.ID, config: this.config });
      console.log(vodColl);
      var self = this;
      vodColl.init().then(function () {
        vodColl.getFirst(3).then(function (res) {
          console.log(res);
          self.collection = res;
          console.log(self.collection);
          self.render();
          loaderShow(false);
        });
      });
    },

    setAttribute: function (data) {},

    render: function () {
      console.log(vodColl);

      var listRowsView = new ListRowsView({
        el: "#other_container",
        template: template,
        collObj: vodColl,
        collect: this.collection,
        config: this.config,
        bus: this.bus,
        roots: this.roots // potrebno za Back
      });

      $("#other").append(listRowsView.render().el);
      /*
      var tag = StartListCollections.at(0);
      this.$el.find("#control_" + tag.get("id_list")).addClass("focus");
      this.$el.find("#control_" + tag.get("id_list")).focus();
      */

      return this;
    },

    onClose: function () {
      this.undelegateEvents();
      this.$el.removeData().unbind();

      this.$el.empty();
      this.unbind();
      this.stopListening();
      return this;

      //this.remove();
    }
  });

  return VodView;
});

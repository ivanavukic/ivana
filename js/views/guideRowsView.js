define([
    'jquery',
    'underscore',
    'backbone',
    'scrollintoview',
    'views/guidelistView'
], function($, _, Backbone, scrollintoview, ListView, template) {

    var st_complete = "";

    var GuideRowsView = Backbone.View.extend({

        tagName: "span",

        initialize: function(options) {
            this.id_channel = 1;
            this.id_channel_load = 1;

            this.tmp_LstCollect = options.LstCollect;
            this.LstCollect = this.tmp_LstCollect.clone();
            this.Collecti = options.Collect;
            this.template = options.template;
            this.bus = options.bus;

            this.bus.off('GuideListUp').on("GuideListUp", this.onGuideListUp, this);
            this.bus.off('GuideListDown').on("GuideListDown", this.onGuideListDown, this);


            document.getElementById('id_epg_content').scrollTop = 300;

        },


        onGuideListDown: function() {
            if (st_complete !== "1") {


                if (this.id_channel < this.id_channel_load - 1) {


                    this.id_channel = this.id_channel + 1;


                    if (this.id_channel_load - this.id_channel <= 10) this.render_item(10);

                    //$('#id_epg_content').scrollintoview({ duration: 100 });
                    //this.scrollToMiddle('#ch_epg_' + (this.id_channel));


                    this.scrollToMiddle2('id_epg_content', 'ch_epg_' + (this.id_channel));


                    $('#guidetv').parent().find('div[epg_ch_focus]').removeClass("epg_ch_focus");
                    $('#epg_ch_focus_' + this.id_channel).addClass('epg_ch_focus');

                    $('#ch_epg_' + this.id_channel).focus();


                    this.bus.trigger("setFooteritem", { id_channel: this.id_channel });
                }
            }

            //this.$el.find('#middle_list').attr('id_channel', this.id_channel);
        },

        onGuideListUp: function() {
            if (st_complete !== "1") {
                if (this.id_channel > 1) {
                    this.id_channel = this.id_channel - 1;

                    //$('#ch_epg_' + (this.id_channel)).scrollintoview({ duration: 100 });

                    this.scrollToMiddle2('id_epg_content', 'ch_epg_' + (this.id_channel));
                    //$('#ch_epg_' + (this.id_channel)).scrollintoview({ duration: 100 });

                    $('#guidetv').parent().find('div[epg_ch_focus]').removeClass("epg_ch_focus");
                    $('#epg_ch_focus_' + this.id_channel).addClass('epg_ch_focus');
                    $('#ch_epg_' + this.id_channel).focus();

                    this.bus.trigger("setFooteritem", { id_channel: this.id_channel });
                } else {
                    this.bus.trigger("EpgSelectCategory");
                    return;
                }

            }

            //this.$el.find('#middle_list').attr('id_channel', this.id_channel);
        },


        scrollToMiddle2: function(containerID, elID) {


            // If element does not Exist then return
            var el = document.getElementById(elID);
            if (el == null) return;

            // If container does not Exist then return
            var container = document.getElementById(containerID);
            if (container == null) return;

            // Position container at the top line then scroll el into view
            container.scrollTop = 0;
            el.scrollIntoView(true);

            // Scroll back nothing if element is at bottom of container else do it
            // for half the height of the containers display area
            var scrollBack = (container.scrollHeight - container.scrollTop <= container.clientHeight) ? 0 : (container.clientHeight / 2) - 45;
            container.scrollTop = container.scrollTop - scrollBack;

            //console.log(middle_height + "px");
        },


        scrollToMiddle: function(id) {

            /*
                        
                        const elementRect = element.getBoundingClientRect();
                        const absoluteElementTop = elementRect.top + window.pageYOffset;
                        const middle = absoluteElementTop - (window.innerHeight / 2);
                        window.scrollTo(0, middle);

                        console.log('elementRect.top', elementRect.top);
                        console.log('pageYOffset', window.pageYOffset);
                        console.log('innerHeight', window.innerHeight);
            */

            const myDiv = document.getElementById('id_epg_content');
            console.log('elementRect.top', myDiv.scrollTop);



            /*
            var elem_position = $(id).offset().top;
            var window_height = $(window).height();
            var y = elem_position - (window_height / 2) + 95;
            window.scrollTo(0, y);
            */

            // document.getElementById("id_epg_content").scrollIntoView();

            // $('#id_epg_content').animate({ scrollTop: '230px' }, 'slow');


            //$('#id_epg_content').scrollTo({ top: 100 });

            document.getElementById('id_epg_content').scrollTop = 300;
            var objDiv = document.getElementById("id_epg_channel_content");
            objDiv.scrollTop = objDiv.scrollHeight;

        },


        render: function() {

            var templ = _.template(this.template);
            var html = templ();
            this.$el.html(html);

            this.render_item(20);

            $('#guidetv').parent().find('div[epg_ch_focus]').removeClass("epg_ch_focus");
            $('#epg_ch_focus_' + this.id_channel).addClass('epg_ch_focus');
            $('#ch_epg_' + this.id_channel).focus();

            return this;
        },


        render_item: function(page) {


            //var Collecti = this.Collecti;
            //var bxxx = this.bus;


            _(this.LstCollect.models.splice(0, page)).each(function(data) {

                $(".channel-content").append("<div id='c_epg_" + data.id + "'></div>");

                var channel = data;

                var epg_list_tmp = new Backbone.Collection(this.Collecti.filter(function(model) {
                    return model.get('id_channel') == data.id;
                }));

                var epg_min_1 = epg_list_tmp.min(function(player) {
                    return player.get('id');
                }).toJSON()['id'];

                var epg_max_1 = epg_list_tmp.max(function(player) {
                    return player.get('id');
                }).toJSON()['id'];

                this.listView = new ListView({
                    el: "#c_epg_" + data.id,
                    id_channel: data.id,
                    channel: channel,
                    epg_min: epg_min_1,
                    epg_max: epg_max_1,
                    bus: this.bus
                });
                this.listView.render();

                this.id_channel_load = this.id_channel_load + 1;

            }, this);



            //$('#guidetv').height($(document).height());


            /*
                        console.log('#', this.LstCollect.size());

                        this.LstCollect.each(function(data) {

                            $(".channel-content").append("<div id='c_epg_" + data.id + "'></div>");


                            var channel = data;

                            var epg_list_tmp = new Backbone.Collection(this.Collecti.filter(function(model) {

                                return model.get('id_channel') == data.id;
                            }));

                            var epg_min_1 = epg_list_tmp.min(function(player) {
                                return player.get('id');
                            }).toJSON()['id'];

                            var epg_max_1 = epg_list_tmp.max(function(player) {
                                return player.get('id');
                            }).toJSON()['id'];

                            this.listView = new ListView({
                                el: "#c_epg_" + data.id,
                                id_channel: data.id,
                                channel: channel,
                                epg_min: epg_min_1,
                                epg_max: epg_max_1,
                                bus: this.bus
                            });
                            this.listView.render();


                            data.destroy();

                        }, this);

                        */

            //console.log('##', this.LstCollect.size());
        }



    });

    return GuideRowsView;
});